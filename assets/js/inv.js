var inv = new Vue({
    el:'#inv',
    data:{
        versionCode:[],
        name:''
    },
    mounted:function(){
        this.getVersionCode();
    },
    computed:{
        version:function(){
            let ver = []
            this.versionCode.map(v=>v.vchVersion).forEach(
                vs=>{
                    if(!ver.includes(vs)){
                        ver.push(vs)
                    }
                }
            )
            return ver
        }
    },
    methods:{
        getVersionCode:function(){
            return $.ajax({
                url:'./orderfunc.asp',
                data:{
                    action:'getVersionCode'
                }
            })
            .done(function(data){
                this.versionCode = data
            }.bind(this))
        },
        createActivate:function(){
            return $.ajax({
                url:'./orderfunc.asp',
                method:'POST',
                data:{
                    action:'createActivate',
                    name:this.name,
                    code:this.version.join()
                }
            })
            .done(function(data){
                this.name = ''
                this.getVersionCode()
            }.bind(this))
        }
    }
})