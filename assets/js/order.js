const stateList = [
                    {value:"AL",text:'Alabama (AL)'},
                    {value:"AK",text:'Alaska (AK)'},
                    {value:"AZ",text:'Arizona (AZ)'},
                    {value:"AR",text:'Arkansas (AR)'},
                    {value:"CA",text:'California (CA)'},
                    {value:"CO",text:'Colorado (CO)'},
                    {value:"CT",text:'Connecticut (CT)'},
                    {value:"DE",text:'Delaware (DE)'},
                    {value:"DC",text:'District Of Columbia (DC)'},
                    {value:"FL",text:'Florida (FL)'},
                    {value:"GA",text:'Georgia (GA)'},
                    {value:"HI",text:'Hawaii (HI)'},
                    {value:"ID",text:'Idaho (ID)'},
                    {value:"IL",text:'Illinois (IL)'},
                    {value:"IN",text:'Indiana (IN)'},
                    {value:"IA",text:'Iowa (IA)'},
                    {value:"KS",text:'Kansas (KS)'},
                    {value:"KY",text:'Kentucky (KY)'},
                    {value:"LA",text:'Louisiana (LA)'},
                    {value:"ME",text:'Maine (ME)'},
                    {value:"MD",text:'Maryland (MD)'},
                    {value:"MA",text:'Massachusetts (MA)'},
                    {value:"MI",text:'Michigan (MI)'},
                    {value:"MN",text:'Minnesota (MN)'},
                    {value:"MS",text:'Mississippi (MS)'},
                    {value:"MO",text:'Missouri (MO)'},
                    {value:"MT",text:'Montana (MT)'},
                    {value:"NE",text:'Nebraska (NE)'},
                    {value:"NV",text:'Nevada (NV)'},
                    {value:"NH",text:'New Hampshire (NH)'},
                    {value:"NJ",text:'New Jersey (NJ)'},
                    {value:"NM",text:'New Mexico (NM)'},
                    {value:"NY",text:'New York (NY)'},
                    {value:"NC",text:'North Carolina (NC)'},
                    {value:"ND",text:'North Dakota (ND)'},
                    {value:"OH",text:'Ohio (OH)'},
                    {value:"OK",text:'Oklahoma (OK)'},
                    {value:"OR",text:'Oregon (OR)'},
                    {value:"PA",text:'Pennsylvania (PA)'},
                    {value:"RI",text:'Rhode Island (RI)'},
                    {value:"SC",text:'South Carolina (SC)'},
                    {value:"SD",text:'South Dakota (SD)'},
                    {value:"TN",text:'Tennessee (TN)'},
                    {value:"TX",text:'Texas (TX)'},
                    {value:"UT",text:'Utah (UT)'},
                    {value:"VT",text:'Vermont (VT)'},
                    {value:"VA",text:'Virginia (VA)'},
                    {value:"WA",text:'Washington (WA)'},
                    {value:"WV",text:'West Virginia (WV)'},
                    {value:"WI",text:'Wisconsin (WI)'},
                    {value:"WY",text:'Wyoming (WY)'}
                ]
const noteList=[
    "Replacement Guides",
    "Guides increased",
    "Guides activated",
    "Guides reactivated",
    "Guides lost in transit"
]
var order = new Vue({
    el:'#dashboard',
    data:{
        orders:[],
        stockUpdate:false,
        versionCode:[],
        selOrder:null,
        editing:false,
        sending:false,
        obj:{
            propName:'',
            address1:'',
            address2:'',
            city:'',
            state:'',
            zip:'',
            qty:1,
            guide:null,
            note:'Replacement Guides',
            otherNote:''
        },
        stateList:stateList,
        noteList:noteList,
        ordersField:[
            {key:'intID',label:'Shipment'},
            {key:'vchCompany',label:'Property Name'},
            {key:'vchAddress1',label:'Address 1'},
            {key:'vchAddress2',label:'Address 2'},
            {key:'vchCity',label:'City'},
            {key:'vchZip',label:'Zip'},
            {key:'intQuantity',label:'Quantity'},
            {
                key:'guideVersion',
                label:'Guide Version'
            },
            {key:'vchNote',label:'Notes'},
            {key:'vchNote',label:'Notes'},
            {key:'button',label:''}
        ]
    },
    mounted:function(){
        this.getOrders()
        this.getVersionCode()
        this.getProductAvail()
    },
    computed:{
        editHasOther(){
            return this.selOrder?!this.noteList.includes(this.selOrder.vchNote):null
        },
        invList(){
            let inv = []
            this.versionCode.forEach(i => {
                if(!(inv.map(n=>n.intInvID).includes(i.intInvID))){
                    let codeVal = this.versionCode.filter(v=>v.intInvID===i.intInvID).map(v2=>v2.vchCode).reduce((tot,val)=>tot+'/'+val)
                    let currQtyVal= this.orders.filter(o=>o.intInvID===i.intInvID).map(o2=>o2.intQuantity).reduce((tot,val)=>tot+(val*50),0)
                    inv.push({
                        intInvID:i.intInvID,
                        vchPartNumber:i.vchPartNumber,
                        intStock:i.intStock*50,
                        name:codeVal+' '+i.vchPartNumber,
                        code:codeVal,
                        currQty: currQtyVal
                    })
                }
            });
            return inv
        },
        invFields(){
            return [
                {key:'name',label:'Item name'},
                {key:'intStock',label:'Current Stock'},
                {key:'currQty',label:'Requested Amount'}
            ]
        }
    },
    methods:{
        formatVC(item){
            let vc = this.versionCode.filter(v=>v.intInvID===item.intInvID)
            return vc.map(v2=>v2.vchCode).reduce((tot,val)=>tot+'/'+val)+' - '+vc[0].vchVersion+' - '+item.vchItemName.replace(' - '+vc[0].vchVersion,'')
        },
        getVersionCode:function(){
            return $.ajax({
                url:'./orderfunc.asp',
                data:{
                    action:'getVersionCode'
                }
            })
            .done(function(data){
                this.versionCode = data
            }.bind(this))
        },
        getOrders:function(){
            return $.ajax({
                url:'./orderfunc.asp',
                data:{
                    action:'getOrders'
                }
            })
            .done(function(data){
                this.orders=data
            }.bind(this))
        },
        getProductAvail(){
            this.stockUpdate = true;
            return $.ajax({
                url:'./orderfunc.asp',
                data:{
                    action:'getProductAvail'
                }
            })
            .always(function(data){
                this.stockUpdate=false;
            }.bind(this))
        },
        addOrder:function(){
            var vm = this
            var inv = this.versionCode.find(function(i){return i.intID==vm.obj.guide})
            return $.ajax({
                url:'./orderfunc.asp',
                data:{
                    action:'addOrder',
                    propName:this.obj.propName,
                    address1:this.obj.address1,
                    address2:this.obj.address2,
                    city:this.obj.city,
                    state:this.obj.state,
                    zip:this.obj.zip,
                    intInvID:inv.intInvID,
                    vchItemName:inv.vchItemName,
                    vchPartNumber:inv.vchPartNumber,
                    intQuantity:this.obj.qty,
                    vchNote:this.obj.note?this.obj.note:this.obj.otherNote
                }
            })
            .done(function(){
                this.obj={
                    propName:'',
                    address1:'',
                    address2:'',
                    city:'',
                    state:'',
                    zip:'',
                    qty:1,
                    guide:null,
                    note:'Replacement Guides',
                    otherNote:''
                }
                this.getOrders()

            }.bind(this))
        },
        sendOrder:function(){
            if(this.orders.length>0){
                this.sending=true;
                $.ajax({
                    url:'./orderfunc.asp',
                    method:'POST',
                    data:{
                        action:'sendVera',
                        orders:this.orders.map(function(o){return o.intID})
                    }
                })
                .done(function(data){
                    this.getOrders().done(()=>{
                        this.sending=false;
                        this.getProductAvail()
                        this.getVersionCode()
                    });
                }.bind(this))
            }
        },
        deleteOrder:function(orderID){
            if(confirm('Are you sure you want to delete this order?')){
                return $.ajax({
                    url:'./orderfunc.asp',
                    method:'POST',
                    data:{
                        action:'deleteOrder',
                        orderID:orderID
                    }
                })
                .done(function(data){
                    this.getOrders();
                }.bind(this))
            }
        },
        editOrder:function(order){
            this.selOrder=JSON.parse(JSON.stringify(order))
            this.selOrder.otherNote=!this.noteList.includes(order.vchNote)?order.vchNote+'':null
            this.$bvModal.show('edit')
        },
        invChange:function(){
            this.selOrder.vchItemName=this.versionCode.find(v=>v.intInvID===this.selOrder.intInvID).vchItemName
            this.selOrder.vchPartNumber=this.versionCode.find(v=>v.intInvID===this.selOrder.intInvID).vchPartNumber
        },
        sendEdit:function(){
            this.editing = true;
            if(this.selOrder)
            $.ajax({
                url:'./orderfunc.asp',
                method:'POST',
                data:{
                    action:'editOrder',
                    order:{
                        vchCompany:this.selOrder.vchCompany,
                        vchAddress1:this.selOrder.vchAddress1,
                        vchAddress2:this.selOrder.vchAddress2,
                        vchCity:this.selOrder.vchCity,
                        vchState:this.selOrder.vchState,
                        vchZip:this.selOrder.vchZip,
                        intID:this.selOrder.intID,
                        intInvID:this.selOrder.intInvID,
                        vchItemName:this.selOrder.vchItemName,
                        vchPartNumber:this.selOrder.vchPartNumber,
                        intQuantity:this.selOrder.intQuantity,
                        vchNote:this.selOrder.otherNote?this.selOrder.otherNote:this.selOrder.vchNote                      
                    }
                }
            })
            .done(function(data){
                this.editing=false;
                this.$bvModal.hide('edit');
                this.getOrders();
            }.bind(this))
        }
    }
})