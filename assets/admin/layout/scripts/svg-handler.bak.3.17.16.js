var allIds = [];
var url = "../config/ajax_functions.asp";

var incomplete = "rgba(192,192,192,.50)";
var complete = "rgba(92,184,92,.70)";

$(document).ready()
{
	loadProfileDropdown();
	loadRightSide();
}

function loadMap(){

	var layoutImage = "";
	var profileID = 0;
	profileID = $("#profileDropdown").val();
	$.ajax({
		type: "POST",
		url: url,
		async:false,
		data: "action=loadLayout&profileID=" + profileID, // serializes the form's elements.
		success: function(data)
		{
			var actualData = data[0];
			layoutImage = actualData["vchImageLocation"];
			$("#layoutID").html(actualData["intID"]);
		},
		error: function(data)
		{
			console.log(data);
		}
	});

	$("#stage").load('../images/layouts/' + layoutImage,function(response){

		$(this).addClass("svgLoaded");
		
		var allItems = document.getElementsByTagName("g");
		
		
		for (i=0;i<allItems.length;i++) {
			if (allItems[i].id != "Layer_1" && allItems[i].id != "Layer_2" && allItems[i].id != "") {
				allIds.push(allItems[i].id);
				
			}
		}
		for (i=0;i<allIds.length;i++) {
			$("#" + allIds[i]).attr( "onclick", "loadItemSlot(" + $("#profileDropdown").val() + ", " + $("#layoutID").text() + ", '" + allIds[i] + "');" );
			$("#" + allIds[i]).children().attr("fill", "rgba(192,192,192,50)");
			$("#" + allIds[i]).children("text").removeAttr("fill");
			$("#" + allIds[i]).attr( "style", "cursor: pointer;" );
		}
		
		$(".h3-" + profileID).click();
		
		if(!response){
			// Error loading SVG!
			// Make absolutely sure you are running this on a web server or localhost!
		}

	});
}

function loadItemSlot(profileID, layoutID, slotName)
{
	$.ajax({
		type: "POST",
		url: url,
		async:true,
		data: "action=loadItemSlot&layoutID=" + layoutID + "&slotName=" + slotName + "&campaignID=" + $("#campaignID").text() + "&profileID=" + $("#profileDropdown").val(), // serializes the form's elements.
		success: function(data)
		{
			var message = "";
			var itemName = "";
			var itemImage = "";
			var savedItemName = "";
			var itemSizes = "";
			var oldSides = ""
			var actualData = data[0];
			itemName = actualData["vchItemName"];
			itemImage = actualData["vchImageURL"];
			oldSides = actualData["oldSides"];
			savedItemName = actualData["savedItemName"];
			itemSizes = actualData["vchOptionList2"].split(",");
			if (savedItemName == null)
			{
				savedItemName = "";
			}
			if (oldSides == null)
			{
				oldSides = "";
			}
			message += "<div style=\"float: left; width:300px; padding: 30px\">";
			message += "<img src=\"../images/Products/" + itemImage + "\" width=\"250px\" class=\"img-thumbnail\"> </div>";
			message += "<div style=\"float: left; padding: 30px\" > <form id=\"createItem\" name=\"createItem\" class=\"form-horizontal\"> <input type=\"hidden\" name=\"profileID\" value=\"" + profileID + "\"> <input type=\"hidden\" name=\"slotName\" value=\"" + slotName + "\"> <input type=\"hidden\" name=\"layoutID\" value=\"" + layoutID + "\"> <input type=\"hidden\" name=\"itemID\" value=\"" + actualData["intItemID"] + "\">";
			message += "<div class=\"form-group\"><label for=\"textfield\" style=\"display: inline-block; float: left; clear: left; width: 150px; text-align: left;\">Fixture Location:</label> <input type=\"text\" name=\"slotNamePair\" id=\"slotNamePair\" value=\"" + slotName + " - " + itemName + "\" style=\"outline:none; display: inline-block; float: left; \" class=\"form-control\" readonly></div>";
			message += "<div class=\"form-group\"><label for=\"textfield\" style=\"display: inline-block; float: left; clear: left; width: 150px; text-align: left;\">Sides:</label><select name=\"vchNumSides\" id=\"vchNumSides\" class=\"form-control\">";
				message += "<option>Number of Sides</option>";
                for (i=0;i<itemSizes.length;i++)
				{
                    message += "<option value=\"" + itemSizes[i] + "\" "
					if (itemSizes[i] == oldSides)
					{
						message += " selected ";
					}
					message += ">" + itemSizes[i] + "</option>";
                }
            message += "</select></div>";
			message += "<div class=\"form-group\"><label for=\"textfield\" style=\"display: inline-block; float: left; clear: left; width: 150px; text-align: left;\">" + itemName + " Name:</label> <input type=\"text\" name=\"vchItemName\" id=\"vchItemName\" style=\"display: inline-block; float: left; \" value=\"" + savedItemName + "\" class=\"form-control\"></div><br /> </form> </div>";
			//message += "<img src=\"../images/Products/" + itemImage + "\" width=\"250px\" />" + itemName + "<input type=\"text\" name=\"vchItemName\" id=\"vchItemName\" /></form>";
			$("#itemDialog").html(message);
			$( "#itemDialog" ).dialog({
				modal: true,
				draggable: false,
				resizable: false,
				width: "auto",
				minHeight: 400,
				dialogClass: "signageTitle",
				buttons: [
					{
						text: "Save",
						"class":"btn btn-default",
						click: function() {
							saveItem();
							loadOverview($("#campaignID").text(), profileID)
							$( this ).dialog( "close" );
							return false;
						}
					}
				]
			});
		},
		error: function(data)
		{
			console.log(data);
		}
	});
}

function saveItem ()
{

	$.ajax({
		type: "POST",
		url: url,
		async:false,
		data: "action=saveItemSlot&campaignID=" + $("#campaignID").text() + "&" + $("#createItem").serialize(), // serializes the form's elements.
		success: function(data)
		{
			
		},
		error: function(data)
		{
			console.log(data);
		}
	});

}

function loadRightSide()
{
	var message = "";
	message += "<center><h1>Overview</h1></center>";
	
	$.ajax({
		type: "POST",
		url: url,
		async:true,
		data: "action=loadProfiles", // serializes the form's elements.
		success: function(data)
		{
			message += "<div id=\"accordion\">"
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					var actualData = data[key];
						
					message += "<h3 class=\"h3-" + actualData["intID"] + "\" style=\"font-size:11pt;\" onclick=\"javascript:loadOverview(" + $("#campaignID").text() + ", " + actualData["intID"] + ");\"><strong>" + actualData["vchProfileName"] + "</strong></h3>";
					message += "<div><a id=\"" + $("#campaignID").text() + "-" + actualData["intID"] +"\"></a>"
				
					message += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"campaignidProfileID-" + $("#campaignID").text() + "-" + actualData["intID"] +"-table\" class=\"table table-hover table-condensed\" >";
	              	                   
					message += "</table>";
					message += "</div>";
					
				}
			};
			message += "</div>";
			$("#overview").html(message);
			$( "#accordion" ).accordion({
				collapsible: true,
				heightStyle: "content",
				active: false
			});
		},
		error: function(data)
		{
			console.log(data);
		}
		
	});
}

function loadProfileDropdown ()
{
	var dialOptions = "";
	dialOptions = "<option selected=\"selected\" value=\"\">-- Please Select a Profile --</option>";
	var count = 0;
	
	$.ajax({
		type: "POST",
		url: url,
		async:true,
		data: "action=loadProfiles", // serializes the form's elements.
		success: function(data)
		{
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					var actualData = data[key];
					
					dialOptions += "<option value=\"" + actualData["intID"] + "\"" 
					if (count == 0)
					{
						dialOptions += " selected ";
					}
					dialOptions += ">" + actualData["vchProfileName"] + "</option>";					
				}
				count++;
			};
			$("#profileDropdown").html(dialOptions);
			loadMap();
		},
		error: function(data)
		{
			console.log(data);
		}
	});
}

function loadOverview(campaignID, profileID)
{

	$.ajax({
		type: "POST",
		url: url,
		async:true,
		data: "action=loadOverview&campaignID=" + campaignID + "&profileID=" + profileID, // serializes the form's elements.
		success: function(data)
		{
			var message = ""; 
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					var actualData = data[key];
					var color = ""
					var itemID = actualData["ItemID"];
					if ($.isNumeric(itemID))
					{
						color = complete;
					}
					else
					{
						color = incomplete;
					}
					message += "<tr onclick=\"loadItemSlot(" + $("#profileDropdown").val() + ", " + $("#layoutID").text() + ", '" + actualData["vchSlotName"] + "'); \" style=\"cursor: pointer;\">";
					
						message += "<td style=\"background-color: " + color + "\">&nbsp;</td>";
						message += "<td>" + actualData["vchSlotName"] +"</td>";
						message += "<td>" + actualData["ItemName"] +" ";
						if (color == complete)
						{
							var sides = ""; 
							if (actualData["vchSides"] == "1")
							{
								sides = "Side";
							}
							else
							{
								sides = "Sides";
							}
							message += "(" + actualData["vchSides"] + "-" + sides + ")";
						}
						message += "</td>";
					message += "</tr>";
					if (profileID == $("#profileDropdown").val())
					{
						$("#" + actualData["vchSlotName"]).children().attr("fill", color);
						$("#" + actualData["vchSlotName"]).children("text").removeAttr("fill");
					}
				}
			}
			//console.log(data);
			$("#campaignidProfileID-" + campaignID + "-" + profileID +"-table").html(message);
		},
		error: function(data)
		{
			console.log(data);
		}
	});

}