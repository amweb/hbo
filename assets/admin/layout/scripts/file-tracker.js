$(document).ready(function(){
	$("#popout").dialog({
		open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).hide(); },
		draggable: false,
		resizable: false,
		modal: true,
		dialogClass: "noTitleStuff",
		maxHeight: screen.height,
		minWidth : 500
	});
	/** FileTracker Landing **/
	$("select[name='campaignid']").change(function(){
		//var landingformhtml = $("#campaigndropdown").html();
		var landingformhtml = "";
		var selectedID = $("#campaignid").val();
		//console.log($(this).html());
		//$("#campaigndropdown").submit();
		$.ajax({
			url: AJAXurl,  //Server script to process data
			type: 'POST',
			data: "action=loadProfilesFTLanding&campaign=" + selectedID,
			async:false,
			error: function(data, err){
				console.log("error: " + err);
				console.log(data);
			},
			success: function(data, err){
				//landingformhtml += "<select name='profileid' id='profileid'>";
				for (var key in data) {
					if (data.hasOwnProperty(key)) {
						var actualData = data[key];
						landingformhtml += "<option>Select a profile</option>";
						landingformhtml += "<option value='" + actualData["intID"] + "'";
						if(actualData["intID"] == parseInt(selectedID)){
							landingformhtml += " selected";
						}
						landingformhtml += ">" + actualData["vchProfileName"] + "</option>";
					}
				}
				//landingformhtml += "</select>";
				$("#profileid").html(landingformhtml);
				$("#profileid").show();
			}
		});
	});
	$("#profileid").change(function(){
		$(".buttoncontainer button[name='continue']").removeClass("disabled");
		$(".buttoncontainer button[name='continue']").prop("disabled", false);
	});
	$(document).on("click", "#campaigndropdown button[name='backtohome']", function(){
		window.location.assign("../store/dashboard.asp");
	});
	
	
	/** individual item image popout **/
	$("a.imageexpand").fancybox({
		type: 'image',
		arrows: false,
		autoSize: true,
		maxWidth: 500,
		maxHeight: 425,
		fitToView: false,
		beforeShow: function() {
			var alt = this.element.find('img').attr('alt');
			var url = this.element.attr("href");
			this.inner.find('img').attr('alt', alt);
			//var externalurl = "<br><a href='" + url + "' target='_blank'>Open this image in a new tab</a>. <i class='fa fa-external-link' aria-hidden='true'></i>"
			//this.title = alt + externalurl;
			this.title = alt;
		}
	});
	$("button[name='uploadthumnail']").click(function(event){
		event.preventDefault();
		var intItemID = $(this).attr("data-itemid");
		$("form.individualitemupload[data-itemid='" + intItemID + "']").dialog({
			open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).hide(); },
			draggable: false,
			resizable: false,
			modal: true,
			dialogClass: "noTitleStuff",
			maxHeight: screen.height,
			minWidth : 600
		});
	});
	$("a.campaignthumbnailzoom").fancybox({
		type: 'image',
		arrows: false,
		autoSize: true,
		maxWidth: 500,
		maxHeight: 425,
		fitToView: false,
		beforeShow: function() {
			var alt = this.element.find('img').attr('alt');
			var url = this.element.attr("href");
			this.inner.find('img').attr('alt', alt);
			//var externalurl = "<br><a href='" + url + "' target='_blank'>Open this image in a new tab</a>. <i class='fa fa-external-link' aria-hidden='true'></i>"
			//this.title = alt + externalurl;
			this.title = alt;
		}
	});
	
	/* thumbnail uploader */
	$("button[name='uploadthumnailsubmit']").each(function(){
		$(this).click(function(event){
			event.preventDefault();
			var intObjectID = $(this).parents("form").attr("data-itemid");
			
			var objFormToSubmit = $("form[data-itemid='" + intObjectID + "']");
			var filesToUpload = $("form[data-itemid='" + intObjectID + "'] input[name='artwork']")[0].files[0];
			var objProgressBar = $(".progress[data-progressbar-itemid=" + intObjectID + "]");
			
			
			var jsFormData = new FormData();
			jsFormData.append("Thumbnail", filesToUpload, filesToUpload.name);
			jsFormData.append("id", intObjectID);
			
			var fd = new FormData();
			for (var i = 0; i < 3000; i += 1) {
				fd.append('data[]', Math.floor(Math.random() * 999999));
			}
			
			xhr = new XMLHttpRequest();
			xhr.open("POST", AJAXurl + "?action=uploadThumbnailForFileTracker", true);
			xhr.setRequestHeader('Content-Type', filesToUpload.type);
			//xhr.open("POST", "https://zinoui.com/demo/progress-bar/upload.php", true);
			if(xhr.upload){
				xhr.upload.onprogress = function(e){
					if(e.lengthComputable){
						$(objProgressBar).attr({
							"value": e.loaded,
							"max": e.total
						});
					}
				}
				xhr.upload.onloadstart = function(e){
					$(objFormToSubmit).find("fieldset").toggle();
					$(objProgressBar).toggleClass("progress-hidden");
					$(objProgressBar).show();
					$(objProgressBar).val = 0;
					$(objProgressBar).progressbar();
				}
				xhr.upload.onloadend = function(e){
					$(objProgressBar).attr("value", e.loaded);
					objFormToSubmit[0].reset();
					$(objProgressBar).removeAttr("max");
					$(objProgressBar).val = 0;
					$(objProgressBar).hide();
					$(objFormToSubmit).dialog("close");
				}
				xhr.onload = function () {
					if (xhr.status === 200) {
						/* $(objFormToSubmit).dialog("close"); */
						location.reload();
					} else {
						console.log(xhr.responseText);
					}
				};
			}
			//xhr.send(fd);
			xhr.send(jsFormData);
		});
	});
	$("button[name='uploadthumnailreset']").each(function(){
		$(this).click(function(event){
			$("form[data-itemid='" + $(this).attr("data-itemid") + "'").dialog("close");
		});
	});
	$("button[name='approvethumbnail']").each(function(){
		$(this).click(function(){
			var intThumbnailID = $(this).attr("data-itemid");
			$.ajax({
				url: AJAXurl,  //Server script to process data
				type: 'POST',
				data: "action=approveThumbnail&itemid=" + intThumbnailID,
				async:false,
				error: function(data, err){
					console.log("error updating thumbnail " + intThumbnailID);
					console.log("error: " + err);
					console.log(data);
				},
				success: function(data, err){
					$("button[data-itemid='" + intThumbnailID + "']").toggle();
					location.reload();
				}
			});
		});
	});
	$("button[name='rejectthumbnail']").each(function(){
		$(this).click(function(){
			var intThumbnailID = $(this).attr("data-itemid");
			//console.log("Reject item id: " + intThumbnailID);
			$.ajax({
				url: AJAXurl,
				type: 'POST',
				data: "action=rejectThumbnail&itemid=" + intThumbnailID,
				async: false,
				error: function(data, error){
					console.log("Error rejecting thumbnail: " + intThumbnailID + " reason: " + error);
					console.log(data);
				},
				success: function(data, err){
					$("button[data-itemid='" + intThumbnailID + "']").toggle();
					location.reload();
				}
			});
		});
	});
	$("button[name='markasprinted']").each(function(){
		$(this).click(function(){
			var intThumbnailID = $(this).attr("data-itemid");
			//console.log("Reject item id: " + intThumbnailID);
			$.ajax({
				url: AJAXurl,
				type: 'POST',
				data: "action=printArtwork&itemid=" + intThumbnailID,
				async: false,
				error: function(data, error){
					console.log("Error rejecting thumbnail: " + intThumbnailID + " reason: " + error);
					console.log(data);
				},
				success: function(data, err){
					$("button[data-itemid='" + intThumbnailID + "']").toggle();
					location.reload();
				}
			});
		});
	});
	
	
	
	/*** Store View **/
	$("#sv_campaigndropdown").dialog({
		open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).hide(); },
		draggable: false,
		resizable: false,
		modal: true,
		dialogClass: "noTitleStuff",
		maxHeight: screen.height,
		minWidth : 500
	});
	$("#sv_nocampaigns").dialog({
		open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).hide(); },
		draggable: false,
		resizable: false,
		modal: true,
		dialogClass: "noTitleStuff",
		maxHeight: screen.height,
		minWidth : 500
	});
	try {
		loadMap();
	}
	catch(e){
		return null;
	}
	$("input[name='departmentToggler']").click(function(){
		toggleDepartments();
	});
	$("button[name='printplanningdiagram']").click(function(){
		window.print();
	});
});