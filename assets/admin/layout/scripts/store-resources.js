$(document).ready(function(){
	$(".storeresourcescategories").find("h3").click(function(event){
		var intItemID = $(this).attr("data-itemid");
		/*
		$(this).parent(0).find("i.fa").toggleClass("fa-angle-right");
		$(this).parent(0).find("i.fa").toggleClass("fa-angle-down");
		$(this).parent(0).find("ul.children").toggle();
		*/
		loadItemsIntoResourceCenter(intItemID);
	});
	$(".storeresourcescategories").accordion({
		collapsible: true,
		icons: {
			header: " ui-icon-caret-1-e",
			activeHeader: " ui-icon-caret-1-s"
		},
		active: false,
		heightStyle: "content"
	});
	$("a.fancybox").fancybox();
	
	
	
	
	
	
	
	/***** ADMIN SECTION ***************/
	//$("#storeresourcesadministration .tabs").tabs();
	//$("#storeitemsneedingattention .tabs").tabs();
	//Load available categories
	loadAvailableCategories();
	$("form[name='uploadnewfile'] select[name='category']").html(getAvailableCategoriesForUpload());
	StoreRSRCItemAdmin();
	loadItemsNeedingAttention();
	$("button[name='createcategory']").click(function(){
		$(".createcategory").dialog({
			resizeable: false,
			height: "auto",
			width: 400,
			modal: true,
			dialogClass: "noTitleStuff"
		});
	});
	$("button[name='sendnewcategory']").click(function(event){
		event.preventDefault;
		event.stopPropagation();
		createNewCategory();
		StoreRSRCItemAdmin();
		loadItemsNeedingAttention();
	});
	$("button[name='newfileupload']").click(function(event){
		event.preventDefault;
		event.stopPropagation();
		uploadNewFileToRSRC();
		StoreRSRCItemAdmin();
		loadItemsNeedingAttention();
	});
	$(document).on("click", ".individualitemedit[data-action='edit']", function(event){
		event.preventDefault;
		event.stopPropagation();
		editExistingItem($(this).attr("data-itemid"));
		$("form[name='alterindividualitem']").dialog({
			resizeable: false,
			height: "auto",
			width: 400,
			modal: true,
			dialogClass: "noTitleStuff"
		});
	});
	$(document).on("click", ".individualitemedit[data-action='delete']", function(){
		event.preventDefault;
		event.stopPropagation();
		$(".deleteindividualitem button[name='deleteindividualitem']").attr("data-itemid", $(this).attr("data-itemid"));
		$(".deleteindividualitem").dialog({
			resizeable: false,
			height: "auto",
			width: 400,
			modal: true,
			dialogClass: "noTitleStuff"
		});
	});
	$(".deleteindividualitem button[name='deleteindividualitem']").click(function(event){
		event.preventDefault;
		event.stopPropagation();
		deleteExistingItem($(this).attr("data-itemid"));
		//console.log("Delete item: " + $(this).attr("data-itemid"));
		//$(".deleteindividualitem").dialog("close");
	});
	$(".deleteindividualitem button[name='closedialog']").click(function(event){
		$(".deleteindividualitem").dialog("close");
	});
	$("form[name='alterindividualitem'] button[name='updateitem']").click(function(event){
		updateExistingItem($(this).attr("data-intid"), "alterindividualitem");
		StoreRSRCItemAdmin();
	});
	$("form[name='alterindividualitem'] button[name='reset']").click(function(event){
		$("form[name='alterindividualitem']").dialog("close");
	});
	
	$(".createcategory button[name='resetnewcategory']").click(function(event){
		$(".createcategory").dialog("close");
	});
	
	$("form[name='attentionform'] select").html(getAvailableCategoriesForUpload());
	$("form[name='attentionform'] button[name='updateitem']").click(function(event){
		event.preventDefault;
		event.stopPropagation();
	});
	$("form[name='attentionform'] button[name='reset']").click(function(event){
		event.preventDefault;
		$("form[name='attentionform']").dialog("close");
	});
	
	$(document).on("click", "#storeitemsneedingattention .tabs .item .edititem a[data-action='edit']", function(event){
		event.preventDefault;
		event.stopPropagation();
		//console.log("Edit item: " + $(this).attr("data-itemid"));
		//editExistingItem($(this).attr("data-itemid"));
		
		var argIntID = $(this).attr("data-itemid");
		$.ajax({
			url: AJAXurl,
			type: "POST",
			data: "action=getItemInfo&id=" + argIntID,
			error: function(data, error){
				console.log("Error getting information for " + argIntID + ": " + error);
				console.log(data);
			},
			success: function(data){
				//console.log(data);
				$("form[name='attentionform'] legend").text("Edit " + data[0]["vchItemName"]);
				$("form[name='attentionform'] input[name='itemname']").val(data[0]["vchItemName"]);
				$("form[name='attentionform'] input[name='itemid']").val(argIntID);
				$("form[name='attentionform'] button[name='updateitem']").attr("data-intid", argIntID);
				$("form[name='attentionform']").dialog({
					resizeable: false,
					height: "auto",
					width: 400,
					modal: true,
					dialogClass: "noTitleStuff"
				});
			}
		});
	});
	$(document).on("click", "#storeitemsneedingattention .tabs .item .edititem a[data-action='delete']", function(event){
		event.preventDefault;
		event.stopPropagation();
	});
	$("form[name='attentionform'] button[name='updateitem']").click(function(event){
		updateExistingItem($(this).attr("data-intid"), 'attentionform');
		loadItemsNeedingAttention();
	});
	$("form[name='attentionform'] button[name='reset']").click(function(event){
		$("form[name='attentionform']").dialog("close");
	});
	
	
});

function loadItemsIntoResourceCenter(intParent){
	$("#storeresourcesfiles").html("");
	$.ajax({
		url: AJAXurl,  //Server script to process data
		type: 'POST',
		data: "action=loadItemsIntoResourceCenter&parent=" + intParent,
		async:false,
		error: function(data, err){
			console.log("error loading items for " + intThumbnailID + ": " + err);
			console.log(data);
		},
		success: function(data, err){
			var message = "<div id='wrapper'><div id='columns'>";
			for(var key in data){
				if(data.hasOwnProperty(key)){
					var resourceItem = data[key];
					message += "<div class='pin'>";
					message += getMIMETypeImage(resourceItem["intID"]);
					message += "<p>" + resourceItem["vchItemName"] + "<br>";
					message += "<a class='downloaditem' href='" + resourceItem["base"] + resourceItem["vchURL"] + "' download><i class='fa fa-download' aria-hidden='true'></i></a>";
					message += "</p>";
					message += "</div>";
				}
			}
			message += "</div></div>";
			$("#storeresourcesfiles").html(message);
			/*
			//$("button[data-itemid='" + intThumbnailID + "']").toggle();
			var message = "<div class='flex-container'>";
			for(var key in data){
				if(data.hasOwnProperty(key)){
					var resourceItem = data[key];
					message += "<div class='resourceitem' data-itemid='" + resourceItem["intID"] + "'>";
					message += "<span class='mimetype'>" + getMIMETypeImage(resourceItem["intID"]) + "</span>";
					message += "<span class='specification'>" + resourceItem["vchItemName"] + "</span>";
					message += "<span class='specification'><a class='downloaditem' href='" + resourceItem["base"] + resourceItem["vchURL"] + "' download><i class='fa fa-download' aria-hidden='true'></i></a></span>";
					message += "</div>";
				}
			}
			message += "</div>";
			$("#storeresourcesfiles").html(message);
			*/
			/*
			$("#storeresourcesfiles").html();
			var message = "<div class='card-columns'>";
			for(var key in data){
				if(data.hasOwnProperty(key)){
					var resourceItem = data[key];
					message += '<div class="card">';
					message += '<span class="mimetype">' + getMIMETypeImage(resourceItem["intID"]) + "</span>";
					message += '<div class="card-block">';
					message += '<h4 class="card-title">' + resourceItem["vchItemName"] + '</h4>';
					message += '<p class="card-text">' + '<a class="downloaditem" href="' + resourceItem["base"] + resourceItem["vchURL"] + '" download><i class="fa fa-download" aria-hidden="true"></i></a></p>';
					message += '</div>';
					message += '</div>';
				}
			}
			message += "</div>";
			$("#storeresourcesfiles").html(message);
			*/
		}
	});
}
function editACategory(){
	//Edit a category
	$("#storeresourcescategoryadministration ul.listallcategories li a[data-action='edit']").each(function(){
		$(this).click(function(event){
			event.preventDefault;
			var strCategoryName = $.trim($(this).parent(1).text());
			$("#storeresourcescategoryadministration .alert .edit").show();
			$("#storeresourcescategoryadministration .alert .delete").hide();
			var intID = $(this).attr("data-intid");
			var headerHTML = "";
			$(".alert .edit h3").html("Edit " + strCategoryName);
			$(".alert .edit form[name='updatecategory']").attr("data-formid", intID);
			$(".alert .edit input[name='categoryname']").attr("value", strCategoryName);
			$(".alert .edit input[name='id']").attr("value", intID);
			$(".alert .edit button[name='post']").attr("data-formid", intID);
			$(".alert .edit button[name='close']").attr("data-formid", intID);
			
			$("#storeresourcescategoryadministration .alert").attr("data-formid", intID);
			$(".alert").dialog({
				resizeable: false,
				height: "auto",
				width: 400,
				modal: true,
				dialogClass: "noTitleStuff"
			});
			$(".alert").on("click", "button[name='post']", {}, function(event){				
				event.preventDefault;
				$.ajax({
					url: AJAXurl,
					type: "POST",
					data: "action=updateSelectedCategory&" + $("form[name='updatecategory']").serialize(),
					async: false,
					error: function(data, err){
						console.log("error: " + err);
						console.log(data);
					},
					success: function(data){
						loadAvailableCategories();
						$(".alert[data-formid=" + intID + "]").dialog("close");
						StoreRSRCItemAdmin();
						loadItemsNeedingAttention();
						$("form[name='uploadnewfile'] select[name='category']").html(getAvailableCategoriesForUpload());
					}
				});
			});
			//Close the form when cancelling
			$(".alert").on("click", "button[name='close']", {}, function(event){
				event.preventDefault;
				$(".alert[data-formid=" + intID + "]").dialog("close");
			});
		});
	});
}

function deleteACategory(){
	//Delete an itemid
	$("#storeresourcescategoryadministration ul li a[data-action='delete']").each(function(){
		var strCategoryName = $.trim($(this).parent(0).text());
		$(this).click(function(){
			event.preventDefault;
			var intID = $(this).attr("data-intid");
			$("#storeresourcescategoryadministration .alert .delete").show();
			$("#storeresourcescategoryadministration .alert .edit").hide();
			$("#storeresourcescategoryadministration .alert").attr("data-intid", intID);
			$(".delete p span#strCategoryName").html(strCategoryName);
			$(".alert").dialog({
				resizeable: false,
				height: "auto",
				width: 400,
				modal: true,
				dialogClass: "noTitleStuff"
			});
			$(".delete").on("click", "button[name='delete']", {}, function(event){
				event.preventDefault;
				$.ajax({
					url: AJAXurl,
					type: "POST",
					data: "action=deleteSelectedCategory&id=" + intID,
					async: false,
					error: function(data, err){
						console.log("error: " + err);
						console.log(data);
					},
					success: function(data){
						loadAvailableCategories();
						StoreRSRCItemAdmin();
						loadItemsNeedingAttention();
						$("form[name='uploadnewfile'] select[name='category']").html(getAvailableCategoriesForUpload());
					}
				});
				$(".alert[data-intid=" + intID + "]").dialog("close");
			});
			$(".delete").on("click", "button[name='cancel']", {}, function(event){
				event.preventDefault;
				$(".alert[data-intid=" + intID + "]").dialog("close");
				loadAvailableCategories();
			});
		});
	});
}

function loadAvailableCategories(){
	$.ajax({
		url: AJAXurl,  //Server script to process data
		type: 'POST',
		data: "action=getAvailableCategories",
		async:false,
		error: function(data, err){
			console.log("error: " + err);
			console.log(data);
		},
		success: function(data, err){
			var message = "";
			for(var key in data){
				if(data.hasOwnProperty(key)){
					var resourceItem = data[key];
					message += "<li>";
					message += resourceItem["vchItemName"];
					message += "<a href='#' data-action='edit' data-intid='" + resourceItem["intID"] + "'><i class='fa fa-pencil' aria-hidden='true'></i></a>";
					message += "<a href='#' data-action='delete' data-intid='" + resourceItem["intID"] + "'><i class='fa fa-trash' aria-hidden='true'></i></a>";
					message += "</li>";
				}
			}
			$("#storeresourcescategoryadministration ul.listallcategories").html(message);
			$("form[name='uploadnewfile'] select[name='category']").html(getAvailableCategoriesForUpload());
			deleteACategory();
			editACategory();
		}
	});
}
function getAvailableCategoriesForUpload(){
	var message;
	$.ajax({
		url: AJAXurl,  //Server script to process data
		type: 'POST',
		data: "action=getAvailableCategories",
		async:false,
		error: function(data, err){
			console.log("error: " + err);
			console.log(data);
		},
		success: function(data, err){
			message = "";
			for(var key in data){
				if(data.hasOwnProperty(key)){
					var resourceItem = data[key];
					message += "<option value='" + resourceItem["intID"] + "'>" + resourceItem["vchItemName"] + "</option>";
				}
			}
			//$("#uploadnewfile select[name='category']").html(message);
		}
	});
	return message;
}

function createNewCategory(){
		$.ajax({
		url: AJAXurl,  //Server script to process data
		type: 'POST',
		data: "action=createNewCategory&" + $("form[name='createcategory']").serialize(),
		async:false,
		error: function(data, err){
			console.log("error creating new category: " + err);
			console.log(data);
		},
		success: function(data, err){
			$(".createcategory").dialog("close");
			loadAvailableCategories();
			StoreRSRCItemAdmin();
			$("form[name='uploadnewfile'] select[name='category']").html(getAvailableCategoriesForUpload());
		}
	});
}
function getMIMETypeImage(argIntID) {
	var returnHTML = "";
	$.ajax({
		url: AJAXurl,
		type: 'POST',
		data: "action=getThumbnailImage&id=" + argIntID,
		async: false,
		error: function(data, err){
			console.log("error getting thumbnail: " + err);
			console.log(data);
		},
		success: function(data, err){
			var thumbnailItem = data;
			if(thumbnailItem["vchThumbnailURL"] !== "false"){
				returnHTML += "<a href='" + thumbnailItem["vchURL"] + "' class='fancybox'"
				if(thumbnailItem["vchMIMEType"].indexOf("image") !== -1) {
					returnHTML += " download";
				}
				returnHTML += "><img src='" + thumbnailItem["vchThumbnailURL"] + "' alt='" + thumbnailItem["alt"] + "' class='resourcethumbnail'></a>";
			}else{
				var argMIMEType = thumbnailItem["vchMIMEType"];
				var MIMEhtml = '<i class="mime-status ';
				switch(argMIMEType){
					case "application/pdf":
						MIMEhtml += "fa fa-file-pdf-o";
						break;
					case "image":
						MIMEhtml += "fa fa-file-image-o";
						break;
					case "application/postscript":
						MIMEhtml += "fa fa-file-image-o";
						break;
					case "video":
						MIMEhtml += "fa fa-file-video-o";
						break;
					case "video/quicktime":
						MIMEhtml += "fa fa-file-video-o";
						break;
					default:
						break;
				}
				MIMEhtml += '" aria-hidden="true"></i>';
				returnHTML = MIMEhtml;
				return returnHTML;
			}
		}
	});
	return returnHTML;
}

// Load items into Item Administration on Store Resources admin side
function StoreRSRCItemAdmin(){
		var itemadmintab = $("#storeresourcesadministration .tabs");
		$(itemadmintab).html("");
		$.ajax({
			url: AJAXurl,
			type: "POST",
			data: "action=getItemsForAdmin",
			async: false,
			error: function(data, error){
				console.log("Error getting items: " + error);
				console.log(data);
			},
			success: function(data){
				//console.log(data);
				var LineItemHTML = "";
				var DivHTML = "";
				for(var key in data){
					if(data.hasOwnProperty(key)){
						var categoryItem = data[key];
						LineItemHTML += "<li><a href='#tabs-" + categoryItem["ID"] + "'>" + categoryItem["category"] + "</a></li>";
						DivHTML += "<div id='tabs-" + categoryItem["ID"] + "' class='itemcontainer'>";
						for(var subkey in categoryItem["Items"]){
							if(categoryItem["Items"].hasOwnProperty(subkey)){
								var individualItem = categoryItem["Items"][subkey];
								DivHTML += "<div class='item'>";
								if(individualItem["Thumbnail"] !== ""){
									DivHTML += "<span class='mimecontainer'><img src='" + individualItem["virtualbase"] + "resources/thumbnails/" + individualItem["Thumbnail"] + "' alt='" + individualItem["Name"] + "'></span>";
								}else{
									DivHTML += "<span class='mimecontainer'>" + getMIMETypeImage(individualItem["ID"]) + "</span>";
								}
								//DivHTML += "<span data-itemid='" + individualItem["ID"] + "'>" + individualItem["Name"] + "</span>";
								DivHTML += "<span>" + individualItem["Name"] + "</span>";
								DivHTML += "<span class='changeitem'>";
								DivHTML += "<a href='#' class='individualitemedit' data-action='edit' data-tabid='" + categoryItem["ID"] + "' data-itemid='" + individualItem["ID"] + "'><i class='fa fa-pencil' aria-hidden='true'></i></a>";
								DivHTML += "<a href='#' class='individualitemedit' data-action='delete' data-tabid='" + categoryItem["ID"] + "' data-itemid='" + individualItem["ID"] + "'><i class='fa fa-trash' aria-hidden='true'></i></a>";
								DivHTML += "</span>";
								DivHTML += "</div>";
							};
						};
						DivHTML += "</div>";
					}
				}
				$(itemadmintab).html("<ul>" + LineItemHTML + "</ul>" + DivHTML);
				try{
					$("#storeresourcesadministration .tabs").tabs("refresh");
				}
				catch(e){
					$("#storeresourcesadministration .tabs").tabs();
				}
			}
		});
}
function editExistingItem(argIntID){
	$.ajax({
		url: AJAXurl,
		type: "POST",
		data: "action=getItemInfo&id=" + argIntID,
		error: function(data, error){
			console.log("Error getting information for " + argIntID + ": " + error);
			console.log(data);
		},
		success: function(data){
			$("form[name='alterindividualitem'] fieldset legend").text("Edit " + data[0]["vchItemName"]);
			$("form[name='alterindividualitem'] fieldset input[name='itemname']").attr("value", data[0]["vchItemName"]);
			$("form[name='alterindividualitem'] fieldset select[name='changecategory']").html(getAvailableCategoriesForUpload);
			$("form[name='alterindividualitem'] fieldset input[name='itemid']").attr("value", argIntID);
			$("form[name='alterindividualitem'] button[name='updateitem']").attr("data-intid", argIntID);
		}
	});
}
function deleteExistingItem(argIntID){
	$.ajax({
		url: AJAXurl,
		type: "POST",
		data: "action=deleteExistingItem&id=" + argIntID,
		error: function(data, error){
			console.log("Error deleting " + argIntID + ": " + error);
			console.log(data);
		},
		success: function(data){
			$(".deleteindividualitem").dialog("close");
			StoreRSRCItemAdmin();
		}
	});
}
function updateExistingItem(argIntID, argFormName){
	$.ajax({
		url: AJAXurl,
		type: "POST",
		data: "action=updateInventoryItem&" + $("form[name='" + argFormName + "']").serialize(),
		error: function(data, error){
			console.log("Error getting information for " + argIntID + ": " + error);
			console.log(data);
		},
		success: function(data){
			$("form[name='" + argFormName + "']").dialog("close");
			StoreRSRCItemAdmin();
			loadItemsNeedingAttention();
		}
	});
}
function loadItemsNeedingAttention(){
	$.ajax({
		url: AJAXurl,
		type: "POST",
		data: "action=getItemsNeedingAttention",
		error: function(data, error){
			console.log("error loading trouble items");
			console.log(data);
		},
		success: function(data){
			//console.log(data);
			var listhtml = "";
			var divhtml = "";
			$("#storeitemsneedingattention .tabs").html("");
			for(var key in data){
				if(data.hasOwnProperty(key)){
					var categoryItem = data[key];
					listhtml += "<li><a href='#" + categoryItem["Shorthand"] + "'>" + categoryItem["Title"] + "</a></li>";
					divhtml += "<div id='" + categoryItem["Shorthand"] + "' class='itemcontainer'>";
					for(var subkey in categoryItem["Items"]){
						if(categoryItem["Items"].hasOwnProperty(subkey)){
							var individualItem = categoryItem["Items"][subkey];
							divhtml += "<div class='item'>";
							divhtml += "<span class='name'>" + individualItem["Name"] + "</span>";
							divhtml += "<span class='edititem'>";
							divhtml += "<a href='#' data-action='edit' data-itemid='" + individualItem["ID"] + "'><i class='fa fa-pencil' aria-hidden='true'></i></a>";
							//divhtml += "<a href='#' data-action='delete' data-itemid='" + individualItem["ID"] + "'><i class='fa fa-trash' aria-hidden='true'></i></a>";
							divhtml += "</span>";
							divhtml += "</div>";
						}
					}
					divhtml += "</div>";
				}
			}
			$("#storeitemsneedingattention .tabs").html("<ul>" + listhtml + "</ul>" + divhtml);
			try {
				$("#storeitemsneedingattention .tabs").tabs("refresh");
			}
			catch(e) {
				$("#storeitemsneedingattention .tabs").tabs({ selected: 1 });
			}
		}
	});
};




function uploadNewFileToRSRC(){
	var objFormToSubmit = $("form[name='uploadnewfile']");
	var filesToUpload = $("form[name='uploadnewfile'] input[name='newfile']")[0].files[0];
	var thumbnailsToUpload = $("form[name='uploadnewfile'] input[name='newthumbnail']")[0].files[0];
	var objProgressBar = $("form[name='uploadnewfile'] progress");
	var jsFormData = new FormData();
	jsFormData.append("filename", filesToUpload, filesToUpload.name);
	jsFormData.append("thumbnail", thumbnailsToUpload, thumbnailsToUpload.name);
	jsFormData.append("mimetype", filesToUpload.type);
	jsFormData.append("colloquialname", $("input[name='newfilename']").val());
	jsFormData.append("intCategory", $("select[name='category']").val());
	
	var fd = new FormData();
	for (var i = 0; i < 3000; i += 1) {
		fd.append('data[]', Math.floor(Math.random() * 999999));
	}
	
	xhr = new XMLHttpRequest();
	xhr.open("POST", AJAXurl + "?action=uploadNewFileToRSRC&", true);
	xhr.setRequestHeader('Content-Type', filesToUpload.type);
	//xhr.open("POST", "https://zinoui.com/demo/progress-bar/upload.php", true);
	
	if(xhr.upload){
		xhr.upload.onprogress = function(e){
			if(e.lengthComputable){
				$(objProgressBar).attr({
					"value": e.loaded,
					"max": e.total
				});
			}
		}
		xhr.upload.onloadstart = function(e){
			$(objFormToSubmit).find("fieldset").toggle();
			$(objProgressBar).show();
			$(objProgressBar).val = 0;
			$(objProgressBar).progressbar();
		}
		xhr.upload.onloadend = function(e){
			$(objProgressBar).attr("value", e.loaded);
			objFormToSubmit[0].reset();
			$(objProgressBar).removeAttr("max");
			$(objProgressBar).val = 0;
			$(objProgressBar).hide();
			//getAvailableCategoriesForUpload();
			StoreRSRCItemAdmin();
		}
	}
	//xhr.send(fd);
	xhr.send(jsFormData);
}