//var url = "../config/ajax_functions_mcb.asp";
var intItemID;

$("form#updateprofile button[type='submit']").click(function(event){
	var vchProfileName = encodeURIComponent($("form#updateprofile input#vchProfileName").val());
	var intProfileID = $("form#updateprofile input[name='intProfileID']").val();
	$.ajax({
		type: "POST",
		url: url,
		async:false,
		data: "action=checkForDuplicate&vchProfileName=" + vchProfileName + "&intProfileID=" + intProfileID,
		success: function(data)
		{
			if(data["DuplicateItems"] == "true"){
				//console.log("Duplicates exist");
				$("p.profilecreateerror").toggle();
				event.preventDefault();
			}
		},
		error: function(data)
		{
			console.log(data);
			event.preventDefault();
		}
	});
});