var url = "../config/ajax_functions_mcb.asp";
var intItemID;

function loadStoresInProfile(intProfileID)
{
	$.ajax({
		type: "POST",
		url: url,
		async:false,
		data: "action=viewProfileContents&profileID="+ intProfileID,
		success: function(data)
		{
			var message = "";
			//message += "<table width='100%'>";
			message += "<thead>";
			message += "<tr><th>Store Name</th><th>Store</th><th>District</th><th>Address</th><th>Type</th><th>Region</th><th>View Schematic</th></tr>";
			message += "</thead>";
			message += "<tbody>";
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					var actualData = data[key];
					message += "<tr>";
					message += "<td>" + actualData["vchLastName"] + "</td>"; //store
					message += "<td>" + actualData["intStoreID"] + "</td>"; //store
					message += "<td>" + actualData["intDistrict"] + "</td>"; //district
					message += "<td>" + actualData["vchAddress1"] + "<br>" + actualData["vchCity"]  + ", " + actualData["vchState"]  + " " + actualData["vchZip"] + "</td>"; //address (street address, city, state zip)
					//type
					switch(actualData["chrStoreType"]){
						case "N":
							message += "<td>Neighborhood</td>";
							break;
						case "T":
							message += "<td>Traditional</td>";
							break;
						default:
							message += "<td></td>";
							break;
					}
					message += "<td>" + actualData["vchDivision"]  + "</td>"; //region
					message += "<td>";
					message += "<a href='" + actualData["vchImageLocation"] + "' class='layoutzoom' data-width='640'>";
					message += "<img src='" + iconurl + "' alt='" + actualData["vchLastName"] + "' class='viewschematic'>";
					message += "</a>";
					message += "</td>";
					message += "</tr>"; 
				}
			}
			message += "</tbody>"
			$("#currentProfileTable").html(message);
			$("#StoreProfileDialogBox").dialog({
				modal: true,
				draggable: false,
				resizable: true,
				width: "45%",
				minHeight: 300,
				maxHeight: screen.height,
				dialogClass: "noTitleStuff"
			});
			var intTableScrollHeight = $("#StoreProfileDialogBox").height() - ($("#profilepopout").height()/3);
			$("#StoreProfileDialogBox .table-scroll").css("height", intTableScrollHeight.toString() + "px");
		},
		error: function(data)
		{
			//console.log(data);
		}
	});
}
function getItemName(intItemID){
	$.ajax({
		type: "POST",
		url: url,
		async: false,
		data: "action=getitemname&intItemID="+ intItemID,
		success: function(data)
		{
			//console.log("success");
			//console.log(data);
			//return data[0]["vchItemName"];
		},
		error: function(data){
			console.log(data);
		}
	});
}


function loadProfilePackage(intProfileID,intCampaign)
{
	$.ajax({
		type: "POST",
		url: url,
		async:false,
		data: "action=viewProfilePackage&profileID="+ intProfileID + "&campaignID=" + intCampaign,
		success: function(data)
		{
			var accordionHeader = "";
			var itemCount = 0;
			var accordionContents = "";
			var currentFixture = "";
			var finalOutput = "";
			var allIDs = [];
			//var strItemColumnName = getItemName(results["intItemID"]);
			
			//intItemID = results["intItemID"];
			
			finalOutput = "";
			for(var key in data) {
				var results = data[key];
				if (results["vchFixtureName"] == "Hardware" && results["Items"].length > 0){
					finalOutput += "<div class='profileaccordion'>";
					finalOutput += "<h3>" + results["vchFixtureName"];
					finalOutput += "<i class='fa fa-check' aria-hidden='true'></i>";
					finalOutput += "</h3>";
					if(results["Items"].length > 0){
						finalOutput += "<ul>";
						for(i = 0; i < results["Items"].length; i++){
							finalOutput += "<li>";
							finalOutput += "<span class='profilepackageitem'>";
							finalOutput += results["Items"][i]["Item"] + " - " + results["Items"][i]["vchPartNumber"] + " x" + results["Items"][i]["intQty"];
							finalOutput += "</span>";
							finalOutput += "<span class='profilepackagedeleteitem'>";
							finalOutput += "<a href='#' data-attribute-profilepackageintid='" + results["Items"][i]["DeleteID"] + "' class='removeitemfromprofile'>";
							finalOutput += "<i class='fa fa-trash' aria-hidden='true'></i></a>";
							finalOutput += "</span>";
							finalOutput += "<div class='deleteprofileitemalert' title='Remove item ";
							//finalOutput += "<i class='fa fa-exclamation-triangle' aria-hidden='true'></i> Remove item ";
							finalOutput += results["Items"][i]["Item"] + "' data-attribute-profilepackageintid='" + results["Items"][i]["DeleteID"] + "'>";
							finalOutput += "<p><i class='fa fa-exclamation-triangle' aria-hidden='true'></i>  Item ";
							finalOutput += results["Items"][i]["Item"] + " will be deleted from this profile for this campaign. Are you sure you wish to proceed?</p>";
							finalOutput += "</div>";
							finalOutput += "</li>";
						}
						finalOutput += "<ul>";
					}
					finalOutput += "</div>"
				}
				else if(parseInt(results["intDenominator"]) !== 0){
					finalOutput += "<div class='profileaccordion'>";
					finalOutput += "<h3>" + results["vchFixtureName"] + " - " + results["intNumerator"] + " unique sign(s) created for up to " + results["intDenominator"] + " fixtures";
					if(parseInt(results["intDenominator"]) == parseInt(results["intNumerator"])){
						/* style='float: right; display: block; color: #1d381d; font-size: 2em;' */
						finalOutput += "<i class='fa fa-check' aria-hidden='true'></i>";
					}
					finalOutput += "</h3>";
					if(results["Items"].length > 0){
						finalOutput += "<ul>";
						for(i = 0; i < results["Items"].length; i++){
							finalOutput += "<li>";
							finalOutput += "<span class='profilepackageitem'>";
							finalOutput += results["Items"][i]["Item"];
							finalOutput += "</span>";
							finalOutput += "<span class='profilepackagedeleteitem'>";
							finalOutput += "<a href='#' data-attribute-profilepackageintid='" + results["Items"][i]["DeleteID"] + "' class='removeitemfromprofile'>";
							finalOutput += "<i class='fa fa-trash' aria-hidden='true'></i></a>";
							finalOutput += "</span>";
							finalOutput += "<div class='deleteprofileitemalert' title='Remove item ";
							//finalOutput += "<i class='fa fa-exclamation-triangle' aria-hidden='true'></i> Remove item ";
							finalOutput += results["Items"][i]["Item"] + "' data-attribute-profilepackageintid='" + results["Items"][i]["DeleteID"] + "'>";
							finalOutput += "<p><i class='fa fa-exclamation-triangle' aria-hidden='true'></i>  Item ";
							finalOutput += results["Items"][i]["Item"] + " will be deleted from this profile for this campaign. Are you sure you wish to proceed?</p>";
							finalOutput += "</div>";
							finalOutput += "</li>";
						}
						finalOutput += "<ul>";
					}
					finalOutput += "</div>"
				}
			}
			$("#profile-" + intProfileID + "-container").html(finalOutput);
			for (i = 0; i < allIDs.length; i++) {
				$("#"+allIDs[i]).accordion({
					collapsible: true,
					active: false,
					heightStyle: "content",
					auotHeight: false
				});
			}
			$("div.profileaccordion").each(function(){
				$(this).accordion({
					collapsible: true,
					active: false,
					heightStyle: "content",
					auotHeight: false
				});
			});
			$("a.removeitemfromprofile").click(function(e){
				e.preventDefault();
				var intItemID = $(this).attr("data-attribute-profilepackageintid");
				$(".deleteprofileitemalert[data-attribute-profilepackageintid='" + intItemID + "'").dialog({
					resizable: false,
					height: "auto",
					width: 400,
					draggable: false,
					modal: true,
					buttons: [
						{
							//confirmation
							text: "Delete this item",
							"class": "deleteitemconfirmation",
							click: function() {
								$.ajax({
									type: "POST",
									url: url,
									async:false,
									data: "action=deleteItemFromProfilePackages&PPIntID="+ intItemID,
									success: function(){
										$(".deleteprofileitemalert[data-attribute-profilepackageintid='" + intItemID + "'").dialog( "close" );
										loadProfilePackage(intProfileID,intCampaign);
									},
									error: function(data){
										console.log(data);
									}
								})
							}
						},
						{
							//cancellation
							text: "Cancel",
							"class": "deleteitemcancellation",
							click: function() {
								$( this ).dialog( "close" );
							}
						}
					]
				});
				//$(".ui-dialog-titlebar-close").append($("<i class='fa fa-times' aria-hidden='true'></i>"));
			});
		},
		error: function(data)
		{
			console.log(data)
		}
	});
}
function ColorProcessHandler(argIntFixtureID, argIntNumSides){
	var colorProcessOptions = "<option value='default'>Color Process</option>";
	$.ajax({
		type: "POST",
		url: url,
		async:true,
		data: "action=getColorProcesses&fixture="+ argIntFixtureID + "&sides=" + argIntNumSides,
		success: function(data){
			for(var key in data){
				var actualData = data[key];
				colorProcessOptions += "<option value='" + actualData["name"] + "'>" + actualData["name"] + "</option>";
				$("#vchColorProcess").html(colorProcessOptions);
			}
		},
		error: function(data, error){
			
		}
	});
	/*
	var xhttp = new XMLHttpRequest();
	xhttp.open(url + "action=getColorProcesses&fixture="+ argIntFixtureID + "&sides=" + argIntNumSides, false);
	xhttp.send();
	*/
}

function addNewHardware() {
	
	$( "#hardwarePopup" ).dialog({
			modal: true,
			draggable: false,
			resizable: false,
			dialogClass: "noTitleStuff"
		});
	
}

function saveNewHardware() {
	var optionList = "<option value=\"default\">Select Hardware</option>";
	$.ajax({
		type: "POST",
		url: url,
		async:true,
		data: "action=saveNewHardware&" + $("#hardwareForm").serialize(),
		success: function(data){
			for(var key in data){
				var actualData = data[key];
				optionList += "<option value='" + actualData["intID"] + "'>" + actualData["vchItemName"] + " - " + actualData["vchPartNumber"] + "</option>";
				$("#intInvID").html(optionList);
			}
			//$("#intInvID").val($("#hardwareID").val());
			var text1 = $("#hardwareName").val() + ' - ' + $("#hardwareID").val();
			
			$("#intInvID option").filter(function() {
				return this.text == text1; 
			}).attr('selected', true);
			
			$("#hardwareID").val("");
			$("#hardwareName").val("");
			$( "#hardwarePopup" ).dialog("close");
		},
		error: function(data, error){
			
		}
	});
	
}