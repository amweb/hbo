var allIds = [];
var url = "../config/ajax_functions.asp";
var url2 = "../config/ajax_functions_mcb.asp";

var incomplete = "rgba(192,192,192,.50)";
var complete = "rgba(92,184,92,.70)";

var vchLayoutImage;
var intLayoutID;

var topLeftCorner = [];
var topRightCorner = [];
var bottomLeftCorner = [];
var bottomRightCorner = [];
var highestX = 0;
var highestY = 0;
var lowestX = 10000000000;
var lowestY = 10000000000;
var totalWidth = 0;
var totalHeight = 0;
var blnIsAdmin;
var strDataURL;
var profileID;
var firstCheck = false;
var secondCheck = false;

var FixtureIDs = ["X-", "XA", "DC", "FD", "SC", "SCO", "ECH", "ECDW", "ECD1", "ECD2", "ECD3", "Banner", "BDW"]


$(document).ready(function(){
	if(window.location.href.indexOf("admin") > -1) {
		loadProfileDropdown();
		blnIsAdmin = true;
	}
	if($("#profileDropdown").is("select")){
		profileID = $("#profileDropdown").val();
	}else{
		profileID = $("#profileDropdown").text();
	}
	/*$(document).on("change", "#profileDropdown", function(){
		
		loadStoreList();
		$("#aqr").show();
	});*/
	$("#profileDropdown").change(function(event){
		showLoader("on");
		setTimeout(function () {
			loadStoreList();
			$("#aqr").show();
		}, 1000);
	});
	$(document).bind({
	   ajaxStart: function() { showLoader("on"); },
	   ajaxStop: function() {  showLoader("off"); }
	});
});

function showLoader(toggle)
{
	if (toggle == "on")
	{
		$( "#loader" ).dialog({
			modal: true,
			draggable: false,
			resizable: false,
			dialogClass: "noTitleStuff"
		});
	}
	else
	{
		if($("#loader").is(":visible")){
			$( "#loader" ).dialog("close");
		}
	}
}

function storeSetCampaign() {
	$("#campaignID").html($("#campaignSelection").val());
	
    if ($("#campaignSelection").val() == "") {
        $("#storeSelection").hide();
        $("#storeSelection").val("");
        $("#profileSelection").hide();
        $("#profileSelection").val("");
        $('#stage').empty();
    } else {
        $("#storeSelection").show();
        $("#storeSelection").val("");
        $("#profileSelection").hide();
        $("#profileSelection").val("");
    }
}

function showAdvancedOptions() {
    $("#advanced").hide();
    $("#advanced_options").show();
}

function showSelectProfiles() {
   $("#advanced_options").hide();
   $("#select_profiles").show();
}

function backOne() {
    $("#advanced").show();
    $("#advanced_options").hide();
}

function backTwo() {
    $("#advanced_options").show();
    $("#select_profiles").hide();
}

function loadMap(){
	
	
	(function(){
		$('#stage').html("");
		if(vchLayoutImage == ""){
			vchLayoutImage = $("#layoutsvgurl").text();
		}
		/*
		if($("#layoutsvgurl").text() !== ""){
			vchLayoutImage = $("#layoutsvgurl").text();
		}else if(vchLayoutImage !== ""){
			vchLayoutImage = vchLayoutImage;
		}*/
		var svg, g; $.get('../images/layouts/' + vchLayoutImage, function(svg){
			$('#stage').append($(svg));
			init();
		},'text');

		function init() {

			svg = d3.select('svg');
			g = d3.select('svg > #all');
			var svgViewBox = $("svg")[0].getAttribute("viewBox");
			svgViewBox = svgViewBox.split(" ")
			var svgWidth = svgViewBox[2];
			var svgHeight = svgViewBox[3]

			var zoom = d3.behavior.zoom()
			.translate([0, 0])
			.scale(1)
			.scaleExtent([1, 20])
			.on("zoom", zoomed);

			svg
			.call(zoom)
			.call(zoom.event);
			
			var promoArea = $("#Promo");
			allIds = [];
			$(promoArea).find('*').each(function (){
				
				if (this.nodeName == "g") {
					//getShapes(this);
				}
				else if (this.nodeName != "g" && this.nodeName != "text") {
					for (var i=0; i<FixtureIDs.length; i++)
					{
						if (this.id.indexOf(FixtureIDs[i]) > -1)
						{
							allIds.push(this.id);
							{ break; }
						}
					}
				}
			});
			
			var zoomAreas = $("#all > g");
			var zoomDDOptions = "<option value=''>Select One</option>";
			
			$(zoomAreas).each(function (){
				if (this.id != "Background_Layer")
				{
					zoomDDOptions += "<option value='" + this.id + "'>" + this.id + "</option>";
				}
			});
			
			$("#zoomDD").html(zoomDDOptions);
			$("#leftSide").show();
			
			for (i=0;i<allIds.length;i++) {
				var loadItemSlotOnClick = "loadItemSlot(";
				if(profileID !== ""){
					loadItemSlotOnClick += profileID + ", ";
				}else{
					loadItemSlotOnClick += "'', ";
				}
				loadItemSlotOnClick += intLayoutID + ", '" + allIds[i] + "');"
				$("#" + allIds[i]).attr( "onclick", loadItemSlotOnClick);
				//$("#" + allIds[i]).children().attr("fill", "rgba(192,192,192,50)");
				//$("#" + allIds[i]).children("text").removeAttr("fill");
				//console.log("ECH")
				if ($("#" + allIds[i]).attr( "style").indexOf("!important") == -1)
				{
					if (allIds[i].indexOf("ECH") > -1)
					{
						$("#" + allIds[i]).attr( "style", $("#" + allIds[i]).attr( "style") + ";cursor: pointer; fill:rgba(0,0,0,0) !important;" ); //
					}
					else
					{
						$("#" + allIds[i]).attr( "style", $("#" + allIds[i]).attr( "style") + ";stroke: none !important;" ); //fill:rgba(192,192,192,50) !important;
					}
				}
				//console.log(allIds[i] + " : " + $("#" + allIds[i]).attr( "style"))
			}
			
			$(".h3-" + profileID).click();

			$('#zoomDD').on('change', function(){
				//       var id = '#g-1011';
				var scale = 4;
				var bbox = $('#' + $("#zoomDD").val())[0].getBBox(); 
				//console.log($("#zoomDD").val())
				//console.log(bbox);
				getShapes($('#' + $("#zoomDD").val()));
				$.each( bottomRightCorner, function() {
						if (this.x > highestX) {
							highestX = this.x;
						}
						else if (this.x < lowestX)
						{
							lowestX = this.x;
						}
						
						if (this.y > highestY) {
							highestY = this.y;
						}
						else if (this.y < lowestY)
						{
							lowestY = this.y;
						}
					}
				)
				$.each( topLeftCorner, function() {
						if (this.x > highestX) {
							highestX = this.x;
						}
						else if (this.x < lowestX)
						{
							lowestX = this.x;
						}
						
						if (this.y > highestY) {
							highestY = this.y;
						}
						else if (this.y < lowestY)
						{
							lowestY = this.y;
						}
					}
				)
				$.each( topRightCorner, function() {
						if (this.x > highestX) {
							highestX = this.x;
						}
						else if (this.x < lowestX)
						{
							lowestX = this.x;
						}
						
						if (this.y > highestY) {
							highestY = this.y;
						}
						else if (this.y < lowestY)
						{
							lowestY = this.y;
						}
					}
				)
				$.each( bottomLeftCorner, function() {
						if (this.x > highestX) {
							highestX = this.x;
						}
						else if (this.x < lowestX)
						{
							lowestX = this.x;
						}
						
						if (this.y > highestY) {
							highestY = this.y;
						}
						else if (this.y < lowestY)
						{
							lowestY = this.y;
						}
					}
				)
				console.log(bottomRightCorner);
				console.log(topLeftCorner);
				console.log(topRightCorner);
				console.log(bottomLeftCorner);
				console.log(highestX + "," + highestY);
				console.log(lowestX + "," + lowestY);
				var dx = highestX - lowestX;
				var dy = highestY - lowestY;
				var x = (lowestX + highestX) /2;
				var y = (lowestY + highestY) /2;
				console.log(dx);
				console.log(dy);
				console.log(x);
				console.log(y);
				//width and height from svg viewport
				//width = 602 
				// height = 456
				width = svgWidth;
				height = svgHeight;
				scale = .9 / Math.max(dx / width, dy / height);
				
				//var translate = [-((((highestX+lowestX)/2)+lowestX)-264*(scale-1)),-((((highestY+lowestY)/2)+lowestY)*(scale-1))];
				var translate = [(width/2)-(scale)*x, (height/2)-(scale)*y];
				console.log(translate)
				g.transition().duration(0) .call(zoom.translate(translate).scale(scale).event);
				highestY = 0;
				highestX = 0;
				lowestX = 100000000000;
				lowestY = 100000000000;
				topLeftCorner = [];
				topRightCorner = [];
				bottomLeftCorner = [];
				bottomRightCorner = [];
			});



		}

		function zoomed() {
			g.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
		}
		
		function getShapes(parentID) {
			$(parentID).find('*').each(function (){
				
				if (this.nodeName == "g") {
					//getShapes(this);
				}
				else if (this.nodeName != "g" && this.nodeName != "text") {
					console.log(this.id);
					addBBtoArrays(this.getBBox());
					console.log(this.getBBox())
				}
			});
		}
		
		function addBBtoArrays(bbox) {
			topLeftCorner.push({"x": bbox.x, "y": bbox.y});
			topRightCorner.push({"x": bbox.x + bbox.width, "y": bbox.y});
			bottomLeftCorner.push({"x": bbox.x, "y": bbox.y + bbox.height});
			bottomRightCorner.push({"x": bbox.x + bbox.width, "y": bbox.y + bbox.height});
		}


	})();
	if($("#campaignID").text() !== ""){
		loadOverview($("#campaignID").text());
	}
}

function loadItemSlot(profileID, layoutID, slotName)
{
	if (slotName.indexOf("ECH-") > -1 && blnIsAdmin)
	{
		strDataURL = "action=loadSelectionSlot&layoutID=" + layoutID + "&slotName=" + slotName + "&campaignID=" + $("#campaignID").text() + "&profileID=" + profileID;
		if(blnIsAdmin){
			strDataURL += "&admin=true";
		}
		$.ajax({
			type: "POST",
			url: url2,
			async: true,
			data: strDataURL,
			success: function(data)
			{
				var splitSlotName = slotName.split("-");
				var header = "End Cap " + splitSlotName[1];
				
				var message = "";
				var allIds = [];
				var selectedID = 0;
				var fixtureImage = "";
				var itemSides = "";
				var itemSizes = "";
				var itemSubstrates = "";
				var itemColors = "";
				fixtureImage = data[0]["FixtureImage"];
				if (data[0]["vchOptionList2"])
				{
					if (data[0]["vchOptionList2"].indexOf(",") >-1 )
					{
						itemSides = data[0]["vchOptionList2"].split(",");
					}
					else
					{
						itemSides = data[0]["vchOptionList2"];
					}
				}
				
				if (data[0]["vchOptionList1"])
				{
					if (data[0]["vchOptionList1"].indexOf(",") >-1 )
					{
						itemSizes = data[0]["vchOptionList1"].split(",");
					}
					else
					{
						itemSizes = data[0]["vchOptionList1"];
					}
				}
				
				if (data[0]["vchOptionList3"])
				{
					if (data[0]["vchOptionList3"].indexOf(",") >-1 )
					{
						itemSubstrates = data[0]["vchOptionList3"].split(",");
					}
					else
					{
						itemSubstrates = data[0]["vchOptionList3"];
					}
				}
				
				if (data[0]["vchOptionList4"])
				{
					if (data[0]["vchOptionList4"].indexOf(",") >-1 )
					{
						itemColors = data[0]["vchOptionList4"].split(",");
					}
					else
					{
						itemColors = data[0]["vchOptionList4"];
					}
				}
				
				
				$("#slotName").html(header);
				$("#slotDropDown").html(message);
				$("#vchSlotName").val(slotName);
				$("#slotDropDown").val(selectedID);
				message += "<div id='sign_loc_img' style='height:300px; float:left; padding:30px; text-align:center;'>";
				message += "<div style='text-align:center;position: relative; top: -163px; left: 230px;'><img height='100px' width='100px' src='../images/Products/" + fixtureImage + "'><br>Location Example</div></div>";
				message += "<div class=\"form-group\"><label for=\"slotDropDown\" style=\"display: inline-block; float: left; clear: left; width: 150px; text-align: left;\">Fixture Location:</label> "
				message += "<input type=\"text\" name=\"slotNamePair\" id=\"slotNamePair\" value=\"" + slotName + " - End Cap Header\" style=\"outline:none; display: inline-block; float: left; \" class=\"form-control\" readonly></div>";
				
				message += "<div style='margin-left:auto;margin-right:auto;width:400px;'><form id=\"createItem\" name=\"createItem\" class=\"form-horizontal\"> <input type=\"hidden\" name=\"profileID\" value=\"" + profileID + "\"> <input type=\"hidden\" name=\"slotName\" value=\"" + slotName + "\"> <input type=\"hidden\" name=\"layoutID\" value=\"" + layoutID + "\"> ";
				
				message += "</select></div>"
					
				message += "<div class=\"form-group\"><label for=\"textfield\" style=\"display: inline-block; float: left; clear: left; width: 150px; text-align: left;\">Name:</label> "
				message += "<select id=\"vchItemName\" name=\"vchItemName\" class=\"form-control\" onchange=\"loadItemDetails()\" "
				if (blnIsAdmin == false)
				{
					message += "readonly ";
				}
				message += ">";
				message += "<option value=\"\"></option>";
				for (var key in data) {
					if (data.hasOwnProperty(key)) {
						var actualData = data[key];
						
						if (allIds.indexOf(actualData["intID"]) < 0)
						{
							message += "<option value=\"" + actualData["intID"] + "\" ";
							message += ">" + actualData["vchItemName"] + "</option>";
							allIds.push(actualData["intID"]);
						}
						
						if (actualData["vchSlotName"] == slotName)
						{
							selectedID = actualData["intID"]
						}
						
					}
				}
				
				message += "</select></div>"
				
				message += "<div class=\"form-group\"><label for=\"textfield\" style=\"display: inline-block; float: left; clear: left; width: 150px; text-align: left;\">Sides:</label><select name=\"vchNumSides\" id=\"vchNumSides\" class=\"form-control\" readonly>";
				message += "<option>Number of Sides</option>";
				
				if (Array.isArray(itemSides))
				{
					for (i=0; i < itemSides.length; i++)
					{
						message += "<option value='" + itemSides[i] + "'' ";
						message += ">" + itemSides[i] + "</option>";
					}
				}
				else
				{
					message += "<option value='" + itemSides + "'' ";
					message += ">" + itemSides + "</option>";
				}

				message += "</select></div>";
				message += "<div class=\"form-group\"><label for=\"textfield\" style=\"display: inline-block; float: left; clear: left; width: 150px; text-align: left;\">Size:</label><select name=\"vchSize\" id=\"vchSize\" class=\"form-control\" readonly>";
				message += "<option>Sizes</option>";
				
				if (Array.isArray(itemSizes))
				{
					for (i=0; i < itemSizes.length; i++) {
						message += "<option value=\"" + itemSizes[i].replace(/"/g, '&quot;') + "\" ";
						message += ">" + itemSizes[i] + "</option>";
					}
				}
				else
				{
					message += "<option value='" + itemSizes + "' ";
					message += ">" + itemSizes + "</option>";
				}
				
				message += "</select></div>";
				message += "<div class=\"form-group\"><label for=\"textfield\" style=\"display: inline-block; float: left; clear: left; width: 150px; text-align: left;\">Color Process:</label><select name=\"vchColorProcess\" id=\"vchColorProcess\" class=\"form-control\" readonly>";
				message += "<option>Color Process</option>";
					
				if (Array.isArray(itemColors))
				{
					for (i=0; i < itemColors.length; i++) {
						message += "<option value=\"" + itemColors[i].replace(/"/g, '&quot;') + "\" ";
						message += ">" + itemColors[i] + "</option>";
					}
				}
				else
				{
					message += "<option value='" + itemColors + "'' ";
					message += ">" + itemColors + "</option>";
				}
				
				message += "</select></div>";
				message += "<div class=\"form-group\"><label for=\"textfield\" style=\"display: inline-block; float: left; clear: left; width: 150px; text-align: left;\">Substrate:</label><select name=\"vchSubstrate\" id=\"vchSubstrate\" class=\"form-control\" readonly>";
				message += "<option>Substrate</option>";
				
				if (Array.isArray(itemSubstrates))
				{
					for (i=0; i < itemSubstrates.length; i++) {
						message += "<option value=\"" + itemSubstrates[i].replace(/"/g, '&quot;') + "\" ";
						message += ">" + itemSubstrates[i] + "</option>";
					}
				}
				else
				{
					message += "<option value='" + itemSubstrates + "'' ";
					message += ">" + itemSubstrates + "</option>";
				}
				
				message += "</select></div>";
				/*
				message += "<div id='advanced' class=\"form-group\"><button type='button'  onclick='showAdvancedOptions();'>Advanced</button><br></div>";
				message += "<div id='advanced_options' style='display:none;' class=\"form-group\">Advanced:<br><button id='similar' type='button'>Add this item to the \"" + slotName + "\" location for all profiles within this campaign.</button><br><br>";
				message += "<button type='button' id='select' onclick='showSelectProfiles();'>Add this item to the \"" + slotName + "\" location for select profiles within this campaign.</button><br><br><button type='button' onclick='backOne();'>Go Back</button></div>";
				message += "<div id='select_profiles' style='display:none;' class=\"form-group\">Add this item to the \"" + slotName + "\" location for select profiles within this campaign.<br><select id='select_profiles_dd' class='form-control multiselect' multiple='multiple'>";
				message += "<option value=''>1.0 Format</option>";
				message += "<option value=''>2.0 Format</option>";
				message += "<option value=''>3.0 Format</option>";
				message += "<option value=''>Central Division</option>";
				message += "<option value=''>Eastern Division</option>";
				message += "<option value=''>Western Division</option>";
				message += "</select><br><br><button type='button' onclick='backTwo();'>Go Back</button></div>";
				*/
				message += "</form></div>";
				
				$("#itemDialog").html(message);
				$("#vchItemName").val(selectedID);
				if (selectedID)
				{
					loadItemDetails();
				}
				
				if ($('#profileDropdown').length == 0) { //Enhanced Storeview Simulator
					$("#itemDialog").dialog({
						modal: true,
						draggable: false,
						resizable: false,
						width: "550",
						height: "400",
						title: 'Assign Signage',
						buttons: [
							{
								text: "Close",
								"class":"btn btn-default",
								click: function() {
									$( this ).dialog( "close" );
									return false;
								}
							}
						]
						
					});
					$('#vchItemName').attr('readonly', true);
					$('#vchNumSides, #vchSize').attr('disabled', true);
					$('#advanced').hide();
					
					if (actualData["sign_img"] == null) {
						actualData["sign_img"] = 'icon-no-artwork-512.png';
						artwork_status = 'Not Received';
					} 
					
					$('#sign_loc_img').prepend("<img style='margin-right: 25px;' height='225' width='225' src='../images/Products/" + 
						actualData["sign_img"] + "'>");
					$('#createItem').prepend("<div class='form-group'><label for=\"textfield\" style=\"display: inline-block; float: left; clear: left; width: 150px; text-align: left;\">Artwork Status: </label><input type='text' value='" + 
						artwork_status + "' style='outline:none; display: inline-block; float: left; ' class='form-control' readonly></div>");
					$('.ui-widget-header').css('background', '#13436D url("images/ui-bg_gloss-wave_55_5c9ccc_500x100.png") 50% 50% repeat-x');
					$('.ui-icon-closethick').hide();
					$('.ui-widget-header').css('font-size', '24px');
					
				} else { // Enhanced Merchandise Builder
					$("#itemDialog").dialog({
						modal: true,
						draggable: false,
						resizable: false,
						width: "550",
						height: "800",
						title: 'Assign Signage',
						buttons: [
							{
								text: "Save and Assign to This Store",
								"class":"btn btn-default",
								click: function(){
									saveSlotAndReloadColors("single", slotName, profileID)
									$( this ).dialog( "close" );
								}
							},
							{
								text: "Save and Assign to All Stores in This Profile",
								"class":"btn btn-default",
								click: function(){
									if ( confirm("This will save this end cap header to this slot for all stores in this profile. Click OK to confirm this.")) {
										saveSlotAndReloadColors("multi", slotName, profileID);
										$( this ).dialog( "close" );
									}
								}
							}
						]
					});
					$('#sign_loc_img div').css('position', '');
					$('#sign_loc_img, #sign_loc_img div').css('float', '');
					$('#sign_loc_img, #sign_loc_img div').css('width', '250px');
					$('#sign_loc_img, #sign_loc_img div').css('height', '250px');
					$('#sign_loc_img').css('margin', '15px auto');
					$('#sign_loc_img').css('padding', '');
					$('#sign_loc_img div img').css('width', '225px');
					$('#sign_loc_img div img').css('height', '225px');
					
				}
				
				$("#select_profiles_dd").multiselect({
					selectedText: "# of # selected",
					nonSelectedText: "Select Profile(s)"
				});
				$("#itemDialog button").css('width', '100%');
				$(".ui-widget-header").css('border', '1px solid');
				
				/*$("#manualSelection").dialog({
					modal: true,
					draggable: false,
					resizable: false,
					width: "550",
					height: "400",
					title: 'Assign Signage',
					buttons: [
						{
							text: "Save",
							"class":"btn btn-default",
							click: function() {
								saveItem($("#slotDropDown").val(), slotName);
								$("#" +slotName).attr("style", "");
								color = $("#" + slotName).css("stroke");
								$("#" + slotName).attr("style", "fill: " + color + " !important; cursor: pointer;");
								loadOverview($("#campaignID").text(), profileID);
								$( this ).dialog( "close" );
								return false;
							}
						}
					]
				});*/
			},
			error: function(data)
			{
				console.log(data);
			}
		});
	}
	else
	{
		strDataURL = "action=loadItemSlot&layoutID=" + layoutID + "&slotName=" + slotName + "&campaignID=" + $("#campaignID").text() + "&profileID=" + profileID;
		if(blnIsAdmin){
			strDataURL += "&admin=true";
		}
		$.ajax({
			type: "POST",
			url: url,
			async:true,
			data: strDataURL, // serializes the form's elements.
			success: function(data)
			{
				if(blnIsAdmin){
					var artwork_status = 'Received';
					var message = "";
					var itemName = "";
					var itemImage = "";
					var savedItemName = "";
					var itemSizes = "";
					var itemSides = "";
					var itemColors = "";
					var itemSubstrates = "";
					var oldSides = "";
					var oldSize = "";
					var actualData = data[0];
					itemName = actualData["FixtureName"];
					itemImage = actualData["ItemImage"];
					oldSides = actualData["NumSides"];
					oldSize = actualData["vchSize"];
					oldColor = actualData["vchColorProcess"];
					oldSubstrate = actualData["substrate"];
					savedItemName = actualData["ItemName"];
					if (actualData["vchOptionList2"])
					{
						if (actualData["vchOptionList2"].indexOf(",") >-1 )
						{
							itemSides = actualData["vchOptionList2"].split(",");
						}
						else
						{
							itemSides = actualData["vchOptionList2"];
						}
					}
					
					if (actualData["vchOptionList1"])
					{
						if (actualData["vchOptionList1"].indexOf(",") >-1 )
						{
							itemSizes = actualData["vchOptionList1"].split(",");
						}
						else
						{
							itemSizes = actualData["vchOptionList1"];
						}
					}
					
					if (actualData["vchOptionList3"])
					{
						if (actualData["vchOptionList3"].indexOf(",") >-1 )
						{
							itemSubstrates = actualData["vchOptionList3"].split(",");
						}
						else
						{
							itemSubstrates = actualData["vchOptionList3"];
						}
					}
					
					if (actualData["vchOptionList4"])
					{
						if (actualData["vchOptionList4"].indexOf(",") >-1 )
						{
							itemColors = actualData["vchOptionList4"].split(",");
						}
						else
						{
							itemColors = actualData["vchOptionList4"];
						}
					}
					
					if (savedItemName == null)
					{
						savedItemName = "";
					}
					if (oldSides == null)
					{
						oldSides = "";
					}
					if (oldSize == null) {
						oldSize = "";
					}
					if (oldSubstrate == null) {
						oldSubstrate = "";
					}
					message += "<div id='sign_loc_img' style='height:300px; float:left; padding:30px; text-align:center;'>";
					message += "<div style='text-align:center;position: relative; top: -163px; left: 230px;'><img height='100px' width='100px' src='../images/Products/" + actualData["FixtureImage"] + "'><br>Location Example</div></div>";
					message += "<div style='margin-left:auto;margin-right:auto;width:400px;'><form id=\"createItem\" name=\"createItem\" class=\"form-horizontal\"> <input type=\"hidden\" name=\"profileID\" value=\"" + profileID + "\"> <input type=\"hidden\" name=\"slotName\" value=\"" + slotName + "\"> <input type=\"hidden\" name=\"layoutID\" value=\"" + layoutID + "\"> <input type=\"hidden\" name=\"itemID\" value=\"" + actualData["intItemID"] + "\">";
					message += "<div class=\"form-group\"><label for=\"slotDropDown\" style=\"display: inline-block; float: left; clear: left; width: 150px; text-align: left;\">Fixture Location:</label> "
					message += "<input type=\"text\" name=\"slotNamePair\" id=\"slotNamePair\" value=\"" + slotName + " - " + itemName + "\" style=\"outline:none; display: inline-block; float: left; \" class=\"form-control\" readonly></div>";
					
					message += "<div class=\"form-group\"><label for=\"textfield\" style=\"display: inline-block; float: left; clear: left; width: 150px; text-align: left;\">Name:</label> <input type=\"text\" name=\"vchItemName\" id=\"vchItemName\" style=\"display: inline-block; float: left; \" value=\"" + savedItemName + "\" class=\"form-control\" readonly></div>"
					message += "<div class=\"form-group\"><label for=\"textfield\" style=\"display: inline-block; float: left; clear: left; width: 150px; text-align: left;\">Sides:</label><select name=\"vchNumSides\" id=\"vchNumSides\" class=\"form-control\" readonly>";
					message += "<option>Number of Sides</option>";
					if (Array.isArray(itemSides))
					{
						for (i=0; i < itemSides.length; i++)
						{
							message += "<option value='" + itemSides[i] + "'' ";
							if (itemSides[i] == oldSides)
							{
								message += " selected ";
							}
							message += ">" + itemSides[i] + "</option>";
						}
					}
					else
					{
						message += "<option value='" + itemSides + "'' ";
						message += " selected ";
						message += ">" + itemSides + "</option>";
					}
					message += "</select></div>";
					message += "<div class=\"form-group\"><label for=\"textfield\" style=\"display: inline-block; float: left; clear: left; width: 150px; text-align: left;\">Size:</label><select name=\"vchSize\" id=\"vchSize\" class=\"form-control\" readonly>";
					message += "<option>Sizes</option>";
					if (Array.isArray(itemSizes))
					{
						for (i=0; i < itemSizes.length; i++) {
							message += "<option value=\"" + itemSizes[i].replace(/"/g, '&quot;') + "\" " 
							if (itemSizes[i] == oldSize) {
								message += " selected ";
							}
							message += ">" + itemSizes[i] + "</option>";
						}
					}
					else
					{
						message += "<option value='" + itemSizes + "'' ";
						message += " selected ";
						message += ">" + itemSizes + "</option>";
					}
					message += "</select></div>";
					message += "<div class=\"form-group\"><label for=\"textfield\" style=\"display: inline-block; float: left; clear: left; width: 150px; text-align: left;\">Color Process:</label><select name=\"vchColorProcess\" id=\"vchColorProcess\" class=\"form-control\" readonly>";
					message += "<option>Color Process</option>";
					if (Array.isArray(itemColors))
					{
						for (i=0; i < itemColors.length; i++) {
							message += "<option value=\"" + itemColors[i].replace(/"/g, '&quot;') + "\" " 
							if (itemColors[i] == oldColor) {
								message += " selected ";
							}
							message += ">" + itemColors[i] + "</option>";
						}
					}
					else
					{
						message += "<option value='" + itemColors + "'' ";
						message += " selected ";
						message += ">" + itemColors + "</option>";
					}
					message += "</select></div>";
					message += "<div class=\"form-group\"><label for=\"textfield\" style=\"display: inline-block; float: left; clear: left; width: 150px; text-align: left;\">Substrate:</label><select name=\"vchSubstrate\" id=\"vchSubstrate\" class=\"form-control\" readonly>";
					message += "<option>Substrate</option>";
					if (Array.isArray(itemSubstrates))
					{
						for (i=0; i < itemSubstrates.length; i++) {
							message += "<option value=\"" + itemSubstrates[i].replace(/"/g, '&quot;') + "\" " 
							if (itemSubstrates[i] == oldSize) {
								message += " selected ";
							}
							message += ">" + itemSubstrates[i] + "</option>";
						}
					}
					else
					{
						message += "<option value='" + itemSubstrates + "'' ";
						message += " selected ";
						message += ">" + itemSubstrates + "</option>";
					}
					message += "</select></div>";
					/*
					message += "<div id='advanced' class=\"form-group\"><button type='button'  onclick='showAdvancedOptions();'>Advanced</button><br></div>";
					message += "<div id='advanced_options' style='display:none;' class=\"form-group\">Advanced:<br><button id='similar' type='button'>Add this item to the \"" + slotName + "\" location for all profiles within this campaign.</button><br><br>";
					message += "<button type='button' id='select' onclick='showSelectProfiles();'>Add this item to the \"" + slotName + "\" location for select profiles within this campaign.</button><br><br><button type='button' onclick='backOne();'>Go Back</button></div>";
					message += "<div id='select_profiles' style='display:none;' class=\"form-group\">Add this item to the \"" + slotName + "\" location for select profiles within this campaign.<br><select id='select_profiles_dd' class='form-control multiselect' multiple='multiple'>";
					message += "<option value=''>1.0 Format</option>";
					message += "<option value=''>2.0 Format</option>";
					message += "<option value=''>3.0 Format</option>";
					message += "<option value=''>Central Division</option>";
					message += "<option value=''>Eastern Division</option>";
					message += "<option value=''>Western Division</option>";
					message += "</select><br><br><button type='button' onclick='backTwo();'>Go Back</button></div>";
					*/
					message += "</form></div>";
					
					$("#itemDialog").html(message);
					
					if ($('#profileDropdown').length == 0) { //Enhanced Storeview Simulator
						$("#itemDialog").dialog({
							modal: true,
							draggable: false,
							resizable: false,
							width: "550",
							height: "820",
							title: 'View Signage Details',
							buttons: 
							[
								{
									text: "Request profile change",
									"class":"btn btn-default",
									click: function() 
									{
										saveItem('');
										loadOverview($("#campaignID").text(), profileID);
										$( this ).dialog( "close" );
										return false;
										
									}
								}
								,
								{
									text: "Reorder",
									"class":"btn btn-default",
									click: function() 
									{
										saveItem('');
										loadOverview($("#campaignID").text(), profileID);
										$( this ).dialog( "close" );
										return false;
										
									}
								}
								
							]
							
						});
						$('#vchItemName').attr('readonly', true);
						$('#vchNumSides, #vchSize').attr('disabled', true);
						$('#advanced').hide();
						
						if (actualData["sign_img"] == null) {
							actualData["sign_img"] = 'icon-no-artwork-512.png';
							artwork_status = 'Not Received';
						} 
						
						$('#sign_loc_img').prepend("<img style='margin-right: 25px;' height='225' width='225' src='../images/Products/" + 
							actualData["sign_img"] + "'>");
						$('#createItem').prepend("<div class='form-group'><label for=\"textfield\" style=\"display: inline-block; float: left; clear: left; width: 150px; text-align: left;\">Artwork Status: </label><input type='text' value='" + 
							artwork_status + "' style='outline:none; display: inline-block; float: left; ' class='form-control' readonly></div>");
						$('.ui-widget-header').css('background', '#13436D url("images/ui-bg_gloss-wave_55_5c9ccc_500x100.png") 50% 50% repeat-x');
						$('.ui-icon-closethick').hide();
						$('.ui-widget-header').css('font-size', '24px');
						
					} else { // Enhanced Merchandise Builder
						$("#itemDialog").dialog({
							modal: true,
							draggable: false,
							resizable: false,
							width: "550",
							height: "800",
							title: 'Assign Signage',
							buttons: [
								{
									text: "Close",
									"class":"btn btn-default",
									click: function() {
										//saveItem('');
										//loadOverview($("#campaignID").text(), profileID);
										$( this ).dialog( "close" );
										return false;
									}
								}
							]
						});
						$('#sign_loc_img div').css('position', '');
						$('#sign_loc_img, #sign_loc_img div').css('float', '');
						$('#sign_loc_img, #sign_loc_img div').css('width', '250px');
						$('#sign_loc_img, #sign_loc_img div').css('height', '250px');
						$('#sign_loc_img').css('margin', '15px auto');
						$('#sign_loc_img').css('padding', '');
						$('#sign_loc_img div img').css('width', '225px');
						$('#sign_loc_img div img').css('height', '225px');
						
					}
					
					$("#select_profiles_dd").multiselect({
						selectedText: "# of # selected",
						nonSelectedText: "Select Profile(s)"
					});
					$("#itemDialog button").css('width', '100%');
					$(".ui-widget-header").css('border', '1px solid');
					
					$("#vchSize").val(oldSize);
					$("#vchSubstrate").val(oldSubstrate);
					$("#vchNumSides").val(oldSides);
				}else{
					var actualData = data[0];
					var itemname = "";
					var message = "";
					message += "<div id='sign_loc_img'>";
					message += "<figure class='storeview artwork'>";
					message += "<figcaption>Fixture Sample:</figcaption>";
					message += "<a class='storeview lightbox' href='" + actualData["vbase"] + "images/Products/" + actualData["FixtureImage"] + "'>";
					message += "<img src='" + actualData["vbase"] + "images/Products/" + actualData["FixtureImage"] + "' alt='" + actualData["FixtureName"] + "'></a>";
					message += "</figure>";
					message += "<figure class='storeview fixture'>";
					message += "<figcaption>Artwork:</figcaption>";
					if(actualData["ItemImage"]){
						message += "<a class='storeview lightbox' href='" + actualData["vbase"] + "images/" + actualData["ItemImage"] + "'>";
						message += "<img src='" + actualData["vbase"] + "images/" + actualData["Thumbnail"] + "' alt='" + actualData["ItemName"] + "'></a>";
					}else{
						message += "<a class='storeview lightbox' href='" + actualData["vbase"] + "images/icon-no-image-512.png'>";
						message += "<img src='" + actualData["vbase"] + "images/icon-no-image-512.png' alt='Design Artwork Not Yet Available'></a>";
					}
					message += "</figure>";
					message += "<div class='signage info'>";
					message += "<p><span class='bold'>Fixture Location</span>: " + actualData["vchslotname"] + "</p>";
					message += "<p><span class='bold'>Name</span>: " + actualData["ItemName"] + "</p>";
					message += "<p><span class='bold'>Size</span>: " + actualData["vchSize"] + "</p>";
					message += "<p><span class='bold'>Sides</span>: " + actualData["NumSides"] + "</p>";
					message += "<p><span class='bold'>Color profile</span>: " + actualData["vchColorProcess"] + "</p>";
					message += "</div>";
					message += "</div>";
					
					
					$("#itemDialog").html(message);
					$("#itemDialog").dialog({
						modal: true,
						draggable: false,
						resizable: false,
						minWidth: "550",
						maxHeight: "800",
						title: 'View Signage',
						buttons: [
							{
								text: "Close",
								"class":"btn btn-default",
								click: function() {
									$( this ).dialog( "close" );
									return false;
								}
							}
						]
					});
					$(".storeview.lightbox").fancybox({
						type: 'image',
						arrows: false,
						autoSize: true,
						maxWidth: 500,
						maxHeight: 425,
						fitToView: false,
						beforeShow: function() {
							var alt = this.element.find('img').attr('alt');
							var url = this.element.attr("href");
							this.inner.find('img').attr('alt', alt);
							this.title = alt;
						}
					});
				}
			}
		});
	}
}

function saveSlotAndReloadColors(saveType,slotName, profileID) {
	if (saveType == "single")
	{
		saveItem($("#vchItemName").val(), slotName);
	}
	else
	{
		saveItemMulti($("#vchItemName").val(), slotName, profileID);
	}
	
	
	$("#" + slotName).attr("style", $("#" + slotName).attr( "style").replace("stroke: none !important;","").replace("fill:rgba(0,0,0,0) !important;",""));
	if ($("#vchItemName").val() == "")
	{
		color = "rgba(0,0,0,0)";
	}
	else
	{
		color = $("#" + slotName).css("stroke");
	}
	
	$("#" + slotName).attr("style",$("#" + slotName).attr( "style") + ";fill: " + color + " !important; cursor: pointer;");
	strDataURL = "action=loadStoreList&intProfileID=" + profileID + "&intCampaignID=" + $("#campaignID").text();
	if(blnIsAdmin){
		strDataURL += "&admin=true";
	}
	message = "";
	$.ajax({
		type: "POST",
		url: url2,
		async:true,
		data: strDataURL,
		success: function(data)
		{
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					var actualData = data[key];
					
					message += "<div class=\"storeContainer\" id=\"container-" + actualData["intID"] + "\"><div class=\"circleMain\"><div class=\"circleColor-"
					if (parseInt(actualData["percentage"]) < 20)
					{
						message += "orangeRed"
					}
					else if (parseInt(actualData["percentage"]) >= 20 && parseInt(actualData["percentage"]) < 40)
					{
						message += "orange"
					}
					else if (parseInt(actualData["percentage"]) >= 40 && parseInt(actualData["percentage"]) < 60)
					{
						message += "yellow"
					}
					else if (parseInt(actualData["percentage"]) >= 60 && parseInt(actualData["percentage"]) < 80)
					{
						message += "lightGreen"
					}
					else if (parseInt(actualData["percentage"]) >= 80 )
					{
						message += "green"
					}
					
					message += "\" style=\"width: " + actualData["percentage"] + "; height: " + actualData["percentage"] + "\"></div></div><p>" + actualData["percentage"] + "</p><div class=\"text\"><strong>" + actualData["storeName"] + "<br />" + actualData["DisplayName"] + "</strong>";
					message += " <a href=\"#\" onclick=\"loadQuickView(" + profileID + "," + $("#campaignID").text() + "," + actualData["intID"] + "); return false;\">Quick View</a></div></div>"
				}
			};
			$("#storeList").html(message);
			$("#storeList").show();
		},
		error: function(data)
		{
			console.log(data);
		}
	});
	loadOverview($("#campaignID").text(), profileID);
	return false;
}

function loadItemDetails()
{
	$.ajax({
		type: "POST",
		url: url2,
		async: false,
		data: "action=loadItemDetails&intItemID=" + $("#vchItemName").val(),
		success: function(data)
		{
			$("#vchNumSides").val(data[0]["vchNumSides"]);
			$("#vchSize").val(data[0]["vchSize"]);
			$("#vchColorProcess").val(data[0]["vchColorProcess"]);
			$("#vchSubstrate").val(data[0]["vchSubstrate"]);
		}
	});
	
	
}

function saveItem(intItemID, vchSlotName) {
	$.ajax({
		type: "POST",
		url: url2,
		async: false,
		data: "action=saveItemSlot&campaignID=" + $("#campaignID").text() + "&intItemID=" + intItemID + "&vchSlotName=" + vchSlotName + "&intProfileID=" + profileID + "&intLayoutID=" + intLayoutID
	});
}

function saveItemMulti(intItemID, vchSlotName, profileID) {
	$.ajax({
		type: "POST",
		url: url2,
		async: false,
		data: "action=saveItemSlotMulti&campaignID=" + $("#campaignID").text() + "&intItemID=" + intItemID + "&vchSlotName=" + vchSlotName + "&intProfileID=" + profileID + "&intLayoutID=" + intLayoutID
	});
}

function loadProfileDropdown()
{
	var dialOptions = "";
	dialOptions = "<option selected=\"selected\" value=\"\">-- Please Select a Profile --</option>";
	var count = 0;
	var campaignid = $("#profileDropdown").attr("data-campaign");
	
	$.ajax({
		type: "POST",
		url: url,
		async:true,
		data: "action=loadProfiles&campaign=" + campaignid,
		success: function(data)
		{
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					var actualData = data[key];
					
					dialOptions += "<option value=\"" + actualData["intID"] + "\"" 
					dialOptions += ">Profile " +actualData["intID"] + " - " + actualData["vchProfileName"] + "</option>";					
				}
				count++;
			};
			$("#profileDropdown").html(dialOptions);
		},
		error: function(data)
		{
			console.log(data);
		}
	});
}

function loadStoreList()
{
	//	showLoader("on");
	if($("#profileDropdown").is("select")){
		profileID = $("#profileDropdown").val();
	}else{
		profileID = $("#profileDropdown").text();
	}
	
	buildcampaign(profileID)
	console.log("firstcheck " + firstCheck)
	/*while (firstCheck)
	{
		console.log("firstcheck " + firstCheck)
		setTimeout(function () {
			console.log("waiting 1")
		}, 1000);
	}*/
	console.log("after while firstcheck " + firstCheck)
	firstCheck = false;
	$(function () {
        function checkPendingRequest() {
            if ($.active > 0) {
                window.setTimeout(checkPendingRequest, 1000);
                //Mostrar peticiones pendientes ejemplo: $("#control").val("Peticiones pendientes" + $.active);
            }
            else {
				showLoader("on");
                strDataURL = "action=loadStoreList&intProfileID=" + profileID + "&intCampaignID=" + $("#campaignID").text();
				$("#loader p").html("Percentage Calculation<br />This may take a couple minutes");
				if(blnIsAdmin){
					strDataURL += "&admin=true";
				}
				$.ajax({
					type: "POST",
					url: url2,
					async:true,
					data: strDataURL,
					success: function(data)
					{
						for (var key in data) {
							if (data.hasOwnProperty(key)) {
								var actualData = data[key];
								
								message += "<div class=\"storeContainer\" id=\"container-" + actualData["intID"] + "\"><div class=\"circleMain\"><div class=\"circleColor-"
								if (parseInt(actualData["percentage"]) < 20)
								{
									message += "orangeRed"
								}
								else if (parseInt(actualData["percentage"]) >= 20 && parseInt(actualData["percentage"]) < 40)
								{
									message += "orange"
								}
								else if (parseInt(actualData["percentage"]) >= 40 && parseInt(actualData["percentage"]) < 60)
								{
									message += "yellow"
								}
								else if (parseInt(actualData["percentage"]) >= 60 && parseInt(actualData["percentage"]) < 80)
								{
									message += "lightGreen"
								}
								else if (parseInt(actualData["percentage"]) >= 80 )
								{
									message += "green"
								}
								
								message += "\" style=\"width: " + actualData["percentage"] + "; height: " + actualData["percentage"] + "\"></div></div><p>" + actualData["percentage"] + "</p><div class=\"text\"><strong>" + actualData["storeName"] + "<br />" + actualData["DisplayName"] + "</strong>";
								message += " <a href=\"#\" onclick=\"loadQuickView(" + profileID + "," + $("#campaignID").text() + "," + actualData["intID"] + "); return false;\">Quick View</a></div></div>"
							}
							count++;
						};
						$("#storeList").html(message);
						$("#storeList").show();
						$("#loader p").html("Loading...");
						$("#loader .progressloader").removeAttr( "max" );
						$("#loader .progressloader").removeAttr( "value" );
					},
					error: function(data,error)
					{
						console.log(data);
						console.log(error);
					}
				});

            }
        };

        window.setTimeout(checkPendingRequest, 1000);
	});
		var message = "";
		var count = 0;
		
		//showLoader("on");
		//$("#loader p").html("Loading");
}

function buildcampaign(profileID) {
	var message = "";
	var count = 0;
	$("#storeList").html(message);
	strDataURL = "action=getStoreListingZero&intProfileID=" + profileID
	console.log(strDataURL);
	$.ajax({
		type: "POST",
		url: url2,
		async:true,
		data: strDataURL,
		success: function(data)
		{
			console.log("store list return")
			$("#loader .progressloader").attr("max", data.length);
			$("#loader .progressloader").attr("value", 0);
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					var actualData = data[key];
					
					message += "<div class=\"storeContainer\" id=\"container-" + actualData["intID"] + "\"><div class=\"circleMain\"><div class=\"circleColor-"
					
					message += "\" style=\"width: 0; height: 0\"></div></div><p>0%</p><div class=\"text\"><strong>" + actualData["storeName"] + "<br />" + actualData["DisplayName"] + "</strong>";
					message += " <a href=\"#\" onclick=\"loadQuickView(" + profileID + "," + $("#campaignID").text() + "," + actualData["intID"] + "); return false;\">Quick View</a></div></div>"
				}
				count++;
				$("#storeList").append(message);
				$("#storeList").show();
				message = "";
				strDataURL = "action=runAutoAssign&intProfileID=" + profileID + "&intCampaignID=" + $("#campaignID").text() + "&intLayoutID=" + actualData["intLayout"];
				
				
				if(blnIsAdmin){
					strDataURL += "&admin=true";
				}
				console.log("start auto for " + data.length)
				$("#loader p").html("Building Campaign<br />Assigning Signage - 0/" + data.length + " stores");
				console.log("Building Campaign<br />Assigning Signage " + count + " of " + data.length)
				autoAssign(strDataURL, count, data.length)
				console.log(" before while secondCheck " + secondCheck)
				//$("#loader .progressloader").attr("value", count);
				console.log("after while secondCheck " + secondCheck)
				secondCheck = false;
				firstCheck = false;
				if (key = data.length)
				{
					firstCheck = true;
				}
			};
			
		},
		error: function(data,error)
		{
			console.log(data);
			console.log(error);
		}
	});
	console.log("build campaign end")
}

function autoAssign(strDataURL, count, max)
{
	//secondCheck = false;
	console.log("in auto assign")
	$.ajax({
		type: "GET",
		url: url2,
		async:true,
		data: strDataURL,
		success: function(data2)
		{
				console.log("Progress bar: " + count)
				$("#loader p").html("Building Campaign<br />Assigning Signage - " + count + "/" + max + " stores");
				$("#loader .progressloader").attr("value", count);
				secondCheck = true;
		},
		error: function(data2,error) {
			console.log(data2);
			console.log(error);
		}
	});
	console.log("auto assign end")
}

function loadQuickView(intProfileID, intCampaignID, intStoreID)
{
	
	$.ajax({
		type: "POST",
		url: url,
		async:false,
		data: "action=loadLayout&intStoreID=" + intStoreID, // serializes the form's elements.
		success: function(data)
		{
			var actualData = data[0];
			vchLayoutImage = actualData["vchImageLocation"];
			intLayoutID = actualData["intID"];
			$("#quickViewStore").html(actualData["vchFirstName"] + " " + actualData["vchLastName"]);
			$("#quickViewImage").attr("src","../images/layouts/" + vchLayoutImage);
		},
		error: function(data)
		{
			console.log(data);
		}
	});
	var vchOverViewText = "";
	strDataURL = "action=loadQuickViewOverview&intProfileID=" + intProfileID + "&intCampaignID=" + intCampaignID + "&intLayoutID=" + intLayoutID;
	if(blnIsAdmin){
		strDataURL += "&admin=true";
	}
	$.ajax({
		type: "POST",
		url: url,
		async: true,
		data: strDataURL, 
		success: function(data) {
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					var actualData = data[key];
					if (parseInt(actualData["intDenominator"]) > 0)
					{
						vchOverViewText += actualData["vchFixtureName"]	+ " <strong> - " + actualData["intNumerator"] + " Total Signs Assigned, " + actualData["intDenominator"] + " Unique Sign(s)</strong><br />";
					}
				}
			};
            
			$("#quickViewOverView").html(vchOverViewText);
            
			$("#quickView").dialog({
						modal: true,
						draggable: false,
						resizable: false,
						width: "550",
						height: "800",
						title: 'Quick View',
						dialogClass: "noTitleStuff quickViewPopUp"
					});
			/*$.blockUI(
			{
				message: $('#quickView'),
			});*/
	
		}
	});
}


function loadOverview(campaignID) {
	strDataURL = "action=loadOverview&campaignID=" + campaignID + "&intLayoutID=" + intLayoutID + "&profileID=" + profileID;
	var finalOutput = "";
	if(blnIsAdmin){
		strDataURL += "&admin=true";
	}
	$.ajax({
		type: "POST",
		url: url2,
		async:true,
		data: strDataURL, // serializes the form's elements.
		success: function(data)
		{
			var currentFixture = ""
			var accordionContents = "";
			var allIDs = [];
			var color = "";
			var idClass = "";
			var isEndCapHeader = false;
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					var actualData = data[key];
					if (actualData["vchFixtureName"] == "End Cap Header")
					{
						finalOutput += "<h2>Manually Assigned</h2>";
						isEndCapHeader = true;
					}
					else if (key == "0")
					{
						finalOutput += "<h2>Auto Assigned</h2>"
					}
					
					finalOutput += "<div id='accordion-profile-" + profileID + "-" + actualData["vchFixtureName"].replace(/ /g,'') + "'";
					if(isEndCapHeader){
						finalOutput += "data-expand='true'";
					}else{
						finalOutput += "data-expand='false'";
					}
					finalOutput += " class='svgaccordion'>";
					allIDs.push("accordion-profile-" + profileID + "-" + actualData["vchFixtureName"].replace(/ /g,''));
					
					finalOutput += "<h3 class=\"" + actualData["vchFixtureName"].replace(/\s/g, "") + "Style\">" + actualData["vchFixtureName"] + " - " + actualData["intNumerator"] + " Total Signs Assigned, " + actualData["intDenominator"] + " Unique Sign(s)"	
					
					if ( actualData["intNumerator"] == actualData["intDenominator"])
					{
						// if full do something
					}
					finalOutput += "</h3>";
					finalOutput += "<div class=\"" + actualData["vchFixtureName"].replace(/\s/g, "") + "Style\">";
					if (actualData["Items"].length > 0)
					{
						finalOutput += "<p>";
						for(i = 0; i < actualData["Items"].length; i++){
							finalOutput += "<span style=\"cursor: pointer;\" onclick=\"";
							var loadItemSlotOnClick = "loadItemSlot(";
							if(profileID !== ""){
								loadItemSlotOnClick += profileID + ", ";
							}else{
								loadItemSlotOnClick += "'', ";
							}
							loadItemSlotOnClick += intLayoutID + ", '" + actualData["Items"][i]["vchSlotName"] + "');"
							finalOutput += loadItemSlotOnClick + "\">";
							if (actualData["Items"][i]["vchItemName"].length > 0)
							{
								$("#" + actualData["Items"][i]["vchSlotName"]).attr("style", $("#" + actualData["Items"][i]["vchSlotName"]).attr( "style").replace("stroke: none !important;","").replace("fill:rgba(0,0,0,0) !important;",""));
								color = $("#" + actualData["Items"][i]["vchSlotName"]).css("stroke");
								$("#" + actualData["Items"][i]["vchSlotName"]).attr("style",$("#" + actualData["Items"][i]["vchSlotName"]).attr( "style") + ";fill: " + color + " !important; cursor: pointer;");
								//finalOutput += actualData["vchFixtureName"].replace(" ", "");
							}
							if (actualData["Items"][i]["vchSlotName"].length > 0)
							{
								var slotNumber = actualData["Items"][i]["vchSlotName"].split("-");
								finalOutput += " " + actualData["vchFixtureName"]  + " "
								//console.log(slotNumber)
								if ( slotNumber[1] )
								{
									finalOutput += slotNumber[1] + " ";
								}
								
							}
							if (actualData["Items"][i]["vchItemName"].length > 0)
							{
								finalOutput += "- " + actualData["Items"][i]["vchItemName"];
							}
							finalOutput += "</span>"
							finalOutput += "<br />";
							
						}
						finalOutput += "</p>";
					}
					finalOutput += "</div></div>";
					if (actualData["vchFixtureName"] == "End Cap Header" && data[parseInt(key)+1])
					{
						finalOutput += "<hr />"
						finalOutput += "<h2>Auto Assigned</h2>"
					}
				}
				isEndCapHeader = false;
			}
			//if(blnIsAdmin){
				$("#rightSide").html(finalOutput);
			//}
			$(".svgaccordion").each(function(){
				var mustExpand = ($(this).attr("data-expand") == "true") ? 0 : false;
				$(this).accordion({
					collapsible: true,
					active: mustExpand,
					heightStyle: "content",
					autoHeight: false
				});
			});
		},
		error: function(data) {
			console.log(data);
		}
	});
}

function toggleDepartments()
{
	var zoomAreas = $("#all > g");
	$(zoomAreas).each(function (){
		if (this.id != "Background_Layer" && this.id != "Promo")
		{
			$("#" + this.id).toggle();
		}
	});
	$("#Promo-Box").toggle();
}


