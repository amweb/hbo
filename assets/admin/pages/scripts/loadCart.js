function loadCart(orderid, campaignid) {
	var message = "";
	$.ajax({
		type: "POST",
		url: "../config/ajax_functions.asp",
		data: "action=loadCartContents&orderid="+orderid,
		success: function(data)
		{
			//console.log(data);
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					var actualData = data[key];
					var strPhotoURL = "";
					message += "<tr class=\"cartitem\">";
						message += "<td valign=\"top\" class=\"cartitem-image\" style=\"width:120px\">";
						message += "<div style=\"background-image:url('../images/products/" + actualData["vchImageURL"] + "'), url('../images/icon-no-image-512.png');margin-bottom:20px;width:100px;height:80px;background-repeat:no-repeat;background-size:cover;boder:1px solid #ccc;\"></div>";
						message += "</td>";
						message += "<td valign=\"top\" class=\"cartitem-data\">";
                
							message += "<table cellpadding=\"1\" cellspacing=\"1\">";
								message += "<tr align=\"left\">";
									message += "<td colspan=\"2\" style=\"font-weight:bold;font-size:larger;\">" + actualData["vchItemName"] + "</td>";
								message += "</tr>";
								message += "<tr align=\"left\">";
									message += "<td>Item #:</td><td algin=\"right\">" + actualData["vchPartNumber"] + "</td>";
								message += "</tr>";
								message += "<tr align=\"left\">";
									message += "<td>Bundle Quantity:&nbsp;</td><td algin=\"right\">1</td>";
								message += "</tr>";
								message += "<tr class=\"no-print\">";
									message += "<td>Quantity:</td><td><input type=\"text\" name=\"intQty_" + orderid + "_" + actualData["intID"] + "\" value=\"" + actualData["intQuantity"] +"\" size=\"2\" /> <input type=\"submit\" value=\"Update Quantities\" /></td>";
								message += "</tr>";
								message += "<tr class=\"no-print\">";
									message += "<td colspan=\"2\" style=\"padding:10px 0px 20px 0px;\">";
										message += "<a style=\"color: red;\" href=\"mcb.asp?id=" + actualData["intID"] + "&action=delitem&campaignid=" + campaignid + "\" onClick=\"return RUSure();\" >Remove Item From Cart</a>";
									message += "</td>";
								message += "</tr>";
							message += "</table>";
						message += "</td>";
					message += "</tr>";
					//console.log(message);
					$("#orderid-" + orderid + "-table").html(message);
				}
			};// show response from the php script.
		},
			error: function(data)
		{
			console.log(data);
		}
	});
	return false;
}
