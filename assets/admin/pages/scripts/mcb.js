var blnAllowSubmit = false;
function validate(){
	$(".specifications select").each(function(){
		if($(this).val() == "default"){
			blnAllowSubmit = false;
			$("button.invididualitemaddtocart").addClass("invididualitemaddtocart-disabledbutton");
			$("button.invididualitemaddtocart").removeClass("invididualitemaddtocart-enabledbutton");
			$("button.invididualitemaddtocart").attr("disabled", "disabled");
			/*
			$("button.invididualitemaddtocart").removeClass("invididualitemaddtocart-disabledbutton");
			$("button.invididualitemaddtocart").addClass("invididualitemaddtocart-enabledbutton");
			$("button.invididualitemaddtocart").removeAttr("disabled");
			*/
			return false;
		}else{
			$("button.invididualitemaddtocart").removeClass("invididualitemaddtocart-disabledbutton");
			$("button.invididualitemaddtocart").addClass("invididualitemaddtocart-enabledbutton");
			$("button.invididualitemaddtocart").removeAttr("disabled");
			if($("input[name='vchItemName']").val() == ""){
				$("button.invididualitemaddtocart").addClass("invididualitemaddtocart-disabledbutton");
				$("button.invididualitemaddtocart").removeClass("invididualitemaddtocart-enabledbutton");
				$("button.invididualitemaddtocart").attr("disabled", "disabled");
			}else{
				$("button.invididualitemaddtocart").removeClass("invididualitemaddtocart-disabledbutton");
				$("button.invididualitemaddtocart").addClass("invididualitemaddtocart-enabledbutton");
				$("button.invididualitemaddtocart").removeAttr("disabled");
			}
		}
	});
	blnAllowSubmit = true;
}

$(document).ready(function(){
	/* validate new items being added to profiles */
	$(".specifications select").change(function(){
		validate();
	});
	$(".specifications input[name='vchItemName']").keyup(function(){
		validate();
	});
	/* Color Process */
	$("#vchNumSides").change(function(){
		var fixture = $(this).attr("data-fixture-id");
		var intSides = $("option:selected", this).attr("value");
		ColorProcessHandler(fixture, intSides);
	});
	/* Dialogboxes */
	$("#createform").dialog({
		//open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).hide(); },
		//close: function(event, ui){ window.history.back(); },
		draggable: true,
		resizable: true,
		modal: true,
		dialogClass: "noTitleStuff",
		width: "30%"
	});
	$("#createform .dialogclose").click(function(){
		window.location.assign("../store/dashboard.asp");
	});
	$("#addresses").dialog({
		//open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).hide(); },
		//close: function(event, ui){ window.history.back(); },
		draggable: true,
		resizable: false,
		modal: true,
		dialogClass: "noTitleStuff",
		width: 700,
		height: 575
	});
	/* fixture selection */
	$("#fixtureid").change(function(){
		$("form.mcbinventorysearch").submit();
	});
	/** right hand side AJAX builder listener for accordions **/
	$(".mcbinventoryright .accordion h3").click(function(){
		if($(this).attr("data-fixtures-loaded") == "false"){
			loadProfilePackage(parseInt($(this).attr("data-profilepackage-intid")), parseInt($(this).attr("data-profilepackage-campaign")));
			$(this).attr("data-fixtures-loaded", "true");
		}
	});
	/* accordions, as needed */
	$( ".accordion" ).each(function(){
		$(this).accordion({
			collapsible: true,
			heightStyle: "content",
			active: false
		});
	});
	/* Change campaign on sign builder */
	$("a#changecampaign").click(function(event){
		event.preventDefault;
		$("form[name='changecampaign']").dialog({
			draggable: true,
			resizable: false,
			modal: true,
			dialogClass: "noTitleStuff",
			width: 700,
			height: 100
		});
	});
	$("form[name='changecampaign'] #campaignid").change(function(){
		if($(this).val()){
			$("form[name='changecampaign']")[0].submit();
		}
	});
});