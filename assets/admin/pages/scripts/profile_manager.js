/*
$(".fancybox").fancybox({
	arrows : false
});
*/
$(document).ready(function(){
	/* schematic popout */
	$("a.layoutzoom").fancybox({
		arrows: false,
		type: 'iframe',
		autoSize: false,
		width: 500,
		height: 425,
		fitToView: false,
		beforeShow : function() {
			var alt = this.element.find('img').attr('alt');
			var url = this.element.attr("href");
			this.inner.find('img').attr('alt', alt);
			var externalurl = "<br><a href='" + url + "' target='_blank'>Open this image in a new tab</a>. <i class='fa fa-external-link' aria-hidden='true'></i>"
			this.title = alt + externalurl;
		}
	});
	/* AJAX */
	/*
	$("input.addstoretoprofile").change(function(){
		if($(this).is(":checked")){
			AddStore($(this).attr("data-profileid"), $(this).attr("data-storeid"));
		}else{
			RemoveStore($(this).attr("data-profileid"), $(this).attr("data-storeid"));
		}
	});
	*/
	
	
	
	/* popout for UI */
	var minimumwidth = parseInt($("#profilepopout").attr("data-dialog-minwidth"));
	$("#profilepopout").dialog({
		open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).hide(); },
		draggable: false,
		resizable: false,
		modal: true,
		dialogClass: "noTitleStuff",
		maxHeight: screen.height,
		minWidth : minimumwidth
	});	
	/* Button reset listener */
	$("button[type='reset']").click(function(){
		window.history.go(-1);
	});
	$("form#assignaddressestoprofile").submit(function(){
		var intNumCheckedBoxes = 0;
		$("form#assignaddressestoprofile .addstoretoprofile").each(function(){
			if($(this).attr("checked")){
				intNumCheckedBoxes++;
			}
		});
		if(intNumCheckedBoxes == 0){
			$("p#assignmenterror").show();
			event.preventDefault();
		}
	});
	/* Profile list */
	$("#storelistcontainer button.resetnoredirect").click(function(){
		$("input[name='searchstore']").removeAttr("value");
		$("#storelistcontainer form select").each(function(){
			$(this).val('');
		});
		$("#storelistcontainer form").submit();
	});
	
	
	/* Address selection */
	$("input[name='selectAll']").click(function(){
		if($(this).is(":checked")){
			$("#assigntoprofile tbody tr[visible='true'] input[type='checkbox']").each(function(){
				$(this).prop('checked', "true");
			});
		}else{
			$("#assigntoprofile tbody tr[visible='true'] input[type='checkbox']").each(function(){
				$(this).removeAttr('checked');
			});
		}
	});
	var arrayTableRows = $("#assigntoprofile tbody tr[visible='true']");
	for(n = 0; n < arrayTableRows.length; n++){
		if(n % 2 !== 0){
			$(arrayTableRows[n]).css("background-color", "#f0f0f0");
		}
	}
	/* Autosubmit form for Profile Create and Profile Assignment */
	$(".formautosubmit").submit();
	
	/* search add address to profile table */
	$("button#addresssearchsubmit").click(function(){
		// text search
		var searchStr = $(".search").val();
		var listItem = $('.results tbody').children('tr');
		var searchSplit = searchStr.replace(/ /g, "'):containsi('")
		
		$.extend($.expr[':'], {
			'containsi': function(elem, i, match, array){
				return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
			}
		});
		$(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e){
			$(this).attr('visible','false');
		});
		$(".results tbody tr:containsi('" + searchSplit + "')").each(function(e){
			$(this).attr('visible','true');
		});
		var jobCount = $('.results tbody tr[visible="true"]').length;
		$('.counter').text(jobCount + ' item');
		if(jobCount == '0'){
			$('.no-result').show();
		}else{
			$('.no-result').hide();
		}
		// store id search
		var storeIDSearchTerm = $("form#jqSearch select[name='intStoreID']").val();
		
		$(".results tbody tr[visible='true'] td[data-search-id='intStoreID']").filter(function(){
			if($(this).text() == storeIDSearchTerm){
				$(this).parent().attr('visible','true');
			}else if (storeIDSearchTerm == "") {
				$(this).parent().attr('visible','true');
			}else{
				$(this).parent().attr('visible','false');
			}
		});
		//district search
		var districtIDSearchTerm = $("form#jqSearch select[name='intDistrict']").val();
		$(".results tbody tr[visible='true'] td[data-search-id='intDistrict']").filter(function(){
			if($(this).text() == districtIDSearchTerm){
				$(this).parent().attr('visible','true');
			}else if (districtIDSearchTerm == "") {
				$(this).parent().attr('visible','true');
			}else{
				$(this).parent().attr('visible','false');
			}
		});
		//region search
		var regionSearchTerm = $("form#jqSearch select[name='vchDivision']").val();
		$(".results tbody tr[visible='true'] td[data-search-id='vchDivision']").filter(function(){
			if($(this).text() == regionSearchTerm){
				$(this).parent().attr('visible','true');
			}else if (regionSearchTerm == "") {
				// Clear any search parameters
				$(this).parent().attr('visible','true');
			}else{
				$(this).parent().attr('visible','false');
			}
		});
		$("#assigntoprofile tbody tr").each(function(){
			$(this).css("background-color", "");
		});
		
		for(n = 0; n < $("#assigntoprofile tbody tr[visible='true']").length; n++){
			if(n % 2 !== 0){
				$($("#assigntoprofile tbody tr[visible='true']")[n]).css("background-color", "#f0f0f0");
			}
		}
	});
	$("#resetsearch").click(function(){
		$("form#jqSearch")[0].reset();
		$(".results tbody tr").each(function(){
			$(this).attr("visible", "true");
			$(this).css("background-color", "");
		});
		for(n = 0; n < $("#assigntoprofile tbody tr").length; n++){
			if(n % 2 !== 0){
				$($("#assigntoprofile tbody tr")[n]).css("background-color", "#f0f0f0");
			}
		}
	});
	
	//var intPopoutHeight = $("#profilepopout").height() - (screen.height/20);
	var intPopoutHeight = $("#profilepopout").height() - ($("#profilepopout").height()/3);
	//var intPopoutHeight = $("#profilepopout").height() - (250);
	$("#table-scroll").css("height", intPopoutHeight.toString() + "px");
	
	/* end search table */
	
});

function ResetFormAndReturn() {
	for(i = 0; i < $("form").length; i++){
		$("form")[i].reset();
	}
	//$("form").find('input, select').not(':button, :submit, :reset, :hidden').val('').removeAttr('checked').removeAttr('selected');
	//window.location.replace("profile_manager.asp");
}