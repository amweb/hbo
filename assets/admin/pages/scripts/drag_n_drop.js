var $draggedItem;

$(document).ready(function () {
	jQuery.event.props.push('dataTransfer');
	$('.item').on('dragstart', dragging);
	$('.item').on('dragend', draggingEnded);
	$('.hole').on('dragenter', preventDefault);
	$('.hole').on('dragover', preventDefault);
	$('.hole').on('drop', dropItem);
});

function dragging(e) {
	var val = e.target.getAttribute('data-value');
	e.dataTransfer.effectAllowed = 'copy';
	e.dataTransfer.setData('text', val);
	
	//console.log(e);
	//console.log(val);
	$draggedItem = $(e.target);
}

function draggingEnded(e) {
	$(e.target).removeClass('dragging');
}

function preventDefault(e) {
	e.preventDefault();
}

function dropItem(e) {
	//console.log(e);
	/*e.preventDefault();
	var data = e.dataTansfer.getData('text').split(',');
	if (data[0] == 'sku') {
		var li = document.createElement('li');
		li.textContent = data[1];
		e.target.appendChild(li);
	}
	*/
	var hole = $(e.target);
	if (hole.hasClass('hole') || hole.parents().hasClass('hole') ) {
		
		$draggedItem.detach();
		$draggedItem.appendTo($(hole).closest('.hole'));
		//console.log("action=savePackSlip&pickSlipNum="+$($(hole).closest('.hole')).attr('id').replace('packSlip', '')+"&sku="+encodeURIComponent($($draggedItem[0]).data('value')));
		$.ajax({
           type: "POST",
           url: "../config/ajax_functions.asp",
           data: "action=savePackSlip&pickSlipNum="+$($(hole).closest('.hole')).attr('id').replace('packSlip', '')+"&sku="+encodeURIComponent($($draggedItem[0]).data('value')), // serializes the form's elements.
		   //console.log ("action=savePackSlip&pickSlipNum="+$($(hole).closest('.hole')).attr('id').replace('packSlip', '')+"&sku="+$($draggedItem[0]).data('value'));
           success: function(data)
           {
               // alert(data); // show response from the php script.
           },
		   error: function(data)
		   {
			//console.log(data);
		   }
         });
		
	}
	
}