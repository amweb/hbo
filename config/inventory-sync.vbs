dim gblnConnOpen, gobjConn, gstrConnectString,strSQL,rs

'gstrConnectString = "DSN=drcportalLive; UID=webuser; PWD=welcome"

gstrConnectString = "Provider=SQLOLEDB.1;Data Source='10.0.10.146';Initial Catalog='WebData';user id = 'webuser';password='welcome'"

sub OpenConn()
	if not gblnConnOpen then
		set gobjConn = CreateObject("ADODB.Connection")
        'Response.Write gstrConnectString
		gobjConn.Open gstrConnectString
		gblnConnOpen = true
	end if
end sub

sub CloseConn()
	if gblnConnOpen and IsObject(gobjConn) then
		gobjConn.Close
		set gobjConn = nothing
		gblnConnOpen = false
	end if
end sub

OpenConn
CONST STR_USERNAME = "hbouser"
CONST STR_PASSWORD = "2ujA@WaYx9Yq"
const STR_TABLE_INVENTORY = "HBO_Inv"
const STR_TABLE_CODE = "HBO_GuideVersion"

call getProductAvail
CloseConn

Function getProductAvail
	strSQL = "SELECT intID,vchPartNumber FROM "&STR_TABLE_INVENTORY&" WHERE intID in (SELECT DISTINCT intInvID FROM "&STR_TABLE_CODE&") and chrStatus='A'"
	set rs = gobjConn.execute(strSQL)
	if not rs.eof then
		Do Until rs.eof
			Dim stock
			stock = GetProductAvailabilites(rs("vchPartNumber"))
			strSQL = "UPDATE "&STR_TABLE_INVENTORY&" SET intStock="&stock&" ,dtmUpdated=GETDATE(), vchUpdatedByUser='"&gintUserName&"'"
			strSQL = strSQL &" WHERE intID="&rs("intID")
			gobjConn.execute(strSQL)

			rs.movenext
		Loop
	end if
End Function

Function GetProductAvailabilites(strProductNumber)

Dim oXmlHTTP, oXmlResp,oXMlNodes, oXmlProductAvailabilityResult, oXmlOfferResultId, oXmlOfferResultDescription, oXmlOfferResultAvailable


Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
oXmlHTTP.Open "POST", "http://www.drcinventory.com/pmomsws/order.asmx", False 

oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
oXmlHTTP.setRequestHeader "SOAPAction", "http://sma-promail/GetProductAvailabilities"
'On Error Resume Next
'response.write GetProductAvailabilitiesXml(Trim(strProductNumber))
oXmlHTTP.send GetProductAvailabilitiesXml(Trim(strProductNumber))    
'Response.Write strProductNumber &"<br />"
'Response.Write oXmlHTTP.responseText


Set oXmlResp = oXmlHTTP.responseXML
Set oXmlNodes = oXmlResp.getElementsByTagName("Available")
GetProductAvailabilites = oXmlNodes(0).Text

If Err.Number Then
   GetProductAvailabilites = 0
  

End If
On Error Goto 0
End Function
Function GetProductAvailabilitiesXml(vchPartNumber)

Dim strXml

strXml = ""
strXml = strXml & "<GetProductAvailabilities xmlns=""http://sma-promail/"">" & vbNewLine
strXml = strXml & "  <partNumber>"&vchPartNumber&"</partNumber>" & vbNewLine
strXml = strXml & "  <owner>HBO</owner>" & vbNewLine
strXml = strXml & "</GetProductAvailabilities>" & vbNewLine

GetProductAvailabilitiesXml = GetXml(strXml)
End Function
Function SoapHeader
SoapHeader =""
SoapHeader = SoapHeader & "<soap:Header>" & vbNewLine
SoapHeader = SoapHeader & "    <AuthenticationHeader xmlns=""http://sma-promail/"">" & vbNewLine
SoapHeader = SoapHeader & "      <Username>" & STR_USERNAME & "</Username>" & vbNewLine
SoapHeader = SoapHeader & "      <Password>" & STR_PASSWORD & "</Password>" & vbNewLine
SoapHeader = SoapHeader & "    </AuthenticationHeader>" & vbNewLine
SoapHeader = SoapHeader & "  </soap:Header>" & vbNewLine

End Function

Function GetXml(strBodyXML)

GetXml = ""
GetXml = GetXml & "<?xml version=""1.0"" encoding=""utf-8""?>" &vbNewLine
GetXml = GetXml & "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" &vbNewLine
GetXml = GetXml & SoapHeader

GetXml = GetXml & "<soap:Body>" &vbNewLine

GetXml = GetXml & strBodyXML

GetXml = GetXml & "</soap:Body>" &vbNewLine
GetXml = GetXml & "</soap:Envelope>" &vbNewLine

End Function