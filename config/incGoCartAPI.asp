<!--#include file="incGoCartCustom.asp"-->
<!--#include file="incUPS.asp"-->
<%

' =====================================================================================
' = File: incGoCartAPI.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   GoCart Main API Code
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   Check with ssutterfield before making any changes to this code
' =====================================================================================

dim dctOrderStatus, dctOrderStatusInvoiceValues, dctOrderStatusRawValues, dctModifyFlagValues, dctElecCheckAcctValues, dctTransType, dctTransStatusValues, dctAVSValues, dctOrderTypeValues, dctOrderStatusSearchValues, dctYesNoValues, dctInvTypeValues, dctInvStatusValues, dctShopperStatusValues, dctShopperTypeValues, dctTransModeValues, dctTransAcctTypeValues, dctCheckAcctTypeValues

set dctAVSValues = Server.CreateObject("Scripting.Dictionary")
	dctAVSValues.Add "X", "Street Address/ZIP Matched"
	dctAVSValues.Add "Y", "Street Address/ZIP Matched"
	dctAVSValues.Add "A", "Street Address Matched but not ZIP"
	dctAVSValues.Add "W", "9-digit ZIP Matched but not Street Address"
	dctAVSValues.Add "Z", "5-digit ZIP Matched but not Street Address"
	dctAVSValues.Add "U", "Card Issuer Unavailable, unable to verify"
	dctAVSValues.Add "N", "Neither Street Address nor ZIP Matched"
	dctAVSValues.Add "R", "Card Issuer Not Able to Process Request"
	dctAVSValues.Add "S", "Card Issuer Does Not Support AVS"
	dctAVSValues.Add "E", "Unable to Verify Due to Error"

set dctYesNoValues = Server.CreateObject("Scripting.Dictionary")
	dctYesNoValues.Add "Y", "Yes"
	dctYesNoValues.Add "N", "No"

set dctInvTypeValues = Server.CreateObject("Scripting.Dictionary")
	dctInvTypeValues.Add "A", "Folder"
	dctInvTypeValues.Add "I", "Item"

set dctInvStatusValues = Server.CreateObject("Scripting.Dictionary")
	dctInvStatusValues.Add "A", "Active"
	dctInvStatusValues.Add "I", "Inactive"

set dctShopperStatusValues = Server.CreateObject("Scripting.Dictionary")
	dctShopperStatusValues.Add "A", "Active"
	dctShopperStatusValues.Add "I", "Inactive"
    dctShopperStatusValues.Add "C", "Must Change Password"

set dctShopperTypeValues = Server.CreateObject("Scripting.Dictionary")
	dctShopperTypeValues.Add "A", "Shopper"
	dctShopperTypeValues.Add "B", "Billing"
	dctShopperTypeValues.Add "S", "Shipping"

set dctOrderStatus = Server.CreateObject("Scripting.Dictionary")
	dctOrderStatus.Add "B", "Abandoned"
	dctOrderStatus.Add "0", "Open"
	dctOrderStatus.Add "1", "Open"
	dctOrderStatus.Add "2", "Open"
	dctOrderStatus.Add "3", "Open"
	dctOrderStatus.Add "4", "Open"
	dctOrderStatus.Add "5", "Open"
	dctOrderStatus.Add "6", "Open"
	dctOrderStatus.Add "7", "Open"
	dctOrderStatus.Add "8", "Open"
	dctOrderStatus.Add "9", "Open"
	dctOrderStatus.Add "P", "Pending"
	dctOrderStatus.Add "S", "Closed"
	dctOrderStatus.Add "A", "Closed"
	dctOrderStatus.Add "H", "Shipped"
	dctOrderStatus.Add "K", "Declined"
	dctOrderStatus.Add "Z", "Authorized"
	dctOrderStatus.Add "X", "Deposited"
	dctOrderStatus.Add "C", "Credited"
	dctOrderStatus.Add "E", "Error"
	dctOrderStatus.Add "V", "Voice-Auth"

set dctOrderStatusInvoiceValues = Server.CreateObject("Scripting.Dictionary")
	dctOrderStatusInvoiceValues.Add "0", "Not Submitted"
	dctOrderStatusInvoiceValues.Add "1", "Not Submitted"
	dctOrderStatusInvoiceValues.Add "2", "Not Submitted"
	dctOrderStatusInvoiceValues.Add "3", "Not Submitted"
	dctOrderStatusInvoiceValues.Add "4", "Not Submitted"
	dctOrderStatusInvoiceValues.Add "5", "Not Submitted"
	dctOrderStatusInvoiceValues.Add "6", "Not Submitted"
	dctOrderStatusInvoiceValues.Add "7", "Not Submitted"
	dctOrderStatusInvoiceValues.Add "8", "Not Submitted"
	dctOrderStatusInvoiceValues.Add "9", "Not Submitted"
	dctOrderStatusInvoiceValues.Add "P", "Pending"
	dctOrderStatusInvoiceValues.Add "S", "Submitted"
	dctOrderStatusInvoiceValues.Add "A", "Submitted"
	dctOrderStatusInvoiceValues.Add "H", "Shipped"
	dctOrderStatusInvoiceValues.Add "K", "Declined"
	dctOrderStatusInvoiceValues.Add "Z", "Submitted"
	dctOrderStatusInvoiceValues.Add "X", "Shipped"
	dctOrderStatusInvoiceValues.Add "C", "Credited"
	dctOrderStatusInvoiceValues.Add "E", "Submitted"
	dctOrderStatusInvoiceValues.Add "V", "Submitted"

set dctOrderStatusRawValues = Server.CreateObject("Scripting.Dictionary")
	dctOrderStatusRawValues.Add "B", "B-Abandoned"
	dctOrderStatusRawValues.Add "0", "0-In Progress"
	dctOrderStatusRawValues.Add "1", "1-In Progress"
	dctOrderStatusRawValues.Add "2", "2-In Progress"
	dctOrderStatusRawValues.Add "3", "3-In Progress"
	dctOrderStatusRawValues.Add "4", "4-In Progress"
	dctOrderStatusRawValues.Add "5", "5-In Progress"
	dctOrderStatusRawValues.Add "6", "6-In Progress"
	dctOrderStatusRawValues.Add "7", "7-In Progress"
	dctOrderStatusRawValues.Add "8", "8-In Progress"
	dctOrderStatusRawValues.Add "9", "9-Merchant In Progress"
	dctOrderStatusRawValues.Add "P", "P-Pending"
	dctOrderStatusRawValues.Add "S", "S-Submitted"
	dctOrderStatusRawValues.Add "A", "A-Approved"
	dctOrderStatusRawValues.Add "H", "H-Shipped"
	dctOrderStatusRawValues.Add "K", "K-Declined"
	dctOrderStatusRawValues.Add "Z", "Z-Authorized"
	dctOrderStatusRawValues.Add "X", "X-Deposited"
	dctOrderStatusRawValues.Add "C", "C-Credited"
	dctOrderStatusRawValues.Add "E", "E-Error"

set dctOrderStatusSearchValues = Server.CreateObject("Scripting.Dictionary")
	dctOrderStatusSearchValues.Add "B", "Abandoned"
	dctOrderStatusSearchValues.Add "0", "In Progress"
	dctOrderStatusSearchValues.Add "9", "Merchant In Progress"
	dctOrderStatusSearchValues.Add "P", "Pending"
	dctOrderStatusSearchValues.Add "S", "Submitted"
	dctOrderStatusSearchValues.Add "A", "Approved"
	dctOrderStatusSearchValues.Add "H", "Shipped"
	dctOrderStatusSearchValues.Add "K", "Declined"
	dctOrderStatusSearchValues.Add "Z", "Authorized"
	dctOrderStatusSearchValues.Add "X", "Deposited"
	dctOrderStatusSearchValues.Add "C", "Credited"
	dctOrderStatusSearchValues.Add "E", "Error"

set dctCheckAcctTypeValues = Server.CreateObject("Scripting.Dictionary")
	dctCheckAcctTypeValues.Add "R", "Personal"
	dctCheckAcctTypeValues.Add "B", "Business"

set dctOrderTypeValues = Server.CreateObject("Scripting.Dictionary")
	dctOrderTypeValues.Add "R", "Regular"
	dctOrderTypeValues.Add "D", "Demo"

' translation list for Order.chrModifyFlag field
set dctModifyFlagValues = Server.CreateObject("Scripting.Dictionary")
	dctModifyFlagValues.Add "A", "by admin"
	dctModifyFlagValues.Add "M", "by merchant"

set dctTransModeValues = Server.CreateObject("Scripting.Dictionary")
	dctTransModeValues.Add "R", "Real-Time"
	dctTransModeValues.Add "M", "Manual"

set dctTransAcctTypeValues = Server.CreateObject("Scripting.Dictionary")
	dctTransAcctTypeValues.Add "M", "Merchant"
	dctTransAcctTypeValues.Add "D", "Demo"

set dctTransType = Server.CreateObject("Scripting.Dictionary")
	dctTransType.Add "ES", "Auth/Dep"
	dctTransType.Add "EV", "Auth/Dep w/Address"
	dctTransType.Add "AS", "Auth"
	dctTransType.Add "AD", "AVC"
	dctTransType.Add "AV", "Auth"
	dctTransType.Add "DS", "Dep"
	dctTransType.Add "CR", "Credit"
	dctTransType.Add "DD", "Debit"
	dctTransType.Add "DC", "Credit"

set dctTransStatusValues = Server.CreateObject("Scripting.Dictionary")
	dctTransStatusValues.Add "P", "Pending"
	dctTransStatusValues.Add "A", "Approved"
	dctTransStatusValues.Add "K", "Declined"
	dctTransStatusValues.Add "V", "Declined (voice-auth)"
	dctTransStatusValues.Add "I", "VOID (payment changed)"
	dctTransStatusValues.Add "E", "Error"

set dctElecCheckAcctValues = Server.CreateObject("Scripting.Dictionary")
	dctElecCheckAcctValues.Add "P", "Personal Account"
	dctElecCheckAcctValues.Add "B", "Business Account"

' Session Variables
const SESSION_PUB_ORDER_ID = "FLORAL_PUB_ORDER_ID"	' gintOrderID
const SESSION_PUB_USER_ID = "FLORAL_PUB_USER_ID"	' gintUserID
const SESSION_PUB_USER_NAME = "FLORAL_PUB_USER_NAME"	' gintUserName

' Global Variables
dim gintOrderID	' equals zero if no current order, otherwise equals ID of current order
gintOrderID = 0	' must call SelectCurrentOrder() to initialize
dim gstrOrderStatus ' current order status
gstrOrderStatus = "" ' must call SelectCurrentOrder() to initialize
dim gintUserID ' equals zero if no logged-in user, otherwise equals ID of current user
dim gintUserName
dim gintOrderItemCount ' # of items in the current order
dim gintOrderSubtotal ' subtotal of the current order (minus shipping and taxes)

gintUserID = Session(SESSION_PUB_USER_ID)
if IsNumeric(gintUserID) then
	gintUserID = CLng(gintUserID)
else
	gintUserID = 0
end if
gintUserName = Session(SESSION_PUB_USER_NAME)

' Misc Routines
sub GoCartEncodeString(strSource, strDestin)
	strDestin = strSource
end sub

function GoCartDecodeString(strSource, strDestin)
	strDestin = strSource
	GoCartDecodeString = true
end function

function CheckForError(strLabel, tfSendEmail, tfClearError)
	if err.number <> 0 then
		if tfSendEmail then
			dim strText
			strText = "ASP Err " & err.number & ": " & err.description & " (" & err.source & ")" & vbcrlf
			strText = strText & "Source Label: " & strLabel & vbcrlf
			SendAdminErrorEmail strText
		end if
		if tfClearError then
			err.clear
		end if
		CheckForError = true
	else
		CheckForError = false
	end if
end function

sub SendAdminErrorEmail(strText)
	' notify SacWeb about a critical system error
	' and redirect the user to an error page
	' this should ONLY email gocartadmin@sacweb.com, please...
	dim b
	b = "A serious GoCart system error has occurred." & vbcrlf
	b = b & vbcrlf
	b = b & "Error: " & strText & vbcrlf
	b = b & vbcrlf
	b = b & "Current Order ID (var): " & gintOrderID & vbcrlf
	b = b & "Current Order ID (sess): " & Session(SESSION_PUB_ORDER_ID) & vbcrlf
	b = b & "Current User ID (var): " & gintUserID & vbcrlf
	b = b & "Current User ID (sess): " & Session(SESSION_PUB_USER_ID) & vbcrlf
	b = b & vbcrlf
	b = b & "Time: " & Now() & vbcrlf
	b = b & "Host: " & Request.ServerVariables("HTTP_HOST") & vbcrlf
	b = b & "Script: " & Request.ServerVariables("SCRIPT_NAME") & vbcrlf
	b = b & "User: " & Request.ServerVariables("REMOTE_USER") & vbcrlf
	b = b & "User IP: " & Request.ServerVariables("REMOTE_ADDR") & vbcrlf
	b = b & "Browser: " & Request.ServerVariables("HTTP_USER_AGENT") & vbcrlf
	b = b & vbcrlf
	SendMail STR_MERCHANT_CS_EMAIL, "support@americanwebservices.com", "", "", "GoCart System Error", b
	response.redirect aspbase & "error.asp?err=" & Server.URLEncode(strText)
	response.write "</A></TD></TR></TABLE></TD></TR></TABLE></TD></TR></TABLE><BR><BR><BR>"
	response.write "<B>A system error has occurred. Please <A HREF=""error.asp?err=" & Server.URLEncode(strText) & """>click here</A> to continue."
	response.end
end sub

' User Account Routines
function LoginUser(strEmail, strPassword)
	' attempt to login the specified user
	' if login is successful and there is a current order in progress,
	' assign this user to the order
	' returns true if login successful	
	dim strSQL, rsTemp	
	strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE chrStatus in ('A','C') AND vchEmail='" & SQLEncode(strEmail) & "' AND vchPassword='" & SQLEncode(strPassword) & "'"
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		gintUserID = CLng(rsTemp("intID"))
		Session(SESSION_PUB_USER_ID) = gintUserID
		Session(SESSION_PUB_USER_NAME) = rsTemp("vchFirstName") & " " & rsTemp("vchLastName")
        Session("chrStatus") = rsTemp("chrStatus")
		gintUserName = Session(SESSION_PUB_USER_NAME)		
		LoginUser = true
		if gintOrderID > 0 then
			strSQL = "UPDATE " & STR_TABLE_ORDER & " SET intShopperID=" & gintUserID & " WHERE intID=" & gintOrderID
			gobjConn.execute(strSQL)
		end if
	else
		LoginUser = false
	end if
	rsTemp.close
	set rsTemp = nothing
end function

function LoginDummyUser()
	' attempt to login the specified user
	' if login is successful and there is a current order in progress,
	' assign this user to the order
	' returns true if login successful	
	dim strSQL, rsTemp
	'------------------JRMCodeChange---------	
	strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE chrStatus='A' AND intID=1"
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		'gintUserID = CLng(rsTemp("intID"))
		gintUserId = -1
		'response.write "gint: " & gintUserID
		Session(SESSION_PUB_USER_ID) = gintUserID
		Session(SESSION_PUB_USER_NAME) = "Dummy User"
		gintUserName = Session(SESSION_PUB_USER_NAME)
		'------------------JRMCodeChange---------
		Session("DummyUser")=True
		LoginDummyUser = true
		if gintOrderID > 0 then
			strSQL = "UPDATE " & STR_TABLE_ORDER & " SET intShopperID=" & gintUserID & " WHERE intID=" & gintOrderID
			gobjConn.execute(strSQL)
		end if
	else
		LoginDummyUser = false
	end if
	rsTemp.close
	set rsTemp = nothing
end function

sub LogoutUser()
	' logout the current user
	' abandons any current order
	if gintOrderID > 0 then
		AbandonOrder
	end if
	gintUserID = 0
	Session(SESSION_PUB_USER_ID) = 0
end sub

function CreateNewUser(strEmail, strPassword, strHint, strFirstname, strLastname, strAddress, strCity, strState, strZip, strCountry)
	' creates a new user
	' returns the user ID of the new account
	' returns zero if duplicate username
	' logs in the new user
	dim strSQL, rsTemp
	strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE chrType='A' AND chrStatus<>'D' AND vchEmail='" & SQLEncode(strEmail) & "'"
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		CreateNewUser = 0
		rsTemp.close
		set rsTemp = nothing
	else
		rsTemp.close
		set rsTemp = nothing
		dim dctSaveList
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "*@dtmCreated", "GETDATE()"
			dctSaveList.Add "@dtmUpdated", "GETDATE()"
			dctSaveList.Add "*vchCreatedByUser", "pub"
			dctSaveList.Add "vchUpdatedByUser", "pub"
			dctSaveList.Add "*vchCreatedByIP", gstrUserIP
			dctSaveList.Add "vchUpdatedByIP", gstrUserIP
			dctSaveList.Add "chrType", "A"
			dctSaveList.Add "*chrStatus", "A"
			dctSaveList.Add "vchUsername", strEmail
			dctSaveList.Add "vchEmail", strEmail
			dctSaveList.Add "vchPassword", strPassword
			dctSaveList.Add "vchHint", strHint
			dctSaveList.Add "vchLastname", strLastname
			dctSaveList.Add "vchFirstname", strFirstname
			dctSaveList.Add "vchAddress1", strAddress
			dctSaveList.Add "vchCity", strCity
			dctSaveList.Add "vchState", strState
			dctSaveList.Add "vchZip", strZip
            dctSaveList.Add "vchCountry", strCountry
		CreateNewUser = SaveDataRecord("" & STR_TABLE_SHOPPER, Request, 0, dctSaveList)
		LoginUser strEmail, strPassword
	end if
end function

sub ChangeUserInfo(strEmail,strPassword, strHint, strFirstName, strLastName, strAddress, strCity, strState, strZip, strCountry, strBusinessUnit, strDivision)
	' updates the account information for the logged-in user
	' password is only changed if it is not blank
	if gintUserID > 0 then
		dim dctSaveList
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "@dtmUpdated", "GETDATE()"
			dctSaveList.Add "vchUpdatedByUser", "pub"
            dctSaveList.Add "chrStatus","A"
			dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		if strPassword <> "" then
			dctSaveList.Add "vchPassword", strPassword
		end if
			dctSaveList.Add "vchHint", strHint
			dctSaveList.Add "vchFirstName", strFirstName
			dctSaveList.Add "vchLastName", strLastName
			dctSaveList.Add "vchEmail", strEmail
			dctSaveList.Add "vchAddress1", strAddress
			dctSaveList.Add "vchCity", strCity
			dctSaveList.Add "vchState", strState
			dctSaveList.Add "vchZip", strZip
            dctSaveList.Add "vchCountry", strCountry
            dctSaveList.Add "vchBusinessUnit", strBusinessUnit
            dctSaveList.Add "vchDivision", strDivision
		SaveDataRecord STR_TABLE_SHOPPER, Request, gintUserID, dctSaveList
		Session(SESSION_PUB_USER_NAME) = strFirstName & " " & strLastName
		gintUserName = Session(SESSION_PUB_USER_NAME)
	end if
end sub


sub ChangeUserInfoEx(strEmail,strPassword, strHint, strFirstName, strLastName, strAddress, strCity, strState, strZip, strCountry, strBusinessUnit, strDivision, strDayPhone, strNightPhone, strFax)
	' updates the account information for the logged-in user
	' password is only changed if it is not blank
	if gintUserID > 0 then
		dim dctSaveList
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "@dtmUpdated", "GETDATE()"
			dctSaveList.Add "vchUpdatedByUser", "pub"
            dctSaveList.Add "chrStatus","A"
			dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		if strPassword <> "" then
			dctSaveList.Add "vchPassword", strPassword
		end if
			dctSaveList.Add "vchHint", strHint
			dctSaveList.Add "vchFirstName", strFirstName
			dctSaveList.Add "vchLastName", strLastName
			dctSaveList.Add "vchEmail", strEmail
			dctSaveList.Add "vchAddress1", strAddress
			dctSaveList.Add "vchCity", strCity
			dctSaveList.Add "vchState", strState
			dctSaveList.Add "vchZip", strZip
            dctSaveList.Add "vchCountry", strCountry
            dctSaveList.Add "vchBusinessUnit", strBusinessUnit
            dctSaveList.Add "vchDivision", strDivision
            dctSaveList.Add "vchDayPhone", strDayPhone
            dctSaveList.Add "vchNightPhone", strNightPhone
            dctSaveList.Add "vchFax", strFax
		SaveDataRecord STR_TABLE_SHOPPER, Request, gintUserID, dctSaveList
		Session(SESSION_PUB_USER_NAME) = strFirstName & " " & strLastName
		gintUserName = Session(SESSION_PUB_USER_NAME)
	end if
end sub

sub GetUserInfo(strHint, strFirstName, strLastName, strCompany, strEmail, strAddress, strCity, strState, strZip, strCountry, strBusinessUnit, strDivision)
	' returns information about the logged-in user
	if gintUserID > 0 then
		dim strSQL, rsTemp
		strSQL = "SELECT vchHint, vchFirstName, vchLastName, vchCompany, vchEmail, vchLastName, vchFirstName, vchAddress1, vchCity, vchState, vchZip, vchCountry, vchBusinessUnit, vchDivision FROM " & STR_TABLE_SHOPPER & " WHERE  intID=" & gintUserID
		set rsTemp = gobjConn.execute(strSQL)
		if not rsTemp.eof then
			strHint = rsTemp("vchHint")
			strFirstName = rsTemp("vchFirstName")
			strLastName = rsTemp("vchLastName")
			strCompany = rsTemp("vchCompany")
			strEmail = rsTemp("vchEmail")
			strLastname = rsTemp("vchLastName")
			strFirstname = rsTemp("vchFirstname")
			strAddress = rsTemp("vchAddress1")
			strCity = rsTemp("vchCity")
			strState = rsTemp("vchState")
			strZip = rsTemp("vchZip")
            strCountry = rsTemp("vchCountry")
            strBusinessUnit = rsTemp("vchBusinessUnit")
            strDivision = rsTemp("vchDivision")
		end if
		rsTemp.close
		set rsTemp = nothing
	end if
end sub

sub GetUserInfoEx(strHint, strFirstName, strLastName, strCompany, strEmail, strAddress, strCity, strState, strZip, strCountry, strBusinessUnit, strDivision, strDayPhone, strNightPhone, strFax)
	' returns information about the logged-in user
	if gintUserID > 0 then
		dim strSQL, rsTemp
		strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE  intID=" & gintUserID
		set rsTemp = gobjConn.execute(strSQL)
		if not rsTemp.eof then
			strHint = rsTemp("vchHint")
			strFirstName = rsTemp("vchFirstName")
			strLastName = rsTemp("vchLastName")
			strCompany = rsTemp("vchCompany")
			strEmail = rsTemp("vchEmail")
			strLastname = rsTemp("vchLastName")
			strFirstname = rsTemp("vchFirstname")
			strAddress = rsTemp("vchAddress1")
			strCity = rsTemp("vchCity")
			strState = rsTemp("vchState")
			strZip = rsTemp("vchZip")
            strCountry = rsTemp("vchCountry")
            strBusinessUnit = rsTemp("vchBusinessUnit")
            strDivision = rsTemp("vchDivision")
            strDayPhone = rsTemp("vchDayPhone")
            strNightPhone = rsTemp("vchNightPhone")
            strFax = rsTemp("vchFax")
		end if
		rsTemp.close
		set rsTemp = nothing
	end if
end sub
' Order Handling Routines
sub SelectCurrentOrder
	' if there is a current order, verifies that it is still 'in progress'
	gintOrderItemCount = 0
	gintOrderSubtotal = 0
	dim intOrderID
	if Request("order") <> "" then
		GoCartDecodeString Request("order"), gintOrderID
	else
		gintOrderID = Session(SESSION_PUB_ORDER_ID)
	end if
	
	if IsNumeric(gintOrderID) then
		gintOrderID = CLng(gintOrderID)
	else
		gintOrderID = 0
	end if
	
	gstrOrderStatus = ""
	
	if gintOrderID > 0 then
		dim strSQL, rsTemp
		strSQL = "SELECT O.chrStatus, O.intShopperID, O.mnyNonTaxSubtotal + O.mnyTaxSubtotal - O.mnyShipAmount AS mnySubtotal,"
		strSQL = strSQL & "(SELECT SUM(L.intQuantity) FROM " & STR_TABLE_LINEITEM & " AS L WHERE L.intOrderID=" & gintOrderID & " AND L.chrStatus='A') AS L_intCount "
		strSQL = strSQL & " FROM " & STR_TABLE_ORDER & " AS O WHERE O.intID=" & gintOrderID
		set rsTemp = gobjConn.execute(strSQL)
		if not rsTemp.eof then
			gstrOrderStatus = rsTemp("chrStatus")
			gintOrderItemCount = rsTemp("L_intCount")
			gintOrderSubtotal = rsTemp("mnySubtotal")
			if InStr("012345678U", gstrOrderStatus) > 0 then
				if not IsNull(rsTemp("intShopperID")) and gintUserID > 0 then
					if rsTemp("intShopperID") <> gintUserID then
						' this order isn't for this user--what's up with this?
						gintOrderID = 0
						gstrOrderStatus = ""
						gintOrderItemCount = 0
						gintOrderSubtotal = 0
					end if
				end if
			else
				gintOrderID = 0
				gstrOrderStatus = ""
				gintOrderItemCount = 0
				gintOrderSubtotal = 0
			end if
		else
			gintOrderID = 0
		end if
		rsTemp.close
		set rsTemp = nothing
	end if

	Session(SESSION_PUB_ORDER_ID) = gintOrderID
end sub

sub SelectSavedOrder(intID)
	' selects one of the user's saved orders
	if gintUserID > 0 then
		gintOrderID = intID
		gstrOrderStatus = ""
		if IsNumeric(gintOrderID) then
			gintOrderID = CLng(gintOrderID)
		else
			gintOrderID = 0
		end if
		
		if gintOrderID > 0 then
			dim strSQL, rsTemp
			strSQL = "SELECT chrStatus,intShopperID FROM " & STR_TABLE_ORDER & " WHERE intID=" & gintOrderID
			set rsTemp = gobjConn.execute(strSQL)
			if not rsTemp.eof then
				gstrOrderStatus = rsTemp("chrStatus")
				if InStr("012345678U", gstrOrderStatus) > 0 then
					if rsTemp("intShopperID") <> gintUserID then
						' this order isn't for this user--what's up with this?
						gintOrderID = 0
						gstrOrderStatus = ""
					end if
				else
					gintOrderID = 0
					gstrOrderStatus = ""
				end if
			else
				gintOrderID = 0
			end if
			rsTemp.close
			set rsTemp = nothing
		end if
		
		Session(SESSION_PUB_ORDER_ID) = gintOrderID
		
		if gintOrderID > 0 then
			' return order to "step 0" status
			UpdateOrderStatus "0"
		end if
	end if
end sub

function GetOrderStatus_Other(intOrderID)
	GetOrderStatus_Other = ""
	if intOrderID > 0 then
		dim strSQL, rsTemp
		strSQL = "SELECT chrStatus FROM " & STR_TABLE_ORDER & " WHERE intID=" & intOrderID
		set rsTemp = gobjConn.execute(strSQL)
		if not rsTemp.eof then
			GetOrderStatus_Other = rsTemp("chrStatus")
		end if
		rsTemp.close
		set rsTemp = nothing
	end if
end function


sub CreateNewOrderLt
	' creates a new order for the current user (or for no user if no user is logged in)
	gintOrderID = CreateNewOrder_OtherLt(gintUserID, "0")
	Session(SESSION_PUB_ORDER_ID) = gintOrderID
	gstrOrderStatus = "0"
end sub

function CreateNewOrder_OtherLt(intShopperID, strStatus)
	' creates a new order
	dim dctSaveList, intOrderID
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "*@dtmCreated", "GETDATE()"
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "*vchCreatedByUser", "pub"
		dctSaveList.Add "vchUpdatedByUser", "pub"
		dctSaveList.Add "*vchCreatedByIP", gstrUserIP
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "chrType", "R"
		dctSaveList.Add "chrStatus", strStatus
		dctSaveList.Add "vchShopperIP", gstrUserIP
		dctSaveList.Add "#intShopperID", intShopperID
	intOrderID = SaveDataRecord("" & STR_TABLE_ORDER_LONG_TERM, Request, 0, dctSaveList)
	CreateNewOrder_OtherLt = intOrderID
end function

sub CreateNewOrder
	' creates a new order for the current user (or for no user if no user is logged in)
	gintOrderID = CreateNewOrder_Other(gintUserID, "0")
	Session(SESSION_PUB_ORDER_ID) = gintOrderID
	'SetOrderDiscount_Other gintOrderID, Session(SESSION_REFERAL_ID), Session(SESSION_REFERAL_NAME), Session(SESSION_REFERAL_DISCOUNT)
	'SetOrderReferral(gintOrderID)
	gstrOrderStatus = "0"
end sub

function CreateNewOrder_Other(intShopperID, strStatus)
	' creates a new order
	dim dctSaveList, intOrderID
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "*@dtmCreated", "GETDATE()"
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "*vchCreatedByUser", "pub"
		dctSaveList.Add "vchUpdatedByUser", "pub"
		dctSaveList.Add "*vchCreatedByIP", gstrUserIP
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "chrType", "R"
		dctSaveList.Add "chrStatus", strStatus
		dctSaveList.Add "vchShopperIP", gstrUserIP
		dctSaveList.Add "#intShopperID", intShopperID
	intOrderID = SaveDataRecord("" & STR_TABLE_ORDER, Request, 0, dctSaveList)
	CreateNewOrder_Other = intOrderID
end function

sub UpdateOrderStatus(strStatus)
	' updates the status of the current order
	UpdateOrderStatus_Other gintOrderID, strStatus
	gstrOrderStatus = strStatus
end sub

sub UpdateOrderStatus_Other(intOrderID, strStatus)

	if intOrderID > 0 then
		dim dctSaveList
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "@dtmUpdated", "GETDATE()"
			dctSaveList.Add "vchUpdatedByUser", "pub"
			dctSaveList.Add "vchUpdatedByIP", gstrUserIP
			dctSaveList.Add "chrStatus", strStatus
		SaveDataRecord STR_TABLE_ORDER, Request, intOrderID, dctSaveList
	end if
end sub

sub CancelOrder
	' cancels the current order
	if gintOrderID > 0 then
		UpdateOrderStatus "D"
		gintOrderID = 0
		Session(SESSION_PUB_ORDER_ID) = 0
	end if
end sub

sub AbandonOrder
	' if the current order is associated with a user, the order is saved
	' otherwise, the order is cancelled
	if gintUserID = 0 then
		UpdateOrderStatus "B"
	else
		UpdateOrderStatus "0"
	end if
	gintOrderID = 0
	Session(SESSION_PUB_ORDER_ID) = 0
end sub

' Order History Routines
function GetSavedOrders()
	' returns a dictionary list of saved orders
	' format: {order_id, order_description)
	set GetSavedOrders = Server.CreateObject("Scripting.Dictionary")
	if gintUserID > 0 then
		dim strSQL, rsTemp, strLabel
		strSQL = "SELECT intID, chrStatus, dtmCreated, dtmUpdated FROM " & STR_TABLE_ORDER & " WHERE chrStatus IN ('0','1','2','3','4','5','6','7','8') AND intShopperID=" & gintUserID & " AND dtmUpdated >= DATEADD(dd,-14,GETDATE())"
		set rsTemp = gobjConn.execute(strSQL)
		while not rsTemp.eof
			strLabel = rsTemp("intID") & "|" & rsTemp("dtmCreated") & "|" & rsTemp("dtmUpdated")
			strLabel = replace(strLabel,":00 "," ")
			GetSavedOrders.Add CLng(rsTemp("intID")), strLabel
			rsTemp.MoveNext
		wend
		rsTemp.close
		set rsTemp = nothing
	end if
end function

function GetOrderHistory()
	' returns a dictionary list of saved orders
	' format: {order_id, order_description)
	set GetOrderHistory = Server.CreateObject("Scripting.Dictionary")
	if gintUserID > 0 then
		dim strSQL, rsTemp, strLabel
		strSQL = "SELECT intID, chrStatus, dtmCreated, dtmUpdated FROM " & STR_TABLE_ORDER & " WHERE chrStatus IN ('S','A','Z','X','C','H','E','R','V') AND intShopperID=" & gintUserID & " AND dtmUpdated >= DATEADD(yy,-1,GETDATE()) ORDER BY intID DESC"
		set rsTemp = gobjConn.execute(strSQL)
		while not rsTemp.eof
			strLabel = rsTemp("intID") & "|" & rsTemp("chrStatus") & "|" & rsTemp("dtmCreated") & "|" & rsTemp("dtmUpdated")
			GetOrderHistory.Add CLng(rsTemp("intID")), strLabel
			rsTemp.MoveNext
		wend
		rsTemp.close
		set rsTemp = nothing
	end if
end function

function GetUserBillingList(blnIncludeShipping)
	'-For Dummy users, gintuserid will be -1
	'response.write "<BR>gintuser: " & gintUserID
	set GetUserBillingList = GetUserBillingList_Other(gintUserID, blnIncludeShipping)
end function

sub SetOrderBillID(blnShipRecord, intBillID)
	' set the billing record for the current order
	if gintOrderID > 0 then
		SetOrderBillID_Other gintOrderID, blnShipRecord, intBillID
	end if
end sub

function GetOrderBillID(blnShipRecord)
	GetOrderBillID = GetOrderBillID_Other(gintOrderID, blnShipRecord)
end function

function GetOrderBillID_Other(intOrderID, blnShipRecord)
	' set the billing record for the current order	
	GetOrderBillID_Other = ""
	'response.write intorderid 
	if intOrderID > 0 then
		dim rsTemp, strSQL
		strSQL = "SELECT intBillShopperID, intShipShopperID FROM " & STR_TABLE_ORDER & " WHERE intID=" & intOrderID
'		response.write strSQL & "<br />"
		set rsTemp = gobjConn.execute(strSQL)
		if not rsTemp.eof then
			if blnShipRecord and IsValid(rsTemp("intShipShopperID")) then
				GetOrderBillID_Other = iif(blnShipRecord, rsTemp("intShipShopperID"), rsTemp("intBillShopperID"))
			else
				GetOrderBillID_Other = rsTemp("intBillShopperID")
			end if
		end if
		rsTemp.close
		set rsTemp = nothing
	end if
end function


Function ExistsBillingRecord(intORderID)
	ExistsBillingRecord = 0
	if intOrderID > 0 then
		dim rsTemp, strSQL
		strSQL = "SELECT * FROM " & STR_TABLE_ORDER & " WHERE intID=" & intOrderID
		'response.write strsql
		set rsTemp = gobjConn.execute(strSQL)
		If Not IsNull(rsTEmp("intBillShopperID")) then 'a billing record already exists
			strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & rsTemp("intBillShopperID")
			'response.write strsql
			set rsTemp = gobjConn.execute(strSQL)
			if not rsTemp.eof then
				ExistsBillingRecord = rsTemp("intID")
			end if		
		end if 
		'response.write "billrec: " & existsbillingrecord
		rsTemp.close
		set rsTemp = nothing
	end if
End Function

sub CopyPaymentFromBillRecord(intBillID)
	' copies the payment information from the billing record, if it exists
	' if the payment method is NULL in the billing record, no changes are made
	if gintOrderID > 0 then
		dim strSQL, rsTemp
		strSQL = "SELECT vchLastName + ', ' + vchFirstName AS vchName,"
		strSQL = strSQL & " chrPaymentMethod, vchCardType, vchCardNumber, vchCardExtended, chrCardExpMonth, chrCardExpYear, vchCheckBankName,"
		strSQL = strSQL & " vchCheckRtnNumber, vchCheckDLNumber, vchCheckDLState, chrCheckAcctType,"
		strSQL = strSQL & " vchCheckAcctNumber"
		strSQL = strSQL & " FROM " & STR_TABLE_SHOPPER
		strSQL = strSQL & " WHERE intID=" & intBillID
		set rsTemp = gobjConn.execute(strSQL)
		if not rsTemp.eof then
			dim dctSaveList
			set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "@dtmUpdated", "GETDATE()"
			dctSaveList.Add "vchUpdatedByUser", "pub"
			dctSaveList.Add "vchUpdatedByIP", gstrUserIP
			dctSaveList.Add "!chrPaymentMethod", ""
			if not IsNull(rsTemp("chrPaymentMethod")) then
				if rsTemp("chrPaymentMethod") = "OCC" then
					dctSaveList.Add "!vchPaymentCardName", "vchName"
					dctSaveList.Add "!vchPaymentCardType", "vchCardType"
					dctSaveList.Add "!vchPaymentCardNumber", "vchCardNumber"
					dctSaveList.Add "!vchPaymentCardExtended", "vchCardExtended"
					dctSaveList.Add "!chrPaymentCardExpMonth", "chrCardExpMonth"
					dctSaveList.Add "!chrPaymentCardExpYear", "chrCardExpYear"
					dctSaveList.Add "vchPaymentBankName", ""
					dctSaveList.Add "vchPaymentRtnNumber", ""
					dctSaveList.Add "vchPaymentDLNumber", ""
					dctSaveList.Add "vchPaymentDLState", ""
					dctSaveList.Add "chrPaymentAcctType", ""
					dctSaveList.Add "vchPaymentAcctNumber", ""
				elseif rsTemp("chrPaymentMethod") = "OEC" then
					dctSaveList.Add "vchPaymentCardName", ""
					dctSaveList.Add "vchPaymentCardType", ""
					dctSaveList.Add "vchPaymentCardNumber", ""
					dctSaveList.Add "vchPaymentCardExtended", ""
					dctSaveList.Add "chrPaymentCardExpMonth", ""
					dctSaveList.Add "chrPaymentCardExpYear", ""
					dctSaveList.Add "!vchPaymentBankName", "vchCheckBankName"
					dctSaveList.Add "!vchPaymentRtnNumber", "vchCheckRtnNumber"
					dctSaveList.Add "!vchPaymentDLNumber", "vchCheckDLNumber"
					dctSaveList.Add "!vchPaymentDLState", "vchCheckDLState"
					dctSaveList.Add "!chrPaymentAcctType", "chrCheckAcctType"
					dctSaveList.Add "!vchPaymentAcctNumber", "vchCheckAcctNumber"
				else
					dctSaveList.Add "vchPaymentCardName", ""
					dctSaveList.Add "vchPaymentCardType", ""
					dctSaveList.Add "vchPaymentCardNumber", ""
					dctSaveList.Add "vchPaymentCardExtended", ""
					dctSaveList.Add "chrPaymentCardExpMonth", ""
					dctSaveList.Add "chrPaymentCardExpYear", ""
					dctSaveList.Add "vchPaymentBankName", ""
					dctSaveList.Add "vchPaymentRtnNumber", ""
					dctSaveList.Add "vchPaymentDLNumber", ""
					dctSaveList.Add "vchPaymentDLState", ""
					dctSaveList.Add "chrPaymentAcctType", ""
					dctSaveList.Add "vchPaymentAcctNumber", ""
				end if
			end if
			SaveDataRecord STR_TABLE_ORDER, rsTemp, gintOrderID, dctSaveList
		end if
		rsTemp.close
		set rsTemp = nothing
		VoidPriorTransactions_Other gintOrderID
	end if
end sub

function GetOrderGrandTotal()
	GetOrderGrandTotal = ""
	if gintOrderID > 0 then
		dim rsTemp, strSQL
		strSQL = "SELECT mnyGrandTotal FROM " & STR_TABLE_ORDER & " WHERE intID=" & gintOrderID
		set rsTemp = gobjConn.execute(strSQL)
		if not rsTemp.eof then
			GetOrderGrandTotal = rsTemp("mnyGrandTotal")
		end if
		rsTemp.close
		set rsTemp = nothing
	end if
end function

function GetOrderPaymentMethod()
	GetOrderPaymentMethod = GetOrderPaymentMethod_Other(gintOrderID)
end function

function GetOrderPaymentMethod_Other(intOrderID)
	GetOrderPaymentMethod_Other = ""
	if intOrderID > 0 then
		dim rsTemp, strSQL
		strSQL = "SELECT chrPaymentMethod FROM " & STR_TABLE_ORDER & " WHERE intID=" & intOrderID
		set rsTemp = gobjConn.execute(strSQL)
		if not rsTemp.eof then
			GetOrderPaymentMethod_Other = rsTemp("chrPaymentMethod")
		end if
		rsTemp.close
		set rsTemp = nothing
	end if
end function

sub SetOrderPaymentMethod(strPaymentMethod)
	SetOrderPaymentMethod_Other gintOrderID, strPaymentMethod
end sub

sub SetOrderPaymentMethod_Other(intOrderID, strPaymentMethod)
	dim dctSaveList
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "vchUpdatedByUser", "pub"
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "chrPaymentMethod", strPaymentMethod
		dctSaveList.Add "vchPaymentCardType", ""
		dctSaveList.Add "vchPaymentCardNumber", ""
		dctSaveList.Add "vchPaymentCardExtended", ""
		dctSaveList.Add "chrPaymentCardExpMonth", ""
		dctSaveList.Add "chrPaymentCardExpYear", ""
		dctSaveList.Add "vchPaymentBankName", ""
		dctSaveList.Add "vchPaymentRtnNumber", ""
		dctSaveList.Add "vchPaymentDLNumber", ""
		dctSaveList.Add "vchPaymentDLState", ""
		dctSaveList.Add "chrPaymentAcctType", ""
		dctSaveList.Add "vchPaymentAcctNumber", ""
	SaveDataRecord STR_TABLE_ORDER, Request, intOrderID, dctSaveList
	VoidPriorTransactions_Other intOrderID
end sub

function GetOrderBillName()
	GetOrderBillName = GetOrderBillName_Other(gintOrderID)
end function

function GetOrderBillName_Other(intOrderID)
	GetOrderBillName_Other = ""
	if intOrderID > 0 then
		dim rsTemp, strSQL
		strSQL = "SELECT B.vchLastName + ', ' + B.vchFirstName AS vchName"
		strSQL = strSQL & " FROM " & STR_TABLE_ORDER & " AS O,"
		strSQL = strSQL & " " & STR_TABLE_SHOPPER & " AS B"
		strSQL = strSQL & " WHERE O.intID=" & intOrderID
		strSQL = strSQL & " AND O.intBillShopperID = B.intID"
		set rsTemp = gobjConn.execute(strSQL)
		if not rsTemp.eof then
			GetOrderBillName_Other = rsTemp("vchName")
		end if
		rsTemp.close
		set rsTemp = nothing
	end if
end function

sub GetOrderPaymentInfo_OCC(strCardName, strCardType, strCardNumber, strCardExtended, strCardExpMonth, strCardExpYear)
	GetOrderPaymentInfo_OCC_Other gintOrderID, strCardName, strCardType, strCardNumber, strCardExtended, strCardExpMonth, strCardExpYear
end sub

sub GetOrderPaymentInfo_OCC_Other(intOrderID, strCardName, strCardType, strCardNumber, strCardExtended, strCardExpMonth, strCardExpYear)
	strCardType = ""
	strCardNumber = ""
	strCardExtended = ""
	strCardExpMonth = ""
	strCardExpYear = ""
	if intOrderID > 0 then
		dim rsTemp, strSQL
		strSQL = "SELECT vchPaymentCardName, vchPaymentCardType, vchPaymentCardNumber, vchPaymentCardExtended, chrPaymentCardExpMonth, chrPaymentCardExpYear FROM " & STR_TABLE_ORDER & " WHERE intID=" & intOrderID
		set rsTemp = gobjConn.execute(strSQL)
		if not rsTemp.eof then
			strCardName = rsTemp("vchPaymentCardName")
			strCardType = rsTemp("vchPaymentCardType")
			strCardNumber = rsTemp("vchPaymentCardNumber")
			strCardExtended = rsTemp("vchPaymentCardExtended")
			strCardExpMonth = rsTemp("chrPaymentCardExpMonth")
			strCardExpYear = rsTemp("chrPaymentCardExpYear")
		end if
		rsTemp.close
		set rsTemp = nothing
	end if
end sub

sub SetOrderPaymentInfo_OCC(strCardName, strCardType, strCardNumber, strCardExtended, strCardExpMonth, strCardExpYear)
	SetOrderPaymentInfo_OCC_Other gintOrderID, strCardName, strCardType, strCardNumber, strCardExtended, strCardExpMonth, strCardExpYear
end sub

sub SetOrderPaymentInfo_OCC_Other(intOrderID, strCardName, strCardType, strCardNumber, strCardExtended, strCardExpMonth, strCardExpYear)
	dim dctSaveList
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "vchUpdatedByUser", "pub"
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "vchPaymentCardName", strCardName
		dctSaveList.Add "vchPaymentCardType", strCardType
		dctSaveList.Add "vchPaymentCardNumber", strCardNumber
		dctSaveList.Add "vchPaymentCardExtended", strCardExtended
		dctSaveList.Add "chrPaymentCardExpMonth", strCardExpMonth
		dctSaveList.Add "chrPaymentCardExpYear", strCardExpYear
	SaveDataRecord STR_TABLE_ORDER, Request, intOrderID, dctSaveList
	VoidPriorTransactions_Other intOrderID
end sub

function CheckIsBillID(blnShipRecord, intBillID)
	CheckIsBillID = CheckIsBillID_Other(gintUserID, blnShipRecord, intBillID)
end function

function CheckIsBillID_Other(intUserID, blnShipRecord, intBillID)
	' verify that the specified billing record is valid for this user
	if intUserID > 0 and IsNumeric(intBillID) then
		dim strSQL, rsTemp
		strSQL = "SELECT intID FROM " & STR_TABLE_SHOPPER & " WHERE chrStatus='A' AND intShopperID=" & intUserID & " AND intID=" & CLng(intBillID) & " AND (chrType='B'"
		if blnShipRecord then
			strSQL = strSQL & " OR chrType='S'"
		end if
		strSQL = strSQL & ")"
		set rsTemp = gobjConn.execute(strSQL)
		CheckIsBillID_Other = not rsTemp.eof
		rsTemp.close
		set rsTemp = nothing
	else
		CheckIsBillID_Other = false
	end if
end function

function CheckIsBillIDProtected(blnShipRecord, intID)
	dim strSQL, rsTemp
	strSQL = "SELECT intID FROM " & STR_TABLE_ORDER & " WHERE chrStatus NOT IN ('D','B','0','1','2','3','4','5','6','7','8')"
	strSQL = strSQL & " AND (intBillShopperID=" & intID & " OR intShipShopperID=" & intID & ")"
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		CheckIsBillIDProtected = true
	else
		CheckIsBillIDProtected = false
	end if
	rsTemp.close
	set rsTemp = nothing
end function

sub UpdateBillRecordSwitch(intOldID, intNewID)
	' deactivate the old billing record
	' change any "in progress" orders that were using the old ID to use the new ID
	dim strSQL
	strSQL = "UPDATE " & STR_TABLE_SHOPPER & " SET chrStatus='I' WHERE intID=" & intOldID
	gobjConn.execute(strSQL)
	strSQL = "UPDATE " & STR_TABLE_ORDER & " SET intBillShopperID=" & intNewID & " WHERE intBillShopperID=" & intOldID
	strSQL = strSQL & " AND chrStatus IN ('0','1','2','3','4','5','6','7','8','9')"
	gobjConn.execute(strSQL)
	strSQL = "UPDATE " & STR_TABLE_ORDER & " SET intShipShopperID=" & intNewID & " WHERE intShipShopperID=" & intOldID
	strSQL = strSQL & " AND chrStatus IN ('0','1','2','3','4','5','6','7','8','9')"
	gobjConn.execute(strSQL)
end sub

function CreateNewBillRecord(blnShipRecord, strLabel, strEmail, strFirstName, strLastName, strCompany, strAddress1, strAddress2, strCity, strState, strZip, strCountry, strDayPhone, strNightPhone, strFax, intTaxZone)
	CreateNewBillRecord = CreateNewBillRecord_Other(gintUserID, blnShipRecord, strLabel, strEmail, strFirstName, strLastName, strCompany, strAddress1, strAddress2, strCity, strState, strZip, strCountry, strDayPhone, strNightPhone, strFax, intTaxZone)
end function

function CreateNewBillRecord_Other(intUserID, blnShipRecord, strLabel, strEmail, strFirstName, strLastName, strCompany, strAddress1, strAddress2, strCity, strState, strZip, strCountry, strDayPhone, strNightPhone, strFax, intTaxZone)
	CreateNewBillRecord_Other = UpdateBillRecord_Other(intUserID, blnShipRecord, 0, strLabel, strEmail, strFirstName, strLastName, strCompany, strAddress1, strAddress2, strCity, strState, strZip, strCountry, strDayPhone, strNightPhone, strFax, intTaxZone)
end function

function UpdateBillRecord(blnShipRecord, intID, strLabel, strEmail, strFirstName, strLastName, strCompany, strAddress1, strAddress2, strCity, strState, strZip, strCountry, strDayPhone, strNightPhone, strFax, intTaxZone)
	UpdateBillRecord = 0	
	if gintUserID > 0 or GintuserID = -1 then
		
		UpdateBillRecord = UpdateBillRecord_Other(gintUserID, blnShipRecord, intID, strLabel, strEmail, strFirstName, strLastName, strCompany, strAddress1, strAddress2, strCity, strState, strZip, strCountry, strDayPhone, strNightPhone, strFax, intTaxZone)
	end if
end function

function UpdateBillRecord_Other(intUserID, blnShipRecord, intID, strLabel, strEmail, strFirstName, strLastName, strCompany, strAddress1, strAddress2, strCity, strState, strZip, strCountry, strDayPhone, strNightPhone, strFax, intTaxZone)
	' add a new billing record or update an existing one for the current user
	' return the billing record ID
	'response.write "<BR>intID: " & IntUserID
	if intUserID > 0 or IntUserID=-1 then		
		dim intOldID
		intOldID = 0
	'	response.write "id: " & intID
		if intID <> 0 then
			
			' first verify that this is a valid billing ID - if false, create new billing record
			' also verify that no existing orders (except for 'in progress') are using this billing ID
			' if false, create new billing record and then set status of old record to 'I'
			if not CheckIsBillID_Other(intUserID, blnShipRecord, intID) then
				intID = 0
			elseif CheckIsBillIDProtected(blnShipRecord, intID) then
				' existing orders are using this billing ID
				' create a new billing record and then set status of old record to 'I'
				intOldID = intID
				intID = 0
			end if
		end if
		
		dim dctSaveList
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "*@dtmCreated", "GETDATE()"
			dctSaveList.Add "@dtmUpdated", "GETDATE()"
			dctSaveList.Add "*vchCreatedByUser", "pub"
			dctSaveList.Add "vchUpdatedByUser", "pub"
			dctSaveList.Add "*vchCreatedByIP", gstrUserIP
			dctSaveList.Add "vchUpdatedByIP", gstrUserIP
			dctSaveList.Add "chrType", iif(blnShipRecord, "S", "B")
			dctSaveList.Add "*chrStatus", "A"
			dctSaveList.Add "#intShopperID", intUserID
			dctSaveList.Add "vchLabel", strLabel
			dctSaveList.Add "vchEmail", strEmail
			dctSaveList.Add "vchFirstName", strFirstName
			dctSaveList.Add "vchLastName", strLastName
			dctSaveList.Add "vchCompany", strCompany
			dctSaveList.Add "vchAddress1", strAddress1
			dctSaveList.Add "vchAddress2", strAddress2
			dctSaveList.Add "vchCity", strCity
			dctSaveList.Add "vchState", strState
			dctSaveList.Add "vchZip", strZip
			dctSaveList.Add "vchCountry", strCountry
			dctSaveList.Add "vchDayPhone", strDayPhone
			dctSaveList.Add "vchNightPhone", strNightPhone
			dctSaveList.Add "vchFax", strFax
'			if blnShipRecord then
				dctSaveList.Add "#intTaxZone", intTaxZone
'			end if
		UpdateBillRecord_Other = SaveDataRecord("" & STR_TABLE_SHOPPER, Request, intID, dctSaveList)
		'response.write "<BR>old id: " & intoldid
		if intOldID > 0 then
			UpdateBillRecordSwitch intOldID, UpdateBillRecord_Other
		end if
	else
		UpdateBillRecord_Other = 0
	end if
end function

function UpdateBillRecordPayment(strPaymentMethod, strCardType, strCardNumber, strCardExtended, strCardExpMonth, strCardExpYear)
	UpdateBillRecordPayment = UpdateBillRecordPayment_Other(gintOrderID, gintUserID, strPaymentMethod, strCardType, strCardNumber, strCardExtended, strCardExpMonth, strCardExpYear)
end function

function UpdateBillRecordPayment_Other(intOrderID, intUserID, strPaymentMethod, strCardType, strCardNumber, strCardExtended, strCardExpMonth, strCardExpYear)
	if intUserID > 0 then
		dim intOldID, intID
		intID = GetOrderBillID_Other(intOrderID, false)
		intOldID = 0
		if intID <> 0 then
			' first verify that this is a valid billing ID - if false, create new billing record
			' also verify that no existing orders (except for 'in progress') are using this billing ID
			' if false, create new billing record and then set status of old record to 'I'
			if not CheckIsBillID_Other(intUserID, false, intID) then
				UpdateBillRecordPayment_Other = 0
				exit function
			elseif CheckIsBillIDProtected(false, intID) then
				' existing orders are using this billing ID
				' create a new billing record and then set status of old record to 'I'
				intOldID = intID
				intID = 0
			end if
		end if
		dim dctSaveList
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "@dtmUpdated", "GETDATE()"
			dctSaveList.Add "vchUpdatedByUser", "pub"
			dctSaveList.Add "vchUpdatedByIP", gstrUserIP
			dctSaveList.Add "chrPaymentMethod", strPaymentMethod
			dctSaveList.Add "vchCardType", strCardType
			dctSaveList.Add "vchCardNumber", strCardNumber
			dctSaveList.Add "vchCardExtended", strCardExtended
			dctSaveList.Add "chrCardExpMonth", strCardExpMonth
			dctSaveList.Add "chrCardExpYear", strCardExpYear
		if intID = 0 then
			' we're going to need to copy all the fields from the old record
			dim strLabel, strEmail, strFirstName, strLastName, strCompany, strAddress1, strAddress2, strCity, strState
			dim strZip, strCountry, strDayPhone, strNightPhone, strFax, intTaxZone
			GetOrderAddress_Other intOrderID, false, intOldID, strLabel, strEmail, strFirstName, strLastName, strCompany, strAddress1, strAddress2, strCity, strState, strZip, strCountry, strDayPhone, strNightPhone, strFax, intTaxZone
			dctSaveList.Add "@dtmCreated", "GETDATE()"
			dctSaveList.Add "vchCreatedByUser", "pub"
			dctSaveList.Add "vchCreatedByIP", gstrUserIP
			dctSaveList.Add "chrType", "B"
			dctSaveList.Add "chrStatus", "A"
			dctSaveList.Add "#intShopperID", intUserID
			dctSaveList.Add "vchLabel", strLabel
			dctSaveList.Add "vchEmail", strEmail
			dctSaveList.Add "vchFirstName", strFirstName
			dctSaveList.Add "vchLastName", strLastName
			dctSaveList.Add "vchCompany", strCompany
			dctSaveList.Add "vchAddress1", strAddress1
			dctSaveList.Add "vchAddress2", strAddress2
			dctSaveList.Add "vchCity", strCity
			dctSaveList.Add "vchState", strState
			dctSaveList.Add "vchZip", strZip
			dctSaveList.Add "vchCountry", strCountry
			dctSaveList.Add "vchDayPhone", strDayPhone
			dctSaveList.Add "vchNightPhone", strNightPhone
			dctSaveList.Add "vchFax", strFax
		end if
		intID = SaveDataRecord("" & STR_TABLE_SHOPPER, Request, intID, dctSaveList)
		if intOldID > 0 then
			UpdateBillRecordSwitch intOldID, intID
		end if
		UpdateBillRecordPayment_Other = intID
	else
		UpdateBillRecordPayment_Other = 0
	end if
end function

sub GetOrderAddress(blnShipRecord, intID, strLabel, strEmail, strFirstName, strLastName, strCompany, strAddress1, strAddress2, strCity, strState, strZip, strCountry, strDayPhone, strNightPhone, strFax, intTaxZone)
	GetOrderAddress_Other gintOrderID, blnShipRecord, intID, strLabel, strEmail, strFirstName, strLastName, strCompany, strAddress1, strAddress2, strCity, strState, strZip, strCountry, strDayPhone, strNightPhone, strFax, intTaxZone
end sub

sub GetOrderAddress_Other(intOrderID, blnShipRecord, intID, strLabel, strEmail, strFirstName, strLastName, strCompany, strAddress1, strAddress2, strCity, strState, strZip, strCountry, strDayPhone, strNightPhone, strFax, intTaxZone)
'	response.write "intID: " & intID & "&nbsp;&nbsp; intOrderID: " & intOrderID & "<br />"
	strLabel = ""
	strEmail = ""
	strFirstName = ""
	strLastName = ""
	strCompany = ""
	strAddress1 = ""
	strAddress2 = ""
	strCity = ""
	strState = ""
	strZip = ""
	strCountry = ""
	strDayPhone = ""
	strNightPhone = ""
	strFax = ""
	intTaxZone = 0
	if IsNull(intID) or intID = "" then
		intID=0
	end if 	
	if intOrderID > 0 and intID > 0 then
		dim strSQL, rsTemp
		strSQL = "SELECT S.vchLabel, S.vchEmail, S.vchFirstName, S.vchLastName, S.vchCompany, S.vchAddress1, S.vchAddress2,"
		strSQL = strSQL & " S.vchCity, S.vchState, S.vchZip, S.vchCountry, S.vchDayPhone, S.vchNightPhone, S.vchFax, S.intTaxZone"
		strSQL = strSQL & " FROM " & STR_TABLE_SHOPPER & " AS S, " & STR_TABLE_ORDER & " AS O"
		strSQL = strSQL & " WHERE S.intID = " & intID
		strSQL = strSQL & " AND S.chrStatus <> 'D'"
		strSQL = strSQL & " AND (S.chrType = 'B'"
		if blnShipRecord then
			strSQL = strSQL & " OR S.chrType = 'S'"
		end if
		strSQL = strSQL & ") AND S.intShopperID = O.intShopperID"
		strSQL = strSQL & " AND O.intID=" & intOrderID
'		response.write strSQL & "<br />"
		set rsTemp = gobjConn.execute(strSQL)
		if not rsTemp.eof then
			strLabel = rsTemp("vchLabel") & ""
			strEmail = rsTemp("vchEmail") & ""
			strFirstName = rsTemp("vchFirstName") & ""
			strLastName = rsTemp("vchLastName") & ""
			strCompany = rsTemp("vchCompany") & ""
			strAddress1 = rsTemp("vchAddress1") & ""
			strAddress2 = rsTemp("vchAddress2") & ""
			strCity = rsTemp("vchCity") & ""
			strState = rsTemp("vchState") & ""
			strZip = rsTemp("vchZip") & ""
			strCountry = rsTemp("vchCountry") & ""
			strDayPhone = rsTemp("vchDayPhone") & ""
			strNightPhone = rsTemp("vchNightPhone") & ""
			strFax = rsTemp("vchFax") & ""
			intTaxZone = rsTemp("intTaxZone")
		end if
		rsTemp.close
		set rsTemp = nothing
	end if
end sub

function LookupHint(strEmail, strHint)
	' returns the hint for the specified username
	' returns true if a hint was found, otherwise false
	dim strSQL, rsTemp
	strSQL = "SELECT isNull(vchHint,'none') as vchHint FROM " & STR_TABLE_SHOPPER & " WHERE chrStatus='A' AND vchEmail='" & SQLEncode(strEmail) & "'"
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		strHint = rsTemp("vchHint")
		LookupHint = true
	else
		LookupHint = false
	end if
	rsTemp.close
	set rsTemp = nothing
end function

function GetOrderRecord_Direct()
	if gintOrderID > 0 then
		dim strSQL
		strSQL = "SELECT * FROM " & STR_TABLE_ORDER & " WHERE chrStatus<>'D' AND intID=" & gintOrderID
		set GetOrderRecord_Direct = gobjConn.execute(strSQL)
	else
		set GetOrderRecord_Direct = nothing
	end if
end function

	
function GetOrderLineItems_WithPromoCode(PromoCode)
	if gintOrderID > 0 then
		dim strSQL,TempRS
				
		'strSQL = "UPDATE " & STR_TABLE_ORDER & " SET 
		
		strSQL = "SELECT L.intQuantity,mnyAmount,mnyUnitPrice AS mnyUnit,vchPartNumber,vchItemName,ISNull(mnyUnitPrice-(mnyAmount*mnyUnitPrice),mnyUnitPrice) AS mnyUnitPrice,(ISNull(mnyAmount,mnyUnitPrice)*intQuantity) AS mnyPrice FROM " & STR_TABLE_LINEITEM & " L LEFT JOIN " & STR_TABLE_PROMO & " P on L.intInvId=P.intProductID  AND chrPromoCode='" & PromoCode & "' WHERE intOrderID=" & gintOrderID & " AND chrStatus<>'D' ORDER BY L.intID"
		'strSQL = "SELECT *,(mnyUnitPrice * intQuantity) AS mnyPrice FROM " & STR_TABLE_LINEITEM & " WHERE intOrderID=" & gintOrderID & " AND chrStatus<>'D' ORDER BY intID"
		set GetOrderLineItems_WithPromoCode = gobjConn.execute(strSQL)
	else
		set GetOrderLineItems_WithPromoCode = nothing
	end if
end function


function GetOrderLineItems_Direct()
	if gintOrderID > 0 then
		dim strSQL
		strSQL = "SELECT *,(mnyUnitPrice * intQuantity) AS mnyPrice FROM " & STR_TABLE_LINEITEM & " WHERE intOrderID=" & gintOrderID & " AND chrStatus<>'D' ORDER BY intID"
		set GetOrderLineItems_Direct = gobjConn.execute(strSQL)
	else
		set GetOrderLineItems_Direct = nothing
	end if
end function

function GetOrderTrans_Direct()
	if gintOrderID > 0 then
		dim strSQL
		strSQL = "SELECT * FROM " & STR_TABLE_TRANS & " WHERE intOrderID=" & gintOrderID & " AND (chrStatus='A' OR chrStatus='V') ORDER BY intID"
		set GetOrderTrans_Direct = gobjConn.execute(strSQL)
	else
		set GetOrderTrans_Direct = nothing
	end if
end function

function GetOrderShopper_Direct(blnShipRecord)
	if gintOrderID > 0 then
		dim strSQL, intID
		intID = GetOrderBillID(blnShipRecord)
		if not IsNull(intID) then
			strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & intID & " AND chrStatus<>'D'"
			set GetOrderShopper_Direct = gobjConn.execute(strSQL)
		end if
	else
		set GetOrderShopper_Direct = nothing
	end if
end function

function GetOrderRecord_Other(intOrderID)
	dim strSQL
	strSQL = "SELECT * FROM " & STR_TABLE_ORDER & " WHERE chrStatus<>'D' AND intID=" & intOrderID
	set GetOrderRecord_Other = gobjConn.execute(strSQL)
end function

function GetOrderLineItems()
	set GetOrderLineItems = GetOrderLineItems_Other(gintOrderID)
end function

function GetOrderLineItems_Other(intOrderID)
	dim strSQL
		'" & STR_TABLE_LINEITEM & " L LEFT JOIN " & STR_TABLE_PROMO & " P on L.intInvId=P.intProductID  AND chrPromoCode='" & PromoCode & "'
	strSQL = "SELECT i.intparentid, intInvID,L.chrStatus,L.vchPartNumber,L.intOrderID,L.intID,L.vchItemName,L.dtmUpdated,L.chrType,L.intForceShipMethod,L.chrForceSoloItem,L.mnyShipPrice,L.vchShippingNumber,L.intQuantity,I.vchImageURL, I.vchSpecifications,I.vchValidStates,I.vchBundleQuantity,I.vchExpireDate, Pr.vchItemName as vchProgram,ISNull(L.mnyUnitPrice-(P.mnyAmount*L.mnyUnitPrice),L.mnyUnitPrice) AS mnyUnitPrice,(ISNull(L.mnyUnitPrice-(P.mnyAmount*L.mnyUnitPrice),L.mnyUnitPrice) * L.intQuantity) AS mnyPrice, I.txtDescription  FROM " & STR_TABLE_LINEITEM & " L JOIN " & STR_TABLE_ORDER & " O on O.intID=intOrderID JOIN " & STR_TABLE_INVENTORY & " I on L.intInvId=I.intID LEFT JOIN " & STR_TABLE_INVENTORY & " Pr ON Pr.intID=I.intProgram LEFT JOIN " & STR_TABLE_PROMO & " P on L.intInvId=P.intProductID  AND P.chrPromoCode=O.chrPromoCode WHERE intOrderID=" & intOrderID & " AND L.chrStatus<>'D' ORDER BY L.intID"
	'Response.Write strSQL
    set GetOrderLineItems_Other = gobjConn.execute(strSQL)
end function



function GetOrderLineItems_OtherLt(intOrderID)
	dim strSQL
		'" & STR_TABLE_LINEITEM & " L LEFT JOIN " & STR_TABLE_PROMO & " P on L.intInvId=P.intProductID  AND chrPromoCode='" & PromoCode & "'
	strSQL = "SELECT intInvID,L.chrStatus,L.vchPartNumber,L.intOrderID,L.intID,L.vchItemName,L.dtmUpdated,L.chrType,L.intForceShipMethod,L.chrForceSoloItem,L.mnyShipPrice,L.vchShippingNumber,L.intQuantity,I.vchImageURL, I.vchSpecifications,I.vchValidStates,I.vchBundleQuantity,I.vchExpireDate, Pr.vchItemName as vchProgram,ISNull(L.mnyUnitPrice-(P.mnyAmount*L.mnyUnitPrice),L.mnyUnitPrice) AS mnyUnitPrice,(ISNull(L.mnyUnitPrice-(P.mnyAmount*L.mnyUnitPrice),L.mnyUnitPrice) * L.intQuantity) AS mnyPrice, I.txtDescription  FROM " & STR_TABLE_LINEITEM_LONG_TERM & " L JOIN " & STR_TABLE_ORDER_LONG_TERM & " O on O.intID=intOrderID JOIN " & STR_TABLE_INVENTORY_LONG_TERM & " I on L.intInvId=I.intID LEFT JOIN " & STR_TABLE_INVENTORY_LONG_TERM & " Pr ON Pr.intID=I.intProgram LEFT JOIN " & STR_TABLE_PROMO & " P on L.intInvId=P.intProductID  AND P.chrPromoCode=O.chrPromoCode WHERE intOrderID=" & intOrderID & " AND L.chrStatus<>'D' ORDER BY L.intID"
	'Response.Write strSQL
    set GetOrderLineItems_OtherLt = gobjConn.execute(strSQL)
end function

function GetOrderTrans_Other(intOrderID)
	dim strSQL
	strSQL = "SELECT * FROM " & STR_TABLE_TRANS & " WHERE intOrderID=" & intOrderID & " AND (chrStatus='A' OR chrStatus='V') ORDER BY intID"
	set GetOrderTrans_Other = gobjConn.execute(strSQL)
end function

function GetOrderShopper_Other(intID)
	dim strSQL
	if not IsNull(intID) then
		strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & intID & " AND chrStatus<>'D'"
		set GetOrderShopper_Other = gobjConn.execute(strSQL)
	else
		set GetOrderShopper_Other = nothing
	end if
end function

sub GetOrderAuthCode(strAuthCode, strRefCode, strOrderCode)
	strAuthCode = ""
	strRefCode = ""
	strOrderCode = ""
	if gintOrderID > 0 then
		dim strSQL, rsTemp
		strSQL = "SELECT vchAuthCode, vchRefCode, vchOrderCode FROM " & STR_TABLE_TRANS & " WHERE intOrderID=" & gintOrderID & " AND (chrStatus='A' OR chrStatus='V') ORDER BY intID DESC"
		set rsTemp = gobjConn.execute(strSQL)
		if not rsTemp.eof then
			strAuthCode = rsTemp("vchAuthCode") & ""
			strRefCode = rsTemp("vchRefCode") & ""
			strOrderCode = rsTemp("vchOrderCode") & ""
		end if
		rsTemp.close
		set rsTemp = nothing
	end if
end sub

function GetUserBillingList_Other(intUserID, blnIncludeShipping)
	' returns a dictionary list of billing records
	' format: (record_id, description)
	dim strLabel
	set GetUserBillingList_Other = Server.CreateObject("Scripting.Dictionary")
	'response.write "<BR>User: " & intuserID
	'response.write "<BR>guser: " & gintuserid
	if intUserID > 0 and Session("DummyUser")<>True then
		dim strSQL, rsTemp, strOldType
		strSQL = "SELECT intID, chrType, vchLabel, chrPaymentMethod, vchCardType FROM " & STR_TABLE_SHOPPER & " WHERE intShopperID=" & intUserID & " AND (chrType='B'"
		if blnIncludeShipping then
			strSQL = strSQL & " OR chrType='S'"
		end if
		strSQL = strSQL & ") AND chrStatus='A' ORDER BY chrType DESC, vchLabel"
		set rsTemp = gobjConn.execute(strSQL)
		while not rsTemp.eof
			if strOldType <> rsTemp("chrType") then
				strOldType = rsTemp("chrType")
				if strOldType = "B" then
					GetUserBillingList_Other.Add " ", "** Billing Addresses **"
				else
					GetUserBillingList_Other.Add "  ", "** Shipping Addresses **"
				end if
			end if
			strLabel = rsTemp("vchLabel") & ""
			if not blnIncludeShipping then
				' add payment types
				if not isNull(rsTemp("chrPaymentMethod")) then
					if rsTemp("chrPaymentMethod") = "OCC" then
						strLabel = strLabel & " (" & rsTemp("vchCardType") & ")"
					end if
'					strLabel = strLabel & " (" & GetArrayValue(rsTemp("chrPaymentMethod"), dctPaymentMethod) & ")"
				end if
			end if
			GetUserBillingList_Other.Add CLng(rsTemp("intID")), strLabel
			rsTemp.MoveNext
		wend
		rsTemp.close
		set rsTemp = nothing
	end if
end function

function VoidPriorTransactions_Other(intOrderID)
	' this function called whenever the payment info in an order is changed
	' any existing 'approved' transaction records should be changed to 'voided'
	' (this is to prevent depositing to a different card than was authorized, or credit a different
	' card than was deposited, etc.)
	dim strSQL
	strSQL = "UPDATE " & STR_TABLE_TRANS & " SET chrStatus='I' WHERE intOrderID=" & intOrderID & " AND chrStatus='A'"
	gobjConn.execute(strSQL)
end function

sub UpdateOrderShippingNumber_Other(intOrderID, strShippingNumber)
	if intOrderID > 0 then
		dim dctSaveList
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "@dtmUpdated", "GETDATE()"
			dctSaveList.Add "@dtmShipped", "GETDATE()"
			dctSaveList.Add "vchUpdatedByUser", "pub"
			dctSaveList.Add "vchUpdatedByIP", gstrUserIP
			dctSaveList.Add "vchShippingNumber", strShippingNumber
		SaveDataRecord STR_TABLE_ORDER, Request, intOrderID, dctSaveList
	end if
end sub

'added by dde, 11/02/2011
sub UpdateOrderShippedDate(intOrderID)
	if intOrderID > 0 then
		dim dctSaveList
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "@dtmUpdated", "GETDATE()"
			dctSaveList.Add "@dtmShipped", "GETDATE()"
			dctSaveList.Add "vchUpdatedByUser", "pub"
			dctSaveList.Add "vchUpdatedByIP", gstrUserIP
			'dctSaveList.Add "vchShippingNumber", strShippingNumber
		SaveDataRecord STR_TABLE_ORDER, Request, intOrderID, dctSaveList
	end if
end sub

'======================================================
'UpdateItemQuantity
'	Desc:	Decrements Quantity controlled items stock count
'	Inputs: intOrderId	-ID of the order to perform update with
'======================================================
sub UpdateItemQuantity(intOrderID)
	dim strSQL, rsTemp, intNewQty
	strSQL = 		  "SELECT I.intID, I.intStock, L.intQuantity "
	strSQL = strSQL & "FROM " & STR_TABLE_INVENTORY & " AS I, " & STR_TABLE_LINEITEM & " AS L "
	strSQL = strSQL & "WHERE ((L.intOrderID = " & intOrderID & ") AND (L.chrStatus = 'A')) "
	strSQL = strSQL & "AND ((I.intID = L.intInvId) AND (I.chrICFlag = 'Y'))"
'	response.write strSQL
	set rsTemp = gobjConn.execute(strSQL)
	
	while not rsTemp.eof and response.IsClientConnected
		if IsNumeric(rsTemp("intStock")) and IsNumeric(rsTemp("intQuantity")) then			 
			intNewQty = CLng(rsTemp("intStock")) - CLng(rsTemp("intQuantity"))
			intNewQty = iif(intNewQty > 0, intNewQty, 0)
		end if
			strSQL = 		  "UPDATE " & STR_TABLE_INVENTORY & " SET intStock = " & intNewQty & " WHERE intID = " & rsTemp("intID")
'			response.write strSQL
			gobjConn.execute(strSQL)
		rsTemp.MoveNext
	wend
end sub
'======================================================

function ValidateOrder(dctErrors)
	ValidateOrder = ValidateOrder_Other(gintOrderID, dctErrors)
end function

function ValidateOrder_Other(intOrderID, dctErrors)
	dim strSQL, rsTemp, intShopperID
if false then
'-- old method...
	strSQL = "SELECT A.intID AS A_intID, B.intShopperID AS B_intShopperID, S.intShopperID AS S_intShopperID,"
	strSQL = strSQL & " O.intShopperID AS O_intShopperID,"
	strSQL = strSQL & " O.chrPaymentMethod, O.vchPaymentCardType, O.vchPaymentCardName, O.vchPaymentCardNumber, O.vchPaymentCardExtended, O.chrPaymentCardExpMonth, O.chrPaymentCardExpYear,"
	strSQL = strSQL & " O.vchPaymentBankName, O.vchPaymentRtnNumber, O.vchPaymentCheckNumber, O.vchPaymentDLNumber, O.vchPaymentDLState, O.chrPaymentAcctType, O.vchPaymentAcctNumber,"
	strSQL = strSQL & " O.intShipOption, ISNULL(L.intForceShipMethod, 0) AS intForceShipMethod,"
	strSQL = strSQL & " S.vchCountry AS S_vchCountry, S.vchState AS S_vchState"		', O.txtGiftMessage
	strSQL = strSQL & " FROM " & STR_TABLE_ORDER & " AS O, " & STR_TABLE_SHOPPER & " AS A, " & STR_TABLE_SHOPPER & " AS B, " & STR_TABLE_SHOPPER & " AS S, " & STR_TABLE_LINEITEM & " AS L"
	strSQL = strSQL & " WHERE O.intID = " & intOrderID
	strSQL = strSQL & " AND O.intShopperID *= A.intID AND A.chrStatus<>'D' AND A.chrType='A'"
	strSQL = strSQL & " AND O.intBillShopperID *= B.intID AND B.chrStatus<>'D' AND B.chrType IN ('B','S')"
	strSQL = strSQL & " AND O.intShipShopperID *= S.intID AND S.chrStatus<>'D' AND S.chrType IN ('B','S')"
	strSQL = strSQL & " AND O.intID *= L.intOrderID AND ISNULL(L.intForceShipMethod, 0) > 0"
else
'-- new method...
	strSQL = "SELECT A.intID AS A_intID, B.intShopperID AS B_intShopperID, S.intShopperID AS S_intShopperID,"
	strSQL = strSQL & " O.intShopperID AS O_intShopperID,"
	strSQL = strSQL & " O.chrPaymentMethod, O.vchPaymentCardType, O.vchPaymentCardName, O.vchPaymentCardNumber, O.vchPaymentCardExtended, O.chrPaymentCardExpMonth, O.chrPaymentCardExpYear,"
	strSQL = strSQL & " O.vchPaymentBankName, O.vchPaymentRtnNumber, O.vchPaymentCheckNumber, O.vchPaymentDLNumber, O.vchPaymentDLState, O.chrPaymentAcctType, O.vchPaymentAcctNumber,"
	strSQL = strSQL & " O.intShipOption, ISNULL(L.intForceShipMethod, 0) AS intForceShipMethod,"
	strSQL = strSQL & " S.vchCountry AS S_vchCountry, S.vchState AS S_vchState"
	strSQL = strSQL & " FROM " & STR_TABLE_ORDER & " AS O LEFT OUTER JOIN"
	strSQL = strSQL & " " & STR_TABLE_SHOPPER & " A ON O.intShopperID = A.intID AND A.chrStatus<>'D' AND A.chrType='A' LEFT OUTER JOIN"
	strSQL = strSQL & " " & STR_TABLE_SHOPPER & " B ON O.intBillShopperID = B.intID AND B.chrStatus<>'D' AND B.chrType IN ('B','S') LEFT OUTER JOIN"
	strSQL = strSQL & " " & STR_TABLE_SHOPPER & " S ON O.intShipShopperID = S.intID AND S.chrStatus<>'D' AND S.chrType IN ('B','S') LEFT OUTER JOIN"
	strSQL = strSQL & " " & STR_TABLE_LINEITEM & " L ON O.intID = L.intOrderID AND ISNULL(L.intForceShipMethod, 0) > 0"
	strSQL = strSQL & " WHERE O.intID = " & intOrderID
end if
	'response.write strsql
	set rsTemp = gobjConn.execute(strSQL)
	if rsTemp.eof then
		dctErrors.Add "order", "Order could not be located."
	else
		if IsNull(rsTemp("A_intID")) and rsTemp("O_intShopperID") <> -1 then
			dctErrors.Add "account", "Missing account"
			intShopperID = 0
		else
			intShopperID = rsTemp("O_intShopperID")
		end if
		if IsNull(rsTemp("B_intShopperID")) or intShopperID <> rsTemp("B_intShopperID") then
			dctErrors.Add "billing", "Missing billing"
		else
			
		end if
		if IsNull(rsTemp("S_intShopperID")) or intShopperID <> rsTemp("S_intShopperID") then
			dctErrors.Add "shipping", "Missing shipping"
		else
			
		end if
		if IsNull(rsTemp("chrPaymentMethod")) then
			dctErrors.Add "payment", "Missing payment"
		else
			if right(rsTemp("chrPaymentMethod"),2) = "CC" then
				' validate credit card information
				if rsTemp("chrPaymentMethod") = "OCC" or not dctErrors.Exists("paymentok") then
					if IsNull(rsTemp("vchPaymentCardType")) or IsNull(rsTemp("vchPaymentCardName")) or IsNull(rsTemp("vchPaymentCardNumber")) or IsNull(rsTemp("chrPaymentCardExpMonth")) or IsNull(rsTemp("chrPaymentCardExpYear")) then
						dctErrors.Add "payment", "Incomplete payment"
					end if
				end if
			elseif rsTemp("chrPaymentMethod") = "OEC" then
				' validate electronic check information
				
			end if
		end if
'		if IsNull(rsTemp("txtGiftMessage")) then
'			dctErrors.Add "giftmsg", "Missing Gift Message"
'		end if
		dim intShipOption, intForceShipMethod
		intShipOption = rsTemp("intShipOption")
		intForceShipMethod = rsTemp("intForceShipMethod")
		if IsNull(intShipOption) then
			intShipOption = 0
		elseif intShipOption <> intForceShipMethod then
			' validate if shipping option correct for this shipping address
			dim strShipCountry, strShipState
			strShipCountry = rsTemp("S_vchCountry")
			strShipState = rsTemp("S_vchState")
			if intShipOption < 6 or intShipOption > 7 then
				if strShipCountry <> "US" and strShipCountry <> "CA" then
					if intShipOption <> 1 then
						intShipOption = 0
					end if
				elseif (strShipCountry = "US" and (strShipState = "HI" OR strShipState = "AK")) or (strShipCountry = "VI") then
					if intShipOption <> 2 then
						intShipOption = 0
					end if
				else
					if intShipOption = 5 then
						intShipOption = 0
					end if
				end if
			end if
		end if
		if intShipOption = 0 then
			dctErrors.Add "shipoption", "Invalid shipping option"
		end if
	end if
	rsTemp.close
	set rsTemp = nothing
	strSQL = "SELECT COUNT(*) AS intCount FROM " & STR_TABLE_LINEITEM & " WHERE intOrderID=" & intOrderID & " AND chrStatus<>'D'"
	set rsTemp = gobjConn.execute(strSQL)
	if rsTemp("intCount") = 0 then
		dctErrors.Add "items", "Missing line items"
	end if
	rsTemp.close
	set rsTemp = nothing
	ValidateOrder_Other = (dctErrors.count = 0)
end function

function Merchant_GetOrderPaymentMethod(intOrderID)
	Merchant_GetOrderPaymentMethod = ""
	dim rsTemp, strSQL
	strSQL = "SELECT chrPaymentMethod FROM " & STR_TABLE_ORDER & " WHERE intID=" & intOrderID
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		Merchant_GetOrderPaymentMethod = rsTemp("chrPaymentMethod")
	end if
	rsTemp.close
	set rsTemp = nothing
end function

sub Merchant_SetOrderPaymentMethod(intOrderID, strPaymentMethod)
	dim dctSaveList
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "vchUpdatedByUser", "pub"
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "chrPaymentMethod", strPaymentMethod
		dctSaveList.Add "vchPaymentCardType", ""
		dctSaveList.Add "vchPaymentCardNumber", ""
		dctSaveList.Add "vchPaymentCardExtended", ""
		dctSaveList.Add "chrPaymentCardExpMonth", ""
		dctSaveList.Add "chrPaymentCardExpYear", ""
		dctSaveList.Add "vchPaymentBankName", ""
		dctSaveList.Add "vchPaymentRtnNumber", ""
		dctSaveList.Add "vchPaymentDLNumber", ""
		dctSaveList.Add "vchPaymentDLState", ""
		dctSaveList.Add "chrPaymentAcctType", ""
		dctSaveList.Add "vchPaymentAcctNumber", ""
	SaveDataRecord STR_TABLE_ORDER, Request, intOrderID, dctSaveList
	VoidPriorTransactions_Other intOrderID
end sub

function Merchant_GetOrderBillName(intOrderID)
	Merchant_GetOrderBillName = ""
	dim rsTemp, strSQL
	strSQL = "SELECT B.vchLastName + ', ' + B.vchFirstName AS vchName"
	strSQL = strSQL & " FROM " & STR_TABLE_ORDER & " AS O,"
	strSQL = strSQL & " " & STR_TABLE_SHOPPER & " AS B"
	strSQL = strSQL & " WHERE O.intID=" & intOrderID
	strSQL = strSQL & " AND O.intBillShopperID = B.intID"
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		Merchant_GetOrderBillName = rsTemp("vchName")
	end if
	rsTemp.close
	set rsTemp = nothing
end function

sub Merchant_GetOrderPaymentInfo_OCC(intOrderID, strCardName, strCardType, strCardNumber, strCardExtended, strCardExpMonth, strCardExpYear)
	strCardType = ""
	strCardNumber = ""
	strCardExtended = ""
	strCardExpMonth = ""
	strCardExpYear = ""
	dim rsTemp, strSQL
	strSQL = "SELECT vchPaymentCardName, vchPaymentCardType, vchPaymentCardNumber, vchPaymentCardExtended, chrPaymentCardExpMonth, chrPaymentCardExpYear FROM " & STR_TABLE_ORDER & " WHERE intID=" & intOrderID
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		strCardName = rsTemp("vchPaymentCardName")
		strCardType = rsTemp("vchPaymentCardType")
		strCardNumber = rsTemp("vchPaymentCardNumber")
		strCardExtended = rsTemp("vchPaymentCardExtended")
		strCardExpMonth = rsTemp("chrPaymentCardExpMonth")
		strCardExpYear = rsTemp("chrPaymentCardExpYear")
	end if
	rsTemp.close
	set rsTemp = nothing
end sub

sub Merchant_SetOrderPaymentInfo_OCC(intOrderID, strCardName, strCardType, strCardNumber, strCardExtended, strCardExpMonth, strCardExpYear)
	dim dctSaveList
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "vchUpdatedByUser", "pub"
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "vchPaymentCardName", strCardName
		dctSaveList.Add "vchPaymentCardType", strCardType
		dctSaveList.Add "vchPaymentCardNumber", strCardNumber
		dctSaveList.Add "vchPaymentCardExtended", strCardExtended
		dctSaveList.Add "chrPaymentCardExpMonth", strCardExpMonth
		dctSaveList.Add "chrPaymentCardExpYear", strCardExpYear
	SaveDataRecord STR_TABLE_ORDER, Request, intOrderID, dctSaveList
	VoidPriorTransactions_Other intOrderID
end sub

sub Merchant_GetOrderPaymentPostInfo(intOrderID, mnyGrandTotal, mnyAuthAmount, mnyDepositAmount, mnyCreditAmount)
	mnyGrandTotal = 0
	mnyAuthAmount = 0
	mnyDepositAmount = 0
	mnyCreditAmount = 0
	dim strSQL, rsTemp
	strSQL = "SELECT mnyGrandTotal, "
	strSQL = strSQL & "(SELECT ISNULL(SUM(mnyTransAmount),0) FROM " & STR_TABLE_TRANS & " WHERE chrStatus='A' AND chrTransType IN ('AV','AS','ES','EV','DD') AND intOrderID=" & intOrderID & ") AS mnyAuthTotal,"
	strSQL = strSQL & "(SELECT ISNULL(SUM(mnyTransAmount),0) FROM " & STR_TABLE_TRANS & " WHERE chrStatus='A' AND chrTransType IN ('ES','EV','DS','DD') AND intOrderID=" & intOrderID & ") AS mnyDepositTotal,"
	strSQL = strSQL & "(SELECT ISNULL(SUM(mnyTransAmount),0) FROM " & STR_TABLE_TRANS & " WHERE chrStatus='A' AND chrTransType IN ('CR','DC') AND intOrderID=" & intOrderID & ") AS mnyCreditTotal"
	strSQL = strSQL & " FROM " & STR_TABLE_ORDER & " WHERE intID=" & intOrderID & " AND chrStatus<>'D'"
	set rstemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		mnyGrandTotal = rsTemp("mnyGrandTotal")
		mnyAuthAmount = rsTemp("mnyAuthAmount")
		mnyDepositAmount = rsTemp("mnyDepositAmount")
		mnyCreditAmount = rsTemp("mnyCreditAmount")
	end if
	rsTemp.close
	set rsTemp = nothing
end sub

function Merchant_GetUserAccountList(byVal strSearch)
	set Merchant_GetUserAccountList = Server.CreateObject("Scripting.Dictionary")
	dim strSQL, rsTemp
	strSearch = SQLEncode(strSearch)
	strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE chrStatus='A' AND chrType='A' AND (vchFirstName LIKE '%" & strSearch & "%' OR vchLastName LIKE '%" & strSearch & "%' OR vchCompany LIKE '%" & strSearch & "%') ORDER BY vchCompany, vchLastName, vchFirstName"
	set rsTemp = gobjConn.execute(strSQL)
	while not rsTemp.eof
		Merchant_GetUserAccountList.Add rsTemp("intID") & "", rsTemp("vchLastName") & ", " & rsTemp("vchFirstName") & " " & rsTemp("vchCompany")
		rsTemp.MoveNext
	wend
	rsTemp.close
	set rsTemp = nothing
end function

sub Merchant_SetOrderShopper(intOrderID, intShopperID)
	Dim strSQL, intBillShopperID, intShipShopperID, rsTemp
    
    strSQL = "SELECT top 1 intID, chrType, vchLabel, chrPaymentMethod, vchCardType FROM " & STR_TABLE_SHOPPER & " WHERE intShopperID=" & intShopperID & " AND (chrType='B') AND chrStatus='A' ORDER BY chrType DESC, vchLabel"
	set rsTemp = gobjConn.execute(strSQL)
    
    if not rsTemp.Eof Then
        intBillShopperID = rsTemp(0)&""
    End if

    dim dctSaveList
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "vchUpdatedByUser", "pub"
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "#intShopperID", intShopperID
		dctSaveList.Add "#intBillShopperID", ""
		dctSaveList.Add "#intShipShopperID", ""
		dctSaveList.Add "chrPaymentMethod", ""
	SaveDataRecord STR_TABLE_ORDER, Request, intOrderID, dctSaveList


    SetOrderBillID_Other intOrderID, false, intBillShopperID
    SetOrderBillID_Other intOrderID, true, intBillShopperID

end sub

function Merchant_GetOrderShopper(intOrderID)
	Merchant_GetOrderShopper = ""
	dim rsTemp, strSQL
	strSQL = "SELECT intShopperID FROM " & STR_TABLE_ORDER & " WHERE intID=" & intOrderID
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		Merchant_GetOrderShopper = rsTemp("intShopperID")
	end if
	rsTemp.close
	set rsTemp = nothing
end function


sub SetOrderBillID_Other(intOrderID, blnShipRecord, intBillID)
	' set the billing record for the current order
	if intOrderID > 0 then
		dim dctSaveList
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "@dtmUpdated", "GETDATE()"
			dctSaveList.Add "vchUpdatedByUser", "pub"
			dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		if blnShipRecord then
			dim strSQL, rsTemp, intTaxZone
			strSQL = "SELECT intTaxZone FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & intBillID
			set rsTemp = gobjConn.execute(strSQL)
			if not rsTemp.eof then
				intTaxZone = rsTemp("intTaxZone")
			else
				intTaxZone = 0
			end if
			rsTemp.close
			set rsTemp = nothing
			dctSaveList.Add "#intShipShopperID", intBillID
			dctSaveList.Add "#intTaxZone", intTaxZone
		else
			dctSaveList.Add "#intBillShopperID", intBillID
		end if
		SaveDataRecord STR_TABLE_ORDER, Request, intOrderID, dctSaveList
	end if
end sub

sub CopyPaymentFromBillRecord_Other(intOrderID, intBillID)
	' copies the payment information from the billing record, if it exists
	' if the payment method is NULL in the billing record, no changes are made
	if intOrderID > 0 then
		dim strSQL, rsTemp
		strSQL = "SELECT vchLastName + ', ' + vchFirstName AS vchName,"
		strSQL = strSQL & " chrPaymentMethod, vchCardType, vchCardNumber, vchCardExtended, chrCardExpMonth, chrCardExpYear, vchCheckBankName,"
		strSQL = strSQL & " vchCheckRtnNumber, vchCheckDLNumber, vchCheckDLState, chrCheckAcctType,"
		strSQL = strSQL & " vchCheckAcctNumber"
		strSQL = strSQL & " FROM " & STR_TABLE_SHOPPER
		strSQL = strSQL & " WHERE intID=" & intBillID
		set rsTemp = gobjConn.execute(strSQL)
		if not rsTemp.eof then
			if not IsNull(rsTemp("chrPaymentMethod")) then
				dim dctSaveList
				if rsTemp("chrPaymentMethod") = "OCC" then
					set dctSaveList = Server.CreateObject("Scripting.Dictionary")
						dctSaveList.Add "@dtmUpdated", "GETDATE()"
						dctSaveList.Add "vchUpdatedByUser", "pub"
						dctSaveList.Add "vchUpdatedByIP", gstrUserIP
						dctSaveList.Add "!chrPaymentMethod", ""
						dctSaveList.Add "!vchPaymentCardName", "vchName"
						dctSaveList.Add "!vchPaymentCardType", "vchCardType"
						dctSaveList.Add "!vchPaymentCardNumber", "vchCardNumber"
						dctSaveList.Add "!vchPaymentCardExtended", "vchCardExtended"
						dctSaveList.Add "!chrPaymentCardExpMonth", "chrCardExpMonth"
						dctSaveList.Add "!chrPaymentCardExpYear", "chrCardExpYear"
					SaveDataRecord STR_TABLE_ORDER, rsTemp, intOrderID, dctSaveList
				elseif rsTemp("chrPaymentMethod") = "OEC" then
					set dctSaveList = Server.CreateObject("Scripting.Dictionary")
						dctSaveList.Add "@dtmUpdated", "GETDATE()"
						dctSaveList.Add "vchUpdatedByUser", "pub"
						dctSaveList.Add "vchUpdatedByIP", gstrUserIP
						dctSaveList.Add "!chrPaymentMethod", ""
						dctSaveList.Add "!vchPaymentBankName", "vchCheckBankName"
						dctSaveList.Add "!vchPaymentRtnNumber", "vchCheckRtnNumber"
						dctSaveList.Add "!vchPaymentDLNumber", "vchCheckDLNumber"
						dctSaveList.Add "!vchPaymentDLState", "vchCheckDLState"
						dctSaveList.Add "!chrPaymentAcctType", "chrCheckAcctType"
						dctSaveList.Add "!vchPaymentAcctNumber", "vchCheckAcctNumber"
					SaveDataRecord STR_TABLE_ORDER, rsTemp, intOrderID, dctSaveList
				end if
			end if
		end if
		rsTemp.close
		set rsTemp = nothing
		VoidPriorTransactions_Other intOrderID
	end if
end sub

function CheckIsBillID_Other(intUserID, blnShipRecord, intBillID)
	' verify that the specified billing record is valid for this user
	if intUserID > 0 and IsNumeric(intBillID) then
		dim strSQL, rsTemp
		strSQL = "SELECT intID FROM " & STR_TABLE_SHOPPER & " WHERE chrStatus='A' AND intShopperID=" & intUserID & " AND intID=" & CLng(intBillID) & " AND (chrType='B'"
		if blnShipRecord then
			strSQL = strSQL & " OR chrType='S'"
		end if
		strSQL = strSQL & ")"
		set rsTemp = gobjConn.execute(strSQL)
		CheckIsBillID_Other = not rsTemp.eof
		rsTemp.close
		set rsTemp = nothing
	else
		CheckIsBillID_Other = false
	end if
end function

function GetProtectedCardNumber(strValue)
	' convert every character except the last 4 digits to an 'x'
	dim c, x, ch, s
	s = ""
	x = len(strValue)
	c = 4
	while x > 0
		ch = mid(strValue,x,1)
		if InStr("0123456789", ch) > 0 then
			if c > 0 then
				c = c - 1
				s = ch & s
			else
				s = "x" & s
			end if
		else
			s = ch & s
		end if
		x = x - 1
	wend
	GetProtectedCardNumber = s
end function

function GetProtectedCardNumberX(strValue)
	' SWSCodeChange - 7/19/00 reversed IF condition
	if not CheckUserAccess(ACC_CARDS_VIEW) then
		GetProtectedCardNumberX = GetProtectedCardNumber(strValue)
	else
		GetProtectedCardNumbeRX = strValue
	end if
end function

sub MakeAuditRecord(strType, strAction, strObject, strDescription)
'	on error resume next
	dim dctSaveList
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "@dtmCreated", "GETDATE()"
	if Session(SESSION_MERCHANT_ULOGIN) <> "" then
		dctSaveList.Add "vchCreatedByUser", Session(SESSION_MERCHANT_ULOGIN)
	else
		dctSaveList.Add "vchCreatedbyUser", "*"
	end if
		dctSaveList.Add "vchCreatedByIP", gstrUserIP
		dctSaveList.Add "chrType", strType
		dctSaveList.Add "chrStatus", "A"
		dctSaveList.Add "vchAction", strAction
		dctSaveList.Add "vchObject", strObject
		dctSaveList.Add "vchDescription", strDescription
	SaveDataRecord STR_TABLE_AUDIT, Request, 0, dctSaveList
end sub

sub xGetReportDateRange(dtmStart, dtmEnd)
	dtmStart = Session(SESSION_REPORT_START)
	dtmEnd = Session(SESSION_REPORT_END)
	if IsDate(dtmStart) then
		dtmStart = CDate(dtmStart)
	else
		dtmStart = CDate(Month(Date()) & "/1/" & Year(Date()))
		Session(SESSION_REPORT_START) = dtmStart
	end if
	dtmStart = DateValue(dtmStart)
	if IsDate(dtmEnd) then
		dtmEnd = CDate(dtmEnd)
	else
		dtmEnd = CDate(Date() & " 11:59:59 pm")
		Session(SESSION_REPORT_END) = dtmEnd
	end if
end sub

sub xSetReportDateRange(dtmStart, dtmEnd)
	Session(SESSION_REPORT_START) = dtmStart
	Session(SESSION_REPORT_END) = dtmEnd
end sub

sub GetExportFlags(blnIncludeBilling, blnIncludeShipping, blnIncludeNotes, strFormat)
	dim strShopperTypes, strShopperNotes
	strFormat = Session(SESSION_EXPORT_FORMAT)
	if strFormat = "" then
		strFormat = "CSV"
		strShopperTypes = "S"
		strShopperNotes = ""
		blnIncludeShipping = true
		blnIncludeBilling = false
		blnIncludeNotes = false
		Session(SESSION_EXPORT_FORMAT) = strFormat
		Session(SESSION_EXPORT_SHOPPER) = strShopperTypes
		Session(SESSION_EXPORT_NOTES) = strShopperNotes
	else
		strShopperTypes = Session(SESSION_EXPORT_SHOPPER)
		blnIncludeShipping = (InStr(strShopperTypes, "S") > 0)
		blnIncludeBilling = (InStr(strShopperTypes, "B") > 0)
		'blnIncludeNotes = (InStr(strShopperNotes, "Y") > 0)
		blnIncludeNotes = "Y"
	end if
end sub

sub SetExportFlags(blnIncludeBilling, blnIncludeShipping, blnIncludeNotes, strFormat)
	dim strShopperTypes, strShopperNotes
	strShopperTypes = ""
	strShopperNotes = ""
	if blnIncludeBilling then
		strShopperTypes = strShopperTypes & "B"
	end if
	if blnIncludeShipping then
		strShopperTypes = strShopperTypes & "S"
	end if
	strShopperNotes = "Y"
	Session(SESSION_EXPORT_FORMAT) = strFormat
	Session(SESSION_EXPORT_SHOPPER) = strShopperTypes
	Session(SESSION_EXPORT_NOTES) = strShopperNotes
end sub

' SWSCodeChange 7/19/00 - Added SetOrderDateSubmitted() and SetOrderDateSubmitted_Other()
sub SetOrderDateSubmitted(dtmDate)
	if gintOrderID > 0 then
		SetOrderDateSubmitted_Other gintOrderID, dtmDate
	end if
end sub

sub SetOrderDateSubmitted_Other(intOrderID, dtmDate)
	dim dctSaveList
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "vchUpdatedByUser", "pub"
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "%dtmSubmitted", dtmDate
	SaveDataRecord STR_TABLE_ORDER, Request, intOrderID, dctSaveList
end sub

%>