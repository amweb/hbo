<%@ LANGUAGE="VBScript" %>
<!--#include file="incInit.asp"-->
<!--#include file="Json_2.0.4.asp"-->
<%
Response.ContentType = "application/json; charset=utf-8"

dim reqAction
dim strSQL, cmd, arParams, col

reqAction = request("action")

select case reqAction
	case "checkForDuplicate"
		checkForDuplicate
end select



function URLDecode(sText)
	dim sDecoded, oRegExpr, oMatchCollection, oMatch
	sDecoded = sText
	Set oRegExpr = Server.CreateObject("VBScript.RegExp")
	oRegExpr.Pattern = "%[0-9,A-F]{2}"
	oRegExpr.Global = True
	Set oMatchCollection = oRegExpr.Execute(sText)
	For Each oMatch In oMatchCollection
		sDecoded = Replace(sDecoded,oMatch.value,Chr(CInt("&H" & Right(oMatch.Value,2))))
	Next
	URLDecode = sDecoded
End function


sub checkForDuplicate
	OpenConn()
	dim cmd, strSQL, arParams
	dim intProfileID, rsNumberOfProfiles, rsProfileNameFromDB, blnDuplicateDetected, vchNewProfileName, vchInitialProfileName
	intProfileID = request("intProfileID")
	vchInitialProfileName = URLDecode(request("vchProfileName")&"")
	
	'check if the name that has been submitted is the same as the one in the database for this ID. If so, do nothing. Otherwise, return an error and prevent action.
	strSQL = "SELECT [vchProfileName] FROM " & STR_TABLE_PROFILES & " WHERE [intID]=" & intProfileID
	set rsProfileNameFromDB = gobjconn.execute(strSQL)
	if not rsProfileNameFromDB.eof then
		if not rsProfileNameFromDB("vchProfileName") = vchInitialProfileName then
			strSQL = "SELECT COUNT([intID]) AS [numProfiles] FROM " & STR_TABLE_PROFILES & " WHERE [vchProfileName]='" & vchInitialProfileName & "' AND [chrStatus]='A'"
			set rsNumberOfProfiles = gobjconn.execute(strSQL)
			'response.write rsNumberOfProfiles("numProfiles")
			if rsNumberOfProfiles("numProfiles") > 0 then
				response.write "{""DuplicateItems"": ""true""}"
			else
				response.write "{""DuplicateItems"": ""false""}"
			end if
		else
			response.write "{""DuplicateItems"": ""false""}"
		end if
	end if
	
	CloseConn()
	response.end
end sub


%>