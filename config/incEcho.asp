<%

' =====================================================================================
' = File: incEcho.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   ECHOnline Processor Integration
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   Check with Shaun before making any changes to this code (other than ID/PIN codes)
' =====================================================================================

const STR_MERCHANT_ECHO_ID = "916>4413734"	' Client
const STR_MERCHANT_ECHO_PIN = "10311953"	' Client
const STR_ISP_ECHO_ID = "916>4467457"	' SacWeb
const STR_ISP_ECHO_PIN = "67882963"		' SacWeb
const STR_TEST_ECHO_ID = "123>4681007"	' SacWeb Test
const STR_TEST_ECHO_PIN = "24441664"	' SacWeb Test

dim dctDeclineValues ' ECHO decline codes
set dctDeclineValues = Server.CreateObject("Scripting.Dictionary")
	dctDeclineValues.Add 1, "Refer to card issuer"
	dctDeclineValues.Add 3, "Invalid merchant number"
	dctDeclineValues.Add 4, "Capture card"
	dctDeclineValues.Add 5, "Do not honor"
	dctDeclineValues.Add 12, "Invalid transaction"
	dctDeclineValues.Add 13, "Invalid amount"
	dctDeclineValues.Add 14, "Invalid card number"
	dctDeclineValues.Add 30, "Format error"
	dctDeclineValues.Add 41, "Lost card"
	dctDeclineValues.Add 43, "Stolen card"
	dctDeclineValues.Add 51, "Over credit limit"
	dctDeclineValues.Add 54, "Expired card"
	dctDeclineValues.Add 55, "Incorrect PIN"
	dctDeclineValues.Add 57, "Card does not support this type of transaction"
	dctDeclineValues.Add 58, "Merchant account does not support this type of transaction"
	dctDeclineValues.Add 61, "Daily withdrawl limit exceeded"
	dctDeclineValues.Add 62, "Restricted card"
	dctDeclineValues.Add 63, "Security violation - the card has been restricted"
	dctDeclineValues.Add 65, "The allowed number of daily transactionshas been exceeded"
	dctDeclineValues.Add 75, "The allowed number of PIN retries has been exceeded"
	dctDeclineValues.Add 76, "Invalid 'to' account"
	dctDeclineValues.Add 77, "Invalid 'from' account"
	dctDeclineValues.Add 78, "Invalid account"
	dctDeclineValues.Add 84, "The authorization life cycle is above or below limits established by the issuer"
	dctDeclineValues.Add 91, "The bank is not available to authorize this transaction"
	dctDeclineValues.Add 92, "Unable to route"
	dctDeclineValues.Add 94, "Duplicate transaction"
	dctDeclineValues.Add 96, "A system error has occurred"
	dctDeclineValues.Add 1000, "A system error has occurred"
	dctDeclineValues.Add 1001, "Merchant account has been closed"
	dctDeclineValues.Add 1012, "Invalid transaction code"
	dctDeclineValues.Add 1013, "ECHO-ID is invalid"
	dctDeclineValues.Add 1015, "Invalid card number"
	dctDeclineValues.Add 1016, "Invalid expiration date"
	dctDeclineValues.Add 1017, "Invalid amount"
	dctDeclineValues.Add 1021, "Merchant or card holder is not allowed to perform this transaction"
	dctDeclineValues.Add 1024, "Invalid auth code"
	dctDeclineValues.Add 1025, "Invalid reference number"

' Trans_Prepare():
'	prepare transaction (create pending trans record, check billing data, etc.)
'	return transaction record ID
function Trans_Prepare(intOrderID, strTransType, mnyAmount)
	dim rsTemp, strSQL
	strSQL = "SELECT * FROM " & STR_TABLE_ORDER & " WHERE intID=" & intOrderID
	set rsTemp = gobjConn.execute(strSQL)
	if rsTemp.eof then
		Trans_Prepare = 0
		rsTemp.close
		set rsTemp = nothing
		exit function
	end if
	
	dim strAccountType
	' check if test order or real order
	if InStr(rsTemp("vchPaymentCardName"), "{TEST}") > 0 then
		strAccountType = "D"
	else
		strAccountType = "M"
	end if
	
	rsTemp.close
	set rsTemp = nothing
	
	dim dctSaveList
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "*@dtmCreated", "GETDATE()"
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "*vchCreatedByUser", "pub"
		dctSaveList.Add "vchUpdatedByUser", "pub"
		dctSaveList.Add "*vchCreatedByIP", gstrUserIP
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "chrType", "R"
		dctSaveList.Add "*chrStatus", "P"
		dctSaveList.Add "#intOrderID", intOrderID
		dctSaveList.Add "chrAccountType", strAccountType
		dctSaveList.Add "chrTransType", strTransType
		dctSaveList.Add "%dtmTransDate", Now()
		dctSaveList.Add "#mnyTransAmount", mnyAmount
	Trans_Prepare = SaveDataRecord("" & STR_TABLE_TRANS, Request, 0, dctSaveList)
end function

' Trans_Run()
'	perform the actual transaction and update the transaction record as appropriate
'	return one of the following values:
'		0 = transaction successful
'		1 = transaction declined
'		2 = transaction requires verbal merchant assistance
'		3 = system error occurred -- transaction pending
' 	bkonvalin 2003/04/04: added sales_tax & purchase_order_number to transaction
' 	to get the ECHO reduced rate ("partially-qualified commercial transaction")
function Trans_Run(intTransID)
	'on error resume next
	on error goto 0
	' AV or ES: authorize or deposit
	' DS: deposit previous auth
	' CR: credit previous deposit
	
	' Get Order Info
	dim objEcho, rsTrans, strSQL, intOrderID, chrAccountType, chrTransType, mnyTransAmount, mnyTaxAmount
	dim intPurchaseOrder, vchShopperIP, vchPaymentCardNumber, chrPaymentCardExpMonth, chrPaymentCardExpYear
	dim strEchoStatus, strEchoOrderID, strRefID, strAuthID, strDeclineID, strAV_ID, strShopperMessage
	dim vchPaymentCardName, vchBillAddress1, vchBillAddress2, vchBillCity, vchBillState, vchBillZip, vchBillPhone, vchBillEmail
	strSQL = "SELECT T.intOrderID, T.chrAccountType, T.chrTransType, T.mnyTransAmount, O.mnyTaxAmount,"
	strSQL = strSQL & " O.vchShopperIP, O.vchPaymentCardNumber, O.chrPaymentCardExpMonth, O.chrPaymentCardExpYear, O.vchPaymentCardName,"
	strSQL = strSQL & " B.vchAddress1, B.vchAddress2, B.vchCity, B.vchState, B.vchZip, B.vchDayPhone, B.vchEmail"
	strSQL = strSQL & " "
	strSQL = strSQL & " FROM " & STR_TABLE_TRANS & " AS T, " & STR_TABLE_ORDER & " AS O, " & STR_TABLE_SHOPPER & " AS B"
	strSQL = strSQL & " WHERE T.intID=" & intTransID
	strSQL = strSQL & " AND T.intOrderID = O.intID"
	strSQL = strSQL & " AND O.intBillShopperID = B.intID"
	set rsTrans = gobjConn.execute(strSQL)
	if rsTrans.eof then
		rsTrans.close
		set rsTrans = nothing
		Trans_UpdateSysError intTransID, "Trans EOF"
		Trans_Run = 3
		exit function
	end if
	intOrderID = rsTrans("intOrderID") & ""
	intPurchaseOrder = rsTrans("intOrderID") & ""
	chrAccountType = rsTrans("chrAccountType") & ""
	chrTransType = rsTrans("chrTransType") & ""
	mnyTransAmount = rsTrans("mnyTransAmount") & ""
	mnyTaxAmount = rsTrans("mnyTaxAmount") & ""
	vchShopperIP = rsTrans("vchShopperIP") & ""
	vchPaymentCardNumber = rsTrans("vchPaymentCardNumber") & ""
	chrPaymentCardExpMonth = rsTrans("chrPaymentCardExpMonth") & ""
	chrPaymentCardExpYear = rsTrans("chrPaymentCardExpYear") & ""
	' 09may2001 ssutterfield: added information fields for AVS verification
	vchPaymentCardName = rsTrans("vchPaymentCardName") & ""
	vchBillAddress1 = rsTrans("vchAddress1") & ""
	vchBillAddress2 = rsTrans("vchAddress2") & ""
	vchBillCity = rsTrans("vchCity") & ""
	vchBillState = rsTrans("vchState") & ""
	vchBillZip = rsTrans("vchZip") & ""
	vchBillEmail = rsTrans("vchEmail") & ""
	rsTrans.close
	set rsTrans = nothing
	
	' Is it a test order?
	if chrAccountType = "D" then
		if not CheckTestCardNumber(chrTransType, vchPaymentCardNumber, chrPaymentCardExpMonth, chrPaymentCardExpYear, strEchoStatus, strEchoOrderID, strRefID, strAuthID, strDeclineID, strAV_ID, strShopperMessage) then
			' simulated result -- no need to perform a real transaction
			Trans_Run = Trans_ProcessResult(intTransID, strEchoStatus, strEchoOrderID, strRefID, strAuthID, strDeclineID, strAV_ID, strShopperMessage)
			exit function
		end if
		' need to perform a real transaction
	end if
	
	' setup ECHO call
	set objEcho = Server.CreateObject("GoTransact.Transaction")
	if CheckForError("", false, false) then
		Trans_UpdateSysError intTransID, "Trans_Run checkpoint 1"
		Trans_Run = 3
		exit function
	end if
	objEcho.SelectProcessor "Echo"
	objEcho.SetField "transaction_type", chrTransType
	
	select case chrAccountType
		case "D":
			' use SacWeb test account
			objEcho.SetField "order_type", "S"
			objEcho.SetField "merchant_echo_id", STR_TEST_ECHO_ID
			objEcho.SetField "merchant_pin", STR_TEST_ECHO_PIN
			objEcho.SetField "counter", randnum(1,999)
		case "M":
			' use merchant account
			objEcho.SetField "order_type", "F"
			objEcho.SetField "merchant_echo_id", STR_MERCHANT_ECHO_ID
			objEcho.SetField "merchant_pin", STR_MERCHANT_ECHO_PIN
			objEcho.SetField "isp_echo_id", STR_ISP_ECHO_ID
			objEcho.SetField "isp_pin", STR_ISP_ECHO_PIN
			' 11dec2000 - ssutterfield: updated to use order ID for duplication counter
			objEcho.SetField "counter", intOrderID
			'objEcho.SetField "counter", 1
		case else:
			' unknown account type -- system error
			Trans_UpdateSysError intTransID, "Unknown Account Type"
			Trans_Run = 3
	end select
	
	objEcho.SetField "merchant_email", STR_MERCHANT_EMAIL
	objEcho.SetField "billing_ip_address", Request("REMOTE_ADDR")
	
	objEcho.SetField "billing_name", vchPaymentCardName
	objEcho.SetField "billing_address1", vchBillAddress1
	objEcho.SetField "billing_address2", vchBillAddress2
	objEcho.SetField "billing_city", vchBillCity
	objEcho.SetField "billing_state", vchBillState
	objEcho.SetField "billing_zip", vchBillZip
	objEcho.SetField "billing_phone", vchBillPhone
	objEcho.SetField "billing_email", vchBillEmail
	
	if chrTransType = "DS" or chrTransType = "DV" then
		' order number and auth from AS or AV transaction
		strSQL = "SELECT vchAuthCode,vchOrderCode FROM " & STR_TABLE_TRANS & " WHERE intOrderID=" & intOrderID & " AND chrStatus='A' AND chrTransType IN ('AS','AV')"
		set rsTrans = gobjConn.execute(strSQL)
		if not rsTrans.eof then
			objEcho.SetField "authorization", rsTrans("vchAuthCode") & ""
			objEcho.SetField "order_number", rsTrans("vchOrderCode") & ""
		end if
		rsTrans.close
		set rsTrans = nothing
	end if
	
	if chrTransType = "CR" then
		' order number from ES or DS transaction
		strSQL = "SELECT mnyTransAmount,vchOrderCode FROM " & STR_TABLE_TRANS & " WHERE intOrderID=" & intOrderID & " AND chrStatus='A' AND chrTransType IN ('ES','EV','DS')"
		set rsTrans = gobjConn.execute(strSQL)
		if not rsTrans.eof then
			objEcho.SetField "original_amount", SafeFormatNumber("0",rsTrans("mnyTransAmount"),2) & ""
			objEcho.SetField "order_number", rsTrans("vchOrderCode") & ""
		end if
		rsTrans.close
		set rsTrans = nothing
	end if

' debug code for objEcho
'response.write "objEcho - Debug Code" & "<br />"
'response.write "mnyTransAmount: " & mnyTransAmount & " " & SafeFormatNumber("0",mnyTransAmount,2) & " " & SafeFormatNumber("0",mnyTransAmount+.0045,2) & "<br />"
'response.write "mnyTaxAmount: " & mnyTaxAmount & SafeFormatNumber("0",mnyTaxAmount,2) & "<br />"

	objEcho.SetField "shipping_flag", "N"
	objEcho.SetField "cc_number", vchPaymentCardNumber
	objEcho.SetField "ccexp_month", chrPaymentCardExpMonth
	objEcho.SetField "ccexp_year", chrPaymentCardExpYear
	objEcho.SetField "grand_total", SafeFormatNumberEcho(mnyTransAmount+.0045)
	objEcho.SetField "sales_tax", SafeFormatNumber("0",mnyTaxAmount,2)
	objEcho.SetField "purchase_order_number", intPurchaseOrder
	
	dim blnReply, strDebugInfo
	blnReply = objEcho.Process()
	if CheckForError("", false, false) then
		Trans_UpdateSysError intTransID, "Trans_Run checkpoint 2"
		Trans_Run = 3
		exit function
	end if
	
	strDebugInfo = "HTTP time: " & objEcho.GetResult("dbg_httptime") & vbcrlf
	strDebugInfo = strDebugInfo & "Sent: " & objEcho.GetResult("dbg_querysent") & vbcrlf
	strDebugInfo = strDebugInfo & "Received: " & objEcho.GetResult("dbg_responsercvd") & vbcrlf
	
	Trans_SaveDebugInfo intTransID, strDebugInfo
	
	if blnReply = false then
		' system error
		Trans_UpdateSysError intTransID, objEcho.GetError(1) & vbCRLF & objEcho.GetError(2) & vbCRLF & objEcho.GetError(3) & vbcrlf
		set objEcho = nothing
		Trans_Run = 3
		exit function
	end if
	
	strEchoStatus = objEcho.GetResult("status")
	strEchoOrderID = objEcho.GetResult("order_number")
	strRefID = objEcho.GetResult("echo_reference")
	strAuthID = objEcho.GetResult("auth_code")
	strDeclineID = objEcho.GetResult("decline_code")
	strAV_ID = objEcho.GetResult("avs_result")
	strShopperMessage = objEcho.GetResult("msg_for_shopper")
	
	Trans_Run = Trans_ProcessResult(intTransID, strEchoStatus, strEchoOrderID, strRefID, strAuthID, strDeclineID, strAV_ID, strShopperMessage)
	
	set objEcho = Nothing
	
	if CheckForError("", false, false) then
		Trans_UpdateSysError intTransID, "Trans_Run checkpoint 3"
		Trans_Run = 3
		exit function
	end if
end function

sub Trans_UpdateSysError(intTransID, strMsg)
	if err.number <> 0 then
		strMsg = "ASP Err " & err.number & ": " & err.description & " (" & err.source & ")" & vbcrlf & strMsg
	end if
	
	on error resume next
	SendAdminErrorEmail strMsg
	
	err.clear
	
	' update the transaction record--append this to the end of the existing txtComments field
	dim strSQL, rsTemp
	strSQL = "SELECT txtComments FROM " & STR_TABLE_TRANS & " WHERE intID = " & intTransID
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		strMsg = rsTemp("txtComments") & vbcrlf & strMsg
		rsTemp.close
		set rsTemp = nothing
		dim dctSaveList
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "@dtmUpdated", "GETDATE()"
			dctSaveList.Add "vchUpdatedByUser", "pub"
			dctSaveList.Add "vchUpdatedByIP", gstrUserIP
			dctSaveList.Add "chrStatus", "E"
			dctSaveList.Add "txtComments", strMsg
		SaveDataRecord STR_TABLE_TRANS, Request, intTransID, dctSaveList
	else
		rsTemp.close
		set rsTemp = nothing
	end if
end sub

function randnum(intMin, intMax)
	randomize
	randnum = fix(rnd * (intMax - intMin)) + intMin
end function

function CheckTestCardNumber(chrTransType, vchPaymentCardNumber, chrPaymentCardExpMonth, chrPaymentCardExpYear, strEchoStatus, strEchoOrderID, strRefID, strAuthID, strDeclineID, strAV_ID, strShopperMessage)
	dim strCardNumber
	CheckTestCardNumber = false
	strCardNumber = GetDigitsOnly(vchPaymentCardNumber)
	if strCardNumber = "4111111111111111" then	' AOL QuickCheckout test card #
		strCardNumber = "1111222233335555"
	end if
	if len(strCardNumber) <> 16 or left(strCardNumber,12) <> "111122223333" then
		' invalid test card number
		strEchoStatus = "D"
		strEchoOrderID = ""
		strRefID = ""
		strAuthID = ""
		strDeclineID = "1015"
		strAV_ID = ""
		strShopperMessage = "Invalid TEST credit card number."
		exit function
	end if
	if not IsNumeric(chrPaymentCardExpMonth) or not IsNumeric(chrPaymentCardExpYear) then
		' invalid expiration date
		strEchoStatus = "D"
		strEchoOrderID = ""
		strRefID = ""
		strAuthID = ""
		strDeclineID = "54"
		strAV_ID = ""
		strShopperMessage = "Invalid TEST expiration date."
		exit function
	end if
	' determine action to take based on last four digits of card number
	' (the exact digit depends on the transaction being performed)
	dim intPos, chrAction
	select case chrTransType
	' AV or ES: authorize or deposit
	' DS: deposit previous auth
	' CR: credit previous deposit
		case "AS", "AV": intPos = 13
		case "DS", "DV", "ES", "EV": intPos = 15
		case "CR": intPos = 16
		case else:
			' invalid transaction
			strEchoStatus = "E"
			strEchoOrderID = ""
			strRefID = ""
			strAuthID = ""
			strDeclineID = "-1"
			strAV_ID = ""
			strShopperMessage = "Invalid TEST transaction."
			exit function
	end select
	chrAction = mid(strCardNumber, intPos, 1)
	select case chrAction:
		case "4": ' do real transaction - substitute ECHO test credit card number
			CheckTestCardNumber = true
			vchPaymentCardNumber = "4005-5500-0000-0019"
		case "5": ' fake OK
			strEchoStatus = "G"
			strEchoOrderID = "1111-22222-33333"
			if chrTransType = "DS" or chrTransType = "ES" or chrTransType = "CR" then
				strRefID = "999999"
			else
				strRefID = ""
			end if
			strAuthID = "TT1111"
			strDeclineID = ""
			strAV_ID = ""
			strShopperMessage = "TEST Transaction Approved"
		case "0": ' fake 1015 error
			strEchoStatus = "D"
			strEchoOrderID = ""
			strRefID = ""
			strAuthID = ""
			strDeclineID = "1015"
			strAV_ID = ""
			strShopperMessage = "TEST - invalid credit card number"
		case "1": ' fake 51 error
			strEchoStatus = "D"
			strEchoOrderID = ""
			strRefID = ""
			strAuthID = ""
			strDeclineID = "51"
			strAV_ID = ""
			strShopperMessage = "TEST - insufficient funds"
		case "2": ' fake 2071 error
			strEchoStatus = "D"
			strEchoOrderID = ""
			strRefID = ""
			strAuthID = ""
			strDeclineID = "2071"
			strAV_ID = ""
			strShopperMessage = "TEST - voice authorization required"
		case "3": ' fake 57
			strEchoStatus = "D"
			strEchoOrderID = ""
			strRefID = ""
			strAuthID = ""
			strDeclineID = "57"
			strAV_ID = ""
			strShopperMessage = "TEST - transaction not permitted"
		case "7": ' fake 1511
			strEchoStatus = "D"
			strEchoOrderID = ""
			strRefID = ""
			strAuthID = ""
			strDeclineID = "1511"
			strAV_ID = ""
			strShopperMessage = "TEST - duplicate transaction rejected"
		case "9": ' fake 4
			strEchoStatus = "D"
			strEchoOrderID = ""
			strRefID = ""
			strAuthID = ""
			strDeclineID = "4"
			strAV_ID = ""
			strShopperMessage = "TEST - card reported with lost/stolen status"
		case else:
			strEchoStatus = "D"
			strEchoOrderID = ""
			strRefID = ""
			strAuthID = ""
			strDeclineID = "1015"
			strAV_ID = ""
			strShopperMessage = "TEST - invalid credit card number"
	end select
	if chrTransType = "AV" or chrTransType = "EV" or chrTransType = "DV" then
		' also need AV result
		select case mid(strCardNumber, 14, 1):
			case "4", "5": ' fake OK
				strAV_ID = "X"
			case "0": ' no match
				strAV_ID = "N"
			case "1": ' 5-zip match
				strAV_ID = "Z"
			case "2": ' 9-zip match
				strAV_ID = "W"
			case "3": ' street match
				strAV_ID = "A"
			case "6": ' no info
				strAV_ID = "E"
			case else:
				strAV_ID = ""
		end select
	end if
end function

function GetDigitsOnly(s)
	dim strNew, c, x, ch
	strNew = ""
	x = len(s)
	for c = 1 to x
		ch = mid(s, c, 1)
		if InStr("0123456789", ch) > 0 then
			strNew = strNew & ch
		end if
	next
	GetDigitsOnly = strNew
end function

sub Trans_SaveDebugInfo(intTransID, strMsg)
	' add the given string to the txtComments field
	dim strSQL, rsTemp
	strSQL = "SELECT txtComments FROM " & STR_TABLE_TRANS & " WHERE intID=" & intTransID
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		strMsg = rsTemp("txtComments") & vbcrlf & strmsg
		rsTemp.close
		set rsTemp = nothing
		strSQL = "UPDATE " & STR_TABLE_TRANS & " SET txtComments='" & SQLEncode(strMsg) & "' WHERE intID=" & intTransID
		gobjConn.execute(strSQL)
	else
		rsTemp.close
		set rsTemp = nothing
	end if
end sub

function Trans_ProcessResult(intTransID, strEchoStatus, strEchoOrderID, strRefID, strAuthID, strDeclineID, strAV_ID, strShopperMessage)
	on error resume next
	' return values:
	'   0 = ok
	'   1 = failed (declined)
	'   2 = verbal auth
	'   3 = system error
	dim strDescription
	select case strEchoStatus
		case "G": ' approved
			Trans_ProcessResult = 0
			Trans_UpdateApproved intTransID, strEchoOrderID, strRefID, strAuthID, strAV_ID, strShopperMessage
			if CheckForError("Trans_ProcessResult checkpoint 1", true, true) then
				Trans_UpdateSysError intTransID, "Trans_ProcessResult checkpoint 1"
				Trans_ProcessResult = 3
			end if
		case "D", "C": ' declined
			Trans_ProcessResult = 1
			select case Trans_LookupDeclineCode(strDeclineID, strDescription)
				case 0: ' normal decline
					Trans_UpdateDeclined intTransID, strEchoOrderID, strDeclineID, strShopperMessage
				case 1: ' fraud catch
					Trans_UpdateDeclined intTransID, strEchoOrderID, strDeclineID, strShopperMessage
				case 2: ' voice auth
					Trans_ProcessResult = 2
					Trans_UpdateVoiceDeclined intTransID, strEchoOrderID, strDeclineID, strShopperMessage
				case 3: ' system error
					Trans_ProcessResult = 3
					Trans_UpdateSysError intTransID, "TIMEOUT Decline Code " & strDeclineID & " (" & strDescription & ") - " & strShopperMessage
				case else: ' unknown - 
			end select			
			if CheckForError("Trans_ProcessResult checkpoint 2", true, true) then
				Trans_UpdateSysError intTransID, "Trans_ProcessResult checkpoint 2"
				Trans_ProcessResult = 3
			end if
		case "T": ' error
			Trans_ProcessResult = 3
			Trans_UpdateSysError intTransID, "TIMEOUT Decline Code " & strDeclineID & " - " & strShopperMessage
			if CheckForError("Trans_ProcessResult checkpoint 3", true, true) then
				Trans_UpdateSysError intTransID, "Trans_ProcessResult checkpoint 3"
			end if
		case else: ' unknown result - system error
			Trans_ProcessResult = 3
			Trans_UpdateSysError intTransID, "UNKNOWN RESULT CODE: " & strEchoStatus & " - " & strShopperMessage
			if CheckForError("Trans_ProcessResult checkpoint 4", true, true) then
				Trans_UpdateSysError intTransID, "Trans_ProcessResult checkpoint 4"
			end if
	end select
end function

sub Trans_UpdateApproved(intTransID, strEchoOrderID, strRefID, strAuthID, strAV_ID, strShopperMessage)
	dim dctSaveList
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "vchUpdatedByUser", "pub"
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "chrStatus", "A"
		dctSaveList.Add "vchAuthCode", strAuthID
		dctSaveList.Add "vchOrderCode", strEchoOrderID
		dctSaveList.Add "vchRefCode", strRefID
		dctSaveList.Add "vchAVCode", strAV_ID
		dctSaveList.Add "vchShopperMsg", left(strShopperMessage, 255)
	SaveDataRecord STR_TABLE_TRANS, Request, intTransID, dctSaveList
end sub

sub Trans_UpdateDeclined(intTransID, strEchoOrderID, strDeclineID, strShopperMessage)
	dim dctSaveList
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "vchUpdatedByUser", "pub"
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "chrStatus", "K"
		dctSaveList.Add "vchDeclineCode", strDeclineID
		dctSaveList.Add "vchOrderCode", strEchoOrderID
		dctSaveList.Add "vchShopperMsg", left(strShopperMessage, 255)
	SaveDataRecord STR_TABLE_TRANS, Request, intTransID, dctSaveList
end sub

sub Trans_UpdateVoiceDeclined(intTransID, strEchoOrderID, strDeclineID, strShopperMessage)
	dim dctSaveList
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "vchUpdatedByUser", "pub"
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "chrStatus", "V"
		dctSaveList.Add "vchDeclineCode", strDeclineID
		dctSaveList.Add "vchOrderCode", strEchoOrderID
		dctSaveList.Add "vchShopperMsg", left(strShopperMessage, 255)
	SaveDataRecord STR_TABLE_TRANS, Request, intTransID, dctSaveList
end sub

function Trans_LookupDeclineCode(strDeclineCode, strDescription)
	' returns:
	'   intAction = 0 (nothing special), 1 (fraud catch), 2 (voice auth), 3 (sys err)
	'   strDescription = decline text (or "" if unknown)
	dim intDeclineCode
	if IsNumeric(strDeclineCode) then
		intDeclineCode = CLng(strDeclineCode)
	else
		intDeclineCode = -1
	end if
	select case intDeclineCode
		case 4, 41, 43:
			Trans_LookupDeclineCode = 1
		case 2071, 2072, 2073, 2074, 2075, 2076, 2078, 2079:
			Trans_LookupDeclineCode = 2
		case 3, 30:
			Trans_LookupDeclineCode = 3
		case else:
			Trans_LookupDeclineCode = 0
	end select
	if dctDeclineValues.Exists(intDeclineCode) then
		strDescription = dctDeclineValues(intDeclineCode)
	else
		strDescription = ""
	end if
end function


function SafeFormatNumberEcho(number)
	Dim RegEx
	Set RegEx = New RegExp
	
	RegEx.IgnoreCase=true
	RegEx.Pattern = ","
	SafeFormatNumberEcho = RegEx.Replace(SafeFormatNumber("0",number,2),"")
end function

%>
