<%@ LANGUAGE="VBScript" %>
<!--#include file="incInit.asp"-->
<!--#include file="Json_2.0.4.asp"-->
<%
Response.ContentType = "application/json; charset=utf-8"

dim reqAction
dim strSQL, cmd, arParams, col

reqAction = request("action")

select case reqAction
	case "savePackSlip"
		SavePackSlip
	case "loadCartContents"
		loadCartContents
	case "loadItemSlot"
		loadItemSlot
	case "saveItemSlot"
		saveItemSlot
	case "loadLayout"
		loadLayout
	case "loadProfiles"
		loadProfiles
	case "loadOverview"
		loadOverview
	case "loadProfileID"
		loadProfileID
	case "storeLoadOverview"
		storeLoadOverview
    case "loadProfilesWithStore"
        loadProfilesWithStore
	case "loadQuickViewOverview"
		loadQuickViewOverview
	case "CountsAndAll"
		CountsAndAll
end select

Function QueryToJSON(dbcomm, params)
        Dim rs, jsa
        Set rs = dbcomm.Execute(,params,1)
        Set jsa = jsArray()
        Do While Not (rs.EOF Or rs.BOF)
                Set jsa(Null) = jsObject()
                For Each col In rs.Fields
                        jsa(Null)(col.Name) = col.Value
                Next
        rs.MoveNext
        Loop
        Set QueryToJSON = jsa
        rs.Close
End Function

sub SQLExampleASPJson
	strConn = "Provider=SQLNCLI10;Server=localhost;Database=NorthWind;Trusted_Connection=Yes;"
	Set conn = Server.CreateObject("ADODB.Connection")
	conn.Open strConn
	query = "SELECT * FROM Customers WHERE CustomerID = ?"
	CustomerID = Request.QueryString("CustomerID")
	arParams = array(CustomerID)
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = query
	Set cmd.ActiveConnection = conn
	QueryToJSON(cmd, arParams).Flush
end sub

sub loadCartContents

	dim count, rsData, strOutput
	count = 0
	OpenConn
	set rsData = GetOrderLineItems_Other(request("orderid"))
	strOutput = "["
	while not rsData.EOF
		if count <> 0 then
			strOutput= strOutput & ","
		end if
		
		strOutput = strOutput & "{""intParentID"": """ & rsData("intParentID") & """, ""intID"": """ & rsData("intID") & """, ""vchItemName"": """ & rsData("vchItemName") & """, ""vchPartNumber"": """ & rsData("vchPartNumber") & """, ""intQuantity"": """ & rsData("intQuantity") & """, ""vchImageURL"": """ & rsData("vchImageURL") & """}"
		
		count =  count +1
		rsData.MoveNext
	wend 
	strOutput = strOutput & "]"
	response.write strOutput
	response.flush
	CloseConn
	response.end

end sub

sub SavePackSlip

	Dim pickSlipNum, sku, rsCheck
	
	pickSlipNum = request("pickSlipNum")
	sku = request("sku")
	' response.write sku
	OpenConn
	
	strSQL = "select *  from " & STR_TABLE_PACKGROUP & " where vchPartNumber = '" & SQLEncode(URLDecode(sku)) & "'"
	
	set rsCheck = gobjConn.execute(strSQL)
	
	if rsCheck.eof then
		strSQL = "insert into " & STR_TABLE_PACKGROUP &" (vchPartNumber, intPackGroup) Values ('" & SQLEncode(URLDecode(sku)) & "', "& pickSlipNum & ") "
	else
		strSQL = "update " & STR_TABLE_PACKGROUP & " set intPackGroup = " & pickSlipNum & " where vchPartNumber = '" & SQLEncode(URLDecode(sku)) & "'"
	end if
	
	' response.write strSQL
	gobjConn.execute(strSQL)
	
	CloseConn()

end sub

sub loadProfiles

	strSQL = "SELECT [intID],[vchProfileName] FROM [" & STR_TABLE_PROFILES & "] WHERE [intID] IN (SELECT DISTINCT [intProfileID] FROM ["
	strSQL = strSQL & STR_TABLE_PROFILE_PACKAGES & "] WHERE [intCampaignID]=" & request("campaign") & ") AND [chrStatus]='A' ORDER BY [vchProfileName]"
    
 
    
	'response.write strSQL
	OpenConn()
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()
	response.end

end sub

sub loadProfilesWithStore
    ' strsql = "SELECT DISTINCT P.intid, P.vchprofilename "
    ' strsql = strsql & "FROM HBO_profilepackages AS PP "
    ' strsql = strsql & "INNER JOIN HBO_profilemapper AS PM ON PP.intprofileid = PM.intprofileid "
    ' strsql = strsql & "INNER JOIN HBO_profiles AS P ON PM.intprofileid = P.intid "
    ' strsql = strsql & "WHERE PM.intstoreshopperid = " & request("storeid") & " "
    ' strsql = strsql & "AND P.chrstatus = 'A' "
    ' strsql = strsql & "ORDER BY P.intid ASC "
    
    strsql = "SELECT DISTINCT P.intid, P.vchprofilename "
    strsql = strsql & "FROM " & STR_TABLE_PROFILE_MAPPER & " AS PM "
    strsql = strsql & "INNER JOIN " & STR_TABLE_PROFILES & " AS P ON PM.intprofileid = P.intid "
    strsql = strsql & "WHERE PM.intstoreshopperid = " & request("storeid") & " "
    strsql = strsql & "AND P.chrstatus = 'A' "
    strsql = strsql & "ORDER BY P.intid ASC "
    
    ' response.write strSQL
    ' response.end
	OpenConn()
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()
	response.end
end sub 

sub loadLayout

	dim intStoreID
	
	intStoreID = request("intStoreID")
	
	strSQL = "select L.intID, L.vchImageLocation, S.vchFirstName, S.vchLastName  from " & STR_TABLE_LAYOUTS & " L join " & STR_TABLE_SHOPPER & " S on S.intLayout = L.intID  where S.intID = " & intStoreID & ""
	'response.write strSQL
	OpenConn()
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()
	response.end

end sub

sub loadQuickViewOverview

	dim output, count, intLayoutID, intProfileID, intCampaignID, rsFixtures, rsCounts, intDenominator

	intLayoutID = request("intLayoutID")
	intProfileID = request("intProfileID")
	intCampaignID = request("intCampaignID")
	
	strSQL = "select intID, vchItemName from " & STR_TABLE_INVENTORY & " where intParentId=1"
	OpenConn
	set rsFixtures = gobjConn.execute(strSQL)
	output = "["
	count = 0 
	while not rsFixtures.EOF
		if count <> 0 then
			output= output & ","
		end if
		strSQL = "select (select count(intParentID) from " & STR_TABLE_INVENTORY & " I right join " & STR_TABLE_PROFILE_PACKAGES & " PP on PP.intItemID = I.intID where I.intParentID = " & rsFixtures("intID") & " and PP.intProfileID = " & intProfileID & " and PP.intCampaignID = " & intCampaignID & " ) as denominator,"
		strSQL = strSQL & " (select count(intParentID) from " & STR_TABLE_INVENTORY & " I right join " & STR_TABLE_PROFILE_STORE_PACKAGE & " SP on SP.intItemID = I.intID where I.intParentID = " & rsFixtures("intID") & " and SP.intLayoutID = " & intLayoutID & " and SP.intProfileID = " & intProfileID & " and SP.intCampaignID = " & intCampaignID & ") as numerator"
		
		set rsCounts = gobjConn.execute(strSQL)
		intDenominator = rsCounts("denominator")
		if rsCounts("numerator") > rsCounts("denominator") and rsFixtures("intID") <> "2" then
			intDenominator = rsCounts("numerator")
		end if
		
		output = output & "{""vchFixtureName"": """ & rsFixtures("vchItemName") & """, ""intNumerator"":" & rsCounts("numerator") & ", ""intDenominator"":" & intDenominator & "}"
		count = count + 1
		rsFixtures.MoveNext
	wend
	output = output & "]"
	
	response.write output
	

end sub

sub CountsAndAll
	dim output, count, itemCount, intLayoutID, intProfileID, intCampaignID, rsFixtures, rsCounts, rsItems
	
	intCampaignID = request("intCampaignID")
	intProfileID = request("intProfileID")
	
	strSQL = "select intID, vchItemName from " & STR_TABLE_INVENTORY & " where intParentId=1"
	OpenConn
	set rsFixtures = gobjConn.execute(strSQL)
	output = "["
	count = 0 
	while not rsFixtures.EOF
		if count <> 0 then
			output = output & ","
		end if
		
		strSQL = "select (select count(intParentID) from " & STR_TABLE_INVENTORY & " I right join " & STR_TABLE_PROFILE_PACKAGES & " PP on PP.intItemID = I.intID where I.intParentID = " & rsFixtures("intID") & " and PP.intProfileID = " & intProfileID & " and PP.intCampaignID = " & intCampaignID & " ) as numerator,"
		strSQL = strSQL & " (select [" & replace(rsFixtures("vchItemName"), " ", "") & "] from " & STR_TABLE_PROFILES & " where  intID = " & intProfileID & " ) as denominator"
		
		set rsCounts = gobjConn.execute(strSQL)
		
		output = output & "{""vchFixtureName"": """ & rsFixtures("vchItemName") & """, ""intNumerator"":" & rsCounts("numerator") & ", ""intDenominator"":" & rsCounts("denominator") & ", ""Items"": ["
		
		strSQL = "select I.vchItemName as Item "
		strSQL = strSQL & "from " & STR_TABLE_INVENTORY & " I "
		strSQL = strSQL & "left join " & STR_TABLE_PROFILE_PACKAGES & " PP on I.intID = PP.intItemID "
		strSQL = strSQL & "where I.chrType = 'I' and PP.intCampaignID = " & intCampaignID & " and PP.intProfileID = " & intProfileID & " and I.intParentID = " & rsFixtures("intID")
		set rsItems = gobjConn.execute(strSQL)
		if not rsItems.EOF then
			itemCount = 1
			output = output & "{"
			while not rsItems.EOF
				if itemCount <> 1 then
					output= output & ","
				end if
				output = output & """Item" & itemCount & """: """ & rsItems("Item") & """"
				rsItems.MoveNext
				itemCount = itemCount + 1
			wend
			output = output & "}"
		end if 
		
		output = output & "]}"
		count = count + 1
		rsFixtures.MoveNext
	wend
	output = output & "]"
	
	response.write output
	
end sub 


sub loadOverview

	dim intCampaignID, intLayoutID, rsTemp, output, itemID

	output = ""
	intCampaignID = request("campaignID")
	intLayoutID = request("intLayoutID")
	OpenConn
	strSQL = "select LS.vchSlotName, case when SP.intItemID is null then (select vchItemName from " & STR_TABLE_INVENTORY & " where intID =  LS.intItemID) else (select vchItemName from " & STR_TABLE_INVENTORY & " where intID =  SP.intItemID) end as ItemName, SP.intItemID as ItemID, case when SP.intItemID is not null then (select vchNumSides from " & STR_TABLE_INVENTORY & " where intID = SP.intItemID) else null end as vchSides from " & STR_TABLE_LAYOUTS_SLOTS & " LS left join " & STR_TABLE_PROFILE_STORE_PACKAGE & " SP ON LS.intID = SP.intSlotId and SP.intCampaignID = " & intCampaignID & " and SP.intProfileID = " & intLayoutID & " where LS.intLayoutID = " & intLayoutID
	'response.write strSQL
	'set rsTemp = gobjConn.execute(strSQL)
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()
	response.end

end sub

sub storeLoadOverview
	dim rsTemp

    strsql =          "SELECT PP.intitemid, PP.vchslotname "
    strsql = strsql & "FROM " & STR_TABLE_PROFILE_PACKAGES & " AS PP "
    strsql = strsql & "INNER JOIN " & STR_TABLE_PROFILE_MAPPER & " AS PM ON PP.intprofileid = PM.intprofileid "
    strsql = strsql & "INNER JOIN " & STR_TABLE_INVENTORY & " AS I ON PP.intitemid = I.intid "
    strsql = strsql & "WHERE PP.intcampaignid = " & request("campaignid") & " " 
    strsql = strsql & "AND PM.intstoreshopperid = " & request("storeid") & " " 
    strsql = strsql & "AND PP.intprofileid = " & request("profileid") & " " 
    
    ' response.write strSQL
    ' response.end
    OpenConn
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()
	response.end
end sub

sub loadItemSlot

	dim intLayoutID, vchItemSlotName, intCampaignID, intProfileID
	
	intLayoutID = request("layoutID")
	vchItemSlotName = request("slotName")
	intCampaignID = request("campaignID")
	intProfileID = request("profileID")
	OpenConn
	strSQL = "select F.vchItemName as FixtureName, F.vchImageURL as FixtureImage, F.vchOptionList1, F.vchOptionList2,"
	strSQL = strSQL & "F.vchOptionList3, F.vchOptionList4, I.vchItemName as ItemName, '" & virtualbase & "' as vbase, "
	strSQL = strSQL & " CASE WHEN I.[chrStatus]='A' OR I.[chrStatus]='P' THEN I.vchImageURL ELSE NULL END AS ItemImage, "
	strSQL = strSQL & " CASE WHEN I.[chrStatus]='A' OR I.[chrStatus]='P' THEN I.vchImageURL2 ELSE NULL END AS Thumbnail, "
	strSQL = strSQL & " I.vchSize as vchSize, I.vchNumSides as NumSides, I.vchSubstrate as substrate, I.vchColorProcess, I.[vchColorProcess], LS.[vchslotname] "
	strSQL = strSQL & "from " & STR_TABLE_PROFILE_STORE_PACKAGE & " SP left join " & STR_TABLE_INVENTORY & " I on I.intID = SP.intItemID left join "
	strSQL = strSQL & STR_TABLE_INVENTORY & " F on F.intID = I.intParentID left join " & STR_TABLE_LAYOUTS_SLOTS & " LS on LS.intID = SP.intSlotID "
	strSQL = strSQL & "where SP.intLayoutID = " & intLayoutID & " and LS.vchSlotName = '" & vchItemSlotName & "' and SP.intCampaignID =" & intCampaignID
	'response.write strSQL
    'response.end
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()
	response.end

end sub

sub saveItemSlot

	dim intCampaignID, intLayoutID, vchItemSlotName, intProfileID, vchItemName, intOldItemID, intItemID, vchNumSides, vchsize
	
	intCampaignID = request("campaignID")
	intLayoutID = request("layoutID")
	vchItemSlotName = request("slotName")
	intProfileID = request("profileID")
	vchItemName = request("vchItemName")
	intOldItemID = request("itemID")
	vchNumSides = request("vchNumSides")
    vchSize = request("vchSize")
	
	OpenConn()
	
	intItemID = CreateInventoryItem(vchItemName, intCampaignID, intLayoutID, vchItemSlotName, intOldItemID, vchNumSides, vchSize)
	if intOldItemID = "null" then
		strSQL = "insert into " & STR_TABLE_PROFILE_PACKAGES & " (intCampaignID, intProfileID, intItemID, vchSlotName) values (" & intCampaignID & ", " & intProfileID & ", " & intItemID & ", '" & vchItemSlotName & "')"
		gobjConn.execute(strSQL)
	end if
	
	CloseConn()
	response.end

end sub



function CreateInventoryItem(vchItemName, intCampaignID, intLayoutID,vchItemSlotName,intOldItemID, vchNumSides, vchSize)
	dim arrFixtureInitials, strFixtureInitials, strCampaignName, vchImage, intInvID, strSKU, rsTemp, dctSaveList, intFixtureId, x
    strSKU = ""
	strFixtureInitials = ""

    strSQL = "SELECT vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intCampaignID
    'response.write strSQL
	set rsTemp = gobjConn.execute(strSQL)
    strCampaignName =  rsTemp(0) & ""

    strSQL = "SELECT I.vchItemName, I.vchImageURL, I.intID FROM " & STR_TABLE_INVENTORY & " I WHERE I.intID= ( select LS.intItemID from " & STR_TABLE_LAYOUTS_SLOTS & " LS where LS.intLayoutID = " & intLayoutID & " and vchSlotName = '" & vchItemSlotName & "')"
    'response.write strSQL
	set rsTemp = gobjConn.execute(strSQL)
	
	vchImage = rsTemp("vchImageURL")
	intFixtureId = rsTemp("intID")
	arrFixtureInitials = split(rsTemp("vchItemName")&"", " ")
	
	For Each x In arrFixtureInitials
		strFixtureInitials = strFixtureInitials & left(x,1)
	Next
	
    strSKU = strFixtureInitials & "_" & vchItemName & "_" & strCampaignName
	'response.write "<br />" &strSKU
	
	if intOldItemID <>"null" then
	
		strSQL = "update " & STR_TABLE_INVENTORY & " set dtmUpdated = GETDATE(), vchUpdatedByUser = '" & Session(SESSION_MERCHANT_ULOGIN) & "', vchUpdatedByIP = '" & gstrUserIP & "', vchItemName = '" & SQLEncode(vchItemName) & "', vchNumSides = '" & vchNumSides & "', vchPartNumber = '" & strSKU & "', vchSize = '" & vchSize & "' where intID = " & intOldItemID
		'response.write strSQL
		gobjConn.execute(strSQL)
	
		intInvID = intOldItemID
	
	else
	
		strSQL = "SELECT intID, vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE vchPartNumber='" & strSKU & "'"

		set rsTemp = gobjConn.execute(strSQL)
		
		'response.end
		if rsTemp.EOF then
			set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "*@dtmCreated", "GETDATE()"
			dctSaveList.Add "@dtmUpdated", "GETDATE()"
			dctSaveList.Add "*vchCreatedByUser", Session(SESSION_MERCHANT_ULOGIN)
			dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
			dctSaveList.Add "*vchCreatedByIP", gstrUserIP
			dctSaveList.Add "vchUpdatedByIP", gstrUserIP
			dctSaveList.Add "chrType", "I"
			dctSaveList.Add "*chrStatus", "A"
			dctSaveList.Add "*#intParentID", intFixtureId
			dctSaveList.Add "*#intSortOrder", 0
			dctSaveList.Add "*#intHitCount", 0
			dctSaveList.Add "vchItemName", vchItemName
			dctSaveList.Add "vchSize", vchSize
			dctSaveList.Add "vchNumSides", Request("vchNumSides")
			dctSaveList.Add "vchSubstrate", Request("vchSubstrate")
			dctSaveList.Add "vchPartNumber", strSKU
			dctSaveList.Add "#mnyItemPrice", "0"
			dctSaveList.Add "chrICFlag", "N"
			dctSaveList.Add "chrTaxFlag", "N"
			dctSaveList.Add "chrSoftFlag", "N"
			dctSaveList.Add "!#intStock", "0"
			dctSaveList.Add "!txtDescription", ""
			intInvID = SaveDataRecord(STR_TABLE_INVENTORY, Request, intInvID, dctSaveList)
		else 
			intInvID = rsTemp("intID")
		end if
	end if
	CreateInventoryItem = intInvID
end function

sub MailerDetails

	dim recno, strSql, cmd, arParams
	
	recno = request("recno")
	
	strSQL = "select [full name] from " & mainTable & "  where [recno] = " & recno
	
	OpenConn
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()

end sub

sub OpenConn()
	if not gblnConnOpen then
		set gobjConn = Server.CreateObject("ADODB.Connection")
		gobjConn.Open gstrConnectString
		gblnConnOpen = true
	end if
end sub

sub CloseConn()
	if gblnConnOpen and IsObject(gobjConn) then
		gobjConn.Close
		set gobjConn = nothing
		gblnConnOpen = false
	end if
end sub

sub ConnExecute(strSQL)
	OpenConn
	'response.write strsql
	gobjConn.execute(strSQL)
end sub

function ConnOpenRS(strSQL)
	OpenConn
	set ConnOpenRS = gobjConn.execute(strSQL)
end function

function formatDate(dateToFormat)
	dim newDate, splitDate
	
	splitDate = Split(dateToFormat, "-")
	newDate = splitDate(1) & "-" & splitDate(2) & "-" & splitDate(0)
	
	formatDate = newDate
end function

Function URLDecode(sConvert)
    Dim aSplit
    Dim sOutput
    Dim I
    If IsNull(sConvert) Then
       URLDecode = ""
       Exit Function
    End If

    ' convert all pluses to spaces
    sOutput = REPLACE(sConvert, "+", " ")

    ' next convert %hexdigits to the character
    aSplit = Split(sOutput, "%")

    If IsArray(aSplit) Then
      sOutput = aSplit(0)
      For I = 0 to UBound(aSplit) - 1
        sOutput = sOutput & _
          Chr("&H" & Left(aSplit(i + 1), 2)) &_
          Right(aSplit(i + 1), Len(aSplit(i + 1)) - 2)
      Next
    End If

    URLDecode = sOutput
End Function

' -------------- SQLEncode -----------------
' Replaces Single Quotes with 2 Single Quotes in Input String
Function SQLEncode(strInput)
	SQLEncode = Replace(strInput,"'","''")
End Function
%>