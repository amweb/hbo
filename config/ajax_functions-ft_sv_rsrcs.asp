<%@ LANGUAGE="VBScript" %>
<!--#include file="incInit.asp"-->
<!--#include file="Json_2.0.4.asp"-->
<%
Response.ContentType = "application/json; charset=utf-8"

dim reqAction
dim strSQL, cmd, arParams, col

reqAction = request("action")

select case reqAction
	case "filetrackerLoadProfilesAfterCampaign"
		filetrackerLoadProfilesAfterCampaign
	case "loadProfilesFTLanding"
		loadProfilesFTLanding
	case "uploadThumbnailForFileTracker"
		uploadThumbnailForFileTracker
	case "approveThumbnail"
		approveThumbnail
	case "rejectThumbnail"
		rejectThumbnail
	case "printArtwork"
		printArtwork
	case "loadItemsIntoResourceCenter"
		loadItemsIntoResourceCenter
	case "getAvailableCategories"
		getAvailableCategories
	case "updateSelectedCategory"
		updateSelectedCategory
	case "deleteSelectedCategory"
		deleteSelectedCategory
	case "createNewCategory"
		createNewCategory
	case "getThumbnailImage"
		getThumbnailImage
	case "uploadNewFileToRSRC"
		uploadNewFileToRSRC
	case "getItemsForAdmin"
		getItemsForAdmin
	case "getItemInfo"
		getItemInfo
	case "updateInventoryItem"
		updateInventoryItem
	case "deleteExistingItem"
		deleteExistingItem
	case "getItemsNeedingAttention"
		getItemsNeedingAttention
end select

Function QueryToJSON(dbcomm, params)
        Dim rs, jsa
        Set rs = dbcomm.Execute(,params,1)
        Set jsa = jsArray()
        Do While Not (rs.EOF Or rs.BOF)
                Set jsa(Null) = jsObject()
                For Each col In rs.Fields
                        jsa(Null)(col.Name) = col.Value
                Next
        rs.MoveNext
        Loop
        Set QueryToJSON = jsa
        rs.Close
End Function

sub filetrackerLoadProfilesAfterCampaign
	dim intRequestedCampaign, rsProfiles, cmd
	intRequestedCampaign = request("campaignid")
	OpenConn
	
	strSQL = "SELECT [intID] FROM " & STR_TABLE_PROFILES & " WHERE [chrStatus]='A' "
	strSQL = strSQL & "AND [intID] IN (SELECT [intProfileID] FROM " & STR_TABLE_PROFILE_PACKAGES & " WHERE [intCampaignID] = " & intRequestedCampaign & " )"
	set rsProfiles = gobjconn.execute(strSQL)
	
	if not rsProfiles.eof then
		strSQL = "SELECT [intID],[vchProfileName] FROM " & STR_TABLE_PROFILES & " WHERE [chrStatus]='A' "
		strSQL = strSQL & "AND [intID] IN (SELECT [intProfileID] FROM " & STR_TABLE_PROFILE_PACKAGES & " WHERE [intCampaignID] = " & intRequestedCampaign & " )"
		Set cmd = Server.CreateObject("ADODB.Command")
		cmd.CommandText = strSQL
		arParams = array("")
		Set cmd.ActiveConnection = gobjConn
		QueryToJSON(cmd, arParams).Flush
	end if
	response.end
	
	CloseConn

end sub

sub loadProfilesFTLanding
	dim rsProfiles, intCampaignID
	
	intCampaignID = request("campaign")
	
	'strSQL = "SELECT [intID] FROM " & STR_TABLE_PROFILES & " WHERE [chrStatus]='A' "
	'strSQL = strSQL & "AND [intID] IN (SELECT [intProfileID] FROM " & STR_TABLE_PROFILE_PACKAGES & " WHERE [intCampaignID] = " & intCampaignID & " )"
	strSQL = "SELECT [intID],[vchProfileName] "
	strSQL = strSQL & "FROM " & STR_TABLE_PROFILES & " WHERE [chrStatus]='A' "
	strSQL = strSQL & "AND [intID] IN (SELECT [intProfileID] FROM " & STR_TABLE_PROFILE_PACKAGES & " WHERE [intCampaignID] = " & intCampaignID & " )"
	
	'set rsProfiles = gobjconn.execute(strSQL)
	OpenConn()
	Set rsProfiles = Server.CreateObject("ADODB.Command")
	rsProfiles.CommandText = strSQL
	arParams = array("")
	Set rsProfiles.ActiveConnection = gobjConn
	QueryToJSON(rsProfiles, arParams).Flush
	CloseConn()

end sub

sub uploadThumbnailForFileTracker
	dim dirImageUpload, objUpload, strUpdateSQL, strThumbnailName, strFileUploadName
	OpenConn
	
	Set objUpload = Server.CreateObject("SoftArtisans.FileUp")
	
	dirImageUpload = Server.MapPath(virtualbase & "images")
	objUpload.Path = dirImageUpload
	strFileUploadName = dirImageUpload & "\" & objUpload.Form("Thumbnail").UserFilename
	'response.write dirImageUpload & "\" & objUpload.Form("Thumbnail").UserFilename
	
	if not isNumeric(objUpload.Form("id")) then
		Err.Raise 500
	end if
	strUpdateSQL = "UPDATE " & STR_TABLE_INVENTORY & " SET [vchUpdatedByUser]='" & gintuserid & "',[dtmUpdated]=GETDATE(),[chrStatus]='U',[vchImageURL]='" & objUpload.Form("Thumbnail").UserFilename & "'"	
	
	
	'response.write strThumbnailName
	objUpload.Form("Thumbnail").SaveAs strFileUploadName
	strThumbnailName = SetImageReSize(dirImageUpload & "\", objUpload.Form("Thumbnail").UserFilename)
	strUpdateSQL =  strUpdateSQL & ",[vchImageURL2]='" & strThumbnailName & "'"
	strUpdateSQL = strUpdateSQL & " WHERE [intID]=" & objUpload.Form("id")
	
	
	gobjConn.execute(strUpdateSQL)
	
	
	CloseConn
end sub

sub approveThumbnail
	dim strSQL
	OpenConn
	strSQL = "UPDATE " & STR_TABLE_INVENTORY & " SET [chrStatus]='A',[dtmUpdated]=GETDATE(),[vchUpdatedByUser]=" & gintuserid & " WHERE [intID]=" & request("itemid")
	'response.write strSQL
	'response.end
	gobjConn.execute(strSQL)
	CloseConn
end sub

sub rejectThumbnail
	dim strSQL
	OpenConn
	strSQL = "UPDATE " & STR_TABLE_INVENTORY & " SET [chrStatus]='R',[dtmUpdated]=GETDATE(),[vchUpdatedByUser]=" & gintuserid & ",[vchImageURL]=NULL,[vchImageURL2]=NULL WHERE [intID]=" & request("itemid")
	gobjConn.execute(strSQL)
	CloseConn
end sub

sub printArtwork
	dim strSQL
	OpenConn
	strSQL = "UPDATE " & STR_TABLE_INVENTORY & " SET [chrStatus]='P',[dtmUpdated]=GETDATE(),[vchUpdatedByUser]=" & gintuserid & " WHERE [intID]=" & request("itemid")
	'response.write strSQL
	'response.end
	gobjConn.execute(strSQL)
	CloseConn
end sub

sub loadItemsIntoResourceCenter
	dim strSQL
	strSQL = "SELECT '" & virtualbase & "resources/' AS [base], '" & virtualbase & "resources/thumbnails" & "' AS [thumbbase], [vchItemName], [intID], [vchURL] FROM " & STR_TABLE_RESOURCES & " WHERE [chrStatus]='A' AND [chrType]='I' AND [intParentID]=" & request("parent")
	'response.write strSQL
	OpenConn()
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()
	response.end
end sub

sub getAvailableCategories
	dim strSQL
	strSQL = "SELECT [intID],[vchItemName] FROM " & STR_TABLE_RESOURCES & " WHERE [chrType]='C' AND [chrStatus]='A'"
	OpenConn()
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()
	response.end
end sub

sub updateSelectedCategory
	dim strSQL
	OpenConn()
	strSQL = "UPDATE " & STR_TABLE_RESOURCES & " SET [vchItemName]='" & request("categoryname") & "', [chrStatus]='" & request("active") & "' WHERE [intID]=" & request("id")
	gobjConn.execute(strSQL)
	CloseConn()
	response.end
end sub

sub deleteSelectedCategory
	dim strSQL
	OpenConn()
	strSQL = "UPDATE " & STR_TABLE_RESOURCES & " SET [chrStatus]='D' WHERE [intID]=" & request("id")
	gobjConn.execute(strSQL)
	CloseConn()
	response.end
end sub

sub createNewCategory
	dim strSQL
	OpenConn()
	strSQL = "INSERT INTO " & STR_TABLE_RESOURCES & " ([vchItemName],[chrType],[chrStatus]) VALUES ('" & ProtectSQL(request("newcategoryname")) & "', 'C', 'A')"
	gobjConn.execute(strSQL)
	CloseConn()
	response.end
end sub

sub getThumbnailImage
	dim strSQL, rsImage, strJSONResponse
	OpenConn()
	strSQL = "SELECT [vchThumbnailURL], '" & virtualbase & "resources/thumbnails" & "' AS [thumbbase], '" & virtualbase & "resources' AS [imgbase], [vchURL], [vchMIMEType],"
	strSQL = strSQL & "[vchItemName] FROM " & STR_TABLE_RESOURCES & " WHERE [intID]=" & ProtectSQL(request("id"))
	set rsImage = gobjConn.execute(strSQL)
	strJSONResponse = "{""vchThumbnailURL"": """
	if(rsImage("vchThumbnailURL"))&""<>"" then
		strJSONResponse = strJSONResponse & rsImage("thumbbase") & "/" & rsImage("vchThumbnailURL")
	else
		strJSONResponse = strJSONResponse & "false"
	end if
	strJSONResponse = strJSONResponse & """, ""vchURL"": """
	'strJSONResponse = "{""vchURL"": """
	if(rsImage("vchURL"))&""<>"" then
		strJSONResponse = strJSONResponse & rsImage("imgbase") & "/" & rsImage("vchURL")
	else
		strJSONResponse = strJSONResponse & "false"
	end if
	strJSONResponse = strJSONResponse & """, ""vchMIMEType"": "
	if(rsImage("vchMIMEType"))&""<>"" then
		strJSONResponse = strJSONResponse & """" & rsImage("vchMIMEType") & """"
	else
		strJSONResponse = strJSONResponse & "false"
	end if
	strJSONResponse = strJSONResponse & ", ""alt"": """ & rsImage("vchItemName") & """"
	strJSONResponse = strJSONResponse & "}"
	response.write strJSONResponse
	'while not rsImage.eof
	'	"{""vchThumbnailURL"": """ & rsImage("vchThumbnailURL") & """}"
	'	rsImage.movenext()
	'wend
	CloseConn()
	response.end
end sub

sub uploadNewFileToRSRC
	dim dirImageUpload, objUpload, strThumbnailName, strFileUploadName, strThumbnailUploadName, strSQL
	OpenConn
	
	Set objUpload = Server.CreateObject("SoftArtisans.FileUp")
	dirImageUpload = Server.MapPath(virtualbase & "resources")
	
	objUpload.Path = dirImageUpload
	strFileUploadName = dirImageUpload & "\" & objUpload.Form("filename").UserFilename
	strThumbnailUploadName = dirImageUpload & "\thumbnails\" & objUpload.Form("thumbnail").UserFilename
	
	
	
	objUpload.Form("thumbnail").SaveAs strThumbnailUploadName
	objUpload.Form("filename").SaveAs strFileUploadName
	
	strSQL = "INSERT INTO " & STR_TABLE_RESOURCES & " ([chrType], [chrStatus], [vchItemName], [intParentID], [vchURL], [vchThumbnailURL], [vchMIMEType]) "
	strSQL = strSQL & "VALUES ('I', 'A', '" & ProtectSQL(objUpload.Form("colloquialname")) & "', " & objUpload.Form("intCategory") & ", '" & objUpload.Form("filename").UserFilename
	strSQL = strSQL & "', '" & objUpload.Form("thumbnail").UserFilename & "', '" & objUpload.Form("mimetype") & "' )"
	'response.write strSQL
	gobjConn.execute(strSQL)
	
	set objUpload = Nothing
	
	CloseConn()
	response.end
end sub

sub getItemsForAdmin
	dim strSQL, rsRSRCCategories, strJSONResponse, itemCount, count
	dim strGetItemsSQL, rsItemsInCategory
	OpenConn
	'dim strChildSQL, rsRSRCIndividualItems, rsRSRCChildCount
	strSQL = "SELECT [intID], [vchItemName] FROM " & STR_TABLE_RESOURCES & " WHERE [chrType]='C' AND [chrStatus]='A'"
	set rsRSRCCategories = gobjconn.execute(strSQL)
	
	count = 0
	strJSONResponse = "["
	while not rsRSRCCategories.eof
		if count <> 0 then
			strJSONResponse = strJSONResponse & ","
		end if
		strJSONResponse = strJSONResponse & "{""category"": """ & rsRSRCCategories("vchItemName") & """, ""ID"": " & rsRSRCCategories("intID") & ", ""Items"": ["
		strGetItemsSQL = "SELECT [intID], '" & virtualbase & "' AS [vbase], [vchItemName], [vchThumbnailURL], [vchMIMEType] FROM " & STR_TABLE_RESOURCES & " WHERE [chrType]='I' AND [chrStatus]='A' AND [intParentID]=" & rsRSRCCategories("intID")
		set rsItemsInCategory = gobjConn.execute(strGetItemsSQL)
		if not rsItemsInCategory.EOF then
			itemCount = 1
			while not rsItemsInCategory.EOF
				if itemCount <> 1 then
					strJSONResponse = strJSONResponse & ","
				end if
				
				strJSONResponse = strJSONResponse & "{""Name"": """ & rsItemsInCategory("vchItemName") & """, "
				strJSONResponse = strJSONResponse & """ID"": " & rsItemsInCategory("intID") & ","
				strJSONResponse = strJSONResponse & """Thumbnail"": """ & rsItemsInCategory("vchThumbnailURL") & ""","
				strJSONResponse = strJSONResponse & """virtualbase"": """ & rsItemsInCategory("vbase") & """"
				strJSONResponse = strJSONResponse & ", ""MIME"": """ & rsItemsInCategory("vchMIMEType") & """" & "}"
				
				rsItemsInCategory.MoveNext
				itemCount = itemCount + 1
			wend
			
		end if
		
		strJSONResponse = strJSONResponse & "]}"
		count = count + 1
		rsRSRCCategories.movenext()
	wend
	strJSONResponse = strJSONResponse & "]"
	response.write strJSONResponse
	
	CloseConn()
	response.end
end sub

sub getItemInfo
	dim strSQL
	strSQL = "SELECT [vchItemName] FROM " & STR_TABLE_RESOURCES & " WHERE [intID]=" & request("id")
	OpenConn()
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()
	response.end
end sub

sub updateInventoryItem
	OpenConn()
	dim strSQL
	strSQL = "UPDATE " & STR_TABLE_RESOURCES & " SET [vchItemName]='" & request("itemname") & "', [chrStatus]='" & request("active")
	strSQL = strSQL & "', [intParentID]=" & request("changecategory") & " WHERE [intID]=" & request("itemid")
	gobjConn.execute(strSQL)
	CloseConn()
end sub

sub deleteExistingItem
	OpenConn()
	dim strSQL
	strSQL = "UPDATE " & STR_TABLE_RESOURCES & " SET [chrStatus]='D' WHERE [intID]=" & request("id")
	gobjConn.execute(strSQL)
	CloseConn()
end sub

sub getItemsNeedingAttention
	OpenConn()
	dim strJSONResponse, itemCount, count
	dim strSQL
	dim rsDecategorized, rsUnassigned, rsRecategorizing, rsDeactivatedCategories
	
	strJSONResponse = "["
	'Items in categories that have been disabled, or not belonging to a category
	strJSONResponse = strJSONResponse & "{"
	strJSONResponse = strJSONResponse & """Title"": ""Uncategorized items"","
	strJSONResponse = strJSONResponse & """Shorthand"": ""category-unassigned"","
	strJSONResponse = strJSONResponse & """Items"": ["
	strSQL = "SELECT [vchItemName],[intID] FROM " & STR_TABLE_RESOURCES & " WHERE [chrType]='I' AND [chrStatus]='A' AND [intParentID] NOT IN "
	strSQL = strSQL & " (SELECT DISTINCT [intID] FROM " & STR_TABLE_RESOURCES & " WHERE [chrType]='C' AND [chrStatus]='A')"
	set rsDecategorized = gobjconn.execute(strSQL)
	if not rsDecategorized.eof then
		itemCount = 1
		while not rsDecategorized.eof
			if itemCount <> 1 then
				strJSONResponse = strJSONResponse & ","
			end if
			strJSONResponse = strJSONResponse & "{""ID"": " & rsDecategorized("intID") & ", ""Name"": """ & rsDecategorized("vchItemName") & """}"
			
			
			rsDecategorized.movenext()
			itemCount = itemCount + 1
		wend
	end if
	set itemCount = Nothing
	strJSONResponse = strJSONResponse & "]},"
	strJSONResponse = strJSONResponse & "{"
	strJSONResponse = strJSONResponse & """Title"": ""Decategorized items"","
	strJSONResponse = strJSONResponse & """Shorthand"": ""category-decategorized"","
	strJSONResponse = strJSONResponse & """Items"": ["
	
	'Items that are active, but have no assigned category
	'strSQL = "SELECT [intID],[vchItemName] FROM " & STR_TABLE_RESOURCES & " WHERE [chrType]='I' AND [chrStatus]='A' AND [intParentID] IS NULL "
	set rsUnassigned = gobjconn.execute("SELECT [intID],[vchItemName] FROM " & STR_TABLE_RESOURCES & " WHERE [chrType]='I' AND [chrStatus]='A' AND [intParentID] IS NULL")
	
	if not rsUnassigned.eof then
		itemCount = 1
		while not rsUnassigned.eof
			if itemCount <> 1 then
				strJSONResponse = strJSONResponse & ","
			end if
			strJSONResponse = strJSONResponse & "{""ID"": " & rsUnassigned("intID") & ", ""Name"": """ & rsUnassigned("vchItemName") & """}"
			
			rsUnassigned.movenext()
			itemCount = itemCount + 1
		wend
	end if
	
	set itemCount = Nothing
	strJSONResponse = strJSONResponse & "]},"
	strJSONResponse = strJSONResponse & "{"
	strJSONResponse = strJSONResponse & """Title"": ""Items assigned to deactivated categories"","
	strJSONResponse = strJSONResponse & """Shorthand"": ""category-needs-recategorizing"","
	strJSONResponse = strJSONResponse & """Items"": ["
	' items assigned to categories that have been deactivated
	strSQL = "SELECT * FROM " & STR_TABLE_RESOURCES & " WHERE [chrType]='I' AND [chrStatus]='A' AND [intParentID] IN "
	strSQL = strSQL & " (SELECT DISTINCT [intID] FROM " & STR_TABLE_RESOURCES & " WHERE [chrType]='C' AND [chrStatus]<>'A')"
	set rsRecategorizing = gobjconn.execute(strSQL)
	
	if not rsRecategorizing.eof then
		itemCount = 1
		while not rsRecategorizing.eof
			if itemCount <> 1 then
				strJSONResponse = strJSONResponse & ","
			end if
			strJSONResponse = strJSONResponse & "{""ID"": " & rsRecategorizing("intID") & ", ""Name"": """ & rsRecategorizing("vchItemName") & """}"
			
		
			rsRecategorizing.movenext()
			itemCount = itemCount + 1
		wend
	end if
	strJSONResponse = strJSONResponse & "]"
	
	set itemCount = Nothing
	strJSONResponse = strJSONResponse & "},"
	strJSONResponse = strJSONResponse & "{"
	strJSONResponse = strJSONResponse & """Title"": ""Disabled or deleted items"","
	strJSONResponse = strJSONResponse & """Shorthand"": ""category-disabled-items"","
	strJSONResponse = strJSONResponse & """Items"": ["
	' items assigned to categories that have been deactivated
	set rsRecategorizing = gobjconn.execute("SELECT [intID],[vchItemName] FROM " & STR_TABLE_RESOURCES & " WHERE [chrType]='I' AND [chrStatus]<>'A'")
	
	if not rsRecategorizing.eof then
		itemCount = 1
		while not rsRecategorizing.eof
			if itemCount <> 1 then
				strJSONResponse = strJSONResponse & ","
			end if
			strJSONResponse = strJSONResponse & "{""ID"": " & rsRecategorizing("intID") & ", ""Name"": """ & rsRecategorizing("vchItemName") & """}"
			
		
			rsRecategorizing.movenext()
			itemCount = itemCount + 1
		wend
	end if
	strJSONResponse = strJSONResponse & "]"
	
	set itemCount = Nothing
	strJSONResponse = strJSONResponse & "}"
	'strJSONResponse = strJSONResponse & ",{"
	'strJSONResponse = strJSONResponse & """Title"": ""Disabled or deleted items"","
	'strJSONResponse = strJSONResponse & """Shorthand"": ""category-disabled-items"","
	'strJSONResponse = strJSONResponse & """Items"": ["
	'set rsDeactivatedCategories = gobjconn.execute("SELECT [vchItemName], [intID] FROM " & STR_TABLE_RESOURCES & " WHERE [chrType]='C' AND [chrStatus]<>'A'")
	
	'if not rsDeactivatedCategories.eof then
	'	itemCount = 1
	'	while not rsDeactivatedCategories.eof
	'		strJSONResponse = strJSONResponse & "{""ID"": " & rsDeactivatedCategories("intID") & ", ""Name"": """ & rsDeactivatedCategories("vchItemName") & """}"
		
	'		rsDeactivatedCategories.movenext()
	'		itemCount = itemCount + 1
	'	wend
	'end if
	strJSONResponse = strJSONResponse & "]"
	
	
	'strJSONResponse = strJSONResponse & "}]"
	response.write strJSONResponse
	response.end
	CloseConn()
end sub


function SetImageReSize( uploadPath, fImage)

	'dim fImage, uploadPath, IsImageGood
	'IsImageGood = false
	'fImage = RemoveSpaces("Brand" & intID & "." & Mid(objUpload.form("ImageFile").UserFilename, InstrRev(objUpload.form("ImageFile").UserFilename, "\") + 1))
	'uploadPath = Server.MapPath(brandimagebase) & "\"

	if IsNull(fImage) or fImage = "" then
		SetImageReSize = false
	else
		' resize image here
		dim objImageMagick, strThumbnailName, tmp
		set objImageMagick = CreateObject("ImageMagickObject.MagickImage.1")

		' 1st pass resize category image
		' dnloadPath1 = Server.MapPath(categoryimagebase) & "\"
		'objExec.Application = "c:\com\jdo.exe -ss -q 75 -rh 100 -rw 100 -rb -op " & uploadpath & fImage

		dim intSplitNameCount, x
		strThumbnailName = ""
		
		for intSplitNameCount=0 To UBound(Split(fImage, ".")) - 1
			strThumbnailName = strThumbnailName & Split(fImage, ".")(intSplitNameCount)
			intSplitNameCount = intSplitNameCount + 1
		next
		strThumbnailName = strThumbnailName & "_thumb."
		strThumbnailName = strThumbnailName & Split(fImage, ".")(UBound(Split(fImage, ".")))
		
		tmp = objImageMagick.Convert(uploadpath & fImage, "-geometry", "x76", uploadPath & strThumbnailName)
		'uploadpath & fImage & " -resize 25% -quality 75 " & uploadPath & strThumbnailName
		

		' IsImageGood = true
		set objImageMagick = Nothing
		SetImageReSize = strThumbnailName
	end if
end function

%>