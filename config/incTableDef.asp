<%

' =====================================================================================
' = File: incTableDef.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Database Table Definitions
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

const kintMaxTables = 10
const kintMaxFields = 60
const kintMaxArgs = 4
dim intTableDefCount
redim aryTableList(kintMaxTables, kintMaxFields, kintMaxArgs)
intTableDefCount = 0

' Format:
'   AddTableDef  "TABLE_NAME"
'   AddFieldDef "FIELD_NAME", "DATA_TYPE", "DATA_SIZE", "DEFAULT_VALUE", "NULL or NOT NULL"
'   ...
'   AddFieldDef "$CONSTRAINT", "$_PK", "PRIMARY KEY NONCLUSTERED", "intID", "90"

AddTableDef STR_TABLE_ORDER
AddFieldDef "intID", 					"int identity", "1,1",	"", "NOT NULL"
AddFieldDef "chrType",					"char", "1",			"", "NOT NULL"
AddFieldDef "chrStatus",				"char", "1",			"", "NOT NULL"
AddFieldDef "dtmCreated",				"smalldatetime", "",	"", "NOT NULL"
AddFieldDef "dtmUpdated",				"smalldatetime", "",	"", "NOT NULL"
AddFieldDef "vchCreatedByUser",			"varchar", "32",		"", "NOT NULL"
AddFieldDef "vchUpdatedByUser",			"varchar", "32",		"", "NOT NULL"
AddFieldDef "vchCreatedByIP",			"varchar", "15",		"", "NOT NULL"
AddFieldDef "vchUpdatedByIP",			"varchar", "15",		"", "NOT NULL"
AddFieldDef "vchShippingNumber",		"varchar", "32",		"", "NULL"
AddFieldDef "vchShopperIP",				"varchar", "15",		"", "NOT NULL"
AddFieldDef "vchShopperBrowser",		"varchar", "64",		"", "NULL"
AddFieldDef "dtmSubmitted",				"smalldatetime", "",	"", "NULL"
AddFieldDef "chrModifyFlag",			"char", "1",			"", "NULL"
AddFieldDef "chrPaymentMethod",			"char", "3",			"", "NULL"
AddFieldDef "vchPaymentCardType",		"varchar", "12",		"", "NULL"
AddFieldDef "vchPaymentCardName",		"varchar", "32",		"", "NULL"
AddFieldDef "vchPaymentCardNumber",		"varchar", "32",		"", "NULL"
AddFieldDef "vchPaymentCardExtended",	"varchar", "8",			"", "NULL"
AddFieldDef "chrPaymentCardExpMonth",	"char", "2",			"", "NULL"
AddFieldDef "chrPaymentCardExpYear",	"char", "4",			"", "NULL"
AddFieldDef "vchPaymentBankName",		"varchar", "32",		"", "NULL"
AddFieldDef "vchPaymentRtnNumber",		"varchar", "9",			"", "NULL"
AddFieldDef "vchPaymentCheckNumber",	"varchar", "12",		"", "NULL"
AddFieldDef "vchPaymentDLNumber",		"varchar", "32",		"", "NULL"
AddFieldDef "vchPaymentDLState",		"varchar", "2",			"", "NULL"
AddFieldDef "chrPaymentAcctType",		"char", "1",			"", "NULL"
AddFieldDef "vchPaymentAcctNumber",		"varchar", "20",		"", "NULL"
AddFieldDef "intShopperID",				"int", "",				"", "NULL"
AddFieldDef "intBillShopperID",			"int", "",				"", "NULL"
AddFieldDef "intShipShopperID",			"int", "",				"", "NULL"
AddFieldDef "intTaxZone",				"int", "",				"", "NULL"
AddFieldDef "mnyNonTaxSubtotal",		"money", "",			"", "NULL"
AddFieldDef "mnyTaxSubtotal",			"money", "",			"", "NULL"
AddFieldDef "fltTaxRate",				"real", "",				"", "NULL"
AddFieldDef "mnyTaxAmount",				"money", "",			"", "NULL"
AddFieldDef "mnyShipAmount",			"money", "",			"", "NULL"
AddFieldDef "chrShipTaxFlag",			"char", "1",			"", "NULL"
AddFieldDef "mnyGrandTotal",			"money", "",			"", "NULL"
AddFieldDef "intShipOption",			"int", "",				"", "NULL"
AddFieldDef "vchAOL_QCSessionID",		"varchar", "100",		"", "NULL"
AddFieldDef "intReferalID",				"int", "",				"", "NULL"
AddFieldDef "vchReferalName",			"varchar", "32",		"", "NULL"
AddFieldDef "fltReferalDiscount",		"real", "",				"", "NULL"
AddFieldDef "mnyDiscountAmount",		"money", "",			"", "NULL"
AddFieldDef "txtGiftMessage",			"text", "",				"", "NULL"
AddFieldDef "$CONSTRAINT", "$_PK", "PRIMARY KEY NONCLUSTERED", "intID", "90"

AddTableDef STR_TABLE_TRANS
AddFieldDef "intID", 					"int identity", "1,1",	"", "NOT NULL"
AddFieldDef "chrType",					"char", "1",			"", "NOT NULL"
AddFieldDef "chrStatus",				"char", "1",			"", "NOT NULL"
AddFieldDef "dtmCreated",				"smalldatetime", "",	"", "NOT NULL"
AddFieldDef "dtmUpdated",				"smalldatetime", "",	"", "NOT NULL"
AddFieldDef "vchCreatedByUser",			"varchar", "32",		"", "NOT NULL"
AddFieldDef "vchUpdatedByUser",			"varchar", "32",		"", "NOT NULL"
AddFieldDef "vchCreatedByIP",			"varchar", "15",		"", "NOT NULL"
AddFieldDef "vchUpdatedByIP",			"varchar", "15",		"", "NOT NULL"
AddFieldDef "intOrderID",				"int", "",				"", "NULL"
AddFieldDef "chrAccountType",			"char", "1",			"", "NOT NULL"
AddFieldDef "chrTransType",				"char", "2",			"", "NOT NULL"
AddFieldDef "vchAuthCode",				"varchar", "12",		"", "NULL"
AddFieldDef "vchAVCode",				"varchar", "12",		"", "NULL"
AddFieldDef "vchRefCode",				"varchar", "12",		"", "NULL"
AddFieldDef "vchOrderCode",				"varchar", "20",		"", "NULL"
AddFieldDef "vchDeclineCode",			"varchar", "12",		"", "NULL"
AddFieldDef "dtmTransDate",				"smalldatetime", "",	"", "NOT NULL"
AddFieldDef "mnyTransAmount",			"money", "",			"", "NOT NULL"
AddFieldDef "vchShopperMsg",			"varchar", "255",		"", "NULL"
AddFieldDef "txtComments",				"text", "",				"", "NULL"
AddFieldDef "$CONSTRAINT", "$_PK", "PRIMARY KEY NONCLUSTERED", "intID", "90"

AddTableDef STR_TABLE_SHOPPER
AddFieldDef "intID", 					"int identity", "1,1",	"", "NOT NULL"
AddFieldDef "chrType",					"char", "1",			"", "NOT NULL"
AddFieldDef "chrStatus",				"char", "1",			"", "NOT NULL"
AddFieldDef "dtmCreated",				"smalldatetime", "",	"", "NOT NULL"
AddFieldDef "dtmUpdated",				"smalldatetime", "",	"", "NOT NULL"
AddFieldDef "vchCreatedByUser",			"varchar", "32",		"", "NOT NULL"
AddFieldDef "vchUpdatedByUser",			"varchar", "32",		"", "NOT NULL"
AddFieldDef "vchCreatedByIP",			"varchar", "15",		"", "NOT NULL"
AddFieldDef "vchUpdatedByIP",			"varchar", "15",		"", "NOT NULL"
AddFieldDef "intShopperID",				"int", "",				"", "NULL"
AddFieldDef "vchLabel",					"varchar", "32",		"", "NULL"
AddFieldDef "vchAOL_ID",				"varchar", "32",		"", "NULL"
AddFieldDef "vchUsername",				"varchar", "20",		"", "NULL"
AddFieldDef "vchPassword",				"varchar", "20",		"", "NULL"
AddFieldDef "vchHint",					"varchar", "32",		"", "NULL"
AddFieldDef "vchFirstName",				"varchar", "32",		"", "NULL"
AddFieldDef "vchLastName",				"varchar", "32",		"", "NULL"
AddFieldDef "vchCompany",				"varchar", "32",		"", "NULL"
AddFieldDef "vchAddress1",				"varchar", "50",		"", "NULL"
AddFieldDef "vchAddress2",				"varchar", "50",		"", "NULL"
AddFieldDef "vchCity",					"varchar", "32",		"", "NULL"
AddFieldDef "vchState",					"varchar", "32",		"", "NULL"
AddFieldDef "vchZip",					"varchar", "12",		"", "NULL"
AddFieldDef "vchCountry",				"varchar", "3",			"", "NULL"
AddFieldDef "vchDayPhone",				"varchar", "20",		"", "NULL"
AddFieldDef "vchNightPhone",			"varchar", "20",		"", "NULL"
AddFieldDef "vchFax",					"varchar", "20",		"", "NULL"
AddFieldDef "vchEmail",					"varchar", "50",		"", "NULL"
AddFieldDef "chrPaymentMethod",			"char", "3",			"", "NULL"
AddFieldDef "vchCardType",				"varchar", "12",		"", "NULL"
AddFieldDef "vchCardNumber",			"varchar", "32",		"", "NULL"
AddFieldDef "vchCardExtended",			"varchar", "8",			"", "NULL"
AddFieldDef "chrCardExpMonth",			"char", "2",			"", "NULL"
AddFieldDef "chrCardExpYear",			"char", "4",			"", "NULL"
AddFieldDef "vchCheckBankName",			"varchar", "32",		"", "NULL"
AddFieldDef "vchCheckRtnNumber",		"varchar", "9",			"", "NULL"
AddFieldDef "vchCheckDLNumber",			"varchar", "32",		"", "NULL"
AddFieldDef "vchCheckDLState",			"varchar", "2",			"", "NULL"
AddFieldDef "chrCheckAcctType",			"varchar", "1",			"", "NULL"
AddFieldDef "vchCheckAcctNumber",		"varchar", "20",		"", "NULL"
AddFieldDef "intTaxZone",				"int", "",				"", "NULL"
AddFieldDef "$CONSTRAINT", "$_PK", "PRIMARY KEY NONCLUSTERED", "intID", "90"

AddTableDef STR_TABLE_LINEITEM
AddFieldDef "intID", 					"int identity", "1,1",	"", "NOT NULL"
AddFieldDef "chrType",					"char", "1",			"", "NOT NULL"
AddFieldDef "chrStatus",				"char", "1",			"", "NOT NULL"
AddFieldDef "dtmCreated",				"smalldatetime", "",	"", "NOT NULL"
AddFieldDef "dtmUpdated",				"smalldatetime", "",	"", "NOT NULL"
AddFieldDef "vchCreatedByUser",			"varchar", "32",		"", "NOT NULL"
AddFieldDef "vchUpdatedByUser",			"varchar", "32",		"", "NOT NULL"
AddFieldDef "vchCreatedByIP",			"varchar", "15",		"", "NOT NULL"
AddFieldDef "vchUpdatedByIP",			"varchar", "15",		"", "NOT NULL"
AddFieldDef "intOrderID",				"int", "",				"", "NULL"
AddFieldDef "intInvID",					"int", "",				"", "NULL"
AddFieldDef "vchPartNumber",			"varchar", "32",		"", "NULL"
AddFieldDef "vchItemName",				"varchar", "255",		"", "NOT NULL"
AddFieldDef "mnyUnitPrice",				"money", "",			"", "NULL"
AddFieldDef "mnyShipPrice",				"money", "",			"", "NULL"
AddFieldDef "intQuantity",				"int", "",				"", "NULL"
AddFieldDef "intSortOrder",				"int", "",				"", "NULL"
AddFieldDef "chrTaxFlag",				"char", "1",			"", "NULL"
' intForceShipMethod: if not NULL, requires that shipping method for the order
AddFieldDef "intForceShipMethod",		"int", "",				"", "NULL"
' chrForceSoloItem: "Y" = must be ordered by itself
AddFieldDef "chrForceSoloItem",			"char", "1",			"", "NULL"
AddFieldDef "$CONSTRAINT", "$_PK", "PRIMARY KEY NONCLUSTERED", "intID", "90"

AddTableDef STR_TABLE_INVENTORY
AddFieldDef "intID", 					"int identity", "1,1",	"", "NOT NULL"
AddFieldDef "chrType",					"char", "1",			"", "NOT NULL"
AddFieldDef "chrStatus",				"char", "1",			"", "NOT NULL"
AddFieldDef "dtmCreated",				"smalldatetime", "",	"", "NOT NULL"
AddFieldDef "dtmUpdated",				"smalldatetime", "",	"", "NOT NULL"
AddFieldDef "vchCreatedByUser",			"varchar", "32",		"", "NOT NULL"
AddFieldDef "vchUpdatedByUser",			"varchar", "32",		"", "NOT NULL"
AddFieldDef "vchCreatedByIP",			"varchar", "15",		"", "NOT NULL"
AddFieldDef "vchUpdatedByIP",			"varchar", "15",		"", "NOT NULL"
AddFieldDef "intParentID",				"int", "",				"", "NOT NULL"
AddFieldDef "intTempParentID",			"int", "",				"", "NULL"
AddFieldDef "intSortOrder",				"int", "",				"", "NOT NULL"
AddFieldDef "intStock",					"int", "",				"", "NULL"
AddFieldDef "intLowStock",				"int", "",				"", "NULL"
AddFieldDef "intHighStock",				"int", "",				"", "NULL"
AddFieldDef "intHitCount",				"int", "",				"", "NOT NULL"
AddFieldDef "vchImageURL",				"varchar", "255",		"", "NULL"
AddFieldDef "intSmImageHeight",			"int", "",				"", "NULL"
AddFieldDef "intSmImageWidth",			"int", "",				"", "NULL"
AddFieldDef "intLgImageHeight",			"int", "",				"", "NULL"
AddFieldDef "intLgImageWidth",			"int", "",				"", "NULL"
AddFieldDef "mnyItemPrice",				"money", "",			"", "NOT NULL"
AddFieldDef "mnyShipPrice",				"money", "",			"", "NULL"
AddFieldDef "chrTaxFlag",				"char", "1",			"", "NOT NULL"
AddFieldDef "chrICFlag",				"char", "1",			"", "NOT NULL"
AddFieldDef "chrSoftFlag",				"char", "1",			"", "NOT NULL"
AddFieldDef "vchSoftURL",				"varchar", "255",		"", "NULL"
AddFieldDef "vchItemName",				"varchar", "255",		"", "NOT NULL"
AddFieldDef "vchPartNumber",			"varchar", "32",		"", "NULL"
AddFieldDef "fltShipWeight",			"real", "",				"", "NULL"
' intForceShipMethod: if not NULL, requires that shipping method for the order
AddFieldDef "intForceShipMethod",		"int", "",				"", "NULL"
' chrForceSoloItem: "Y" = must be ordered by itself
AddFieldDef "chrForceSoloItem",			"char", "1",			"", "NULL"
AddFieldDef "txtDescription",			"text", "",				"", "NULL"
AddFieldDef "$CONSTRAINT", "$_PK", "PRIMARY KEY NONCLUSTERED", "intID", "90"

AddTableDef STR_TABLE_USER
AddFieldDef "intID", 					"int identity", "1,1",	"", "NOT NULL"
AddFieldDef "chrType",					"char", "1",			"", "NOT NULL"
AddFieldDef "chrStatus",				"char", "1",			"", "NOT NULL"
AddFieldDef "dtmCreated",				"smalldatetime", "",	"", "NOT NULL"
AddFieldDef "dtmUpdated",				"smalldatetime", "",	"", "NOT NULL"
AddFieldDef "vchCreatedByUser",			"varchar", "32",		"", "NOT NULL"
AddFieldDef "vchUpdatedByUser",			"varchar", "32",		"", "NOT NULL"
AddFieldDef "vchCreatedByIP",			"varchar", "15",		"", "NOT NULL"
AddFieldDef "vchUpdatedByIP",			"varchar", "15",		"", "NOT NULL"
AddFieldDef "vchUsername",				"varchar", "20",		"", "NOT NULL"
AddFieldDef "vchPassword",				"varchar", "20",		"", "NOT NULL"
AddFieldDef "vchFullName",				"varchar", "32",		"", "NOT NULL"
AddFieldDef "dtmLastLogin",				"smalldatetime", "",	"", "NULL"
AddFieldDef "vchReceiveEmail",			"varchar", "50",		"", "NULL"
AddFieldDef "vchCanViewStatus",			"varchar", "32",		"", "NULL"
AddFieldDef "vchAccess",				"varchar", "255",		"", "NULL"
AddFieldDef "$CONSTRAINT", "$_PK", "PRIMARY KEY NONCLUSTERED", "intID", "90"

AddTableDef STR_TABLE_AUDIT
AddFieldDef "intID", 					"int identity", "1,1",	"", "NOT NULL"
AddFieldDef "chrType",					"char", "1",			"", "NOT NULL"
AddFieldDef "chrStatus",				"char", "1",			"", "NOT NULL"
AddFieldDef "dtmCreated",				"smalldatetime", "",	"", "NOT NULL"
AddFieldDef "vchCreatedByUser",			"varchar", "32",		"", "NOT NULL"
AddFieldDef "vchCreatedByIP",			"varchar", "15",		"", "NOT NULL"
AddFieldDef "vchAction",				"varchar", "32",		"", "NOT NULL"
AddFieldDef "vchObject",				"varchar", "32",		"", "NULL"
AddFieldDef "vchDescription",			"varchar", "255",		"", "NULL"
AddFieldDef "$CONSTRAINT", "$_PK", "PRIMARY KEY NONCLUSTERED", "intID", "90"

AddTableDef STR_TABLE_REFERER
AddFieldDef "intID", 					"int identity", "1,1",	"", "NOT NULL"
AddFieldDef "chrType",					"char", "1",			"", "NOT NULL"
AddFieldDef "chrStatus",				"char", "1",			"", "NOT NULL"
AddFieldDef "dtmCreated",				"smalldatetime", "",	"", "NOT NULL"
AddFieldDef "dtmUpdated",				"smalldatetime", "",	"", "NOT NULL"
AddFieldDef "vchCreatedByUser",			"varchar", "32",		"", "NOT NULL"
AddFieldDef "vchUpdatedByUser",			"varchar", "32",		"", "NOT NULL"
AddFieldDef "vchCreatedByIP",			"varchar", "15",		"", "NOT NULL"
AddFieldDef "vchUpdatedByIP",			"varchar", "15",		"", "NOT NULL"
AddFieldDef "vchCompany",				"varchar", "32",		"", "NULL"
AddFieldDef "vchSiteName",				"varchar", "50",		"", "NULL"
AddFieldDef "vchContactName",			"varchar", "32",		"", "NULL"
AddFieldDef "vchContactEmail",			"varchar", "50",		"", "NULL"
AddFieldDef "vchContactPhone1",			"varchar", "32",		"", "NULL"
AddFieldDef "vchContactPhone2",			"varchar", "32",		"", "NULL"
AddFieldDef "fltDiscount",				"real", "",				"", "NULL"
AddFieldDef "$CONSTRAINT", "$_PK", "PRIMARY KEY NONCLUSTERED", "intID", "90"

sub AddTableDef(sName)
	if intTableDefCount > kintMaxTables then
		response.write "<B>Error:</B> AddTableDef() failed, too many tables. Increase kintMaxTables."
		response.end
	end if
	aryTableList(intTableDefCount,0,0) = sName
	aryTableList(intTableDefCount,0,1) = 1
	intTableDefCount = intTableDefCount + 1
end sub

sub AddFieldDef(sName, sType, sArgs, sDef, sAttrib)
	dim x
	x = aryTableList(intTableDefCount-1,0,1)
	if x > kintMaxFields then
		response.write "<B>Error:</B> AddFieldDef() failed, too many fields. Increase kintMaxFields."
		response.end
	end if
	aryTableList(intTableDefCount-1,x,0) = sName
	aryTableList(intTableDefCount-1,x,1) = sType
	aryTablelist(intTableDefCount-1,x,2) = sArgs
	aryTableList(intTableDefCount-1,x,3) = sDef
	aryTableList(intTableDefCount-1,x,4) = sAttrib
	aryTableList(intTableDefCount-1,0,1) = x + 1
end sub
%>