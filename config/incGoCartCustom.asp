<%

' =====================================================================================
' = File: incGoCartCustom.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   GoCart API Customizations
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =====================================================================================
const BLN_SUPPORT_OEC = False

const STR_MERCHANT_NAME = ""
const STR_MERCHANT_ADDRESS1 = "3"
const STR_MERCHANT_ADDRESS2 = ""
const STR_MERCHANT_CITY = ""
const STR_MERCHANT_STATE = ""
const STR_MERCHANT_ZIP = ""
const STR_MERCHANT_COUNTRY = ""
const STR_MERCHANT_PHONE = ""
const STR_MERCHANT_FAX = ""
const STR_MERCHANT_EMAIL = ""
const STR_MERCHANT_CS_PHONE = ""
const STR_MERCHANT_CS_EMAIL = ""

const STR_MERCHANT_DEFAULT_URL = "main.asp?updateheader=1"
const STR_MERCHANT_TRACKING_PREFIX = "HBO-"	'"TRK-"

const SESSION_MERCHANT_UID = "HBO_buy_meruid"			' intID of user record of logged-in user
const SESSION_MERCHANT_ULOGIN = "HBO_buy_merulogin"		' username of logged-in user
const SESSION_MERCHANT_UNAME = "HBO_buy_meruname"		' full name of logged-in user
const SESSION_MERCHANT_UTIME = "HBO_buy_merutime"		' login time of logged-in user
const SESSION_MERCHANT_ACCESS = "HBO_buy_meracc"		' specifies the access flags of the logged-in user
const SESSION_MERCHANT_BID = "HBO_buy_merbid"			' intBrand of user record of logged-in user
const SESSION_MERCHANT_ACCESS_STATUS = "HBO_buy_meraccstatus"	' specifies what order status's can be seen by this user
const SESSION_REPORT_START = "HBO_buy_repstart"			' reporting period start date
const SESSION_REPORT_END = "HBO_buy_repend"				' reporting period end date
const SESSION_REPORT_TYPE = "HBO_buy_reptype"			' reporting period type
const SESSION_REPORT_TOP = "HBO_buy_reptop"				' reporting top record count
const SESSION_REFERAL_ID = "HBO_refid"
const SESSION_REFERAL_NAME = "HBO_refname"
const SESSION_REFERAL_DISCOUNT = "HBO_refdiscount"
const SESSION_EXPORT_SHOPPER = "HBO_buy_expshpr"		' flags for including billing or shipping addresses
const SESSION_EXPORT_FORMAT = "HBO_buy_expformat"		' specifies format of export data
const SESSION_EXPORT_NOTES = "HBO_buy_expnotes"			' specifies only export data with notes

const STR_TABLE_INVENTORY = "HBO_Inv"
const STR_TABLE_CODE = "HBO_GuideVersion"
const STR_TABLE_INVENTORY_LONG_TERM = "HBO_LongTermInv"
const STR_TABLE_SHOPPER = "HBO_Shopper"
const STR_TABLE_SHOPPER_LONG_TERM = "HBO_Shopper"
const STR_TABLE_ORDER = "HBO_Order"
const STR_TABLE_ORDER_LONG_TERM = "HBO_LongTermOrder"
const STR_TABLE_LINEITEM = "HBO_LineItem"
const STR_TABLE_LINEITEM_LONG_TERM = "HBO_LongTermLineItem"
const STR_TABLE_TRANS = "HBO_Trans"
const STR_TABLE_USER = "HBO_AdminUser"
const STR_TABLE_AUDIT = "HBO_Audit"
const STR_TABLE_REFERER = "HBO_Referer"
const STR_TABLE_STOREFRONT = "HBO_StoreFront"
const STR_TABLE_GROUPS = "HBO_Groups"
const STR_TABLE_INVENTORY_LONG_TERM_UPDATE = "HBO_LongTermInvUpdate"
const STR_TABLE_PROFILEMAPPER = "HBO_ProfileMapper"
const STR_TABLE_RESOURCES = "[HBO_Resources]"

'POD Tables
CONST TABLE_PRODUCT_GROUP = "HBO_POD_ProductGroups"
CONST TABLE_PRODUCT_TABLE = "HBO_POD_Products"
CONST TABLE_PRODUCT_CUSTOM_STEPS = "HBO_POD_CustomSteps"
CONST TABLE_PRODUCT_CUSTOM_DIALS = "HBO_POD_CustomDials"
CONST TABLE_PRODUCT_CUSTOM_DROPDOWNS = "HBO_POD_CustomDropDowns"
CONST TABLE_PRODUCT_CUSTOM_PARAMS = "HBO_POD_CustomParams"
CONST TABLE_PRODUCT_SAVED_DIALS = "HBO_POD_SavedDials"
CONST TABLE_PRODUCT_TICKETS = "HBO_POD_Tickets"

const STR_TABLE_LAYOUTS = "HBO_Layouts"
const STR_TABLE_LAYOUTS_SLOTS = "HBO_LayoutSlots"
const STR_TABLE_PROFILES = "HBO_Profiles"
const STR_TABLE_PROFILE_MAPPER = "HBO_ProfileMapper"
const STR_TABLE_PROFILE_PACKAGES = "HBO_ProfilePackages"
const STR_TABLE_PACKGROUP = "HBO_PackGroups"
const STR_TABLE_SHOPPER_FILTER = "HBO_Shopper_Filter"
const STR_TABLE_PROFILE_STORE_PACKAGE = "HBO_ProfileStorePackage"

dim cGoCart_CartListRowBG, cGoCart_CartListHdrText, cgoCart_InvDivider, cGoCart_CSLoginBG, cGoCart_CSHdrText, cGoCart_CSHdrBG, cGoCart_CSTotalRowBG, cGoCart_CSSelectBG
cGoCart_CartListRowBG = cLtBlue
cGoCart_CartListHdrText = cBlack
cGoCart_InvDivider = cGreen
cGoCart_CSLoginBG = cLtBlue
cGoCart_CSHdrText = cGreen
cGoCart_CSHdrBG = cVDkBlue
cGoCart_CSTotalRowBG = cWhite
cGoCart_CSSelectBG = cRed

dim strPromoCode

dim dctPaymentMethod, dctTaxZoneRates, dctCreditCardTypeValues, dctCountryCodes, dctShipOption, dctUSStateCodes, dctFixtureMap, dctFixtureImportance, dctFixtures, dctFixtureAccessoryMap
set dctPaymentMethod = Server.CreateObject("Scripting.Dictionary")
	dctPaymentMethod.Add "OCC", "Online Credit Card"
'	dctPaymentMethod.Add "OEC", "Online Direct Debit"
'	dctPaymentMethod.Add "PCC", "Phone-in Credit Card"
'	dctPaymentMethod.Add "PEC", "Phone-in Direct Debit"
'	dctPaymentMethod.Add "FCC", "Fax-in Credit Card"
'	dctPaymentMethod.Add "FMO", "Fax-in money order"
'	dctPaymentMethod.Add "FPO", "Fax-in purchase order"
'	dctPaymentMethod.Add "FEC", "Fax-in Direct Debit"
'	dctPaymentMethod.Add "MCC", "Mail-in Credit Card"
	dctPaymentMethod.Add "MEC", "Mail-in Check or Money Order"
'	dctPaymentMethod.Add "MMO", "Mail-in Money Order"
'	dctPaymentMethod.Add "PPO", "Phone-in Purchase Order"
'	dctPaymentMethod.Add "MPO", "Mail-in Purchase Order"

set dctFixtureMap = Server.CreateObject("Scripting.Dictionary")
	dctFixtureMap.Add "2", "ECH" 'End Cap Headers
	dctFixtureMap.Add "3", "X-" 'Lobby Card
	dctFixtureMap.Add "4", "X-" 'Floor Stacking Cube 18x18
	dctFixtureMap.Add "5", "X-" 'Pop-Up Totem 24x84
	dctFixtureMap.Add "6", "Banner" 'Banner
	dctFixtureMap.Add "7", "X-" 'Ladder Graphic
	dctFixtureMap.Add "9", "ECD2" 'End Cap Dangler 6x6
	dctFixtureMap.Add "10", "ECDW" 'End Cap Dot Whack
	dctFixtureMap.Add "11", "XA" 'Floor Stacking Cube Decal
	dctFixtureMap.Add "12", "DC" 'Door Cling
	dctFixtureMap.Add "13", "FD" 'Floor Decal
	dctFixtureMap.Add "14", "SC" 'Shelving Cube
	dctFixtureMap.Add "15", "SCO" 'Shelving Cube Overlay
	dctFixtureMap.Add "16", "XA" 'Lug-On
	dctFixtureMap.Add "20", "ECD1" 'End Cap Dangler 5x5
	dctFixtureMap.Add "21", "ECD3" 'End Cap Dangler 8x8
	dctFixtureMap.Add "22", "X-" 'Floor Stacking Cube 22x22
	dctFixtureMap.Add "23", "X-" 'Pop-Up Totem 24x7
	dctFixtureMap.Add "24", "BDW" 'Banner Dot Whack
	dctFixtureMap.Add "26", "XA" 'Ladder Graphic Dot Whack
	
'Fixture column names with description
set dctFixtures=Server.CreateObject("Scripting.Dictionary")
	dctFixtures.Add "EndCapHeader", "End Cap Headers"
	dctFixtures.Add "EndCapDangler6x6","End Cap Dangler 5&quot;x5&quot;"
	dctFixtures.Add "EndCapDangler5x5","End Cap Dangler 5&quot;x5&quot;"
	dctFixtures.Add "EndCapDangler8x8","End Cap Dangler 8&quot;x8&quot;"
	dctFixtures.Add "Pop-UpTotem24x84","Pop Up Totems 24&quot;x84&quot;"
	dctFixtures.Add "Pop-UpTotem24x7","Pop Up Totems 24&quot;x7&quot;"
	dctFixtures.Add "Banner","Banners"
	dctFixtures.Add "LadderGraphic", "Ladder Graphic"
	dctFixtures.Add "BannerDotWhack","Banner Dot Whack"
	dctFixtures.Add "LadderGraphicDotWhack", "Ladder Graphic Dot Whack"
	dctFixtures.Add "EndCapDotWhack","End Cap Dot Whack"
	dctFixtures.Add "FloorStackingCubeDecal","Floor Stacking Cube Decal"
	dctFixtures.Add "FloorStackingCube18x18","Floor Stacking Cube 18&quot;"
	dctFixtures.Add "FloorStackingCube22x22","Floor Stacking Cube 22&quot;"
	dctFixtures.Add "DoorClings","Door Clings"
	dctFixtures.Add "FloorDecal","Floor Decals"
	dctFixtures.Add "ShelvingCubes","Shelving Cubes"
	dctFixtures.Add "Poster","Poster"
	dctFixtures.Add "ShelfCubeDisplay","Shelf Cube Display"
	dctFixtures.Add "LobbyCard","Lobby Card"
	dctFixtures.Add "Lug-On","Lug On"
	dctFixtures.Add "WindowDecal","Window Decal"
	dctFixtures.Add "ShelfCubeOverlay","Shelf Cube Overlay"
'we want to fill out multi first since it has certain rules then the accessories (matching multis first again) then do the rest of auto assign
set dctFixtureImportance = Server.CreateObject("Scripting.Dictionary")
	dctFixtureImportance.Add "1",  "3" 'Lobby Card
	dctFixtureImportance.Add "2",  "5,23" 'Pop-Up Totem 24x84 , 24x7
	dctFixtureImportance.Add "3",  "22,4" 'Floor Stacking Cube 22x22 , 18x18
	dctFixtureImportance.Add "4",  "7" 'Ladder Graphic
	dctFixtureImportance.Add "5",  "16" 'Lug-On
	dctFixtureImportance.Add "6",  "11" 'Floor Stacking Cube Decal
	dctFixtureImportance.Add "7",  "26" 'Ladder Graphic Dot Whack
	dctFixtureImportance.Add "8",  "20,9,21" 'End Cap Dangler 5x5, 6x6, 8x8
	dctFixtureImportance.Add "9",  "10" 'End Cap Dot Whack
	dctFixtureImportance.Add "10",  "14" 'Shelving Cube
	dctFixtureImportance.Add "11",  "15" 'Shelving Cube Overlay
	dctFixtureImportance.Add "12",  "13" 'Floor Decal
	dctFixtureImportance.Add "13",  "6" 'Banner
	dctFixtureImportance.Add "14",  "24" 'Banner Dot Whack
	dctFixtureImportance.Add "15",  "12" 'Door Cling
	
set dctFixtureAccessoryMap = Server.CreateObject("Scripting.Dictionary")
	dctFixtureAccessoryMap.Add "16",  "5,23" 'Lug-On > Pop-Up Totem 24x84 , 24x7
	dctFixtureAccessoryMap.Add "11",  "22,4" 'Decals > Floor Stacking Cube 22x22 , 18x18
	dctFixtureAccessoryMap.Add "26",  "7" 'Ladder Graphic Dot Whack > Ladder Graphic
	dctFixtureAccessoryMap.Add "15",  "14" 'Shelving Cube Overlay > Shelving Cube
	dctFixtureAccessoryMap.Add "20",  "2" 'End Cap Dangler 5x5 > End Cap Headers
	dctFixtureAccessoryMap.Add "9",  "2" 'End Cap Dangler 6x6 > End Cap Headers
	dctFixtureAccessoryMap.Add "21",  "2" 'End Cap Dangler 8x8 > End Cap Headers
	dctFixtureAccessoryMap.Add "10",  "2" 'End Cap Dot Whack > End Cap Headers
	dctFixtureAccessoryMap.Add "24",  "6" 'Banner Dot Whack> Banner

set dctTaxZoneRates = Server.CreateObject("Scripting.Dictionary")
	dctTaxZoneRates.Add "1~8.75", "California, USA"
	dctTaxZoneRates.Add "0~0", "Other"

set dctCreditCardTypeValues = Server.CreateObject("Scripting.Dictionary")
	dctCreditCardTypeValues.Add "VISA", "Visa"
	dctCreditCardTypeValues.Add "MASTERCARD", "MasterCard"
	dctCreditCardTypeValues.Add "AMEX", "American Express"
	dctCreditCardTypeValues.Add "DISCOVER", "Discover"

set dctShipOption = Server.CreateObject("Scripting.Dictionary")
	dctShipOption.Add "1", "FedEx Express Saver"
	dctShipOption.Add "2", "FedEx Overnight"
	dctShipOption.Add "3", "FedEx 2-Day"

Dim dctFreightCode

set dctFreightCode = Server.CreateObject("Scripting.Dictionary")
	dctFreightCode.Add "1", "F14"
	dctFreightCode.Add "2", "F06"
	dctFreightCode.Add "3", "F11"

const STR_SHIP_INTRNL = 10
const STR_SHIP_CUSTOM = 11
const STR_SHIP_FREE = 12

Set dctCountryCodes = CreateObject("Scripting.Dictionary")
' don't include the US -- force them to use the Domestic form!
	dctCountryCodes.Add "US", "United States"
	dctCountryCodes.Add "AS", "American Samoa"
	dctCountryCodes.Add "AR", "Argentina"
	dctCountryCodes.Add "AW", "Aruba"
	dctCountryCodes.Add "AU", "Australia"
	dctCountryCodes.Add "AT", "Austria"
	dctCountryCodes.Add "BS", "Bahamas"
	dctCountryCodes.Add "BB", "Barbados"
	dctCountryCodes.Add "BE", "Belgium"
	dctCountryCodes.Add "BZ", "Belize"
	dctCountryCodes.Add "BM", "Bermuda"
	dctCountryCodes.Add "BO", "Bolivia"
	dctCountryCodes.Add "BR", "Brazil"
	dctCountryCodes.Add "CA", "Canada"
	dctCountryCodes.Add "CL", "Chile"
	dctCountryCodes.Add "CN", "China"
	dctCountryCodes.Add "CO", "Colombia"
	dctCountryCodes.Add "CR", "Costa Rica"
	dctCountryCodes.Add "CZ", "Czech Republic"
	dctCountryCodes.Add "DK", "Denmark"
	dctCountryCodes.Add "DO", "Dominican Republic"
	dctCountryCodes.Add "EC", "Ecuador"
	dctCountryCodes.Add "EG", "Egypt"
	dctCountryCodes.Add "SV", "El Salvador"
	dctCountryCodes.Add "FR", "France"
	dctCountryCodes.Add "DE", "Germany"
	dctCountryCodes.Add "GR", "Greece"
	dctCountryCodes.Add "GU", "Guam"
	dctCountryCodes.Add "HN", "Honduras"
	dctCountryCodes.Add "HK", "Hong Kong"
	dctCountryCodes.Add "HU", "Hungary"
	dctCountryCodes.Add "IS", "Iceland"
	dctCountryCodes.Add "IN", "India"
	dctCountryCodes.Add "IT", "Italy"
	dctCountryCodes.Add "JM", "Jamaica"
	dctCountryCodes.Add "JP", "Japan"
	dctCountryCodes.Add "KR", "Korea, Republic of"
	dctCountryCodes.Add "KW", "Kuwait"
	dctCountryCodes.Add "LI", "Liechtenstein"
	dctCountryCodes.Add "LU", "Luxembourg"
	dctCountryCodes.Add "MX", "Mexico"
	dctCountryCodes.Add "MC", "Monaco"
	dctCountryCodes.Add "NL", "Netherlands"
	dctCountryCodes.Add "NZ", "New Zealand"
	dctCountryCodes.Add "MP", "Northern Mariana Islands"
	dctCountryCodes.Add "NO", "Norway"
	dctCountryCodes.Add "PE", "Peru"
	dctCountryCodes.Add "PL", "Poland"
	dctCountryCodes.Add "PT", "Portugal"
	dctCountryCodes.Add "RO", "Romania"
	dctCountryCodes.Add "SG", "Singapore"
	dctCountryCodes.Add "SB", "Solomon Islands"
	dctCountryCodes.Add "ZA", "South Africa"
	dctCountryCodes.Add "ES", "Spain"
	dctCountryCodes.Add "SE", "Sweden"
	dctCountryCodes.Add "CH", "Switzerland"
	dctCountryCodes.Add "TT", "Trinidad and Tobago"
	dctCountryCodes.Add "GB", "United Kingdom"

'	dctCountryCodes.Add "AF", "Afghanistan"
'	dctCountryCodes.Add "AL", "Albania"
'	dctCountryCodes.Add "DZ", "Algeria"
'	dctCountryCodes.Add "AS", "American Samoa"
'	dctCountryCodes.Add "AD", "Andorra"
'	dctCountryCodes.Add "AO", "Angola"
'	dctCountryCodes.Add "AI", "Anguilla"
'	dctCountryCodes.Add "AQ", "Antarctica"
'	dctCountryCodes.Add "AG", "Antigua and Barbuda"
'	dctCountryCodes.Add "AR", "Argentina"
'	dctCountryCodes.Add "AM", "Armenia"
'	dctCountryCodes.Add "AW", "Aruba"
'	dctCountryCodes.Add "AU", "Australia"
'	dctCountryCodes.Add "AT", "Austria"
'	dctCountryCodes.Add "AZ", "Azerbaijan"
'	dctCountryCodes.Add "BS", "Bahamas"
'	dctCountryCodes.Add "BH", "Bahrain"
'	dctCountryCodes.Add "BD", "Bangladesh"
'	dctCountryCodes.Add "BB", "Barbados"
'	dctCountryCodes.Add "BY", "Belarus"
'	dctCountryCodes.Add "BE", "Belgium"
'	dctCountryCodes.Add "BZ", "Belize"
'	dctCountryCodes.Add "BJ", "Benin"
'	dctCountryCodes.Add "BM", "Bermuda"
'	dctCountryCodes.Add "BT", "Bhutan"
'	dctCountryCodes.Add "BO", "Bolivia"
'	dctCountryCodes.Add "BA", "Bosnia and Herzegovina"
'	dctCountryCodes.Add "BW", "Botswana"
'	dctCountryCodes.Add "BV", "Bouvet Island"
'	dctCountryCodes.Add "BR", "Brazil"
'	dctCountryCodes.Add "IO", "British Indian Ocean Territory"
'	dctCountryCodes.Add "BN", "Brunei Darussalam"
'	dctCountryCodes.Add "BG", "Bulgaria"
'	dctCountryCodes.Add "BF", "Burkina Faso"
'	dctCountryCodes.Add "BI", "Burundi"
'	dctCountryCodes.Add "KH", "Cambodia"
'	dctCountryCodes.Add "CM", "Camaroon"
'	dctCountryCodes.Add "CA", "Canada"
'	dctCountryCodes.Add "CV", "Cape Verde"
'	dctCountryCodes.Add "KY", "Cayman Islands"
'	dctCountryCodes.Add "CF", "Central African Rupublic"
'	dctCountryCodes.Add "TD", "Chad"
'	dctCountryCodes.Add "CL", "Chile"
'	dctCountryCodes.Add "CN", "China"
'	dctCountryCodes.Add "CX", "Chirstmas Island"
'	dctCountryCodes.Add "CC", "Cocos (Keeling) Islands"
'	dctCountryCodes.Add "CO", "Colombia"
'	dctCountryCodes.Add "KM", "Comoros"
'	dctCountryCodes.Add "CG", "Congo"
'	dctCountryCodes.Add "CD", "Congo, The Democratic Republic of the"
'	dctCountryCodes.Add "CK", "Cook Islands"
'	dctCountryCodes.Add "CR", "Costa Rica"
'	dctCountryCodes.Add "CI", "Cote D'Ivoire"
'	dctCountryCodes.Add "HR", "Croatia"
'	dctCountryCodes.Add "CU", "Cuba"
'	dctCountryCodes.Add "CY", "Cyprus"
'	dctCountryCodes.Add "CZ", "Czech Republic"
'	dctCountryCodes.Add "DK", "Denmark"
'	dctCountryCodes.Add "DJ", "Djibouti"
'	dctCountryCodes.Add "DM", "Dominica"
'	dctCountryCodes.Add "DO", "Dominican Republic"
'	dctCountryCodes.Add "TP", "East Timor"
'	dctCountryCodes.Add "EC", "Ecuador"
'	dctCountryCodes.Add "EG", "Egypt"
'	dctCountryCodes.Add "SV", "El Salvador"
'	dctCountryCodes.Add "GQ", "Equatorial Guinea"
'	dctCountryCodes.Add "ER", "Eritrea"
'	dctCountryCodes.Add "EE", "Estonia"
'	dctCountryCodes.Add "ET", "Ethiopia"
'	dctCountryCodes.Add "FK", "Falkland Islands (Malvinas)"
'	dctCountryCodes.Add "FO", "Faroe Islands"
'	dctCountryCodes.Add "FJ", "Fiji"
'	dctCountryCodes.Add "FI", "Finland"
'	dctCountryCodes.Add "FR", "France"
'	dctCountryCodes.Add "GF", "French Guiana"
'	dctCountryCodes.Add "PF", "French Polynesia"
'	dctCountryCodes.Add "TF", "French Southern Territories"
'	dctCountryCodes.Add "GA", "Gabon"
'	dctCountryCodes.Add "GM", "Gambia"
'	dctCountryCodes.Add "GE", "Georgia"
'	dctCountryCodes.Add "DE", "Germany"
'	dctCountryCodes.Add "GH", "Ghana"
'	dctCountryCodes.Add "GI", "Gibraltar"
'	dctCountryCodes.Add "GR", "Greece"
'	dctCountryCodes.Add "GL", "Greenland"
'	dctCountryCodes.Add "GD", "Grenada"
'	dctCountryCodes.Add "GP", "Guadeloupe"
'	dctCountryCodes.Add "GU", "Guam"
'	dctCountryCodes.Add "GT", "Guatemala"
'	dctCountryCodes.Add "GN", "Guinea"
'	dctCountryCodes.Add "GW", "Guinea-Bissau"
'	dctCountryCodes.Add "GY", "Guyana"
'	dctCountryCodes.Add "HT", "Haiti"
'	dctCountryCodes.Add "HM", "Heard Island and McDonald Islands"
'	dctCountryCodes.Add "HN", "Honduras"
'	dctCountryCodes.Add "HK", "Hong Kong"
'	dctCountryCodes.Add "HU", "Hungary"
'	dctCountryCodes.Add "IS", "Iceland"
'	dctCountryCodes.Add "IN", "India"
'	dctCountryCodes.Add "ID", "Indonesia"
'	dctCountryCodes.Add "IR", "Iran, Islamic Republic of"
'	dctCountryCodes.Add "IQ", "Iraq"
'	dctCountryCodes.Add "IE", "Ireland"
'	dctCountryCodes.Add "IL", "Israel"
'	dctCountryCodes.Add "IT", "Italy"
'	dctCountryCodes.Add "JM", "Jamaica"
'	dctCountryCodes.Add "JP", "Japan"
'	dctCountryCodes.Add "JO", "Jordan"
'	dctCountryCodes.Add "KZ", "Kazakstan"
'	dctCountryCodes.Add "KE", "Kenya"
'	dctCountryCodes.Add "KI", "Kiribati"
'	dctCountryCodes.Add "KP", "Korea, Democratic People's Republic of"
'	dctCountryCodes.Add "KR", "Korea, Republic of"
'	dctCountryCodes.Add "KW", "Kuwait"
'	dctCountryCodes.Add "KG", "Kyrgyzstan"
'	dctCountryCodes.Add "LA", "Lao People's Democratic Republic"
'	dctCountryCodes.Add "LV", "Latvia"
'	dctCountryCodes.Add "LB", "Lebanon"
'	dctCountryCodes.Add "LS", "Lesotho"
'	dctCountryCodes.Add "LR", "Liberia"
'	dctCountryCodes.Add "LY", "Libyan Aram Jamahiriya"
'	dctCountryCodes.Add "LI", "Liechtenstein"
'	dctCountryCodes.Add "LT", "Lithuania"
'	dctCountryCodes.Add "LU", "Luxembourg"
'	dctCountryCodes.Add "MO", "Macau"
'	dctCountryCodes.Add "MK", "Macendonia, The Former Yugoslav Republic of"
'	dctCountryCodes.Add "MG", "Madagascar"
'	dctCountryCodes.Add "MW", "Malawi"
'	dctCountryCodes.Add "MY", "Malaysia"
'	dctCountryCodes.Add "MV", "Maldives"
'	dctCountryCodes.Add "ML", "Mali"
'	dctCountryCodes.Add "MT", "Malta"
'	dctCountryCodes.Add "MH", "Marshall Islands"
'	dctCountryCodes.Add "MQ", "Martinique"
'	dctCountryCodes.Add "MR", "Mauritania"
'	dctCountryCodes.Add "MU", "Mauritius"
'	dctCountryCodes.Add "YT", "Mayotte"
'	dctCountryCodes.Add "MX", "Mexico"
'	dctCountryCodes.Add "FM", "Micronesia, Federated States of"
'	dctCountryCodes.Add "MD", "Moldova, Republic of"
'	dctCountryCodes.Add "MC", "Monaco"
'	dctCountryCodes.Add "MN", "Mongolia"
'	dctCountryCodes.Add "MS", "Montserrat"
'	dctCountryCodes.Add "MA", "Morocco"
'	dctCountryCodes.Add "MZ", "Mozambique"
'	dctCountryCodes.Add "MM", "Myanmar"
'	dctCountryCodes.Add "NA", "Nambia"
'	dctCountryCodes.Add "NR", "Nauru"
'	dctCountryCodes.Add "NP", "Nepal"
'	dctCountryCodes.Add "NL", "Netherlands"
'	dctCountryCodes.Add "AN", "Netherlands Antilles"
'	dctCountryCodes.Add "NC", "New Caledonia"
'	dctCountryCodes.Add "NZ", "New Zealand"
'	dctCountryCodes.Add "NI", "Nicaragua"
'	dctCountryCodes.Add "NE", "Niger"
'	dctCountryCodes.Add "NG", "Nigeria"
'	dctCountryCodes.Add "NU", "Niue" 
'	dctCountryCodes.Add "NF", "Norfolk Island"
'	dctCountryCodes.Add "MP", "Northern Mariana Islands" 
'	dctCountryCodes.Add "NO", "Norway"
'	dctCountryCodes.Add "OM", "Oman" 
'	dctCountryCodes.Add "PK", "Pakistan" 
'	dctCountryCodes.Add "PW", "Palau" 
'	dctCountryCodes.Add "PS", "Palestinian Territory, Occupied" 
'	dctCountryCodes.Add "PA", "Panama" 
'	dctCountryCodes.Add "PG", "Papau New Guinea" 
'	dctCountryCodes.Add "PY", "Paraguay" 
'	dctCountryCodes.Add "PE", "Peru" 
'	dctCountryCodes.Add "PH", "Philippines" 
'	dctCountryCodes.Add "PN", "Pitcairn" 
'	dctCountryCodes.Add "PL", "Poland"
'	dctCountryCodes.Add "PT", "Portugal" 
'	dctCountryCodes.Add "QA", "Qatar"
'	dctCountryCodes.Add "RE", "Reunion" 
'	dctCountryCodes.Add "RO", "Romania" 
'	dctCountryCodes.Add "RU", "Russian Federation" 
'	dctCountryCodes.Add "RW", "Rwanda" 
'	dctCountryCodes.Add "SH", "Saint Helena" 
'	dctCountryCodes.Add "KN", "Saint Kitts and Nevis" 
'	dctCountryCodes.Add "LC", "Saint Lucia" 
'	dctCountryCodes.Add "PM", "Saint Pierre and Miquelon" 
'	dctCountryCodes.Add "VC", "Saint Vincent and the Grenadines" 
'	dctCountryCodes.Add "WS", "Samoa" 
'	dctCountryCodes.Add "SM", "San Marino" 
'	dctCountryCodes.Add "ST", "Sao Tome and Principe" 
'	dctCountryCodes.Add "SA", "Saudi Arabia" 
'	dctCountryCodes.Add "SN", "Senegal" 
'	dctCountryCodes.Add "SC", "Seychelles" 
'	dctCountryCodes.Add "SL", "Sierra Leone" 
'	dctCountryCodes.Add "SG", "Singapore" 
'	dctCountryCodes.Add "SK", "Slovakia" 
'	dctCountryCodes.Add "SI", "Slovenia" 
'	dctCountryCodes.Add "SB", "Solomon Islands" 
'	dctCountryCodes.Add "SO", "Somalia" 
'	dctCountryCodes.Add "ZA", "South Africa" 
'	dctCountryCodes.Add "GS", "South Georgia and the South Sandwich Islands" 
'	dctCountryCodes.Add "ES", "Spain" 
'	dctCountryCodes.Add "LK", "Sri Lanka" 
'	dctCountryCodes.Add "SD", "Sudan" 
'	dctCountryCodes.Add "SR", "Suriname" 
'	dctCountryCodes.Add "SJ", "Svalbard and Jan Mayen" 
'	dctCountryCodes.Add "SZ", "Swaziland" 
'	dctCountryCodes.Add "SE", "Sweden" 
'	dctCountryCodes.Add "CH", "Switzerland" 
'	dctCountryCodes.Add "SY", "Syrian Arab Republic" 
'	dctCountryCodes.Add "TW", "Taiwan, Province of China" 
'	dctCountryCodes.Add "TJ", "Tajikistan" 
'	dctCountryCodes.Add "TZ", "Tanzania, United Republic of" 
'	dctCountryCodes.Add "TH", "Thailand" 
'	dctCountryCodes.Add "TG", "Togo" 
'	dctCountryCodes.Add "TK", "Tokelau" 
'	dctCountryCodes.Add "TO", "Tonga" 
'	dctCountryCodes.Add "TT", "Trinidad and Tobago" 
'	dctCountryCodes.Add "TN", "Tunisia" 
'	dctCountryCodes.Add "TR", "Turkey" 
'	dctCountryCodes.Add "TM", "Turkmenistan" 
'	dctCountryCodes.Add "TC", "Turks and Caicos Islands" 
'	dctCountryCodes.Add "TV", "Tuvalu" 
'	dctCountryCodes.Add "UG", "Uganda" 
'	dctCountryCodes.Add "UA", "Ukraine" 
'	dctCountryCodes.Add "AE", "United Arab Emirates" 
'	dctCountryCodes.Add "GB", "United Kingdom"
'	dctCountryCodes.Add "UM", "United States Minor Outlying Islands" 
'	dctCountryCodes.Add "UY", "Uruguay"
'	dctCountryCodes.Add "UZ", "Uzbekistan"
'	dctCountryCodes.Add "VA", "Vatican City State"	
'	dctCountryCodes.Add "VU", "Vanuatu" 
'	dctCountryCodes.Add "VE", "Venezuela" 
'	dctCountryCodes.Add "VN", "Viet Nam" 
'	dctCountryCodes.Add "VG", "Virgin Islands, British" 
'	dctCountryCodes.Add "WF", "Wallis and Futuna" 
'	dctCountryCodes.Add "EH", "Western Sahara" 
'	dctCountryCodes.Add "YE", "Yemen" 
'	dctCountryCodes.Add "YU", "Yugoslavia" 
'	dctCountryCodes.Add "ZM", "Zambia" 
'	dctCountryCodes.Add "ZW", "Zimbabwe"

Set dctUSStateCodes = CreateObject("Scripting.Dictionary")
'	dctUSStateCodes.Add "XX", "None"
	dctUSStateCodes.Add "AL", "Alabama (AL)"
	dctUSStateCodes.Add "AK", "Alaska (AK)"
	dctUSStateCodes.Add "AZ", "Arizona (AZ)"
	dctUSStateCodes.Add "AR", "Arkansas (AR)"
	dctUSStateCodes.Add "CA", "California (CA)"
	dctUSStateCodes.Add "CO", "Colorado (CO)"
	dctUSStateCodes.Add "CT", "Connecticut (CT)"
	dctUSStateCodes.Add "DE", "Delaware (DE)"
	dctUSStateCodes.Add "DC", "District of Columbia (DC)"
	dctUSStateCodes.Add "FL", "Florida (FL)"
	dctUSStateCodes.Add "GA", "Georgia (GA)"
	dctUSStateCodes.Add "HI", "Hawaii (HI)"
	dctUSStateCodes.Add "ID", "Idaho (ID)"
	dctUSStateCodes.Add "IL", "Illinois (IL)"
	dctUSStateCodes.Add "IN", "Indiana (IN)"
	dctUSStateCodes.Add "IA", "Iowa (IA)"
	dctUSStateCodes.Add "KS", "Kansas (KS)"
	dctUSStateCodes.Add "KY", "Kentucky (KY)"
	dctUSStateCodes.Add "LA", "Louisiana (LA)"
	dctUSStateCodes.Add "ME", "Maine (ME)"
	dctUSStateCodes.Add "MD", "Maryland (MD)"
	dctUSStateCodes.Add "MA", "Massachusetts (MA)"
	dctUSStateCodes.Add "MI", "Michigan (MI)"
	dctUSStateCodes.Add "MN", "Minnesota (MN)"
	dctUSStateCodes.Add "MS", "Mississippi (MS)"
	dctUSStateCodes.Add "MO", "Missouri (MO)"
	dctUSStateCodes.Add "MT", "Montana (MT)"
	dctUSStateCodes.Add "NE", "Nebraska (NE)"
	dctUSStateCodes.Add "NV", "Nevada (NV)"
	dctUSStateCodes.Add "NH", "New Hampshire (NH)"
	dctUSStateCodes.Add "NJ", "New Jersey (NJ)"
	dctUSStateCodes.Add "NM", "New Mexico (NM)"
	dctUSStateCodes.Add "NY", "New York (NY)"
	dctUSStateCodes.Add "NC", "North Carolina (NC)"
	dctUSStateCodes.Add "ND", "North Dakota (ND)"
	dctUSStateCodes.Add "OH", "Ohio (OH)"
	dctUSStateCodes.Add "OK", "Oklahoma (OK)"
	dctUSStateCodes.Add "OR", "Oregon (OR)"
	dctUSStateCodes.Add "PA", "Pennsylvania (PA)"
	dctUSStateCodes.Add "PR", "Puerto Rico (PR)" 
	dctUSStateCodes.Add "RI", "Rhode Island (RI)"
	dctUSStateCodes.Add "SC", "South Carolina (SC)"
	dctUSStateCodes.Add "SD", "South Dakota (SD)"
	dctUSStateCodes.Add "TN", "Tennessee (TN)"
	dctUSStateCodes.Add "TX", "Texas (TX)"
	dctUSStateCodes.Add "UT", "Utah (UT)"
	dctUSStateCodes.Add "VT", "Vermont (VT)"
	dctUSStateCodes.Add "VA", "Virginia (VA)"
	dctUSStateCodes.Add "VI", "Virgin Islands (VI)" 
	dctUSStateCodes.Add "WA", "Washington (WA)"
	dctUSStateCodes.Add "WV", "West Virginia (WV)"
	dctUSStateCodes.Add "WI", "Wisconsin (WI)"
	dctUSStateCodes.Add "WY", "Wyoming (WY)"

Sub ShowIfValid(PreString, TheString, PostString)
	If IsValid(TheString) Then
		Response.Write PreString & TheString & PostString
	End If
End Sub

Function IsValid(TheString)
	If IsNull(TheString) Or IsEmpty(TheString) Or TheString = "" Or TheString = " " Then
		IsValid = False
	Else
		If IsNumeric(TheString) Then
			IsValid = iif(TheString > 0, True, False)
		Else
			IsValid = True
		End if
	End If
End Function

' GoCart 5 Custom Routines

sub PrepareSpecialAdd(intForceShipMethod, blnForceSoloItem)
	PrepareSpecialAdd_Other intOrderID, intForceShipMethod, blnForceSoloItem
end sub

sub PrepareSpecialAdd_Other(intOrderID, intForceShipMethod, blnForceSoloItem)
	dim rsItem, strSQL, strDelList
	strDelList = ""
	strSQL = "SELECT intID, intForceShipMethod, chrForceSoloItem FROM " & STR_TABLE_LINEITEM & " WHERE intOrderID=" & intOrderID & " AND chrStatus<>'D'"
	set rsItem = gobjConn.execute(strSQL)
	while not rsItem.eof
		if (intForceShipMethod & "" <> "") and (rsItem("intForceShipMethod") & "" <> "") and (intForceShipMethod & "" <> rsItem("intForceShipMethod") & "") then
			strDelList = strDelList & rsItem("intID") & ","
		elseif blnForceSoloItem then
			strDelList = strDelList & rsItem("intID") & ","
		elseif rsItem("chrForceSoloItem") = "Y" then
			strDelList = strDelList & rsItem("intID") & ","
		end if
		rsItem.MoveNext
	wend
	rsItem.close
	set rsItem = nothing
	if strDelList <> "" then
		strSQL = "UPDATE " & STR_TABLE_LINEITEM & " SET chrStatus='D' WHERE intID IN (" & left(strDelList, len(strDelList) - 1) & ")"
		gobjConn.execute(strSQL)
	end if
end sub

function CanAddItem(intID)
	if gintOrderID > 0 then
		CanAddItem = CanAddItem_Other(gintOrderID, intID)
	else
		CanAddItem = true
	end if
end function

function CanAddItem_Other(intOrderID, intID)
	dim strSQL, rsTemp, intForceShipMethod, chrForceSoloItem
	strSQL = "SELECT intForceShipMethod, chrForceSoloItem FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intID
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		intForceShipMethod = rsTemp("intForceShipMethod")
		chrForceSoloItem = rsTemp("chrForceSoloItem")
		rsTemp.close
		set rsTemp = nothing
		CanAddItem_Other = CanAddItemSpecial_Other(intOrderID, intForceShipMethod, chrForceSoloItem)
	else
		CanAddItem_Other = true
		rsTemp.close
		set rsTemp = nothing
	end if
end function

function CanAddItemSpecial_Other(intOrderID, intForceShipMethod, chrForceSoloItem)
	' return true if item can be added without conflict with other items ordered
	' otherwise return false
	dim strSQL, rsTemp
	CanAddItemSpecial_Other = true
	if (chrForceSoloItem = "Y") then
		' item must be ordered separately from all other items
		' if there are any other items in this order, return false
		strSQL = "SELECT COUNT(*) AS intItemCount FROM " & STR_TABLE_LINEITEM & " WHERE intOrderID=" & intOrderID & " AND chrStatus<>'D'"
		set rsTemp = gobjConn.execute(strSQL)
		if rsTemp("intItemCount") > 0 then
			CanAddItemSpecial_Other = false
		end if
		rsTemp.close
		set rsTemp = nothing
		if CanAddItemSpecial_Other = false then
			exit function
		end if
	end if
	if intForceShipMethod & "" <> "" then
		' item can not be ordered along with any other item that requires a forced shipping method
		' other than this method
		strSQL = "SELECT intForceShipMethod FROM " & STR_TABLE_LINEITEM & " WHERE intOrderID=" & intOrderID & " AND chrStatus<>'D' AND intForceShipMethod IS NOT NULL"
		set rsTemp = gobjConn.execute(strSQL)
		while not rsTemp.eof
			if rsTemp("intForceShipMethod") <> intForceShipMethod then
				CanAddItemSpecial_Other = false
				rsTemp.close
				set rsTemp = nothing
				exit function
			end if
			rsTemp.MoveNext
		wend
		rsTemp.close
		set rsTemp = nothing
	end if
	' check existing items in order to see if any are marked as "must be ordered separately"
	strSQL = "SELECT chrForceSoloItem FROM " & STR_TABLE_LINEITEM & " WHERE intOrderID=" & intOrderID & " AND chrStatus<>'D' AND chrForceSoloItem='Y'"
	set rsTemp = gobjConn.execute(strSQL)
	CanAddItemSpecial_Other = rsTemp.eof
	rsTemp.close
	set rsTemp = nothing
end function

sub AddItemToOrder(intID, intQuantity)
	if gintOrderID = 0 then
		CreateNewOrder
	end if
	AddItemToOrder_Other gintOrderID, intID, intQuantity
end sub

sub AddItemToOrder_Other(intOrderID, intID, intQuantity)
	if not IsNumeric(intQuantity) then
		intQuantity = 0
	else
		intQuantity = CLng(intQuantity)
	end if
	if intQuantity > 0 then	
	
	    dim strSQL, rsTemp
	    strSQL = "SELECT intID, ISNULL(intQuantity,0) AS intQuantity FROM " & STR_TABLE_LINEITEM & " WHERE chrStatus='A' AND intOrderID=" & intOrderID & " AND intInvID=" & intID
	    set rsTemp = gobjConn.execute(strSQL)
	    if not rsTemp.eof then
		    dim intItemID
		    intQuantity = intQuantity + rsTemp("intQuantity")
		    intItemID = rsTemp("intID")
		    rsTemp.close
		    set rsTemp = nothing
		    strSQL = "UPDATE " & STR_TABLE_LINEITEM & " SET intQuantity = " & intQuantity & " WHERE intID=" & intItemID
		    gobjConn.execute(strSQL)
	    else
		    rsTemp.close
		    set rsTemp = nothing
		    strSQL = "SELECT * FROM " & STR_TABLE_INVENTORY & " WHERE chrType='I' AND chrStatus='A' AND intID=" & intID
		    set rsTemp = gobjConn.execute(strSQL)
		    if not rsTemp.eof then
			    if IsNumeric(rsTemp("intMinQty")) then
				    if intQuantity < rsTemp("intMinQty") then
					    intQuantity = rsTemp("intMinQty")
				    end if
			    end if
			    PrepareSpecialAdd_Other intOrderID, rsTemp("intForceShipMethod"), (rsTemp("chrForceSoloItem") & "" = "Y")
			    EditOrderItem_Other intOrderID, 0, intID, rsTemp("vchPartNumber"), rsTemp("vchItemName"), rsTemp("mnyItemPrice"), rsTemp("mnyShipPrice"), rsTemp("chrTaxFlag") = "Y", intQuantity, rsTemp("intForceShipMethod"), rsTemp("chrForceSoloItem")
		    end if
		    rsTemp.close
		    set rsTemp = nothing
	    end if
    end if
	ReCalcOrder_Other intOrderID
end sub

sub AddMultipleItemsToOrder (rsItems)
	dim strItemList, intItemID, intQuantity, strName
	for each strName in rsItems.Form
		if Left(strName,9) = "Quantity_" then
			intItemID = Mid(strName,InStr(strName,"_")+1)
			if IsNumeric(intItemID) then
				intQuantity = rsItems("Quantity_" & intItemID)
				if IsNumeric(intQuantity) then
					AddItemToOrder intItemID, intQuantity
				else
					AddItemToOrder intItemID, 1
				end if
			end if
		end if
	next
end sub




sub AddMultipleItemsToOrderDist (rsItems)
	dim strItemList, intItemID, intQuantity, strName, intDistributorID, intOrderID, rsOrders, strSQL
	for each strName in rsItems.Form
		if Left(strName,5) = "Item_" then

            intOrderId = 0
            intItemID = Split(Replace(strName,"Item_",""),"_")(0)
            intDistributorID = Split(Replace(strName,"Item_",""),"_")(1)

            strSQL = "SELECT *  FROM " & STR_TABLE_ORDER & " WHERE intBillShopperId=" & intDistributorID & " AND chrStatus='0'"
            strSQL = strSQL & " AND intid IN (SELECT L.intOrderID FROM " & STR_TABLE_LINEITEM & " AS L, " & STR_TABLE_INVENTORY & " AS I WHERE L.intInvID = I.intID AND I.intCampaign = " & Request("campaignid")&")"
	        set rsOrders = gobjConn.execute(strSQL)

            if not rsOrders.Eof Then
                intOrderId = rsOrders("intId")
            End If

            if intOrderId=0 then
                CreateNewOrder
                intOrderId = gintOrderID

                strSQL = "UPDATE " & STR_TABLE_ORDER & " SET intBillShopperId=" & intDistributorID & ", intShipShopperId=" & intDistributorID & " WHERE intID=" & gintOrderID
	            call gobjConn.execute(strSQL)

            end if


			if IsNumeric(intItemID) then
				intQuantity = rsItems(strName)
				if IsNumeric(intQuantity) then
					AddItemToOrder_Other intOrderId, intItemID, intQuantity
				end if
			end if

            

		end if
	next
end sub


sub ReCalcOrder_OtherLt(intOrderID)
	
end sub

sub AddItemToOrder_OtherLt(intOrderID, intID, intQuantity)
'response.write intQuantity
	if not IsNumeric(intQuantity) then
		intQuantity = 0
	else
		intQuantity = CLng(intQuantity)
	end if
	if intQuantity > 0 then	
	
	    dim strSQL, rsTemp
	    strSQL = "SELECT intID, ISNULL(intQuantity,0) AS intQuantity FROM " & STR_TABLE_LINEITEM_LONG_TERM & " WHERE chrStatus='A' AND intOrderID=" & intOrderID & " AND intInvID=" & intID & " AND vchPartNumber Not like '" & left(STR_TABLE_LINEITEM_LONG_TERM, 3) & "_POD_%'"
	    set rsTemp = gobjConn.execute(strSQL)
	    if not rsTemp.eof then
		    dim intItemID
		    intQuantity = intQuantity + rsTemp("intQuantity")
		    intItemID = rsTemp("intID")
		    rsTemp.close
		    set rsTemp = nothing
			
		    strSQL = "UPDATE " & STR_TABLE_LINEITEM_LONG_TERM & " SET intQuantity = " & intQuantity & " WHERE intID=" & intItemID
		    gobjConn.execute(strSQL)
	    else
			
		    rsTemp.close
		    set rsTemp = nothing
		    strSQL = "SELECT * FROM " & STR_TABLE_INVENTORY_LONG_TERM & " WHERE chrType='I' AND chrStatus='A' AND intID=" & intID
		    set rsTemp = gobjConn.execute(strSQL)
		    if not rsTemp.eof then
			    if IsNumeric(rsTemp("intMinQty")) then
				    if intQuantity < rsTemp("intMinQty") then
					    intQuantity = rsTemp("intMinQty")
				    end if
			    end if
				'response.write "here"
			    PrepareSpecialAdd_OtherLt intOrderID, rsTemp("intForceShipMethod"), (rsTemp("chrForceSoloItem") & "" = "Y")
			    EditOrderItem_OtherLt intOrderID, 0, intID, rsTemp("vchPartNumber"), rsTemp("vchItemName"), rsTemp("mnyItemPrice"), rsTemp("mnyShipPrice"), rsTemp("chrTaxFlag") = "Y", intQuantity, rsTemp("intForceShipMethod"), rsTemp("chrForceSoloItem")
		    end if
			
		    rsTemp.close
		    set rsTemp = nothing
	    end if
    end if
	
	ReCalcOrder_OtherLt intOrderID
end sub


sub PrepareSpecialAdd_OtherLt(intOrderID, intForceShipMethod, blnForceSoloItem)
	dim rsItem, strSQL, strDelList
	strDelList = ""
	strSQL = "SELECT intID, intForceShipMethod, chrForceSoloItem FROM " & STR_TABLE_LINEITEM_LONG_TERM & " WHERE intOrderID=" & intOrderID & " AND chrStatus<>'D'"
	set rsItem = gobjConn.execute(strSQL)
	while not rsItem.eof
		if (intForceShipMethod & "" <> "") and (rsItem("intForceShipMethod") & "" <> "") and (intForceShipMethod & "" <> rsItem("intForceShipMethod") & "") then
			strDelList = strDelList & rsItem("intID") & ","
		elseif blnForceSoloItem then
			strDelList = strDelList & rsItem("intID") & ","
		elseif rsItem("chrForceSoloItem") = "Y" then
			strDelList = strDelList & rsItem("intID") & ","
		end if
		rsItem.MoveNext
	wend
	rsItem.close
	set rsItem = nothing
	if strDelList <> "" then
		strSQL = "UPDATE " & STR_TABLE_LINEITEM_LONG_TERM & " SET chrStatus='D' WHERE intID IN (" & left(strDelList, len(strDelList) - 1) & ")"
		gobjConn.execute(strSQL)
	end if
end sub


function EditOrderItem_OtherLt(intOrderID, intLineItemID, intInvID, strPartNumber, strItemName, mnyUnitPrice, mnyShipPrice, blnTaxable, intQuantity, intForceShipMethod, chrForceSoloItem)
	if not IsNumeric(intQuantity) then
		intQuantity = 1
	else
		intQuantity = CLng(intQuantity)
	end if
	if intQuantity < 1 then
		intQuantity = 1
	end if
	dim dctSaveList
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "*@dtmCreated", "GETDATE()"
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "*vchCreatedByUser", "pub"
		dctSaveList.Add "vchUpdatedByUser", "pub"
		dctSaveList.Add "*vchCreatedByIP", gstrUserIP
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "chrType", "I"
		dctSaveList.Add "chrStatus", "A"
		dctSaveList.Add "#intOrderID", intOrderID
		dctSaveList.Add "#intInvID", intInvID
		dctSaveList.Add "vchPartNumber", strPartNumber
		dctSaveList.Add "vchItemName", strItemName
		dctSaveList.Add "#mnyUnitPrice", mnyUnitPrice
		dctSaveList.Add "#mnyShipPrice", mnyShipPrice
		dctSaveList.Add "#intQuantity", intQuantity
		dctSaveList.Add "chrTaxFlag", iif(blnTaxable, "Y", "N")
		dctSaveList.Add "#intForceShipMethod", intForceShipMethod
		dctSaveList.Add "chrForceSoloItem", chrForceSoloItem
	EditOrderItem_OtherLt = SaveDataRecord("" & STR_TABLE_LINEITEM_LONG_TERM, Request, intLineItemID, dctSaveList)
	ReCalcOrder_OtherLt intOrderID
end function

sub AddMultipleItemsToOrderDistLt (rsItems)
	dim strItemList, intItemID, intQuantity, strName, intDistributorID, intOrderID, rsOrders, strSQL
	for each strName in rsItems.Form
		if Left(strName,5) = "Item_" then

            intOrderId = 0
            intItemID = Split(Replace(strName,"Item_",""),"_")(0)
            intDistributorID = Split(Replace(strName,"Item_",""),"_")(1)

            strSQL = "SELECT *  FROM " & STR_TABLE_ORDER_LONG_TERM & " WHERE intBillShopperId=" & intDistributorID & " AND chrStatus='0'"
	        set rsOrders = gobjConn.execute(strSQL)

            if not rsOrders.Eof Then
                intOrderId = rsOrders("intId")
            End If

            if intOrderId=0 then
                CreateNewOrderLt
                intOrderId = gintOrderID

                strSQL = "UPDATE " & STR_TABLE_ORDER_LONG_TERM & " SET intBillShopperId=" & intDistributorID & ", intShipShopperId=" & intDistributorID & " WHERE intID=" & gintOrderID
	            call gobjConn.execute(strSQL)

            end if


			if IsNumeric(intItemID) then
				intQuantity = rsItems(strName)
				if IsNumeric(intQuantity) then
					AddItemToOrder_OtherLt intOrderId, intItemID, intQuantity
				end if
			end if

            

		end if
	next
end sub

sub AddItemToOrderWithOption(intID, intQuantity, strOption1, strOption2, strOption3)
	if gintOrderID = 0 then
		CreateNewOrder
	end if
	AddItemToOrderWithOption_Other gintOrderID, intID, intQuantity, strOption1, strOption2, strOption3
end sub

sub AddItemToOrderWithOption_Other(intOrderID, intID, intQuantity, strOption1, strOption2, strOption3)
	dim strOption, strQty, strQty2, strItemPrice, w, x, y, z, blnFoundQty, mnyNewPrice
	strOption = ""
	strItemPrice = ""
	if IsNumeric(intQuantity) then
		intQuantity = CLng(intQuantity)
		if intQuantity < 0 then
			intQuantity = 0
		end if
	else
		intQuantity = 0
	end if
	blnFoundQty = false
	if strOption1 <> "" then
		strOption = strOption & ", " & strOption1
		strOption1 = Trim(strOption1)
		w = instr(strOption1,"=") -1
		if w < 1 then
			w = instr(strOption1,"$") -1
		end if
		x = instr(strOption1,"-") -1
		if x < 1 then
			x = instr(lcase(strOption1),"plus") -1
			if x < 1 then
				x = instr(lcase(strOption1),"to") -1
			end if
			if x < 1 then
				x = instr(strOption1,"=") -1
			end if
			if x < 1 then
				x = instr(strOption1,"$") -1
			end if
		end if
		' check for single quanity pricing
		if w > 0 and w <= x then
			strQty = left(strOption1,w)
			strQty2 = strQty
			x = w
		elseif x > 0 then
			strQty = left(strOption1,x)
			y = instr(lcase(strOption1),"plus")
			if y > 0 then
				strQty2 = "plus"
			else
				z = instr(lcase(strOption1),"to")
				if z > 0 then
					if mid(strOption1,z+2,1) = " " then
						z = z + 1
					end if
					y = instr(mid(strOption1,z+2)," ") -1
					if y < 1 then
						y = instr(mid(strOption1,z+2),"=") -1
					end if
					if y > 0 then
						y = y + z + 2
						strQty2 = mid(strOption1,z+2,y-z-1)
					end if
				else
					z = instr(lcase(strOption1),"-")
					if z > 0 then
						if mid(strOption1,z+1,1) = " " then
							z = z + 1
						end if
						y = instr(mid(strOption1,z+1)," ") -1
						if y < 1 then
							y = instr(mid(strOption1,z+1),"=") -1
						end if
						if y > 0 then
							z = z + 1
							y = y + z
							strQty2 = mid(strOption1,z,y-z)
						end if
					end if
				end if
			end if
		end if
		y = instr(strOption1,"$")
		if y = 0 then
			y = instr(strOption1,"=")
		end if
		if y > 0 then
			y = (len(strOption1) - y)
			strItemPrice = trim(right(strOption1,y))
			z = instr(strItemPrice," ") -1
			if z > 1 then
				strItemPrice = left(strItemPrice,z)
			end if
			if left(strItemPrice,1) = "." then
				strItemPrice = "0" & strItemPrice
			end if
		end if
		if IsNumeric(strQty) then
			if intQuantity < CLng(strQty) then
				intQuantity = CLng(strQty)
			end if
			if not IsNumeric(strQty2) then
				if lcase(strQty2) = "plus" then
					if intQuantity >= CLng(strQty) then
						strQty2 = intQuantity & ""
					end if
				else
					strQty2 = strQty
				end if
			end if
			if IsNumeric(strItemPrice) then
				if CCur(strItemPrice) > 0 then
					mnyNewPrice = CCur(strItemPrice)
'					blnFoundQty = true
				end if
			end if
		end if
'	debug code for parsing option1 field
'		response.write "len: " & len(strOption1) & " option1: " & strOption1 & "<br />"
'		response.write "x: " & x & " qty: " & strQty & " - " & strQty2 & " vs " & intQuantity & " " & CCur(strItemPrice) & " " & (CCur(strItemPrice)>0) & " " & (CLng(strQty)=CLng(strQty2) and clng(strQty)>=intQuantity) & " " & blnFoundQty & "<br />"
'		response.write "y: " & y & " price: " & strItemPrice  & " vs " & mnyNewPrice & "<br />"
'		if blnFoundQty = false then
'			response.end
'		end if
	end if

	if strOption2 <> "" then
		strOption = strOption & ", " & strOption2
	end if
	if strOption3 <> "" then
		strOption = strOption & ", " & strOption3
	end if
	if intQuantity < 1 then
		intQuantity = 1
	end if
	dim strSQL, rsTemp
	strSQL = "SELECT * FROM " & STR_TABLE_INVENTORY & " WHERE chrType='I' AND chrStatus='A' AND intID=" & intID
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		dim strItemName, strLineItemName, strOptionList1, mnyMinPrice
		strItemName = rsTemp("vchItemName") & strOption
		strLineItemName = rsTemp("vchItemName") & strOption
		strOptionList1 = rsTemp("vchOptionList1")
		mnyNewPrice = "0"& rsTemp("mnyItemPrice")
		mnyMinPrice = "0"& rsTemp("mnyMinPrice")
		if IsNumeric("0"& rsTemp("intMinQty")) then
			if intQuantity < "0"& rsTemp("intMinQty") then
				intQuantity = "0"& rsTemp("intMinQty")
			end if
		end if
		if blnFoundQty = false then
			mnyNewPrice = GetOrderItemPrice_Other(strItemName, strLineItemName, intQuantity, mnyNewPrice, strOptionList1)
			if mnyNewPrice > 0 then
				strItemPrice = mnyNewPrice & ""
			end if
		elseif not IsNumeric(strItemPrice) then
			strItemPrice = mnyNewPrice & ""
		end if
		if (strItemPrice * intQuantity) < mnyMinPrice then
			intQuantity = 1
			strItemPrice = mnyMinPrice
		end if
		EditOrderItem_Other intOrderID, 0, intID, rsTemp("vchPartNumber"), strLineItemName, strItemPrice, rsTemp("mnyShipPrice"), rsTemp("chrTaxFlag") = "Y", intQuantity, rsTemp("intForceShipMethod"), rsTemp("chrForceSoloItem")
	end if
	rsTemp.close
	set rsTemp = nothing
	ReCalcOrder_Other intOrderID
end sub

function GetOrderItemPrice_Other(strItemName, strLineItemName, intQuantity, mnyNewPrice, strOptionList1)

	if InStr(strOptionList1, ",") > 0 then
		dim strQty, strQty2, strItemPrice, blnFoundQty
		blnFoundQty = false
		dim a, b, c, w, x, y, z, strOption1
		a = split(strOptionList1, ",")
		b = ubound(a)

for c = 0 to b
	strOption1 = a(c) & ""
	strQty = ""
	strQty2 = ""
	if strOption1 <> "" then
		strOption1 = Trim(strOption1)
		w = instr(strOption1,"=") -1
		if w < 1 then
			w = instr(strOption1,"$") -1
		end if
		x = instr(strOption1,"-") -1
		if x < 1 then
			x = instr(lcase(strOption1),"plus") -1
			if x < 1 then
				z = instr(lcase(strOption1),"to")
			end if
			if x < 1 then
				x = instr(strOption1,"=") -1
			end if
			if x < 1 then
				x = instr(strOption1,"$") -1
			end if
		end if
		' check for single quanity pricing before qty range pricing
		if w > 0 and w <= x then
			strQty = left(strOption1,w)
			strQty2 = strQty
			x = w
		elseif x > 0 then
			strQty = left(strOption1,x)
			y = instr(lcase(strOption1),"plus") -1
			if y > 0 then
				strQty2 = "plus"
			else
				z = instr(lcase(strOption1),"to")
				if z > 0 then
					if mid(strOption1,z+2,1) = " " then
						z = z + 1
					end if
					y = instr(mid(strOption1,z+2)," ") -1
					if y < 1 then
						y = instr(mid(strOption1,z+2),"=") -1
					end if
					if y > 0 then
						y = y + z + 2
						strQty2 = mid(strOption1,z+2,y-z-1)
					end if
				else
					z = instr(lcase(strOption1),"-")
					if z > 0 then
						if mid(strOption1,z+1,1) = " " then
							z = z + 1
						end if
						y = instr(mid(strOption1,z+1)," ") -1
						if y < 1 then
							y = instr(mid(strOption1,z+1),"=") -1
						end if
						if y > 0 then
							z = z + 1
							y = y + z
							strQty2 = mid(strOption1,z,y-z)
						end if
					end if
				end if
			end if
		end if
		y = instr(strOption1,"$")
		if y = 0 then
			y = instr(strOption1,"=")
		end if
		if y > 0 then
			y = (len(strOption1) - y)
			strItemPrice = trim(right(strOption1,y))
			z = instr(strItemPrice," ") -1
			if z > 1 then
				strItemPrice = left(strItemPrice,z)
			end if
			if left(strItemPrice,1) = "." then
				strItemPrice = "0" & strItemPrice
			end if
		end if
		if IsNumeric(strQty) then
			if not IsNumeric(strQty2) then
				if lcase(strQty2) = "plus" then
					if intQuantity >= CLng(strQty) then
						strQty2 = intQuantity & ""
					end if
				else
					strQty2 = strQty
				end if
			end if
			if (intQuantity >= CLng(strQty) and intQuantity <= CLng(strQty2)) then ' correct quanity range
				if IsNumeric(strItemPrice) then
					if CCur(strItemPrice) > 0 then
						mnyNewPrice = CCur(strItemPrice)
						blnFoundQty = true
					end if
				end if
			elseif (CLng(strQty)=CLng(strQty2)) then ' correct quanity range
				if IsNumeric(strItemPrice) then
 					if (CLng(strQty)>=intQuantity) then
						if mnyNewPrice = 0 or not blnFoundQty then
		 					if CLng(strQty)=intQuantity and CCur(strItemPrice) > 0 then
								mnyNewPrice = CCur(strItemPrice)
							else
								strOption1 = trim(a(c-1) & "")
							end if
							blnFoundQty = true
						end if
					else
	 					if CCur(strItemPrice) > 0 then
							mnyNewPrice = CCur(strItemPrice)
							' check for last item in the list
							if (c = b) and (CLng(strQty) < intQuantity) then
								blnFoundQty = true
							end if
						end if
					end if
				end if
			end if
		end if
'	debug code for parsing option1 field
'		response.write "<div id=""cartitem""><br />"
'		response.write "len: " & len(strOption1) & " option1: " & strOption1 & "<br />"
'		response.write "x: " & x & " qty: " & strQty & iif(strQty2<>""," - " & strQty2,"") & " vs " & intQuantity & " " & CCur(strItemPrice) & " " & (CCur(strItemPrice)>0) & " " & (CLng(strQty)=CLng(strQty2) and clng(strQty)>=intQuantity) & " " & blnFoundQty & "<br />"
'		response.write "y: " & y & " price: " & strItemPrice & " vs " & mnyNewPrice & "<br />"
'		response.write "</div>"
		if blnFoundQty then
			' parse the item name field
			x = instr(strItemName,",") -1
			y = 0
			if x > 0 then
				strLineItemName = left(strItemName,x) & ", " & strOption1
				y = instr(mid(strItemName,x+2),",") -1
				if y > 0 then
					y = y + x + 2
					strLineItemName = strLineItemName & mid(strItemName,y)
				end if
			else
				strLineItemName = strItemName & ", " & strOption1
			end if
'		response.write "<div id=""cartitem""><br />"
'		response.write "len: " & len(strItemName) & " strItemName: " & strItemName & "<br />"
'		response.write "x: " & x & " strLineItemName: " & strLineItemName  & "<br />"
'		response.write "y: " & y & " strOption1: " & strOption1 & "<br />"
'		response.write "</div>"
			Exit For
		end if
	end if
next
'		response.end
	end if
	GetOrderItemPrice_Other = mnyNewPrice
end function

function GetOrderItemPrice(intLineItemID, intQuantity, strLineItemName)
	' need to add code to check inventory table for qty/price matrix
	dim strSQL, rsTemp, mnyNewPrice, strOptionList1, strItemName
	mnyNewPrice = 0
	if intQuantity > 0 then
		strSQL = "SELECT L.mnyUnitPrice, L.vchItemName AS vchLineItemName, I.* FROM " & STR_TABLE_INVENTORY & " AS I, " & STR_TABLE_LINEITEM & " AS L"
		strSQL = strSQL & "	WHERE I.chrType='I' AND I.chrStatus='A' AND I.intID=L.intInvID"
		strSQL = strSQL & "	AND L.intID=" & intLineItemID & " AND L.intOrderID=" & gintOrderID
'		response.write strSQL & "<br />"
		set rsTemp = gobjConn.execute(strSQL)
		if not rsTemp.eof then
			mnyNewPrice = rsTemp("mnyUnitPrice")
			strOptionList1 = rsTemp("vchOptionList1")
			strItemName = rsTemp("vchLineItemName")
			if IsNumeric(rsTemp("intMinQty")) then
				if intQuantity < rsTemp("intMinQty") then
					intQuantity = rsTemp("intMinQty")
				end if
			end if
			mnyNewPrice = GetOrderItemPrice_Other(strItemName, strLineItemName, intQuantity, mnyNewPrice, strOptionList1)
		end if
	end if
	GetOrderItemPrice = mnyNewPrice
end function

function EditOrderItemQty_Other(intOrderID, intLineItemID, intNewQty)
	dim mnyNewPrice, strLineItemName
	' change price in cart
	mnyNewPrice = GetOrderItemPrice(intLineItemID, intNewQty, strLineItemName)
	if intNewQty > 0 then
		dim strSQL
		strSQL = "UPDATE " & STR_TABLE_LINEITEM & " SET intQuantity=" & intNewQty
		if mnyNewPrice > 0 then
		strSQL = strSQL & " , mnyUnitPrice=" & mnyNewPrice	
			if IsValid(strLineItemName) then
		strSQL = strSQL & " , vchItemName='" & strLineItemName & "'"
			end if
		end if
		strSQL = strSQL & " WHERE intID=" & intLineItemID & " AND intOrderID=" & intOrderID
'		response.write "<div id=""cartitem"">" & strSQL & "<br /></div>"
		gobjConn.execute(strSQL)
		ReCalcOrder_Other gintOrderID
	end if
end function

function EditOrderItemQtyLT_Other(intOrderID, intLineItemID, intNewQty)
	dim mnyNewPrice, strLineItemName
	' change price in cart
	mnyNewPrice = GetOrderItemPrice(intLineItemID, intNewQty, strLineItemName)
	if intNewQty > 0 then
		dim strSQL
		strSQL = "UPDATE " & STR_TABLE_LINEITEM_LONG_TERM & " SET intQuantity=" & intNewQty
		if mnyNewPrice > 0 then
		strSQL = strSQL & " , mnyUnitPrice=" & mnyNewPrice	
			if IsValid(strLineItemName) then
		strSQL = strSQL & " , vchItemName='" & strLineItemName & "'"
			end if
		end if
		strSQL = strSQL & " WHERE intID=" & intLineItemID & " AND intOrderID=" & intOrderID
'		response.write "<div id=""cartitem"">" & strSQL & "<br /></div>"
		gobjConn.execute(strSQL)
		ReCalcOrderLT_Other gintOrderID
	end if
end function
function EditOrderItemQty(intLineItemID, intNewQty)
	dim mnyNewPrice, strLineItemName
	' change price in cart
	mnyNewPrice = GetOrderItemPrice(intLineItemID, intNewQty, strLineItemName)
	if intNewQty > 0 then
		dim strSQL
		strSQL = "UPDATE " & STR_TABLE_LINEITEM & " SET intQuantity=" & intNewQty
		if mnyNewPrice > 0 then
		strSQL = strSQL & " , mnyUnitPrice=" & mnyNewPrice	
			if IsValid(strLineItemName) then
		strSQL = strSQL & " , vchItemName='" & strLineItemName & "'"
			end if
		end if
		strSQL = strSQL & " WHERE intID=" & intLineItemID & " AND intOrderID=" & gintOrderID
'		response.write "<div id=""cartitem"">" & strSQL & "<br /></div>"
		gobjConn.execute(strSQL)
		ReCalcOrder_Other gintOrderID
	end if
end function

function EditOrderItem_Other(intOrderID, intLineItemID, intInvID, strPartNumber, strItemName, mnyUnitPrice, mnyShipPrice, blnTaxable, intQuantity, intForceShipMethod, chrForceSoloItem)
	if not IsNumeric(intQuantity) then
		intQuantity = 1
	else
		intQuantity = CLng(intQuantity)
	end if
	if intQuantity < 1 then
		intQuantity = 1
	end if
	dim dctSaveList
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "*@dtmCreated", "GETDATE()"
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "*vchCreatedByUser", "pub"
		dctSaveList.Add "vchUpdatedByUser", "pub"
		dctSaveList.Add "*vchCreatedByIP", gstrUserIP
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "chrType", "I"
		dctSaveList.Add "chrStatus", "A"
		dctSaveList.Add "#intOrderID", intOrderID
		dctSaveList.Add "#intInvID", intInvID
		dctSaveList.Add "vchPartNumber", strPartNumber
		dctSaveList.Add "vchItemName", strItemName
		dctSaveList.Add "#mnyUnitPrice", mnyUnitPrice
		dctSaveList.Add "#mnyShipPrice", mnyShipPrice
		dctSaveList.Add "#intQuantity", intQuantity
		dctSaveList.Add "chrTaxFlag", iif(blnTaxable, "Y", "N")
		dctSaveList.Add "#intForceShipMethod", intForceShipMethod
		dctSaveList.Add "chrForceSoloItem", chrForceSoloItem
	EditOrderItem_Other = SaveDataRecord("" & STR_TABLE_LINEITEM, Request, intLineItemID, dctSaveList)
	ReCalcOrder_Other intOrderID
end function

function EditOrderNote_Other(intOrderID, intLineItemID, strItemName)
	dim dctSaveList
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "*@dtmCreated", "GETDATE()"
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "*vchCreatedByUser", "pub"
		dctSaveList.Add "vchUpdatedByUser", "pub"
		dctSaveList.Add "*vchCreatedByIP", gstrUserIP
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "chrType", "N"
		dctSaveList.Add "chrStatus", "A"
		dctSaveList.Add "#intOrderID", intOrderID
	'	dctSaveList.Add "#intInvID", intInvID
	'	dctSaveList.Add "vchPartNumber", strPartNumber
		dctSaveList.Add "vchItemName", strItemName
	'	dctSaveList.Add "#mnyUnitPrice", mnyUnitPrice
	'	dctSaveList.Add "#mnyShipPrice", mnyShipPrice
	'	dctSaveList.Add "#intQuantity", intQuantity
	'	dctSaveList.Add "chrTaxFlag", iif(blnTaxable, "Y", "N")
	EditOrderNote_Other = SaveDataRecord("" & STR_TABLE_LINEITEM, Request, intLineItemID, dctSaveList)
	ReCalcOrder_Other intOrderID
end function

function EditOrderReturn_Other(intOrderID, intItemID, intInvID, strPartNumber, strReturnName, strReturnPrice, strReturnShip, intQuantity, blnTaxable)
	if not IsNumeric(intQuantity) then
		intQuantity = 1
	else
		intQuantity = CLng(intQuantity)
	end if
	if intQuantity < 1 then
		intQuantity = 1
	end if
	dim dctSaveList
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "*@dtmCreated", "GETDATE()"
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "*vchCreatedByUser", "pub"
		dctSaveList.Add "vchUpdatedByUser", "pub"
		dctSaveList.Add "*vchCreatedByIP", gstrUserIP
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "chrType", "R"
		dctSaveList.Add "chrStatus", "A"
		dctSaveList.Add "#intOrderID", intOrderID
		dctSaveList.Add "#intInvID", intInvID
		dctSaveList.Add "vchPartNumber", strPartNumber
		dctSaveList.Add "vchItemName", strReturnName
		dctSaveList.Add "#mnyUnitPrice", strReturnPrice
		dctSaveList.Add "#mnyShipPrice", strReturnShip
		dctSaveList.Add "#intQuantity", intQuantity
		dctSaveList.Add "chrTaxFlag", iif(blnTaxable, "Y", "N")
	EditOrderReturn_Other = SaveDataRecord("" & STR_TABLE_LINEITEM, Request, intItemID, dctSaveList)
	ReCalcOrder_Other intOrderID
end function

sub RemoveLineFromOrder(intItemID)
	RemoveLineFromOrder_Other gintOrderID, intItemID
end sub

sub RemoveLineFromOrder_Other(intOrderID, intItemID)
	dim dctSaveList
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "vchUpdatedByUser", "pub"
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "chrStatus", "D"
	SaveDataRecord STR_TABLE_LINEITEM, Request, intItemID, dctSaveList
	ReCalcOrder_Other intOrderID
end sub


sub RemoveLineFromOrderLT_Other(intItemID)
Dim intOrderID, rsOrder

    set rsOrder = gobjConn.execute("SELECT intOrderID, vchPartNumber, intInternalID FROM " & STR_TABLE_LINEITEM_LONG_TERM & " WHERE intID=" & intItemID)
    intOrderID = rsOrder(0)
	dim dctSaveList
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "vchUpdatedByUser", "pub"
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "chrStatus", "D"
	SaveDataRecord STR_TABLE_LINEITEM_LONG_TERM, Request, intItemID, dctSaveList
	if instr(rsOrder("vchPartNumber"), "_POD_") > 0 then 
		strSQL = "Update " & TABLE_PRODUCT_TICKETS  & " set chrStatus = 'D' where chrStatus = 'W' and intID = " & rsOrder("intInternalID") & " and intShopperID = " & Session(SESSION_PUB_USER_ID)
		gobjConn.execute(strSQL)
	end if
	ReCalcOrderLT_Other intOrderID
end sub

sub ReCalcOrder_WithPromo(PromoCode)
	strPromoCode = PromoCode
	'Response.Write strPromoCode
	'Response.End
	ReCalcOrder_Other gintOrderID
end sub

sub ReCalcOrder
	ReCalcOrder_Other gintOrderID
end sub

sub ReCalcOrderLT_Other(intOrderID)
	'SendMail "webuser@americanwebservices.com", "jhelms@americanwebservices.com", "", "", " ReCalcOrder_Other(" & intOrderID & ")", ""

	if intOrderID > 0 then
		
		' recalculate the following fields:
		'   mnyShipAmount
		'   chrShipTaxFlag
		'   mnyNonTaxSubtotal
		'   mnyTaxSubtotal
		'   fltTaxRate
		'   mnyGrandTotal
		
		' business logic:
		'   shipping is non-taxable
		'   shipping price is set in inventory, fixed price per item ordered
		
		dim strSQL, rsTemp, mnyNonTaxSubtotal, mnyTaxSubtotal, mnyShipSubtotal, mnyTaxAmount, mnyGrandTotal, fltDiscount, mnyDiscountAmount
		mnyNonTaxSubtotal = 0
		mnyTaxSubtotal = 0
		mnyShipSubtotal = 0
		mnyDiscountAmount = 0
		' step 1: calculate sum of taxable and non-taxable items ordered, and shipping cost
		strSQL = "SELECT mnyAmount,IsNull(mnyUnitPrice - (mnyUnitPrice*mnyAmount),mnyUnitPrice) AS mnyUnitPrice,intQuantity,mnyShipPrice,ISNULL(chrTaxFlag,'N') AS chrTaxFlag FROM " & STR_TABLE_LINEITEM_LONG_TERM & " L LEFT JOIN " & STR_TABLE_PROMO & " P on L.intInvId=P.intProductID  AND chrPromoCode='" & strPromoCode & "' WHERE intOrderID=" & intOrderID & " AND chrStatus='A' AND (chrType='I' OR chrType='R')"
		'strSQL = "SELECT SUM(ISNull(mnyAmount,mnyUnitPrice)*intQuantity) AS mnyPrice, SUM(mnyShipPrice*intQuantity) AS mnyShipPrice, ISNULL(chrTaxFlag,'N') AS chrTaxFlag FROM " & STR_TABLE_LINEITEM & " L LEFT JOIN " & STR_TABLE_PROMO & " P on L.intInvId=P.intProductID  AND chrPromoCode='" & strPromoCode & "' WHERE intOrderID=" & intOrderID & " AND chrStatus='A' AND (chrType='I' OR chrType='R') GROUP BY chrTaxFlag"
		'Response.Write strSQL
		'Response.End
		set rsTemp = gobjConn.execute(strSQL)
		
		Dim blnDiscountApplied
		
		blnDiscountApplied =   false
		
		while not rsTemp.eof
			Dim mnyAmount,mnyUnitPrice,mnyShipPrice,mnyPrice,intQuantity,chrTaxFlag
			
			mnyAmount = rsTemp("mnyAmount")
			mnyUnitPrice= rsTemp("mnyUnitPrice")
			mnyShipPrice= rsTemp("mnyShipPrice")
			intQuantity= rsTemp("intQuantity")
			chrTaxFlag= rsTemp("chrTaxFlag")
			
			
			mnyPrice = mnyUnitPrice * intQuantity
			
		
			
			if not IsNull(mnyPrice) then
				if lcase(chrTaxFlag) = "y" then
					mnyTaxSubtotal = mnyTaxSubtotal + mnyPrice
				else
					mnyNonTaxSubtotal = mnyNonTaxSubtotal + mnyPrice
				end if
			end if
			
			'Response.Write mnyShipPrice & " " & mnyShipSubtotal & " " & intQuantity & "<br />"
			
			if not IsNull(mnyShipPrice) then
				mnyShipSubtotal = mnyShipSubtotal +(mnyShipPrice * intQuantity)
			end if
			rsTemp.MoveNext
		wend
		'Response.End
		
		rsTemp.close
		set rsTemp = nothing
		
		' shipping cost calculation:
		dim intShipOption, mnySubtotal
		intShipOption = GetOrderShipOption_Other(intOrderID)
		mnySubtotal = mnyTaxSubtotal + mnyNonTaxSubtotal - mnyShipSubtotal
		
		'SendMail "webuser@americanwebservices.com", "jhelms@americanwebservices.com", "", "", strSubject, strMessage
		
		if intShipOption <> STR_SHIP_CUSTOM then
			mnyShipSubtotal = mnyShipSubtotal + GetShipPriceByOption(mnySubTotal, intShipOption)
			'mnyShipSubtotal = mnyShipSubtotal + PollShippingPriceByOrder(intOrderID, intShipOption, intShipWeight)
		else
			' custom shipping price set by merchant
			mnyShipSubtotal = mnyShipSubtotal '+ GetOrderShippingPrice_Other(intOrderID)
		end if
		
		' for this cart, shipping is NOT taxable!
		' add shipping cost to nontaxable subtotal
		mnyNonTaxSubtotal = mnyNonTaxSubtotal + mnyShipSubtotal
		
		' step 2: calculate tax rate based on tax zone
		dim intTaxZone, fltTaxRate
		intTaxZone = 0
		fltDiscount = 0
'		strSQL = "SELECT ISNULL(intTaxZone,0) AS intTaxZone, ISNULL(fltReferalDiscount,0) AS fltReferalDiscount FROM " & STR_TABLE_ORDER & " WHERE intID=" & intOrderID
		strSQL = "SELECT ISNULL(O.intTaxZone,0) AS intTaxZone, ISNULL(O.fltReferalDiscount,0) AS fltReferalDiscount, S.vchState, S.vchCountry"
		strSQL = strSQL & " FROM " & STR_TABLE_ORDER_LONG_TERM & " O, "  & STR_TABLE_SHOPPER & " S" 
		strSQL = strSQL & " WHERE (O.intID=" & intOrderID & ") AND (S.intID=O.intShipShopperID)"

		set rsTemp = gobjConn.execute(strSQL)
		if not rsTemp.eof then
			intTaxZone = rsTemp("intTaxZone")
			fltDiscount = rsTemp("fltReferalDiscount")
			if rsTemp("intTaxZone") = 0 AND rsTemp("vchState") = "CA"  AND  rsTemp("vchCountry") = "US" then
				intTaxZone = 1
			end if
		end if
		rsTemp.close
		set rsTemp = nothing
		fltTaxRate = FindTaxRate(intTaxZone)
		if fltTaxRate = 0 then
			' no taxes apply to this order
			mnyNonTaxSubtotal = mnyNonTaxSubtotal + mnyTaxSubtotal
			mnyTaxSubtotal = 0
			mnyTaxAmount = 0
		else
			' calculate tax
			mnyTaxAmount = mnyTaxSubtotal * fltTaxRate
		end if
		
		mnyGrandTotal = mnyNonTaxSubtotal + mnyTaxSubtotal + mnyTaxAmount
		
		' calculate discount
		if fltDiscount <> 0 then
			' discount does not apply to shipping charges
			mnyDiscountAmount = (mnyGrandTotal - mnyShipSubtotal) * (fltDiscount / 100)
		end if
			
		mnyGrandTotal = mnyGrandTotal - mnyDiscountAmount		
		
		' save results
		dim dctSaveList
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "#mnyNonTaxSubtotal", mnyNonTaxSubtotal
			dctSaveList.Add "#mnyTaxSubtotal", mnyTaxSubtotal
			dctSaveList.Add "#mnyTaxAmount", mnyTaxAmount
			dctSaveList.Add "#mnyShipAmount", mnyShipSubtotal
			dctSaveList.Add "chrShipTaxFlag", "N"
			dctSaveList.Add "#fltTaxRate", fltTaxRate
			dctSaveList.Add "#mnyDiscountAmount", mnyDiscountAmount
			dctSaveList.Add "#mnyGrandTotal", mnyGrandTotal
			dctSaveList.Add "chrPromoCode",strPromoCode
		SaveDataRecord STR_TABLE_ORDER_LONG_TERM, Request, intOrderID, dctSaveList
	end if
end sub
sub ReCalcOrder_Other(intOrderID)
	'SendMail "webuser@americanwebservices.com", "jhelms@americanwebservices.com", "", "", " ReCalcOrder_Other(" & intOrderID & ")", ""

	if intOrderID > 0 then
		
		' recalculate the following fields:
		'   mnyShipAmount
		'   chrShipTaxFlag
		'   mnyNonTaxSubtotal
		'   mnyTaxSubtotal
		'   fltTaxRate
		'   mnyGrandTotal
		
		' business logic:
		'   shipping is non-taxable
		'   shipping price is set in inventory, fixed price per item ordered
		
		dim strSQL, rsTemp, mnyNonTaxSubtotal, mnyTaxSubtotal, mnyShipSubtotal, mnyTaxAmount, mnyGrandTotal, fltDiscount, mnyDiscountAmount
		mnyNonTaxSubtotal = 0
		mnyTaxSubtotal = 0
		mnyShipSubtotal = 0
		mnyDiscountAmount = 0
		' step 1: calculate sum of taxable and non-taxable items ordered, and shipping cost
		strSQL = "SELECT mnyAmount,IsNull(mnyUnitPrice - (mnyUnitPrice*mnyAmount),mnyUnitPrice) AS mnyUnitPrice,intQuantity,mnyShipPrice,ISNULL(chrTaxFlag,'N') AS chrTaxFlag FROM " & STR_TABLE_LINEITEM & " L LEFT JOIN " & STR_TABLE_PROMO & " P on L.intInvId=P.intProductID  AND chrPromoCode='" & strPromoCode & "' WHERE intOrderID=" & intOrderID & " AND chrStatus='A' AND (chrType='I' OR chrType='R')"
		'strSQL = "SELECT SUM(ISNull(mnyAmount,mnyUnitPrice)*intQuantity) AS mnyPrice, SUM(mnyShipPrice*intQuantity) AS mnyShipPrice, ISNULL(chrTaxFlag,'N') AS chrTaxFlag FROM " & STR_TABLE_LINEITEM & " L LEFT JOIN " & STR_TABLE_PROMO & " P on L.intInvId=P.intProductID  AND chrPromoCode='" & strPromoCode & "' WHERE intOrderID=" & intOrderID & " AND chrStatus='A' AND (chrType='I' OR chrType='R') GROUP BY chrTaxFlag"
		'Response.Write strSQL
		'Response.End
		set rsTemp = gobjConn.execute(strSQL)
		
		Dim blnDiscountApplied
		
		blnDiscountApplied =   false
		
		while not rsTemp.eof
			Dim mnyAmount,mnyUnitPrice,mnyShipPrice,mnyPrice,intQuantity,chrTaxFlag
			
			mnyAmount = rsTemp("mnyAmount")
			mnyUnitPrice= rsTemp("mnyUnitPrice")
			mnyShipPrice= rsTemp("mnyShipPrice")
			intQuantity= rsTemp("intQuantity")
			chrTaxFlag= rsTemp("chrTaxFlag")
			
			
			mnyPrice = mnyUnitPrice * intQuantity
			
		
			
			if not IsNull(mnyPrice) then
				if lcase(chrTaxFlag) = "y" then
					mnyTaxSubtotal = mnyTaxSubtotal + mnyPrice
				else
					mnyNonTaxSubtotal = mnyNonTaxSubtotal + mnyPrice
				end if
			end if
			
			'Response.Write mnyShipPrice & " " & mnyShipSubtotal & " " & intQuantity & "<br />"
			
			if not IsNull(mnyShipPrice) then
				mnyShipSubtotal = mnyShipSubtotal +(mnyShipPrice * intQuantity)
			end if
			rsTemp.MoveNext
		wend
		'Response.End
		
		rsTemp.close
		set rsTemp = nothing
		
		' shipping cost calculation:
		dim intShipOption, mnySubtotal
		intShipOption = GetOrderShipOption_Other(intOrderID)
		mnySubtotal = mnyTaxSubtotal + mnyNonTaxSubtotal - mnyShipSubtotal
		
		'SendMail "webuser@americanwebservices.com", "jhelms@americanwebservices.com", "", "", strSubject, strMessage
		
		if intShipOption <> STR_SHIP_CUSTOM then
			mnyShipSubtotal = mnyShipSubtotal + GetShipPriceByOption(mnySubTotal, intShipOption)
			'mnyShipSubtotal = mnyShipSubtotal + PollShippingPriceByOrder(intOrderID, intShipOption, intShipWeight)
		else
			' custom shipping price set by merchant
			mnyShipSubtotal = mnyShipSubtotal '+ GetOrderShippingPrice_Other(intOrderID)
		end if
		
		' for this cart, shipping is NOT taxable!
		' add shipping cost to nontaxable subtotal
		mnyNonTaxSubtotal = mnyNonTaxSubtotal + mnyShipSubtotal
		
		' step 2: calculate tax rate based on tax zone
		dim intTaxZone, fltTaxRate
		intTaxZone = 0
		fltDiscount = 0
'		strSQL = "SELECT ISNULL(intTaxZone,0) AS intTaxZone, ISNULL(fltReferalDiscount,0) AS fltReferalDiscount FROM " & STR_TABLE_ORDER & " WHERE intID=" & intOrderID
		strSQL = "SELECT ISNULL(O.intTaxZone,0) AS intTaxZone, ISNULL(O.fltReferalDiscount,0) AS fltReferalDiscount, S.vchState, S.vchCountry"
		strSQL = strSQL & " FROM " & STR_TABLE_ORDER & " O, "  & STR_TABLE_SHOPPER & " S" 
		strSQL = strSQL & " WHERE (O.intID=" & intOrderID & ") AND (S.intID=O.intShipShopperID)"

		set rsTemp = gobjConn.execute(strSQL)
		if not rsTemp.eof then
			intTaxZone = rsTemp("intTaxZone")
			fltDiscount = rsTemp("fltReferalDiscount")
			if rsTemp("intTaxZone") = 0 AND rsTemp("vchState") = "CA"  AND  rsTemp("vchCountry") = "US" then
				intTaxZone = 1
			end if
		end if
		rsTemp.close
		set rsTemp = nothing
		fltTaxRate = FindTaxRate(intTaxZone)
		if fltTaxRate = 0 then
			' no taxes apply to this order
			mnyNonTaxSubtotal = mnyNonTaxSubtotal + mnyTaxSubtotal
			mnyTaxSubtotal = 0
			mnyTaxAmount = 0
		else
			' calculate tax
			mnyTaxAmount = mnyTaxSubtotal * fltTaxRate
		end if
		
		mnyGrandTotal = mnyNonTaxSubtotal + mnyTaxSubtotal + mnyTaxAmount
		
		' calculate discount
		if fltDiscount <> 0 then
			' discount does not apply to shipping charges
			mnyDiscountAmount = (mnyGrandTotal - mnyShipSubtotal) * (fltDiscount / 100)
		end if
			
		mnyGrandTotal = mnyGrandTotal - mnyDiscountAmount		
		
		' save results
		dim dctSaveList
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "#mnyNonTaxSubtotal", mnyNonTaxSubtotal
			dctSaveList.Add "#mnyTaxSubtotal", mnyTaxSubtotal
			dctSaveList.Add "#mnyTaxAmount", mnyTaxAmount
			dctSaveList.Add "#mnyShipAmount", mnyShipSubtotal
			dctSaveList.Add "chrShipTaxFlag", "N"
			dctSaveList.Add "#fltTaxRate", fltTaxRate
			dctSaveList.Add "#mnyDiscountAmount", mnyDiscountAmount
			dctSaveList.Add "#mnyGrandTotal", mnyGrandTotal
			dctSaveList.Add "chrPromoCode",strPromoCode
		SaveDataRecord STR_TABLE_ORDER, Request, intOrderID, dctSaveList
	end if
end sub

sub RemoveItemFromOrder(intID)
	if gintOrderID > 0 then
		dim strSQL
		strSQL = "UPDATE " & STR_TABLE_LINEITEM & " SET chrStatus='D' WHERE intID=" & intID & " AND intOrderID=" & gintOrderID
		gobjConn.execute(strSQL)
	end if
end sub

function FindTaxRate(byVal intTaxZone)
	FindTaxRate = 0
	dim k, c, x, s, xLen
	intTaxZone = intTaxZone & "~"
	xLen = len(intTaxZone)
	x = dctTaxZoneRates.count - 1
	k = dctTaxZoneRates.keys
	for c = 0 to x
		if left(k(c), xLen) = intTaxZone then
			' found it!
			FindTaxRate = CDbl(mid(k(c), xLen + 1)) / 100
			exit function
		end if
	next
end function

function GetTaxZoneList()
	' returns a dictionary list of tax zones {tax zone, label }
	set GetTaxZoneList = Server.CreateObject("Scripting.Dictionary")
	dim k, c, x, s, pos, i
	x = dctTaxZoneRates.count - 1
	k = dctTaxZoneRates.keys
	i = dctTaxZoneRates.items
	for c = 0 to x
		pos = InStr(k(c), "~")
		GetTaxZoneList.Add left(k(c), pos - 1), i(c)
	next
end function

function GetTaxZoneName(intTaxZone)
	GetTaxZoneName = ""
	dim k, c, x, s, xLen, i
	intTaxZone = intTaxZone & "~"
	xLen = len(intTaxZone)
	x = dctTaxZoneRates.count - 1
	k = dctTaxZoneRates.keys
	i = dctTaxZoneRates.items
	for c = 0 to x
		if left(k(c), xLen) = intTaxZone then
			' found it!
			GetTaxZoneName = i(c)
			exit function
		end if
	next
end function

sub DrawOrderCart(rsOrder, rsLineItem, blnCanEdit, strAuxMsgItem, strAuxMsgNote, strAuxMsgReturn)
%>
	<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="530">
	<TR>
		<TD><TABLE BORDER="0" CELLPADDING="1" CELLSPACING="0" WIDTH="530">
		<TR BGCOLOR="#AAAAAA">
			<TD NOWRAP><B>ITEM #</B></TD>
			<TD COLSPAN="3" WIDTH="100%" NOWRAP><B>PRODUCT</B></TD>
			<TD ALIGN="right" NOWRAP><B>SUBTOTAL</B></TD>
			<% if blnCanEdit then %>
			<TD>&nbsp;</TD>
			<% end if %>
		</TR>
<!--		<TR BGCOLOR="#AAAAAA">
			<TD>&nbsp;</TD>
			<TD NOWRAP ALIGN="right"><B>UNIT $</B></TD>
			<TD NOWRAP ALIGN="right"><B>S/H $</B></TD>
			<TD NOWRAP ALIGN="center"><B>QTY</B></TD>
			<TD NOWRAP ALIGN="right"><B>SUBTOTAL</B></TD>
			<% if blnCanEdit then %>
			<TD>&nbsp;</TD>
			<% end if %>
		</TR>-->
<%
	dim strColor
	strColor = ""
	while not rsLineItem.eof
		if strColor = "" then strColor = " BGCOLOR=""#DDDDDD""" else strColor = ""
%>
		<TR<%= strColor %>>
			<TD NOWRAP><%= rsLineItem("vchPartNumber") %>&nbsp;</TD>
			<TD COLSPAN="4"><%= rsLineItem("vchItemName") %>&nbsp;</TD>
			<% if blnCanEdit then %>
			<TD NOWRAP><%= replace(iif(rsLineItem("chrType") = "I", strAuxMsgItem, iif(rsLineItem("chrType") = "R", strAuxMsgReturn, strAuxMsgNote)), "$ID$", rsLineItem("intID")) %></TD>
			<% end if %>
		</TR>
		<% if rsLineItem("chrType") = "I" or rsLineItem("chrType") = "R" then %>
		<TR<%= strColor %>>
			<TD>&nbsp;</TD>
			<TD NOWRAP ALIGN="right"><%= SafeFormatCurrency("&nbsp;", rsLineItem("mnyUnitPrice"), 2) %></TD>
			<TD NOWRAP ALIGN="right">S/H: <%= SafeFormatCurrency("&nbsp;", rsLineItem("mnyShipPrice"), 2) %></TD>
			<TD NOWRAP ALIGN="center">Qty: <%= iif(IsNull(rsLineItem("intQuantity")), "&nbsp;", rsLineItem("intQuantity")) %></TD>
			<TD NOWRAP ALIGN="right"><%= SafeFormatCurrency("&nbsp;", rsLineItem("mnyPrice"), 2) %></TD>
			<% if blnCanEdit then %>
			<TD NOWRAP>&nbsp;</TD>
			<% end if %>
		</TR>
<%
		end if
		rsLineItem.MoveNext
	wend
	if strColor = "" then strColor = " BGCOLOR=""#DDDDDD""" else strColor = ""
%>
		<TR<%= strColor %>>
			<TD>&nbsp;</TD>
			<TD COLSPAN="4">&nbsp;</TD>
			<% if blnCanEdit then %>
			<TD>&nbsp;</TD>
			<% end if %>
		</TR>
<!--		<TR<%= strColor %>>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
			<TD>&nbsp;</TD>
			<% if blnCanEdit then %>
			<TD>&nbsp;</TD>
			<% end if %>
		</TR>-->
<%
	' Shipping: xx
	' Nontaxable Subtotal: xx
	' Taxable Subtotal: xx
	' Tax: xx
	' Total: xx
	
	dim mnyTaxSubTotal, mnyNonTaxSubTotal
	if not IsNull(rsOrder("mnyNonTaxSubtotal")) then
		mnyNonTaxSubTotal = rsOrder("mnyNonTaxSubtotal")
	else
		mnyNonTaxSubTotal = 0
	end if
	if not IsNull(rsOrder("mnyTaxSubtotal")) then
		mnyTaxSubTotal = rsOrder("mnyTaxSubTotal")
	else
		mnyTaxSubTotal = 0
	end if
	if mnyTaxSubTotal = 0 or mnyNonTaxSubTotal = 0 then
		' only one subtotal--we'll list it as "Subtotal"
		if strColor = "" then strColor = " BGCOLOR=""#DDDDDD""" else strColor = ""
%>
		<TR<%= strColor %>>
			<TD NOWRAP COLSPAN="4" ALIGN="right"><B>Subtotal:</B>&nbsp;</TD>
			<TD ALIGN="right"><%= FormatCurrency(mnyTaxSubTotal + mnyNonTaxSubTotal, 2) %></TD>
			<% if blnCanEdit then %>
			<TD>&nbsp;</TD>
			<% end if %>
		</TR>
<%
	else
		if strColor = "" then strColor = " BGCOLOR=""#DDDDDD""" else strColor = ""
%>
		<TR<%= strColor %>>
			<TD NOWRAP COLSPAN="4" ALIGN="right"><B>Taxable Subtotal:</B>&nbsp;</TD>
			<TD ALIGN="right"><%= FormatCurrency(mnyTaxSubtotal, 2) %></TD>
			<% if blnCanEdit then %>
			<TD>&nbsp;</TD>
			<% end if %>
		</TR>
<%
		if strColor = "" then strColor = " BGCOLOR=""#DDDDDD""" else strColor = ""
%>
		<TR<%= strColor %>>
			<TD NOWRAP COLSPAN="4" ALIGN="right"><B>Non-Taxable Subtotal:</B>&nbsp;</TD>
			<TD ALIGN="right"><%= FormatCurrency(mnyNonTaxSubtotal, 2) %></TD>
			<% if blnCanEdit then %>
			<TD>&nbsp;</TD>
			<% end if %>
		</TR>
<%
	end if
	if rsOrder("mnyTaxAmount") <> 0 and not IsNull(rsOrder("mnyTaxAmount")) then
		if strColor = "" then strColor = " BGCOLOR=""#DDDDDD""" else strColor = ""
%>
		<TR<%= strColor %>>
			<TD NOWRAP COLSPAN="4" ALIGN="right">(<B><%= GetTaxZoneName(rsOrder("intTaxZone")) %>) Tax @ <%= SafeFormatNumber("0", rsOrder("fltTaxRate") * 100, 2) %>%:</B>&nbsp;</TD>
			<TD ALIGN="right"><%= SafeFormatCurrency("n/a", rsOrder("mnyTaxAmount"), 2) %></TD>
			<% if blnCanEdit then %>
			<TD>&nbsp;</TD>
			<% end if %>
		</TR>
<%
	end if
	
	if strColor = "" then strColor = " BGCOLOR=""#DDDDDD""" else strColor = ""
%>
		<TR<%= strColor %>>
			<TD NOWRAP COLSPAN="4" ALIGN="right"><B>Promo Code Used</B>&nbsp;</TD>
			<TD ALIGN="right"><%=iif(isnull(rsOrder("chrPromoCode")),"None",rsOrder("chrPromoCode")) %></TD>
			<% if blnCanEdit then %>
			<TD>&nbsp;</TD>
			<% end if %>
		</TR>
<%
	if strColor = "" then strColor = " BGCOLOR=""#DDDDDD""" else strColor = ""
%>
		<TR<%= strColor %>>
			<TD NOWRAP COLSPAN="4" ALIGN="right"><B>Total:</B>&nbsp;</TD>
			<TD ALIGN="right"><%= SafeFormatCurrency("$0.00", rsOrder("mnyGrandTotal"), 2) %></TD>
			<% if blnCanEdit then %>
			<TD>&nbsp;</TD>
			<% end if %>
		</TR>
		</TABLE></TD>
	</TR>
	</TABLE>
<%
end sub

sub Merchant_DrawOrderCartLT(rsOrder, rsLineItem, blnCanEdit, strAuxMsgItem, strAuxMsgNote, strAuxMsgReturn, byVal cBGColor, byVal cBGShipColor)
	response.write GetMerchant_DrawOrderCart(rsOrder, rsLineItem, blnCanEdit, strAuxMsgItem, strAuxMsgNote, strAuxMsgReturn, cBGColor, cBGShipColor, true)
end sub

sub Merchant_DrawOrderCart(rsOrder, rsLineItem, blnCanEdit, strAuxMsgItem, strAuxMsgNote, strAuxMsgReturn, byVal cBGColor, byVal cBGShipColor)
	response.write GetMerchant_DrawOrderCart(rsOrder, rsLineItem, blnCanEdit, strAuxMsgItem, strAuxMsgNote, strAuxMsgReturn, cBGColor, cBGShipColor, false)
end sub

function GetMerchant_DrawOrderCart(rsOrder, rsLineItem, blnCanEdit, strAuxMsgItem, strAuxMsgNote, strAuxMsgReturn, byVal cBGColor, cBGShipColor, lt)
	dim s,mnyTaxSubTotal, mnyNonTaxSubTotal
	
	s = ""
	
	if cBGColor = "" or true then
		cBGColor = cVLtYellow
	end if
	s = s & "<TABLE BORDER=""0"" CELLPADDING=""0"" CELLSPACING=""0"" WIDTH=""100%"">"
	s = s & "<TR>"
	s = s & "	<TD><TABLE BORDER=""0"" CELLPADDING=""1"" CELLSPACING=""0"" WIDTH=""100%"">"
	s = s & "<TR BGCOLOR=""" & cBGColor & """>"
	s = s & "	<TD NOWRAP><B>Item #</B></TD>"
    s = s & "	<TD NOWRAP></TD>"
	s = s & "	<TD NOWRAP><B>Product</B></TD>"
    s = s & "	<TD COLSPAN=""1""><B>Quantity</B></TD>"
    s = s & "	<TD COLSPAN=""1""><B>Price</B></TD>"
	s = s & "	<TD ALIGN=""right""  NOWRAP><B>Sub-Total</B></TD>"
	s = s & "</TR>"
	dim strColor
	strColor = ""
	while not rsLineItem.eof
		'Response.Write rsLineItem("chrType") & "<<--<br />"
		if strColor = "" then strColor = " BGCOLOR=""#EEEEEE""" else strColor = ""
		s = s & "<TR" & strColor & ">"
		s = s & "	<TD NOWRAP>" & rsLineItem("vchPartNumber") & "&nbsp;</TD>"

        s = s & "	<TD COLSPAN=""1"">"
        if rsLineItem("chrStatus") = "C" Then
            s = s & "[Cancelled]"
		else
            's = s & "<a href=""?action=cancel&id=" & rsLineItem("intID") & "&orderid=" & rsLineItem("intOrderID") & """>[Cancel]</a>"
        end if

        if rsLineItem("chrStatus")="S" then
            s = s & "	Substituted for " & rsLineItem("vchPartNumber") 
        else
            dim rsItems

            if lt then
                set rsItems = gobjConn.execute("SELECT intID, vchItemName FROM  " & STR_TABLE_INVENTORY_LONG_TERM & " WHERE chrType='I' and chrStatus='A'")
            else
                set rsItems = gobjConn.execute("SELECT intID, vchItemName FROM  " & STR_TABLE_INVENTORY & " WHERE chrType='I' and chrStatus='A'")
            end if

            s = s & "<form style=""display:inline""><input type=""hidden"" name=""orderid"" value=""" & rsLineItem("intOrderID") & """ /><input type=""hidden"" name=""id"" value=""" & rsLineItem("intID") & """ /><input type=""hidden"" name=""action"" value=""substitute"" />"
            s = s & "	<a href=""#"" onclick=""document.getElementById('subselect" & rsLineItem("intID") &"').style.display='inline';this.style.display='none';return false;"" id=""sublink" & rsLineItem("intID") &""">[Substitute]</a>"
            s = s & "<select name=""sub"" id=""subselect" & rsLineItem("intID") &""" style=""display:none;"" onchange=""this.form.submit()"">"
            
            while not rsItems.eof
                s = s & "<option value=""" & rsItems("intID") &""">" & rsItems("vchItemName") &"</option>"

                rsItems.movenext
            wend

            s = s & "</select>"
            s = s & "</form>"
        end if

        s = s & "</TD>"
            
	    
        s = s & "	<TD COLSPAN=""1"">" & rsLineItem("vchItemName") & "&nbsp;</TD>"
        s = s & "	<TD COLSPAN=""1""><form><input type=""hidden"" name=""action"" value=""editsubmit"" /><input type=""hidden"" name=""chrType"" value=""I"" /><input type=""hidden"" name=""intOrderID"" value=""" & rsLineItem("intOrderID") & """ /><input type=""hidden"" name=""intLineItemID"" value=""" & rsLineItem("intID") & """ /><input type=""hidden"" name=""intInvID"" value=""" & rsLineItem("intInvID") & """ /><input name=""intQuantity"" value=""" & rsLineItem("intQuantity") & """ size=""3"" /><input type=""submit"" value=""Update"" /></form></TD>"
        s = s & "	<TD COLSPAN=""1"">" & FormatCurrency("0"&rsLineItem("mnyUnitPrice")) & "</TD>"
	    s = s & "	<TD ALIGN=""right""  NOWRAP>" & FormatCurrency("0"&rsLineItem("mnyUnitPrice") * rsLineItem("intQuantity")) & "&nbsp;</TD>"
		s = s & "</TR>"
		
		rsLineItem.MoveNext
	wend
	' Shipping: xx
	' Nontaxable Subtotal: xx
	' Taxable Subtotal: xx
	' Tax: xx
	' Total: xx
	if not IsNull(rsOrder("mnyNonTaxSubtotal")) then
		mnyNonTaxSubTotal = rsOrder("mnyNonTaxSubtotal")
	else
		mnyNonTaxSubTotal = 0
	end if
	
	'Response.Write mnyNonTaxSubTotal
	if not IsNull(rsOrder("mnyTaxSubtotal")) then
		mnyTaxSubTotal = rsOrder("mnyTaxSubTotal")
	else
		mnyTaxSubTotal = 0
	end if
	if mnyTaxSubTotal = 0 or mnyNonTaxSubTotal = 0 then
		' only one subtotal--we'll list it as "Subtotal"
		if strColor = "" then strColor = " BGCOLOR=""#EEEEEE""" else strColor = ""
		s = s & "<TR" & strColor & ">"
        s = s & "	<TD COLSPAN=""2"">&nbsp;</TD>"
        s = s & "	<TD COLSPAN=""2"">&nbsp;</TD>"
		s = s & "	<TD NOWRAP ALIGN=""right""><B>Subtotal:</B>&nbsp;</TD>"
		s = s & "	<TD ALIGN=""right"">" & FormatCurrency(mnyTaxSubTotal + mnyNonTaxSubTotal, 2) & "</TD>"
		if blnCanEdit then
			s = s & "	<TD>&nbsp;</TD>"
		end if
		s = s & "</TR>"
	else
		if strColor = "" then strColor = " BGCOLOR=""#EEEEEE""" else strColor = ""
		s = s & "<TR" & strColor & ">"
		s = s & "	<TD NOWRAP COLSPAN=""3"" ALIGN=""right""><B>Taxable Subtotal:</B>&nbsp;</TD>"
		s = s & "	<TD ALIGN=""right"">" & FormatCurrency(mnyTaxSubtotal, 2) & "</TD>"
		if blnCanEdit then
			s = s & "	<TD>&nbsp;</TD>"
		end if
		s = s & "</TR>"
		if strColor = "" then strColor = " BGCOLOR=""#EEEEEE""" else strColor = ""
		s = s & "<TR" & strColor & ">"
		s = s & "	<TD NOWRAP COLSPAN=""5"" ALIGN=""right""><B>Non-Taxable Subtotal:</B>&nbsp;</TD>"
		s = s & "	<TD ALIGN=""right"">" & FormatCurrency(mnyNonTaxSubtotal, 2) & "</TD>"
		if blnCanEdit then
			s = s & "	<TD>&nbsp;</TD>"
		end if
		s = s & "</TR>"
	end if
	if rsOrder("mnyDiscountAmount") <> 0 and not IsNull(rsOrder("mnyDiscountAmount")) then
		if strColor = "" then strColor = " BGCOLOR=""#EEEEEE""" else strColor = ""
		s = s & "<TR" & strColor & ">"
		s = s & "	<TD NOWRAP COLSPAN=""3"" ALIGN=""right""><B>" & rsOrder("vchReferalName") & " " & rsOrder("fltReferalDiscount") & "% Discount:</B>&nbsp;</TD>"
		s = s & "	<TD ALIGN=""right"">-" & SafeFormatCurrency("n/a", rsOrder("mnyDiscountAmount"), 2) & "</TD>"
		if blnCanEdit then
			s = s & "	<TD>&nbsp;</TD>"
		end if
		s = s & "</TR>"
	end if
	if rsOrder("mnyTaxAmount") <> 0 and not IsNull(rsOrder("mnyTaxAmount")) then
		if strColor = "" then strColor = " BGCOLOR=""#EEEEEE""" else strColor = ""
		s = s & "<TR" & strColor & ">"
		s = s & "	<TD NOWRAP COLSPAN=""3"" ALIGN=""right""><B>(" & GetTaxZoneName(rsOrder("intTaxZone")) & ") Tax @ " & SafeFormatNumber("0", rsOrder("fltTaxRate") * 100, 2) & "%:</B>&nbsp;</TD>"
		s = s & "	<TD ALIGN=""right"">" & SafeFormatCurrency("n/a", rsOrder("mnyTaxAmount"), 2) & "</TD>"
		if blnCanEdit then
			s = s & "	<TD>&nbsp;</TD>"
		end if
		s = s & "</TR>"
	end if
	
	if strColor = "" then strColor = " BGCOLOR=""#EEEEEE""" else strColor = ""
	s = s & "<TR" & strColor & ">"
    s = s & "	<TD COLSPAN=""2"">&nbsp;</TD>"
    s = s & "	<TD COLSPAN=""2"">&nbsp;</TD>"
	s = s & "	<TD NOWRAP ALIGN=""right""><B>Total:</B>&nbsp;</TD>"
	s = s & "	<TD ALIGN=""right"">" & SafeFormatCurrency("$0.00", rsOrder("mnyGrandTotal"), 2) & "</TD>"
	if blnCanEdit then
		s = s & "	<TD>&nbsp;</TD>"
	end if
	s = s & "</TR>"
	s = s & "</TABLE>"
	GetMerchant_DrawOrderCart = s
end function

' rkonvalin - added Brand_DrawOrderCart per w/o #991 2/7/03
' rkonvalin - changed Brand_DrawOrderCart per - James Dutra email 10/23/03
' does not show dollar amounts on orders so Brand (Vendor) can see/fill/ship items on orders by brand
' changed to show dollar amounts on orders so Vendor can easily cross reference order.

sub Brand_DrawOrderCart(rsOrder, rsLineItem, blnCanEdit, strAuxMsgItem, strAuxMsgNote, strAuxMsgReturn, byVal cBGColor, byVal cBGShipColor)
	response.write GetBrand_DrawOrderCart(rsOrder, rsLineItem, blnCanEdit, strAuxMsgItem, strAuxMsgNote, strAuxMsgReturn, cBGColor, cBGShipColor)
end sub

function GetBrand_DrawOrderCart(rsOrder, rsLineItem, blnCanEdit, strAuxMsgItem, strAuxMsgNote, strAuxMsgReturn, byVal cBGColor, cBGShipColor)
	dim s, strColor, intColspan, blnShipNumber
	strColor = ""
	s = ""
	
	if cBGColor = "" then
		cBGColor = cVLtYellow
	end if
	s = s & "<TABLE BORDER=""0"" CELLPADDING=""1"" CELLSPACING=""0"" WIDTH=""100%"">"
	s = s & "<TR BGCOLOR=""" & cBGColor & """>"
	s = s & "	<TD NOWRAP><B>Item #</B></TD>"
	s = s & "	<TD COLSPAN=4 NOWRAP><B>Product</B></TD>"
	if blnCanEdit then
		s = s & "	<TD>&nbsp;</TD>"
	end if
	s = s & "	<TD NOWRAP><B>Ship Date</B></TD>"
	s = s & "	<TD NOWRAP><B>Shipping #</B></TD>"
	if blnCanEdit then
		s = s & "	<TD>&nbsp;</TD>"
	end if
	s = s & "</TR>"
	while not rsLineItem.eof
		blnShipNumber = IsValid(rsLineItem("vchShippingNumber"))
		if rsLineItem("chrType") = "I" or rsLineItem("chrType") = "R" then
			intColspan = 1
		else
			intColspan = 4
		end if
		'intColspan = intColspan + iif(blnCanEdit,0,1)
		if strColor = "" then strColor = " BGCOLOR=""#EEEEEE""" else strColor = ""
		s = s & "<TR" & strColor & ">"
		s = s & "	<TD NOWRAP>" & rsLineItem("vchPartNumber") & "&nbsp;</TD>"
		s = s & "	<TD COLSPAN=" & intColspan & " WIDTH=""42%"">" & rsLineItem("vchItemName") & "&nbsp;</TD>"
		if blnCanEdit then
			s = s & "	<TD NOWRAP>" & replace(iif(rsLineItem("chrType") = "I", strAuxMsgItem, iif(rsLineItem("chrType") = "R", strAuxMsgReturn, strAuxMsgNote)), "$ID$", rsLineItem("intID")) & "</TD>"
		end if
		if rsLineItem("chrType") = "I" or rsLineItem("chrType") = "R" then
			s = s & "	<TD>" & iif(rsLineItem("chrType") = "R","RETURNED","&nbsp;") & "</TD>"
			s = s & "	<TD ALIGN=""right"">&nbsp;"
			if (not IsNull(rsLineItem("intForceShipMethod"))) then
				s = s & "Must Ship By " & GetArrayValue(rsLineItem("intForceShipMethod"), dctShipOption)
			end if
			if (rsLineItem("chrForceSoloItem") & "" = "Y") then
				s = s & "&nbsp;" & "(Individually)"
			end if
			s = s & "</TD>"
			s = s & "	<TD NOWRAP ALIGN=""center"">&nbsp;Qty: " & iif(IsNull(rsLineItem("intQuantity")), "", rsLineItem("intQuantity")) & "&nbsp;</TD>"
		end if
		s = s & "	<TD NOWRAP ALIGN=""center"">" & iif(blnShipNumber, SafeFormatDateTime("", rsLineItem("dtmUpdated"), 2), "") & "&nbsp;</TD>"
		s = s & "	<TD NOWRAP ALIGN=""center"">" & iif(blnShipNumber, rsLineItem("vchShippingNumber"), "") & "&nbsp;</TD>"
		if blnCanEdit then
			s = s & "	<TD>&nbsp;</TD>"
		end if
		s = s & "</TR>"
		rsLineItem.MoveNext
	wend
	s = s & "</TABLE>"
	s = s & "<TABLE BORDER=""0"" CELLPADDING=""0"" CELLSPACING=""0"" WIDTH=""100%"">"

	if strColor = "" then strColor = " BGCOLOR=""#EEEEEE""" else strColor = ""
	s = s & "<TR" & strColor & ">"
	s = s & "	<TD COLSPAN=""6"">&nbsp;</TD>"
	s = s & "	<TD>&nbsp;</TD>"
	if blnCanEdit then
		s = s & "	<TD>&nbsp;</TD>"
	end if
	s = s & "</TR>"

	' Shipping: xx
	' Nontaxable Subtotal: xx
	' Taxable Subtotal: xx
	' Tax: xx
	' Total: xx
	
	dim mnyTaxSubTotal, mnyNonTaxSubTotal
	if not IsNull(rsOrder("mnyNonTaxSubtotal")) then
		mnyNonTaxSubTotal = rsOrder("mnyNonTaxSubtotal")
	else
		mnyNonTaxSubTotal = 0
	end if
	if not IsNull(rsOrder("mnyTaxSubtotal")) then
		mnyTaxSubTotal = rsOrder("mnyTaxSubTotal")
	else
		mnyTaxSubTotal = 0
	end if
	if mnyTaxSubTotal = 0 or mnyNonTaxSubTotal = 0 then
		' only one subtotal--we'll list it as "Subtotal"
		if strColor = "" then strColor = " BGCOLOR=""#EEEEEE""" else strColor = ""
		s = s & "<TR" & strColor & ">"
		s = s & "	<TD NOWRAP COLSPAN=""6"" ALIGN=""right""><B>Subtotal:</B>&nbsp;</TD>"
		s = s & "	<TD ALIGN=""right"">" & FormatCurrency(mnyTaxSubTotal + mnyNonTaxSubTotal, 2) & "</TD>"
		if blnCanEdit then
			s = s & "	<TD>&nbsp;</TD>"
		end if
		s = s & "</TR>"
	else
		if strColor = "" then strColor = " BGCOLOR=""#EEEEEE""" else strColor = ""
		s = s & "<TR" & strColor & ">"
		s = s & "	<TD NOWRAP COLSPAN=""6"" ALIGN=""right""><B>Taxable Subtotal:</B>&nbsp;</TD>"
		s = s & "	<TD ALIGN=""right"">" & FormatCurrency(mnyTaxSubtotal, 2) & "</TD>"
		if blnCanEdit then
			s = s & "	<TD>&nbsp;</TD>"
		end if
		s = s & "</TR>"
		if strColor = "" then strColor = " BGCOLOR=""#EEEEEE""" else strColor = ""
		s = s & "<TR" & strColor & ">"
		s = s & "	<TD NOWRAP COLSPAN=""6"" ALIGN=""right""><B>Non-Taxable Subtotal:</B>&nbsp;</TD>"
		s = s & "	<TD ALIGN=""right"">" & FormatCurrency(mnyNonTaxSubtotal, 2) & "</TD>"
		if blnCanEdit then
			s = s & "	<TD>&nbsp;</TD>"
		end if
		s = s & "</TR>"
	end if
	if rsOrder("mnyDiscountAmount") <> 0 and not IsNull(rsOrder("mnyDiscountAmount")) then
		if strColor = "" then strColor = " BGCOLOR=""#EEEEEE""" else strColor = ""
		s = s & "<TR" & strColor & ">"
		s = s & "	<TD NOWRAP COLSPAN=""6"" ALIGN=""right""><B>" & rsOrder("vchReferalName") & " " & rsOrder("fltReferalDiscount") & "% Discount:</B>&nbsp;</TD>"
		s = s & "	<TD ALIGN=""right"">-" & SafeFormatCurrency("n/a", rsOrder("mnyDiscountAmount"), 2) & "</TD>"
		if blnCanEdit then
			s = s & "	<TD>&nbsp;</TD>"
		end if
		s = s & "</TR>"
	end if
	if rsOrder("mnyTaxAmount") <> 0 and not IsNull(rsOrder("mnyTaxAmount")) then
		if strColor = "" then strColor = " BGCOLOR=""#EEEEEE""" else strColor = ""
		s = s & "<TR" & strColor & ">"
		s = s & "	<TD NOWRAP COLSPAN=""6"" ALIGN=""right""><B>(" & GetTaxZoneName(rsOrder("intTaxZone")) & ") Tax @ " & SafeFormatNumber("0", rsOrder("fltTaxRate") * 100, 2) & "%:</B>&nbsp;</TD>"
		s = s & "	<TD ALIGN=""right"">" & SafeFormatCurrency("n/a", rsOrder("mnyTaxAmount"), 2) & "</TD>"
		if blnCanEdit then
			s = s & "	<TD>&nbsp;</TD>"
		end if
		s = s & "</TR>"
	end if
	
	if strColor = "" then strColor = " BGCOLOR=""#DDDDDD""" else strColor = ""
		s = s & "<TR" & strColor & ">"
		s = s & "<TD NOWRAP COLSPAN=""6"" ALIGN=""right""><B>Promo Code Used</B>&nbsp;</TD>"
		s = s & "<TD ALIGN=""right"">" &  iif(isnull(rsOrder("chrPromoCode")),"None",rsOrder("chrPromoCode")) & "</TD>"
		
		if blnCanEdit then 
			s = s & "<TD>&nbsp;</TD>"
		end if
		s = s & "</TR>"

	
	if strColor = "" then strColor = " BGCOLOR=""#EEEEEE""" else strColor = ""
	s = s & "<TR" & strColor & ">"
	s = s & "	<TD NOWRAP COLSPAN=""6"" ALIGN=""right""><B>Total:</B>&nbsp;</TD>"
	s = s & "	<TD ALIGN=""right"">" & SafeFormatCurrency("$0.00", rsOrder("mnyGrandTotal"), 2) & "</TD>"
	if blnCanEdit then
		s = s & "	<TD>&nbsp;</TD>"
	end if
	s = s & "</TR>"
	s = s & "</TABLE>"
	GetBrand_DrawOrderCart = s
end function

Function getRealWords(inArray)
 Dim lCount, lCurWord, i, j, isGood
 dim checkArr
 checkArr = Array("a","and","the","in","of","or")
 Redim tempArray(UBound(inArray)+1)
 lCount = 0
 For i = 0 to UBound(inArray)
	lCurWord = inArray(i)
'	response.write lCurWord & "<br />"
	if len(lCurWord) > 0 then
		isGood = true
		For j = 0 to UBound(checkArr)
			if lCurWord = checkArr(j) then
				isGood = false
				exit For
			end if
		Next
		if isGood then
			tempArray(lCount) = lCurWord
			lCount = lCount + 1
		end if
	end if
 Next
 if lCount > 0 then
	lCount = lCount - 1
 end if
 ReDim Preserve tempArray(lCount)
 getRealWords = tempArray
End function


function Inventory_GetItemsFromFolderLt(intParentID, blnGetFolders, blnGetItems)
	dim strSQL

   '  strSQL ="SELECT vchFormat from " & STR_TABLE_SHOPPER & " WHERE intID=" & gintUserID
   ' dim rsUser
   ' set rsUser = gobjConn.execute(strSQL)
   
	strSQL = "select vchGroups from " & STR_TABLE_SHOPPER & " WHERE intID=" & gintUserID
	dim rsUser, strSplitGroups, x, i
    set rsUser = gobjConn.execute(strSQL)

	strSQL = "SELECT *"
	strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY_LONG_TERM
	strSQL = strSQL & " WHERE chrStatus='A' AND intParentID=" & intParentID
	if blnGetFolders and not blnGetItems then
		strSQL = strSQL & " AND chrType='A'"
	elseif not blnGetFolders and blnGetItems then
		strSQL = strSQL & " AND chrType='I'"
	end if
	strSQL = strSQL & " AND chrType<>'B'"
	
	if not rsUser.eof then
		strSplitGroups = split(rsUser("vchGroups")&"",",")
		if rsUser("vchGroups")&"" <> "" then
		strSQL = strSQL & " And ("
		i = 0
			for each x in strSplitGroups
				if i > 0 then
					strSQL = strSQL & " OR "
				end if
				strSQL = strSQL & " vchGroupingIDs LIKE '%, " & x & ",%' " '--middle
				strSQL = strSQL & " OR "
				strSQL = strSQL & " vchGroupingIDs LIKE '" & x & ",%'" ' --start
				strSQL = strSQL & " OR "
				strSQL = strSQL & " vchGroupingIDs LIKE '%, " & x & "' " '--end
				strSQL = strSQL & " OR "
				strSQL = strSQL & " vchGroupingIDs =  '" & x & "'  "
				i = i + 1
			next
			strSQL = strSQL & " OR vchGroupingIDs is null ) "
		else
			strSQL = strSQL & " And vchGroupingIDs is null  "
		end if
	else
		strSQL = strSQL & " And vchGroupingIDs is null  "
	end if
	'response.write strSQL
   
   ' if not rsUser.Eof then
            
   '     if rsUser(0)&"" = "1.0" then
   '         strSQL = strSQL & " AND ((intBrand=15 AND chrType<>'A') OR chrType='A')"
   '     elseif rsUser(0)&"" = "2.0" then
   '         strSQL = strSQL & " AND ((intBrand=16 AND chrType<>'A') OR chrType='A')"
   '     end if

    'end if


	strSQL = strSQL & " ORDER BY intCategory, vchItemName"
	set Inventory_GetItemsFromFolderLt = gobjConn.execute(strSQL)	
end function

function Inventory_GetItemsFromFolderLtEx2(intParentID,intBrandID, blnGetFolders, blnGetItems)
	dim strSQL

   '  strSQL ="SELECT vchFormat from " & STR_TABLE_SHOPPER & " WHERE intID=" & gintUserID
   ' dim rsUser
   ' set rsUser = gobjConn.execute(strSQL)
   
	strSQL = "select vchGroups from " & STR_TABLE_SHOPPER & " WHERE intID=" & gintUserID
	dim rsUser, strSplitGroups, x, i
    set rsUser = gobjConn.execute(strSQL)

	strSQL = "SELECT *"
	strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY_LONG_TERM
	strSQL = strSQL & " WHERE chrStatus='A' AND intBrand in (" & intBrandID &")"
	if blnGetFolders and not blnGetItems then
		strSQL = strSQL & " AND chrType='A'"
	elseif not blnGetFolders and blnGetItems then
		strSQL = strSQL & " AND chrType='I'"
	end if
	strSQL = strSQL & " AND chrType<>'B'"
	i = 0 
	if not rsUser.eof then
		strSplitGroups = split(rsUser("vchGroups")&"",",")
		if rsUser("vchGroups")&"" <> "" then
		strSQL = strSQL & " And ("
			for each x in strSplitGroups
				if i > 0 then
					strSQL = strSQL & " OR "
				end if
				strSQL = strSQL & " vchGroupingIDs LIKE '%, " & x & ",%' " '--middle
				strSQL = strSQL & " OR "
				strSQL = strSQL & " vchGroupingIDs LIKE '" & x & ",%'" ' --start
				strSQL = strSQL & " OR "
				strSQL = strSQL & " vchGroupingIDs LIKE '%, " & x & "' " '--end
				strSQL = strSQL & " OR "
				strSQL = strSQL & " vchGroupingIDs =  '" & x & "'  "
				i = i + 1
			next
			strSQL = strSQL & " OR vchGroupingIDs is null ) "
		else
			strSQL = strSQL & " And vchGroupingIDs is null  "
		end if
	else
		strSQL = strSQL & " And vchGroupingIDs is null  "
	end if

  ' Response.Write strSQL
   ' if not rsUser.Eof then
            
   '     if rsUser(0)&"" = "1.0" then
   '         strSQL = strSQL & " AND ((intBrand=15 AND chrType<>'A') OR chrType='A')"
   '     elseif rsUser(0)&"" = "2.0" then
   '         strSQL = strSQL & " AND ((intBrand=16 AND chrType<>'A') OR chrType='A')"
   '     end if

    'end if


	strSQL = strSQL & " ORDER BY intCategory, vchItemName"
	SET Inventory_GetItemsFromFolderLtEx2 = Server.CreateObject("ADODB.recordset")

    Inventory_GetItemsFromFolderLtEx2.CursorLocation = adUseClient
    Inventory_GetItemsFromFolderLtEx2.CursorType = adOpenStatic
    Inventory_GetItemsFromFolderLtEx2.LockType = adLockBatchOptimistic

	Inventory_GetItemsFromFolderLtEx2.Open strSQL, gobjConn	
end function


function Inventory_GetItemsFromFolderLtByNumber(posNumber, intItemSuperCategory)
	dim strSQL

   '  strSQL ="SELECT vchFormat from " & STR_TABLE_SHOPPER & " WHERE intID=" & gintUserID
   ' dim rsUser
   ' set rsUser = gobjConn.execute(strSQL)
   
	strSQL = "select vchGroups from " & STR_TABLE_SHOPPER & " WHERE intID=" & gintUserID
	dim rsUser, strSplitGroups, x, i
    set rsUser = gobjConn.execute(strSQL)

	strSQL = "SELECT *"
	strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY_LONG_TERM
	strSQL = strSQL & " WHERE chrStatus='A'  AND (vchItemName like '%" & posNumber & "%'"
    if IsNumeric(posNumber) and posNumber<>"" then
    strSQL = strSQL & "  Or intID=" & posNumber & ") "
    else
    strSQL = strSQL & " )"
    end if
    strSQL = strSQL & " AND chrType='I' and intBrand in (select intId from " & STR_TABLE_INVENTORY_LONG_TERM  & " where intItemSuperCategory = " & intItemSuperCategory & ") "
	i = 0
	if not rsUser.eof then
		strSplitGroups = split(rsUser("vchGroups")&"",",")
		if rsUser("vchGroups")&"" <> "" then
		strSQL = strSQL & " And ("
			for each x in strSplitGroups
				if i > 0 then
					strSQL = strSQL & " OR "
				end if
				strSQL = strSQL & " vchGroupingIDs LIKE '%, " & x & ",%' " '--middle
				strSQL = strSQL & " OR "
				strSQL = strSQL & " vchGroupingIDs LIKE '" & x & ",%'" ' --start
				strSQL = strSQL & " OR "
				strSQL = strSQL & " vchGroupingIDs LIKE '%, " & x & "' " '--end
				strSQL = strSQL & " OR "
				strSQL = strSQL & " vchGroupingIDs =  '" & x & "'  "
				i = i + 1
			next
			strSQL = strSQL & " OR vchGroupingIDs is null ) "
		else
			strSQL = strSQL & " And vchGroupingIDs is null  "
		end if
	else
		strSQL = strSQL & " And vchGroupingIDs is null  "
	end if
	
	'response.write strSQL
	
   ' if not rsUser.Eof then
            
   '     if rsUser(0)&"" = "1.0" then
   '         strSQL = strSQL & " AND ((intBrand=15 AND chrType<>'A') OR chrType='A')"
   '     elseif rsUser(0)&"" = "2.0" then
   '         strSQL = strSQL & " AND ((intBrand=16 AND chrType<>'A') OR chrType='A')"
   '     end if

    'end if


	strSQL = strSQL & "  ORDER BY intCategory, vchItemName"
	SET Inventory_GetItemsFromFolderLtByNumber = gobjConn.execute(strSQL)
end function

function Inventory_GetItemsFromFolderLtEx3(intParentID,intBrandID,intCategoryId, blnGetFolders, blnGetItems)
	dim strSQL

   '  strSQL ="SELECT vchFormat from " & STR_TABLE_SHOPPER & " WHERE intID=" & gintUserID
   ' dim rsUser
   ' set rsUser = gobjConn.execute(strSQL)
   
	strSQL = "select vchGroups from " & STR_TABLE_SHOPPER & " WHERE intID=" & gintUserID
	dim rsUser, strSplitGroups, x, i
    set rsUser = gobjConn.execute(strSQL)

	strSQL = "SELECT *"
	strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY_LONG_TERM
	strSQL = strSQL & " WHERE chrStatus='A'  AND intBrand in (" & intBrandID & ") AND intCategory=" & intCategoryId
	if blnGetFolders and not blnGetItems then
		strSQL = strSQL & " AND chrType='A'"
	elseif not blnGetFolders and blnGetItems then
		strSQL = strSQL & " AND chrType='I'"
	end if
	strSQL = strSQL & " AND chrType<>'B'"
	i = 0
	if not rsUser.eof then
		strSplitGroups = split(rsUser("vchGroups")&"",",")
		if rsUser("vchGroups")&"" <> "" then
		strSQL = strSQL & " And ("
			for each x in strSplitGroups
				if i > 0 then
					strSQL = strSQL & " OR "
				end if
				strSQL = strSQL & " vchGroupingIDs LIKE '%, " & x & ",%' " '--middle
				strSQL = strSQL & " OR "
				strSQL = strSQL & " vchGroupingIDs LIKE '" & x & ",%'" ' --start
				strSQL = strSQL & " OR "
				strSQL = strSQL & " vchGroupingIDs LIKE '%, " & x & "' " '--end
				strSQL = strSQL & " OR "
				strSQL = strSQL & " vchGroupingIDs =  '" & x & "'  "
				i = i + 1
			next
			strSQL = strSQL & " OR vchGroupingIDs is null ) "
		else
			strSQL = strSQL & " And vchGroupingIDs is null  "
		end if
	else
		strSQL = strSQL & " And vchGroupingIDs is null  "
	end if

   
   ' if not rsUser.Eof then
            
   '     if rsUser(0)&"" = "1.0" then
   '         strSQL = strSQL & " AND ((intBrand=15 AND chrType<>'A') OR chrType='A')"
   '     elseif rsUser(0)&"" = "2.0" then
   '         strSQL = strSQL & " AND ((intBrand=16 AND chrType<>'A') OR chrType='A')"
   '     end if

    'end if


	strSQL = strSQL & " ORDER BY intCategory, vchItemName"
	SET Inventory_GetItemsFromFolderLtEx3 = Server.CreateObject("ADODB.recordset")

    Inventory_GetItemsFromFolderLtEx3.CursorLocation = adUseClient
    Inventory_GetItemsFromFolderLtEx3.CursorType = adOpenStatic
    Inventory_GetItemsFromFolderLtEx3.LockType = adLockBatchOptimistic

	Inventory_GetItemsFromFolderLtEx3.Open strSQL, gobjConn	
end function


function Inventory_GetItemsFromFolderLtEx4(intParentID,intCategoryID, blnGetFolders, blnGetItems)
	dim strSQL

   '  strSQL ="SELECT vchFormat from " & STR_TABLE_SHOPPER & " WHERE intID=" & gintUserID
   ' dim rsUser
   ' set rsUser = gobjConn.execute(strSQL)
   
	strSQL = "select vchGroups from " & STR_TABLE_SHOPPER & " WHERE intID=" & gintUserID
	dim rsUser, strSplitGroups, x, i
    set rsUser = gobjConn.execute(strSQL)

	strSQL = "SELECT *"
	strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY_LONG_TERM
	strSQL = strSQL & " WHERE chrStatus='A' AND intCategory=" & intCategoryId
	if blnGetFolders and not blnGetItems then
		strSQL = strSQL & " AND chrType='A'"
	elseif not blnGetFolders and blnGetItems then
		strSQL = strSQL & " AND chrType='I'"
	end if
	strSQL = strSQL & " AND chrType<>'B'"
	
	if not rsUser.eof then
		strSplitGroups = split(rsUser("vchGroups")&"",",")
		if rsUser("vchGroups")&"" <> "" then
		strSQL = strSQL & " And ("
		i = 0
			for each x in strSplitGroups
				if i > 0 then
					strSQL = strSQL & " OR "
				end if
				strSQL = strSQL & " vchGroupingIDs LIKE '%, " & x & ",%' " '--middle
				strSQL = strSQL & " OR "
				strSQL = strSQL & " vchGroupingIDs LIKE '" & x & ",%'" ' --start
				strSQL = strSQL & " OR "
				strSQL = strSQL & " vchGroupingIDs LIKE '%, " & x & "' " '--end
				strSQL = strSQL & " OR "
				strSQL = strSQL & " vchGroupingIDs =  '" & x & "'  "
				i = i + 1
			next
			strSQL = strSQL & " OR vchGroupingIDs is null ) "
		else
			strSQL = strSQL & " And vchGroupingIDs is null  "
		end if
	else
		strSQL = strSQL & " And vchGroupingIDs is null  "
	end if

  ' Response.Write strSQL
   ' if not rsUser.Eof then
            
   '     if rsUser(0)&"" = "1.0" then
   '         strSQL = strSQL & " AND ((intBrand=15 AND chrType<>'A') OR chrType='A')"
   '     elseif rsUser(0)&"" = "2.0" then
   '         strSQL = strSQL & " AND ((intBrand=16 AND chrType<>'A') OR chrType='A')"
   '     end if

    'end if


	strSQL = strSQL & " ORDER BY intCategory, vchItemName"
	SET Inventory_GetItemsFromFolderLtEx4 = Server.CreateObject("ADODB.recordset")

    Inventory_GetItemsFromFolderLtEx4.CursorLocation = adUseClient
    Inventory_GetItemsFromFolderLtEx4.CursorType = adOpenStatic
    Inventory_GetItemsFromFolderLtEx4.LockType = adLockBatchOptimistic

	Inventory_GetItemsFromFolderLtEx4.Open strSQL, gobjConn	
end function

function Inventory_GetItemsFromFolder(intParentID, blnGetFolders, blnGetItems)
	dim strSQL

   '  strSQL ="SELECT vchFormat from " & STR_TABLE_SHOPPER & " WHERE intID=" & gintUserID
   ' dim rsUser
   ' set rsUser = gobjConn.execute(strSQL)

	strSQL = "SELECT *"
	strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY
	strSQL = strSQL & " WHERE chrStatus='A' AND intParentID=" & intParentID
	if blnGetFolders and not blnGetItems then
		strSQL = strSQL & " AND chrType='A'"
	elseif not blnGetFolders and blnGetItems then
		strSQL = strSQL & " AND chrType='I'"
	end if
	strSQL = strSQL & " AND chrType<>'B'"

   
   ' if not rsUser.Eof then
            
   '     if rsUser(0)&"" = "1.0" then
   '         strSQL = strSQL & " AND ((intBrand=15 AND chrType<>'A') OR chrType='A')"
   '     elseif rsUser(0)&"" = "2.0" then
   '         strSQL = strSQL & " AND ((intBrand=16 AND chrType<>'A') OR chrType='A')"
   '     end if

    'end if


	strSQL = strSQL & " ORDER BY vchPartNumber"
	set Inventory_GetItemsFromFolder = gobjConn.execute(strSQL)	
end function

function Inventory_GetItemsFromFolderEx2(intParentID,intProgramID, blnGetFolders, blnGetItems)
	dim strSQL

   '  strSQL ="SELECT vchFormat from " & STR_TABLE_SHOPPER & " WHERE intID=" & gintUserID
   ' dim rsUser
   ' set rsUser = gobjConn.execute(strSQL)

	strSQL = "SELECT *"
	strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY
	strSQL = strSQL & " WHERE chrStatus='A' AND intParentID=" & intParentID & " AND intProgram=" & intProgramID
	if blnGetFolders and not blnGetItems then
		strSQL = strSQL & " AND chrType='A'"
	elseif not blnGetFolders and blnGetItems then
		strSQL = strSQL & " AND chrType='I'"
	end if
	strSQL = strSQL & " AND chrType<>'B'"

   
   ' if not rsUser.Eof then
            
   '     if rsUser(0)&"" = "1.0" then
   '         strSQL = strSQL & " AND ((intBrand=15 AND chrType<>'A') OR chrType='A')"
   '     elseif rsUser(0)&"" = "2.0" then
   '         strSQL = strSQL & " AND ((intBrand=16 AND chrType<>'A') OR chrType='A')"
   '     end if

    'end if


	strSQL = strSQL & " ORDER BY vchPartNumber"
	SET Inventory_GetItemsFromFolderEx2 = Server.CreateObject("ADODB.recordset")

    Inventory_GetItemsFromFolderEx2.CursorLocation = adUseClient
    Inventory_GetItemsFromFolderEx2.CursorType = adOpenStatic
    Inventory_GetItemsFromFolderEx2.LockType = adLockBatchOptimistic

	Inventory_GetItemsFromFolderEx2.Open strSQL, gobjConn	
end function


function Inventory_GetItemsFromFolderEx3(intParentID,intProgramID,intBrandID, blnGetFolders, blnGetItems)
	dim strSQL

   '  strSQL ="SELECT vchFormat from " & STR_TABLE_SHOPPER & " WHERE intID=" & gintUserID
   ' dim rsUser
   ' set rsUser = gobjConn.execute(strSQL)

	strSQL = "SELECT *"
	strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY
	strSQL = strSQL & " WHERE chrStatus='A' AND intParentID=" & intParentID & " AND intBrand=" & intBrandID & " AND intProgram=" & intProgramID
	if blnGetFolders and not blnGetItems then
		strSQL = strSQL & " AND chrType='A'"
	elseif not blnGetFolders and blnGetItems then
		strSQL = strSQL & " AND chrType='I'"
	end if
	strSQL = strSQL & " AND chrType<>'B'"

   
   ' if not rsUser.Eof then
            
   '     if rsUser(0)&"" = "1.0" then
   '         strSQL = strSQL & " AND ((intBrand=15 AND chrType<>'A') OR chrType='A')"
   '     elseif rsUser(0)&"" = "2.0" then
   '         strSQL = strSQL & " AND ((intBrand=16 AND chrType<>'A') OR chrType='A')"
   '     end if

    'end if


	strSQL = strSQL & " ORDER BY vchPartNumber"
	SET Inventory_GetItemsFromFolderEx3 = Server.CreateObject("ADODB.recordset")

    Inventory_GetItemsFromFolderEx3.CursorLocation = adUseClient
    Inventory_GetItemsFromFolderEx3.CursorType = adOpenStatic
    Inventory_GetItemsFromFolderEx3.LockType = adLockBatchOptimistic

	Inventory_GetItemsFromFolderEx3.Open strSQL, gobjConn	
end function


function Inventory_GetItemsFromFolderEx4(intParentID,intCampaignID, blnGetFolders, blnGetItems)
	dim strSQL

   '  strSQL ="SELECT vchFormat from " & STR_TABLE_SHOPPER & " WHERE intID=" & gintUserID
   ' dim rsUser
   ' set rsUser = gobjConn.execute(strSQL)

	strSQL = "SELECT *"
	strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY
	strSQL = strSQL & " WHERE chrStatus='A' AND intParentID=" & intParentID & " AND intCampaign=" & intCampaignID
	if blnGetFolders and not blnGetItems then
		strSQL = strSQL & " AND chrType='A'"
	elseif not blnGetFolders and blnGetItems then
		strSQL = strSQL & " AND chrType='I'"
	end if
	strSQL = strSQL & " AND chrType<>'B'"

   
   ' if not rsUser.Eof then
            
   '     if rsUser(0)&"" = "1.0" then
   '         strSQL = strSQL & " AND ((intBrand=15 AND chrType<>'A') OR chrType='A')"
   '     elseif rsUser(0)&"" = "2.0" then
   '         strSQL = strSQL & " AND ((intBrand=16 AND chrType<>'A') OR chrType='A')"
   '     end if

    'end if


	strSQL = strSQL & " ORDER BY vchPartNumber"
	SET Inventory_GetItemsFromFolderEx4 = Server.CreateObject("ADODB.recordset")

    Inventory_GetItemsFromFolderEx4.CursorLocation = adUseClient
    Inventory_GetItemsFromFolderEx4.CursorType = adOpenStatic
    Inventory_GetItemsFromFolderEx4.LockType = adLockBatchOptimistic

	Inventory_GetItemsFromFolderEx4.Open strSQL, gobjConn	
end function


function Inventory_GetItemsFromFolderEx5(intParentID,intCampaignID,intBrandID, blnGetFolders, blnGetItems)
	dim strSQL

   '  strSQL ="SELECT vchFormat from " & STR_TABLE_SHOPPER & " WHERE intID=" & gintUserID
   ' dim rsUser
   ' set rsUser = gobjConn.execute(strSQL)

	strSQL = "SELECT *"
	strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY
	strSQL = strSQL & " WHERE chrStatus='A' AND intParentID=" & intParentID & " AND intBrand in (" & intBrandID & ") AND intCampaign=" & intCampaignID
    if blnGetFolders and not blnGetItems then
		strSQL = strSQL & " AND chrType='A'"
	elseif not blnGetFolders and blnGetItems then
		strSQL = strSQL & " AND chrType='I'"
	end if
	strSQL = strSQL & " AND chrType<>'B'"

   
   ' if not rsUser.Eof then
            
   '     if rsUser(0)&"" = "1.0" then
   '         strSQL = strSQL & " AND ((intBrand=15 AND chrType<>'A') OR chrType='A')"
   '     elseif rsUser(0)&"" = "2.0" then
   '         strSQL = strSQL & " AND ((intBrand=16 AND chrType<>'A') OR chrType='A')"
   '     end if

    'end if


	strSQL = strSQL & " ORDER BY vchPartNumber"
	SET Inventory_GetItemsFromFolderEx5 = Server.CreateObject("ADODB.recordset")

    Inventory_GetItemsFromFolderEx5.CursorLocation = adUseClient
    Inventory_GetItemsFromFolderEx5.CursorType = adOpenStatic
    Inventory_GetItemsFromFolderEx5.LockType = adLockBatchOptimistic

	Inventory_GetItemsFromFolderEx5.Open strSQL, gobjConn	
end function

function Inventory_GetItemsFromFolderEx6(intParentID,intCampaignID,intBrandID,intCategoryId, blnGetFolders, blnGetItems)
	dim strSQL

   '  strSQL ="SELECT vchFormat from " & STR_TABLE_SHOPPER & " WHERE intID=" & gintUserID
   ' dim rsUser
   ' set rsUser = gobjConn.execute(strSQL)

	strSQL = "SELECT *"
	strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY
	strSQL = strSQL & " WHERE chrStatus='A' AND intParentID=" & intParentID & " AND intBrand in (" & intBrandID & ") AND intCampaign=" & intCampaignID& " AND intCategory=" & intCategoryId
    if blnGetFolders and not blnGetItems then
		strSQL = strSQL & " AND chrType='A'"
	elseif not blnGetFolders and blnGetItems then
		strSQL = strSQL & " AND chrType='I'"
	end if
	strSQL = strSQL & " AND chrType<>'B'"

   
   ' if not rsUser.Eof then
            
   '     if rsUser(0)&"" = "1.0" then
   '         strSQL = strSQL & " AND ((intBrand=15 AND chrType<>'A') OR chrType='A')"
   '     elseif rsUser(0)&"" = "2.0" then
   '         strSQL = strSQL & " AND ((intBrand=16 AND chrType<>'A') OR chrType='A')"
   '     end if

    'end if


	strSQL = strSQL & " ORDER BY vchPartNumber"

    SET Inventory_GetItemsFromFolderEx6 = Server.CreateObject("ADODB.recordset")

    Inventory_GetItemsFromFolderEx6.CursorLocation = adUseClient
    Inventory_GetItemsFromFolderEx6.CursorType = adOpenStatic
    Inventory_GetItemsFromFolderEx6.LockType = adLockBatchOptimistic

	Inventory_GetItemsFromFolderEx6.Open strSQL, gobjConn	
end function

function Inventory_GetItemsFromFolderEx7(intParentID,intProgramID,intCampaignID, blnGetFolders, blnGetItems)
	dim strSQL

	strSQL = "SELECT *"
	strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY
	strSQL = strSQL & " WHERE chrStatus='A' AND intParentID=" & intParentID & " AND intCampaign=" & intCampaignID & " AND intProgram=" & intProgramID
	if blnGetFolders and not blnGetItems then
		strSQL = strSQL & " AND chrType='A'"
	elseif not blnGetFolders and blnGetItems then
		strSQL = strSQL & " AND chrType='I'"
	end if
	strSQL = strSQL & " AND chrType<>'B'"
	strSQL = strSQL & " ORDER BY vchItemName"
	SET Inventory_GetItemsFromFolderEx7 = Server.CreateObject("ADODB.recordset")

    Inventory_GetItemsFromFolderEx7.CursorLocation = adUseClient
    Inventory_GetItemsFromFolderEx7.CursorType = adOpenStatic
    Inventory_GetItemsFromFolderEx7.LockType = adLockBatchOptimistic

	Inventory_GetItemsFromFolderEx7.Open strSQL, gobjConn	
end function

function Inventory_GetItemsFromFolderEx(intParentID,intBrandID, blnGetFolders, blnGetItems)
	dim strSQL

   '  strSQL ="SELECT vchFormat from " & STR_TABLE_SHOPPER & " WHERE intID=" & gintUserID
   ' dim rsUser
   ' set rsUser = gobjConn.execute(strSQL)

	strSQL = "SELECT *"
	strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY
	strSQL = strSQL & " WHERE chrStatus='A' AND intParentID=" & intParentID & " AND intBrand=" & intBrandID
	if blnGetFolders and not blnGetItems then
		strSQL = strSQL & " AND chrType='A'"
	elseif not blnGetFolders and blnGetItems then
		strSQL = strSQL & " AND chrType='I'"
	end if
	strSQL = strSQL & " AND chrType<>'B'"

   
   ' if not rsUser.Eof then
            
   '     if rsUser(0)&"" = "1.0" then
   '         strSQL = strSQL & " AND ((intBrand=15 AND chrType<>'A') OR chrType='A')"
   '     elseif rsUser(0)&"" = "2.0" then
   '         strSQL = strSQL & " AND ((intBrand=16 AND chrType<>'A') OR chrType='A')"
   '     end if

    'end if


	strSQL = strSQL & " ORDER BY vchPartNumber"
	SET Inventory_GetItemsFromFolderEx = Server.CreateObject("ADODB.recordset")

    Inventory_GetItemsFromFolderEx.CursorLocation = adUseClient
    Inventory_GetItemsFromFolderEx.CursorType = adOpenStatic
    Inventory_GetItemsFromFolderEx.LockType = adLockBatchOptimistic

	Inventory_GetItemsFromFolderEx.Open strSQL, gobjConn	
end function

function Inventory_GetFolderContent(intID)
	dim strSQL
	strSQL = "SELECT intParentID, intID, chrType, chrStatus, intMinQty, intStock, vchImageURL, intSmImageHeight, intsmImageWidth, intLgImageHeight, intLgImageWidth, mnyItemPrice, mnyShipPrice, chrSoftFlag, vchItemName, vchPartNumber, fltShipWeight, intForceShipMethod, chrForceSoloItem, vchOptionList1, vchOptionList2, vchOptionList3, txtDescription"
	strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY
	strSQL = strSQL & " WHERE chrStatus='A' AND intID=" & intID
	strSQL = strSQL & " AND chrType='A'"
	set Inventory_GetFolderContent = gobjConn.execute(strSQL)	
end function

function Inventory_GetItemsFromFolderTop6(intParentID, blnGetFolders, blnGetItems)
	dim strSQL
	strSQL = "SELECT TOP 6 intParentID, intID, chrType, chrStatus, intMinQty, intStock, vchImageURL, intSmImageHeight, intsmImageWidth, intLgImageHeight, intLgImageWidth, mnyItemPrice, mnyShipPrice, chrSoftFlag, vchItemName, vchPartNumber, fltShipWeight, intForceShipMethod, chrForceSoloItem, vchOptionList1, vchOptionList2, vchOptionList3, txtDescription"
	strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY
	strSQL = strSQL & " WHERE chrStatus='A' AND intParentID=" & intParentID
	if blnGetFolders and not blnGetItems then
		strSQL = strSQL & " AND chrType='A'"
	elseif not blnGetFolders and blnGetItems then
		strSQL = strSQL & " AND chrType='I'"
	end if
	strSQL = strSQL & " AND chrType<>'B'"
	strSQL = strSQL & " ORDER BY chrType, intSortOrder"
	set Inventory_GetItemsFromFolderTop6 = gobjConn.execute(strSQL)	
end function

function Inventory_GetItemsByKeyword(byVal strKeywords)
	strKeywords = SQLEncode(strKeywords)
	' 25aug2003 bkonvalin - Added new search engine from politicsnationwide per Ed
	' 27aug2003 bkonvalin - Added union to sort by title or partnumber then description

	dim strSQL
	strSQL = "SELECT intParentID, intID, chrType, chrStatus, intStock, vchImageURL, intSmImageHeight, intSmImageWidth, intLgImageHeight, intLgImageWidth, mnyItemPrice, intMinQty, mnyShipPrice, chrSoftFlag, vchItemName, vchPartNumber, fltShipWeight, intForceShipMethod, chrForceSoloItem, vchOptionList1, vchOptionList2, vchOptionList3, txtDescription"
	strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY
	strSQL = strSQL & " WHERE chrStatus='A'"
	strSQL = strSQL & " AND (chrType='I' OR chrType='A')"

	 dim srchStr, srchStr2, searchWds, srchArray, i
'	 searchWds = LCase(Request("search"))
	 searchWds = LCase(strKeywords)
	 if searchWds <> "" then
	 	srchStr = ""
		srchStr2 = ""
	 	'remove connector words
		srchArray = Split(searchWds, " ")
		srchArray = getRealWords(srchArray)
		if UBound(srchArray) >= 0 then
			For i = 0 to UBound(srchArray)
				srchStr = srchStr & " AND (vchItemName LIKE '%" & CStr(srchArray(i)) & "%'"
				srchStr = srchStr & " OR vchPartNumber LIKE '%" & CStr(srchArray(i)) & "%')"
				srchStr2 = srchStr2 & " AND (vchItemName NOT LIKE '%" & CStr(srchArray(i)) & "%')"
				srchStr2 = srchStr2 & " AND (txtDescription LIKE '%" & CStr(srchArray(i)) & "%')"
			Next
		end if
	end if
	strSQL = strSQL & srchStr & " UNION ALL " & strSQL & srchStr2

'	strSQL = strSQL & " ORDER BY chrType, intParentID, intID, intSortOrder"
'	response.write strSQL & "<br />"
	set Inventory_GetItemsByKeyword = gobjConn.execute(strSQL)
	' set rsData = Data_OpenRS(strSQL)
end function

function Inventory_GetItemsByKeyword_X(byVal strKeywords)
	strKeywords = SQLEncode(strKeywords)
	' 14aug2002 rallen - Added intMinQty to SQL select query. Without this search results were breaking on store/default.asp
	' 22aug2003 bkonvalin - Added chrType='A' to SQL select query. Expanded Order by clause to folder, parent, child, sort key on store/default.asp
	dim strSQL
	strSQL = "SELECT intParentID, intID, chrType, chrStatus, intStock, vchImageURL, intSmImageHeight, intSmImageWidth, intLgImageHeight, intLgImageWidth, mnyItemPrice, intMinQty, mnyShipPrice, chrSoftFlag, vchItemName, vchPartNumber, fltShipWeight, intForceShipMethod, chrForceSoloItem, vchOptionList1, vchOptionList2, vchOptionList3, txtDescription"
	strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY
	strSQL = strSQL & " WHERE chrStatus='A'"
	strSQL = strSQL & " AND (chrType='I' OR chrType='A')"

	strSQL = strSQL & " AND (vchItemName LIKE '%" & strKeywords & "%'"
	strSQL = strSQL & " OR vchPartNumber LIKE '%" & strKeywords & "%'"
	strSQL = strSQL & " OR txtDescription LIKE '%" & strKeywords & "%'"

	strSQL = strSQL & ") ORDER BY chrType, intParentID, intID, intSortOrder"
	set Inventory_GetItemsByKeyword = gobjConn.execute(strSQL)
end function

sub Custom_GetGlobalInventoryFolders(intParentID)
	if IsObject(gobjConn) then
		set grsInvFolders = Inventory_GetItemsFromFolder(intParentID, true, false)
	end if
end sub

sub Inventory_GetFolderInfo(intParentID, strPageTitle, strPageText, intParentUpID)
	intParentUpID = 0
	strPageTitle = ""
	strPageText = ""
	dim strSQL, rsTemp
	strSQL = "SELECT intParentID, vchItemName, txtDescription FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intParentID & " AND chrStatus='A'"
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		strPageText = rsTemp("txtDescription")
		intParentUpID = rsTemp("intParentID")
		strPageTitle = rsTemp("vchItemName")
	end if
	rsTemp.close
	set rsTemp = nothing
end sub

sub Inventory_GetItemInfo(intID, intParentID, strItemName, strPartNumber, intStock, strImageURL, strImageURL2, mnyItemPrice, mnyShipPrice, blnTaxFlag, blnSoftFlag, fltShipWeight, intForceShipMethod, blnForceSoloItem, strOptionList1, strOptionList2, strOptionList3, intMinQty, strDescription)
	intParentID = 0
	strItemName = ""
	strPartNumber = ""
	intStock = 0
	strImageURL = ""
	strImageURL2 = ""
	mnyItemPrice = 0
	mnyShipPrice = 0
	blnTaxFlag = false
	blnSoftFlag = false
	fltShipWeight = 0
	intForceShipMethod = NULL
	blnForceSoloItem = false
	strOptionList1 = ""
	strOptionList2 = ""
	intMinQty = 1
	strDescription = ""
	dim strSQL, rsTemp
	strSQL = "SELECT intParentID, vchItemName, vchPartNumber, intStock, vchImageURL, vchImageURL2, mnyItemPrice, mnyShipPrice, chrTaxFlag, chrSoftFlag, fltShipWeight, intForceShipMethod, chrForceSoloItem, vchOptionList1, vchOptionList2, vchOptionList3, intMinQty, txtDescription FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intID & " AND chrStatus='A'"
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		intParentID = rsTemp("intParentID")
		strItemName = rsTemp("vchItemName")
		strPartNumber = rsTemp("vchPartNumber")
		intStock = rsTemp("intStock")
		strImageURL = rsTemp("vchImageURL")
		strImageURL2 = rsTemp("vchImageURL2")
		mnyItemPrice = rsTemp("mnyItemPrice")
		mnyShipPrice = rsTemp("mnyShipPrice")
		blnTaxFlag = (rsTemp("chrTaxFlag") = "Y")
		blnSoftFlag = (rsTemp("chrSoftFlag") = "Y")
		fltShipWeight = rsTemp("fltShipWeight")
		intForceShipMethod = rsTemp("intForceShipMethod")
		blnForceSoloItem = (rsTemp("chrForceSoloItem") & "" = "Y")
		strOptionList1 = rsTemp("vchOptionList1")
		strOptionList2 = rsTemp("vchOptionList2")
		strOptionList3 = rsTemp("vchOptionList3")
		strDescription = rsTemp("txtDescription") & ""
		if IsNumeric(rsTemp("intMinQty")) then
			intMinQty = rsTemp("intMinQty")
			if intMinQty < 1 then
				intMinQty = 1
			end if
		end if
	end if
	rsTemp.close
	set rsTemp = nothing
end sub

sub Inventory_UpdateHitCount(intID)
	dim strSQL
	strSQL = "UPDATE " & STR_TABLE_INVENTORY & " SET intHitCount=intHitCount+1 WHERE intID=" & intID
end sub

sub SendInvoiceMail(rsInput, strMessage, strEmail1, strEmail2, strEmail3)
	dim s, strEmailAddress
	strEmailAddress = strEmail1
	if strEmail2 <> "" then
		if strEmailAddress <> "" then strEmailAddress = strEmailAddress & ";"
		strEmailAddress = strEmailAddress & strEmail2
	end if
	if strEmail3 <> "" then
		if strEmailAddress <> "" then strEmailAddress = strEmailAddress & ";"
		strEmailAddress = strEmailAddress & strEmail3
	end if
	s = GetInvoiceHTML(rsInput, strMessage)
	if lcase(strServerHost) = "awsdev" then
		' running from internal
		s = s & "  Sent To: " & strEmailAddress
		strEmailAddress = "anthony@alenterprises.net"
	end if
	SendHTMLMail STR_MERCHANT_CS_EMAIL, strEmailAddress, "", "", "Copy of Invoice - Order # " & STR_MERCHANT_TRACKING_PREFIX & rsInput("intID"), s
end sub

sub SendInvoiceMailMAF(rsInput, strMessage, strEmail1, strEmail2, strEmail3)
	dim s, strEmailAddress
	strEmailAddress = strEmail1
	if strEmail2 <> "" then
		if strEmailAddress <> "" then strEmailAddress = strEmailAddress & ";"
		strEmailAddress = strEmailAddress & strEmail2
	end if
	if strEmail3 <> "" then
		if strEmailAddress <> "" then strEmailAddress = strEmailAddress & ";"
		strEmailAddress = strEmailAddress & strEmail3
	end if
	s = GetInvoiceHTML_MAF(rsInput, strMessage)
	if lcase(strServerHost) = "awsdev" then
		' running from internal
		s = s & "  Sent To: " & strEmailAddress
		strEmailAddress = "anthony@alenterprises.net"
	end if
	SendHTMLMail STR_MERCHANT_CS_EMAIL, strEmailAddress, "", "", "Copy of Invoice - Order # " & STR_MERCHANT_TRACKING_PREFIX & rsInput("intID"), s
end sub

function GetInvoiceHTML_MAF(rsInput, strMessage)
	dim s
    
    dim strSQL
    dim rsOrderInfo, rsOrder
    strSQL = "SELECT vchItemName, intQuantity FROM " & STR_TABLE_LINEITEM & " WHERE intOrderID = " & rsInput("intID") & " AND chrStatus = 'A'"
    set rsOrderInfo = gobjConn.execute(strSQL)

    s = s & "<!doctype html public ""-//W3C//DTD XHTML 1.0 Transitional//EN"" ""https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"">"
    s = s & "<html xmlns=""https://www.w3.org/1999/xhtml"">"
    s = s & "<head>"
    s = s & "<meta http-equiv=""Content-Type"" content=""text/html; charset=UTF-8""/>"
    s = s & "<title>Move America Forward - Military K9 Care Packages</title>"
    s = s & "</head>"

    s = s & "<table width=""650"" border=""0"" align=""center"" cellpadding=""10"" cellspacing=""0"">"
    s = s & "<tr>"
    s = s & "<td colspan=""2"">"
    s = s & "<div align=""center""><img src=""http://www.HBO.com/MoveAmericaForward/MAFimages/header-email.jpg"" width=""650"" height=""138"" border=""0"" /></div>"
    s = s & "</td>"
    s = s & "</tr>"
    s = s & "<tr>"
    s = s & "<td colspan=""2"" align=""center"">"
    s = s & "<font size=""6"" face=""Arial"" color=""#000000""><strong>THANK YOU, YOUR ORDER<br />HAS BEEN RECEIVED!</strong></font>"
    s = s & "</td>"
    s = s & "</tr>"
    s = s & "<tr valign=""top"">"
    s = s & "<td valign=""top"">"
    s = s & "<center><font size=""5"" face=""Impact,sans-serif"" color=""#073783"">Thank You for Supporting Our Troops</font></center>"

    s = s & "<p><center><font face=""Arial"" color=""#FF0000""><strong>Print and save a copy of this screen for your records.</strong></font></center></p>"

    s = s & "<p><font size=""2"" face=""Arial"">Your order # <strong>" & rsInput("intID") & "</strong> has been received by Move America Forward and will be processed shortly. Our volunteers here at MAF will pack the care packages you sponsored and ship them to our troops on the front lines in Afghanistan. Your support for our brave troops is greatly appreciated.</font></p>"

    s = s & "<table width=""100%"" border=""0"" cellpadding=""10"" cellspacing=""0"">"
    s = s & "<tr bgcolor=""#c0d5fa"">"
    s = s & "<td>"
    s = s & "<font size=""2"" face=""Arial"">"
    
    while not rsOrderInfo.eof
        s = s & "(" & rsOrderInfo("intQuantity") & ") - " & rsOrderInfo("vchItemName") & "<br />"
        rsOrderInfo.MoveNext
    wend

    s = s & "</font>"
    s = s & "</td>"
    s = s & "</tr>"
    s = s & "<tr bgcolor=""#0543a8"">"
    s = s & "<td>"
    s = s & "<font size=""2"" face=""Arial"" color=""#FFFFFF"">"
    s = s & "Order total: " & SafeFormatCurrency("&nbsp;", GetOrderGrandTotal, 2)
    s = s & "</font>"
    s = s & "</td>"
    s = s & "</tr>"
    s = s & "</table>"
			
    s = s & "<p><font size=""2"" face=""Arial"">Please print this page and mail along with check or money order to:</font></p>"

    s = s & "<p><font size=""2"" face=""Arial"" color=""#0000FF"">The Campaign Store<br />"
    s = s & "8795 Folsom Blvd., Suite 103<br />"
    s = s & "Sacramento, CA 95826</font></p>"

    strSQL = "SELECT chrPaymentMethod FROM " & STR_TABLE_ORDER & " WHERE intID = " & rsInput("intID")
    set rsOrder = gobjConn.execute(strSQL)

    if rsOrder("chrPaymentMethod") = "MEC" then
        s = s & "<p><font size=""2"" face=""Arial""><strong>PLEASE NOTE: Your order can NOT be processed until payment is received.</strong></font></p>"
        s = s & "<p><font size=""2"" face=""Arial"">You will also receive an order confirmation via email from HBO.COM, a service we use to process donations. Move America Forward is a registered 501(c)(3) your donation is also tax-deductible, you should receive a written record of your donation at the beginning of next year for IRS purposes.</font></p>"
    else
        s = s & "Please note that your credit card bill will be charged by HBO.COM, a service we use to process donations. You will also receive an order confirmation via email from HBO.COM. Move America Forward is a registered 501(c)(3) so your donation is also tax-deductible. You may use this response as proof of your �tax receipt� for your donation for IRS purposes."
    end if			
    
    s = s & "<p><font size=""2"" face=""Arial"">If you would like more ways to connect with Move America Forward and support the United States military, you can...</font></p>"
    s = s & "</td>"
    s = s & "<td>"
    s = s & "<img src=""http://www.HBO.com/MoveAmericaForward/MAFimages/thankyou.jpg"" width=""225"" height=""475"" border=""0"" />"
    s = s & "</td>"
    s = s & "</tr>"
    s = s & "<tr>"
    s = s & "<td colspan=""2"" align=""center"">"
    s = s & "<table width=""90%"" border=""0"" cellpadding=""10"" cellspacing=""0"">"
    s = s & "<tr>"
    s = s & "<td align=""center"">"
    s = s & "<font size=""2"" face=""Arial""><strong>Join Our<br />Mailing List<br /><a href=""https://paracom.paramountcommunication.com/phase2/survey1/survey.htm?cid=qphgos&1323378051"" target=""_blank""><img src=""http://www.HBO.com/MoveAmericaForward/MAFimages/mailinglist.png"" width=""50"" height=""50"" /></a></strong></font>"
    s = s & "</td>"
    s = s & "<td align=""center"">"
    s = s & "<font size=""2"" face=""Arial""><strong>Like Us<br />on Facebook<br /><a href=""https://www.facebook.com/moveamericaforward"" target=""_blank""><img src=""http://www.HBO.com/MoveAmericaForward/MAFimages/facebook.png"" width=""50"" height=""50"" /></a></strong></font>"
    s = s & "</td>"
    s = s & "<td align=""center"">"
    s = s & "<font size=""2"" face=""Arial""><strong>Follow Us<br />on Twitter<br /><a href=""https://twitter.com/m_a_f"" target=""_blank""><img src=""http://www.HBO.com/MoveAmericaForward/MAFimages/twitter.png"" width=""50"" height=""50"" /></a></strong></font>"
    s = s & "</td>"
    s = s & "<td align=""center"">"
    s = s & "<font size=""2"" face=""Arial""><strong>Subscribe to Us<br />on YouTube<br /><a href=""https://www.youtube.com/moveamericaforward "" target=""_blank""><img src=""http://www.HBO.com/MoveAmericaForward/MAFimages/youtube.png"" width=""50"" height=""50"" /></a></strong></font>"
    s = s & "</td>"
    s = s & "</tr>"
    s = s & "</table>"
    s = s & "</td>"
    s = s & "</tr>"
    s = s & "<tr>"
    s = s & "<td colspan=""2"" bgcolor=""#041531"">"
    s = s & "<table width=""100%"" border=""0"" cellpadding=""10"" cellspacing=""0"">"
    s = s & "<tr>"
    s = s & "<td width=""75%"">"
    s = s & "<font face=""Arial"" color=""#EEEEEE"" size=""1"">Move America Forward is a registered 501(c)(3).</font>"
    s = s & "<p><font face=""Arial"" color=""#EEEEEE"" size=""1"">All donations and care package sponsorships are tax-deductible. An annual receipt will be mailed to you at the end of the year.</font></p>"
    s = s & "</td>"
    s = s & "<td width=""25%"" style=""border-left: 1px solid #335293;"">"
    s = s & "<font face=""Arial"" color=""#EEEEEE"" size=""1""><a href=""https://moveamericaforward.org/"" target=""_blank""><font color=""#FFFFFF"">MoveAmericaForward.org</font></a><br />"
    s = s & "8795 Folsom Blvd Suite #103<br />"
    s = s & "Sacramento, CA 95826<br />"
    s = s & "(916) 441-6197</font>"
    s = s & "</td>"
    s = s & "</tr>"
    s = s & "</table>"					
    s = s & "</td>"
    s = s & "</tr>"
    s = s & "</table>"

    s = s & "</html>"
    
	GetInvoiceHTML_MAF = s
end function


function GetInvoiceHTML(rsInput, strMessage)
	dim s, strSQL
	
	dim rsLineItem, rsShopper, strColor
	dim blnErrors, dctErrorList
	dim strStatus, strStatusText, strStatusCode
	strStatusCode = rsInput("chrStatus")
	strStatus = GetArrayValue(strStatusCode, dctOrderStatusInvoiceValues)
	select case rsInput("chrStatus")
		case "0","1","2","3","4","5","6","7","8", "P", "9": ' in progress
			strStatusText = "This order has not yet been submitted."
		case "S", "A", "Z", "V", "E":	' submitted
			strStatusText = "This order has been submitted for processing."
		case "X", "H":	' deposited
			strStatusText = "This order has been shipped."
		case "C":	' credited
			strStatusText = "Funds have been returned to purchaser."
		case "B":	' abandoned
		case "K":	' declined
	end select
	
	dim strRecurringOrder
	if rsInput("chkRecurringOrder") = "Y" then
		strRecurringOrder = strRecurringOrder & "You have signed up for Monthly Smiles Club and this order will be generated on a monthly basis. To cancel or change this order at any time call 916-441-6197 or email LisaBaron@moveamericaforward.org. Thank You."
	else
		strRecurringOrder = ""
	end if
	
	
	s = ""
	' HTML Header
	s = s & "<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">"
	s = s & "<HTML>"
	s = s & "<HEAD>"
	s = s & "	<TITLE>Copy of Invoice - Order # " & STR_MERCHANT_TRACKING_PREFIX & rsInput("intID") & "</TITLE>"
	s = s & "	<STYLE TYPE=""text/css"">"
	s = s & "	<!--"
	s = s & "	body {"
	s = s & "		font-size: 13px;"
	s = s & "		font-family: Verdana,Geneva,Arial;"
	s = s & "	}"
	s = s & "	td {"
	s = s & "		font-size: 10px;"
	s = s & "		font-family: Verdana,Geneva,Arial;"
	s = s & "	}"
	s = s & "	-->"
	s = s & "	</STYLE>"
	s = s & "</HEAD>"
	s = s & "<BODY"
	s = s & "	BGCOLOR=""" & cBodyBG & """"
	s = s & "	TEXT=""" & cText & """ LINK=""" & cLink & """ VLINK=""" & cVLink & """ ALINK=""" & cALink & """"
	s = s & "	LEFTMARGIN=""" & PAGE_LEFT_MARGIN & """ TOPMARGIN=""" & PAGE_TOP_MARGIN & """ MARGINWIDTH=""" & PAGE_LEFT_MARGIN & """ MARGINHEIGHT=""" & PAGE_TOP_MARGIN & """>"
	s = s & "<div style=""width:500;align:right;float:right;"">"
	s = s & "	<a href=""http://www.HBO.com/store/""><img src=""/_images/banners/banner-sm2.jpg"" border=""0"" /></a>"
	s = s & "</div>"
	s = s & "<TABLE BORDER=""0"" CELLPADDING=""0"" CELLSPACING=""0"" WIDTH=""" & INT_PAGE_WIDTH & """>"
	s = s & "<TR>"
	s = s & "	<TD>"

	' BODY
	s = s & "<CENTER>"
	s = s & "<FONT SIZE=""2""><B>" & STR_MERCHANT_NAME & "</B></FONT><BR>"
	s = s & STR_MERCHANT_ADDRESS1 & iif(STR_MERCHANT_ADDRESS2 <> "", ", " & STR_MERCHANT_ADDRESS2, "") & "<BR>"
	s = s & STR_MERCHANT_CITY & ", " & STR_MERCHANT_STATE & " " & STR_MERCHANT_ZIP & "<BR>"
	s = s & "Phone " & STR_MERCHANT_PHONE & "&nbsp;&nbsp;Fax " & STR_MERCHANT_FAX
	s = s & "</CENTER>"
	s = s & "<BR>"
	if strMessage <> "" then
		s = s & "<TABLE BORDER=1 CELLPADDING=2 CELLSPACING=0 WIDTH=""100%"">"
		s = s & "<TR>"
		s = s & "	<TD>" & strMessage & "</TD>"
		s = s & "</TR>"
		s = s & "</TABLE>"
		s = s & "<BR>"
	end if
	
	if strRecurringOrder <> "" then
		s = s & "<TABLE BORDER=1 CELLPADDING=2 CELLSPACING=0 WIDTH=""100%"">"
		s = s & "<TR>"
		s = s & "	<TD>" & strRecurringOrder & "</TD>"
		s = s & "</TR>"
		s = s & "</TABLE>"
		s = s & "<BR>"
	end if
	
	'stlBeginStdTable "100%"
	s = s & "<TABLE BORDER=""0"" CELLPADDING=""0"" CELLSPACING=""0"" WIDTH=""100%"">"
	s = s & "<TR BGCOLOR=""" & cVLtYellow & """>"
	s = s & "	<TD><B>Created:</B></TD>"
	s = s & "	<TD><B>Submitted:</B></TD>"
	s = s & "	<TD><B>Updated:</B></TD>"
	s = s & "</TR>"
	s = s & "<TR>"
	s = s & "	<TD>&nbsp;&nbsp;" & FormatDateTimeNoSeconds(rsInput("dtmCreated")) & "&nbsp;&nbsp;&nbsp;</TD>"
	s = s & "	<TD>&nbsp;&nbsp;" & FormatDateTimeNoSeconds(rsInput("dtmSubmitted")) & "&nbsp;&nbsp;&nbsp;</TD>"
	s = s & "	<TD>&nbsp;&nbsp;" & FormatDateTimeNoSeconds(rsInput("dtmUpdated")) & "&nbsp;&nbsp;&nbsp;</TD>"
	s = s & "</TR>"
	s = s & "<TR>"
	s = s & "	<TD COLSPAN=""3"">&nbsp;</TD>"
	s = s & "</TR>"
	s = s & "<TR BGCOLOR=""" & cVLtYellow & """>"
	s = s & "	<TD><B>Tracking Number:</B></TD>"
	s = s & "	<TD><B>Store:</B></TD>"
	s = s & "	<TD><B>Shipping/Conf #:</B></TD>"
	s = s & "</TR>"
	s = s & "<TR>"
	s = s & "	<TD>&nbsp;&nbsp;" & STR_MERCHANT_TRACKING_PREFIX & rsInput("intID") & "&nbsp;&nbsp;&nbsp;</TD>"
	s = s & "	<TD>&nbsp;&nbsp;" & rsInput("A_vchShopperAccount") & "&nbsp;&nbsp;&nbsp;</TD>"
	s = s & "	<TD>&nbsp;&nbsp;" & rsInput("vchShippingNumber") & "&nbsp;&nbsp;&nbsp;</TD>"
	s = s & "</TR>"
	s = s & "</TABLE>"
	s = s & "<BR>"
	s = s & "<TABLE CELLPADDING=""0"" CELLSPACING=""0"" WIDTH=""100%"">"
	s = s & "<TR BGCOLOR=""" & cVLtYellow & """>"
	s = s & "	<TD><B>Status:</B></TD>"
	s = s & "</TR>"
	s = s & "<TR>"
	s = s & "	<TD>&nbsp;&nbsp;" & strStatus & " - " & strStatusText & "</TD>"
	s = s & "</TR>"
	s = s & "</TABLE>"
	s = s & "<BR>"
	s = s & "<TABLE CELLPADDING=""0"" CELLSPACING=""0"" WIDTH=""100%"">"
	s = s & "<TR BGCOLOR=""" & cVLtYellow & """>"
	s = s & "	<TD><B>Payment Information:</B></TD>"
	s = s & "</TR>"
	s = s & "<TR>"
	s = s & "	<TD>"
	s = s & "&nbsp;&nbsp;" & GetArrayValue(rsInput("chrPaymentMethod"), dctPaymentMethod)
	if right(rsInput("chrPaymentMethod"), 2) = "CC" then
		if not IsNull(rsInput("vchPaymentCardNumber")) then
			s = s & " " & rsInput("vchPaymentCardType") & " " & GetProtectedCardNumber(rsInput("vchPaymentCardNumber")) & " (exp " & rsInput("chrPaymentCardExpMonth") & "/" & rsInput("chrPaymentCardExpyear") & ")"
		end if
	elseif right(rsInput("chrPaymentMethod"), 2) = "EC" then
		s = s & " " & rsInput("vchPaymentBankName") & " " & rsInput("vchPaymentRtnNumber") & "-" & GetProtectedCardNumber(rsInput("vchPaymentAcctNumber")) & " " & rsInput("vchPaymentCheckNumber")
	end if
	s = s & "	</TD>"
	s = s & "</TR>"
	s = s & "</TABLE>"
	s = s & "<BR>"
	s = s & "<TABLE BORDER=""0"" CELLPADDING=""0"" CELLSPACING=""0"" WIDTH=""100%"">"
	s = s & "<TR>"
	s = s & "	<TD VALIGN=""top"" WIDTH=""50%""><TABLE CELLPADDING=""0"" CELLSPACING=""0"" WIDTH=""90%"">"
	s = s & "	<TR BGCOLOR=""" & cVLtYellow & """>"
	s = s & "		<TD><B>Bill To:</B></TD>"
	s = s & "	</TR>"
	s = s & "	<TR>"
	s = s & "		<TD>"
	
	if isNull(rsInput("intBillShopperID")) or IsNull(rsInput("intShopperID")) then
		s = s & "&nbsp;&nbsp;no information"
	else
		strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & rsInput("intBillShopperID") & " AND chrStatus<>'D' AND chrType='B' AND intShopperID=" & rsInput("intShopperID")
		set rsShopper = gobjConn.execute(strSQL)
		if rsShopper.eof then
			s = s & "&nbsp;&nbsp;<B>System error</B>--unable to retrieve shopper information."
		else
			s = s & "&nbsp;&nbsp;" & rsShopper("vchLastName") & ", " & rsShopper("vchFirstName") & "<BR>"
			if not IsNull(rsShopper("vchCompany")) then
				s = s & "&nbsp;&nbsp;" & rsShopper("vchCompany") & "<BR>"
			end if
			if not IsNull(rsShopper("vchAddress1")) then
				s = s & "&nbsp;&nbsp;" & rsShopper("vchAddress1") & "<BR>"
			end if
			if not IsNull(rsShopper("vchAddress2")) then
				s = s & "&nbsp;&nbsp;" & rsShopper("vchAddress2") & "<BR>"
			end if
			s = s & "&nbsp;&nbsp;" & rsShopper("vchCity") & ", " & rsShopper("vchState") & " " & rsShopper("vchZip") & "<BR>"
			if not IsNull(rsShopper("vchCountry")) then
				s = s & "&nbsp;&nbsp;" & rsShopper("vchCountry") & "<BR>"
			end if
			if not IsNull(rsShopper("vchDayPhone")) then
				s = s & "&nbsp;&nbsp;Day: " & rsShopper("vchDayPhone") & "<BR>"
			end if
			if not IsNull(rsShopper("vchNightPhone")) then
				s = s & "&nbsp;&nbsp;Night: " & rsShopper("vchNightPhone") & "<BR>"
			end if
			if not IsNull(rsShopper("vchFax")) then
				s = s & "&nbsp;&nbsp;Fax: " & rsShopper("vchFax") & "<BR>"
			end if
			if not IsNull(rsShopper("vchEmail")) then
				s = s & "&nbsp;&nbsp;Email: " & rsShopper("vchEmail") & "<BR>"
			end if
		end if
		rsShopper.close
		set rsShopper = nothing
	end if
	s = s & "		</TD>"
	s = s & "	</TR>"
	s = s & "	</TABLE></TD>"
	s = s & "	<TD VALIGN=""top"" WIDTH=""50%"" ALIGN=""right""><TABLE CELLPADDING=""0"" CELLSPACING=""0"" WIDTH=""90%"">"
	s = s & "	<TR BGCOLOR=""" & cVLtYellow & """>"
	s = s & "		<TD><B>Ship To:</B></TD>"
	s = s & "	</TR>"
	s = s & "	<TR>"
	s = s & "		<TD>"
	if IsNull(rsInput("intShipShopperID")) or isNull(rsInput("intShopperID")) then
		s = s & "&nbsp;&nbsp;no information"
	else
		strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & rsInput("intShipShopperID") & " AND chrStatus<>'D' and (chrType='S' OR chrType='B') and intShopperID=" & rsInput("intShopperID")
		set rsShopper = gobjConn.execute(strSQL)
		if rsShopper.eof then
			s = s & "&nbsp;&nbsp;<B>System error</B>--unable to retrieve shopper information."
		else
			s = s & "&nbsp;&nbsp;" & rsShopper("vchLastName") & ", " & rsShopper("vchFirstName") & "<BR>"
			if not IsNull(rsShopper("vchCompany")) then
				s = s & "&nbsp;&nbsp;" & rsShopper("vchCompany") & "<BR>"
			end if
			if not IsNull(rsShopper("vchAddress1")) then
				s = s & "&nbsp;&nbsp;" & rsShopper("vchAddress1") & "<BR>"
			end if
			if not IsNull(rsShopper("vchAddress2")) then
				s = s & "&nbsp;&nbsp;" & rsShopper("vchAddress2") & "<BR>"
			end if
			s = s & "&nbsp;&nbsp;" & rsShopper("vchCity") & ", " & rsShopper("vchState") & " " & rsShopper("vchZip") & "<BR>"
			if not IsNull(rsShopper("vchCountry")) then
				s = s & "&nbsp;&nbsp;" & rsShopper("vchCountry") & "<BR>"
			end if
			if not IsNull(rsShopper("vchDayPhone")) then
				s = s & "&nbsp;&nbsp;Day: " & rsShopper("vchDayPhone") & "<BR>"
			end if
			if not IsNull(rsShopper("vchNightPhone")) then
				s = s & "&nbsp;&nbsp;Night: " & rsShopper("vchNightPhone") & "<BR>"
			end if
			if not IsNull(rsShopper("vchFax")) then
				s = s & "&nbsp;&nbsp;Fax: " & rsShopper("vchFax") & "<BR>"
			end if
			if not IsNull(rsShopper("vchEmail")) then
				s = s & "&nbsp;&nbsp;Email: " & rsShopper("vchEmail") & "<BR>"
			end if
		end if
		rsShopper.close
		set rsShopper = nothing
	end if
	s = s & "		</TD>"
	s = s & "	</TR>"
	s = s & "	</TABLE></TD>"
	s = s & "</TR>"
	s = s & "</TABLE>"
	s = s & "<BR>"
	
	set rsLineItem = GetOrderLineItems_Other(rsInput("intID"))
	s = s & GetMerchant_DrawOrderCart(rsInput, rsLineItem, false, "", "", "", "", "")
	rsLineItem.close
	set rsLineItem = nothing
	
	' FOOTER
	s = s & "	</TD>"
	s = s & "</TR>"
	
	
	s = s & "</TABLE>"
	s = s & "</BODY>"
	s = s & "</HTML>"
	
	GetInvoiceHTML = s
end function

Function GetGenericEmailHTML(strMessage, intID)
	Dim s

	' HTML Header
	s = "<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">"
	s = s & "<HTML>"
	s = s & "<HEAD>"
	s = s & "	<TITLE>Copy of Invoice - Order # " & STR_MERCHANT_TRACKING_PREFIX & intID & "</TITLE>"
	s = s & "	<STYLE TYPE=""text/css"">"
	s = s & "	<!--"
	s = s & "	body {"
	s = s & "		font-size: 13px;"
	s = s & "		font-family: Verdana,Geneva,Arial;"
	s = s & "	}"
	s = s & "	td {"
	s = s & "		font-size: 10px;"
	s = s & "		font-family: Verdana,Geneva,Arial;"
	s = s & "	}"
	s = s & "	-->"
	s = s & "	</STYLE>"
	s = s & "</HEAD>"
	s = s & "<BODY"
	s = s & "	BGCOLOR=""" & cBodyBG & """"
	s = s & "	TEXT=""" & cText & """ LINK=""" & cLink & """ VLINK=""" & cVLink & """ ALINK=""" & cALink & """"
	s = s & "	LEFTMARGIN=""" & PAGE_LEFT_MARGIN & """ TOPMARGIN=""" & PAGE_TOP_MARGIN & """ MARGINWIDTH=""" & PAGE_LEFT_MARGIN & """ MARGINHEIGHT=""" & PAGE_TOP_MARGIN & """>"
	s = s & "<div style=""width:500;align:right;float:right;"">"
	s = s & "	<a href=""http://www.HBO.com/store/""><img src=""http://www.HBO.com/_images/banners/banner-sm2.jpg"" border=""0"" /></a>"
	s = s & "</div>"
	s = s & "<TABLE BORDER=""0"" CELLPADDING=""0"" CELLSPACING=""0"" WIDTH=""" & INT_PAGE_WIDTH & """>"
	s = s & "<TR>"
	s = s & "	<TD>"

	' BODY


	if strMessage <> "" then
		s = s & "<TABLE BORDER=1 CELLPADDING=2 CELLSPACING=0 WIDTH=""100%"">"
		s = s & "<TR>"
		s = s & "	<TD>" & strMessage & "</TD>"
		s = s & "</TR>"
		s = s & "</TABLE>"
		s = s & "<BR>"
	end if


	s = s & "		</TD>"
	s = s & "	</TR>"
	s = s & "	</TABLE></TD>"
	s = s & "</TR>"
	s = s & "</TABLE>"
	s = s & "<BR>"
	
	' FOOTER
	s = s & "	</TD>"
	s = s & "</TR>"
	s = s & "</TABLE>"
	s = s & "</BODY>"
	s = s & "</HTML>"


GetGenericEmailHTML = s
End function

sub SendOrderShipEmail_Other(intOrderID)
	dim strMessage, strSQL, rsData
	strSQL = "SELECT O.*, A.vchFirstName + ' ' + A.vchLastName AS A_vchShopperAccount, A.vchEmail AS A_vchEmail, B.vchEmail AS B_vchEmail, S.vchEmail AS S_vchEmail"
	strSQL = strSQL & " FROM " & STR_TABLE_ORDER & " AS O"
	strSQL = strSQL & " LEFT JOIN " & STR_TABLE_SHOPPER & " AS A ON (A.intID = O.intShopperID)"
	strSQL = strSQL & " LEFT JOIN " & STR_TABLE_SHOPPER & " AS B ON (B.intID = O.intBillShopperID)"
	strSQL = strSQL & " LEFT JOIN " & STR_TABLE_SHOPPER & " AS S ON (S.intID = O.intShipShopperID)"
	strSQL = strSQL & " WHERE O.intID=" & intOrderID & " AND (O.chrStatus<>'D') AND (A.chrType='A' OR A.chrType IS NULL)"
	set rsData = gobjConn.execute(strSQL)
	if not rsData.eof then
		dim strEmail1, strEmail2, strEmail3
		strEmail1 = rsData("A_vchEmail") & ""
		strEmail2 = rsData("B_vchEmail") & ""
		'strEmail3 = rsData("S_vchEmail") & ""
		
		if strEmail1 = strEmail2 then
			strEmail2 = ""
		end if
		if (strEmail1 = strEmail3) or (strEmail2 = strEmail3) then
			strEmail3 = ""
		end if
		
		if len(rsData("vchShippingNumber") & "") > 0 then
			'strMessage = "SHIPMENT CONFIRMATION - Your order has been shipped by UPS. Your shipping tracking number is: " & rsData("vchShippingNumber")
			strMessage = "SHIPMENT CONFIRMATION - We thought you would like to know that your order has been shipped today. Thank you for shopping at http://www.HBO.com. Your shipping tracking number is: " & rsData("vchShippingNumber")
		else
			strMessage = "SHIPMENT CONFIRMATION - Your order has been shipped. If you have any questions, please contact our customer service staff at the address or phone number shown above."
		end if
		SendInvoiceMail rsData, strMessage, strEmail1, strEmail2, strEmail3
	end if
	rsData.close
	set rsData = nothing
end sub

sub SendOrderEmail_Other(intOrderID, strMessage)
	dim strSQL, rsData
	strSQL = "SELECT O.*, A.vchFirstName + ' ' + A.vchLastName AS A_vchShopperAccount, A.vchEmail AS A_vchEmail, B.vchEmail AS B_vchEmail, S.vchEmail AS S_vchEmail FROM ((" & STR_TABLE_ORDER & " AS O LEFT JOIN " & STR_TABLE_SHOPPER & " AS A ON O.intShopperID = A.intID) LEFT JOIN " & STR_TABLE_SHOPPER & " AS B ON O.intBillShopperID = B.intID) LEFT JOIN " & STR_TABLE_SHOPPER & " AS S ON O.intShipShopperID = S.intID WHERE O.intID=" & intOrderID & " AND O.chrStatus<>'D' AND (A.chrType='A' OR A.chrType IS NULL)"
	set rsData = gobjConn.execute(strSQL)
	if not rsData.eof then
		dim strEmail1, strEmail2, strEmail3
		strEmail1 = rsData("A_vchEmail") & ""
		strEmail2 = rsData("B_vchEmail") & ""
		'strEmail3 = rsData("S_vchEmail") & ""
		
		if strEmail1 = strEmail2 then
			strEmail2 = ""
		end if
		if (strEmail1 = strEmail3) or (strEmail2 = strEmail3) then
			strEmail3 = ""
		end if
		
		SendInvoiceMail rsData, strMessage, strEmail1, strEmail2, strEmail3
	end if
	rsData.close
	set rsData = nothing
end sub


sub SendOrderEmail_OtherMAF(intOrderID, strMessage)
	dim strSQL, rsData
	strSQL = "SELECT O.*, A.vchFirstName + ' ' + A.vchLastName AS A_vchShopperAccount, A.vchEmail AS A_vchEmail, B.vchEmail AS B_vchEmail, S.vchEmail AS S_vchEmail FROM ((" & STR_TABLE_ORDER & " AS O LEFT JOIN " & STR_TABLE_SHOPPER & " AS A ON O.intShopperID = A.intID) LEFT JOIN " & STR_TABLE_SHOPPER & " AS B ON O.intBillShopperID = B.intID) LEFT JOIN " & STR_TABLE_SHOPPER & " AS S ON O.intShipShopperID = S.intID WHERE O.intID=" & intOrderID & " AND O.chrStatus<>'D' AND (A.chrType='A' OR A.chrType IS NULL)"
	set rsData = gobjConn.execute(strSQL)
	if not rsData.eof then
		dim strEmail1, strEmail2, strEmail3
		strEmail1 = rsData("A_vchEmail") & ""
		strEmail2 = rsData("B_vchEmail") & ""
		'strEmail3 = rsData("S_vchEmail") & ""
		
		if strEmail1 = strEmail2 then
			strEmail2 = ""
		end if
		if (strEmail1 = strEmail3) or (strEmail2 = strEmail3) then
			strEmail3 = ""
		end if
		
		SendInvoiceMailMAF rsData, strMessage, strEmail1, strEmail2, strEmail3
	end if
	rsData.close
	set rsData = nothing
end sub

function GetOrderShippingPrice_Other(intOrderID)
	GetOrderShippingPrice_Other = 0
	dim strSQL, rsTemp
	strSQL = "SELECT mnyShipAmount FROM " & STR_TABLE_ORDER & " WHERE intID=" & intOrderID
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		if not IsNull(rsTemp("mnyShipAmount")) then
			GetOrderShippingPrice_Other = rsTemp("mnyShipAmount")
		end if
	end if
	rsTemp.close
	set rsTemp = nothing
	
	Response.Write "inside<br />"
	
	
end function

function GetOrderShippingZone(mnySubtotal, strCountry, strState, intShipOption)
	dim mnyShipPrice
	mnySubtotal = 0
	strCountry = ""
	intShipOption = 0
	if gintOrderID > 0 then
		GetOrderShippingZone = GetOrderShippingZone_Other(gintOrderID, mnySubtotal, strCountry, strState, intShipOption, mnyShipPrice)
	end if
end function

function GetOrderShippingZone_Other(intOrderID, mnySubtotal, strCountry, strState, intShipOption, mnyShipPrice)
	dim strSQL, rsTemp
	mnySubtotal = 0
	strCountry = ""
	strState = ""
	intShipOption = 0
	strSQL = "SELECT ISNULL(mnyTaxSubtotal,0) + ISNULL(mnyNonTaxSubtotal,0) - ISNULL(mnyShipAmount,0) AS mnySubTotal, O.mnyShipAmount, S.vchCountry, S.vchState, O.intShipOption"
	strSQL = strSQL & " FROM " & STR_TABLE_ORDER & " AS O, " & STR_TABLE_SHOPPER & " AS S"
	strSQL = stRSQL & " WHERE O.intID=" & intOrderID & " AND O.intShipShopperID = S.intID"
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		mnySubtotal = rsTemp("mnySubTotal")
		strCountry = rsTemp("vchCountry")
		strState = rsTemp("vchState")
		intShipOption = rsTemp("intShipOption")
		mnyShipPrice = rsTemp("mnyShipAmount")
	end if
	rsTemp.close
	set rsTemp = nothing
end function

sub SetOrderTaxExempt(intTaxZone)
	if gintOrderID > 0 then
		SetOrderTaxExempt_Other gintOrderID, 0, 0
	end if
end sub

sub SetOrderTaxExempt_Other(intOrderID, intTaxZone, mnyTaxAmount)
	dim dctSaveList, fltTaxRate
	fltTaxRate = 0.0
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "vchUpdatedByUser", "pub"
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "#intTaxZone", intTaxZone
		dctSaveList.Add "#fltTaxRate", fltTaxRate
		dctSaveList.Add "#mnyTaxAmount", mnyTaxAmount
	SaveDataRecord STR_TABLE_ORDER, Request, intOrderID, dctSaveList
	RecalcOrder_Other intOrderID
end sub

sub SetOrderShipOption(intShipOption)
	if gintOrderID > 0 then
		SetOrderShipOption_Other gintOrderID, intShipOption, 0
	end if
end sub

sub SetOrderShipOption_Other(intOrderID, intShipOption, mnyShipPrice)
	dim dctSaveList
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "vchUpdatedByUser", "pub"
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "#intShipOption", intShipOption
'	if (intShipOption = 6 or intShipOption = 7) then
	if (intShipOption = STR_SHIP_INTRNL or intShipOption = STR_SHIP_CUSTOM) then
		dctSaveList.Add "#mnyShipAmount", mnyShipPrice
	end if

	SaveDataRecord STR_TABLE_ORDER, Request, intOrderID, dctSaveList
	RecalcOrder_Other intOrderID
end sub

function GetOrderShipOption_Other(intOrderID)
	dim strSQL, rsTemp
	strSQL = "SELECT intShipOption FROM " & STR_TABLE_ORDER & " WHERE intID=" & intOrderID
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		GetOrderShipOption_Other = rsTemp("intShipOption")
	else
		GetOrderShipOption_Other = 0
	end if
	rsTemp.close
	set rsTemp = nothing
end function

' old method - looks ups receive email from user table

sub SendMerchantEmailX(strSubject, strMessage)
	dim strSQL, rsTemp
	strSQL = "SELECT vchReceiveEmail FROM " & STR_TABLE_USER & " WHERE chrStatus='A' AND vchReceiveEmail IS NOT NULL"
	set rsTemp = gobjConn.execute(strSQL)
	while not rsTemp.eof
'		SendMail "webuser@americanwebservices.com", rsTemp("vchReceiveEmail"), "", "mred@americanwebservices.com", strSubject, strMessage
		SendMail "webuser@americanwebservices.com", rsTemp("vchReceiveEmail"), "", "", strSubject, strMessage
		rsTemp.MoveNext
	wend
	rsTemp.close
	set rsTemp = nothing
end sub

' new method - link merchant brandid to inventory brandid by joining the inventory items to the order line items.

sub SendMerchantEmail(strSubject, strMessage)
	dim strSQL, rsTemp
	strSQL = "SELECT  O.intOrderID, U.intBrand, U.vchReceiveEmail"
	strSQL = strSQL & " FROM " & STR_TABLE_LINEITEM & " AS O, " & STR_TABLE_INVENTORY & " AS I, "  & STR_TABLE_USER & " AS U "
	strSQL = strSQL & " WHERE (O.chrStatus='A') AND (O.intOrderID = '" & gintOrderID & "')"
	strSQL = strSQL & " AND (I.chrStatus='A') AND (I.intID = O.intInvID) "
	if left(strSubject,18) = "Order Notification" then
'		order has been appoved - send email to admin and merchants...
		strSQL = strSQL & " AND (U.chrStatus='A') AND ((U.intBrand = I.intBrand) OR (IsNull(U.intBrand,0) = 0)) "
	else
'		order has NOT been approved - send email to admin only...
		strSQL = strSQL & " AND (U.chrStatus='A') AND (IsNull(U.intBrand,0) = 0) "
	end if
	strSQL = strSQL & " AND (IsNull(U.vchReceiveEmail,'') <> '') "
	strSQL = strSQL & " GROUP BY O.intOrderID, U.intBrand, U.vchReceiveEmail"
'	response.write strSQL & "<br />"
'	response.end
	
	set rsTemp = gobjConn.execute(strSQL)
	dim strTo, strNewMessage, n
	strNewMessage = strMessage
	strTo = ""
	n = 0
	while not rsTemp.eof
		if IsValid(rsTemp("vchReceiveEmail")&"") then
			strTo = strTo & iif(n > 0, ", ", " ") & rsTemp("vchReceiveEmail") & ""
			n = n + 1
		end if
		rsTemp.MoveNext
	wend
	rsTemp.close
	set rsTemp = nothing

	strNewMessage = strNewMessage & vbcrlf & "Message sent to: " & n & " users." & vbcrlf
	if lcase(strServerHost) = "awsdev" then
		' running from internal
		strNewMessage = strNewMessage & strTo & vbcrlf
		SendMail STR_MERCHANT_CS_EMAIL, "aleach@americanwebservices.com", "", "", strSubject, strNewMessage
	else
		if IsValid(strTo) then
'			SendMail STR_MERCHANT_CS_EMAIL, strTo, "", "mred@americanwebservices.com", strSubject, strNewMessage
			SendMail STR_MERCHANT_CS_EMAIL, strTo, "", "", strSubject, strNewMessage
		end if
	end if
end sub

function MakeShipPriceMatrixTable(aryShipBaseMatrix)
	dim s, a, x
	s = "<TABLE BORDER=""0"" CELLPADDING=""0"" CELLSPACING=""0"">"
	x = ubound(aryShipBaseMatrix) - 1
	for a = 0 to x
		s = s & "<TR><TD>" & font(1) & "Up to " & FormatCurrency(aryShipBaseMatrix(a,0),2) & "...&nbsp;&nbsp;</TD>"
		s = s & "<TD>" & font(1) & FormatCurrency(aryShipBaseMatrix(a,1),2) & "</TD></TR>"
	next
	s = s & "<TR><TD>" & font(1) & "Over " & FormatCurrency(aryShipBaseMatrix(x,0),2) & "...&nbsp;&nbsp;</TD>"
	s = s & "<TD>" & font(1) & FormatNumber(aryShipBaseMatrix(x + 1,0) * 100,0) & "% of subtotal</TD></TR>"
	s = s & "</TABLE>"
	MakeShipPriceMatrixTable = s
end function

function GetShipPriceMatrixTable(mnySubTotal, aryShipBaseMatrix, mnyBaseShipping)
	redim aryShipBaseMatrix(6,1)

	' price matrix for base shipping:
	'   (n,0) = upper-limit for sub-total :  (n,1) = shipping price for this level
	' last item in matrix: (n,0) = percentage against sub-total to determine shipping price
	aryShipBaseMatrix(0,0) = 25.00 : aryShipBaseMatrix(0,1) = 7.97
	aryShipBaseMatrix(1,0) = 50.00 : aryShipBaseMatrix(1,1) = 9.97
	aryShipBaseMatrix(2,0) = 75.00 : aryShipBaseMatrix(2,1) = 14.97
	aryShipBaseMatrix(3,0) = 100.00 : aryShipBaseMatrix(3,1) = 19.97
	aryShipBaseMatrix(4,0) = 150.00 : aryShipBaseMatrix(4,1) = 24.97
	aryShipBaseMatrix(5,0) = 200.00 : aryShipBaseMatrix(5,1) = 29.97
	aryShipBaseMatrix(6,0) = 0.15 : aryShipBaseMatrix(6,1) = -1
'	aryShipBaseMatrix(7,0) = 0.0 : aryShipBaseMatrix(6,1) = -1
	
	dim c, x
	x = ubound(aryShipBaseMatrix) - 1
	c = 0
	mnyBaseShipping = -1
	while (c <= x) and (mnyBaseShipping = -1)
		if mnySubTotal < aryShipBaseMatrix(c,0) then
			mnyBaseShipping = aryShipBaseMatrix(c,1)
		end if
		c = c + 1
	wend
	if (mnyBaseShipping = -1) then
		mnyBaseShipping = Fix(mnySubTotal * aryShipBaseMatrix(c,0) * 100) / 100
	end if
	
	GetShipPriceMatrixTable = aryShipBaseMatrix
end function

sub GetShipPriceMatrix(intShipOption, mnySubTotal, aryShipBaseMatrix, aryShipUPS_Matrix, aryShipOption, mnyBaseShipping)
	aryShipBaseMatrix = GetShipPriceMatrixTable(mnySubTotal, aryShipBaseMatrix, mnyBaseShipping)
	redim aryShipOption(7,2)
	' shipping option matrix:
	'   (n,0) = shipping option value
	'   (n,1) = shipping option label
	'   (n,2) = shipping price

	dim intShipWeight
	intShipWeight = PollShippingWeightByOrder(gintOrderID)
	aryShipUPS_Matrix = PollShipPriceMatrixUPS(intShipOption, aryShipUPS_Matrix, intShipWeight)

	dim x, xCnt, maxOption, maxX
	maxOption = ubound(aryShipUPS_Matrix)
	maxX = ubound(aryShipOption)

' deactivate standard shipping per Ed (client doesn't want shipping based on price)
if true then ' and intShipWeight > 0
	redim aryShipOption(maxOption+4,2)
	maxX = ubound(aryShipOption)
 	aryShipOption(0,0) = 1 
	aryShipOption(0,1) = "<B>Standard Shipping</B><BR>" & MakeShipPriceMatrixTable(aryShipBaseMatrix)
	aryShipOption(0,2) = mnyBaseShipping

	' new UPS shipping options
	for xCnt = 0 to maxOption
		x = xCnt + 1
		if x <= maxX then
			aryShipOption(x,0) = iif((aryShipUPS_Matrix(xCnt,1)>0),x+1,-1)
			aryShipOption(x,1) = aryShipUPS_Matrix(xCnt,0)		'&"<br />"
			aryShipOption(x,2) = aryShipUPS_Matrix(xCnt,1)
		end if
	next

	' old UPS shipping options
'	aryShipOption(1,0) = -1	'2
'	aryShipOption(1,1) = "<B>UPS Ground</B><BR>"
'	aryShipOption(1,2) = PollShippingPrice(2, intShipWeight)

'	aryShipOption(2,0) = -1	'3
'	aryShipOption(2,1) = "<B>UPS 2nd Day Air</B><BR>"
'	aryShipOption(2,2) = PollShippingPrice(3, intShipWeight)

'	aryShipOption(3,0) = -1	'4
'	aryShipOption(3,1) = "<B>UPS Overnight</B><BR>"
'	aryShipOption(3,2) = PollShippingPrice(4, intShipWeight)
'	dim x
'	for x = 1 to 3
'		if aryShipOption(x,2) > 0 then
'			aryShipOption(x,0) = x+1
'		end if
'	next

	x = x+1
	aryShipOption(x,0) = x+1	'5
	aryShipOption(x,1) = "<B>International Orders</B><BR>Shipping cost determined on a case by case basis. Customers will be notified of the final price before final billing."
	aryShipOption(x,2) = 0

	x = x+1
	aryShipOption(x,0) = x+1	'6
	aryShipOption(x,1) = "<B>Custom Orders</B><BR>Shipping cost determined on a case by case basis. Customers will be notified of the final price before final billing."
	aryShipOption(x,2) = 0

	x = x+1
	aryShipOption(x,0) = x+1	'7
	aryShipOption(x,1) = "<B>Free Shipping</B><BR>No Shipping cost on this item."
	aryShipOption(x,2) = 0

else
 	aryShipOption(0,0) = 1
	aryShipOption(0,1) = "<B>Standard Shipping</B><BR>" & MakeShipPriceMatrixTable(aryShipBaseMatrix)
	aryShipOption(0,2) = mnyBaseShipping

	' old shipping options added costs to baseshipping
	aryShipOption(1,0) = 2
	aryShipOption(1,1) = "<B>2nd Day Air</B><BR>add $15.97 to base rate"
	aryShipOption(1,2) = mnyBaseShipping + 15.97
	
	aryShipOption(2,0) = 3
	aryShipOption(2,1) = "<B>Overnight Service</B><BR>add $19.97 to base rate"
	aryShipOption(2,2) = mnyBaseShipping + 19.97

	aryShipOption(3,0) = 4
	aryShipOption(3,1) = "<B>Saturday Delivery</B><BR>add $27.97 to base rate"
	aryShipOption(3,2) = mnyBaseShipping + 27.97

	aryShipOption(4,0) = 5
	aryShipOption(4,1) = "<B>International Orders</B><BR>Shipping cost determined on a case by case basis. Customers will be notified of the final price before final billing."
	aryShipOption(4,2) = 0

	aryShipOption(5,0) = 6
	aryShipOption(5,1) = "<B>Custom Orders</B><BR>Shipping cost determined on a case by case basis. Customers will be notified of the final price before final billing."
	aryShipOption(5,2) = 0

	aryShipOption(6,0) = 7
	aryShipOption(6,1) = "<B>Free Shipping</B><BR>No Shipping cost on this item."
	aryShipOption(6,2) = 0
end if
end sub

sub GetShipPriceMatrix_Other(intOrderID, intShipOption, mnySubTotal, aryShipBaseMatrix, aryShipUPS_Matrix, aryShipOption, mnyBaseShipping)
	redim aryShipOption(4,2)
	' shipping option matrix:
	'   (n,0) = shipping option value
	'   (n,1) = shipping option label
	'   (n,2) = shipping price

	dim intShipWeight
	intShipWeight = PollShippingWeightByOrder(intOrderID)
	aryShipUPS_Matrix = PollShipPriceMatrixUPS(intShipOption, aryShipUPS_Matrix, intShipWeight)

	dim x, xCnt, maxOption
	maxOption = ubound(aryShipUPS_Matrix)

	aryShipOption(0,0) = 1
	aryShipOption(0,1) = "<B>US Postal Service</B><BR>"
	aryShipOption(0,2) = PollShippingPriceByOrder(intOrderID,1,intShipWeight)
	aryShipOption(1,0) = -1	'2

	' new UPS shipping options
	for xCnt = 0 to maxOption
		x = xCnt + 1
		aryShipOption(x,0) = iif((aryShipUPS_Matrix(xCnt,1)>0),x+1,-1)
		aryShipOption(x,1) = aryShipUPS_Matrix(xCnt,0)		'&"<br />"
		aryShipOption(x,2) = aryShipUPS_Matrix(xCnt,1)
	next

	' old UPS shipping options
'	aryShipOption(1,1) = "<B>UPS Ground</B><BR>"
'	aryShipOption(1,2) = PollShippingPriceByOrder(intOrderID,2,intShipWeight)
'	aryShipOption(2,0) = -1	'3

'	aryShipOption(2,1) = "<B>UPS 2nd Day Air</B><BR>"
'	aryShipOption(2,2) = PollShippingPriceByOrder(intOrderID,3,intShipWeight)
'	if aryShipOption(1,2) > 0 then
'		aryShipOption(1,0) = 2
'	end if
'	if aryShipOption(2,2) > 0 then
'		aryShipOption(2,0) = 3
'	end if

end sub

function PollShippingWeightByOrder (intOrderID)
	dim strSQL, rsWeight, intShipWeight
	strSQL = "SELECT ISNULL(SUM(ISNULL(I.fltShipWeight * L.intQuantity, 0)), 0) AS fltShipWeight FROM " & STR_TABLE_LINEITEM & " L, " & STR_TABLE_INVENTORY & " I "
	strSQL = strSQL & " WHERE (L.chrStatus='A') AND (L.intOrderID=" & intOrderID &") AND (I.intID=L.intInvID)"

'	if intShipOption = 1 then	' don't include free shipping items in total if ground
'		strSQL = strSQL & " AND chrFreeShip<>'Y'"
'	end if
'	response.write strSQL & "<br />"

	set rsWeight = gobjConn.execute(strSQL)
	intShipWeight = rsWeight("fltShipWeight")
	if intShipWeight > 0 then
		intShipWeight = cDbl(intShipWeight) + 0.5 '0.5lbs for box/envelope weight
	end if
	rsWeight.close
	set rsWeight = nothing
'	response.write "intOrderID = " & intOrderID & "&nbsp;&nbsp;"
'	response.write "intShipWeight = " & intShipWeight & "<br />"
	
	if intShipWeight > 250 then intShipWeight = 250
	PollShippingWeightByOrder = intShipWeight
end function

function PollShipPriceMatrixUPS (intShipOption, aryShipUPS_Matrix, intShipWeight)
	redim aryShipUPS_Matrix(6,1)
	' Build UPS Shipping Price Matrix
	PollShipPriceMatrixUPS = UPS_Trans_Matrix(gintOrderID, intShipOption, aryShipUPS_Matrix, intShipWeight)
end function

function PollShippingPrice (intShipOption, intShipWeight)
	PollShippingPrice = PollShippingPriceByOrder(gintOrderID, intShipOption, intShipWeight)
end function

function PollShippingPriceByOrder (intOrderID, intShipOption, intShipWeight)
	dim fltPrice
	if IsNumeric(intShipWeight) then
		intShipWeight = cDbl(intShipWeight)
	else
		intShipWeight = 0
	end if
	if intShipWeight = 0 then
		intShipWeight = PollShippingWeightByOrder(intOrderID)
	end if
	if intShipWeight < 1 then
		intShipWeight = 1
	end if
'	response.write "intOrderID = " & intOrderID & "&nbsp;&nbsp;"
'	response.write "intShipOption = " & intShipOption & "&nbsp;&nbsp;"
'	response.write "intShipWeight = " & intShipWeight & "<br />"

select case intShipOption
case 1
	dim intOrderShipID
	intOrderShipID = GetOrderBillID_Other(intOrderID,true)

	dim strSQL, rsTemp, intZone, intRate, strZip
	dim strLabel, strEmail, strFirstName, strLastName, strCompany, strAddress1, strAddress2, strCity, strState, strCountry, strDayPhone, strNightPhone, strFax, intTaxZone
	GetOrderAddress_Other intOrderID, true, intOrderShipID, strLabel, strEmail, strFirstName, strLastName, strCompany, strAddress1, strAddress2, strCity, strState, strZip, strCountry, strDayPhone, strNightPhone, strFax, intTaxZone

'	response.write "Zip Code: " & strZip & "<br />"
	
	' US Postal Service
	strZip = Left(strZip,3)
	
	strSQL = "SELECT * "
	strSQL = strSQL & " FROM " & STR_TABLE_SHIPZONE & " WHERE intHigh >= " & strZip & " AND intLow <= " & strZip
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.EOF then
		intZone = rsTemp("intZone")
	end if
	rsTemp.close
	set rsTemp = nothing

'	response.write "intZone: " & intZone & "<br />"
	
	strSQL = "SELECT * "
	strSQL = strSQL & " FROM " & STR_TABLE_SHIPRATE & " WHERE intShipWeight = " & round(intShipWeight)
	set rsTemp = gobjConn.execute(strSQL)
	
	If not rsTemp.EOF then
		select case intZone
		case 1, 2, 3
			fltPrice = rsTemp("mnyZone1")
		case 4
			fltPrice = rsTemp("mnyZone4")
		case 5
			fltPrice = rsTemp("mnyZone5")
		case 6
			fltPrice = rsTemp("mnyZone6")
		case 7
			fltPrice = rsTemp("mnyZone7")
		case 8
			fltPrice = rsTemp("mnyZone8")
		case else
			fltPrice = rsTemp("mnyZone8")
		end select
	else ' no rates found for this weight
		fltPrice = 3.85
	end if
	fltPrice = fltPrice + .45  ' USPS Shipping Confirmation
	rsTemp.close
	set rsTemp = nothing

case 2, 3, 4
	' 2 - UPS Ground
	' 3 - UPS 2nd Day Air
	' 4 - UPS Overnight
	if IsNumeric(intShipOption) then
		intShipOption = CLng(intShipOption)
		if intShipOption < 1 then
			intShipOption = 1
		end if
	else
		intShipOption = 1
	end if
	if IsNumeric(intShipWeight) then
		intShipWeight = cDbl(intShipWeight)
		if intShipWeight < 1 then
			intShipWeight = 1
		end if
	else
		intShipWeight = 1
	end if
	fltPrice = UPS_Trans_Run(intOrderID, intShipOption, intShipWeight)
	if fltPrice > 0 and false then
		fltPrice = fltPrice + 1.00  ' UPS Shipping Confirmation
	end if
case else
	fltPrice = 0
end select
	
'	response.write "Shipping: " & fltPrice & "<br />"

	if fltPrice < 0 then
		fltPrice = 0
	elseif fltPrice > 0 and false then
		fltPrice = fltPrice + 2.00 ' ADD $2 Handling Fee to all LavenderLane orders
	'	response.write "Total S&H Price: " & fltPrice & "<br />"
	end if
	
	PollShippingPriceByOrder = fltPrice
end function

function GetShipPriceByOption(mnySubTotal, intShipOption)
	dim c, x, aryShipBaseMatrix, aryShipUPS_Matrix, aryShipOption, mnyBaseShipping
	GetShipPriceMatrix intShipOption, mnySubTotal, aryShipBaseMatrix, aryShipUPS_Matrix, aryShipOption, mnyBaseShipping
	x = ubound(aryShipOption)
	for c = 0 to x
		if aryShipOption(c,0) = intShipOption then
			GetShipPriceByOption = aryShipOption(c,2)
			exit function
		end if
	next
	GetShipPriceByOption = 0
end function

function GetOrderGiftMessage()
	GetOrderGiftMessage = GetOrderGiftMessage_Other(gintOrderID)
end function

function GetOrderGiftMessage_Other(intOrderID)
	dim strSQL, rsTemp
	strSQL = "SELECT txtGiftMessage FROM " & STR_TABLE_ORDER & " WHERE intID=" & intOrderID
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		GetOrderGiftMessage_Other = rsTemp("txtGiftMessage") & ""
	end if
	rsTemp.close
	set rsTemp = nothing
end function

sub SetOrderGiftMessage(strMsg)
	SetOrderGiftMessage_Other gintOrderID, strMsg
end sub

sub SetOrderGiftMessage_Other(intOrderID, byVal strMsg)
	if intOrderID > 0 then
		dim strSQL
		if trim(strMsg & "") = "" then
			strMsg = "none"
		end if
		strSQL = "UPDATE " & STR_TABLE_ORDER & " SET txtGiftMessage='" & SQLEncode(strMsg) & "' WHERE intID=" & intOrderID
		gobjConn.execute(strSQL)
	end if
end sub

sub UpdateOrderGiftMessage(strGiftMessage)
	' updates the gift message of the current order
	UpdateOrderGiftMessage_Other gintOrderID, strGiftMessage
'	gstrGiftMessage = strMessage
end sub

sub UpdateOrderGiftMessage_Other(intOrderID, strGiftMessage)
	if intOrderID > 0 and strGiftMessage <> "" then
		dim dctSaveList
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "@dtmUpdated", "GETDATE()"
			dctSaveList.Add "vchUpdatedByUser", "pub"
			dctSaveList.Add "vchUpdatedByIP", gstrUserIP
			dctSaveList.Add "txtGiftMessage", strGiftMessage
		SaveDataRecord STR_TABLE_ORDER, Request, intOrderID, dctSaveList
	end if
end sub




function GetOrderReferral()
	GetOrderReferral = GetOrderReferral_Other(gintOrderID)
end function

function GetOrderReferral_Other(intOrderID)
	dim strSQL, rsTemp
	strSQL = "SELECT vchCustomReferralName FROM " & STR_TABLE_ORDER & " WHERE intID=" & intOrderID
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		GetOrderReferral_Other = rsTemp("vchCustomReferralName") & ""
	end if
	rsTemp.close
	set rsTemp = nothing
end function

sub SetOrderReferral(strRef)
	SetOrderReferral_Other gintOrderID, strRef
end sub

sub SetOrderReferral_Other(intOrderID, byVal strMsg)
	if intOrderID > 0 then
		dim strSQL
		if trim(strMsg & "") = "" then
			strMsg = "none"
		end if
		strSQL = "UPDATE " & STR_TABLE_ORDER & " SET vchCustomReferralName='" & SQLEncode(strMsg) & "' WHERE intID=" & intOrderID
		gobjConn.execute(strSQL)
	end if
end sub

sub UpdateOrderReferral(strRef)
	' updates the gift message of the current order
	UpdateOrderReferral_Other gintOrderID, strRef
'	gstrGiftMessage = strMessage
end sub

sub UpdateOrderReferral_Other(intOrderID, strRef)
	if intOrderID > 0 and strGiftMessage <> "" then
		dim dctSaveList
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "@dtmUpdated", "GETDATE()"
			dctSaveList.Add "vchUpdatedByUser", "pub"
			dctSaveList.Add "vchUpdatedByIP", gstrUserIP
			dctSaveList.Add "vchCustomReferralName", strRef
		SaveDataRecord STR_TABLE_ORDER, Request, intOrderID, dctSaveList
	end if
end sub





sub SetOrderDiscount_Other(intOrderID, byVal intRefID, strRefName, fltRefDiscount)
	
	if intOrderID > 0 then
		if IsNumeric(intRefID) then
			intRefID = CLng(intRefID)
			if intRefID >=0 then
				dim strSQL
				strSQL = "UPDATE " & STR_TABLE_ORDER & " SET intReferalID=" & intRefID & ", vchReferalName=(SELECT vchCompany FROM HBO_Referer WHERE intID=" & intRefID & "), fltReferalDiscount=(SELECT IsNull(fltDiscount,0) FROM HBO_Referer WHERE intID=" & intRefID & ")" '& fltRefDiscount
				strSQL = strSQL & " WHERE intID=" & intOrderID
			'	response.write strSQL & "<br />"
				
				gobjConn.execute(strSQL)
				RecalcOrder
			end if
		elseif strRefName  & "" <> "" then
			'///
			'/// just add the referral 
			'///		
			strSQL = "UPDATE " & STR_TABLE_ORDER & " SET vchReferalName='" & SQLEncode(strRefName & "") & "'"
			strSQL = strSQL & " WHERE intID=" & intOrderID
			response.write strSQL & "<br />"
			gobjConn.execute(strSQL)
		end if
		
	end if
end sub

function LoginUserAOL(strEmail, strAOL_ID)
	' attempt to login the specified user
	' if login is successful and there is a current order in progress,
	' assign this user to the order
	' returns true if login successful
	dim strSQL, rsTemp
	strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE chrStatus='A' AND vchAOL_ID='" & SQLEncode(strAOL_ID) & "'"
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		gintUserID = CLng(rsTemp("intID"))
		Session(SESSION_PUB_USER_ID) = gintUserID
		Session(SESSION_PUB_USER_NAME) = rsTemp("vchFirstName") & " " & rsTemp("vchLastName")
		gintUserName = Session(SESSION_PUB_USER_NAME)
		LoginUserAOL = true
		if gintOrderID > 0 then
			strSQL = "UPDATE " & STR_TABLE_ORDER & " SET intShopperID=" & gintUserID & " WHERE intID=" & gintOrderID
			gobjConn.execute(strSQL)
		end if
	else
		LoginUserAOL = false
	end if
	rsTemp.close
	set rsTemp = nothing
end function

function CreateNewUserAOL(strEmail, strAOLID, strFirstname, strLastname)
	' creates a new user
	' returns the user ID of the new account
	' returns zero if duplicate username
	' logs in the new user
	dim strSQL, rsTemp
	strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE chrStatus<>'D' AND vchAOL_ID='" & SQLEncode(strAOLID) & "'"
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		CreateNewUserAOL = 0
		rsTemp.close
		set rsTemp = nothing
	else
		rsTemp.close
		set rsTemp = nothing
		dim dctSaveList
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "*@dtmCreated", "GETDATE()"
			dctSaveList.Add "@dtmUpdated", "GETDATE()"
			dctSaveList.Add "*vchCreatedByUser", "pub"
			dctSaveList.Add "vchUpdatedByUser", "pub"
			dctSaveList.Add "*vchCreatedByIP", gstrUserIP
			dctSaveList.Add "vchUpdatedByIP", gstrUserIP
			dctSaveList.Add "chrType", "A"
			dctSaveList.Add "*chrStatus", "A"
			dctSaveList.Add "vchEmail", strEmail
			dctSaveList.Add "vchHint", "Created by AOL QuickCheckout"
			dctSaveList.Add "vchLastname", strLastname
			dctSaveList.Add "vchFirstname", strFirstname
			dctSaveList.Add "vchAOL_ID", strAOLID
		CreateNewUserAOL = SaveDataRecord("" & STR_TABLE_SHOPPER, Request, 0, dctSaveList)
		LoginUserAOL strEmail, strAOLID
	end if
end function

function GetOptionDct(strOptionList)
	set GetOptionDct = Server.CreateObject("Scripting.Dictionary")
	if strOptionList <> "" and not IsNull(strOptionList) then
		if InStr(strOptionList, ",") > 0 then
			dim a, c, x
			a = split(strOptionList, ",")
			x = ubound(a)
			for c = 0 to x
				GetOptionDct.Add a(c) & "", a(c) & ""
			next
		else
			GetOptionDct.Add strOptionList & "", strOptionList & ""
		end if
	end if
end function

function GetSubscriptions(intID)
	dim strSQl
	strSQL = "SELECT O.intID, I.vchItemName, I.vchSoftURL"
	strSQL = strSQL & " FROM dbo.HBO_Inv AS I, dbo.HBO_LineItem AS L, dbo.HBO_Order AS O"
	strSQL = strSQL & " WHERE NOT I.vchSoftURL is NULL"
	strSQL = strSQL & " AND	L.intInvID = I.intID AND L.intOrderID = O.intID"
	strSQL = strSQL & " AND	L.chrStatus <> 'D' AND L.chrStatus <> 'D'"
	strSQL = strSQL & " AND	O.intShopperID = " & intID
	strSQL = strSQL & " AND	O.chrStatus IN ('X','H')"
	
	set GetSubscriptions = ConnOpenRS(strSQL)
end function

function GetExpressCheckOut(intOrderID)
	dim strSQl, rsTemp
	strSQL = "SELECT O.intID, I.vchItemName, I.vchSoftURL"
	strSQL = strSQL & " FROM dbo.HBO_Inv AS I, dbo.HBO_LineItem AS L, dbo.HBO_Order AS O"
	strSQL = strSQL & " WHERE NOT I.vchSoftURL is NULL"
	strSQL = strSQL & " AND L.intInvID = I.intID AND L.intOrderID = O.intID"
	strSQL = strSQL & " AND L.chrStatus <> 'D' AND I.chrStatus <> 'D'"
	strSQL = strSQL & " AND	O.intID = " & intOrderID
'	strSQL = strSQL & " AND	O.chrStatus IN ('X', 'H')"
'	response.write strSQL & "<br />"

	set rsTemp = ConnOpenRS(strSQL)
	if rsTemp.eof then
		GetExpressCheckOut = true
	else
		GetExpressCheckOut = false
	end if
	rsTemp.close
	set rsTemp = nothing
end function

sub SetOrderReferral(intOrderID)
	ConnExecute("UPDATE " & STR_TABLE_ORDER & " SET vchCustomReferralName='" & Session("rname") & "' WHERE intID = " & intOrderID & "")
end sub

sub GetReportDateRangeX(dtmStart, dtmEnd, RptType)
	dtmStart = Session(SESSION_REPORT_START)
	dtmEnd = Session(SESSION_REPORT_END)
	intRptType = Session(SESSION_REPORT_TYPE)
	
	if IsDate(dtmStart) then
		dtmStart = CDate(dtmStart)
	else
		dtmStart = CDate(Month(Date()) & "/1/" & Year(Date()))
		Session(SESSION_REPORT_START) = dtmStart
	end if
	dtmStart = DateValue(dtmStart)
	if IsDate(dtmEnd) then
		dtmEnd = CDate(dtmEnd)
	else
		dtmEnd = CDate(Date() & " 11:59:59 pm")
		Session(SESSION_REPORT_END) = dtmEnd
	end if
	if intRptType > 0 then
		if intRptType <= 3 then
			intRptType = Session(SESSION_REPORT_TYPE)
		else
			intRptType = 1
		end if
	else
		intRptType = 1
	end if
end sub

function GetSQLColumnName()

dim dtmStart, dtmEnd
GetReportDateRangeX dtmStart, dtmEnd, intRptType

select case intRptType
	case 2:
		GetSQLColumnName = " AND O.dtmSubmitted >= '" & dtmStart & "' AND O.dtmSubmitted <= '" & dtmEnd & "'"
	case else
		GetSQLColumnName = " AND O.dtmShipped >= '" & dtmStart & "' AND O.dtmShipped <= '" & dtmEnd & "'"
end select

end function

sub SetReportDateRangeX(dtmStart, dtmEnd, RptType)
	Session(SESSION_REPORT_START) = dtmStart
	Session(SESSION_REPORT_END) = dtmEnd
	Session(SESSION_REPORT_TYPE) = intRptType
end sub

function GetDateRangeType()

dim dtmStart, dtmEnd, intRptType
GetReportDateRangeX dtmStart, dtmEnd, intRptType

select case Session(SESSION_REPORT_TYPE)
	case 1:
		GetDateRangeType = "Date Shipped Range: " & DateValue(dtmStart) & " to " & DateValue(dtmEnd)
	case 2:
		GetDateRangeType = "Date Submitted Range: " & DateValue(dtmStart) & " to " & DateValue(dtmEnd)
end select
end function

function ResetLineItems()
    dim strSQL
    strSQL = "UPDATE " & STR_TABLE_LINEITEM & " SET chrStatus='D' WHERE intOrderID = " & gintORderID
    gobjConn.execute(strSQL)
end function

function Inventory_GetItemsFromCampaign(intParentID, intCampaignID, intprofileid, blnUnused1, blnUnused2) 
	dim strSQL
				
	strSQL = strSQL & "SELECT *"
    strSQL = strSQL & "FROM " & STR_TABLE_PROFILE_PACKAGES & " AS PP "
    strSQL = strSQL & "INNER JOIN " & STR_TABLE_INVENTORY & " AS I "
    strSQL = strSQL & "    ON PP.intitemid = I.intid "
    strSQL = strSQL & "        WHERE PP.intcampaignid = " & intCampaignID & " "
    if intprofileid > 0 then
        strSQL = strSQL & "    AND PP.intprofileid = " & intprofileid & " "
    end if
	
	strSQL = strSQL & " order by vchslotname"
    
	SET Inventory_GetItemsFromCampaign = Server.CreateObject("ADODB.recordset")

    Inventory_GetItemsFromCampaign.CursorLocation = adUseClient
    Inventory_GetItemsFromCampaign.CursorType = adOpenStatic
    Inventory_GetItemsFromCampaign.LockType = adLockBatchOptimistic
	Inventory_GetItemsFromCampaign.Open strSQL, gobjConn	
end function

Function ProtectSQL(SQLString)
	SQLString = Replace(SQLString, "'", "''") ' replace single Quotes with Double Quotes
	SQLString = Replace(SQLString, ">", "&gt;") ' replace > with &gt;
	SQLString = Replace(SQLString, "<", "&lt;") ' replace < with &lt;
	SQLString = Replace(SQLString, "(","&#40;") ' replace ( with &#40;
	SQLString = Replace(SQLString, ")","&#41;") ' replace ) with &#41;
	SQLString = Replace(SQLString, "&", "&amp;")
	SQLString = Replace(SQLString, "%", "&#37;")
	' replace vblf with <br /> (This is mainly used for Memo fields).
	SQLString = Replace(SQLString, vblf,"<br />") 
	SQLString = Trim(SQLString)
	ProtectSQL = SQLString
End Function
%>