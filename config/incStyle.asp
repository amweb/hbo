<%
' =====================================================================================
' = File: incStyle.asp
' = File Version: 5.1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Data validation functions
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

' Color Names
const cWhite =		"#FFFFFF"
const cVVLtGrey =	"#EEEEEE"
const cVLtGrey =	"#DDDDDD"
const cLtGrey =		"#CCCCCC"
const cGrey =		"#999999"
const cDkGrey =		"#666666"
const cVDkGrey =	"#333333"
const cBlack = 		"#000000"

const cRed =		"#FF0000"
const cDkRed = 		"#CD0215"
const cYellow =		"#FFFF00"
const cLtYellow =	"#FFFF99"
const cVLtYellow =	"#FFFFCC"
const cOrange = 	"#FFCC00"
const cPurple = 	"#660066"

const cGreen  =		"#003333"
const cDkBlue =		"#003399"
const cBlue = 		"#000099"
const cLtBlue = 	"#CCD9E5"
const cVDkBlue = 	"#000369"
const cBeige =		"#F4E9D2"
const cBrown =		"#663300"
const cLtrBrown =	"#996633"
const cLtstBrown =	"#CC9966"

' Page margins (set in BODY tag)
const PAGE_LEFT_MARGIN = 0
const PAGE_TOP_MARGIN = 0
const INT_PAGE_WIDTH = "100%"
const cDisabled = "#AAAAAA"

dim cBodyBG, cText, cLink, cVLink, cALink, cLight, cTitle
dim cInfoBarText, cInfoBarTextHilite
dim cFormBorder, cFormBG, cHeading, cLabelBG, cSubmitBG, cRule, cCaptionTxt, cErrorTxt, cErrorBG
dim cRow1, cRow2
dim stl_kintHeadingSize, stl_kintHeadingFace, stl_kintFormBorderWidth, stl_kintFormMargin, stl_kintFormSpacing, stl_kintFormPadding, stl_kintFormLabelSize

dim dctPageActions

' Body Colors
cBodyBG =   cWhite
cText = 	cBlack
cLink = 	cVDkBlue
cVLink = 	cDkRed
cALink =	cDkRed
cLight =	cBeige

cTitle = "#996666"

' Form colors
cFormBorder =	cWhite
cFormBG =		cWhite
cHeading =		cVDkBlue
cLabelBG =  	cWhite
cRule =			cVDkBlue
cCaptionTxt = 	cGreen
cErrorTxt = 	cRed
cErrorBG =		cVLtGrey
cSubmitBG =		cWhite
cInfoBarText = 	cBlack
cInfoBarTextHilite = cWhite

cRow1 =			cVLtGrey
cRow2 =			cVVLtGrey

stl_kintHeadingSize = 5
stl_kintHeadingFace = 6

stl_kintFormBorderWidth = 1
stl_kintFormMargin = 5
stl_kintFormSpacing = 4
stl_kintFormPadding = 4

stl_kintFormLabelSize = 1


' face(n) - will return the appropriate font face to use for the specified size
' Valid sizes: 1-6 (required)

' font(n) - will return a font tag for the appropriate font to use for the specified size
' Valid sizes: 1-6 (required)

' fontx(size, face, color) - will return a font tag for the given font
' Valid sizes: 1-6, or "" to leave unspecified
' Valid faces: 1-6 or a valid font name (required)
' Valid colors: an HTML color string, or "" to leave unspecified

' spacer(w,h) - returns a spacer tag with the specified width and height
'  width & height: both required, positive integers

' All stlXXX() functions will response.write their output
' stlHeading(s) - will output the string in a heading format
' stlCopyright() - will output a standard client-copyright line

dim face(6)
face(1) = "arial, verdana, geneva"
face(2) = face(1)
face(3) = face(2)
face(4) = "Arial Narrow, Helvetica Narrow, Tahoma, Aria, Helvetica"
face(5) = face(4)
face(6) = face(4)

' ========== Font ==========
function font(size)
	font = "<FONT SIZE=" & size & " FACE=""" & face(size) & """>"
end function

' ========== Fontx ==========
function fontx(size, theface, color)
	fontx = "<FONT"
	if size <> "" then fontx = fontx & " SIZE=" & size
	select case theface
		case "1","2","3","4","5","6"
			fontx = fontx & " FACE=""" & face(theface) & """"
		case else
			fontx = fontx & " FACE=""" & theface & """"
	end select
	if color <> "" then fontx = fontx & " COLOR=""" & color & """"
	fontx = fontx & ">"
end function

' ========== Spacer ==========
function spacer(width,height)
	spacer = "<IMG SRC=""" & imagebase & "spacer.gif"" WIDTH=" & width & " HEIGHT=" & height & ">"
end function

' ========== stlHeading ==========
sub stlHeading(strInput)
%>
	<%= fontx(stl_kintHeadingSize, stl_kintHeadingFace, cHeading) %><%= strInput %></FONT><BR>
<%
end sub

' ========== Copyright ==========
sub stlCopyright
%>
	<%= fontx(1,1,cGrey) %><%= gstrClientCopyright %></FONT>
<%
end sub

' ========== MixCase ==========
function MixCase(strInput)
	Cse = 1
	for i = 1 to len(strInput)
		thischar = mid(strInput,i,1)
		if Cse = 1 then StrOut = StrOut & ucase(thischar)
		if Cse = 0 then StrOut = StrOut & lcase(thischar)
		select case thischar
			case " ", ",", "-", "/", ".", """", "(" ' Whitespace characters
				Cse = 1
			case else
				Cse = 0
		end select
	next
	MixCase = StrOut
end function

' ========== SpaceOut ==========
function SpaceOut(strInput)
	for i = 1 to len(strInput) - 1
		spaceout = spaceout & mid(strInput,i,1) & "&nbsp;"
	next
	spaceout = spaceout & right(strInput,1)
end function

' ========== stlSmallCaps ==========
function SmallCaps(strInput,basesize)
	StrOut = fontx(basesize,basesize,cLight)
	for i = 1 to len(strInput)
		thischar = mid(strInput,i,1)
		if asc(thischar) >= asc("A") and asc(thischar) <= asc("Z") then
			StrOut = StrOut & "<FONT SIZE=" & (basesize + 1) & ">" & ucase(thischar) & "</FONT>"
		else
			StrOut = StrOut & ucase(thischar)
		end if
		' add spaceout (optional)
		' StrOut = StrOut + "<FONT SIZE=1>&nbsp;</FONT>"
	next
	StrOut = StrOut & "</FONT>"
	SmallCaps = StrOut
end function

' ========== stlRule ==========
function stlRule()
	%>
	<HR SIZE=1 COLOR="<%= cRule %>">
	<%
end function 

' ========== Smallrule ==========
function stlSmallRule()
	%>
	<HR SIZE=1 COLOR="<%= cRule %>" WIDTH=280>
	<%
end function 

' ================================
Sub stlBeginTableFrame(byVal strWidth)
	if strWidth = "" then
		strWidth = "100%"
	end if
%>
<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 WIDTH="<%= strWidth %>" BGCOLOR="<%= iif(blnPrintMode, cBlack, cFormBorder) %>">
<TR>
	<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cFormBG %>">
	<TR>
		<TD><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%">
<%
End Sub

' ==============
Sub stlEndTableFrame
%>
		</TABLE></TD>
	</TR>
	</TABLE></TD>
</TR>
</TABLE>
<%
End Sub

' ==============
Sub stlTableRule(strColSpan)
%>
<TR>
	<TD<% if strColSpan <> "1" then %> COLSPAN="<%= strColSpan %>"<% end if %>><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cRule %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
</TR>
<%
End Sub


dim gstlDataRows	' # of rows per block in the datatable
dim gstlDataCurRow	' current row #
dim gstlDataCols	' # of columns in the datatable
dim gstlDataCurCol	' current column #
dim gstlDataColorFlag	' used to alternate row colors
dim gstlDataSize	' font size
dim gstlDataHdrSize	' font size
dim gstlDataAlign	' default alignment
dim gstlDataHdrAlign
dim gstlDataFormatPre	' prefix format
dim gstlDataFormatPost	' post format
dim gstlDataHdrFormatPre	' prefix header format
dim gstlDataHdrFormatPost	' post header format
dim gstlDataHiliteOnce	' once-only hilite color
gstlDataCols = 0
gstlDataRows = 1

' ==============
Sub stlDataHeaderFormat(strAlign, strPre, strPost)
	gstlDataHdrAlign = strAlign
	gstlDataHdrFormatPre = strPre
	gstlDataHdrFormatPost = strPost
End Sub

' ==============
Sub stlDataFormat(strAlign, strPre, strPost)
	gstlDataAlign = strAlign
	gstlDataFormatPre = strPre
	gstlDataFormatPost = strPost
End Sub

' ==============
Sub stlDataHiliteOnce(strColor)
	gstlDataHiliteOnce = strColor
end sub

' ==============
Sub stlBeginDataTable(ByVal strWidth, intColSpan)
	stlBeginTableFrame strWidth
	gstlDataRows = 1
	gstlDataCurRow = 0
	gstlDataCols = intColSpan
	gstlDataCurCol = 1
	gstlDataColorFlag = true
	gstlDataSize = 1
	gstlDataHdrSize = 1
	gstlDataAlign = "left"
	gstlDataHdrAlign = "left"
	gstlDataFormatPre = ""
	gstlDataFormatPost = ""
	gstlDataHdrFormatPre = "<B>"
	gstlDataHdrFormatPost = "</B>"
	gstlDataHiliteOnce = ""
%>
<TABLE BORDER=0 CELLPADDING=5 CELLSPACING=1 WIDTH="100%">
<%
End Sub

' ==============
Sub stlEndDataTable
	if gstlDataCurCol mod gstlDataCols <> 1 then
%>
</TR>
<%
	end if
%>
</TABLE>
<%
	stlEndTableFrame
	gstlDataCols = 0
End Sub

sub stlResetRow
	gstlDataCurRow = 1
	gstlSectionCurCol = 1
end sub

sub stlOpenDataBlock(intColSpan)
	dim strColor
	if gstlDataCurCol mod gstlDataCols = 1 then
		gstlDataCurRow = gstlDataCurRow + 1
		if gstlDataCurRow mod gstlDataRows = 0 then
			gstlDataColorFlag = not gstlDataColorFlag
		end if
		%>
		<TR>
		<%
	end if
	if gstlDataColorFlag then
		strColor = cVVLtGrey
	else
		strColor = cVLtGrey
	end if
	if gstlDataHiliteOnce <> "" then
		strColor = gstlDataHiliteOnce
		gstlDataHiliteOnce = ""
	end if
	%>
	<TD BGCOLOR="<%= strColor %>"<% if gstlDataAlign<>"left" then %> ALIGN="<%= gstlDataAlign %>"<% end if %><% if intColSpan<>1 then %> COLSPAN="<%= intColSpan %>"<% end if %>><%= font(gstlDataSize) %><%= gstlDataFormatPre %>
<%
	gstlDataCurCol = gstlDataCurCol + intColSpan
end sub

sub stlEndDataBlock
%>
<%= gstlDataFormatPost %></TD>
	<%
	if gstlDataCurCol mod gstlDataCols = 1 then
		%>
		</TR>
		<%
	end if
end sub

' ==============
Sub stlDataHeader(ByVal strData, intColSpan)
	if strData = "" then
		strData = "&nbsp;"
	end if
	if gstlDataCurCol mod gstlDataCols = 1 then
		gstlDataCurRow = gstlDataCurRow + 1
		if gstlDataCurRow mod gstlDataRows = 0 then
			gstlDataColorFlag = not gstlDataColorFlag
		end if
		%>
		<TR>
		<%
	end if
	%>
	<TD BGCOLOR="<%= cWhite %>"<% if gstlDataHdrAlign<>"left" then %> ALIGN="<%= gstlDataHdrAlign %>"<% end if %><% if intColSpan<>1 then %> COLSPAN="<%= intColSpan %>"<% end if %>><%= font(1) %><%= gstlDataHdrFormatPre & strData & gstlDataHdrFormatPost %></TD>
	<%
	gstlDataCurCol = gstlDataCurCol + intColSpan
	if gstlDataCurCol mod gstlDataCols = 1 then
		%>
		</TR>
		<%
	end if
end Sub

' ==============
Sub stlData(ByVal strData, intColSpan)
	dim strColor
	if strData = "" then
		strData = "&nbsp;"
	end if
	if gstlDataCurCol mod gstlDataCols = 1 then
		gstlDataCurRow = gstlDataCurRow + 1
		if gstlDataCurRow mod gstlDataRows = 0 then
			gstlDataColorFlag = not gstlDataColorFlag
		end if
		%>
		<TR>
		<%
	end if
	if gstlDataColorFlag then
		strColor = cVVLtGrey
	else
		strColor = cVLtGrey
	end if
	if gstlDataHiliteOnce <> "" then
		strColor = gstlDataHiliteOnce
		gstlDataHiliteOnce = ""
	end if
	%>
	<TD BGCOLOR="<%= strColor %>"<% if gstlDataAlign<>"left" then %> ALIGN="<%= gstlDataAlign %>"<% end if %><% if intColSpan<>1 then %> COLSPAN="<%= intColSpan %>"<% end if %>><%= font(gstlDataSize) %><%= gstlDataFormatPre & strData & gstlDataFormatPost %></TD>
	<%
	gstlDataCurCol = gstlDataCurCol + intColSpan
	if gstlDataCurCol mod gstlDataCols = 1 then
		%>
		</TR>
		<%
	end if
end sub

sub stlBeginFormTable(strWidth)
	stlBeginStdTable strWidth
	stlBeginFormSection strWidth, 2
end sub

sub stlEndFormTable()
	stlEndFormSection
	stlEndStdTable
end sub

sub stlFormCaption(strCaption)
	stlCaption strCaption, 2
end sub

' ==============
Sub stlBeginStdTable(ByVal strWidth)

	if strWidth = "" then
		strWidth = "100%"
	end if
	gstlSectionCurCol = 1
	gstlSectionCols = 2
	stlBeginTableFrame strWidth
	'<TABLE BORDER=0 CELLPADDING=4 CELLSPACING=0 WIDTH=" strWidth ">

End Sub

' ==============
Sub stlCaption(strCaption, ByVal strColSpan)
	' strColSpan = number of columns to span, default = 2
	if gstlDataCols > 0 then
		strColSpan = CStr(gstlDataCols)
	elseif strColSpan = "" or strColSpan = 0 then
		strColSpan = "2"
	end if
%>
<TR>
	<TD<% if strColSpan <> "1" then %> COLSPAN="<%= strColSpan %>"<% end if %>><%= font(1) %><%= strCaption %></TD>
</TR>
<%
End Sub

sub stlCheckFormError()
	if FormErrors.count > 0 then
		stlFormError "There are " & FormErrors.count & " errors below:", 0
	end if
end sub

function stlGetErrorStr(strFieldName)
	if FormErrors.Exists(strFieldName) then
		stlGetErrorStr = FormErrors(strFieldName)
	else
		stlGetErrorStr = ""
	end if
end function

sub stlCheckError(strFieldName)
	if FormErrors.Exists(strFieldName) then
		stlFormError FormErrors(strFieldName), 0
	end if
end sub

sub stlCheckErrorX(strFieldName, strColSpan)
	if FormErrors.Exists(strFieldName) then
		stlFormError FormErrors(strFieldName), strColSpan
	end if
end sub

' ==============
sub stlFormError(strErr, ByVal strColSpan)
	' strColSpan = number of columns to span, default = 2
	if gstlDataCols > 0 then
		strColSpan = CStr(gstlDataCols)
	elseif strColSpan = "" or strColSpan = 0 then
		strColSpan = "2"
	end if
%>
<TR>
	<TD<% if strColSpan<>"1" then %> COLSPAN="<%= strColSpan %>"<% end if %> ALIGN="center"><%= fontx(1,1,cRed) %><B><%= strErr %></B></TD>
</TR>
<%
end sub

' ==============
Sub stlEndStdTable
	'</TABLE>
	stlEndTableFrame
End Sub

' ==============
Function FindHotKey(ByRef strCaption)
	' if strCaption contains a hotkey marker (a character preceded by a backslash)
	' then return the character, and change the caption so that the backslash is
	' removed and underline tags surround the hotkey
	dim c, x
	x = len(strCaption)
	c = InStr(strCaption, "\")
	if c > 0 and c < x then
		FindHotKey = mid(strCaption, c + 1, 1)
		strCaption = left(strCaption, c - 1) & "<U>" & FindHotKey & "</U>" & mid(strCaption, c + 2)
	else
		FindHotKey = ""
	end if
End Function

' ==============
Sub stlTextInput(strFieldName, intSize, intMax, rsSource, ByVal strCaption, strCheck, strError)
	stlTextInputX strFieldName, intSize, intMax, rsSource, strCaption, strCheck, strError, "", 1
end sub

Sub stlTextInputX(strFieldName, intSize, intMax, rsSource, ByVal strCaption, strCheck, strError, strSuffixCaption, intColSpan)
	dim strHotKey, s, strValue, strErr
	
	if IsObject(rsSource) then
		strValue = rsSource(strFieldName) & ""
	else
		strValue = rsSource & ""
	end if
	
	if strCaption <> "" and strCaption <> "-" then
		strCaption = strCaption & ":"
		if strCheck <> "" and lcase(left(strCheck,3)) <> "opt" then
			strCaption = "<B>" & strCaption & "</B>"
		end if
	end if
	
	strHotKey = FindHotKey(strCaption)
	if strHotKey <> "" then
		strHotKey = " ACCESSKEY=""" & strHotKey & """"
	end if
	
	strErr = stlGetErrorStr(strFieldName)
	s = GetAutoCheckStr(strFieldName, strCheck, strError)
	s = s & "<INPUT TYPE=""text"" NAME=""" & strFieldName & """ SIZE=""" & intSize & """ MAXLENGTH=""" & intMax & """ VALUE=""" & Server.HTMLEncode(strValue) & """" & strHotKey & ">"
	s = s & strSuffixCaption
	if strErr <> "" then
		s = fontx(1,1,cErrorTxt) & "<B>" & strErr & "</B></FONT><BR>" & s
		if strCaption <> "-" then
			stlRawCellX strCaption, cLabelBG, "", "", 1
		end if
		stlRawCellX s, cErrorBG, "", "", intColSpan
	else
		if strCaption <> "-" then
			stlRawCellX strCaption, cLabelBG, "", "", 1
		end if
		stlRawCellX s, "", "", "", intColSpan
	end if
End Sub

Sub stlPasswordInput(strFieldName, intSize, intMax, rsSource, ByVal strCaption, strCheck, strError)
	dim strHotKey, s, strValue, strErr
	
	if IsObject(rsSource) then
		strValue = rsSource(strFieldName) & ""
	else
		strValue = rsSource
	end if
	
	if strCaption <> "" and strCaption <> "-" then
		strCaption = strCaption & ":"
		if strCheck <> "" and lcase(left(strCheck,3)) <> "opt" then
			strCaption = "<B>" & strCaption & "</B>"
		end if
	end if
	
	strHotKey = FindHotKey(strCaption)
	if strHotKey <> "" then
		strHotKey = " ACCESSKEY=""" & strHotKey & """"
	end if
	
	strErr = stlGetErrorStr(strFieldName)
	s = GetAutoCheckStr(strFieldName, strCheck, strError)
	s = s & "<INPUT TYPE=""password"" NAME=""" & strFieldName & """ SIZE=""" & intSize & """ MAXLENGTH=""" & intMax & """ VALUE=""" & Server.HTMLEncode(strValue) & """" & strHotKey & ">"
	if strErr <> "" then
		s = "<FONT COLOR=""" & cErrorTxt & """><B>" & strErr & "</B></FONT><BR>" & s
		if strCaption <> "-" then
			stlRawCellX strCaption, cLabelBG, "", "", 1
		end if
		stlRawCellX s, cErrorBG, "", "", 1
	else
		stlRawCellX strCaption, cLabelBG, "", "", 1
		stlRawCell s
	end if
End Sub

Sub stlCheckbox(strFieldName, strMarkValue, rsSource, ByVal strCaptionLeft, strCaptionRight)
	dim strHotKey, s, strValue, strErr
	
	if strCaptionLeft <> "" then
		strCaptionLeft = strCaptionLeft & ":"
	end if
	
	if IsObject(rsSource) then
		strValue = rsSource(strFieldName) & ""
	else
		strValue = rsSource
	end if
	
	strHotKey = FindHotKey(strCaptionLeft)
	if strHotKey = "" then
		strHotKey = FindHotKey(strCaptionRight)
	end if
	if strHotKey <> "" then
		strHotKey = " ACCESSKEY=""" & strHotKey & """"
	end if
	
	strErr = stlGetErrorStr(strFieldName)
	s = "<INPUT TYPE=""checkbox"" NAME=""" & strFieldName & """ VALUE=""" & Server.HTMLEncode(strMarkValue) & """" & strHotKey
	if strMarkValue = strValue then
		s = s & " CHECKED"
	end if
	s = s & ">" & strCaptionRight
	if strErr <> "" then
		s = "<FONT COLOR=""" & cErrorTxt & """><B>" & strErr & "</B></FONT><BR>" & s
		stlRawCellX strCaptionLeft, cErrorBG, "", "top", 1
		stlRawCellX s, cErrorBG, "", "top", 1
	else
		stlRawCellX strCaptionLeft, "", "", "top", 1
		stlRawCellX s, "", "", "top", 1
	end if
End Sub

Sub stlRadioButton(strFieldName, byVal strMarkValue, rsSource, ByVal strCaptionLeft, strCaptionRight)
	dim strHotKey, s, strValue, strErr
	
	strMarkValue = strMarkValue & ""
	
	if strCaptionLeft <> "" and strCaptionleft <> "-" then
		strCaptionLeft = strCaptionLeft & ":"
	end if
	
	if IsObject(rsSource) then
		strValue = rsSource(strFieldName) & ""
	else
		strValue = rsSource & ""
	end if
	
	strHotKey = FindHotKey(strCaptionLeft)
	if strHotKey = "" then
		strHotKey = FindHotKey(strCaptionRight)
	end if
	if strHotKey <> "" then
		strHotKey = " ACCESSKEY=""" & strHotKey & """"
	end if
	
	strErr = stlGetErrorStr(strFieldName)
	s = "<INPUT TYPE=""radio"" NAME=""" & strFieldName & """ VALUE=""" & Server.HTMLEncode(strMarkValue) & """" & strHotKey
	if strMarkValue = strValue then
		s = s & " CHECKED"
	end if
	s = s & ">" & strCaptionRight
	if strErr <> "" then
		s = "<FONT COLOR=""" & cErrorTxt & """><B>" & strErr & "</B></FONT><BR>" & s
		if strCaptionLeft <> "-" then
			stlRawCellX strCaptionLeft, cErrorBG, "", "top", 1
		end if
		stlRawCellX s, cErrorBG, "", "top", 1
	else
		if strCaptionLeft <> "-" then
			stlRawCellX strCaptionLeft, "", "", "top", 1
		end if
		stlRawCellX s, "", "", "top", 1
	end if
End Sub

Sub stlRadioGroup(strFieldName, byVal strMarkValue, rsSource, ByVal strCaptionLeft)
	dim strHotKey, s, strValue, strErr, strCaptionRight
	dim dctGroup
	set dctGroup = Server.CreateObject("Scripting.Dictionary")
	
	if IsObject(rsSource) then
		strValue = rsSource(strFieldName) & ""
	else
		strValue = rsSource
	end if
	
	dim c, x, c2, s2
	x = 1
	c = InStr(strMarkValue, "|")
	while x > 0
		s = mid(strMarkValue, x, c - x)
		c = c + 1
		c2 = InStr(c, strMarkValue, "|")
		if c2 > 0 then
			s2 = mid(strMarkValue, c, c2 - c)
			x = c2 + 1
			c = InStr(c2 + 1, strMarkValue, "|")
		else
			s2 = mid(strMarkValue, c)
			x = 0
		end if
		dctGroup.Add s, s2
	wend
	
	s = ""
	dim i, k
	x = dctGroup.count - 1
	i = dctGroup.items
	k = dctGroup.keys
	for c = 0 to x
		strMarkValue = k(c)
		strHotKey = FindHotKey(strCaptionLeft)
		if strHotKey = "" then
			strHotKey = FindHotKey(strCaptionRight)
		end if
		if strHotKey <> "" then
			strHotKey = " ACCESSKEY=""" & strHotKey & """"
		end if
		
		strErr = stlGetErrorStr(strFieldName)
		if c > 0 then
			s = s & "<BR>"
		end if
		s = s & "<INPUT TYPE=""radio"" NAME=""" & strFieldName & """ VALUE=""" & Server.HTMLEncode(strMarkValue) & """" & strHotKey
		if strMarkValue = strValue then
			s = s & " CHECKED"
		end if
		s = s & ">" & i(c)
	next
	
	if strErr <> "" then
		s = "<FONT COLOR=""" & cErrorTxt & """><B>" & strErr & "</B></FONT><BR>" & s
		stlRawCellX strCaptionLeft & ":", cErrorBG, "", "top", 1
		stlRawCellX s, cErrorBG, "", "top", 1
	else
		stlRawCellX strCaptionLeft & ":", "", "", "top", 1
		stlRawCellX s, "", "", "top", 1
	end if
End Sub

' ==============

Sub stlArrayPulldown(strFieldName, strFirstOption, rstDefault, dctContent, ByVal strCaption, strCheck, strError)
	stlArrayPulldownX strFieldName, strFirstOption, rstDefault, dctContent, strCaption, strCheck, strError, "", 1
end sub

Sub stlArrayPulldownX(strFieldName, strFirstOption, rsSource, dctContent, ByVal strCaption, strCheck, strError, strRightCaption, intColSpan)
	dim strHotKey, s, strValue, strErr
	
	if IsObject(rsSource) then
		strValue = rsSource(strFieldName) & ""
	else
		strValue = rsSource
	end if
	
	if strCaption <> "" and strCaption <> "-" then
		strCaption = strCaption & ":"
	end if
	
	strHotKey = FindHotKey(strCaption)
	if strHotKey <> "" then
		strHotKey = " ACCESSKEY=""" & strHotKey & """"
	end if
	
	strErr = stlGetErrorStr(strFieldName)
	s = GetAutoCheckStr(strFieldName, strCheck, strError)
	s = s & PulldownFromArray(strFieldName, strFirstOption, strValue, dctContent, strHotKey)
	s = s & strRightCaption
	if strErr <> "" then
		s = "<FONT COLOR=""" & cErrorTxt & """><B>" & strErr & "</B></FONT><BR>" & s
		if strCaption <> "-" then
			stlRawCellX strCaption, cLabelBG, "", "", 1
		end if
		stlRawCellX s, cErrorBG, "", "", intColSpan
	else
		if strCaption <> "-" then
			stlRawCellX strCaption, cLabelBG, "", "", 1
		end if
		stlRawCellX s, "", "", "", intColSpan
	end if
End Sub

function PulldownFromArray(strFieldName, strFirstOption, strDefault, dctContent, strHotKey)
	dim strResult, s, c, i, k, x
	strResult = "<SELECT NAME=""" & strFieldName & """ " & strHotKey & ">"
	if strFirstOption <> "" then
		strResult = strResult & "<OPTION VALUE="""">" & strFirstOption & "</OPTION>"
	end if
	if IsNull(strDefault) then
		s = ""
	else
		s = CStr(strDefault)
	end if
	x = dctContent.count - 1
	i = dctContent.items
	k = dctContent.keys
	for c = 0 To x
		strResult = strResult & "<OPTION VALUE=""" & Server.HTMLEncode(k(c)) & """"
		if s = k(c) & "" then
			strResult = strResult & " SELECTED"
			s = ""
		end if
		strResult = strResult & ">" & Server.HTMLEncode(i(c))
	next
	if s <> "" then
		' current selection does not exist in the dictionary object
		' display in the pulldown as: ?? VALUE ??
		strResult = strResult & "<OPTION VALUE=""" & Server.HTMLEncode(s) & """ SELECTED>?? " & Server.HTMLEncode(s) & " ??</OPTION>"
	end if
	strResult = strResult & "</SELECT>"
	PulldownFromArray = strResult
end function

' ==============
Sub stlTextArea(strFieldName, intRows, intCols, strValue, ByVal strCaption)
	dim strHotKey
	
	strHotKey = FindHotKey(strCaption)
	if strHotKey <> "" then
		strHotKey = "ACCESSKEY=""" & strHotKey & """"
	end if
	stlCheckError strFieldName
%>
<TR>
	<TD ALIGN=RIGHT VALIGN=TOP>&nbsp;<% if strCaption <> "" then %><%= font(1) %><%= strCaption %>:<% end if %></TD>
	<TD VALIGN=TOP><TEXTAREA NAME="<%= strFieldName %>" ROWS="<%= intRows %>" COLS="<%= intCols %>" <%= strHotKey %>><%= Server.HTMLEncode(strValue) %></TEXTAREA></TD>
</TR>
<%
End Sub

sub stlRawSubmit(strName, strCaption)
	%>
	<INPUT TYPE="submit" CLASS="button" NAME="<%= strName %>" VALUE="<%= Server.HTMLEncode(strCaption) %>">
	<%
end sub

' ==============
Sub stlSubmit(strCaption)
%>
<TR BGCOLOR="<%= cSubmitBG %>">
	<TD ALIGN=CENTER><% stlRawSubmit "submit", strCaption %></TD>
</TR>
<%
End Sub

Sub stlSubmitImage(strImage, intWidth, intLength)
%>
<TR BGCOLOR="<%= cSubmitBG %>">
	<TD ALIGN=CENTER><INPUT TYPE="image" SRC="<%= imagebase & strImage %>" BORDER=0></TD>
</TR>
<%
End Sub

sub stlFormSubmit(strSubmitCaption, strResetCaption)
%>
<TR BGCOLOR="<%= cSubmitBG %>">
	<TD COLSPAN=2 ALIGN=CENTER><% stlRawSubmit "submit", strSubmitCaption %></TD>
</TR>
<%
end sub

sub stlRawCell(s)
	stlRawCellX s, "", "", "", 1
end sub

sub stlRawCellX(s, strColor, strAlign, strVAlign, intColSpan)
	if gstlSectionCurCol mod gstlSectionCols = 1 then
		response.write "<TR>"
	end if
	response.write "<TD"
	if strColor <> "" then
		response.write " BGCOLOR=""" & strColor & """"
	end if
	if strAlign <> "" then
		response.write " ALIGN=""" & strAlign & """"
	end if
	if strVAlign <> "" then
		response.write " VALIGN=""" & strVAlign & """"
	end if
	if intColSpan <> 1 then
		response.write " COLSPAN=""" & intColSpan & """"
	end if
	response.write ">" & font(1) & s & "</TD>" & vbCRLF
	gstlSectionCurCol = gstlSectionCurCol + intColSpan
	if gstlSectionCurCol mod gstlSectionCols = 1 then
		response.write "</TR>"
		gstlSectionCurCol = 1
	end if
end sub

dim gstl_dctFormButtons, gstlSectionCurCol, gstlSectionCols, gstlSectionOpen
gstlSectionOpen = false

sub stlBeginFormSection(strWidth, intColSpan)
	gstlSectionOpen = true
	gstlSectionCurCol = 1
	gstlSectionCols = intColSpan
	response.write "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=""" & strWidth & """>" & vbCRLF
end sub

sub stlEndFormSection()
	if gstlSectionCurCol mod gstlSectionCols <> 1 then
		response.write "</TR>" & vbCRLF
	end if
	response.write "</TABLE>" & vbCRLF
	gstlSectionOpen = false
end sub

sub stlAddFormHdrButton(byVal strLabel, byVal strURL, strConfirm)
	if not IsObject(gstl_dctFormButtons) then
		set gstl_dctFormButtons = Server.CreateObject("Scripting.Dictionary")
	end if
	strURL = "<A HREF=""" & strURL & """"
	if strConfirm <> "" then
		strURL = strURL & " onClick=""return confirm('" & strConfirm & "');"""
	end if
	strURL = strURL & ">[" & strLabel & "]</A>"
	gstl_dctFormButtons.Add strLabel, strURL
end sub

sub stlFormRecHeader(strRecInfo, dtmCreated, dtmUpdated)
	dim s, c, x, i
	stlBeginFormSection "100%", 2
	if dtmCreated <> "" then
		s = "Created: " & dtmCreated & "&nbsp;&nbsp;&nbsp;Updated: " & dtmUpdated & "<BR>"
	end if
	if strRecInfo <> "" then
		s = s & "Record ID: " & strRecInfo
	end if
	stlRawCellX s, "", "left", "top", 1
	s = ""
	if IsObject(gstl_dctFormButtons) then
		x = gstl_dctFormButtons.count - 1
		i = gstl_dctFormButtons.items
		for c = 0 to x
			s = s & "<BR>" & i(c)
		next
		stlRawCellX mid(s,5), "", "right", "top", 1
	end if
	stlEndFormSection
end sub

sub AddPageAction(strLabel, strLink, strConfirm)
	if not IsObject(dctPageActions) then
		set dctPageActions = Server.CreateObject("Scripting.Dictionary")
	end if
	dctPageActions.Add dctPageActions.count, strLabel & "~" & strLink & "~" & strConfirm
end sub

sub DrawFormHeader(byVal strWidth, strTitle, strInfoText)
	if strWidth = "" then
		strWidth = "100%"
	end if
	response.write "<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=""" & strWidth & """>" & vbCRLF
	response.write "<TR>" & vbCRLF
	response.write "<TD>" & font(5) & strTitle & "</FONT></TD>" & vbCRLF
	response.write "<TD ALIGN=RIGHT>"
	if IsObject(dctPageActions) then
		dim c, x, i, k, strLabel, strLink, strConfirm, a, b
		x = dctPageActions.count - 1
		i = dctPageActions.items
		k = dctPageActions.keys
		for c = 0 to x
			if c > 0 then
				response.write " | "
			end if
			strLabel = i(c)
			a = InStr(strLabel, "~")
			b = InStr(a + 1, strLabel, "~")
			strLink = mid(strLabel, a + 1, b - a - 1)
			strConfirm = mid(strLabel, b + 1)
			strLabel = left(strLabel, a - 1)
			response.write "<A HREF=""" & strLink & """"
			if strConfirm <> "" then
				response.write " onClick=""return confirm('" & strConfirm & "');"""
			end if
			response.write ">" & strLabel & "</A>"
		next
	end if
	response.write "</TD>" & vbCRLF
	response.write "</TR>" & vbCRLF
	response.write "</TABLE>" & vbCRLF
	response.write spacer(1,5) & "<BR>"
	response.write "<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH=""" & strWidth & """ BGCOLOR=""" & cRed & """>" & vbCRLF
	response.write "<TR>" & vbCRLF
	response.write "<TD>" & fontx(1,1,cWhite) & "&nbsp;" & strInfoText & "</TD>" & vbCRLF
	response.write "</TR>" & vbCRLF
	response.write "</TABLE>" & vbCRLF
	response.write spacer(1,5) & "<BR>"
end sub

sub stlStaticText(strA, strB)
	stlRawCell strA
	stlRawCell strB
end sub
%>
