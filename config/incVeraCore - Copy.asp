<%
Dim TestMode

if lcase(strServerHost) = "awsdev" Or InStr(lcase(strServerHost), "clients") > 0 then
    TestMode = true
else
    TestMode = false
end if

CONST STR_USERNAME = "hbouser"
CONST STR_PASSWORD = "2ujA@WaYx9Yq"

Function SoapHeader
SoapHeader =""
SoapHeader = SoapHeader & "<soap:Header>" & vbNewLine
SoapHeader = SoapHeader & "    <AuthenticationHeader xmlns=""http://sma-promail/"">" & vbNewLine
SoapHeader = SoapHeader & "      <Username>" & STR_USERNAME & "</Username>" & vbNewLine
SoapHeader = SoapHeader & "      <Password>" & STR_PASSWORD & "</Password>" & vbNewLine
SoapHeader = SoapHeader & "    </AuthenticationHeader>" & vbNewLine
SoapHeader = SoapHeader & "  </soap:Header>" & vbNewLine

End Function

Function GetXml(strBodyXML)

GetXml = ""
GetXml = GetXml & "<?xml version=""1.0"" encoding=""utf-8""?>" &vbNewLine
GetXml = GetXml & "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" &vbNewLine
GetXml = GetXml & SoapHeader

GetXml = GetXml & "<soap:Body>" &vbNewLine

GetXml = GetXml & strBodyXML

GetXml = GetXml & "</soap:Body>" &vbNewLine
GetXml = GetXml & "</soap:Envelope>" &vbNewLine

End Function


Function AddOrderXml(intOrderID)
Dim strXML, rsOrder, rsLineItems,rsShopper, rsShipShopper, rsBillShopper, mnyShipAmount

strSQL = "SELECT * FROM " & STR_TABLE_ORDER & " WHERE intID=" & CLNG(intOrderID) & ""
set rsOrder = gobjConn.execute(strSQL)

If not rsOrder.Eof Then ' there is an order

'strSQL = "SELECT * FROM " & STR_TABLE_LINEITEM_LONG_TERM & " WHERE chrStatus <> 'D' and intOrderID=" & CLNG(intOrderID) & ""
'set rsLineItems = gobjConn.execute(strSQL)
'
'strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & rsOrder("intShopperId") & ""
'set rsShopper = gobjConn.execute(strSQL)
'
'strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & rsOrder("intShipShopperId") & ""
'set rsShipShopper = gobjConn.execute(strSQL)
'
'strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & rsOrder("intBillShopperId") & ""
'set rsBillShopper = gobjConn.execute(strSQL)
'
'mnyShipAmount = rsOrder("mnyShipAmount")&""
'
'if mnyShipAmount&""<>"" and IsNumeric(mnyShipAmount) Then
'    mnyShipAmount=CDBL(mnyShipAmount)
'else
'    mnyShipAmount = 0.0
'end if

Dim nYear, nMonth, nDay, nHour, nMinute, nSecond

nYear = Year(Now)
nMonth = Month(Now)
nDay = Day(Now)
nHour = Hour(Now)
nMinute = Minute(Now)
nSecond = Second(Now)

if nMonth<10 then nMonth = "0" & nMonth&""
if nDay<10 then nDay = "0" & nDay&""
if nHour<10 then nHour = "0" & nHour&""
if nMinute<10 then nMinute = "0" & nMinute&""
if nSecond<10 then nSecond = "0" & nSecond&""

strXml = strXML & "<AddOrder xmlns=""http://sma-promail/"">" & vbNewLine
strXml = strXML & "	      <order>" & vbNewLine
strXml = strXML & "        <Header>" & vbNewLine
strXml = strXML & "          <ID>" & intOrderID & "</ID>" & vbNewLine
strXml = strXML & "          <EntryDate>" & nYear & "-" & nMonth & "-" & nDay & "T" & nHour & ":" & nMinute & ":" & nSecond & "</EntryDate>" & vbNewLine
strXml = strXML & "          <InsertDate>" & nYear & "-" & nMonth & "-" & nDay & "T" & nHour & ":" & nMinute & ":" & nSecond & "</InsertDate>" & vbNewLine
strXml = strXML & "          <Comments></Comments>" & vbNewLine
strXml = strXML & "        </Header>" & vbNewLine
strXml = strXML & "        <Money>" & vbNewLine
strXml = strXML & "            <ShippingHandlingCharge>0</ShippingHandlingCharge>" & vbNewLine
strXml = strXML & "        </Money>" & vbNewLine
strXml = strXML & "        <Payment></Payment>" & vbNewLine
strXml = strXML & "        <Shipping>" & vbNewLine
strXml = strXML & "            <FreightCode></FreightCode>" & vbNewLine
strXml = strXML & "        </Shipping>" & vbNewLine
strXml = strXML & "        <OrderedBy>" & vbNewLine
strXml = strXML & "          <FirstName>Housekeeping</FirstName>" & vbNewLine
strXml = strXML & "          <MiddleInitial></MiddleInitial>" & vbNewLine
strXml = strXML & "          <LastName></LastName>" & vbNewLine
strXml = strXML & "          <Address1>" & Trim(rsOrder("vchAddress1")) & "</Address1>" & vbNewLine
strXml = strXML & "          <Address2>" & Trim(rsOrder("vchAddress2")&"") & "</Address2>" & vbNewLine
strXml = strXML & "          <City>" & Trim(rsOrder("vchCity")) & "</City>" & vbNewLine
strXml = strXML & "          <State>" & Trim(rsOrder("vchState")) & "</State>" & vbNewLine
strXml = strXML & "          <PostalCode>" & Trim(rsOrder("vchZip")) & "</PostalCode>" & vbNewLine
strXml = strXML & "          <Email></Email>" & vbNewLine
strXml = strXML & "          <CompanyName>" & Trim(rsOrder("vchCompany")) & "</CompanyName>" & vbNewLine
strXml = strXML & "          <UID>" & rsOrder("intID") & "</UID>" & vbNewLine
strXml = strXML & "          <TaxExempt>false</TaxExempt>" & vbNewLine
strXml = strXML & "          <TaxExemptApproved>false</TaxExemptApproved>" & vbNewLine
strXml = strXML & "          <Commercial>false</Commercial>" & vbNewLine
strXml = strXML & "        </OrderedBy>" & vbNewLine
strXml = strXML & "        <ShipTo>" & vbNewLine
strXml = strXML & "          <OrderShipTo>" & vbNewLine
strXml = strXML & "             <FirstName>Housekeeping</FirstName>" & vbNewLine
strXml = strXML & "             <MiddleInitial></MiddleInitial>" & vbNewLine
strXml = strXML & "             <LastName></LastName>" & vbNewLine
strXml = strXML & "             <Address1>" & Trim(rsOrder("vchAddress1")) & "</Address1>" & vbNewLine
strXml = strXML & "             <City>" & Trim(rsOrder("vchCity")) & "</City>" & vbNewLine
strXml = strXML & "             <State>" & Trim(rsOrder("vchState")) & "</State>" & vbNewLine
strXml = strXML & "             <PostalCode>" & Trim(rsOrder("vchZip")) & "</PostalCode>" & vbNewLine
strXml = strXML & "             <Email></Email>" & vbNewLine
strXml = strXML & "             <CompanyName>" & Trim(rsOrder("vchCompany")) & "</CompanyName>" & vbNewLine
strXml = strXML & "             <TaxExempt>false</TaxExempt>" & vbNewLine
strXml = strXML & "             <TaxExemptApproved>false</TaxExemptApproved>" & vbNewLine
strXml = strXML & "             <Commercial>false</Commercial>" & vbNewLine
strXml = strXML & "             <Key>" & rsOrder("intID") & "</Key>" & vbNewLine
strXml = strXML & "          	<ThirdPartyType>1</ThirdPartyType>" & vbNewLine
strXml = strXML & "             <ThirdPartyAccountNumber>91X2X6</ThirdPartyAccountNumber>" & vbNewLine
strXml = strXML & "          </OrderShipTo>" & vbNewLine
strXml = strXML & "        </ShipTo>" & vbNewLine
strXml = strXML & "        <BillTo>" & vbNewLine
strXml = strXML & "          <FirstName>Housekeeping</FirstName>" & vbNewLine
strXml = strXML & "          <LastName></LastName>" & vbNewLine
strXml = strXML & "          <Address1>" & Trim(rsOrder("vchAddress1")) & "</Address1>" & vbNewLine
strXml = strXML & "          <City>" & Trim(rsOrder("vchCity")) & "</City>" & vbNewLine
strXml = strXML & "          <State>" & Trim(rsOrder("vchState")) & "</State>" & vbNewLine
strXml = strXML & "          <PostalCode>" & Trim(rsOrder("vchZip")) & "</PostalCode>" & vbNewLine
strXml = strXML & "          <Email></Email>" & vbNewLine
strXml = strXML & "          <CompanyName>" & Trim(rsOrder("vchCompany")) & "</CompanyName>" & vbNewLine
strXml = strXML & "          <TaxExempt>false</TaxExempt>" & vbNewLine
strXml = strXML & "          <TaxExemptApproved>false</TaxExemptApproved>" & vbNewLine
strXml = strXML & "          <Commercial>false</Commercial>" & vbNewLine
strXml = strXML & "        </BillTo>" & vbNewLine
strXml = strXML & "        <Offers>" & vbNewLine
strXml = strXML & "          <OfferOrdered>" & vbNewLine
strXml = strXML & "            <Offer>" & vbNewLine
strXml = strXML & "              <Header>" & vbNewLine
strXml = strXML & "                <ID>" & Trim(rsOrder("vchPartNumber")) & "</ID>" & vbNewLine
strXml = strXML & "              </Header>" & vbNewLine
strXml = strXML & "            </Offer>" & vbNewLine
strXml = strXML & "            <Quantity>" & rsOrder("intQuantity") & "</Quantity>" & vbNewLine
strXml = strXML & "            <OrderShipToKey>" & vbNewLine
strXml = strXML & "             <Key>" & rsOrder("intID") & "</Key>" & vbNewLine
strXml = strXML & "            </OrderShipToKey>" & vbNewLine
strXml = strXML & "            <PriceType>0</PriceType>" & vbNewLine
strXml = strXML & "            <UnitPrice>0</UnitPrice>" & vbNewLine
strXml = strXML & "            <ShipType>0</ShipType>" & vbNewLine
strXml = strXML & "            <ShippingHandling>0</ShippingHandling>" & vbNewLine
strXml = strXML & "            <Discounts>0</Discounts>" & vbNewLine
strXml = strXML & "            <LineNumber>" & rsOrder("intInvID") & "</LineNumber>" & vbNewLine
strXml = strXML & "            <ProductDetails>" & vbNewLine
strXml = strXML & "              <OrderProductDetail>" & vbNewLine
strXml = strXML & "                 <ClusterPartNumber>" & Trim(rsOrder("intInvID")) & "</ClusterPartNumber>" & vbNewLine
strXml = strXML & "                 <PartNumber>" & Trim(rsOrder("vchPartNumber")) & "</PartNumber>" & vbNewLine
strXml = strXML & "                 <Quantity>" & Trim(rsOrder("intQuantity")) & "</Quantity>" & vbNewLine
strXml = strXML & "              </OrderProductDetail>" & vbNewLine
strXml = strXML & "            </ProductDetails>" & vbNewLine
strXml = strXML & "          </OfferOrdered>" & vbNewLine
strXml = strXML & "        </Offers>" & vbNewLine
strXml = strXML & "      </order>" & vbNewLine
strXml = strXML & "	 </AddOrder>" & vbNewLine

End If ' there is an order

AddOrderXml = GetXml(strXml)

End Function


Function GetProductXml(strID)

Dim strXml

strXml = ""
strXml = strXML & "<GetOffers xmlns=""http://sma-promail/"">" & vbNewLine
strXml = strXML & "<sortGroups/>" & vbNewLine
strXml = strXML & "  <categoryGroupDescription/>" & vbNewLine
strXml = strXML & "  <customCategories/>" & vbNewLine
strXml = strXML & "  <mailerUID/>" & vbNewLine
strXml = strXML & "  <searchString>" & strID & "</searchString>" & vbNewLine
strXml = strXML & "  <searchID>true</searchID>" & vbNewLine
strXml = strXML & "  <searchDescription>false</searchDescription>" & vbNewLine
strXml = strXML & "  <priceClassDescription/>" & vbNewLine
strXml = strXML & "</GetOffers>" & vbNewLine


GetProductXml = GetXml(strXml)
End Function

Function NewGetProductXml(strID)

Dim strXml

strXml = ""
strXml = strXML & "<GetProduct xmlns=""http://sma-promail/"">" & vbNewLine
strXml = strXML & "  <OwnerID>HBO</OwnerID>" & vbNewLine
strXml = strXML & "  <ProductID>" & strID & "</ProductID>" & vbNewLine
strXml = strXML & "</GetProduct>" & vbNewLine


NewGetProductXml = GetXml(strXml)
End Function

Function NewAddProductXml(strPartName,strPartNumber,intQty,intBundleAmt,intReorderAmt,strHeight,strWidth,strlength,strWeight,strComments)

Dim strXml

strXml = ""
strXml = strXML & "<AddProduct xmlns=""http://sma-promail/"">" & vbNewLine
strXml = strXML & "	<product>" & vbNewLine
strXml = strXML & "		<Valuation>" & vbNewLine
strXml = strXML & "			<Valued>false</Valued>" & vbNewLine
strXml = strXML & "			<ReceiptValution>Manual</ReceiptValution>" & vbNewLine
strXml = strXML & "		</Valuation>" & vbNewLine
strXml = strXML & "		<Acquisition>" & vbNewLine
strXml = strXML & "			<AcquisitionType>Unknown</AcquisitionType>" & vbNewLine
strXml = strXML & "		</Acquisition>" & vbNewLine
strXml = strXML & "		<VersionTrack>" & vbNewLine
strXml = strXML & "			<VersionTrack>false</VersionTrack>" & vbNewLine
strXml = strXML & "		</VersionTrack>" & vbNewLine
strXml = strXML & "		<WarehouseSystems>" & vbNewLine
strXml = strXML & "			<ProductWMSSystem>" & vbNewLine
strXml = strXML & "				<System>" & vbNewLine
strXml = strXML & "					<ID>DOMEWM</ID>" & vbNewLine
strXml = strXML & "					<OverrideAllowHistoricalConnection>false</OverrideAllowHistoricalConnection>" & vbNewLine
strXml = strXML & "				</System>" & vbNewLine
if intReorderAmt <> "" then
strXml = strXML & "				<ReorderPoint>" & Trim(intReorderAmt) & "</ReorderPoint>" & vbNewLine
end if
strXml = strXML & "				<CountFrequency>OnDemand</CountFrequency>" & vbNewLine
strXml = strXML & "				<Reserved>0</Reserved>" & vbNewLine
strXml = strXML & "				<Active>true</Active>" & vbNewLine
strXml = strXML & "				<Deleted>false</Deleted>" & vbNewLine
strXml = strXML & "				<IsDefault>true</IsDefault>" & vbNewLine
strXml = strXML & "			</ProductWMSSystem>" & vbNewLine
strXml = strXML & "		</WarehouseSystems>" & vbNewLine
strXml = strXML & "		<SerialNumber>" & vbNewLine
strXml = strXML & "			<SerialNumbers>NoSerialNumbers</SerialNumbers>" & vbNewLine
strXml = strXML & "		</SerialNumber>" & vbNewLine
strXml = strXML & "		<OptionalInfo>" & vbNewLine
strXml = strXML & "			<DefaultPriceType>Each</DefaultPriceType>" & vbNewLine
strXml = strXML & "			<ReturnTreatment>CaseByCase</ReturnTreatment>" & vbNewLine
strXml = strXML & "		</OptionalInfo>" & vbNewLine
strXml = strXML & "		<BillFactors>" & vbNewLine
strXml = strXML & "			<ProductBillFactor>" & vbNewLine
strXml = strXML & "				<FromQuantity>1</FromQuantity>" & vbNewLine
strXml = strXML & "				<BillFactor>1</BillFactor>" & vbNewLine
strXml = strXML & "			</ProductBillFactor>" & vbNewLine
strXml = strXML & "		</BillFactors>" & vbNewLine
strXml = strXML & "		<Header>" & vbNewLine
strXml = strXML & "			<PartNumber>" & Trim(strPartNumber) & "</PartNumber>" & vbNewLine
strXml = strXML & "			<Description>" & Trim(strPartName) & " - " & Trim(intBundleAmt) & "</Description>" & vbNewLine
strXml = strXML & "			<Comments>" & strComments & "</Comments>" & vbNewLine
strXml = strXML & "			<BuildType>Product</BuildType>" & vbNewLine
strXml = strXML & "			<UsageCode>ExclusiveToOwner</UsageCode>" & vbNewLine
strXml = strXML & "			<Owner>" & vbNewLine
strXml = strXML & "				<ID>HBO</ID>" & vbNewLine
strXml = strXML & "			</Owner>" & vbNewLine
strXml = strXML & "			<CostCenter>" & vbNewLine
strXml = strXML & "				<Removable>false</Removable>" & vbNewLine
strXml = strXML & "			</CostCenter>" & vbNewLine
strXml = strXML & "		</Header>" & vbNewLine
strXml = strXML & "		<Sort>" & vbNewLine
strXml = strXML & "			<ProductType>" & vbNewLine
strXml = strXML & "				<Description>Regular</Description>" & vbNewLine
strXml = strXML & "			</ProductType>" & vbNewLine
strXml = strXML & "		</Sort>" & vbNewLine
strXml = strXML & "		<Characteristics>" & vbNewLine
if strHeight <> "" then
strXml = strXML & "			<Height>" & strHeight & "</Height>" & vbNewLine
end if
if strWidth <> "" then
strXml = strXML & "			<Width>" & strWidth & "</Width>" & vbNewLine
end if
if strLength <> "" then
strXml = strXML & "			<Length>" & strLength & "</Length>" & vbNewLine
end if
strXml = strXML & "			<DefaultWeightType>Oz</DefaultWeightType>" & vbNewLine
strXml = strXML & "			<PrePack>false</PrePack>" & vbNewLine
strXml = strXML & "			<PackTrack>Each</PackTrack>" & vbNewLine
strXml = strXML & "			<ShipSeparatePackages>false</ShipSeparatePackages>" & vbNewLine
strXml = strXML & "			<ImageLocal>true</ImageLocal>" & vbNewLine
strXml = strXML & "		</Characteristics>" & vbNewLine
strXml = strXML & "		<DefaultVersion>" & vbNewLine
if strWeight <> "" then
strXml = strXML & "			<Weight>" & strWeight & "</Weight>" & vbNewLine
end if
strXml = strXML & "			<WeightType>Oz</WeightType>" & vbNewLine
strXml = strXML & "			<Status>" & vbNewLine
strXml = strXML & "				<Description>OK TO USE</Description>" & vbNewLine
strXml = strXML & "			</Status>" & vbNewLine
strXml = strXML & "			<HasWarehouseTransactions>false</HasWarehouseTransactions>" & vbNewLine
strXml = strXML & "		</DefaultVersion>" & vbNewLine
strXml = strXML & "		<Activation>" & vbNewLine
strXml = strXML & "			<ProductActivation>" & vbNewLine
strXml = strXML & "				<Active>true</Active>" & vbNewLine
strXml = strXML & "			</ProductActivation>" & vbNewLine
strXml = strXML & "		</Activation>" & vbNewLine
strXml = strXML & "		<OnOrder>" & vbNewLine
strXml = strXML & "			<OnOrder>" & vbNewLine
strXml = strXML & "				<DateRecorded>2014-10-08T17:16:41</DateRecorded>" & vbNewLine
strXml = strXML & "				<PriceType>Each</PriceType>" & vbNewLine
strXml = strXML & "				<Quantity>" & Clng(intQty) & "</Quantity>" & vbNewLine
strXml = strXML & "				<UnitPrice>0.00</UnitPrice>" & vbNewLine
strXml = strXML & "				<ExtendedPrice>0.00</ExtendedPrice>" & vbNewLine
strXml = strXML & "				<QuantityReceived>0</QuantityReceived>" & vbNewLine
strXml = strXML & "				<ToDelete>false</ToDelete>" & vbNewLine
strXml = strXML & "			</OnOrder>" & vbNewLine
strXml = strXML & "		</OnOrder>" & vbNewLine
strXml = strXML & "	</product>" & vbNewLine
strXml = strXML & "	<offer>" & vbNewLine
strXml = strXML & "		<Header>" & vbNewLine
strXml = strXML & "			<ID>" & Trim(strPartNumber) & "</ID>" & vbNewLine
strXml = strXML & "			<Description>" & Trim(strPartName) & " - " & Trim(intBundleAmt) & "</Description>" & vbNewLine
strXml = strXML & "		</Header>" & vbNewLine
strXml = strXML & "		<Settings>" & vbNewLine
strXml = strXML & "			<UnitOfMeasure>" & vbNewLine
strXml = strXML & "				<ID>EA</ID>" & vbNewLine
strXml = strXML & "			</UnitOfMeasure>" & vbNewLine
strXml = strXML & "		</Settings>" & vbNewLine
strXml = strXML & "		<Components>" & vbNewLine
strXml = strXML & "			<OfferComponent>" & vbNewLine
strXml = strXML & "				<Product>" & vbNewLine
strXml = strXML & "					<Header>" & vbNewLine
strXml = strXML & "						<PartNumber>" & Trim(strPartNumber) & "</PartNumber>" & vbNewLine
strXml = strXML & "						<Description>" & Trim(strPartName) & " - " & Trim(intBundleAmt) & "</Description>" & vbNewLine
strXml = strXML & "						<Owner>" & vbNewLine
strXml = strXML & "							<ID>7</ID>" & vbNewLine
strXml = strXML & "						</Owner>" & vbNewLine
strXml = strXML & "					</Header>" & vbNewLine
strXml = strXML & "					<Sort>" & vbNewLine
strXml = strXML & "						<ProductType>" & vbNewLine
strXml = strXML & "							<Description>Regular</Description>" & vbNewLine
strXml = strXML & "						</ProductType>" & vbNewLine
strXml = strXML & "					</Sort>" & vbNewLine
strXml = strXML & "				</Product>" & vbNewLine
strXml = strXML & "				<Quantity>1</Quantity>" & vbNewLine
strXml = strXML & "				<IsPrimaryRevenue>true</IsPrimaryRevenue>" & vbNewLine
strXml = strXML & "			</OfferComponent>" & vbNewLine
strXml = strXML & "		</Components>" & vbNewLine
strXml = strXML & "	</offer>" & vbNewLine
strXml = strXML & "</AddProduct>" & vbNewLine

NewAddProductXml = GetXml(strXml)
End Function

Function AddProductXml(strPartNumber,intQty)

Dim strXml

strXml = ""
strXml = strXML & "<AddProduct xmlns=""http://sma-promail/"">" & vbNewLine
strXml = strXML & "	<product>" & vbNewLine
strXml = strXML & "		<Valuation>" & vbNewLine
strXml = strXML & "			<Valued>false</Valued>" & vbNewLine
strXml = strXML & "			<ReceiptValution>Manual</ReceiptValution>" & vbNewLine
strXml = strXML & "		</Valuation>" & vbNewLine
strXml = strXML & "		<Acquisition>" & vbNewLine
strXml = strXML & "			<AcquisitionType>Unknown</AcquisitionType>" & vbNewLine
strXml = strXML & "		</Acquisition>" & vbNewLine
strXml = strXML & "		<VersionTrack>" & vbNewLine
strXml = strXML & "			<VersionTrack>false</VersionTrack>" & vbNewLine
strXml = strXML & "		</VersionTrack>" & vbNewLine
strXml = strXML & "		<WarehouseSystems>" & vbNewLine
strXml = strXML & "			<ProductWMSSystem>" & vbNewLine
strXml = strXML & "				<System>" & vbNewLine
strXml = strXML & "					<ID>DOMEWM</ID>" & vbNewLine
strXml = strXML & "					<OverrideAllowHistoricalConnection>false</OverrideAllowHistoricalConnection>" & vbNewLine
strXml = strXML & "				</System>" & vbNewLine
strXml = strXML & "				<CountFrequency>OnDemand</CountFrequency>" & vbNewLine
strXml = strXML & "				<Reserved>0</Reserved>" & vbNewLine
strXml = strXML & "				<Active>true</Active>" & vbNewLine
strXml = strXML & "				<Deleted>false</Deleted>" & vbNewLine
strXml = strXML & "				<IsDefault>true</IsDefault>" & vbNewLine
strXml = strXML & "			</ProductWMSSystem>" & vbNewLine
strXml = strXML & "		</WarehouseSystems>" & vbNewLine
strXml = strXML & "		<SerialNumber>" & vbNewLine
strXml = strXML & "			<SerialNumbers>NoSerialNumbers</SerialNumbers>" & vbNewLine
strXml = strXML & "		</SerialNumber>" & vbNewLine
strXml = strXML & "		<OptionalInfo>" & vbNewLine
strXml = strXML & "			<DefaultPriceType>Each</DefaultPriceType>" & vbNewLine
strXml = strXML & "			<ReturnTreatment>CaseByCase</ReturnTreatment>" & vbNewLine
strXml = strXML & "		</OptionalInfo>" & vbNewLine
strXml = strXML & "		<BillFactors>" & vbNewLine
strXml = strXML & "			<ProductBillFactor>" & vbNewLine
strXml = strXML & "				<FromQuantity>1</FromQuantity>" & vbNewLine
strXml = strXML & "				<BillFactor>1</BillFactor>" & vbNewLine
strXml = strXML & "			</ProductBillFactor>" & vbNewLine
strXml = strXML & "		</BillFactors>" & vbNewLine
strXml = strXML & "		<Header>" & vbNewLine
strXml = strXML & "			<PartNumber>" & Trim(strPartNumber) & "</PartNumber>" & vbNewLine
strXml = strXML & "			<Description>" & Trim(strPartNumber) & "</Description>" & vbNewLine
strXml = strXML & "			<BuildType>Product</BuildType>" & vbNewLine
strXml = strXML & "			<OfferFlag>true</OfferFlag>" & vbNewLine
strXml = strXML & "			<UsageCode>ExclusiveToOwner</UsageCode>" & vbNewLine
strXml = strXML & "			<Owner>" & vbNewLine
strXml = strXML & "				<ID>HBO</ID>" & vbNewLine
strXml = strXML & "			</Owner>" & vbNewLine
strXml = strXML & "			<CostCenter>" & vbNewLine
strXml = strXML & "				<Removable>false</Removable>" & vbNewLine
strXml = strXML & "			</CostCenter>" & vbNewLine
strXml = strXML & "		</Header>" & vbNewLine
strXml = strXML & "		<Sort>" & vbNewLine
strXml = strXML & "			<ProductType>" & vbNewLine
strXml = strXML & "				<Description>Regular</Description>" & vbNewLine
strXml = strXML & "			</ProductType>" & vbNewLine
strXml = strXML & "		</Sort>" & vbNewLine
strXml = strXML & "		<Characteristics>" & vbNewLine
strXml = strXML & "			<DefaultWeightType>Oz</DefaultWeightType>" & vbNewLine
strXml = strXML & "			<PrePack>false</PrePack>" & vbNewLine
strXml = strXML & "			<PackTrack>Each</PackTrack>" & vbNewLine
strXml = strXML & "			<ShipSeparatePackages>false</ShipSeparatePackages>" & vbNewLine
strXml = strXML & "			<ImageLocal>true</ImageLocal>" & vbNewLine
strXml = strXML & "		</Characteristics>" & vbNewLine
strXml = strXML & "		<DefaultVersion>" & vbNewLine
strXml = strXML & "			<WeightType>Oz</WeightType>" & vbNewLine
strXml = strXML & "			<Status>" & vbNewLine
strXml = strXML & "				<Description>OK TO USE</Description>" & vbNewLine
strXml = strXML & "			</Status>" & vbNewLine
strXml = strXML & "			<HasWarehouseTransactions>false</HasWarehouseTransactions>" & vbNewLine
strXml = strXML & "		</DefaultVersion>" & vbNewLine
strXml = strXML & "		<Activation>" & vbNewLine
strXml = strXML & "			<ProductActivation>" & vbNewLine
strXml = strXML & "				<Active>true</Active>" & vbNewLine
strXml = strXML & "			</ProductActivation>" & vbNewLine
strXml = strXML & "		</Activation>" & vbNewLine
strXml = strXML & "		<OnOrder>" & vbNewLine
strXml = strXML & "			<OnOrder>" & vbNewLine
strXml = strXML & "				<DateRecorded>2014-10-08T17:16:41</DateRecorded>" & vbNewLine
strXml = strXML & "				<PriceType>Each</PriceType>" & vbNewLine
strXml = strXML & "				<Quantity>" & Clng(intQty) & "</Quantity>" & vbNewLine
strXml = strXML & "				<UnitPrice>0.00</UnitPrice>" & vbNewLine
strXml = strXML & "				<ExtendedPrice>0.00</ExtendedPrice>" & vbNewLine
strXml = strXML & "				<QuantityReceived>0</QuantityReceived>" & vbNewLine
strXml = strXML & "				<ToDelete>false</ToDelete>" & vbNewLine
strXml = strXML & "			</OnOrder>" & vbNewLine
strXml = strXML & "		</OnOrder>" & vbNewLine
strXml = strXML & "	</product>" & vbNewLine
strXml = strXML & "</AddProduct>" & vbNewLine

AddProductXml = GetXml(strXml)
End Function



Function AddPODProductXml(strPartNumber,strPartName,intQty)

Dim strXml

strXml = ""
strXml = strXML & "<AddProduct xmlns=""http://sma-promail/"">" & vbNewLine
strXml = strXML & "	<product>" & vbNewLine
strXml = strXML & "		<Valuation>" & vbNewLine
strXml = strXML & "			<Valued>false</Valued>" & vbNewLine
strXml = strXML & "			<ReceiptValution>Manual</ReceiptValution>" & vbNewLine
strXml = strXML & "		</Valuation>" & vbNewLine
strXml = strXML & "		<Acquisition>" & vbNewLine
strXml = strXML & "			<AcquisitionType>Unknown</AcquisitionType>" & vbNewLine
strXml = strXML & "		</Acquisition>" & vbNewLine
strXml = strXML & "		<VersionTrack>" & vbNewLine
strXml = strXML & "			<VersionTrack>false</VersionTrack>" & vbNewLine
strXml = strXML & "		</VersionTrack>" & vbNewLine
strXml = strXML & "		<WarehouseSystems>" & vbNewLine
strXml = strXML & "			<ProductWMSSystem>" & vbNewLine
strXml = strXML & "				<System>" & vbNewLine
strXml = strXML & "					<ID>DOMEWM</ID>" & vbNewLine
strXml = strXML & "					<OverrideAllowHistoricalConnection>false</OverrideAllowHistoricalConnection>" & vbNewLine
strXml = strXML & "				</System>" & vbNewLine
strXml = strXML & "				<CountFrequency>OnDemand</CountFrequency>" & vbNewLine
strXml = strXML & "				<Reserved>0</Reserved>" & vbNewLine
strXml = strXML & "				<Active>true</Active>" & vbNewLine
strXml = strXML & "				<Deleted>false</Deleted>" & vbNewLine
strXml = strXML & "				<IsDefault>true</IsDefault>" & vbNewLine
strXml = strXML & "			</ProductWMSSystem>" & vbNewLine
strXml = strXML & "		</WarehouseSystems>" & vbNewLine
strXml = strXML & "		<SerialNumber>" & vbNewLine
strXml = strXML & "			<SerialNumbers>NoSerialNumbers</SerialNumbers>" & vbNewLine
strXml = strXML & "		</SerialNumber>" & vbNewLine
strXml = strXML & "		<OptionalInfo>" & vbNewLine
strXml = strXML & "			<DefaultPriceType>Each</DefaultPriceType>" & vbNewLine
strXml = strXML & "			<ReturnTreatment>CaseByCase</ReturnTreatment>" & vbNewLine
strXml = strXML & "		</OptionalInfo>" & vbNewLine
strXml = strXML & "		<BillFactors>" & vbNewLine
strXml = strXML & "			<ProductBillFactor>" & vbNewLine
strXml = strXML & "				<FromQuantity>1</FromQuantity>" & vbNewLine
strXml = strXML & "				<BillFactor>1</BillFactor>" & vbNewLine
strXml = strXML & "			</ProductBillFactor>" & vbNewLine
strXml = strXML & "		</BillFactors>" & vbNewLine
strXml = strXML & "		<Header>" & vbNewLine
strXml = strXML & "			<PartNumber>" & Trim(strPartNumber) & "</PartNumber>" & vbNewLine
strXml = strXML & "			<Description>" & Trim(strPartName) & "</Description>" & vbNewLine
strXml = strXML & "			<LeadDays>3</LeadDays>" & vbNewLine
strXml = strXML & "			<BuildType>MOD</BuildType>" & vbNewLine
strXml = strXML & "			<OfferFlag>true</OfferFlag>" & vbNewLine
strXml = strXML & "			<UsageCode>ExclusiveToOwner</UsageCode>" & vbNewLine
strXml = strXML & "			<Owner>" & vbNewLine
strXml = strXML & "				<ID>HBO</ID>" & vbNewLine
strXml = strXML & "			</Owner>" & vbNewLine
strXml = strXML & "			<CostCenter>" & vbNewLine
strXml = strXML & "				<Removable>false</Removable>" & vbNewLine
strXml = strXML & "			</CostCenter>" & vbNewLine
strXml = strXML & "		</Header>" & vbNewLine
strXml = strXML & "		<Sort>" & vbNewLine
strXml = strXML & "			<ProductType>" & vbNewLine
strXml = strXML & "				<Description>Regular</Description>" & vbNewLine
strXml = strXML & "			</ProductType>" & vbNewLine
strXml = strXML & "		</Sort>" & vbNewLine
strXml = strXML & "		<Characteristics>" & vbNewLine
strXml = strXML & "			<DefaultWeightType>Oz</DefaultWeightType>" & vbNewLine
strXml = strXML & "			<PrePack>false</PrePack>" & vbNewLine
strXml = strXML & "			<PackTrack>Each</PackTrack>" & vbNewLine
strXml = strXML & "			<ShipSeparatePackages>false</ShipSeparatePackages>" & vbNewLine
strXml = strXML & "			<ImageLocal>true</ImageLocal>" & vbNewLine
strXml = strXML & "		</Characteristics>" & vbNewLine
strXml = strXML & "		<DefaultVersion>" & vbNewLine
strXml = strXML & "			<WeightType>Oz</WeightType>" & vbNewLine
strXml = strXML & "			<Status>" & vbNewLine
strXml = strXML & "				<Description>OK TO USE</Description>" & vbNewLine
strXml = strXML & "			</Status>" & vbNewLine
strXml = strXML & "			<HasWarehouseTransactions>false</HasWarehouseTransactions>" & vbNewLine
strXml = strXML & "		</DefaultVersion>" & vbNewLine
strXml = strXML & "		<Activation>" & vbNewLine
strXml = strXML & "			<ProductActivation>" & vbNewLine
strXml = strXML & "				<Active>true</Active>" & vbNewLine
strXml = strXML & "			</ProductActivation>" & vbNewLine
strXml = strXML & "		</Activation>" & vbNewLine
strXml = strXML & "		<OnOrder>" & vbNewLine
strXml = strXML & "			<OnOrder>" & vbNewLine
strXml = strXML & "				<DateRecorded>2014-10-08T17:16:41</DateRecorded>" & vbNewLine
strXml = strXML & "				<PriceType>Each</PriceType>" & vbNewLine
strXml = strXML & "				<Quantity>" & Clng(intQty) & "</Quantity>" & vbNewLine
strXml = strXML & "				<UnitPrice>0.00</UnitPrice>" & vbNewLine
strXml = strXML & "				<ExtendedPrice>0.00</ExtendedPrice>" & vbNewLine
strXml = strXML & "				<QuantityReceived>0</QuantityReceived>" & vbNewLine
strXml = strXML & "				<ToDelete>false</ToDelete>" & vbNewLine
strXml = strXML & "			</OnOrder>" & vbNewLine
strXml = strXML & "		</OnOrder>" & vbNewLine
strXml = strXML & "	</product>" & vbNewLine

strXml = strXML & "	<offer>" & vbNewLine
strXml = strXML & "		<Header>" & vbNewLine
strXml = strXML & "			<ID>" & Trim(strPartNumber) & "</ID>" & vbNewLine
strXml = strXML & "			<Description>" & Trim(strPartName) & "</Description>" & vbNewLine
strXml = strXML & "		</Header>" & vbNewLine
strXml = strXML & "		<Settings>" & vbNewLine
strXml = strXML & "			<UnitOfMeasure>" & vbNewLine
strXml = strXML & "				<ID>EA</ID>" & vbNewLine
strXml = strXML & "			</UnitOfMeasure>" & vbNewLine
strXml = strXML & "		</Settings>" & vbNewLine
strXml = strXML & "		<Components>" & vbNewLine
strXml = strXML & "			<OfferComponent>" & vbNewLine
strXml = strXML & "				<Product>" & vbNewLine
strXml = strXML & "					<Header>" & vbNewLine
strXml = strXML & "						<PartNumber>" & Trim(strPartNumber) & "</PartNumber>" & vbNewLine
strXml = strXML & "						<Description>" & Trim(strPartName) & "</Description>" & vbNewLine
strXml = strXML & "						<Owner>" & vbNewLine
strXml = strXML & "							<CompanyName>HBO</CompanyName>" & vbNewLine
strXml = strXML & "						</Owner>" & vbNewLine
strXml = strXML & "					</Header>" & vbNewLine
strXml = strXML & "					<Sort>" & vbNewLine
strXml = strXML & "						<ProductType>" & vbNewLine
strXml = strXML & "							<Description>Regular</Description>" & vbNewLine
strXml = strXML & "						</ProductType>" & vbNewLine
strXml = strXML & "					</Sort>" & vbNewLine
strXml = strXML & "				</Product>" & vbNewLine
strXml = strXML & "				<Quantity>1</Quantity>" & vbNewLine
strXml = strXML & "				<IsPrimaryRevenue>true</IsPrimaryRevenue>" & vbNewLine
strXml = strXML & "			</OfferComponent>" & vbNewLine
strXml = strXML & "		</Components>" & vbNewLine
strXml = strXML & "	</offer>" & vbNewLine

strXml = strXML & "</AddProduct>" & vbNewLine

AddPODProductXml = GetXml(strXml)
End Function


Function AddPODSaveOfferXml(strPartNumber,strPartName,intQty)

Dim strXml

strXml = ""
strXml = strXML & "<SaveOffer xmlns=""http://sma-promail/"">" & vbNewLine
strXml = strXML & "	<offer>" & vbNewLine
strXml = strXML & "		<Header>" & vbNewLine
strXml = strXML & "			<ID>" & Trim(strPartNumber) & "</ID>" & vbNewLine
strXml = strXML & "			<Description>" & Trim(strPartName) & "</Description>" & vbNewLine
strXml = strXML & "		</Header>" & vbNewLine
strXml = strXML & "		<Info>" & vbNewLine
strXml = strXML & "			<BillOfMaterials>CustomAssembly</BillOfMaterials>" & vbNewLine
strXml = strXML & "		</Info>" & vbNewLine
strXml = strXML & "		<Settings>" & vbNewLine
strXml = strXML & "			<UnitOfMeasure>" & vbNewLine
strXml = strXML & "				<ID>EA</ID>" & vbNewLine
strXml = strXML & "			</UnitOfMeasure>" & vbNewLine
strXml = strXML & "		</Settings>" & vbNewLine
strXml = strXML & "		<Components>" & vbNewLine
strXml = strXML & "			<OfferComponent>" & vbNewLine
strXml = strXML & "				<Product>" & vbNewLine
strXml = strXML & "					<Header>" & vbNewLine
strXml = strXML & "						<PartNumber>" & Trim(strPartNumber) & "</PartNumber>" & vbNewLine
strXml = strXML & "						<Description>" & Trim(strPartName) & "</Description>" & vbNewLine
strXml = strXML & "						<Owner>" & vbNewLine
strXml = strXML & "							<CompanyName>HBO</CompanyName>" & vbNewLine
strXml = strXML & "						</Owner>" & vbNewLine
strXml = strXML & "					</Header>" & vbNewLine
strXml = strXML & "					<Sort>" & vbNewLine
strXml = strXML & "						<ProductType>" & vbNewLine
strXml = strXML & "							<Description>Regular</Description>" & vbNewLine
strXml = strXML & "						</ProductType>" & vbNewLine
strXml = strXML & "					</Sort>" & vbNewLine
strXml = strXML & "				</Product>" & vbNewLine
strXml = strXML & "				<Quantity>" & intQty & "</Quantity>" & vbNewLine
strXml = strXML & "				<IsPrimaryRevenue>true</IsPrimaryRevenue>" & vbNewLine
strXml = strXML & "			</OfferComponent>" & vbNewLine
strXml = strXML & "		</Components>" & vbNewLine
strXml = strXML & "	</offer>" & vbNewLine
strXml = strXML & "</SaveOffer>" & vbNewLine

AddPODSaveOfferXml = GetXml(strXml)
End Function


Function GetProductAvailabilitiesXml(strID)

Dim strXml


strXml = ""
strXml = strXML & "<GetOffers xmlns=""http://sma-promail/"">" & vbNewLine
strXml = strXML & "<sortGroups/>" & vbNewLine
strXml = strXML & "  <categoryGroupDescription/>" & vbNewLine
strXml = strXML & "  <customCategories/>" & vbNewLine
strXml = strXML & "  <mailerUID/>" & vbNewLine
strXml = strXML & "  <searchString>" & strID & "</searchString>" & vbNewLine
strXml = strXML & "  <searchID>true</searchID>" & vbNewLine
strXml = strXML & "  <searchDescription>false</searchDescription>" & vbNewLine
strXml = strXML & "  <priceClassDescription/>" & vbNewLine
strXml = strXML & "</GetOffers>" & vbNewLine



GetProductAvailabilitiesXml = GetXml(strXml)
End Function

Function GetOrderXml(intOrderNumber)

Dim strXml

strXml = ""
strXml = strXML & "<GetOrderInfo xmlns=""http://sma-promail/"">" & vbNewLine
strXml = strXML & "<orderId>" & Trim(intOrderNumber) & "</orderId>" & vbNewLine
strXml = strXML & "</GetOrderInfo>" & vbNewLine


GetOrderXml = GetXml(strXml)
End Function

'===============================================================

Function GetOrder(intOrderID)

   
    Dim oXmlHTTP, oXmlResp,oXMlNodes, oXmlProductAvailabilityResult, oXmlOfferResultId, oXmlOfferResultDescription, oXmlOfferResultAvailable, dctReturnValue

    Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
    oXmlHTTP.Open "POST", "http://www.drcinventory.com/pmomsws/order.asmx", False 

    oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
    oXmlHTTP.setRequestHeader "SOAPAction", "http://sma-promail/GetOrderInfo"
    On Error Resume Next

    
    oXmlHTTP.send GetOrderXml(intOrderID)    
    'Response.Write AddOrderXml(intOrderID)  
    'Response.Write oXmlHTTP.responseText
    'Response.End
    
    
    Set oXmlResp = oXmlHTTP.responseXML
    
    set dctReturnValue = Server.CreateObject("Scripting.Dictionary")
    
    Set oXmlNodes = oXmlResp.getElementsByTagName("UploadDate")
    dctReturnValue("UploadDate") = oXmlNodes(0).Text
    Set oXmlNodes = oXmlResp.getElementsByTagName("Unapproved")
    dctReturnValue("Unapproved") = oXmlNodes(0).Text
    Set oXmlNodes = oXmlResp.getElementsByTagName("Unprocessed")
    dctReturnValue("Unprocessed") = oXmlNodes(0).Text
    Set oXmlNodes = oXmlResp.getElementsByTagName("Accepted")
    dctReturnValue("Accepted") = oXmlNodes(0).Text
    Set oXmlNodes = oXmlResp.getElementsByTagName("Hold")
    dctReturnValue("Hold") = oXmlNodes(0).Text
    
    Set oXmlNodes = oXmlResp.getElementsByTagName("DeniedCredit")
    dctReturnValue("DeniedCredit") = oXmlNodes(0).Text
    Set oXmlNodes = oXmlResp.getElementsByTagName("Processed")
    dctReturnValue("Processed") = oXmlNodes(0).Text
    Set oXmlNodes = oXmlResp.getElementsByTagName("Pending")
    dctReturnValue("Pending") = oXmlNodes(0).Text
    Set oXmlNodes = oXmlResp.getElementsByTagName("Picked")
    dctReturnValue("Picked") = oXmlNodes(0).Text
    Set oXmlNodes = oXmlResp.getElementsByTagName("Backordered")
    dctReturnValue("Backordered") = oXmlNodes(0).Text
    Set oXmlNodes = oXmlResp.getElementsByTagName("Shipped")
    dctReturnValue("Shipped") = oXmlNodes(0).Text
    Set oXmlNodes = oXmlResp.getElementsByTagName("Canceled")
    dctReturnValue("Canceled") = oXmlNodes(0).Text
    Set oXmlNodes = oXmlResp.getElementsByTagName("Complete")
    dctReturnValue("Complete") = oXmlNodes(0).Text
    Set oXmlNodes = oXmlResp.getElementsByTagName("TrackingId")
    dctReturnValue("TrackingId") = oXmlNodes(0).Text

    

    If Err.Number Then
    'Response.Write AddOrderXml(intOrderID)  
    '   Response.Write oXmlHTTP.responseText
    '   Response.End
    End If
    On Error Goto 0

    set GetOrder = dctReturnValue
End Function

'===============================================================

Function AddOrder(intOrderID)

    Dim oXmlHTTP, oXmlResp,oXMlNodes, oXmlProductAvailabilityResult, oXmlOfferResultId, oXmlOfferResultDescription, oXmlOfferResultAvailable,FSO, myFile

    Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
    oXmlHTTP.Open "POST", "http://www.drcinventory.com/pmomsws/order.asmx", False 

    oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
    oXmlHTTP.setRequestHeader "SOAPAction", "http://sma-promail/AddOrder"
    On Error Resume Next

    
    oXmlHTTP.send AddOrderXml(intOrderID)  
	set FSO = Server.CreateObject("scripting.FileSystemObject")
	set myFile = FSO.CreateTextFile("C:\\WebLogs\\HBO\\SOAP-History\\" & intOrderID & ".txt", true)

	myFile.WriteLine(AddOrderXml(intOrderID) )
	myFile.WriteLine(oXmlHTTP.responseText)
	myFile.Close
    'Response.Write AddOrderXml(intOrderID)
    'Response.Write oXmlHTTP.responseText
    'Response.End
    AddOrder = true
    If Err.Number Then
       AddOrder = false
    'Response.Write AddOrderXml(intOrderID)
    '   Response.Write oXmlHTTP.responseText
    '   Response.End
    End If
    On Error Goto 0


End Function


'==========================================================


Function AddProduct(strPartNumber,intQty)

Dim oXmlHTTP, oXmlResp,oXMlNodes, oXmlProductAvailabilityResult, oXmlOfferResultId, oXmlOfferResultDescription, oXmlOfferResultAvailable

If TestMode Then
    AddProduct = true
Else
Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
oXmlHTTP.Open "POST", "http://www.drcinventory.com/pmomsws/order.asmx", False 

oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
oXmlHTTP.setRequestHeader "SOAPAction", "http://sma-promail/AddProduct"
On Error Resume Next

'Response.Write AddProductXml(strPartNumber,intQty)    
'Response.End


oXmlHTTP.send AddProductXml(strPartNumber,intQty)    
    
AddProduct = true

If Err.Number Then
   AddProduct = false
End If
On Error Goto 0


End If
End Function

Function GetProductAvailabilites(strProductNumber)

Dim oXmlHTTP, oXmlResp,oXMlNodes, oXmlProductAvailabilityResult, oXmlOfferResultId, oXmlOfferResultDescription, oXmlOfferResultAvailable


Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
oXmlHTTP.Open "POST", "http://www.drcinventory.com/pmomsws/order.asmx", False 

oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
oXmlHTTP.setRequestHeader "SOAPAction", "http://sma-promail/GetOffers"
On Error Resume Next
'response.write GetProductAvailabilitiesXml(Trim(strProductNumber))
oXmlHTTP.send GetProductAvailabilitiesXml(Trim(strProductNumber))    
'Response.Write strProductNumber &"<br />"
'Response.Write oXmlHTTP.responseText


Set oXmlResp = oXmlHTTP.responseXML
Set oXmlNodes = oXmlResp.getElementsByTagName("Available")
GetProductAvailabilites = oXmlNodes(0).Text

If Err.Number Then
   GetProductAvailabilites = 0
  

End If
On Error Goto 0
End Function


Function GetProduct(strProductNumber)

Dim oXmlHTTP, oXmlResp,oXMlNodes, oXmlProductAvailabilityResult, oXmlOfferResultId, oXmlOfferResultDescription, oXmlOfferResultAvailable


Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
oXmlHTTP.Open "POST", "http://www.drcinventory.com/pmomsws/order.asmx", False 

oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
oXmlHTTP.setRequestHeader "SOAPAction", "http://sma-promail/GetProduct"
On Error Resume Next
'response.write GetProductXml(Trim(strProductNumber))
oXmlHTTP.send GetProductXml(Trim(strProductNumber))    
'Response.Write strProductNumber &"<br />"
'Response.Write oXmlHTTP.responseText


Set oXmlResp = oXmlHTTP.responseXML
Set oXmlNodes = oXmlResp.getElementsByTagName("Available")
GetProductAvailabilites = oXmlNodes(0).Text

If Err.Number Then
   GetProductAvailabilites = 0
  

End If
On Error Goto 0
End Function

function NewAddProduct(strPartName,strPartNumber,intBundleAmt,intReorderAmt)

Dim oXmlHTTP, oXmlResp,oXMlNodes, oXmlProductAvailabilityResult, oXmlOfferResultId, oXmlOfferResultDescription, oXmlOfferResultAvailable, strHeight, strWeight, strLength,strWidth, strComments


	Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
	oXmlHTTP.Open "POST", "http://www.drcinventory.com/pmomsws/order.asmx", False 

	oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
	oXmlHTTP.setRequestHeader "SOAPAction", "http://sma-promail/GetProduct"
	'On Error Resume Next
	'response.write NewGetProductXml(Trim(strPartNumber))
	oXmlHTTP.send NewGetProductXml(Trim(strPartNumber))
	'Response.Write oXmlHTTP.responseText
	Set oXmlResp = oXmlHTTP.responseXML
	
	Set oXmlNodes = oXmlResp.selectNodes("//GetProductResult")
	if oXmlNodes.length > 0  then
		Set oXmlNodes = oXmlResp.getElementsByTagName("Height")
		strHeight = oXmlNodes(0).Text
		
		Set oXmlNodes = oXmlResp.getElementsByTagName("Weight")
		strWeight = oXmlNodes(0).Text
		
		Set oXmlNodes = oXmlResp.getElementsByTagName("Length")
		strLength = oXmlNodes(0).Text
		
		Set oXmlNodes = oXmlResp.getElementsByTagName("Width")
		strWidth = oXmlNodes(0).Text
		
		Set oXmlNodes = oXmlResp.getElementsByTagName("Comments")
		if oXmlNodes.length > 0  then
			strComments = oXmlNodes(0).Text
		end if
		
	else
		strHeight = ""
		strWeight = ""
		strLength = ""
		strWidth = ""
		strComments = ""
	end if
'	response.write "strHeight: " & strHeight & " strWeight: " & strWeight & " strLength: " & strLength & " strWidth: " & strWidth & " strComments: " & strComments
	'response.end

Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
oXmlHTTP.Open "POST", "http://www.drcinventory.com/pmomsws/order.asmx", False 

oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
oXmlHTTP.setRequestHeader "SOAPAction", "http://sma-promail/AddProduct"
'On Error Resume Next
'response.write NewAddProductXml("new test product","awstest9002",100,15,500,strHeight,strWidth,strlength,strWeight,strComments)
oXmlHTTP.send NewAddProductXml(strPartName,strPartNumber,0,intBundleAmt,intReorderAmt,strHeight,strWidth,strlength,strWeight,strComments)
'Response.Write oXmlHTTP.responseText
'response.end

End function
%>
