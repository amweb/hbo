﻿<% option explicit %>
<%
Const adOpenKeySet = 1
Const adLockReadOnly = 1
Const adLockPessimistic = 2
Const adLockOptimistic = 3
Const adCmdText = 1
Const adCmdTable = 2
Const adCmdStoredProc = 4
Const adCmdUnknown = 8
Const adStateOpen = 1
Const adStateClosed = 0
Const adExecuteNoRecords = 128
Const adParamInput = 1
Const adParamOutput = 2
Const adParamInputOutput = 3
Const adParamReturnValue = 4
Const adChar = 129
Const adInteger = 3
Const adCurrency = 6
Const adUseNone = 1
Const adUseServer =	2
Const adUseClient = 3
Const adOpenUnspecified	= -1
Const adOpenForwardOnly	= 0
Const adOpenDynamic	= 2
Const adOpenStatic = 3
Const adLockUnspecified	= -1
Const adLockBatchOptimistic	= 4
 %>
<!--#include file="incForm.asp"-->
<!--#include file="incCheck.asp"-->
<!--#include file="incStyle.asp"-->
<!--#include file="incGoCartAPI.asp"-->
<%
' =====================================================================================
' = File: incInit.asp
' = File Version: 5.1 (beta)
' = Copyright (c)1993-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   ASP Init Script
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: removed any client-specific code
' = Description of Customizations:
' =   (none)
' =====================================================================================
dim blnPrintMode

dim gstrHostBase		' {"resdev", "internal", "staging", "live"}
dim virtualbase, aspbase, imagebase, homebase, absbase, userfilebase, g_storefrontbase, g_bannerbase	' special paths
dim securebase, nonsecurebase
dim strServerHost, strScriptName, strScript, gblnSecure		' URL info
dim gstrConnectString		' database info
dim gstrClientCopyright
dim gobjConn, gblnConnOpen
dim gstrUser, gstrUserIP
dim dctLinks, dctSubLinks,currentPage
dim grsInvFolders, gintInvParentUpID	' used for displaying inventory data in DrawHeader()
gintInvParentUpID = -1

dim g_urlbase, g_imagebase, g_strTitle, g_blnShowAllChildren, g_arySpecials, strLocationTitle
g_blnShowAllChildren = false		' used for displaying inventory data in DrawMenuGroup()

const intPopUpActive = "1"
const STR_COMPANY_NAME = "HBO"
const STR_SITE_NAME = ""
const STR_DOMAIN_NAME = "HBO"
const STR_STAGING_DSN = "webdatasql"
const STR_EMAIL_CONTACT = "info@.com"

const STR_SESSION_REFERER_ID = "ref_id"
const STR_SESSION_REFERER_NAME = "site_name"
const STR_SESSION_REFERER_DISCOUNT = "order_discount"

const STR_TABLE_PROMO = "HBO_PromoCodes"

InitDomain()


' -----------------------------------
' =========== DrawHeader ============
' -----------------------------------
Sub DrawHeader(byVal strTitle, strPageType)
	'response.expires = -1	' prevent any client-side or proxy caching of ASP pages
	if strTitle <> "" then
		'strTitle = STR_SITE_NAME & " - " & strTitle
	else
		strTitle = STR_SITE_NAME
	end if
	g_strTitle = strTitle
' ==== common HTML header ====
%>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>HBO | Dome Printing</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/global/plugins/bootstrap-4.3.1/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/global/plugins/bootstrap/css/sticky-footer.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/global/plugins/bootstrap-multiselect/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="<%= virtualbase %>assets/global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/admin/pages/css/dashboard.css" rel="stylesheet" type="text/css">
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<!--link href="<%= virtualbase %>assets/global/css/components.css" rel="stylesheet" type="text/css"/-->
<link href="<%= virtualbase %>assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/admin/layout/css/themes/light2.css" rel="stylesheet" type="text/css" />
<link href="<%= virtualbase %>assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/admin/layout/css/arrow-button.css" rel="stylesheet" type="text/css"/>

<script src="<%= virtualbase %>assets/global/plugins/jquery-3.4.1.min.js" type="text/javascript"></script>
<link href="<%= virtualbase %>drcpod/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>drcpod/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
<script src="<%= virtualbase %>drcpod/js/scripts.js" type="text/javascript"></script>

<link rel="stylesheet" href="<%= virtualbase %>assets/global/plugins/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<link rel="stylesheet" href="<%= virtualbase %>assets/global/plugins/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />

<!-- END THEME STYLES -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="<%= virtualbase %>assets/global/plugins/fancybox/source/jquery.fancybox.js"></script>
<script type="text/javascript" src="<%= virtualbase %>assets/admin/pages/scripts/profile_manager.js"></script>

<link rel="shortcut icon" href="../assets/admin/layout/img/favicon.ico"/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body>
<!-- BEGIN HEADER -->
<% if gintUserName&""<>"" then 
openconn
%>
<!-- BEGIN CONTAINER -->
<div class="container-fluid h-100">
<nav class="navbar navbar-expand-md navbar-static-top shadow-sm border-bottom border-left border-right border-dark">
	<!-- BEGIN HEADER INNER -->
	<a class="navbar-brand" href="<%= virtualbase%>store/dashboard.asp">
		<img src="<%= virtualbase %>images/logo.svg" width="85px" alt="logo"/>
	</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
    	<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse">
		<!-- BEGIN LOGO -->
		<!-- END LOGO -->
		
		<!-- BEGIN TOP NAVIGATION MENU -->
		<ul class="navbar-nav justify-content-end w-100">
			<li class="nav-item">
				<a class="nav-link h6">Welcome <%=gintUserName%><i class="fa fa-user"></i></a>
			</li>
			<li class="nav-item">
				<a class="nav-link h6" href="<%= virtualbase %>default.asp?action=logout"><i class="fa fa-power-off"></i> Logout</a>
			</li>
		</ul>

		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</nav>
<% end if %>
<!-- END HEADER -->
<div class="clearfix">
</div>
<%
end sub

'--------------- DrawFooter ---------------
sub DrawFooter(footertype)
%>
</div>
<!-- END CONTAINER -->	
<!-- BEGIN COPYRIGHT -->
<div class="footer copyright">
	<div class="page-footer">
		&copy; <%= year(Now) %> Dome Solutions. All rights reserved.  Under license to HBO.
	</div>
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<%= virtualbase %>assets/global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<%= virtualbase %>assets/global/plugins/bootstrap-4.3.1/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/vue.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/jquery.blockui.min.js?2" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/jquery.cycle.all.js" type="text/javascript"></script>

<script src="<%= virtualbase %>assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<%= virtualbase %>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%= virtualbase %>assets/global/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<%= virtualbase %>assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/admin/pages/scripts/login.js" type="text/javascript"></script>
<% if instr(Lcase(Request.ServerVariables("URL")), "store/") > 0 then %>
<link rel="stylesheet" href="flexslider.css" type="text/css">
<script src="jquery.flexslider.js"></script>

<%end if %>
<!-- END PAGE LEVEL SCRIPTS -->


<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

<%
End Sub


'--------------- DrawSubPage ---------------
Sub DrawSubPage(strPageType, strSubType)
%>
<%
select case strSubType
	case "topmenu"
	'<a href="%%= nonsecurebase %%default.asp?id=">Specials</a> | <a href="%%= securebase %%store/custserv.asp">Register</a> | <a href="%%= securebase %%store/custservice.asp#login">Log In</a> | <a href="%%= nonsecurebase %%store/cartlist.asp">Shopping Cart</a> | <a href="%%= nonsecurebase %%store/checkout.asp">Checkout</a>
	%>
	<div id="topmenu">
	<form id="frmSearch1" action="<%= nonsecurebase %>store/">
<%
	if not session("isStoreFront") then 
%>		
		<a href="<%= aspbase %>default.asp" target="_top">Home</a> | <a href="<%= nonsecurebase %>store/" target="_top">Products</a> | <a target="_top" href="<%= securebase %>store/custserv.asp<%= iif(gintUserID > 0,"?action=logout"">Log Out",""">" & iif(gintOrderID > 0,"Your Account","Log In")) %></a> | <% if gintUserID > 0 or gintOrderID > 0 then %><a href="<%= securebase %>store/cartlist.asp" target="_top">View Cart</a> | <a href="<%= securebase %>store/checkout.asp" target="_top">Checkout</a> | <% end if %><a href="<%= aspbase %>aboutus.asp" target="_top">About Us</a> | <a href="<%=aspbase%>contact.asp" target="_top">Contact Us</a>
		 | &nbsp;&nbsp;
<%
	else
%>
		<a href="<%= aspbase & session("g_StoreFrontStr")%>" target="_top">Home</a> | <a href="<%= nonsecurebase & session("g_StoreFrontStr")%>" target="_top">Products</a> | <a target="_top" href="<%= securebase %>store/custserv.asp<%= iif(gintUserID > 0,"?action=logout"">Log Out",""">" & iif(gintOrderID > 0,"Your Account","Log In")) %></a> | <% if gintUserID > 0 or gintOrderID > 0 then %><a href="<%= securebase %>store/cartlist.asp" target="_top">View Cart</a> | <a href="<%= securebase %>store/checkout.asp" target="_top">Checkout</a> | <% end if %><a href="<%= aspbase %>aboutus.asp" target="_top">About Us</a> | <a href="<%=aspbase%>contact.asp" target="_top">Contact Us</a>
		 | &nbsp;&nbsp;
<%
	end if
%>
	 <label for="frmSearch1.search">Search &nbsp;</label>
	 <input id="frmSearch1.search" type="text" name="keywords" value="" class="inputskin" />
	 <input type="image" src="<%= g_imagebase %>menu/go.gif" width="27" height="16" border="0" />&nbsp;
	</form>
	</div>
	<% if lcase(g_strTitle) = "home" or lcase(strPageType) = "home" then %>
	
	
	
	<table width="750" height="118" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td>
				<a href="<%= aspbase %>default.asp"><img src="<%= g_imagebase %>banners/banner-lg.jpg" border="0" /></a> 
			</td>
		</tr>
	</table>

	<% else %>
	<% 
		strLocationTitle = Replace(g_strTitle,"&","%26")
		if strLocationTitle = "Checkout Service Center" then
			strLocationTitle = "Customer Service Center"
		elseif strLocationTitle = "Specials!" then
			strLocationTitle = "SPECIALS"
		end if		
		strLocationTitle = Replace(strLocationTitle," ","%20")
	%>
	
<table width="750" height="65" border="0" cellpadding="0" cellspacing="0" >
  <tr>
			<td width="250">
<%				if not session("isStoreFront") then %>				
<br><%				end if %>									
			</td>
			<td width="500" align="Right">
				<a href="<%= nonsecurebase & session("g_StoreFrontStr") %>"><img src="<%= session("g_headbanner") %>" /></a>
			</td>
		</tr>
	</table>


	<% end if %>

	<%
	case "footer"
	%>

	<%
	case "menu"
%>
		<div class="blueborderoutside">
			<div class="blueborderinside">
			<p>Visit These Sections...</p>
<%				
                Custom_GetGlobalInventoryFolders 0
                while not grsInvFolders.eof
				    DrawMenuGroup grsInvFolders
				    grsInvFolders.MoveNext
			    wend
%>

			<div class="menugroup">
<%			if gintUserID > 0 then 
				dim strSQL, rsTemp, strEmail
				strSQL = "SELECT U.vchEmail"
				strSQL = strSQL & " FROM " & STR_TABLE_SHOPPER & " AS U WHERE U.intID=" & gintUserID
				set rsTemp = gobjConn.execute(strSQL)
				if not rsTemp.eof then
					strEmail = Server.URLEncode(rsTemp(0))
				end if
				rsTemp.close
				set rsTemp = nothing
%>
<%					else %>

<%					end if %>
			</div>

					</div>
				</div>
	
	<%
	case "submenu"
	%>
		<% if g_strTitle <> STR_SITE_NAME and false then %>
				<p>You're Currently In...</p>
				<div class="menugroup">
					<a href="" class="menularge"><%= g_strTitle %></a><br />
				</div>
		<% end if %>
	<%
	case "search"
'		<input type="submit" value="Search" class="cleanbtn" />
	%>
				<div class="blueborderoutside">
					<div id="menu" class="blueborderinside">
					<form id="frmSearch" action="<%= nonsecurebase %>store/default.asp">
						<label for="frmSearch.search">Search &nbsp;</label>
						<input id="frmSearch.search" type="text" name="keywords" value="<%= Request("keywords") %>" class="leftinputskin" />
						<input type="image" src="<%= g_imagebase %>menu/go-search.gif" width="27" height="16" hspace="0" vspace="0" border="0">&nbsp;
					</form>
					</div>
				</div>
	<%
	case "specials"
		if not session("isStoreFront") then
			DrawSpecialPromo g_arySpecials
		end if
	case else
	%><%
end select %>

<%
End Sub

'--------------- DrawSpecials ---------------
sub DrawSpecials
	dim strSQL, bannerRS, ProdType, aTag
	dim nCnt, col
	nCnt = 0
	col = 3
	
	strSQL = "SELECT intID, intParentID, chrType, vchItemName, vchImageURL"
	strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY & ""
	strSQL = strSQL & " WHERE chrStatus='A' AND chrSpecial='Y'"
	strSQL = strSQL & " ORDER BY intParentID, vchItemName"
	set bannerRS = gobjConn.execute(strSQL)

if bannerRS.eof then
	bannerRS.close
	set bannerRS = nothing
	%>
	<%
else
	%>
		<div id="specialsHeader">
		Special Promotions
		</div>
	<%
	response.write "<table border=""0"" cellpadding=""0"" cellspacing=""2"">" & vbcrlf
	response.write "<tr>" & vbcrlf

	while not bannerRS.eof
		nCnt = nCnt + 1
		ProdType = bannerRS("chrType")
		select case ProdType
			case "B"
				aTag = "<a href=""store/brandlist.asp?ID=" & bannerRS("intID") & """>"
			case "I"
				aTag = "<a href=""store/itemdetail.asp?ID=" & bannerRS("intID") & """>"
			case "A"
				aTag = "<a href=""store/default.asp?parentID=" & bannerRS("intID") & """>"
			case ""
				aTag = ""
		end select
		response.write  vbtab & "<td class=""specials"">" & aTag & "<img class=""specialsGray"" src=""" & imagebase & "products/small/" & FixFilename(bannerRS("vchImageURL")) & """ border=""0"" alt="""" /><br />" & BannerRS("vchItemName") & "<br /><img class=""specialsMore"" src=""" & g_imagebase & "menu/more-info.gif"" width=""80"" height=""16"" border=""0"" alt="""" />" & "<br /></a>" & "</td>" & vbcrlf

		bannerRS.movenext
		if not bannerRS.eof then
			if (nCnt Mod col) = 0 then
				response.write "</tr>" & vbcrlf 
				response.write "<tr>" & vbcrlf
			end if
		end if
	wend
	response.write "</tr>" & vbcrlf 
	response.write "</table>" & vbcrlf 

	bannerRS.close
	set bannerRS = nothing
end if

end sub

sub DrawSpecialPromo (g_arySpecials)

	dim strSQL, bannerRS, ProdType, aTag
	dim nCnt, col
	nCnt = 0
	col = 3
	
	strSQL = "SELECT intID, intParentID, chrType, vchItemName, vchImageURL"
	strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY & ""
	strSQL = strSQL & " WHERE chrStatus='A' AND chrSpecial='Y'"
	strSQL = strSQL & " ORDER BY intParentID, vchItemName"
	set bannerRS = gobjConn.execute(strSQL)

if bannerRS.eof then
	bannerRS.close
	set bannerRS = nothing
	%>
	<%
else
	g_arySpecials = bannerRS.getrows
	bannerRS.close
	set bannerRS = nothing

	dim numcols, numrows, rowcounter, colcounter, thisfield
	Dim intID, vchItemName, vchImageURL
	numcols=ubound(g_arySpecials,1)
	numrows=ubound(g_arySpecials,2)

	Randomize   ' Initialize random-number generator.
   	rowcounter = Int(((numrows+.5) * rnd ) )   ' Generate random value between 0 and numrows.

	if numrows >= rowcounter then
		colcounter = 0
		intID = g_arySpecials(colcounter,rowcounter)
		colcounter = 3
		vchItemName = g_arySpecials(colcounter,rowcounter)
		colcounter = 4
		vchImageURL = g_arySpecials(colcounter,rowcounter)
	
		%>
				<div class="redborderoutside">
					<div id="promotion" class="redborderinside">
					<p>Special Promotion</p>
					<a href="<%= nonsecurebase %>store/itemdetail.asp?id=<%=intID%>"><img src="<%= imagebase %>products/small/<%= FixFilename(vchImageURL) %>" alt="" border="0"><br /><%= vchItemName %><br /><img class="specialsMore" src="<%= g_imagebase %>menu/more-promos.gif" width="69" height="16" border="0" alt="" /><br /></a>
					</div>
				</div>
		<%
	elseif false then
		' Now lets grab all the records
		dim showblank, shownull
		showblank="&nbsp;"
		shownull="-null-"

		response.write "<table border='1'><tr>" & vbcrlf
		response.write "</tr>" & vbcrlf

		for rowcounter= 0 to numrows
		   response.write "<tr>" & vbcrlf
		   for colcounter=0 to numcols
		      thisfield=g_arySpecials(colcounter,rowcounter)
		      if isnull(thisfield) then
		         thisfield=shownull
		      end if
		      if trim(thisfield)="" then
		         thisfield=showblank
		      end if
		      response.write "<td valign=top>" 
		      response.write thisfield
		      response.write "</td>" & vbcrlf
		   next
		   response.write "</tr>" & vbcrlf
		next
		response.write "</table>" 
	end if
end if

end sub

sub DrawCountDown

	dim swfName, swfWidth, swfHeight, swfBorder, swfBGColor
	swfname = "countdown_v1.8.swf"
	swfWidth = 750
	swfHeight = 22
	swfBorder = 1
	swfBGColor = "#FFFFFF"
	%>
	<div id="counter">
	<object	classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
		codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0"
		width="<%= swfWidth %> VIEWASTEXT VIEWASTEXT>" height="<%= swfHeight %>" id="mainflash">
		<param name="movie" value="<%= swfName %>" />
		<param name="autostart" value="true" />
		<param name="menu" value="false" />
		<param name="quality" value="high" />
		<param name="bgcolor" value="<%= swfBGColor %>" />
		<embed src="<%= swfName %>" width="<%= swfWidth %>" height="<%= swfHeight %>" autostart="true" menu="false" quality="high" bgcolor="<%= swfBGColor %>" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed><br />
	</object>
	</div>
	<%
end sub

' =========== DrawOldHeader ============
Sub DrawOldHeader(byVal strTitle, strPageType)
%>
<center>

<table width="582" border="0" cellspacing="0" cellpadding="0" bgcolor="#CC9933">
	<tr>
		<td width="1"><img src="<%= imagebase %>spacer.gif" width=1 height=1 alt="" border="0"></td>
		<% if strPageType <> "checkout" then %>
		<td width="120" valign="top" bgcolor="#F4E9D2"><table width="120" border="0" cellspacing="0" cellpadding="0">
						<tr>
							<td width="120" colspan="2"><img src="<%= imagebase %>gourmet_categories.gif" width=120 height=42 alt="" border="0"></td>
						</tr>
						<%
						dim strSQL
						strSQL = "SELECT intID, vchItemName FROM TCS_Inv WHERE intParentID = 0 AND chrStatus = 'A' AND chrType = 'A' ORDER BY vchItemName"
						dim rsFolderLinks
						set rsFolderLinks = gobjConn.execute(strSQL)
						
						while not rsFolderLinks.eof
						%>
						<tr>
							<td valign="top"><img src="<%= imagebase %>spacer.gif" width=10 height=11 alt="" border="0"></td>
<!--below is code for navigation bar-->
							<td><font face="arial,hevetica" size=1><a href="<%= nonsecurebase %>store/default.asp?parentid=<%= rsFolderLinks("intID") %>"><%= rsFolderLinks("vchItemName") %></a><BR>
								<img src="<%= imagebase %>spacer.gif" width=10 height=2 alt="" border="0"></td>
						</tr>
						<%
						rsFolderLinks.movenext
						wend
						set rsFolderLinks = nothing
						%>
						<tr>
							<td colspan="2"><br><img src="<%= imagebase %>customer_service.gif" width=120 height=34 alt="" border="0"></td>
						</tr>
						<tr>
							<td></td>
							<td><font face="arial,hevetica" size=1 color="#663300"><a href="<%= aspbase %>store/custservice.asp#guarantee">guarantee</a></td>
						</tr>
						<tr>
							<td></td>
							<td><font face="arial,hevetica" size=1 color="#663300"><a href="<%= aspbase %>store/custservice.asp#how">how to order</a></td>
						</tr>
						<tr>
							<td></td>
							<td><font face="arial,hevetica" size=1 color="#663300"><a href="<%= aspbase %>store/custservice.asp#return">return policy</a></td>
						</tr>
						<tr>
							<td></td>
							<td><font face="arial,hevetica" size=1 color="#663300"><a href="<%= aspbase %>store/custservice.asp#shipping">shipping</a></td>
						</tr>
						<tr>
							<td></td>
							<td><font face="arial,hevetica" size=1 color="#663300"><a href="<%= aspbase %>privacy.asp">privacy & security</a></td>
						</tr>
						<tr>
							<td></td>
							<td><font face="arial,hevetica" size=1 color="#663300"><a href="<%= aspbase %>store/custservice.asp#customerservice">service center</a></td>
						</tr>
						<tr>
							<td colspan="2"><br></td>
						</tr>
					  </table></td>
		<td width="1"><img src="<%= imagebase %>spacer.gif" width=1 height=1 alt="" border="0"></td>
		<% end if %>
		<% if strPageType <> "home" then %>
		<td width="5" bgcolor="#FFFFFF"><img src="<%= imagebase %>spacer.gif" width=1 height=1 alt="" border="0"></td>
		<% end if %>
		<td width=<% select case strPageType
					case "home"	%>
					329
					<% case "checkout" %>
					570
					<% case else %>
					449
					<% end select %>
		 height="100%" vAlign="top" bgColor="#FFFFFF">
<%
	if strPageType <> "home" then
		response.write font(2) & spacer(1,10) & "<BR>"
		stlHeading strTitle
	end if
end sub

' =========== DrawOldFooter ============
Sub DrawOldFooter(footertype)
%>
		<% if footertype <> "home" then %>
		<td width="5" bgcolor="#FFFFFF"><img src="<%= imagebase %>spacer.gif" width=1 height=1 alt="" border="0"></td>
		<% end if %>
		</td>
		<td width="1"><img src="<%= imagebase %>spacer.gif" width=1 height=1 alt="" border="0"></td>	
	</tr>
</table>	
<table width="582" height="1" border="0" cellspacing="0" cellpadding="0" bgcolor="#CC9933">
	<tr>
		<td><img src="<%= imagebase %>spacer.gif" width=1 height=1 alt="" border="0"></td>
	</tr>
</table>
<table width="582" height="1" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td align="center"><font face="arial,hevetica" size=1>The Campaign Store.com, LCC. &nbsp; Experience Really Does Matter.<br><br>
							<font face="arial,hevetica" size=2>
							<a href="<%= nonsecurebase %>store/">shopping</a>&nbsp;&nbsp;
							<a href="<%= nonsecurebase %>staff.asp">staff</a>&nbsp;&nbsp;
							<a href="<%= securebase %>store/custservice.asp#customerservice">customer&nbsp;service</a>&nbsp;&nbsp;
							<a href="<%= nonsecurebase %>contact.asp">contact&nbsp;us</a>&nbsp;&nbsp;
							<a href="<%= nonsecurebase %>default.asp">home</a>
							<br><br>
							<font face="arial,hevetica" size=1 color="#999999">The Campaign Store<sup>SM</sup> 2003 <br>
							<a href="<%= aspbase %>tos.asp">Terms of Service</a> | <a href="<%= aspbase %>privacy.asp">Privacy</a>
		</td>
	</tr>
</table>

</center>

<%
end sub

Sub SubNavBox(pagetype)
%>
<table border="0" cellspacing="0" cellpadding="0" align="right">
	<tr>
		<td><img src="images/spacer.gif" alt="" width="15" height="1" border="0"></td>
		<td><table width="120" border="0" cellspacing="0" cellpadding="1" bgcolor="#CC9933">
		<tr><td><table width="118" border="0" cellspacing="0" cellpadding="10" bgcolor="#F4E9D2">
						<tr>
							<td><%= fontx(1,1,cBlack) %>
								<a href="<%= nonsecurebase %>store/">Shopping</a><br>								
								<img src="images/spacer.gif" width=1 height=4><br>
								<% if pagetype = "staff" then %>
									<font color="#660066"><b>Staff</b></font>
								<% else %>
									<a href="<%= aspbase %>staff.asp">Staff</a>
								<% end if %><br>
								<img src="images/spacer.gif" width=1 height=4><br>
								<a href="<%= aspbase %>store/custservice.asp">Service</a><br>
								<img src="images/spacer.gif" width=1 height=4><br>
								<a href="<%= aspbase %>contact.asp">Contact Us</a><br>
								<img src="images/spacer.gif" width=1 height=4><br>
								<a href="<%= aspbase %>default.asp">Home</a><br>
							</td>
						</tr>
					</table></td>
				</tr>
			</table></td>
	</tr>
</table>
<%
End Sub

set dctLinks = Server.CreateObject("Scripting.Dictionary")
	dctLinks.add "store/default", "Shopping"
'	dctLinks.add "who", "Who We Are"
	dctLinks.add "staff", "Staff"
	dctLinks.add "faqs", "Service"
	dctLinks.add "contact", "Contact&nbsp;Us"
	dctLinks.add "default", "Home"

set dctSubLinks = Server.CreateObject("Scripting.Dictionary")
	' defined in individual asp files

sub InitDomain()
	dim rootfolder
	gstrClientCopyright = "Copyright &copy;2000 " & STR_COMPANY_NAME & ". All rights reserved."
	
	gstrUser = Request.ServerVariables("REMOTE_USER")
	gstrUserIP = Request.ServerVariables("REMOTE_ADDR")
	
	gblnSecure = (Request.ServerVariables("SERVER_PORT_SECURE") = 1)
	strServerHost = Request.ServerVariables("HTTP_HOST")
	strScriptName = Request.ServerVariables("SCRIPT_NAME")
	if gblnSecure then
		strScript = "http://" & strServerHost & strScriptName & "?"
	else
		strScript = "http://" & strServerHost & strScriptName & "?"
	end if
	
	if gblnSecure then
		absbase = "http://"
	else
		absbase = "http://"
	end if


	if lcase(strServerHost) = "awsdev" then
		' running from internal
		rootfolder = "/hbo/www/"
		absbase = absbase & strServerHost & rootfolder
		gstrHostBase = "internal"
		securebase = "http://" & strServerHost & rootfolder
		nonsecurebase = "http://" & strServerHost & rootfolder
		homebase = nonsecurebase
		gstrConnectString = "DSN=webdatasql; UID=webuser; PWD=webdata"
	elseif InStr(lcase(strServerHost), "localhost") > 0 then
		' running from staging
		rootfolder = "/hbo/"
		absbase = absbase & strServerHost & rootfolder
		gstrHostBase = "localhost"
		securebase = "http://" & strServerHost & rootfolder
		nonsecurebase = "http://" & strServerHost & rootfolder
		homebase = nonsecurebase
		gstrConnectString = "DSN=webdatasql; UID=webuser; PWD=welcome"
	elseif lcase(strServerHost) = "staging.drcportal.com" then
		' running from awsdev staging
		rootfolder = "/hbo/"
		absbase = "http://" & strServerHost & "/" & rootfolder
		nonsecurebase = "http://" & strServerHost & "/" & rootfolder
		securebase = nonsecurebase ' secure mode runs under nonsecure
		'absbase = absbase & strServerHost & rootfolder
		gstrHostBase = "staging"
		securebase = "" & rootfolder
		nonsecurebase = "http://" & strServerHost & rootfolder
		homebase = nonsecurebase
		'virtualbase = rootfolder & absbase
		gstrConnectString = "DSN=stagingsql; UID=webuser; PWD=welcome"
	else
		' running from live site
		if Request("dbg") = "" then
			on error resume next
		end if
		rootfolder = "/"
		absbase = absbase & strServerHost & rootfolder
		gstrHostBase = "live"
		securebase = "" & rootfolder
		nonsecurebase = "http://" & strServerHost & rootfolder
		homebase = nonsecurebase
		gstrConnectString = "DSN=webdatasql; UID=webuser; PWD=welcome"
		'gstrConnectString = "DSN=testODBC; UID=webuser2; PWD=welcome"
	end if
	
	dim tmpPos
	tmpPos = instr(lcase(strScriptName),rootfolder) + len(rootfolder)
	while instr(tmpPos, lcase(strScriptName), "/")
		virtualbase = virtualbase & "../"
		tmpPos = instr(tmpPos, lcase(strScriptName), "/") + 1
	wend
	
	aspbase = virtualbase
	imagebase = virtualbase & "images/"
	userfilebase = virtualbase & "GoDocUserFiles/"
	g_imagebase = virtualbase & "_images/"
	g_bannerbase = g_imagebase & "banners/"
	g_urlbase = aspbase
	g_storefrontbase = virtualbase & "storefront/"
	
	if (session("g_headbannerimg") = "" or session("g_headbannerimg") = "banner-sm2.jpg") then
		session("g_headbannerimg") = "banner-sm2.jpg"
		session("g_headfooterimg") = "footer-sm.jpg"
		session("g_headbanner") = g_bannerbase & session("g_headbannerimg")
		session("g_headfooter") = g_bannerbase & session("g_headfooterimg")
		session("isStoreFront") = FALSE
	else
		session("g_headbanner") = g_storefrontbase & session("g_headbannerimg")
		session("g_headfooter") = g_storefrontbase & session("g_headfooterimg")
		session("isStoreFront") = TRUE
	end if
end sub

function GetSacWebUser(strUser, strPassword, intMinLevel)
	' minimum levels:
	'   15 = programmer admin
	'   25 = system admin (normal)  <-- ***
	'   35 = guest admin (read-only)
	'   45 = guest user (read-only, but not to admin pages)
	dim objAuth, intResult
	set objAuth = Server.CreateObject("IIS4Utils.SacWebSecurity")
	intResult = objAuth.Authenticate(strUser, strPassword)
	'if (intResult > 0) and (intResult <= intMinLevel) then
	'	GetSacWebUser = intResult
	'else
	'	GetSacWebUser = intResult
	'end if
	set objAuth = nothing
end function

function FixFilename(s)
	dim strFileName
	strFileName = replace(s & "", " ", "%20")
	FixFilename = replace(strFileName & "", "+", "%20")
end function

sub DrawRefererGreeting
	dim strRefID, strUserName, strDiscount
	strRefID = Session(STR_SESSION_REFERER_ID)
	if strRefID <> "0" and strRefID <> "" then
		strUserName = Session(STR_SESSION_REFERER_NAME)
		strDiscount = Fix(Session(STR_SESSION_REFERER_DISCOUNT) * 100)
		response.write fontx(2,1,cBlack) & "<B>Welcome, " & strUserName & " customer. <BR>Your " & strDiscount & "% discount will be applied to your order upon checkout.</B><BR><BR></FONT>"
	end if
end sub

sub stlSubLinks(dctLinks)

end sub

sub DrawMenuGroup(rsInput)
	dim intParentID, strParentName, blnShowChild
	blnShowChild = true
	if not rsInput.eof then
		intParentID = rsInput("intID")
		strParentName = rsInput("vchItemName")
		%>
						<div class="menugroup">
						<a href="<%= nonsecurebase %>store/default.asp?parentid=<%=intParentID%>" class="menularge"><%= strParentName %></a><br />
		<%
		dim rsTemp
		set rsTemp = Inventory_GetItemsFromFolder(rsInput("intID"), true, false)
		if not g_blnShowAllChildren then
			if g_strTitle = strParentName or gintInvParentUpID = intParentID then
			else
				blnShowChild = false
			end if
		end if
		if not rsTemp.eof then
			while not rsTemp.eof
				intParentID = rsTemp("intID")
				strParentName = rsTemp("vchItemName")
				if blnShowChild or gintInvParentUpID = intParentID then
					%>
						<a href="<%= nonsecurebase %>store/default.asp?parentid=<%= intParentID %>" class="left"><%= strParentName %></a><br />
					<%
					if g_strTitle = strParentName or gintInvParentUpID = intParentID then
						DrawMenuChild(intParentID)
					end if
				end if
				rsTemp.MoveNext
			wend
		end if
		rsTemp.close
		set rsTemp = nothing
		%>
						</div>
	<%
	end if

end sub

sub DrawMenuChild(intParentID)
	if IsNumeric(intParentID) then
	else
		intParentID = 0 
	end if
	if intParentID > 0 then
		dim rsChild, intChildID, strChildName
		set rsChild = Inventory_GetItemsFromFolder(intParentID, true, false)
		if not rsChild.eof then
		%>
						<div class="menugroup">
			<%
			while not rsChild.eof
				intChildID = rsChild("intID")
				strChildName = rsChild("vchItemName")
				%>
						. <a href="<%= nonsecurebase %>store/default.asp?parentid=<%= intChildID %>" class="left"><%= strChildName %></a><br />
				<%
				rsChild.MoveNext
			wend
			%>
						</div>
		<%
		end if
		rsChild.close
		set rsChild = nothing
		%>
	<%
	end if

end sub

sub DrawDocument(intID)
	dim strSQL, rsTemp, strContent
	strSQL = "SELECT vchTitle, txtContent FROM " & STR_TABLE_GOEDIT & " WHERE intID=" & intID
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		strContent = rsTemp("txtContent") & ""
		strContent = replace(strContent, "<" & "% imagebase %" & ">", imagebase)
		strContent = replace(strContent, "<" & "% aspbase %" & ">", aspbase)
		response.write strContent
	else
		response.write "<BR><BR><BR><B>The requested page could not be found or is unavailable.</B><BR><BR><BR>"
	end if
	rsTemp.close
	set rsTemp = nothing
end sub

Sub SendMailX(fromName,toName,ccName,bccName,subject,bodyText)
	dim awscopy
'	awscopy = "mred@americanwebservices.com"
	if IsValid(bccName) then
		if bccName <> awscopy then
			bccName = bccName & "," & awscopy
		end if
	else
		bccName = awscopy
	end if
	dim m
	set m = CreateObject("CDONTS.NewMail")
	m.From = fromName
	m.To = toName
	m.CC = ccName
	m.bcc = bccName
	m.Subject = subject
	m.Body = bodyText
	m.Importance = 1
	m.Send()
	Set m = Nothing
End Sub

Sub AddStdEmailSecurity(b)
	' Adds standard security text to the end of an email mesage
	b = b & vbcrlf
	b = b & "-- Security Information --" & vbcrlf
	' add any database identification below:
	'b = b & "DBUsername= " & rsDealer("ID") & "-" & rsDealer("Username") & vbcrlf
	b = b & "Reference=  " & Request.ServerVariables("REMOTE_ADDR") & vbcrlf
	b = b & "Hostname=   " & Request.ServerVariables("REMOTE_HOST") & vbcrlf
	b = b & "Browser=    " & Request.ServerVariables("HTTP_USER_AGENT") & vbcrlf
	b = b & "Referer=    " & Request.ServerVariables("HTTP_REFERER") & vbcrlf
	b = b & "Encryption= "
	if Request.ServerVariables("SERVER_PORT_SECURE") = 1 then
		b = b & "Enabled" & vbcrlf
	else
		b = b & "Not Enabled" & vbcrlf
	end if
	b = b & vbcrlf
	b = b & "======================================================="
end sub


'Moved to inc_Init
'4/2/2004 J.Helms
sub CheckForReferalEx
	
	if Request("refererid")&""<>"" then
		Session(SESSION_REFERAL_ID) = Request("refererid")
	elseif Session(SESSION_REFERAL_NAME)="" then
		Session(SESSION_REFERAL_NAME) = request.ServerVariables("HTTP_REFERER")
	end if	
	
	'///
	'/// updated with an rname so that it doesn't rely on the refferal system
	'///
	if Request("rname")&""<>"" then
		Session("rname")=Request("rname")&""
	end if
	
end sub

%>