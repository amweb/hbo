<% 
function iC_Main(intOrderID, dctResults)

	'Check for any LineItems in the cart that must be sent to ICampaigns
	dim strSQL, rsICData, strResponse

	strSQL = "SELECT " & _
		"TCS_Shopper.vchFirstName, TCS_Shopper.vchLastName, TCS_Shopper.vchAddress1, TCS_Shopper.vchAddress2, TCS_Shopper.vchCity, TCS_Shopper.vchState, TCS_Shopper.vchZip, TCS_Shopper.vchCountry, TCS_Shopper.vchDayPhone, isnull(TCS_Shopper.vchEmail, (select top 1 TCS_Shopper.vchEmail from TCS_Shopper WHERE TCS_Order.intBillShopperID = TCS_Shopper.intID)) as vchEmail, TCS_Inv.intICampaignsListID, TCS_Inv.vchICampaignsHash " & _
		"FROM TCS_LineItem INNER JOIN TCS_Order INNER JOIN TCS_Shopper ON TCS_Order.intShipShopperID = TCS_Shopper.intID ON TCS_LineItem.intOrderID = TCS_Order.intID INNER JOIN TCS_Inv ON TCS_LineItem.intInvID = TCS_Inv.intID " & _
		"WHERE (TCS_Order.intID = " & intOrderID & ") AND (TCS_Inv.chrICampaignsFlag = 'Y') AND (TCS_LineItem.chrStatus = 'A')"
	
	dim i
	i=1
	
	Set rsICData = ConnOpenRS(strSQL)
	Do While Not rsICData.EOF
		strResponse = XMLSOAPPost(GenerateXMLRequest(iC_dctFieldNames(rsICData)))
		If strResponse = "OK" Then 
			dctResults.Add i, "Success: Posted information to i-Campaigns " & GenerateFriendlyHTML(IC_dctFieldNames(rsICData)) 
		Else 
			dctResults.Add i, "Error: Subscriber information rejected by i-Campaigns (" & strResponse & ") " & GenerateFriendlyHTML(IC_dctFieldNames(rsICData))
		End If
		i=i+1
		rsICData.MoveNext
	Loop
	rsICData.Close
	Set rsICData = Nothing

	Set iC_Main = dctResults

end function

function iC_dctResultCodes
	set iC_dctResultCodes = Server.CreateObject("Scripting.Dictionary")
	with iC_dctResultCodes
		.Add "OK", "Request completed successfully."
		.Add "1000", "This web site is not authorized to access this contact list (check list id and/or hash string)."
		.Add "1001", "An improperly formatted e-mail address was provided."
		.Add "1002", "The mailing address is of poor quality and has been rejected by the address parser."
		.Add "1003", "There was a problem with the address parser module (such as invalid license, missing files, etc.)"
		.Add "1004", "The name is of poor quality and has been rejected by the name parser."
		.Add "1005", "There was a problem with the name parser module (such as invalid license, missing files, etc.)"
		.Add "1006", "A possible duplicate contact record was found in the database."
	end with
end function

function iC_dctFieldNames(rsICData) '
	set iC_dctFieldNames = Server.CreateObject("Scripting.Dictionary")
	with iC_dctFieldNames
		.Add "_fullName", rsICData("vchFirstName") & " " & rsICData("vchLastName")
		.Add "_address1", rsICData("vchAddress1")
		.Add "_address2", rsICData("vchAddress2")
		.Add "_city", rsICData("vchCity")
		.Add "_state", rsICData("vchState")
		.Add "_zipcode", rsICData("vchZip")
		.Add "_country", rsICData("vchCountry")
		.Add "_phone", rsICData("vchDayPhone")
		.Add "_email", rsICData("vchEmail")
		.Add "_listId", rsICData("intICampaignsListID")
		.Add "_dupCheck", "true"
		.Add "_hash", rsICData("vchICampaignsHash")
		.Add "_optin", "false"
	end with
end function

private function XMLSafe(strInput)
	XMLSafe = strInput
end function

private function GenerateXMLRequest(dctFieldNames)

	dim strTemp, strFieldName

	strTemp = strTemp & "<?xml version=""1.0"" encoding=""utf-8""?>" & vbcrlf
	strTemp = strTemp & "<soap12:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap12=""http://www.w3.org/2003/05/soap-envelope"">" & vbcrlf
		strTemp = strTemp & vbTab & "<soap12:Body>" & vbCrLf
			strTemp = strTemp & vbTab & vbTab & "<AddResidentialContact xmlns=""http://i-campaign.com/webservice"">" & vbCrLf
			for each strFieldName in dctFieldNames
				strTemp = strTemp & vbTab & vbTab & vbTab & "<" & strFieldName & ">" & XMLSafe(dctFieldNames(strFieldName)) & "</" & strFieldName & ">" & vbcrlf
			next
			strTemp = strTemp & vbTab & vbTab & "</AddResidentialContact>" & vbCrLf
  		strTemp = strTemp & vbTab & "</soap12:Body>" & vbCrLf
	strTemp = strTemp & "</soap12:Envelope>" & vbCrLf

	Response.write "generatexmlrequest: <pre>" & server.htmlencode(strTemp) & "</pre>"
	GenerateXMLRequest = strTemp
end function

private function GeneratePOSTRequest(dctFieldNames)

	dim strTemp, strFieldName

	for each strFieldName in dctFieldNames
		strTemp = strTemp & strFieldName & "=" & Server.URLEncode(dctFieldNames(strFieldName)&"") & "&"
	next

	'Response.write "generatepostrequest: <pre>" & server.htmlencode(strTemp) & "</pre>"
	GeneratePOSTRequest = strTemp
end function

private function GenerateFriendlyHTML(dctFieldNames)

	dim strTemp, strFieldName
	strTemp = "<p>The information that was posted follows:<ul>"

	for each strFieldName in dctFieldNames
		strTemp = strTemp & "<li>" & strFieldName & ": " & Server.HTMLEncode(dctFieldNames(strFieldName)&"") & "</li>"
	next

	strTemp = strTemp & "</ul></p>"

	'Response.write "GenerateFriendlyHTML: <pre>" & server.htmlencode(strTemp) & "</pre>"
	GenerateFriendlyHTML = strTemp
end Function

private function HTTPPost(strReq)
'uncomment if we need to switch from HTTP XML/SOAP to HTTP POST to get data to iCampaigns
	'dim objHTTP
	'set objHTTP = Server.CreateObject("Msxml2.XMLHTTP")
	
	'If False Then 'do we post to a local web server instead of the production service?
	'If InStr(Request.ServerVariables("SERVER_NAME"), "awsdev") > 0 Then
		'objHTTP.open "POST", "http://mhudson/showme.asp", false
		'objhttp.setRequestHeader "Host", "mhudson"
		'objhttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
		'objhttp.setRequestHeader "Content-Length", CStr(Len(strReq))
		'objHTTP.send(strReq)

	'Else
		'objHTTP.open "POST", "http://tcs.i-campaign.com/ContactService.asmx/AddResidentialContact", false
		'objhttp.setRequestHeader "Host", "tcs.i-campaign.com"
		'objhttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
		'objhttp.setRequestHeader "Content-Length", CStr(Len(strReq))
		'objHTTP.send(strReq)
	'End If
	
	HTTPPost = objHTTP.status & " " & objHTTP.statusText & " <br />" & objHTTP.responseText
	'Response.Write HTTPPost

end function

private function XMLSOAPPost(strReq)
	dim objHTTP
	set objHTTP = Server.CreateObject("Msxml2.XMLHTTP")

	'If False Then 'do we post to a local web server instead of the production service?
	If InStr(Request.ServerVariables("SERVER_NAME"), "awsdev") > 0 Then 
		'mhudson/showme.asp will always return an "OK" result.
		objHTTP.open "POST", "http://mhudson/showme.asp", false
		'objhttp.setRequestHeader "Host", "mhudson"
		objhttp.setRequestHeader "Content-Type", "application/soap+xml; charset=utf-8"
		objhttp.setRequestHeader "Content-Length", CStr(Len(strReq))
		objHTTP.send(strReq)

	Else
		objHTTP.open "POST", "http://tcs.i-campaign.com/ContactService.asmx", false
		objhttp.setRequestHeader "Host", "tcs.i-campaign.com"
		objhttp.setRequestHeader "Content-Type", "application/soap+xml; charset=utf-8"
		objhttp.setRequestHeader "Content-Length", CStr(Len(strReq))
		objHTTP.send(strReq)
	End If
	
	Dim strOutput, strCode, dctPossibleResults
	strOutput = objHTTP.responseText

	if instr(1, strOutput, ">OK<") > 0 Then
		XMLSOAPPost = "OK"
	else
		XMLSoapPost = "Error: <br>" & vbCrLf
		Set dctPossibleResults = iC_dctResultCodes()
		For Each strCode In dctPossibleResults
			If InStr(strOutput, ">" & strCode & "<") Then XMLSOAPPost = XMLSOAPPost & dctPossibleResults(strCode) & "<br>" & vbCrLf
		Next 'strCode
		XMLSoapPost = XMLSoapPost & "<br>" & Server.HTMLEncode(strOutput) & "<br>" & vbCrLf

	end if

end function

%>

