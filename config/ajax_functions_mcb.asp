<%@ LANGUAGE="VBScript" %>
<!--#include file="incInit.asp"-->
<!--#include file="Json_2.0.4.asp"-->
<%
Response.ContentType = "application/json; charset=utf-8"
Server.ScriptTimeout = 2147483647

dim reqAction
dim strSQL, cmd, arParams, col, blnIsAdmin

reqAction = request("action")

select case reqAction
	case "savePackSlip"
		SavePackSlip
	case "loadCartContents"
		loadCartContents
	case "loadItemSlot"
		loadItemSlot
	case "saveItemSlot"
		saveItemSlot
	case "saveItemSlotMulti"
		saveItemSlotMulti
	case "loadLayout"
		loadLayout
	case "loadProfiles"
		loadProfiles
	case "loadOverview"
		loadOverview
	case "viewProfileContents"
		viewProfileContents
	case "viewProfilePackage"
		viewProfilePackage
	case "saveNewHardware"
		saveNewHardware
	case "getStoreListingZero"
		getStoreListingZero
	case "runAutoAssign"
		runAutoAssign
	case "loadStoreList"
		loadStoreList
	case "deleteItemFromProfilePackages"
		deleteItemFromProfilePackages
	case "loadSelectionSlot"
		loadSelectionSlot
	case "loadItemDetails"
		loadItemDetails
	case "getColorProcesses"
		getColorProcesses
end select

if request("admin")<>"" then
	blnIsAdmin = true
else
	blnIsAdmin = false
end if

Function QueryToJSON(dbcomm, params)
        Dim rs, jsa
        Set rs = dbcomm.Execute(,params,1)
        Set jsa = jsArray()
        Do While Not (rs.EOF Or rs.BOF)
                Set jsa(Null) = jsObject()
                For Each col In rs.Fields
                        jsa(Null)(col.Name) = col.Value
                Next
        rs.MoveNext
        Loop
        Set QueryToJSON = jsa
        rs.Close
End Function

sub SQLExampleASPJson
	strConn = "Provider=SQLNCLI10;Server=localhost;Database=NorthWind;Trusted_Connection=Yes;"
	Set conn = Server.CreateObject("ADODB.Connection")
	conn.Open strConn
	query = "SELECT * FROM Customers WHERE CustomerID = ?"
	CustomerID = Request.QueryString("CustomerID")
	arParams = array(CustomerID)
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = query
	Set cmd.ActiveConnection = conn
	QueryToJSON(cmd, arParams).Flush
end sub

sub loadCartContents

	dim count, rsData, strOutput
	count = 0
	OpenConn
	set rsData = GetOrderLineItems_Other(request("orderid"))
	strOutput = "["
	while not rsData.EOF
		if count <> 0 then
			strOutput= strOutput & ","
		end if
		
		strOutput = strOutput & "{""intParentID"": """ & rsData("intParentID") & """, ""intID"": """ & rsData("intID") & """, ""vchItemName"": """ & rsData("vchItemName") & """, ""vchPartNumber"": """ & rsData("vchPartNumber") & """, ""intQuantity"": """ & rsData("intQuantity") & """, ""vchImageURL"": """ & rsData("vchImageURL") & """}"
		
		count =  count +1
		rsData.MoveNext
	wend 
	strOutput = strOutput & "]"
	response.write strOutput
	response.flush
	CloseConn
	response.end

end sub

sub SavePackSlip

	Dim pickSlipNum, sku, rsCheck
	
	pickSlipNum = request("pickSlipNum")
	sku = request("sku")
	' response.write sku
	OpenConn
	
	strSQL = "select *  from " & STR_TABLE_PACKGROUP & " where vchPartNumber = '" & SQLEncode(URLDecode(sku)) & "'"
	
	set rsCheck = gobjConn.execute(strSQL)
	
	if rsCheck.eof then
		strSQL = "insert into " & STR_TABLE_PACKGROUP &" (vchPartNumber, intPackGroup) Values ('" & SQLEncode(URLDecode(sku)) & "', "& pickSlipNum & ") "
	else
		strSQL = "update " & STR_TABLE_PACKGROUP & " set intPackGroup = " & pickSlipNum & " where vchPartNumber = '" & SQLEncode(URLDecode(sku)) & "'"
	end if
	
	' response.write strSQL
	gobjConn.execute(strSQL)
	
	CloseConn()

end sub

sub loadProfiles

	strSQL = "select intID, vchProfileName from " & STR_TABLE_PROFILES & " where chrStatus = 'A'"
	'response.write strSQL
	OpenConn()
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()
	response.end

end sub


sub loadLayout

	dim intStoreID
	
	intStoreID = request("intStoreID")
	
	strSQL = "select intID, vchImageLocation from " & STR_TABLE_LAYOUTS & " where intID = (select intLayoutID from " & STR_TABLE_SHOPPER & " where intID = " & intStoreID & ")"
	'response.write strSQL
	OpenConn()
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()
	response.end

end sub

sub viewProfileContents

	dim intProfileID
	
	intProfileID = request("profileID")
	
	'strSQL = "select S.intDistrict,S.intLayout, S.vchAddress1, S.vchCity, S.vchDayPhone, S.vchDivision, S.vchFirstName, S.vchLastName, S.vchState, S.vchZip, S.vchType from " & STR_TABLE_SHOPPER & " S where S.intID in ( select intStoreShopperID from " & STR_TABLE_PROFILE_MAPPER & " PM where intProfileID = " & intProfileID & ") and S.chrStatus='A'"
	strSQL = "SELECT S.intDistrict, S.vchLabel, S.vchFirstName, S.[vchLastName], S.vchAddress1, S.[vchAddress2], S.vchCity, S.vchState, S.vchZip, S.intLayout, "
	strSQL = strSQL & " S.intStoreID, S.vchDivision, S.chrStoreType, ('" & virtualbase & "images/layouts/' + L.vchImageLocation) AS vchImageLocation"
	strSQL = strSQL & " FROM " & STR_TABLE_SHOPPER & " S "
	strSQL = strSQL & " LEFT JOIN " & STR_TABLE_LAYOUTS & " L ON S.intLayout = L.intID "
	strSQL = strSQL & " WHERE S.intID IN (SELECT [intStoreShopperID] FROM "
	strSQL = strSQL & STR_TABLE_PROFILEMAPPER & " WHERE [intProfileID]=" & intProfileID & ") AND [chrStatus] = 'A' "
	'strSQL = strSQL & " SORY BY [vchLabel] ASC "
	'response.write strSQL
	OpenConn()
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()
	response.end

end sub

sub viewProfilePackage
	dim output, count, itemCount, intLayoutID, intProfileID, intCampaignID, rsFixtures, rsCounts, rsItems
	intCampaignID = request("campaignID")
	intProfileID = request("profileID")
	strSQL = "select intID, vchItemName from " & STR_TABLE_INVENTORY & " where intParentId=1 ORDER BY [vchItemName]"
	OpenConn
	set rsFixtures = gobjConn.execute(strSQL)
	output = "["
	count = 0 
	while not rsFixtures.EOF
		if count <> 0 then
			output= output & ","
		end if
		if rsFixtures("intID")&"" = "314" then
			output = output & "{""vchFixtureName"": """ & rsFixtures("vchItemName") & """, ""intNumerator"":0, ""intDenominator"":0, ""Items"": ["
		else
			strSQL = "select (select count(intParentID) from " & STR_TABLE_INVENTORY & " I right join " & STR_TABLE_PROFILE_PACKAGES & " PP on PP.intItemID = I.intID where I.intParentID = "
			strSQL = strSQL & rsFixtures("intID") & " and PP.intProfileID = " & intProfileID & " and PP.intCampaignID = " & intCampaignID & " ) as numerator,"
			strSQL = strSQL & " (select [" & replace(rsFixtures("vchItemName"), " ", "") & "] from " & STR_TABLE_PROFILES & " where intID = " & intProfileID & " ) as denominator"
			set rsCounts = gobjConn.execute(strSQL)
			output = output & "{""vchFixtureName"": """ & rsFixtures("vchItemName") & """, ""intNumerator"":" & rsCounts("numerator") & ", ""intDenominator"":" & rsCounts("denominator") & ", ""Items"": ["
		end if
		strSQL = "select I.[vchItemName] as Item, I.vchPartNumber, PP.intID AS PPIntID, PP.intQty "
		strSQL = strSQL & "from " & STR_TABLE_INVENTORY & " I "
		strSQL = strSQL & "left join " & STR_TABLE_INVENTORY & " F on I.intParentID = F.intID "
		strSQL = strSQL & "left join " & STR_TABLE_PROFILE_PACKAGES & " PP on I.intID = PP.intItemID "
		strSQL = strSQL & "where I.chrType = 'I' and PP.intCampaignID = " & intCampaignID & " and PP.intProfileID = " & intProfileID & " and I.intParentID = " & rsFixtures("intID")
		strSQL = strSQL & " order by I.vchItemName"
		'response.write strSQL
		'response.end
		set rsItems = gobjConn.execute(strSQL)
		if not rsItems.EOF then
			itemCount = 1
			'output = output & "{"
			while not rsItems.EOF
				if itemCount <> 1 then
					output= output & ","
				end if
				output = output & "{""Item"": """ & rsItems("Item") & """, ""DeleteID"": """ & rsItems("PPIntID") & """, ""intQty"": """ & rsItems("intQty") & """, ""vchPartNumber"": """ & rsItems("vchPartNumber") & """}"
				rsItems.MoveNext
				itemCount = itemCount + 1
			wend
			'output = output & "}"
		end if
		output = output & "]}"
		count = count + 1
		rsFixtures.MoveNext
	wend
	output = output & "]"
	response.write output
end sub

sub saveNewHardware

	dim vchHardwareName, vchHardwareSku
	
	vchHardwareName = request("hardwareName")
	vchHardwareSku = request("hardwareSku")
	
	strSQL = "insert into " & STR_TABLE_INVENTORY & " (chrStatus, chrType, dtmCreated, dtmUpdated, vchCreatedByUser, vchUpdatedByUser, vchCreatedByIP,vchUpdatedByIP,intParentID, intSortOrder, intHitCount, chrTaxFlag, chrICFlag, chrSoftFlag, vchItemName, vchPartNumber) values "
	strSQL = strSQL & " ('A'," 'chrStatus
	strSQL = strSQL & " 'I'," 'chrType
	strSQL = strSQL & " getDate()," 'dtmCreated
	strSQL = strSQL & " getDate()," 'dtmUpdated
	strSQL = strSQL & " '" & Session(SESSION_MERCHANT_ULOGIN) & "'," 'vchCreatedByUser
	strSQL = strSQL & " '" & Session(SESSION_MERCHANT_ULOGIN) & "'," 'vchUpdatedByUser
	strSQL = strSQL & " '" & gstrUserIP & "'," 'vchCreatedByIP
	strSQL = strSQL & " '" & gstrUserIP & "'," 'vchUpdatedByIP
	strSQL = strSQL & " 314," 'intParentID
	strSQL = strSQL & " 0," 'intSortOrder
	strSQL = strSQL & " 0," 'intHitCount
	strSQL = strSQL & " 'N'," 'chrTaxFlag
	strSQL = strSQL & " 'N'," 'chrICFlag
	strSQL = strSQL & " 'N'," 'chrSoftFlag
	strSQL = strSQL & " '" & vchHardwareName & "'," 'vchItemName
	strSQL = strSQL & " '" & vchHardwareSku & "')" 'vchPartNumber
	OpenConn
	gobjConn.execute(strSQL)
	
	strSQL = "Select intID, vchItemName, vchPartNumber from " & STR_TABLE_INVENTORY & " WHERE intParentID=314"
	'response.write strSQL
	OpenConn()
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()
	response.end

end sub

sub loadOverview

	dim intCampaignID, intProfileID, intLayoutID, rsTemp, output, itemID, intDenominator, intNumerator, rsFixtures, rsLayoutCount, rsItemsCount, rsItems, itemCount, count

	intCampaignID = request("campaignID")
	intProfileID = request("profileID")
	intLayoutID = request("intLayoutID")
	OpenConn
	
	'get available fixture types from package
	strSQL = "select intID, vchItemName from " & STR_TABLE_INVENTORY & " where intParentID = 1 and chrStatus = 'A' and intid in (select distinct I.intparentid from "
	strSQL = strSQL & STR_TABLE_INVENTORY & " I join " & STR_TABLE_PROFILE_PACKAGES & " PP on PP.intItemID = I.intID"
	if blnIsAdmin = true then
		strSQL = strSQL & " and PP.intProfileID= " & intProfileID
	end if
	strSQL = strSQL & " and PP.intCampaignID = " & intCampaignID & ") and intID = 2 "
	'response.write strSQL
	'response.end
	set rsFixtures = gobjConn.execute(strSQL)
	
	count = 0
	output = "["
	while not rsFixtures.EOF
		if count <> 0 then
			output = output & ","
		end if
		
		strSQL = "select count(intParentID) as count from " & STR_TABLE_INVENTORY & " I join " & STR_TABLE_PROFILE_PACKAGES 
		strSQL = strSQL & " PP on PP.intItemID = I.intID where I.intParentID = " & rsFixtures("intID")
		if blnIsAdmin = true then
			strSQL = strSQL & " and PP.intProfileID = " & intProfileID
		end if
		strSQL = strSQL & " and PP.intCampaignID = " & intCampaignID & ";"
		'response.write strSQL
		set rsItemsCount = gobjConn.execute(strSQL)
		
		'compare how many possible slots the store can accomidate for the fixture vs how many items were created for the campaignID
		'use the lesser of the 2 to have as the maximum fixtures to assign
		intDenominator = rsItemsCount("count")
		
		strSQL = "select count(intParentID) as count from " & STR_TABLE_INVENTORY & " I join " & STR_TABLE_PROFILE_STORE_PACKAGE &" PP on PP.intItemID = I.intID where I.intParentID = " & rsFixtures("intID")
		if blnIsAdmin = true then
			strSQL = strSQL & " and PP.intProfileID = " & intProfileID
		end if
		strSQL = strSQL & " and PP.intCampaignID = " & intCampaignID & " and PP.intLayoutID = " & intLayoutID
		'response.write strSQL
		set rsItemsCount = gobjConn.execute(strSQL)
		intNumerator = rsItemsCount("count")
		
		output = output & "{""vchFixtureName"": """ & rsFixtures("vchItemName") & """, ""intNumerator"": " & intNumerator & ", ""intDenominator"": " & intDenominator & ", ""Items"": ["
		
		strSQL = "select distinct convert(int,SUBSTRING(LS.vchSlotName,5,3)) as intSort, LS.vchSlotName, SP.intID as intStorePackID, I.vchItemName, I.vchNumSides, SP.intLayoutID as intLayoutID, SP.intItemID "
			strSQL = strSQL & "from " & STR_TABLE_LAYOUTS_SLOTS & " LS "
			strSQL = strSQL & "left join " & STR_TABLE_PROFILE_STORE_PACKAGE & " SP on SP.intSlotID = LS.intID and  SP.intCampaignID = " & intCampaignID & " "
			strSQL = strSQL & "left join " & STR_TABLE_INVENTORY & " I on I.intID = SP.intItemID "
			'strSQL = strSQL & "from " & STR_TABLE_PROFILE_PACKAGES & " PP "
			'strSQL = strSQL & "left join " & STR_TABLE_PROFILE_STORE_PACKAGE & " SP on SP.intItemID = PP.intItemId "
			'strSQL = strSQL & "right join " & STR_TABLE_LAYOUTS_SLOTS & " LS on SP.intSlotID = LS.intId "
			'strSQL = strSQL & "left join " & STR_TABLE_INVENTORY & " I on I.intID = PP.intItemId "
			strSQL = strSQL & "where LS.vchSlotName like '%" & dctFixtureMap(rsFixtures("intID")&"") & "%' and LS.intLayoutID = " & intLayoutID & ""
			strSQL = strSQL & " order by intSort"
		'response.write strSQL
		set rsItems = gobjConn.execute(strSQL)
		
		if not rsItems.EOF then
			itemCount = 1
			while not rsItems.EOF
				'response.write intLayoutID & " " & rsItems("intStorePackID")
				'if clng(intLayoutID) = clng(rsItems("intLayoutID")) then
					if itemCount <> 1 then
						output= output & ","
					end if
					output = output & "{"
					output = output & """vchItemName"": """ & rsItems("vchItemName") & """, ""vchSlotName"": """ & rsItems("vchSlotName") & """, ""intStorePackID"": """ & rsItems("intStorePackID") & """, ""vchNumSides"": """ & rsItems("vchNumSides") & """, ""intLayoutID"": """ & rsItems("intLayoutID") & """"
					itemCount = itemCount + 1
					output = output & "}"
				'end if
				
				rsItems.MoveNext
			wend
		end if
		output = output & "]}"
		count = count + 1
		rsFixtures.MoveNext
	wend
	
	strSQL = "select intID, vchItemName, 2 as filter from " & STR_TABLE_INVENTORY & " where intParentID = 1 and chrStatus = 'A' and intid in (select distinct I.intparentid from "
	strSQL = strSQL & STR_TABLE_INVENTORY & " I join " & STR_TABLE_PROFILE_PACKAGES & " PP on PP.intItemID = I.intID"
	if blnIsAdmin = true then
		strSQL = strSQL & " and PP.intProfileID= " & intProfileID
	end if
	strSQL = strSQL & " and PP.intCampaignID = " & intCampaignID & ") and intID <> 2 order by vchItemName; "
	'response.write strSQL
	set rsFixtures = gobjConn.execute(strSQL)
	
	while not rsFixtures.EOF
		if count <> 0 then
			output = output & ","
		end if
		
		strSQL = "select Count(*) as count from " & STR_TABLE_LAYOUTS_SLOTS & " where vchSlotName like '%" & dctFixtureMap(rsFixtures("intID")) & "%' and intLayoutID = " & intLayoutID & ";"
		'response.write strSQL
		set rsLayoutCount = gobjConn.execute(strSQL)
		
		strSQL = "select count(intParentID) as count from " & STR_TABLE_INVENTORY & " I join " & STR_TABLE_PROFILE_PACKAGES & " PP on PP.intItemID = I.intID where I.intParentID = " & rsFixtures("intID")
		if blnIsAdmin  = true then
			strSQL = strSQL & " and PP.intProfileID = " & intProfileID
		end if
		strSQL = strSQL & " and PP.intCampaignID = " & intCampaignID & ";"
		'response.write strSQL
		set rsItemsCount = gobjConn.execute(strSQL)
		
		'compare how many possible slots the store can accomidate for the fixture vs how many items were created for the campaignID
		'use the lesser of the 2 to have as the maximum fixtures to assign
		intDenominator = rsItemsCount("count")
		
		strSQL = "select count(intParentID) as count from " & STR_TABLE_INVENTORY & " I join " & STR_TABLE_PROFILE_STORE_PACKAGE &" PP on PP.intItemID = I.intID where I.intParentID = " & rsFixtures("intID")
		if blnIsAdmin = true then
			strSQL = strSQL & " and PP.intProfileID = " & intProfileID
		end if
		strSQL = strSQL & " and PP.intCampaignID = " & intCampaignID & " and PP.intLayoutID = " & intLayoutID & ";"
		'response.write strSQL
		set rsItemsCount = gobjConn.execute(strSQL)
		intNumerator = rsItemsCount("count")
		
			output = output & "{""vchFixtureName"": """ & rsFixtures("vchItemName") & """, ""intNumerator"": " & intNumerator & ", ""intDenominator"": " & intDenominator & ", ""Items"": ["
			
			strSQL = "select distinct LS.vchSlotName, SP.intID as intStorePackID, I.vchItemName, I.vchNumSides, SP.intLayoutID as intLayoutID, SP.intItemID "
			strSQL = strSQL & "from " & STR_TABLE_LAYOUTS_SLOTS & " LS "
			strSQL = strSQL & "left join " & STR_TABLE_PROFILE_STORE_PACKAGE & " SP on SP.intSlotID = LS.intID  and  SP.intCampaignID = " & intCampaignID & " " '"" 'SP.intSlotID = LS.intID and  SP.intProfileID = " & intProfileID & " and SP.intCampaignID = " & intCampaignID & " "
			strSQL = strSQL & "left join " & STR_TABLE_INVENTORY & " I on I.intID = SP.intItemID "
			'strSQL = strSQL & "from " & STR_TABLE_PROFILE_PACKAGES & " PP "
			'strSQL = strSQL & "left join " & STR_TABLE_PROFILE_STORE_PACKAGE & " SP on SP.intItemID = PP.intItemId "
			'strSQL = strSQL & "right join " & STR_TABLE_LAYOUTS_SLOTS & " LS on SP.intSlotID = LS.intId "
			'strSQL = strSQL & "left join " & STR_TABLE_INVENTORY & " I on I.intID = PP.intItemId "
			strSQL = strSQL & "where LS.vchSlotName like '%" & dctFixtureMap(rsFixtures("intID")&"") & "%' and LS.intLayoutID = " & intLayoutID & " and I.intParentID = " & rsFixtures("intID")&""
			'response.write strSQL
			set rsItems = gobjConn.execute(strSQL)
			
			if not rsItems.EOF then
				itemCount = 1
				while not rsItems.EOF
					if clng(intLayoutID) = clng(0&rsItems("intLayoutID")) then
						if itemCount <> 1 then
							output= output & ","
						end if
						output = output & "{"
						output = output & """vchItemName"": """ & rsItems("vchItemName") & """, ""vchSlotName"": """ & rsItems("vchSlotName") & """, ""intStorePackID"": """ & rsItems("intStorePackID") & """, ""vchNumSides"": """ & rsItems("vchNumSides") & """, ""intLayoutID"": """ & rsItems("intLayoutID") & """"
						itemCount = itemCount + 1
						output = output & "}"
					end if
					
					rsItems.MoveNext
				wend
			end if
			output = output & "]}"
			count = count + 1
		rsFixtures.MoveNext
	wend
	
	output = output & "]"
	
	response.write output

end sub

sub loadSelectionSlot
	dim intLayoutID, vchItemSlotName, intCampaignID, intProfileID

	intLayoutID = request("layoutID")
	vchItemSlotName = request("slotName")
	intCampaignID = request("campaignID")
	intProfileID = request("profileID")
	
	OpenConn
	strSQL = "select LS.vchSlotName, I.vchItemName, I.intID, F.vchImageURL as FixtureImage, F.vchOptionList1, F.vchOptionList2, F.vchOptionList3, F.vchOptionList4 "
	strSQL = strSQL & "from " & STR_TABLE_PROFILE_PACKAGES & " PP "
	strSQL = strSQL & "left join " & STR_TABLE_PROFILE_STORE_PACKAGE & " SP on PP.intItemID = SP.intItemID "
	strSQL = strSQL & "left join " & STR_TABLE_INVENTORY & " I on I.intID = PP.intItemID "
	strSQL = strSQL & "left join " & STR_TABLE_INVENTORY & " F on F.intID = I.intParentID "
	strSQL = strSQL & "left join " & STR_TABLE_LAYOUTS_SLOTS & " LS on SP.intSlotID = LS.intID "
	if blnIsAdmin = true then
		strSQL = strSQL & "where PP.intProfileID = " & intProfileID & " and"
		strSQL = strSQL & " PP.intCampaignID = " & intCampaignID & " "
	else
		strSQL = strSQL & "WHERE PP.intCampaignID = " & intCampaignID & " "
	end if
	'strSQL = strSQL & "and LS.intLayoutID = " & intLayoutID & " "
	strSQL = strSQL & "and I.intParentID = 2 ORDER BY I.[vchItemName]"
	'response.write strSQL
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()
	response.end

end sub

sub deleteItemFromProfilePackages
	dim intProfilePackagesItem
	
	intProfilePackagesItem = request("PPIntID")
	OpenConn
	strSQL = "DELETE FROM " & STR_TABLE_PROFILE_PACKAGES & " WHERE [intID]=" & intProfilePackagesItem
	gobjConn.execute(strSQL)
	CloseConn()
end sub

sub getStoreListingZero

	dim intProfileID
	
	intProfileID = request("intProfileID")
	
	strSQL = "Select S.intID as intID,  "
	strSQL = strSQL & " S.vchFirstName as storeName, S.vchLastName as DisplayName, S.vchCity as City, S.vchState as State, S.intLayout "
	strSQL = strSQL & " from " & STR_TABLE_PROFILE_MAPPER & " PM "
	strSQL = strSQL & " join " & STR_TABLE_SHOPPER & " S on PM.intStoreShopperID = S.intID "
	strSQL = strSQL & " where PM.intProfileID =  " & intProfileID & " and S.intLayout is not null"
	
	'response.write strSQL
	OpenConn()
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()
	response.end

end sub 

sub runAutoAssign
	dim intCampaignID, rsAllItemsInProfiles, rsStoreProfileList, rsLayoutSlots, rsCheckProfileStoreMap, rsOpenSlots, intprofileid, rsUsedSlots, rsMainFixture, intSlotID, mainFixtureSlot, accessorySlot, accessorySlotSplit, count, pass, strInsertSQL, intLayoutID
	dim intNumberOfItems, i, timerStart, timerEnd
	redim arrItemList(1,0)
	intCampaignID = request("intCampaignID")
	intProfileID = request("intProfileID")
	intLayoutID = request("intLayoutID")
	intNumberOfItems = 0

	strSQL = "select PP.intItemID, F.intID as FixtureID "
	strSQL = strSQL & "from " & STR_TABLE_INVENTORY & " I "
	strSQL = strSQL & "left join " & STR_TABLE_INVENTORY & " F on I.intParentID = F.intID "
	strSQL = strSQL & "left join " & STR_TABLE_PROFILE_PACKAGES & " PP on I.intID = PP.intItemID "
	strSQL = strSQL & "where I.chrType = 'I' and F.intID <> 2 and PP.intCampaignID = " & intCampaignID & " and PP.intProfileID = " & intProfileID & " and I.chrStatus <> 'D' "
	OpenConn
	'response.write "all items sql: " & strSQL & ";<br /><br />"
	set rsAllItemsInProfiles = gobjConn.execute(strSQL)
	
	
	strInsertSQL = ""
	set rsStoreProfileList = gobjConn.execute(strSQL)
	'response.write "store profile sql: " & strSQL & ";<br /><br />"
	if not rsAllItemsInProfiles.EOF then
		i = 0
		while not rsAllItemsInProfiles.eof
			REDIM PRESERVE arrItemList(1,i) 
			arrItemList(0,i) = rsAllItemsInProfiles("intItemID")&""
			arrItemList(1,i) = rsAllItemsInProfiles("FixtureID")&""
			i = i + 1
			rsAllItemsInProfiles.MoveNext
		wend
	end if
	
	For i = 0 to UBound(arrItemList, 2) 
	
	'response.write "store shopper id: " &  rsStoreProfileList("intStoreShopperID") & "<br /><br />"
		'response.write i
		'FixtureID = 2 is End Cap Headers, requested manual assign
		if arrItemList(1,i) <> "2" then
			'check if it is assigned already
			strSQL = "select top 1 intItemID from " & STR_TABLE_PROFILE_STORE_PACKAGE & " "
			strSQL = strSQL & "where intCampaignID = " & intCampaignID & " and intProfileID = " & intProfileID & " and intLayoutID = " & intLayoutID & " "
			strSQL = strSQL & " and intItemID = " & arrItemList(0,i)
			'response.write "Timer: " & Timer & "check profile store map sql: " & strSQL & ";<br /><br />"
			set rsCheckProfileStoreMap = gobjConn.execute(strSQL)
			
			if rsCheckProfileStoreMap.eof then
				
				'response.write "fixture mapping: " & dctFixtureMap(rsAllItemsInProfiles("FixtureID")&"") & "<br /><br />"
				'response.write "fixture instr: " & instr(dctFixtureMap(rsAllItemsInProfiles("FixtureID")&""), "X-") & "<br /><br />"
				if instr(dctFixtureMap(arrItemList(1,i)), "X-") > 0 then
					timerStart = Timer
					'response.write "Timer begin X: " & Timer & "<br />"
					'if it is a variable fixture slot assignment make sure it will not go next to another of the same type
					strSQL = "select LS.vchSlotName from " & STR_TABLE_PROFILE_STORE_PACKAGE  & " SP "'where intLayoutID = " & rsStoreProfileList("intLayout") & " "
					strSQL = strSQL & "join " & STR_TABLE_LAYOUTS_SLOTS & " LS on LS.intID = SP.intSlotID "
					strSQL = strSQL & "join " & STR_TABLE_INVENTORY & " I on I.intID = SP.intItemID "
					strSQL = strSQL & "where I.intParentID = " & arrItemList(1,i) & " "
					strSQL = strSQL & "and SP.intLayoutID = " & intLayoutID & " "
					strSQL = strSQL & "and SP.intCampaignID = " & intCampaignID & " "
					strSQL = strSQL & "and SP.intProfileID = " & intProfileID & " "
					strSQL = strSQL & "order by vchSlotName"
					'response.write "Used slots sql:" & strSQL & ";<br /><br />"
					set rsUsedSlots = gobjConn.execute(strSQL) ' get currently used slots for fixture type
					
					strSQL = "select top 1 intID from " & STR_TABLE_LAYOUTS_SLOTS & " where intLayoutID = " & intLayoutID & " "
					strSQL = strSQL & "and intID not in (select intSlotID from " & STR_TABLE_PROFILE_STORE_PACKAGE & " where intCampaignID = " & intCampaignID & " and intProfileID = " & intProfileID & " and intLayoutID = " & intLayoutID & ") "
					strSQL = strSQL & "and vchSlotName like '%" & dctFixtureMap(arrItemList(1,i)) & "%' "
					'loop the used slots
					if not rsUsedSlots.EOF then
						while not rsUsedSlots.EOF
							strSQL = strSQL & "and vchSlotName not like '%" & left(rsUsedSlots("vchSlotName"),len(rsUsedSlots("vchSlotName"))-1) &"%' "
							rsUsedSlots.MoveNext
						wend
					end if
					strSQL = strSQL & "order by vchSlotName"
					set rsOpenSlots = gobjConn.execute(strSQL)
					'response.write "Open slots sql:" & strSQL & ";<br /><br />"
					if not rsOpenSlots.EOF then
						intSlotID = rsOpenSlots("intID")
						pass = true
						'response.write "pass: true, slot: " & intSlotID & "<br /><br />"
					else
						pass = true
						intSlotID = "bad"
						'response.write "bad slot: " & rsAllItemsInProfiles("intItemID") & "<br /><br />"
					end if
					'response.write "Timer end X: " & (Timer - timerStart) & "<br />"
				'if it is a variable fixture accessory slot assignment make sure it goes next to an appropriate sign
				elseif instr(dctFixtureMap(arrItemList(1,i)), "XA") > 0 then
					timerStart = Timer
					'response.write "Timer begin XA: " & Timer & "<br />"
					strSQL = "select LS.vchSlotName from " & STR_TABLE_PROFILE_STORE_PACKAGE & " SP "
					strSQL = strSQL & "join " & STR_TABLE_LAYOUTS_SLOTS & " LS on SP.intSlotID = LS.intID "
					strSQL = strSQL & "join " & STR_TABLE_INVENTORY & " I on I.intID = SP.intItemID "
					strSQL = strSQL & "where I.intParentID in (" & dctFixtureAccessoryMap(arrItemList(1,i)) & ") "
					strSQL = strSQL & "and SP.intLayoutID = " & intLayoutID & " "
					strSQL = strSQL & "and SP.intCampaignID = " & intCampaignID & " "
					strSQL = strSQL & "and SP.intProfileID = " & intProfileID & " "
					strSQL = strSQL & "order by vchSlotName"
					set rsMainFixture = gobjConn.execute(strSQL) 'get main fixtures that match accessory
					
					strSQL = "select top 1 intID from " & STR_TABLE_LAYOUTS_SLOTS & " where intLayoutID = " & intLayoutID & " "
					strSQL = strSQL & "and intID not in (select intSlotID from " & STR_TABLE_PROFILE_STORE_PACKAGE & " where intCampaignID = " & intCampaignID & " and intProfileID = " & intProfileID & " and intLayoutID = " & intLayoutID & ") "
					'loop the used slots
					if not rsMainFixture.EOF then
						strSQL = strSQL & " and ("
						count = 0
						while not rsMainFixture.EOF
							accessorySlotSplit = split(rsMainFixture("vchSlotName"), "-")
							accessorySlot = accessorySlotSplit(0) & "A-" & accessorySlotSplit(1)
							strSQL = strSQL & "vchSlotName like '%" & accessorySlot &"%'"
							count = count + 1
							rsMainFixture.MoveNext
							if count <> 0 and rsMainFixture.EOF = false then
								strSQL = strSQL & " or "
							end if
						wend
						
						strSQL = strSQL & ") "
					end if
					strSQL = strSQL & "order by vchSlotName"
					'response.write "Open slots sql:" & strSQL & ";<br /><br />"
					set rsOpenSlots = gobjConn.execute(strSQL)
					
					if not rsOpenSlots.EOF then
						intSlotID = rsOpenSlots("intID")
						pass = true
						'response.write "pass: true, slot: " & intSlotID & "<br /><br />"
					else
						pass = true
						intSlotID = "bad"
						'response.write "bad slot: " & rsAllItemsInProfiles("intItemID") & "<br /><br />"
					end if
					'response.write "Timer end XA: " & (Timer - timerStart) & "<br />"
				else
					timerStart = Timer
					'response.write "Timer begin Other: " & Timer & "<br />"
					'response.write " in total else<br /><br />"
					'no restrictions; find slot to insert into
					strSQL = "select top 1 intID from " & STR_TABLE_LAYOUTS_SLOTS & " where intLayoutID = " & intLayoutID & " "
					strSQL = strSQL & "and intID not in (select intSlotID from " & STR_TABLE_PROFILE_STORE_PACKAGE & " where intCampaignID = " & intCampaignID & " and intProfileID = " & intProfileID & " and intLayoutID = " & intLayoutID & ") "
					strSQL = strSQL & "and vchSlotName like '%" & dctFixtureMap(arrItemList(1,i)) & "%' "
					strSQL = strSQL & "order by vchSlotName"
					set rsOpenSlots = gobjConn.execute(strSQL)
					'response.write "Open slots sql:" & strSQL & ";<br /><br />"
					if not rsOpenSlots.EOF then
						intSlotID = rsOpenSlots("intID")
					else
						intSlotID = "bad"
					end if
					'response.write "Timer end Other: " & (Timer - timerStart) & "<br />"
				end if

				'insert it if we got a match
				'response.write "item:" & arrItemList(0,i) & " slot:" & intSlotID & " layout:" & rsStoreProfileList("intLayout") & "<br /><br />"
				if intSlotID <> "bad" then
					strInsertSQL = strInsertSQL & "insert into " & STR_TABLE_PROFILE_STORE_PACKAGE & " (intCampaignID, intProfileID, intLayoutID, intSlotID, intItemID) values "
					strInsertSQL = strInsertSQL & "(" & intCampaignID & "," & intProfileID & "," & intLayoutID & ",'" & intSlotID & "'," & arrItemList(0,i) & ");"
					
					'response.write "insert into statement: " & strInsertSQL
				end if
				if strInsertSQL <> "" then
					gobjConn.execute(strInsertSQL)
					strInsertSQL = ""
				end if
				'response.write "Timer item loop: " & Timer & "<br />"
			else
			
			end if
		end if
	next
	
end sub

sub loadItemSlot

	dim intLayoutID, vchItemSlotName, intCampaignID, intProfileID
	
	intLayoutID = request("layoutID")
	vchItemSlotName = request("slotName")
	intCampaignID = request("campaignID")
	intProfileID = request("profileID")
	OpenConn
	strSQL = "select I.vchItemName, I.vchImageURL, PI.vchItemName as savedItemName, PP.intItemID, I.vchOptionList2, case when PP.intItemID is not null then (select vchNumSides from "
	strSQL = strSQL & STR_TABLE_INVENTORY & " where intID =  PP.intItemID) else null end as oldSides from " & STR_TABLE_INVENTORY & " I left join "
	strSQL = strSQL & STR_TABLE_PROFILE_PACKAGES & " PP ON PP.vchSlotName = '" & vchItemSlotName & "' and PP.intCampaignID = " & intCampaignID & " and PP.intProfileID = " & intProfileID
	strSQL = strSQL & " left join " & STR_TABLE_INVENTORY & " PI on PP.intItemID = PI.intID where I.intID = ( select LS.intItemID from " & STR_TABLE_LAYOUTS_SLOTS
	strSQL = strSQL & " LS where LS.intLayoutID = " & intLayoutID & " and vchSlotName = '" & vchItemSlotName & "')"
	strSQL = strSQL & " order by I.vchItemName"
	'response.write strSQL
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()
	response.end

end sub

sub loadItemDetails

	dim intItemID
	
	intItemID = request("intItemID")
	OpenConn
	strSQL = "select vchItemName, vchSize, vchColorProcess, vchSubstrate, vchNumSides "
	strSQL = strSQL & " from " & STR_TABLE_INVENTORY & " "
	strSQL = strSQL & " where intID = " & intItemID
	'response.write strSQL
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()
	response.end

end sub

sub saveItemSlot

	dim intCampaignID, intLayoutID, vchItemSlotName, intProfileID, intOldItemID, intItemID, rsSlotID, rsCheckSlot
	
	intCampaignID = request("campaignID")
	intLayoutID = request("intLayoutID")
	vchItemSlotName = request("vchSlotName")
	intProfileID = request("intProfileID")
	intItemID = request("intItemID")
	
	OpenConn()
	
	strSQL = "select intID from " & STR_TABLE_LAYOUTS_SLOTS & " where intLayoutID = " & intLayoutID & " and vchSlotName = '" & vchItemSlotName & "'"
	set rsSlotID = gobjConn.execute(strSQL)
	
	if intItemID&"" <> "" then
		strSQL = "select intID from " & STR_TABLE_PROFILE_STORE_PACKAGE & " where intCampaignID = " & intCampaignID & " and intProfileID = " & intProfileID & " and intLayoutID = " & intLayoutID & " and intSlotID = " & rsSlotID("intID")
		set rsCheckSlot = gobjConn.execute(strSQL)
		
		if rsCheckSlot.EOF then
			'insert new record
			strSQL = "Insert Into " & STR_TABLE_PROFILE_STORE_PACKAGE & " (intCampaignID, intProfileID, intLayoutID, intSlotID, intItemID) "
			strSQL = strSQL & "Values (" & intCampaignID & "," & intProfileID & "," & intLayoutID & "," & rsSlotID("intID") & "," & intItemID & ")"
		else
			'replace item id
			strSQL = "Update " & STR_TABLE_PROFILE_STORE_PACKAGE & " set intItemID = " & intItemID & " where intID = " & rsCheckSlot("intID")
		end if
	else
		strSQL = "delete from " & STR_TABLE_PROFILE_STORE_PACKAGE & " where intCampaignID = " & intCampaignID & " and intProfileID = " & intProfileID & " and intLayoutID = " & intLayoutID & " and intSlotID = " & rsSlotID("intID")
	end if
	
	gobjConn.execute(strSQL)
	
	CloseConn()
	response.end

end sub

sub saveItemSlotMulti

	dim intCampaignID, intLayoutID, vchItemSlotName, intProfileID, intOldItemID, intItemID, rsSlotID, rsCheckSlot, rsStoreList
	
	intCampaignID = request("campaignID")
	intLayoutID = request("intLayoutID")
	vchItemSlotName = request("vchSlotName")
	intProfileID = request("intProfileID")
	intItemID = request("intItemID")
	
	OpenConn()
	
	strSQL = "select S.intLayout from " & STR_TABLE_PROFILE_MAPPER & " PM "
	strSQL = strSQL & " join " & STR_TABLE_SHOPPER & " S on PM.intStoreShopperID = S.intID "
	strSQL = strSQL & " where PM.intProfileID = " & intProfileID & " and S.intLayout is not null"
	set rsStoreList =  gobjConn.execute(strSQL)
	
	while not rsStoreList.EOF
		intLayoutID = rsStoreList("intLayout")&""
		if intLayoutID <> "" then
			strSQL = "select intID from " & STR_TABLE_LAYOUTS_SLOTS & " where intLayoutID = " & intLayoutID & " and vchSlotName = '" & vchItemSlotName & "'"
			set rsSlotID = gobjConn.execute(strSQL)
		
			if not rsSlotID.eof then
				if intItemID&"" <> "" then
					strSQL = "select intID from " & STR_TABLE_PROFILE_STORE_PACKAGE & " where intCampaignID = " & intCampaignID & " and intProfileID = " & intProfileID & " and intLayoutID = " & intLayoutID & " and intSlotID = " & rsSlotID("intID")
					set rsCheckSlot = gobjConn.execute(strSQL)
					
					if rsCheckSlot.EOF then
						'insert new record
						strSQL = "Insert Into " & STR_TABLE_PROFILE_STORE_PACKAGE & " (intCampaignID, intProfileID, intLayoutID, intSlotID, intItemID) "
						strSQL = strSQL & "Values (" & intCampaignID & "," & intProfileID & "," & intLayoutID & "," & rsSlotID("intID") & "," & intItemID & ")"
					else
						'replace item id
						strSQL = "Update " & STR_TABLE_PROFILE_STORE_PACKAGE & " set intItemID = " & intItemID & " where intID = " & rsCheckSlot("intID")
					end if
				else
					strSQL = "delete from " & STR_TABLE_PROFILE_STORE_PACKAGE & " where intCampaignID = " & intCampaignID & " and intProfileID = " & intProfileID & " and intLayoutID = " & intLayoutID & " and intSlotID = " & rsSlotID("intID")
				end if
			
				gobjConn.execute(strSQL)
			end if
		end if
		rsStoreList.MoveNext
	wend
	
	CloseConn()
	response.end

end sub

function CreateInventoryItem(vchItemName, intCampaignID, intLayoutID,vchItemSlotName,intOldItemID, vchNumSides)
	dim arrFixtureInitials, strFixtureInitials, strCampaignName, vchImage, intInvID, strSKU, rsTemp, dctSaveList, intFixtureId, x
    strSKU = ""
	strFixtureInitials = ""

    strSQL = "SELECT vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intCampaignID
    'response.write strSQL
	set rsTemp = gobjConn.execute(strSQL)
    strCampaignName =  rsTemp(0) & ""

    strSQL = "SELECT I.vchItemName, I.vchImageURL, I.intID FROM " & STR_TABLE_INVENTORY & " I WHERE I.intID= ( select LS.intItemID from " & STR_TABLE_LAYOUTS_SLOTS & " LS where LS.intLayoutID = " & intLayoutID & " and vchSlotName = '" & vchItemSlotName & "')"
    'response.write strSQL
	set rsTemp = gobjConn.execute(strSQL)
	
	vchImage = rsTemp("vchImageURL")
	intFixtureId = rsTemp("intID")
	arrFixtureInitials = split(rsTemp("vchItemName")&"", " ")
	
	For Each x In arrFixtureInitials
		strFixtureInitials = strFixtureInitials & left(x,1)
	Next
	
    strSKU = strFixtureInitials & "_" & vchItemName & "_" & strCampaignName
	'response.write "<br />" &strSKU
	
	if intOldItemID <>"null" then
	
		strSQL = "update " & STR_TABLE_INVENTORY & " set dtmUpdated = GETDATE(), vchUpdatedByUser = '" & Session(SESSION_MERCHANT_ULOGIN) & "', vchUpdatedByIP = '" & gstrUserIP & "', vchItemName = '" & SQLEncode(vchItemName) & "', vchNumSides = '" & vchNumSides & "', vchPartNumber = '" & strSKU & "' where intID = " & intOldItemID
		'response.write strSQL
		gobjConn.execute(strSQL)
	
		intInvID = intOldItemID
	
	else
	
		strSQL = "SELECT intID, vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE vchPartNumber='" & strSKU & "'"

		set rsTemp = gobjConn.execute(strSQL)
		
		'response.end
		if rsTemp.EOF then
			set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "*@dtmCreated", "GETDATE()"
			dctSaveList.Add "@dtmUpdated", "GETDATE()"
			dctSaveList.Add "*vchCreatedByUser", Session(SESSION_MERCHANT_ULOGIN)
			dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
			dctSaveList.Add "*vchCreatedByIP", gstrUserIP
			dctSaveList.Add "vchUpdatedByIP", gstrUserIP
			dctSaveList.Add "chrType", "I"
			dctSaveList.Add "*chrStatus", "A"
			dctSaveList.Add "*#intParentID", intFixtureId
			dctSaveList.Add "*#intSortOrder", 0
			dctSaveList.Add "*#intHitCount", 0
			dctSaveList.Add "vchItemName", vchItemName
			dctSaveList.Add "vchSize", Request("vchSize")
			dctSaveList.Add "vchNumSides", Request("vchNumSides")
			dctSaveList.Add "vchSubstrate", Request("vchSubstrate")
			dctSaveList.Add "vchPartNumber", strSKU
			dctSaveList.Add "vchImageURL", vchImage
			dctSaveList.Add "#mnyItemPrice", "0"
			dctSaveList.Add "chrICFlag", "N"
			dctSaveList.Add "chrTaxFlag", "N"
			dctSaveList.Add "chrSoftFlag", "N"
			dctSaveList.Add "!#intStock", "0"
			dctSaveList.Add "!txtDescription", ""
			intInvID = SaveDataRecord(STR_TABLE_INVENTORY, Request, intInvID, dctSaveList)
		else 
			intInvID = rsTemp("intID")
		end if
	end if
	CreateInventoryItem = intInvID
end function

sub loadStoreList

	dim intProfileID, intCampaignID, rsStoreList, rsLayoutCount, rsItemsCount, intDenominator, intNumerator, rsFixtures, totalNumerator, totalDenominator, output, count
	
	intProfileID = request("intProfileID")
	intCampaignID = request("intCampaignID")
	
	strSQL = "Select S.intID as intID,  "
	strSQL = strSQL & " S.vchFirstName as storeName, S.vchLastName as DisplayName, S.vchCity as City, S.vchState as State, S.intLayout "
	strSQL = strSQL & " from " & STR_TABLE_PROFILE_MAPPER & " PM "
	strSQL = strSQL & " join " & STR_TABLE_SHOPPER & " S on PM.intStoreShopperID = S.intID "
	strSQL = strSQL & " where PM.intProfileID =  " & intProfileID & " and S.intLayout is not null"
	
	'response.write "store list: " & strSQL &";<br /><br />"

	'{"intID":6,"percentage":"1000%","storeName":"Store 020","City":"PineTown","State":"CA"}
	
	OpenConn
	'Set cmd = Server.CreateObject("ADODB.Command")
	'cmd.CommandText = strSQL
	'arParams = array("")
	'Set cmd.ActiveConnection = gobjConn
	'QueryToJSON(cmd, arParams).Flush
	'if true = false then
	output = "["
	totalDenominator = 0
	totalNumerator = 0
	count = 0
		set rsStoreList = gobjConn.execute(strSQL)
		
		strSQL = "select intID, vchItemName from " & STR_TABLE_INVENTORY & " where intParentID = 1 and chrStatus = 'A' and intid in (select distinct I.intparentid from " & STR_TABLE_INVENTORY & " I join " & STR_TABLE_PROFILE_PACKAGES & " PP on PP.intItemID = I.intID and PP.intProfileID= " & intProfileID & " and PP.intCampaignID = " & intCampaignID & ") "
		'response.write "fixture sql" & strSQL &";<br /><br />"
		'response.write strSQL
		set rsFixtures = gobjConn.execute(strSQL)
		while not rsStoreList.EOF
			
			'get available fixture types from package
			
			
			strSQL = "select Count(intID) as count from " & STR_TABLE_LAYOUTS_SLOTS & " where intLayoutID = " & rsStoreList("intLayout")
			'response.write "layout count sql: " & strSQL &";<br /><br />"
			set rsLayoutCount = gobjConn.execute(strSQL)
			while not rsFixtures.EOF
				
				
				strSQL = "select count(intParentID) as count from " & STR_TABLE_INVENTORY & " I right join " & STR_TABLE_PROFILE_PACKAGES & " PP on PP.intItemID = I.intID where I.intParentID = " & rsFixtures("intID") & " and PP.intProfileID = " & intProfileID & " and PP.intCampaignID = " & intCampaignID & ";"
				strSQL = strSQL & " select count(intParentID) as count from " & STR_TABLE_INVENTORY & " I join " & STR_TABLE_PROFILE_STORE_PACKAGE &" PP on PP.intItemID = I.intID where PP.intLayoutID = " & rsStoreList("intLayout") & " and I.intParentID = " & rsFixtures("intID") & " and PP.intProfileID = " & intProfileID & " and PP.intCampaignID = " & intCampaignID & ";"
				'response.write "item count sql: " & strSQL & "<br /><br />"
				set rsItemsCount = gobjConn.execute(strSQL)
				
				'compare how many possible slots the store can accomidate for the fixture vs how many items were created for the campaignID
				'use the lesser of the 2 to have as the maximum fixtures to assign
				if rsFixtures("intID")&"" = "2" then
					if clng(rsLayoutCount("count")) > clng(rsItemsCount("count")) then
						intDenominator = clng(rsItemsCount("count"))
					else
						intDenominator = clng(rsLayoutCount("count"))
					end if
				else
					intDenominator = clng(rsItemsCount("count"))
				end if
				set rsItemsCount = rsItemsCount.NextRecordset
				
				'set rsItemsCount = gobjConn.execute(strSQL)
				intNumerator = clng(rsItemsCount("count"))
								
				if intNumerator > intDenominator then
					intDenominator = intNumerator
				end if
				
				totalDenominator = totalDenominator + intDenominator
				totalNumerator = totalNumerator + intNumerator
				rsFixtures.MoveNext
			wend
			if count <> 0 then
				output = output + ","
			end if
			'response.write totalNumerator & "/" & totalDenominator
			output = output + "{""intID"": """ & rsStoreList("intID") & """, ""storeName"": """ & rsStoreList("storeName") & """, ""DisplayName"": """ & rsStoreList("DisplayName") & """, City"": """ & rsStoreList("City") & """, ""State"": """ & rsStoreList("State") & """, ""intLayout"": """ & rsStoreList("intLayout") & """, ""percentage"": "
			if totalNumerator = 0 and totalDenominator = 0 then 
				output = output & """0%""" 
			else 
				output = output & """" & FormatPercent(totalNumerator/totalDenominator, 0) & """"
			end if 
			output = output & ",""Numerator"": " & totalNumerator & ", ""Denominator"": " & totalDenominator
			output = output & "}"
			totalDenominator = 0
			totalNumerator = 0
			count = count + 1
			rsFixtures.MoveFirst
			rsStoreList.MoveNext
		wend
	'end if
	output = output + "]"
	
	response.write output
	
	CloseConn()
	
end sub

sub MailerDetails

	dim recno, strSql, cmd, arParams
	
	recno = request("recno")
	
	strSQL = "select [full name] from " & mainTable & "  where [recno] = " & recno
	
	OpenConn
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()

end sub

sub getColorProcesses
	'dim strSQL, rsColorProcesses, vcOpt4
	dim strJSONoutput
	
	
	OpenConn()
	strSQL = "SELECT [intID],[vchOptionList4] FROM " & STR_TABLE_INVENTORY & " WHERE [intID]=" & request("fixture")
	'set rsColorProcesses = gobjConn.execute(strSQL)
	strJSONoutput = "["
	if cint(request("sides"))=1 then
		strJSONoutput = strJSONoutput & "{""name"": ""4CP/0""},"
		strJSONoutput = strJSONoutput & "{""name"": ""K/0""}"
	else
		strJSONoutput = strJSONoutput & "{""name"": ""4CP/4CP""},"
		strJSONoutput = strJSONoutput & "{""name"": ""K/K""}"
	end if
	
	
	
	'count = 0
	'while not rsColorProcesses.eof
		'vcOpt4 = Split(rsColorProcesses("vchOptionList4"), ",")
		'innercount = 0
		'for each x in vcOpt4
			'if innercount <> 0 then
				'strJSONoutput = strJSONoutput & ","
			'end if
			'if cint(request("sides"))=1 then
				'strJSONoutput = strJSONoutput & "{""name"": ""4CP/0""},"
				'strJSONoutput = strJSONoutput & "{""name"": ""K/0""}"
			'else
				'strJSONoutput = strJSONoutput & "{""name"": ""4CP/4CP""},"
				'strJSONoutput = strJSONoutput & "{""name"": ""K/K""}"
			'end if
			'innercount = innercount + 1
		'next
		
		'count = count + 1
		'rsColorProcesses.MoveNext()
	'wend
	strJSONoutput = strJSONoutput & "]"
	response.write strJSONoutput
	
	CloseConn()
end sub

sub OpenConn()
	if not gblnConnOpen then
		set gobjConn = Server.CreateObject("ADODB.Connection")
		gobjConn.Open gstrConnectString
		gblnConnOpen = true
	end if
end sub

sub CloseConn()
	if gblnConnOpen and IsObject(gobjConn) then
		gobjConn.Close
		set gobjConn = nothing
		gblnConnOpen = false
	end if
end sub

sub ConnExecute(strSQL)
	OpenConn
	'response.write strsql
	gobjConn.execute(strSQL)
end sub

function ConnOpenRS(strSQL)
	OpenConn
	set ConnOpenRS = gobjConn.execute(strSQL)
end function

function formatDate(dateToFormat)
	dim newDate, splitDate
	
	splitDate = Split(dateToFormat, "-")
	newDate = splitDate(1) & "-" & splitDate(2) & "-" & splitDate(0)
	
	formatDate = newDate
end function

Function URLDecode(sConvert)
    Dim aSplit
    Dim sOutput
    Dim I
    If IsNull(sConvert) Then
       URLDecode = ""
       Exit Function
    End If

    ' convert all pluses to spaces
    sOutput = REPLACE(sConvert, "+", " ")

    ' next convert %hexdigits to the character
    aSplit = Split(sOutput, "%")

    If IsArray(aSplit) Then
      sOutput = aSplit(0)
      For I = 0 to UBound(aSplit) - 1
        sOutput = sOutput & _
          Chr("&H" & Left(aSplit(i + 1), 2)) &_
          Right(aSplit(i + 1), Len(aSplit(i + 1)) - 2)
      Next
    End If

    URLDecode = sOutput
End Function

' -------------- SQLEncode -----------------
' Replaces Single Quotes with 2 Single Quotes in Input String
Function SQLEncode(strInput)
	SQLEncode = Replace(strInput,"'","''")
End Function
%>