<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%
OpenConn
Dim Pdf, Doc, Filename, intCatalogID, intCampaignID, strPdfFile

intCatalogID =  Request("CatalogId")&""

if intCatalogID&""="" Then
    intCatalogID = 0
elseif intCatalogID&""="all" Then
    intCatalogID = 0
Else
    intCatalogID = CINT(intCatalogID)
End If

intCampaignID =  Request("campaignid")&""

if intCampaignID&""="" Then
    intCampaignID = 0
Else
    intCampaignID = CINT(intCampaignID)
End If

' mred: consolidated PDF file location (todo: move to incInit)
strPdfFile = Server.MapPath(intCatalogID & ".pdf")

Set Pdf = Server.CreateObject("Persits.Pdf")

' Create empty document
Set Doc = Pdf.CreateDocument
'Response.Write DrawPrint
'response.end
Doc.ImportFromUrl DrawPrint

'dim fs
'Set fs=Server.CreateObject("Scripting.FileSystemObject")
'if fs.FileExists(strPdfFile) Then
'fs.DeleteFile(strPdfFile)
'set fs=nothing
'end if

Filename = Doc.Save( strPdfFile, True ) 
CloseConn

' mred: explicit object close
Set Pdf = Nothing
Set Doc = Nothing

' mred: trying redirect to PDF
response.redirect intCatalogID & ".pdf"
response.end

Response.ContentType = "application/pdf"
Set objStream = Server.CreateObject("ADODB.Stream")
objStream.Open
objStream.Type = adTypeBinary
objStream.LoadFromFile strPdfFile

Response.BinaryWrite objStream.Read

objStream.Close
Set objStream = Nothing
Response.End

Function DrawPrint
DrawPrint = ""
DrawPrint = DrawPrint & "<html>" & VbCrLf
DrawPrint = DrawPrint & "<head>" & VbCrLf
DrawPrint = DrawPrint & "</head>" & VbCrLf
DrawPrint = DrawPrint & "<body>" & VbCrLf
DrawPrint = DrawPrint & "<center>" & VbCrLf
DrawPrint = DrawPrint & "<style>" & VbCrLf
DrawPrint = DrawPrint & "    table {text-align:left;}" & VbCrLf
DrawPrint = DrawPrint & "    .title {text-align:center;font-size:larger;font-weight:bold;}" & VbCrLf
DrawPrint = DrawPrint & "    .label {font-weight:bold;color:#000;display:inline-block;margin-right:15px;border:0px !important;}" & VbCrLf
DrawPrint = DrawPrint & "    .data {display:inline-block;padding-right:15px;padding-left:10px;}" & VbCrLf
DrawPrint = DrawPrint & "    .last td {border-bottom:1px solid #000;} .last td table td {border:0px;}" & VbCrLf
DrawPrint = DrawPrint & "    .page-break {page-break-before:always;}" & VbCrLf
DrawPrint = DrawPrint & "    .image {margin:10px;width:100px;height:100px;background-repeat:no-repeat;overflow:hidden;}" & VbCrLf
DrawPrint = DrawPrint & "    .part-number {background-color:#ccc;color:#fff;}" & VbCrLf
DrawPrint = DrawPrint & "    td {vertical-align:middle;} .item {page-break-inside: avoid;}" & VbCrLf
DrawPrint = DrawPrint & "@media print" & VbCrLf
DrawPrint = DrawPrint & "{" & VbCrLf
DrawPrint = DrawPrint & "    .no-print {display:none;}   " & VbCrLf
DrawPrint = DrawPrint & "}" & VbCrLf
DrawPrint = DrawPrint & "</style>" & VbCrLf


    Dim intCatalogID, strSQL,rsTemp,strPhotoURL, currentBrand

    If request("CatalogId")&""="all" Then
        strSQL = "SELECT TOP 1 * FROM HBO_GoDoc WHERE STitle='Cover Page Text'"
    else
        strSQL = "SELECT TOP 1 * FROM HBO_GoDoc WHERE STitle='Cover Page Text'"
    end if

    set rsTemp = gobjConn.execute(strSQL)
	If not rsTemp.EOF Then
		DrawPrint = DrawPrint & "<div>" & rsTemp("description") & "</div>" & VbCrLf    
	End If 
	set rsTemp = Nothing

    If request("CatalogId")&""="all" Then
        strSQL = "SELECT B.vchItemName as BrandName, P.vchItemName as ProgramName, C.vchItemName as CategoryName,I.vchBundleQuantity, I.mnyItemPrice, I.vchImageURL, I.vchExpireDate, I.vchPartNumber,"
        strSQL = strSQL & "I.vchValidStates,I.vchItemName as InvItemName,I.txtDescription,I.txtShortDescription  FROM " & STR_TABLE_INVENTORY & " I " 
        strSQL = strSQL & "LEFT JOIN " & STR_TABLE_INVENTORY & " B ON I.intBrand = B.intID  "
        strSQL = strSQL & "LEFT JOIN " & STR_TABLE_INVENTORY & " C ON I.intCategory = C.intID  "    
        strSQL = strSQL & "LEFT JOIN " & STR_TABLE_INVENTORY & " CM ON I.intCampaign = CM.intID  "
        strSQL = strSQL & "INNER JOIN " & STR_TABLE_INVENTORY & " P ON I.intProgram = P.intID  "
        strSQL = strSQL & "WHERE  I.chrStatus='A' and I.chrType='I' and I.vchPartNumber!='' "
        If intCampaignID > 0 Then
            strSQL = strSQL & "AND  I.intCampaign=" & intCampaignID & " "
        End If
        strSQL = strSQL & "ORDER BY B.vchItemName,I.vchPartNumber "

        set rsTemp = gobjConn.execute(strSQL)
    else
        strSQL = "SELECT B.vchItemName as BrandName, P.vchItemName as ProgramName, C.vchItemName as CategoryName,I.vchBundleQuantity, I.mnyItemPrice, I.vchImageURL, I.vchExpireDate, I.vchPartNumber, I.vchValidStates,I.vchItemName as InvItemName,I.txtDescription,I.txtShortDescription  FROM " & STR_TABLE_LINEITEM & " L "
	    strSQL = strSQL & "INNER JOIN " & STR_TABLE_INVENTORY & " I ON  L.intInvId = I.intID " 
        strSQL = strSQL & "LEFT JOIN " & STR_TABLE_INVENTORY & " B ON I.intBrand = B.intID  "
        strSQL = strSQL & "LEFT JOIN " & STR_TABLE_INVENTORY & " C ON I.intCategory = C.intID  "    
        strSQL = strSQL & "LEFT JOIN " & STR_TABLE_INVENTORY & " CM ON I.intCampaign = CM.intID  "
        strSQL = strSQL & "LEFT JOIN " & STR_TABLE_INVENTORY & " P ON I.intProgram = P.intID  "
        strSQL = strSQL & "WHERE  L.intOrderID=" &  Request("CatalogId")&" " 
        If intCampaignID > 0 Then
            strSQL = strSQL & "AND  I.intCampaign=" & intCampaignID & " "
        End If
        strSQL = strSQL & "ORDER BY B.vchItemName,I.vchPartNumber "
        'response.write strSQL
        set rsTemp = gobjConn.execute(strSQL)
    end if
    'response.Write strSQL

	DrawPrint = DrawPrint & "<br style=""page-break-before: always"" >"

    while not rsTemp.eof

        Dim vchPartNumber,BrandName, ProgramName, txtShortDescription, vchValidStates, mnyItemPrice, vchBundleQuantity, vchItemName, vchExpireDate, txtDescription
        txtDescription = rsTemp("txtDescription")        
        txtShortDescription = rsTemp("txtShortDescription")
        
        vchBundleQuantity = rsTemp("vchBundleQuantity")
        BrandName = rsTemp("BrandName")
        mnyItemPrice = rsTemp("mnyItemPrice")
		vchItemName = rsTemp("InvItemName")
        
        vchExpireDate = rsTemp("vchExpireDate") 
        vchPartNumber =  rsTemp("vchPartNumber")
        ProgramName = rsTemp("ProgramName")
        vchValidStates = rsTemp("vchValidStates")
       
	' mred: refactored if
	if rsTemp("vchImageURL")&""="" then
		'strPhotoURL = encodeBase64(readBytes(Server.MapPath( "../images/icon-no-image-512.png" )))
		strPhotoURL = "http://HBO.drcportal.com/images/icon-no-image-512.png"
	else
	        'strPhotoURL = "data:image/jpg;base64," & encodeBase64(readBytes(Server.MapPath("../images/products/" & rsTemp("vchImageURL")&"")))
	        strPhotoURL = "http://HBO.drcportal.com/images/products/big/" & rsTemp("vchImageURL")
	end if
       
        If BrandName<> currentBrand Then 
            If currentBrand&""<>"" Then
                DrawPrint = DrawPrint & "</table>" & VbCrLf
            End If 

            DrawPrint = DrawPrint & "<table class=""page-break"" cellpadding=""3"" cellspacing=""3"">       " & VbCrLf     
            DrawPrint = DrawPrint & "    <tr>" & VbCrLf
            DrawPrint = DrawPrint & "        <td colspan=""3"" class=""title"">" & rsTemp("BrandName") & "</td>" & VbCrLf
            DrawPrint = DrawPrint & "    </tr>" & VbCrLf
            DrawPrint = DrawPrint & "    <tr>" & VbCrLf
            DrawPrint = DrawPrint & "        <td colspan=""3"" class=""title"">&nbsp;</td>" & VbCrLf
            DrawPrint = DrawPrint & "    </tr>" & VbCrLf
            currentBrand =BrandName 
         End If
          DrawPrint = DrawPrint & "    <tr>" & VbCrLf
            DrawPrint = DrawPrint & "        <td colspan=""3"" valign=""top"" align=""left"" class=""item""><table width=""100%""" & VbCrLf
			
			DrawPrint = DrawPrint & "    <tr>" & VbCrLf
        DrawPrint = DrawPrint & "        <td rowspan=""4"" width=""100""><div class=""image"" style=""overflow:hidden;height:100px;""><img src=""" & strPhotoURL & """ width=""100"" /></div></td><td class=""label"">Item Name:</td><td >" &  vchItemName & "</td>" & VbCrLf
        DrawPrint = DrawPrint & "    </tr>" & VbCrLf
            
        DrawPrint = DrawPrint & "    <tr>" & VbCrLf
        DrawPrint = DrawPrint & "       <td class=""label"">Part Number:</td><td class=""part-number"">" &  vchPartNumber & "</td>" & VbCrLf
        DrawPrint = DrawPrint & "    </tr>" & VbCrLf
        DrawPrint = DrawPrint & "   <tr>" & VbCrLf
        DrawPrint = DrawPrint & "        <td class=""label"">Size:</td><td>" &  ProgramName & "</td>" & VbCrLf
        DrawPrint = DrawPrint & "    </tr>" & VbCrLf
        'DrawPrint = DrawPrint & "    <tr>" & VbCrLf
        'DrawPrint = DrawPrint & "        <td class=""label"">Short Desc:</td><td>" &  txtShortDescription & "</td>" & VbCrLf
        'DrawPrint = DrawPrint & "    </tr>" & VbCrLf
        'DrawPrint = DrawPrint & "    <tr class=""last"">" & VbCrLf
        'DrawPrint = DrawPrint & "        <td class=""label"">Description</td><td>" & txtDescription & "</td>" & VbCrLf
        'DrawPrint = DrawPrint & "    </tr>" & VbCrLf
       ' DrawPrint = DrawPrint & "    <tr>" & VbCrLf
       ' DrawPrint = DrawPrint & "        <td colspan=""3""><span class=""label"">Valid States:</span><span class=""data"">" &  vchValidStates & "</span></td>" & VbCrLf
       ' DrawPrint = DrawPrint & "    </tr>" & VbCrLf
        'DrawPrint = DrawPrint & "    <tr class=""last"">" & VbCrLf
        'DrawPrint = DrawPrint & "        <td colspan=""3""><table cellpadding=""1""><tr><td class=""label"">Cost / Bundle:</td><td class=""data"">" &  FormatCurrency(mnyItemPrice) & "</td><td class=""label"">Bundle Qty:</td><td class=""data"">" &  vchBundleQuantity & "</td><td class=""label"">Expire Date:</td><td class=""data"">" &  vchExpireDate & "</td></tr></table> </td>" & VbCrLf
        'DrawPrint = DrawPrint & "    </tr>" & VbCrLf
       DrawPrint = DrawPrint & " </table>   </td></tr>" & VbCrLf
       
        DrawPrint = DrawPrint & "    <tr>" & VbCrLf
            DrawPrint = DrawPrint & "        <td colspan=""3"" class=""title"">&nbsp;</td>" & VbCrLf
            DrawPrint = DrawPrint & "    </tr>" & VbCrLf

        rsTemp.MoveNext
    wend
    DrawPrint = DrawPrint & "</table>" & VbCrLf
 DrawPrint = DrawPrint & "</center>" & VbCrLf   
 DrawPrint = DrawPrint & "</body>" & VbCrLf   
 DrawPrint = DrawPrint & "</html>" & VbCrLf
End Function

private function readBytes(file)
    dim inStream
    ' ADODB stream object used
    set inStream = Server.CreateObject("ADODB.Stream")
    ' open with no arguments makes the stream an empty container
    inStream.Open
    inStream.type= 1 
    'Response.Write file
    inStream.LoadFromFile(file)
    readBytes = inStream.Read()
  end function

  private function encodeBase64(bytes)
    dim DM, EL
    Set DM = Server.CreateObject("Microsoft.XMLDOM")
    ' Create temporary node with Base64 data type
    Set EL = DM.createElement("tmp")
    EL.DataType = "bin.base64"
    ' Set bytes, get encoded String
    EL.NodeTypedValue = bytes
    encodeBase64 =  Replace(Replace(Replace(Replace(EL.Text,vbNewLine,""),vbCrLf,""), vbCR,""), vbLF,"")
  end function

  private function decodeBase64(base64)
    dim DM, EL
    Set DM = Server.CreateObject("Microsoft.XMLDOM")
    ' Create temporary node with Base64 data type
    Set EL = DM.createElement("tmp")
    EL.DataType = "bin.base64"
    ' Set encoded String, get bytes
    EL.Text = base64
    decodeBase64 = EL.NodeTypedValue
  end function

  private Sub writeBytes(file, bytes)
    Dim binaryStream
    Set binaryStream = Server.CreateObject("ADODB.Stream")
    binaryStream.Type = adTypeBinary
    'Open the stream and write binary data
    binaryStream.Open
    binaryStream.Write bytes
    'Save binary data to disk
    binaryStream.SaveToFile file, adSaveCreateOverWrite
  End Sub

%>