<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%
OpenConn
Dim Pdf, Doc, Filename, intCatalogID, strPdfFile

intCatalogID =  Request("CatalogId")&""

if intCatalogID&""="" Then
    intCatalogID = 0
Else
    intCatalogID = CINT(intCatalogID)
End If

' mred: consolidated PDF file location (todo: move to incInit)
strPdfFile = Server.MapPath(intCatalogID & ".pdf")

Set Pdf = Server.CreateObject("Persits.Pdf")

' Create empty document
Set Doc = Pdf.CreateDocument
'Response.Write DrawPrint
Doc.ImportFromUrl DrawPrint

'dim fs
'Set fs=Server.CreateObject("Scripting.FileSystemObject")
'if fs.FileExists(strPdfFile) Then
'fs.DeleteFile(strPdfFile)
'set fs=nothing
'end if

Filename = Doc.Save( strPdfFile, True ) 
CloseConn

' mred: explicit object close
Set Pdf = Nothing
Set Doc = Nothing

' mred: trying redirect to PDF
response.redirect intCatalogID & ".pdf"
response.end

Response.ContentType = "application/pdf"
Set objStream = Server.CreateObject("ADODB.Stream")
objStream.Open
objStream.Type = adTypeBinary
objStream.LoadFromFile strPdfFile

Response.BinaryWrite objStream.Read

objStream.Close
Set objStream = Nothing
Response.End

Function DrawPrint
DrawPrint = ""
DrawPrint = DrawPrint & "<html>" & VbCrLf
DrawPrint = DrawPrint & "<head>" & VbCrLf
DrawPrint = DrawPrint & "</head>" & VbCrLf
DrawPrint = DrawPrint & "<body>" & VbCrLf
DrawPrint = DrawPrint & "<center>" & VbCrLf
DrawPrint = DrawPrint & "<style>" & VbCrLf
DrawPrint = DrawPrint & "    table {text-align:left;}" & VbCrLf
DrawPrint = DrawPrint & "    .title {text-align:center;font-size:larger;font-weight:bold;}" & VbCrLf
DrawPrint = DrawPrint & "    .label {font-weight:bold;color:#000;display:inline-block;margin-right:15px;border:0px !important;}" & VbCrLf
DrawPrint = DrawPrint & "    .data {display:inline-block;padding-right:15px;padding-left:10px;}" & VbCrLf
DrawPrint = DrawPrint & "    .last td {border-bottom:1px solid #000;} .last td table td {border:0px;}" & VbCrLf
DrawPrint = DrawPrint & "    .page-break {page-break-before:always;}" & VbCrLf
DrawPrint = DrawPrint & "    .image {padding:10px;}" & VbCrLf
DrawPrint = DrawPrint & "    .part-number {background-color:#ccc;color:#fff;}" & VbCrLf
DrawPrint = DrawPrint & "    td {vertical-align:middle;}" & VbCrLf
DrawPrint = DrawPrint & "@media print" & VbCrLf
DrawPrint = DrawPrint & "{" & VbCrLf
DrawPrint = DrawPrint & "    .no-print {display:none;}   " & VbCrLf
DrawPrint = DrawPrint & "}" & VbCrLf
DrawPrint = DrawPrint & "</style>" & VbCrLf


    Dim intCatalogID, strSQL,rsTemp,strPhotoURL, currentBrand
    strSQL = "SELECT TOP 1 * FROM poddemo_GoDoc WHERE STitle='Cover Page Text'"
    set rsTemp = gobjConn.execute(strSQL)
	If not rsTemp.EOF Then
		DrawPrint = DrawPrint & "<div>" & rsTemp("description") & "</div>" & VbCrLf    
	End If 
	set rsTemp = Nothing

    strSQL = "SELECT B.vchItemName as BrandName, P.vchItemName as ProgramName, C.vchItemName as CategoryName,I.vchBundleQuantity, I.mnyItemPrice, I.vchImageURL, I.vchExpireDate, I.vchPartNumber, I.vchValidStates,I.txtDescription,I.txtShortDescription  FROM " & STR_TABLE_LINEITEM & " L "
	strSQL = strSQL & "INNER JOIN " & STR_TABLE_INVENTORY & " I ON  L.intInvId = I.intID " 
    strSQL = strSQL & "LEFT JOIN " & STR_TABLE_INVENTORY & " B ON I.intBrand = B.intID  "
    strSQL = strSQL & "LEFT JOIN " & STR_TABLE_INVENTORY & " C ON I.intCategory = C.intID  "    
    strSQL = strSQL & "LEFT JOIN " & STR_TABLE_INVENTORY & " CM ON I.intCampaign = CM.intID  "
    strSQL = strSQL & "LEFT JOIN " & STR_TABLE_INVENTORY & " P ON I.intProgram = P.intID  "
    strSQL = strSQL & "WHERE  L.intOrderID=" &  Request("CatalogId")&" " 
    strSQL = strSQL & "ORDER BY B.vchItemName "

    'response.Write strSQL

    set rsTemp = gobjConn.execute(strSQL)

    while not rsTemp.eof

        Dim vchPartNumber,BrandName, ProgramName, txtShortDescription, vchValidStates, mnyItemPrice, vchBundleQuantity, vchExpireDate, txtDescription
        txtDescription = rsTemp("txtDescription")        
        txtShortDescription = rsTemp("txtShortDescription")
        
        vchBundleQuantity = rsTemp("vchBundleQuantity")
        BrandName = rsTemp("BrandName")
        mnyItemPrice = rsTemp("mnyItemPrice")
        
        vchExpireDate = rsTemp("vchExpireDate") 
        vchPartNumber =  rsTemp("vchPartNumber")
        ProgramName = rsTemp("ProgramName")
        vchValidStates = rsTemp("vchValidStates")
       
	' mred: refactored if
	if rsTemp("vchImageURL")&""="" then
		'strPhotoURL = encodeBase64(readBytes(Server.MapPath( "../images/icon-no-image-512.png" )))
		strPhotoURL = "http://http://clients.americanweb.com/poddemo/www/images/icon-no-image-512.png"
	else
	        'strPhotoURL = "data:image/jpg;base64," & encodeBase64(readBytes(Server.MapPath("../images/products/" & rsTemp("vchImageURL")&"")))
	        strPhotoURL = "http://clients.americanweb.com/poddemo/www/images/products/" & rsTemp("vchImageURL")
	end if
       
        If BrandName<> currentBrand Then 
            If currentBrand&""<>"" Then
                DrawPrint = DrawPrint & "</table>" & VbCrLf
            End If 

            DrawPrint = DrawPrint & "<table class=""page-break"" cellpadding=""3"" cellspacing=""3"">       " & VbCrLf     
            DrawPrint = DrawPrint & "    <tr>" & VbCrLf
            DrawPrint = DrawPrint & "        <td colspan=""3"" class=""title"">" & rsTemp("BrandName") & "</td>" & VbCrLf
            DrawPrint = DrawPrint & "    </tr>" & VbCrLf
            DrawPrint = DrawPrint & "    <tr>" & VbCrLf
            DrawPrint = DrawPrint & "        <td colspan=""3"" class=""title"">&nbsp;</td>" & VbCrLf
            DrawPrint = DrawPrint & "    </tr>" & VbCrLf
            currentBrand =BrandName 
         End If

        DrawPrint = DrawPrint & "    <tr>" & VbCrLf
        DrawPrint = DrawPrint & "        <td rowspan=""4"" class=""image""><img src="""  & strPhotoURL & """ width=""100"" /></td><td class=""label"">Part Number:</td><td class=""part-number"">" &  vchPartNumber & "</td>" & VbCrLf
        DrawPrint = DrawPrint & "    </tr>" & VbCrLf
        DrawPrint = DrawPrint & "   <tr>" & VbCrLf
        DrawPrint = DrawPrint & "        <td class=""label"">Program:</td><td>" &  ProgramName & "</td>" & VbCrLf
        DrawPrint = DrawPrint & "    </tr>" & VbCrLf
        DrawPrint = DrawPrint & "    <tr>" & VbCrLf
        DrawPrint = DrawPrint & "        <td class=""label"">Short Desc:</td><td>" &  txtShortDescription & "</td>" & VbCrLf
        DrawPrint = DrawPrint & "    </tr>" & VbCrLf
        DrawPrint = DrawPrint & "    <tr>" & VbCrLf
        DrawPrint = DrawPrint & "        <td class=""label"">Description</td><td>" & txtDescription & "</td>" & VbCrLf
        DrawPrint = DrawPrint & "    </tr>" & VbCrLf
        DrawPrint = DrawPrint & "    <tr>" & VbCrLf
        DrawPrint = DrawPrint & "        <td colspan=""3""><span class=""label"">Valid States:</span><span class=""data"">" &  vchValidStates & "</span></td>" & VbCrLf
        DrawPrint = DrawPrint & "    </tr>" & VbCrLf
        DrawPrint = DrawPrint & "    <tr class=""last"">" & VbCrLf
        DrawPrint = DrawPrint & "        <td colspan=""3""><table cellpadding=""1""><tr><td class=""label"">Cost / Bundle:</td><td class=""data"">" &  FormatCurrency(mnyItemPrice) & "</td><td class=""label"">Bundle Qty:</td><td class=""data"">" &  vchBundleQuantity & "</td><td class=""label"">Expire Date:</td><td class=""data"">" &  vchExpireDate & "</td></tr></table> </td>" & VbCrLf
        DrawPrint = DrawPrint & "    </tr>" & VbCrLf
        DrawPrint = DrawPrint & "    <tr>" & VbCrLf
            DrawPrint = DrawPrint & "        <td colspan=""3"" class=""title"">&nbsp;</td>" & VbCrLf
            DrawPrint = DrawPrint & "    </tr>" & VbCrLf

        rsTemp.MoveNext
    wend
    DrawPrint = DrawPrint & "</table>" & VbCrLf
 DrawPrint = DrawPrint & "</center>" & VbCrLf   
 DrawPrint = DrawPrint & "</body>" & VbCrLf   
 DrawPrint = DrawPrint & "</html>" & VbCrLf
End Function

private function readBytes(file)
    dim inStream
    ' ADODB stream object used
    set inStream = Server.CreateObject("ADODB.Stream")
    ' open with no arguments makes the stream an empty container
    inStream.Open
    inStream.type= 1 
    'Response.Write file
    inStream.LoadFromFile(file)
    readBytes = inStream.Read()
  end function

  private function encodeBase64(bytes)
    dim DM, EL
    Set DM = Server.CreateObject("Microsoft.XMLDOM")
    ' Create temporary node with Base64 data type
    Set EL = DM.createElement("tmp")
    EL.DataType = "bin.base64"
    ' Set bytes, get encoded String
    EL.NodeTypedValue = bytes
    encodeBase64 =  Replace(Replace(Replace(Replace(EL.Text,vbNewLine,""),vbCrLf,""), vbCR,""), vbLF,"")
  end function

  private function decodeBase64(base64)
    dim DM, EL
    Set DM = Server.CreateObject("Microsoft.XMLDOM")
    ' Create temporary node with Base64 data type
    Set EL = DM.createElement("tmp")
    EL.DataType = "bin.base64"
    ' Set encoded String, get bytes
    EL.Text = base64
    decodeBase64 = EL.NodeTypedValue
  end function

  private Sub writeBytes(file, bytes)
    Dim binaryStream
    Set binaryStream = Server.CreateObject("ADODB.Stream")
    binaryStream.Type = adTypeBinary
    'Open the stream and write binary data
    binaryStream.Open
    binaryStream.Write bytes
    'Save binary data to disk
    binaryStream.SaveToFile file, adSaveCreateOverWrite
  End Sub

%>