<%@ LANGUAGE="VBScript" %>

<!--#include file="../config/incInit.asp"-->
<%
OpenConn
Dim Pdf, Doc, Filename, intCatalogID, strPdfFile

intCatalogID =  Request("CatalogId")&""

if intCatalogID&""="" Then
    intCatalogID = 0
Else
    intCatalogID = CINT(intCatalogID)
End If

' mred: consolidated PDF file location (todo: move to incInit)
strPdfFile = Server.MapPath(intCatalogID & ".pdf")

Set Pdf = Server.CreateObject("Persits.Pdf")

' Create empty document
Set Doc = Pdf.CreateDocument
Doc.ImportFromUrl DrawPrint
'Response.Write DrawPrint

'dim fs
'Set fs=Server.CreateObject("Scripting.FileSystemObject")
'if fs.FileExists(Server.MapPath(intCatalogID & ".pdf")) Then
'fs.DeleteFile(Server.MapPath(intCatalogID & ".pdf"))
'end if
'set fs=nothing

Filename = Doc.Save( strPdfFile, True ) 
CloseConn
Response.Redirect intCatalogID & ".pdf"
'Call DownloadFile(intCatalogID & ".pdf")


Private Sub DownloadFile(file)
    '--declare variables
    Dim strAbsFile
    Dim strFileExtension
    Dim objFSO
    Dim objFile
    Dim objStream
    '-- set absolute file location
    strAbsFile = Server.MapPath(file)
    '-- create FSO object to check if file exists and get properties
    Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
    '-- check to see if the file exists
    If objFSO.FileExists(strAbsFile) Then
        Set objFile = objFSO.GetFile(strAbsFile)
        '-- first clear the response, and then set the appropriate headers
        Response.Clear
        '-- the filename you give it will be the one that is shown
        ' to the users by default when they save
        Response.AddHeader "Content-Disposition", "attachment; filename=" & objFile.Name
        Response.AddHeader "Content-Length", objFile.Size
        Response.ContentType = "application/octet-stream"
        Set objStream = Server.CreateObject("ADODB.Stream")
        objStream.Open
        '-- set as binary
        objStream.Type = 1
        Response.CharSet = "UTF-8"
        '-- load into the stream the file
        objStream.LoadFromFile(strAbsFile)
        '-- send the stream in the response
        Response.BinaryWrite(objStream.Read)
        objStream.Close
        Set objStream = Nothing
        Set objFile = Nothing
    Else 'objFSO.FileExists(strAbsFile)
        Response.Clear
        Response.Write("No such file exists.")
    End If
    Set objFSO = Nothing
End Sub

Function DrawPrint
Dim rsData, rsTemp,strSQL, mnySubtotal,mnyTotal, intCounter



DrawPrint = ""
DrawPrint = DrawPrint & "<!DOCTYPE html> " & VbNewLine
DrawPrint = DrawPrint & "<!--[if IE 8]> <html lang=""en"" class=""ie8 no-js""> <![endif]--> " & VbNewLine
DrawPrint = DrawPrint & "<!--[if IE 9]> <html lang=""en"" class=""ie9 no-js""> <![endif]--> " & VbNewLine
DrawPrint = DrawPrint & "<!--[if !IE]><!--> " & VbNewLine
DrawPrint = DrawPrint & "<html lang=""en"" class=""no-js""> " & VbNewLine
DrawPrint = DrawPrint & "<!--<![endif]--> " & VbNewLine
DrawPrint = DrawPrint & "<!-- BEGIN HEAD --> " & VbNewLine
DrawPrint = DrawPrint & "<head> " & VbNewLine
DrawPrint = DrawPrint & "<meta charset=""utf-8""/> " & VbNewLine
DrawPrint = DrawPrint & "<title>poddemo Resource Center | Dome Printing</title> " & VbNewLine
DrawPrint = DrawPrint & "<meta http-equiv=""X-UA-Compatible"" content=""IE=edge""> " & VbNewLine
DrawPrint = DrawPrint & "<meta content=""width=device-width, initial-scale=1"" name=""viewport""/> " & VbNewLine
DrawPrint = DrawPrint & "<link href=""https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"" rel=""stylesheet"" type=""text/css""/> " & VbNewLine
DrawPrint = DrawPrint & "<style type=""text/css""> " & VbNewLine
DrawPrint = DrawPrint & "body " & VbNewLine
DrawPrint = DrawPrint & "{ " & VbNewLine
DrawPrint = DrawPrint & "    font-family: 'Open Sans', sans-serif; " & VbNewLine
DrawPrint = DrawPrint & "    font-size:8pt; " & VbNewLine
DrawPrint = DrawPrint & "} " & VbNewLine
DrawPrint = DrawPrint & "@media screen " & VbNewLine
DrawPrint = DrawPrint & "{ " & VbNewLine
DrawPrint = DrawPrint & ".print, .print * {display:none !important;} " & VbNewLine
DrawPrint = DrawPrint & "} " & VbNewLine

DrawPrint = DrawPrint & ".header  {text-align:center;font-weight:bold;font-size:larger;} " & VbNewLine
DrawPrint = DrawPrint & "@media print { " & VbNewLine
DrawPrint = DrawPrint & "    body  " & VbNewLine
DrawPrint = DrawPrint & "    { " & VbNewLine
DrawPrint = DrawPrint & "        background-color: #fff !important; " & VbNewLine
DrawPrint = DrawPrint & "    } " & VbNewLine
DrawPrint = DrawPrint & "    .no-print, .no-print * {display:none !important;} " & VbNewLine
    
DrawPrint = DrawPrint & "    .page-break { page-break-before:always !important;} .page-break:first-child {page-break-before:none !important;} " & VbNewLine
    
    
    
DrawPrint = DrawPrint & "  .cartitem {page-break-inside: avoid;}  .cartitem-image, .cartitem-data {border-top:1px solid #000;padding-top:10px;padding-bottom:10px;} " & VbNewLine
    
DrawPrint = DrawPrint & "    .cartitem-data { padding-left:10px;vertical-align:top;}    " & VbNewLine
DrawPrint = DrawPrint & "} " & VbNewLine
DrawPrint = DrawPrint & "</style> " & VbNewLine
DrawPrint = DrawPrint & "<body class=""page-header-fixed login""> " & VbNewLine
     
set rsData = GetOrderLineItems_Other(intCatalogID)
strSQL = "SELECT S.vchLabel, S.vchLastName,S.vchFirstName, S.vchState, S.vchCity   FROM " & STR_TABLE_SHOPPER & " S INNER JOIN " & STR_TABLE_ORDER & " O ON O.intShipShopperID=S.intID WHERE O.intId=" & intCatalogID & " AND S.chrStatus='A'"
set rsTemp = gobjConn.execute(strSQL)


DrawPrint = DrawPrint & "       <div style=""margin-bottom:20px;margin-top:10px;""> " & VbNewLine
DrawPrint = DrawPrint & "	        <table border=""0"" cellpadding=""0"" cellspacing=""0""> " & VbNewLine
DrawPrint = DrawPrint & "	        <tr> " & VbNewLine
DrawPrint = DrawPrint & "                <td colspan=""2"" class=""header"" style=""font-size:11pt;"">" & rsTemp("vchLabel") & " | " & rsTemp("vchFirstName") & "&nbsp;" & rsTemp("vchLastName") & "|" & rsTemp("vchCity") & ", " & rsTemp("vchState") & "</td> " & VbNewLine
DrawPrint = DrawPrint & "            </tr>" & VbNewLine
DrawPrint = DrawPrint & "            <tr>" & VbNewLine
DrawPrint = DrawPrint & "                <td colspan=""2"">&nbsp;</td>" & VbNewLine
DrawPrint = DrawPrint & "            </tr>" & VbNewLine
            
	        mnySubtotal = 0
	       
	        while not rsData.eof
		     
DrawPrint = DrawPrint & "	        <tr class=""cartitem"">" & VbNewLine
DrawPrint = DrawPrint & "		        <td valign=""top"" class=""cartitem-image"">" & VbNewLine
                    dim strPhotoURL

                    strPhotoURL = "data:image/jpg;base64," & encodeBase64(readBytes(Server.MapPath("../images/products/" & rsData("vchImageURL")&"")))
                    if rsData("vchImageURL")&""="" then strPhotoURL = encodeBase64(readBytes(Server.MapPath( "../images/icon-no-image-512.png" )))

                      
                       
DrawPrint = DrawPrint & "                    <div style=""background-image:url(" &  strPhotoURL & ");margin-bottom:20px;width:100px;height:80px;background-repeat:no-repeat;background-size:cover;boder:1px solid #ccc;"">" & VbNewLine
DrawPrint = DrawPrint & "                <img src=""" &  strPhotoURL & """ width=""100"" />" & VbNewLine
DrawPrint = DrawPrint & "                     </div>" & VbNewLine
DrawPrint = DrawPrint & "                 </td>" & VbNewLine
DrawPrint = DrawPrint & "                <td valign=""top"" class=""cartitem-data"">" & VbNewLine
            
                    Dim vchPartNumber, vchProgram, vchItemName, txtDescription, mnyUnitPrice, vchValidStates, vchBundleQuantity, vchExpireDate
                    vchBundleQuantity = rsData("vchBundleQuantity")
                    vchExpireDate = rsData("vchExpireDate")
                    vchValidStates = rsData("vchValidStates")
                    vchPartNumber = rsData("vchPartNumber")
                    vchProgram = rsData("vchProgram")
                    vchItemName = rsData("vchItemName")
                    
	                mnyUnitPrice = rsData("mnyUnitPrice")
                    
                    
                    txtDescription = rsData("txtDescription")
             
	        
DrawPrint = DrawPrint & "                    <table cellpadding=""1"" cellspacing=""1"">" & VbNewLine
DrawPrint = DrawPrint & "                        <tr align=""left"">" & VbNewLine
DrawPrint = DrawPrint & "                            <td colspan=""2"" style=""font-weight:bold;font-size:larger;"">" & vchItemName & "</td>" & VbNewLine
DrawPrint = DrawPrint & "                        </tr>" & VbNewLine
DrawPrint = DrawPrint & "                        <tr>" & VbNewLine
DrawPrint = DrawPrint & "                            <td colspan=""2"">&nbsp;</td>" & VbNewLine
DrawPrint = DrawPrint & "                        </tr>" & VbNewLine
DrawPrint = DrawPrint & "                        <tr align=""left"">" & VbNewLine
DrawPrint = DrawPrint & "                            <td>Part Number:</td>" & VbNewLine
DrawPrint = DrawPrint & "                            <td>" & vchPartNumber & "</td>" & VbNewLine
DrawPrint = DrawPrint & "                        </tr>" & VbNewLine
DrawPrint = DrawPrint & "                        <tr align=""left"">" & VbNewLine
DrawPrint = DrawPrint & "                            <td>Program:</td>" & VbNewLine
DrawPrint = DrawPrint & "                            <td>" &  vchProgram & "</td>" & VbNewLine
DrawPrint = DrawPrint & "                        </tr>" & VbNewLine
                        
                        
DrawPrint = DrawPrint & "                         <tr align=""left"">" & VbNewLine
DrawPrint = DrawPrint & "                            <td>Cost/Bundle:</td>" & VbNewLine
DrawPrint = DrawPrint & "                            <td>" & FormatCurrency(mnyUnitPrice) & "</td>" & VbNewLine
DrawPrint = DrawPrint & "                        </tr>" & VbNewLine
DrawPrint = DrawPrint & "                       <tr align=""left"">" & VbNewLine
DrawPrint = DrawPrint & "                            <td>Bundle Quantity:</td>" & VbNewLine
DrawPrint = DrawPrint & "                            <td>" &  vchBundleQuantity & "</td>" & VbNewLine
DrawPrint = DrawPrint & "                        </tr>" & VbNewLine
DrawPrint = DrawPrint & "                       <tr align=""left"">" & VbNewLine
DrawPrint = DrawPrint & "                            <td>Quantity:</td>" & VbNewLine
DrawPrint = DrawPrint & "                            <td>" &  rsData("intQuantity") & "</td>" & VbNewLine
DrawPrint = DrawPrint & "                        </tr>" & VbNewLine
DrawPrint = DrawPrint & "                        <tr align=""left"">" & VbNewLine
DrawPrint = DrawPrint & "                            <td>Valid States:</td>" & VbNewLine
DrawPrint = DrawPrint & "                            <td>" &  vchValidStates & "</td>" & VbNewLine
DrawPrint = DrawPrint & "                        </tr>" & VbNewLine
                        
DrawPrint = DrawPrint & "                        <tr align=""left"">" & VbNewLine
DrawPrint = DrawPrint & "                            <td>Expire Date:</td>" & VbNewLine
DrawPrint = DrawPrint & "                            <td>" &  vchExpireDate  &"</td>" & VbNewLine
DrawPrint = DrawPrint & "                        </tr>" & VbNewLine
                        
                       
DrawPrint = DrawPrint & "                   </table>" & VbNewLine
DrawPrint = DrawPrint & "                </td>" & VbNewLine
DrawPrint = DrawPrint & "	        </tr>" & VbNewLine
	          
                mnySubTotal = mnySubTotal + (rsData("mnyUnitPrice")* rsData("intQuantity"))
                mnyTotal = mnyTotal + mnySubTotal
		        rsData.MoveNext
	        wend
	        
DrawPrint = DrawPrint & "	        </table>" & VbNewLine
DrawPrint = DrawPrint & "	    </div>" & VbNewLine
           
	        rsData.close
	        set rsData = nothing
DrawPrint = DrawPrint & "	</body></html>"
End Function

private function readBytes(file)
    dim inStream
    ' ADODB stream object used
    set inStream = Server.CreateObject("ADODB.Stream")
    ' open with no arguments makes the stream an empty container
    inStream.Open
    inStream.type= 1 
    'Response.Write file
    inStream.LoadFromFile(file)
    readBytes = inStream.Read()
  end function

  private function encodeBase64(bytes)
    dim DM, EL
    Set DM = Server.CreateObject("Microsoft.XMLDOM")
    ' Create temporary node with Base64 data type
    Set EL = DM.createElement("tmp")
    EL.DataType = "bin.base64"
    ' Set bytes, get encoded String
    EL.NodeTypedValue = bytes
    encodeBase64 = Replace(Replace(Replace(Replace(EL.Text,vbNewLine,""),vbCrLf,""), vbCR,""), vbLF,"")
  end function

  private function decodeBase64(base64)
    dim DM, EL
    Set DM = Server.CreateObject("Microsoft.XMLDOM")
    ' Create temporary node with Base64 data type
    Set EL = DM.createElement("tmp")
    EL.DataType = "bin.base64"
    ' Set encoded String, get bytes
    EL.Text = base64
    decodeBase64 = EL.NodeTypedValue
  end function

  private Sub writeBytes(file, bytes)
    Dim binaryStream
    Set binaryStream = Server.CreateObject("ADODB.Stream")
    binaryStream.Type = adTypeBinary
    'Open the stream and write binary data
    binaryStream.Open
    binaryStream.Write bytes
    'Save binary data to disk
    binaryStream.SaveToFile file, adSaveCreateOverWrite
  End Sub

%>