<!--#include file="config/incUstore.asp"-->

<%
select case reqAction
	case "CreateRecipientListTemplate"
		CreateRecipientListTemplate
end select

sub CreateRecipientListTemplate()
	'response.Write "in DrawExport"
	dim intProductID, RecipientListSchemaXml
	dim strFieldSep, strFieldQuote, strFieldAltQuote, strFileName, strData, strEOL, strGiftMessage, XML, fullXML, oXmlNodes, indivNode, dataNode, dctHeaderName, resultsString, objTextStream, objFileSystemObject, strSQL, rsTemp
	dim strLine, strItem, strNote, i
	
	intProductID = request("intProductID")
	
	set dctHeaderName = Server.CreateObject("Scripting.Dictionary")
	
	strSQL = "select txtRecipientListSchemaXml from " & TABLE_PRODUCT_TABLE & " where intUstoreID = " & intProductID
	
	openConn
	set rsTemp = gobjConn.execute(strSQL)
	
	RecipientListSchemaXml = rsTemp("txtRecipientListSchemaXml")
	CloseConn
	'if strFormat = "CSV" then
	'	strFileName = "RecipientListTemplate_" & intProductID
	'	strFieldSep = ","
	'	strFieldQuote = """"
	'	strFieldAltQuote = "'"
	'	strEOL = vbCrLf
	'else
	'strFileName = "RecipientListTemplate_" & intProductID
	'strFieldSep = Chr(9)
	'strFieldQuote = ""
	'strFieldAltQuote = ""
	'strEOL = vbCrLf
	'end If
	'response.ContentType = "text/csv"
	'response.AddHeader "Content-transfer-encoding", "binary"
	'response.AddHeader "Content-Disposition", "attachment; filename=" & strFileName
	Response.ContentType = "application/vnd.ms-excel"
	Response.AddHeader "Content-Disposition", "attachment; filename=RecipientListTemplate_" & intProductID & ".xls"
	
	RecipientListSchemaXml = replace(replace(replace(replace(replace(RecipientListSchemaXml, "&amp", "&"), "&lt;", "<"), "&gt;", ">"), "&quote", """"),"<?xml version=""1.0"" encoding=""utf-16""?>", "<?xml version='1.0' encoding='UTF-8' standalone='yes'?>")

	set XML = Server.CreateObject("Msxml2.DOMDocument")
	XML.LoadXml(RecipientListSchemaXml)
	set fullXML = XML
	
	Set oXmlNodes = fullXML.selectNodes("//field")
		
	for i = 0 to oXmlNodes.length-1
		set indivNode = oXmlNodes.item(i)
			
		Set dataNode = indivNode.getElementsByTagName("name")
		dctHeaderName.Add i, dataNode.item(0).text
	next
	'if strFieldQuote <> "" then
        
    Dim intLoop
	resultsString = "<html>"
    resultsString = resultsString & "<head>"
    resultsString = resultsString & "<meta http-equiv=""Content-Type"" "
    resultsString = resultsString & "content=""text/html; charset=UTF-8"">"
    resultsString = resultsString & "<style type=""text/css"">"
    resultsString = resultsString & "html, body, table {"
    resultsString = resultsString & "    margin: 0;"
    resultsString = resultsString & "    padding: 0;"
    resultsString = resultsString & "    font-size: 11pt;"
    resultsString = resultsString & "}"
    resultsString = resultsString & "table, th, td { "
    resultsString = resultsString & "    border: 0.1pt solid #D0D7E5;"
    resultsString = resultsString & "    border-collapse: collapse;"
    resultsString = resultsString & "    border-spacing: 0;"
    resultsString = resultsString & "}"
    resultsString = resultsString & "</style>"
    resultsString = resultsString & "</head>"
    resultsString = resultsString & "<body>"
    resultsString = resultsString & "<table><tr>"
    For intLoop = 0 To (dctHeaderName.Count - 1)
        resultsString = resultsString & "<td>" & dctHeaderName(intLoop) & "</td>"
    Next
    resultsString = resultsString & "</tr></table></body></html>"
	response.write resultsString
	response.end
		
	'Set objFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
 
    ' Create a text file and dump the contents of the string to the file,
    ' using the file name export with the current date and time as a way to seperate out differing users
	'response.write Server.MapPath("\ustoretesting") & "\" & strFileName & ".xls"
	'Set objTextStream = objFileSystemObject.CreateTextFile(Server.MapPath("\ustoretesting") & "\" & strFileName & ".xls", 2, True)
    'objTextStream.Write(ResultsString)
 
    ' Clean up
    'objTextStream.Close
    'Set objTextStream = Nothing
    'Set objFileSystemObject = Nothing
	
	
	'start from here for excel
	Set objExcel = CreateObject("Excel.Application") 
	objExcel.Visible = True 
	objExcel.DisplayAlerts = FALSE 

	Set objWorkbook = objExcel.Workbooks.Add 
	Set objWorksheet = objWorkbook.Worksheets(1) 

	For intLoop = 0 To (dctHeaderName.Count - 1)
		objWorksheet.Cells(intLoop, 1).Value = dctHeaderName(intLoop)
	next
	
	objWorkbook.SaveAs(Server.MapPath("\ustoretesting") & "\" & strFileName & ".xls") 
	objExcel.Quit 

	'end if
	'CreateRecipientListTemplate = strFileName & ".csv"
end sub
%>