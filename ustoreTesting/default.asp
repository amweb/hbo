<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

OpenConn

'If gintUserName&"" = ""  Then
'Response.Redirect virtualbase
'End if

DrawHeader "Dashboard", "custserv"
DrawPage
DrawFooter "custserv"


Sub DrawPage
%>
<link rel="stylesheet" href="<%= virtualbase %>ustoretesting/css/ctabgen.css" type="text/css">
<link rel="stylesheet" href="<%= virtualbase %>ustoretesting/css/templatestyle.css" type="text/css">

<div class="page-content-wrapper">
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h3 class="page-title">
                    Templates
				</h3>
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="../store/dashboard.asp">Home</a>
					</li>
                    <li>
						<i class="fa fa-angle-right"></i>
						<a id="bcPOD" style="font-weight:bold" href="#" onclick="loadProductGroups(); return false">Templates</a>
                    </li>
				</ul>
			</div>
		</div>
		
		<div id="topBreadcrumbArea"></div>
		<div class="col-md-3 backgroundColumn" id="productGroups"></div>
		<div class="leftcolumn col-md-4" id="dialArea"></div>
		<div class="col-md-9" id="products"></div>
		<div class="rightcolumn col-md-offset-1 col-md-6" id="productPreview"></div>
		<div clear="all">&nbsp;</div>
		<div class="full-width" id="fullwidth"></div>
		
		<div id="fileUploader" style="display:none;">
			<form id="file-form" method="POST" enctype="multipart/form-data">
				<input type="file" id="file-select" name="uploadItem" /><br />
				<button type="submit" id="upload-button">Upload</button>
			</form>
		</div>
		
		<div id="cartPage" style="display:none">
			<p id="errorTextFull" style="color: red"></p>
			<table id="CPCartFull" class="CSSTableGenerator" width="50%">
				
			</table>
			<div style="float:right; margin-top:15px;"><button type="button" class="btn btn-success" onclick="AddAndCreateNew(); return false">Create another POD</button>&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-success" onclick="CompleteOrder(); return false">Checkout</button></div>
		</div>
		
	</div>
	
    </div>

<div style="margin-bottom:100px" >&nbsp;</div>
<%
End Sub

%>