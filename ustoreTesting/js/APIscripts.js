var url = "config/ajax_functions.asp";

$(document).ready(function () {
	
	$("#simpleSearch").submit(function() {
		$("#mailTable").jqGrid('setCaption', 'Mail for Member: ');
		$("#memberTable").jqGrid('setGridParam',{datatype:'json'});
		$.ajax({
            type: "POST",
            url: url,
            data: $("#simpleSearch").serialize(),
			dataType: "json",
            success: function (data) 
			{
                    
                $(function () {
                    var thegrid = $("#memberTable");
                    thegrid.clearGridData(true);
					thegrid[0].addJSONData({
						total: 1,
						page: 1,
						records: data.length,
						rows: data
					});
					$("#memberTable").jqGrid('setGridParam',{datatype:'local'});
				});
				$("#mailTable").clearGridData(true);
            },
			error: function (xhr, ajaxOptions, thrownError) {
				alert(xhr.status);
				alert(thrownError);
			}
			});

		return false; // avoid to execute the actual submit of the form.
	});

});

/*
$("#idForm").submit(function() {

    var url = "path/to/your/script.php"; // the script where you handle the form input.

    $.ajax({
           type: "POST",
           url: url,
           data: $("#idForm").serialize(), // serializes the form's elements.
           success: function(data)
           {
               alert(data); // show response from the php script.
           }
         });

    return false; // avoid to execute the actual submit of the form.
});
*/