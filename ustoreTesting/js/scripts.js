if (window.location.host.search("clients") > -1) {
    var url = window.location.protocol + "//" + window.location.host + "/HBO/www/ustoretesting/config/ajax_functions.asp";
} else {
    var url = window.location.protocol + "//" + window.location.host + "/ustoretesting/config/ajax_functions.asp";
}

var dialData = "";

$(document).ready(function () {

	if ( getParameterByName("ticketID") != "")
	{
		editFromCart(getParameterByName("ticketID"));
	}
	else if (window.location.pathname.indexOf("ustoretesting") > 0)
	{
		loadProductGroups();
	}
	$(document).bind("ajaxStart", function(){
		showLoader("on");
	}).bind('ajaxStop', function(){
		showLoader("off");
	});
});

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


function showLoader(toggle)
{
	if (toggle == "on")
	{
		$( "#loader" ).dialog({
			modal: true,
			draggable: false,
			resizable: false,
			dialogClass: "noTitleStuff"
		});
	}
	else
	{
		if($("#loader").is(":visible")){
			$( "#loader" ).dialog("close");
		}
	}
}

function loadProductGroups()
{
	checkLogin();
	$("#bcPOD").attr("style","font-weight:bold");
	var message = "";
	if ($("#bcProductName").length)
	{
		$("#bcProductName").remove();
	}
	
	if ($("#bcProductGroup").length)
	{
		$("#bcProductGroup").remove();
	}
	
	if ($("#bcFinalize").length)
	{
		$("#bcFinalize").remove();
	}
	
	$.ajax({
		type: "POST",
		url: url,
		data: "action=loadProductGroups", // serializes the form's elements.
		success: function(data)
		{
			message += "<ul class=\"nav nav-pills nav-stacked\">";
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					var actualData = data[key];
					
					message += "<li role=\"presentation\" id=\"groupid-" + actualData["intID"] +"\" class=\"" + (actualData["vchName"].replace(" ", "")).toLowerCase() + "\"><a href=\"#\" onclick=\"javascript:loadProductsInGroup(" + actualData["intID"] + ",'" + actualData["vchName"] + "'); return false\">" + actualData["vchName"] + "</a></li>";
					
				}
			};
			//console.log(data);
			message += "</ul>";
			$("#productGroups").html(message);
			//alert(message);
			$("#topBreadcrumbArea").html("");
			$("#topBreadcrumbArea").hide();
			$("#productGroups").show();
			$("#dialArea").hide();
			$("#products").show();
			$("#cartPage").hide();
			$("#products").html("");
			$("#productPreview").hide();
			$("#fullwidth").hide();
			$("#cartPage").hide();
		},
		error: function(err) {
			console.log(err);
		}
	});
	return false;
}

function loadProductsInGroup(groupNum, groupName)
{
	checkLogin();
	var message = "";
	var subNav = "";
	var counter = 1;
	var breadCrumb = "<li id=\"bcProductGroup\" style=\"font-weight:bold\"><i class=\"fa fa-angle-right\"></i><a href=\"#\" onclick=\"javascript:loadProductsInGroup(" + groupNum + ",'" + groupName + "'); return false\">" + groupName + "</a></li>";
	if ($("#bcProductName").length)
	{
		$("#bcProductName").remove();
	}
	
	if ($("#bcFinalize").length)
	{
		$("#bcFinalize").remove();
	}
	
	$(".PODsubitems").remove();
	$("li").removeClass("active");
	$("#bcPOD").attr("style","font-weight:normal");
	if ($("#bcProductGroup").length)
	{
	}
	else
	{
		$(".page-breadcrumb").append(breadCrumb);
	}
	
	$.ajax({
		type: "POST",
		url: url,
		data: "action=loadGroupTemplates&groupNum="+groupNum, // serializes the form's elements.
		success: function(data)
		{
			subNav = "<ul class=\"PODsubitems\">";
			message = "<div class=\"row\">";
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					var actualData = data[key];
					var images = actualData["txtImage64"].split("|");
					var fileName = actualData["vchStaticDocLocation"].split("\\")
					
					subNav += "<li role=\"presentation\"><a href=\"#\" onclick=\"javascript:loadProductStep(" + actualData["intUStoreID"] + ",'" + actualData["vchName"] + "', '" + images[0] + "'); return false\">" +  actualData["vchName"] + "</a></li>";
					if (actualData["vchStaticDocLocation"] != "")
					{
						message += "<div class=\"col-md-4\"><div class=\"thumbnail\"><a href=\"static/" + fileName[fileName.length-1]+ "\" target=\"_blank\">";
					}
					else
					{
						message += "<div class=\"col-md-4\"><div class=\"thumbnail\"><a href=\"#\" onclick=\"javascript:loadProductStep(" + actualData["intUStoreID"] + ",'" + actualData["vchName"] + "', '" + images[0] + "'); return false\">";
					}
					
					//console.log(actualData["txtImage64"]);
					for (var i = 0; i < images.length-1; i++) {
						message += "<img height=\"504px\" width=\"680px\" src=\"data:image/png;base64," + images[i] + "\" />";
						//console.log(images[i]);
					}
					
					message += "</a><div class=\"caption\">";
					message += "<p>" + actualData["vchName"] + "</p>";
					if (actualData["vchStaticDocLocation"] != "")
					{
						
						message += "<p><a href=\"static/" + fileName[fileName.length-1]+ "\" class=\"btn btn-primary dlbutton\" role=\"button\" target=\"_blank\">Download PDF</a></p></div></div></div>";
					}
					else
					{
							message += "<p><a href=\"#\" class=\"btn btn-primary\" role=\"button\" onclick=\"javascript:loadProductStep(" + actualData["intUStoreID"] + ",'" + actualData["vchName"] + "', '" + images[0] + "'); return false\">Customize</a></p></div></div></div>";
					}
					/*
					if (actualData["boolShowShortDesc"] == 1)
					{
						message += actualData["txtShortDescription"] + "<br />";
					}
					
					message += actualData["txtDescription"] + "<br />";
					*/
					
					
					
				}
				
				if(counter % 3 == 0)
				{
					message += "</div><div class=\"row\">";
				}
				counter++;
			};
			
			if(counter % 3 == 0)
			{
				message += "";
			}
			else
			{
				message += "</div>";
			}
			subNav += "</ul>"
			$("#groupid-" + groupNum).append(subNav);
			$("#groupid-" + groupNum).addClass("active");
			$("#products").html(message);
			$("#topBreadcrumbArea").html("");
			$("#topBreadcrumbArea").hide();
			$("#productGroups").show();
			$("#dialArea").hide();
			$("#products").show();
			$("#productPreview").hide();
			$("#fullwidth").hide();
			$("#cartPage").hide();
			//alert(message);
		},
		error: function(err) {
			console.log(err);
		}
	});
	return false;
}

function loadProductStep(productNum, title, base64Image)
{
	checkLogin();
	var message = "";
	var tabs = "";
	var dials = "";
	var previewProofMessage = "";
	var totalSteps;
	var breadCrumb = "";
	var i = 0;
	var loading = true;
	var titleSection = "<div class=\"dialArea\"><div class=\"dialText\"><p class=\"templateHeading\">" + title + "</p><p>Product details</p></div>";
	titleSection += "<div class=\"dialText\"><ul class=\"nav nav-pills breadcrumbBG2\" id=\"tabSteps\"></ul></div><div style=\"margin-top:10px;\"><form id=\"dialForm\">";
	titleSection += "</form></div></div>";
	breadCrumb = "<li id=\"bcProductName\" style=\"font-weight:bold\"><i class=\"fa fa-angle-right\"></i><a href=\"#\" onclick=\"loadProductStep(" + productNum + ", '" + title + "', '" + base64Image + "'); return false\" style=\"font-weight: bold\">" + title + " - Customize</a></li>";
	
	$("#bcProductGroup").attr("style","font-weight:normal");
	
	if ($("#bcProductName").length)
	{
		$("#bcProductName").attr("style","font-weight:bold");
	}
	else
	{
		$(".page-breadcrumb").append(breadCrumb);
	}
	
	if ($("#bcFinalize").length)
	{
		$("#bcFinalize").remove();
	}
	$("#dialArea").show();
	$("#productPreview").show();
	drawTopBreadcrumbs(productNum);
	previewProofMessage += "<img src=\"data:image/png;base64," + base64Image + "\" width=\"540px\" /><br /><a class=\"refreshproof\" href=\"#\" onclick=\"createProof(" + productNum + "); return false\">Refresh Proof</a>";
	$.ajax({
		type: "POST",
		url: url,
		data: "action=loadProductSteps&productNum=" + productNum, // serializes the form's elements.
		success: function(data)
		{
			totalSteps = data.length;
			//console.log(totalSteps);
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					//console.log(data);
					var actualData = data[key];
					if (actualData.hasOwnProperty('intStepID'))
					{
						
						tabs += "<li role=\"presentation\" ";
						if (i == 0)
						{
							tabs += " class=\"active\" ";
						}
						
						tabs += "><a href=\"#\" onclick=\"showDiv(test); return false\">" + actualData["vchName"] + "</a></li>";
						message += "<div id=\"stepid-" + actualData["intStepID"] + "\">";
						$.ajax({
						type: "POST",
						url: url,
						data: "action=loadProductDialsByStep&stepNum=" + actualData["intStepID"], // serializes the form's elements.
						success: function(data)
						{
							for (var key in data) {
								if (data.hasOwnProperty(key)) {
									//console.log(data);
									var actualData2 = data[key];
									if (actualData2.hasOwnProperty('intStepID'))
									{
										dials += createInput(actualData2["intDialID"], actualData2["intControlType"], actualData2["intAssetSelectionSourceID"], actualData2["intAssestUploadSourceID"], actualData2["intDialValueAutoFillTypeID"], actualData2["intDependentOnFieldOptionID"], actualData2["vchName"], actualData2["vchUProduceDialName"], actualData2["vchDefaultValue"], actualData2["blnIsVariable"], actualData2["txtDescription"]) + "<br />";
									}
								}
							}
							var backLink = $("#bcProductGroup a").attr("onclick");
							dials += "<button type=\"button\" class=\"btn btn-success\" onclick=\"" + backLink + "\">Back</button>";
							dials += "<div style=\"float:right; \">";
							
							if ( i < totalSteps - 1 )
							{
								dials += "<button type=\"button\" class=\"btn btn-success\" onclick=\"NextStep; return false\">Next</button>";
							}
							else
							{
								if (getParameterByName("ticketID") != "")
								{
									dials += "<button type=\"button\" class=\"btn btn-success\" onclick=\"createProof(" + productNum + "); updateItems(); return false\">Continue</button>";
								}
								else
								{
									dials += "<button type=\"button\" class=\"btn btn-success\" onclick=\"showRecipientList(" + productNum + "); return false\">Continue</button>";
								}
							}
							
							dials += "</div>";
							$("#stepid-" + actualData["intStepID"]).html(dials);
							dials = "";
							i++;
						},
						error: function(err) {
							console.log(err);
						}});
						message += "</div>";
						
					}
					else if (actualData.hasOwnProperty('recipentlist'))
					{
					
					}
					else
					{
					
					}
					/*
					message = message + "<a href=\"#\" onclick=\"javascript:loadProductStep(" + actualData["intUStoreID"] + "\")>";
					
					var images = actualData["txtImage64"].split("|");
					console.log(actualData["txtImage64"]);
					for (var i = 0; i < images.length-1; i++) {
						message = message + "<img src=\"data:image/png;base64," + images[i] + "\" />";
						console.log(images[i]);
					}
					
					message = message + "</a><br />";
					
					message = message + "<a href=\"#\" onclick=\"javascript:loadProductStep(" + actualData["intUStoreID"] + "\")>" + actualData["vchName"] + "</a><br />";
					
					if (actualData["boolShowShortDesc"] == 1)
					{
						message = message + actualData["txtShortDescription"] + "<br />";
					}
					
					message = message + actualData["txtDescription"] + "<br />";
					*/
				}
			};
			
			$(document).ajaxStop(function() {
				if (loading == true)
				{
					if ( getParameterByName("ticketID") != "")
					{
						loadExistingDialValues(productNum, getParameterByName("ticketID"));
					}
					else
					{
						loadExistingDialValues(productNum, "");
					}
					loading = false;
				}
			});
			$("#productGroups").hide();
			$("#products").hide();
			$("#dialArea").html(titleSection);
			$("#tabSteps").html(tabs);
			$("#productPreview").html(previewProofMessage);
			$("#dialForm").append(message);
			$("#cartPage").hide();
			//alert(message);
		},
		error: function(err) {
			console.log(err);
		}
	});
	
	return false;
}

function createInput(intDialID, intControlType, intAssetSelectionSourceID, intAssestUploadSourceID, intDialValueAutoFillTypeID, intDependentOnFieldOptionID, vchName, vchUProduceDialName, vchDefaultValue, blnIsVariable, txtDescription)
{
	var inputString = "";
	
	switch (intControlType) {
		case 5:
			//TEXT INPUT
			inputString = "<div class=\"form-group\"><label for=\""+ vchName+"\" class=\"control-label\">"+vchName+"</label><input type=\"text\" class=\"form-control\" name=\"" +vchUProduceDialName + "\" value=\"" + vchDefaultValue + "\" id=\"dialid-" +intDialID + "\" /><em>" + txtDescription + "</em></div>";
			break;
		case 3:
			//MULTI LINE TEXT INPUT
			inputString = "<div class=\"form-group\"><label for=\""+vchName+"\" class=\"control-label\">"+vchName+"</label><textarea class=\"form-control\" rows=\"7\" cols=\"33\" name=\"" +vchUProduceDialName + "\" id=\"dialid-" +intDialID + "\" >" + vchDefaultValue + "</textarea><em>" + txtDescription + "</em></div>";
			break;
		case 13:
			//GET ORDER PROPERTIES
			inputString = "<input type=\"hidden\" id=\"dialid-" +intDialID + "\" name=\"" +vchUProduceDialName + "\" value=\"" + vchDefaultValue + "\" id=\"dialid-" +intDialID + "\" />";
			break;
		case 14:
			//CHECK BOX
			inputString = "<div class=\"form-group\"><label for=\""+vchName+"\" class=\"control-label\">"+vchName+"</label>";
			inputString += "<div id=\"dialid-" +intDialID + "\" >";
			inputString += "</div><em>" + txtDescription + "</em></div>";
			loadDialOptions(intDialID, vchUProduceDialName, "checkbox");
			GetDialParams(intDialID,vchUProduceDialName,"checkbox","")
			break;
		case 9:
			//DATE TIME PICKER
			//build check for param to see what type of date/time/both
			vchDefaultValue = DateAdjustment(vchDefaultValue,"defaultValue");
			inputString = "<div class=\"form-group\"><label for=\""+vchName+"\" class=\"control-label\">"+vchName+"</label><div id=\"dial-" + intDialID + "\"></div><em>" + txtDescription + "</em></div>";
			GetDialParams(intDialID, vchUProduceDialName, "datetime", vchDefaultValue)
			break;
		case 11:
			//DROP DOWN LIST
			inputString = "<div class=\"form-group\"><label for=\""+vchName+"\" class=\"control-label\">"+vchName+"</label>";
			inputString += "<select class=\"form-control\" name=\""+vchUProduceDialName+"\" id=\"dialid-" +intDialID + "\" >"
			
			loadDialOptions(intDialID, vchUProduceDialName, "dropdown");
			GetDialParams(intDialID, vchUProduceDialName, "dropdown");
			
			inputString += "</select><em>" + txtDescription + "</em></div>";
			break;
		case 12:
			//RADIO BUTTON LIST
			inputString = "<div class=\"form-group\"><label for=\""+vchName+"\" class=\"control-label\">"+vchName+"</label>";
			inputString += "<div id=\"dialid-" +intDialID + "\" >";
			inputString += "</div><em>" + txtDescription + "</em></div>";
			
			loadDialOptions(intDialID, vchUProduceDialName, "radio");
			break;
		case 16:
			//GALLERY LIST VIEW
			/* not used?
			
			sample (1) item from ustore
			<div id="ctl00_cphMainContent_ucDialCustomization_Duc10316_rptrOptions_ctl00_divFieldOptionItem" class="FieldOptionItem FOList selected" style="background-image:url(/uStore/Images/Skin%20XMPieGreen/PropertyIcons/Event%20State_Test%201.png);">
                <div id="divTitle" class="optionTitle"><span id="ctl00_cphMainContent_ucDialCustomization_Duc10316_rptrOptions_ctl00_lblTitle">Test 1</span></div>
                <div id="divDescription"><span id="ctl00_cphMainContent_ucDialCustomization_Duc10316_rptrOptions_ctl00_lblDescription">this is test 1</span></div>
                <input type="hidden" name="ctl00$cphMainContent$ucDialCustomization$Duc10316$rptrOptions$ctl00$hdnOptionValue" id="ctl00_cphMainContent_ucDialCustomization_Duc10316_rptrOptions_ctl00_hdnOptionValue" value="Test 1" foid="11560">                
            </div>
			*/
			break;
		case 17:
			//GALLERY GRID VIEW
			/* sample (1) item from ustore
			<div id="ctl00_cphMainContent_ucDialCustomization_Duc10317_rptrOptions_ctl00_divFieldOptionItem" class="FieldOptionItem FOGrid HasXmpTooltip selected">
                <div id="ctl00_cphMainContent_ucDialCustomization_Duc10317_rptrOptions_ctl00_divIcon" class="optionIcon" style="background-image:url(/uStore/Images/Skin%20XMPieGreen/PropertyIcons/Event%20Zip_Test%203.png);"></div>
				<div id="divTitle" class="optionTitle"><span id="ctl00_cphMainContent_ucDialCustomization_Duc10317_rptrOptions_ctl00_lblTitle">Test 3</span></div>
				<input type="hidden" name="ctl00$cphMainContent$ucDialCustomization$Duc10317$rptrOptions$ctl00$hdnOptionValue" id="ctl00_cphMainContent_ucDialCustomization_Duc10317_rptrOptions_ctl00_hdnOptionValue" value="Test 3" foid="11562">
			</div>
			*/
			break;
		case 18:
			//GALLERY Image Selecter
			inputString += "<div class=\"form-group\"><label for=\"" + vchName + "\" class=\"control-label\">" + vchName + "</label><input type=\"text\" name=\"" +vchUProduceDialName + "\" id=\"dialid-" +intDialID + "\" readonly><button onclick=\"loadUploader('dialid-" + intDialID + "', 'image'); return false;k\">Upload</button>";
			inputString += "<br /><em>" + txtDescription + "</em>";
			/* sample (1) item from ustore
			<div id="ctl00_cphMainContent_ucDialCustomization_Duc10317_rptrOptions_ctl00_divFieldOptionItem" class="FieldOptionItem FOGrid HasXmpTooltip selected">
                <div id="ctl00_cphMainContent_ucDialCustomization_Duc10317_rptrOptions_ctl00_divIcon" class="optionIcon" style="background-image:url(/uStore/Images/Skin%20XMPieGreen/PropertyIcons/Event%20Zip_Test%203.png);"></div>
				<div id="divTitle" class="optionTitle"><span id="ctl00_cphMainContent_ucDialCustomization_Duc10317_rptrOptions_ctl00_lblTitle">Test 3</span></div>
				<input type="hidden" name="ctl00$cphMainContent$ucDialCustomization$Duc10317$rptrOptions$ctl00$hdnOptionValue" id="ctl00_cphMainContent_ucDialCustomization_Duc10317_rptrOptions_ctl00_hdnOptionValue" value="Test 3" foid="11562">
			</div>
			*/
			break;
		case 10:
			//HTML GENERIC
			// not actual input?
			break;
		default:
			//code
	}
	
	return inputString;
}

function loadDialOptions(dialID, Name, optionType)
{
	var dialOptions = ""
	if (optionType == "dropdown")
	{
		dialOptions = "<option selected=\"selected\" value=\"\">-- Please Select a Value --</option>";
	}
	$.ajax({
		type: "POST",
		url: url,
		async:true,
		data: "action=loadDialOptions&dialID=" + dialID, // serializes the form's elements.
		success: function(data)
		{
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					var actualData = data[key];
					
					//types: dropdown, radio, gallery grid, gallery list, checkbox
					if (optionType == "dropdown")
					{
						dialOptions += "<option value=\"" + actualData["vchValue"] + "\">" + actualData["vchText"] + "</option>";
					}
					else if (optionType == "radio")
					{
						dialOptions += "<input type=\"radio\" class=\"form-control\" name=\"" + Name + "\" value=\"" + actualData["vchValue"] + "\" />" + actualData["vchText"] + "<br />";
					}
					else if (optionType == "checkbox")
					{
						dialOptions += "<input type=\"checkbox\" class=\"form-control\" id=\"dial-" + dialID + "\" name=\"" +Name + "\" value=\"" + actualData["vchValue"] + "\" /><br />";
						
						break;
					}
					
				}
			};
			$("#dialid-" + dialID).html(dialOptions);
		},
		error: function()
		{
			return "error";
		}
	});
}

function DateAdjustment(DateToAdjust, adjustType)
{
	var today =  new Date();
	var tomorrow = new Date(today);
	
	//console.log(DateToAdjust);
	if (adjustType == "defaultValue")
	{
		DateToAdjust = DateToAdjust.replace("&lt;DateTime&gt;&lt;Server&gt;","");
		DateToAdjust = DateToAdjust.replace("&lt;/Client&gt;&lt;/DateTime&gt;","");
		DateToAdjust = DateToAdjust.split("&lt;/Server&gt;&lt;Client&gt;")
		//console.log(DateToAdjust);
		//console.log(today.getMonth());
		
		if (DateToAdjust[0].length > 0)
		{
			var splitString = DateToAdjust[0].split("_");
		
			if (splitString[1].indexOf("Days") > -1)
			{
				tomorrow.setDate(today.getDate() + parseInt(splitString[0], 10));
			}
			else if (splitString[1].indexOf("Weeks") > -1)
			{
				tomorrow.setDate(today.getDate() + (parseInt(splitString[0], 10) * 7));
			}
			else if (splitString[1].indexOf("Months") > -1)
			{
				tomorrow.setMonth(today.getMonth() + parseInt(splitString[0], 10));
			}
		}
		else
		{
			tomorrow = "";
		}
	}
	else
	{
		if (DateToAdjust.length > 0)
		{
			var splitString = DateToAdjust.split("_");
		
			if (splitString[1].indexOf("Days") > -1)
			{
				tomorrow.setDate(today.getDate() + parseInt(splitString[0], 10));
			}
			else if (splitString[1].indexOf("Weeks") > -1)
			{
				tomorrow.setDate(today.getDate() + (parseInt(splitString[0], 10) * 7));
			}
			else if (splitString[1].indexOf("Months") > -1)
			{
				tomorrow.setMonth(today.getMonth() + parseInt(splitString[0], 10));
			}
		}
		else
		{
			tomorrow = "";
		}
	}
	
	return tomorrow;
}

function DateFormater(DateToFormat, formatType)
{
	var newFormat = "";
	var hour = "";
	var amPM = "";
	//console.log(DateToAdjust);
	if (formatType == "datetime")
	{
		if ( (DateToFormat.getHours() + 1) > 12)
		{
			hour = (DateToFormat.getHours() + 1) - 12;
			amPM = "PM";
		}
		else
		{
			hour = DateToFormat.getHours() + 1;
			amPM = "AM";
		}
		
		newFormat = (DateToFormat.getMonth()+1) + "/" + DateToFormat.getDate() + "/" + DateToFormat.getFullYear() + " " +  hour + ":" + DateToFormat.getMinutes() + " " + amPM;
		//Sun Sep 27 2015 14:02:49 GMT-0700 (Pacific Daylight Time)
	}
	else
	{
		newFormat = (DateToFormat.getMonth()+1) + "/" + DateToFormat.getDate() + "/" + DateToFormat.getFullYear();
		
	}
	
	return newFormat;
}

function GetDialParams(dialID,name,optionType,defaultValue)
{
	var inputString ="";
	var minValue = "";
	var maxValue = "";
	var timeFormat = "";
	var dateType = "";
	
	$.ajax({
		type: "POST",
		url: url,
		async:true,
		data: "action=loadDialParams&dialID=" + dialID, // serializes the form's elements.
		success: function(data)
		{
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					var actualData = data[key];
					
					if (optionType == "getorderproperties")
					{
						if (actualData["Property"] == "Order Item ID")
						{
							// use uStoreID
						}
						else
						{
							// use this order id
						}
					}
					else if (optionType == "datetime")
					{
						
						// (EnableUseSelection (this is nothing), ConvertToServerTime (not used?), DatePicker (using only popup), DateMinValue, DateMaxValue, TimeFormat)
						
						if (actualData["vchParamType"] == "DateMinValue") {
							minValue = DateAdjustment(actualData["vchParamValue"], "");
						}
							
						if (actualData["vchParamType"] == "DateMaxValue") {
							maxValue = DateAdjustment(actualData["vchParamValue"], "");
						}
						
						if (actualData["vchParamType"] == "TimeFormat") {
							timeFormat = actualData["vchParamValue"];
						}
						
					}
					else if (optionType == "checkbox")
					{
						if (actualData["vchParamType"] == "CustomerMustCheck" && actualData["vchParamValue"] == "True")
						{
							$("#dial-"+ dialID).prop('required',true);
						}
					}
					else if (optionType == "dropdown" || optionType == "gallerygridview" || optionType == "gallerylistview" || optionType == "RADIO")
					{
						// (sort date, sort alpha)
						// Matt said do not use
						/*if (actualData["sortAlpha"] == "True")
						{
							var options = $("dialid-" +dialid + " option");
							var arr = options.map(function(_, o) { return { t: $(o).text(), v: o.value }; }).get();
							arr.sort(function(o1, o2) { return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0; });
							options.each(function(i, o) {
								o.value = arr[i].v;
								$(o).text(arr[i].t);
							});
						}
						*/
					}
					
				}
			};
			if (optionType == "datetime")
			{
				if ((minValue != "" || maxValue != "") && timeFormat != "")
				{
					dateType = "datetime";
				}
				else if (timeFormat != "")
				{
					dateType = "time";
				}
				else if (minValue != "" || maxValue != "")
				{
					dateType = "date";
				}
				
				if (dateType == "datetime" || dateType == "date")
				{
					defaultValue = DateFormater(defaultValue, dateType)
					
					if (minValue != "")
					{
						minValue = DateFormater(minValue, dateType)
					}
					
					if (maxValue != "")
					{
						maxValue = DateFormater(maxValue, dateType)
					}
				}
				
				inputString = "<input type=\"" + dateType + "\" class=\"form-control\" name=\"" +name + "\" value=\"" + defaultValue + "\" min=\"" + minValue + "\" max=\"" + maxValue + "\" />";
				//console.log(inputString);
				//console.log("#dial-" + dialID);
				$("#dial-" + dialID).html(inputString);
			}
		},
		error: function(err) {
			console.log(err);
		}
	});
}

function SaveDials(productID, ticketID)
{
	checkLogin();
	dialData = $("#dialForm").serialize();
	//console.log("action=SaveDials&ustoreID=" + productID + "&" + dialData);
	$.ajax({
		type: "POST",
		url: url,
		async:true,
		data: "action=SaveDials&ustoreID=" + productID + "&ticketID=" + ticketID + "&" + dialData, // serializes the form's elements.
		success: function(data)
		{
			
		},
		error: function(err) {
			console.log(err);
		}
	});
	
}

function showRecipientList(productID)
{
	checkLogin();
	createProof(productID);
	$("#customizeHeader").removeClass("active");
	$("#bcProductName").attr("style","font-weight:normal");
	
	$.ajax({
		type: "POST",
		url: url,
		async:true,
		data: "action=checkForRecipientList&productID=" + productID, // serializes the form's elements.
		success: function(data)
		{
			$("#refreshProof").remove();
			for (var key in data) {
				console.log(data);
				if (data.hasOwnProperty(key)) {
					var actualData = data[key];
					
					if (actualData["boolRecipientList"] == 1)
					{
						$("#recipientListHeader").addClass("active");
						LoadRecipientListPage(productID);
					}
					else
					{
						$("#finalizeHeader").addClass("active");
						loadFinalize(productID);
					}
				}
			}
		},
		error: function(err) {
			console.log(err);
		}
	});
}

function loadFinalize(productID)
{
	checkLogin();
	SaveDials(productID, getParameterByName("ticketID"));
	var min = 0;
	var max = 0;
	var bundleAmt = 0;
	var price = 0;
	$.ajax({
		type: "POST",
		url: url,
		async:true,
		data: "action=checkType&productID=" + productID, // serializes the form's elements.
		success: function(data)
		{
			var actualData = data[0];
			
			if (actualData["txtKeywords"].indexOf("Download") > -1)
			{
				$.ajax({
					type: "POST",
					url: url,
					async:true,
					data: "action=CreateFinal&ustoreID=" + productID, // serializes the form's elements.
					success: function(data)
					{
						var actualData = data[0];
						
						showDownloadPage(actualData["proofURL"]);
					},
					error: function(err) {
						console.log(err);
					}
				});
			}
			else
			{
			
				$("#recipientListHeader").removeClass("active");
				var message = "";
				var breadCrumb = "";
				breadCrumb = "<li id=\"bcFinalize\" style=\"font-weight:bold\"><i class=\"fa fa-angle-right\"></i><a href=\"#\" onclick=\"loadFinalize(" + productID + "); return false\">Finalize</a></li>";
				
				$("#bcProductName").attr("style","font-weight:normal");
				$("#bcProductName a").attr("style","font-weight:normal");
				
				if ($("#bcFinalize").length)
				{
					$("#bcFinalize").attr("style","font-weight:bold");
				}
				else
				{
					$(".page-breadcrumb").append(breadCrumb);
				}
				
				message = "<div class=\"dialArea\"><div style=\"margin-top:10px;\"><form name=\"finalize\" id=\"finalize\">";
				message += "<p id=\"quantityError\" style=\"color: red;\"></p>";
				//message += "<div class=\"form-group\"><label for=\"Copies\">Quantity</label><input type=\"number\" name=\"Copies\"></div>";
				
				$.ajax({
					type: "POST",
					url: url,
					async:false,
					data: "action=checkMinMax&productID=" + productID, // serializes the form's elements.
					success: function(data)
					{
						for (var key in data) {
							if (data) {
								var actualData = data[key];
								min = parseInt(actualData["intMinQty"]);
								max = parseInt(actualData["intMaxQty"]);
								bundleAmt = parseFloat(actualData["vchBundleQuantity"]);
								price = parseFloat(actualData["mnyItemPrice"]);
							}
						}
						console.log(data);
					},
					error: function(data)
					{
						console.log(data);
					}
				});
				
				$.ajax({
					type: "POST",
					url: url,
					async:true,
					data: "action=loadAddressBook&ustoreID=" + productID, // serializes the form's elements.
					success: function(data)
					{
						console.log(data);
						if (data.length == 1)
						{
							var actualData = data[0];
							message += "<input type=\"hidden\" name=\"shipID\" value=\"" + actualData["intID"] + "\" />";
							message += "<input type=\"hidden\" name=\"price\" id=\"price\" value=\"" + price + "\" />";
							if (min == max)
							{
								message += "<input type=\"hidden\" name=\"Copies\" value=\"" + min + "\" /><div class=\"form-group\">Quantity: " + bundleAmt + " printed pieces</div>";
							}
							else
							{
								message += "<div class=\"form-group\"><label for=\"Copies\">Quantity:&nbsp;</label><input type=\"number\" name=\"Copies\" id=\"copies\" style=\"width:50px\" onkeyup=\"CalculateTotal()\" onchange=\"CalculateTotal()\"> x <span id=\"bundleQuantity\">" + bundleAmt + "</span> = <span id=\"bundleTotal\">0</span> printed pieces&nbsp;&nbsp;</div>";
							}
						}
						if (price != 0)
						{
							message += "<p style=\"font-size:1.2em\"><b>Charged to store: $<span id=\"pricing\">0.00</span></b>";
						}
						var actualData = data[0];
						//$("#previewIframe").attr("src", iframeURL);
						var backLink = $("#bcProductName a").attr("onclick");
						message += "<p>";
						if (min != max)
						{
							if (!isNaN(min))
							{
								message += "Minimum Amount: " + (min * bundleAmt) + " printed pieces<br />";
							}
						
							if (!isNaN(max))
							{
								message += "Maximum Amount: " + (max * bundleAmt) + " printed pieces<br />";
							}
						}
						message += "</p><br clear=\"all\" />";
						
						message += "<p style=\"color:red\">Please review the proof carefully before clicking on \"Add to Cart\"; item will print exactly as shown.</p>";  
						
						message += "<div style=\"float:right; margin-top:15px;\"><button type=\"button\" class=\"btn btn-success\" onclick=\"" + backLink + "\">Back</button>&nbsp;&nbsp;&nbsp;&nbsp;<button type=\"button\" class=\"btn btn-success\" onclick=\"AddToCart(" + productID + "); return false\">Add To Cart</button></div>";
						message += "</form>";
						
						message += "</div></div>";
						$("#dialArea").html(message);
					},
					error: function(err) {
						console.log(err);
					}
					
				});
			
			}
		}
	});
	
}

function showDownloadPage(downloadURL)
{
	checkLogin();
	var message = "";
	message = "<h3>Your download is now ready</h3><p style=\"margin: 20px 0;\"><a href=\"" + downloadURL + "\" style=\"padding: 10px; font-size: 16px; background-color: purple; color: white; \" target=\"_blank\">Click here to download</a></p>";
	$("#topBreadcrumbArea").hide();
	message += "<a href=\"default.asp\" class=\"btn btn-primary\" role=\"button\"); return false\">Create another PDF or Customized Print</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"../store/dashboard.asp\" class=\"btn btn-primary\" role=\"button\"); return false\">Return Home</a>"
	$("#productGroups").hide();
	$("#dialArea").hide();
	$("#productPreview").hide();
	$("#fullwidth").html(message);
	$("#fullwidth").show();
	$("#cartPage").hide();
	

}

function createProof(productID)
{
	checkLogin();
	var iframeURL = "";
	var message = "";
	SaveDials(productID, getParameterByName("ticketID"));
	$.ajax({
		type: "POST",
		url: url,
		async:true,
		data: "action=CreateProof&ustoreID=" + productID + "&ticketid=" + getParameterByName("ticketID"), // serializes the form's elements.
		success: function(data)
		{
			console.log(data);
			var actualData = data[0];
			iframeURL = actualData["proofURL"];
			//window.open(iframeURL, "_blank");
			message = "<iframe src=\"js/web/viewer.html?proofURL=" + encodeURIComponent(iframeURL) + "\" width=\"100%\" height=\"800px\"></iframe><a href=\"#\" class=\"refreshproof\" id=\"refreshProof\" onclick=\"createProof(" + productID + ");  return false\">Refresh Proof</a>";
			$("#productPreview").html(message);
		},
		error: function(err) {
			console.log(err);
		}
	});	
}

function LoadRecipientListPage(productID)
{
	var message = "";
	message = "<input type=\"radio\" selected name=\"recipList\" value=\"upload\"><strong>Upload your Recipient List</strong><br />";

	message += "<div id=\"uploadDiv\"><strong>Upload your Recipient List file</strong>, or<br />";
	message += "<strong>Download a recipient list template</strong> using the 'Download Recipient List Template' link.<br />";
	message += "<strong>Edit the template</strong> using any spreadsheet editor such as Microsoft Excel or OpenOffice Calc.<br />";
	message += "<strong>Save the recipient list as XML Spreadsheet.</strong><br />";
	message += "<strong>Upload it back</strong> using the 'Browse' button.<br />";
	message += "<strong>File (*.xml, *.xls, *.xlsx, *.mdb, *.accdb, *.csv):</strong><input type=\"file\" name=\"recipientUpload\" /><br />";
	message += "<strong>List Name:</strong> <input type=\"text\" name=\"recipientName\" /><br />";
	message += "<a href=\"download.asp?action=CreateRecipientListTemplate&intProductID=" + productID + "\" target=\"_blank\">Download Recipient List Template</a></div>";

	message += "<input type=\"radio\" selected name=\"recipList\" value=\"choose\" onSelect=\"loadLists\">Use a previously uploaded List";
	message += "<div id=\"prevLists\"></div>";
	$("#productGroups").hide();
	$("#products").hide();
	$("#fullwidth").html(message);
	$("#cartPage").hide();
}


function loadUploader(targetID, uploadType)
{
	var extType;
	
	$("#file-select").val("");
	
	$( "#fileUploader" ).dialog({
		modal: true,
		draggable: false,
		resizable: false,
		width: 400,
		buttons: [
			{
				text: "Cancel",
				click: function() {
					$( this ).dialog( "close" );
				}
			}
		]
	});
	
	var form = document.getElementById('file-form');
	var fileSelect = document.getElementById('file-select');
	var uploadButton = document.getElementById('upload-button');
	
	form.onsubmit = function(event) {
		event.preventDefault();
		
		uploadButton.innerHTML = 'Uploading...';

		var files = fileSelect.files;
		var file = files[0];
		var formData = new FormData();
		
		
		if (uploadType == "image") 
		{
			extType = [".EPS", ".TIF", ".TIFF", ".JPG", ".JPEG", ".GIF", ".PNG"];
		}
		else
		{
			extType = [".XLS"];
		}
		
		if ($.inArray(file.name.substr(file.name.length - 4).toUpperCase(), extType) > -1)
		{
			formData.append('uploadItem', file, file.name);
			
			var xhr = new XMLHttpRequest();
			
			xhr.open('POST', url + '?action=upload', true);
			xhr.onload = function () {
			  if (xhr.status === 200) {
				// File(s) uploaded.
				uploadButton.innerHTML = 'Upload';
				$("#" + targetID).val(file.name);
				$("#fileUploader").dialog( "close" );
			  } else {
			  console.log(xhr.responseText);
				alert(xhr.status + xhr.responseText);
			  }
			};
			xhr.send(formData);
		}
		else
		{
			alert("invalid file type");
		}
		
		return false;
	};
	
}

function ValidationObject(dialid, name, type, min, max, message) {
    this.dialid = dialid;
    this.name = name;
    this.type = type;
    this.min = min;
	this.max = max;
	this.message = message;
}

function loadExistingDialValues(productID, ticketID)
{
	var dialType = "";
	$.ajax({
		type: "POST",
		url: url,
		async:true,
		data: "action=loadExistingDialValues&productID=" + productID + "&ticketID=" +ticketID, // serializes the form's elements.
		success: function(data)
		{
			
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					var actualData = data[key];
					console.log(actualData);
					if ( actualData["intControlType"] == "11")
					{
						dialType = "select";
						console.log(dialType);
						//$("#dialid-" + actualData["intDialID"] + " option[value=" + actualData["vchDialValue"] +"]").attr("selected", true);
						$("#dialid-" + actualData["intDialID"]).prop("selectedValue", actualData["vchDialValue"]);
					}
					$("#dialid-" + actualData["intDialID"]).val(actualData["vchDialValue"]);
					console.log("#dialid-" + actualData["intDialID"] + " " + actualData["vchDialValue"]);
				}
			}
		},
		error: function(data)
		{
			console.log(data);
		}
	});
}

function drawTopBreadcrumbs(productID)
{
	var message = "";
	var ajaxedChecks = ""
	
	message += "<div class=\"row\"><div class=\"col-md-12 breadcrumbBG\"><ul class=\"nav nav-pills nav-justified templateBreadcrumb\">";
	
	$.ajax({
		type: "POST",
		url: url,
		async:true,
		data: "action=checkForRecipientList&productID=" + productID, // serializes the form's elements.
		success: function(data)
		{
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					var actualData = data[key];
							
					if (actualData["boolRecipientList"] == 1)
					{
						ajaxedChecks += "<li id=\"recipientListHeader\"><a href=\"#\" onclick=\"LoadRecipientListPage(" + productID + ")\">Recipient List</a></li>";
						$(".templateBreadcrumb").prepend(ajaxedChecks);
						break;
					}
				}
			}
			$.ajax({
				type: "POST",
				url: url,
				async:true,
				data: "action=checkForDials&productID=" + productID, // serializes the form's elements.
				success: function(data)
				{
					for (var key in data) {
						if (data.hasOwnProperty(key)) {
							var actualData = data[key];
							//console.log(actualData["intDialID"]);
							if (actualData["intDialID"] > 0)
							{
								//console.log("here");
								ajaxedChecks += "<li class=\"active\" id=\"customizeHeader\"><a href=\"#\">Customize</a></li>";
								$(".templateBreadcrumb").prepend(ajaxedChecks);
								break;
							}
						}
					}
				},
				error: function(err) {
					console.log(err);
				}
			});
		}
	});
	
          			
	
    message += "<li id=\"finalizeHeader\"><a href=\"#\">Finalize</a></li>";
    message += "</ul>";
    message += "</div>";
            
    message += "</div>"
	
	$("#topBreadcrumbArea").html(message);
	
}

function AddToCart(productID)
{
	checkLogin();
	var quantityError = false;
	if ($.isNumeric($("#copies").val()) == false && $("#copies").val() <= 0)
	{	
		$("#quantityError").html("<strong>Please enter a positive quantity</strong>");
		return false;
	}
	
	$.ajax({
		type: "POST",
		url: url,
		async:false,
		data: "action=validateMinMax&productID=" + productID + "&amount=" + $("#copies").val(), // serializes the form's elements.
		success: function(data)
		{
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					var actualData = data[key];
					if (actualData["maxLimit"])
					{
						$("#quantityError").html("<strong>You cannot order more than " + actualData["maxLimit"] + " printed pieces</strong>");
						quantityError = true;
					}
					else if (actualData["minLimit"])
					{
						$("#quantityError").html("<strong>You must order at least " + actualData["minLimit"] + " printed pieces</strong>");
						quantityError = true;
					}
					console.log(data);
				}
			}
			
		},
		error: function(data)
		{
			console.log(data);
		}
		
	});
	
	if (quantityError)
	{
		return false;
	}
	
	$.ajax({
		type: "POST",
		url: url,
		async:true,
		data: "action=SavePODToCart&productID=" + productID + "&" + $("#finalize").serialize(), // serializes the form's elements.
		success: function(data)
		{
			
			var message= "<tr><th style=\"width:20px\"></th><th>Item</th><th>Type</th><th>Price</th><th>Print Quantity</th></tr>";
			var errors = "";
			var errorLine = [];
			var totalCost = 0;
			var podCost = 0;
	
			$.ajax({
				type: "POST",
				url: url,
				async:false,
				data: "action=validateLTICart", 
				success: function(data)
				{
					if (data)
					{
						for (var key in data) {
							if (data.hasOwnProperty(key)) {
								var actualData = data[key];
								errors += actualData["errMessage"] + "<br />"
								errorLine.push(actualData["lineItemID"]);
							}
						}
					}
				},
				error: function(data)
				{
					console.log(data);
				}
			});
				
			$.ajax({
				type: "POST",
				url: url,
				async:false,
				data: "action=getCartLTI", 
				success: function(data)
				{
					for (var key in data) {
						if (data.hasOwnProperty(key)) {
							var actualData = data[key];
							var errorBackground = "";
							if (errorLine.indexOf(actualData["intID"].toString()) > -1)
							{
								errorBackground = "style=\"color: red;\"";
							}
							//console.log(actualData["intTrueQuantity"]);
							message += "<tr " + errorBackground + "><td " + errorBackground + "><img src=\"../images/products/big/" + actualData["vchImageURL"] + "\" height=\"90px\" /></td><td " + errorBackground + ">" + actualData["vchItemName"] + "<br /><a href=\"#\" onclick=\"removeItem('LTI', " + actualData["intID"] + "); return false;\" style=\"color: red;\">Remove</a></td><td " + errorBackground + ">Inventory</td><td>$" + actualData["mnyPrice"].toFixed(2) + "</td><td><input type=\"text\" name=\"intQty_" + actualData["intOrderID"] + "_" + actualData["intID"] + "\" value=\"" + actualData["intQuantity"] + "\" size=\"10\" /><input type=\"submit\" onclick=\"updateItems(); return false;\" value=\"Update Qty\" /></td></tr>";
							errorBackground = "";
							if (isNaN(actualData["mnyPrice"]) == false)
							{
								totalCost += parseFloat(actualData["mnyPrice"]);
							}
						}
					}
					//console.log(message);
					$.ajax({
						type: "POST",
						url: url,
						async:true,
						data: "action=getCart",
						success: function(data)
						{
							for (var key in data) {
									if (data.hasOwnProperty(key)) {
									var actualData = data[key];
									//console.log(actualData["intTrueQuantity"]);
									if (errorLine.indexOf(actualData["intID"].toString()) > -1)
									{
										errorBackground = "style=\"color: red;\"";
									}
									message += "<tr><td><img src=\"data:image/png;base64," + actualData["txtImage64"].substring(0, actualData["txtImage64"].length - 1) + "\" height=\"90px\" /></td><td>" + actualData["vchName"] + "<br /><a href=\"#\" onclick=\"getProofFrame(" + actualData["intProofJobTicketID"] + "); return false\">View Proof</a>&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick=\"removeItem('POD', " + actualData["intTicketID"] + "); return false;\" style=\"color: red;\">Remove</a></td><td>Customized Print</td><td>$" + actualData["mnyPrice"].toFixed(2) + "</td>";
									console.log( actualData["intMinQty"] + " " + actualData["intMaxQty"]);
									if (actualData["intMinQty"] == actualData["intMaxQty"] && actualData["intMaxQty"] != null && actualData["intMinQty"] != null)
									{
										message += "<td>" +actualData["intTrueQuantity"];
									}
									else
									{
										message += "<td><input type=\"number\" name=\"intQty_" + actualData["intOrderID"] + "_" + actualData["intID"] + "\" id=\"copies-" + actualData["intID"] + "\" style=\"width:50px\" onkeyup=\"CalculateTotalCart(" + actualData["intID"] + ")\" onchange=\"CalculateTotalCart(" + actualData["intID"] + ")\" value=\"" + actualData["intQuantity"] + "\"> x <span id=\"bundleQuantity-" + actualData["intID"] + "\">" + actualData["vchBundleQuantity"] + "</span> = <span id=\"bundleTotal-" + actualData["intID"] + "\">" + actualData["intTrueQuantity"] + "</span> printed pieces&nbsp;&nbsp;<input type=\"submit\" onclick=\"updateItems(); return false;\" value=\"Update Qty\" />";
									}
									message += "</td></tr>";
									errorBackground = "";
									if (isNaN(actualData["mnyPrice"]) == false)
									{
										totalCost += parseFloat(actualData["mnyPrice"]);
										podCost += parseFloat(actualData["mnyPrice"]);
									}
								}
							}
							if (message.indexOf("<img") == -1)
							{
								message = "<tr><td colspan=\"5\"><center>Your cart is empty.</center></td></tr>";
							}
							else
							{
								message += "<tr><td colspan=\"3\">&nbsp;</td><td><b>Cart total: $" + totalCost.toFixed(2) + "</b></td><td>&nbsp;</td></tr>";
								message += "<tr><td colspan=\"3\">&nbsp;</td><td><b>Charged to store: $" + podCost.toFixed(2) + "</b></td><td>&nbsp;</td></tr>";
							}
							message += "<br />";
							$("#CPCartFull").html(message);
							$("#topBreadcrumbArea").hide();
							$("#productGroups").hide();
							$("#dialArea").hide();
							$("#products").hide();
							$("#cartPage").show();
							$("#productPreview").hide();
							$("#fullwidth").hide();
						},
						error: function(data)
						{
							console.log(data);
						}
					});
					$("#errorTextFull").html(errors);
				},
				error: function(data)
				{
					console.log(data);
				}
			});
		},
		error: function(data)
		{
			console.log(data);
		}
	});
}

function AddAndCreateNew()
{
	loadProductGroups();
}

function CompleteOrder()
{
	$.ajax({
		type: "POST",
		url: url,
		async:false,
		data: "action=validateLTICart", 
		success: function(data)
		{
			if (data)
			{
				DisplayCart();
			}
			else
			{
				window.location.assign("../store/default.asp?action=checkout&POD=true")
			}
		},
		error: function(data)
		{
			console.log(data);
		}
	});
}

function DisplayCart()
{
	checkLogin();
	var message= "<tr><th style=\"width:20px\"></th><th>Item</th><th>Type</th><th>Price</th><th>Quantity</th></tr>";
	var errors = "";
	var errorLine = [];
	var totalCost = 0;
	var podCost = 0;
	
	$.ajax({
		type: "POST",
		url: url,
		async:false,
		data: "action=validateLTICart", 
		success: function(data)
		{
			if (data)
			{
				for (var key in data) {
					if (data.hasOwnProperty(key)) {
						var actualData = data[key];
						errors += actualData["errMessage"] + "<br />"
						errorLine.push(actualData["lineItemID"]);
					}
				}
				console.log(errors);
			}
		},
		error: function(data)
		{
			console.log(data);
		}
	});
	
	$.ajax({
		type: "POST",
		url: url,
		async:false,
		data: "action=getCartLTI", 
		success: function(data)
		{
			for (var key in data) {
					if (data.hasOwnProperty(key)) {
					var actualData = data[key];
					var errorBackground = "";
					if (errorLine.indexOf(actualData["intID"].toString()) > -1)
					{
						errorBackground = "style=\"color: red;\"";
					}
					//console.log(actualData["intTrueQuantity"]);
					message += "<tr " + errorBackground + "><td " + errorBackground + "><img src=\"../images/products/big/" + actualData["vchImageURL"] + "\" height=\"90px\" /></td><td " + errorBackground + ">" + actualData["vchItemName"] + "<br /><a href=\"#\" onclick=\"removeItem('LTI', " + actualData["intID"] + "); return false;\" style=\"color: red;\">Remove</a></td><td " + errorBackground + ">Inventory</td><td>$" + actualData["mnyPrice"].toFixed(2) + "</td><td><input type=\"text\" name=\"intQty_" + actualData["intOrderID"] + "_" + actualData["intID"] + "\" value=\"" + actualData["intQuantity"] + "\" size=\"10\" /><input type=\"submit\" onclick=\"updateItems(); return false;\" value=\"Update Qty\" /></td></tr>";
					errorBackground = "";
					if (isNaN(actualData["mnyPrice"]) == false)
					{
						totalCost += parseFloat(actualData["mnyPrice"]);
					}
				}
			}
			//console.log(message);
			$.ajax({
				type: "POST",
				url: url,
				async:true,
				data: "action=getCart",
				success: function(data)
				{
					for (var key in data) {
							if (data.hasOwnProperty(key)) {
							var actualData = data[key];
							//console.log(actualData["intTrueQuantity"]);
							if (errorLine.indexOf(actualData["intID"].toString()) > -1)
							{
								errorBackground = "style=\"color: red;\"";
							}
							message += "<tr><td " +errorBackground + "><img src=\"data:image/png;base64," + actualData["txtImage64"].substring(0, actualData["txtImage64"].length - 1) + "\" height=\"90px\" /></td><td " + errorBackground + ">" + actualData["vchName"] + "<br /><a href=\"#\" onclick=\"getProofFrame(" + actualData["intProofJobTicketID"] + "); return false\">View Proof</a>&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick=\"removeItem('POD', " + actualData["intTicketID"] + "); return false;\" style=\"color: red;\">Remove</a></td><td " +errorBackground + ">Customized Print</td><td>$" + actualData["mnyPrice"].toFixed(2) + "</td>";
							console.log( actualData["intMinQty"] + " " + actualData["intMaxQty"]);
							
							if (actualData["intMinQty"] == actualData["intMaxQty"] && actualData["intMaxQty"] != null && actualData["intMinQty"] != null)
							{
								message += "<td " + errorBackground + ">" +actualData["intTrueQuantity"];
							}
							else
							{
								message += "<td " + errorBackground + "><input style=\"color: black; width:89.1406px;\" type=\"number\" name=\"intQty_" + actualData["intOrderID"] + "_" + actualData["intID"] + "\" id=\"copies-" + actualData["intID"] + "\" style=\"width:50px\" onkeyup=\"CalculateTotalCart(" + actualData["intID"] + ")\" onchange=\"CalculateTotalCart(" + actualData["intID"] + ")\" value=\"" + actualData["intQuantity"] + "\"> x <span id=\"bundleQuantity-" + actualData["intID"] + "\">" + actualData["vchBundleQuantity"] + "</span> = <span id=\"bundleTotal-" + actualData["intID"] + "\">" + actualData["intTrueQuantity"] + "</span> printed pieces&nbsp;&nbsp;<input type=\"submit\" onclick=\"updateItems(); return false;\" value=\"Update Qty\" style=\"color: black\" />";
							}
							message += "</td></tr>";
							errorBackground = "";
							if (isNaN(actualData["mnyPrice"]) == false)
							{
								totalCost += parseFloat(actualData["mnyPrice"]);
								podCost += parseFloat(actualData["mnyPrice"]);
							}
						}
					}
					//console.log(message);
					if (message.indexOf("<img") == -1)
					{
						message = "<tr><td colspan=\"5\"><center>Your cart is empty.</center></td></tr>";
					}
					else
					{
						message += "<tr><td colspan=\"3\">&nbsp;</td><td><b>Cart total: $" + totalCost.toFixed(2) + "</b></td><td>&nbsp;</td></tr>";
						message += "<tr><td colspan=\"3\">&nbsp;</td><td><b>Charged to store: $" + podCost.toFixed(2) + "</b></td><td>&nbsp;</td></tr>";
					}
					message += "<br />";
					$("#CPCart").html(message);
					$( "#shoppingCart" ).dialog({
						modal: true,
						draggable: false,
						resizable: false,
						width: "65%",
						minHeight: 400,
						buttons: [
							{
								text: "Proceed To Checkout",
								"class":"PopChckoutBtn",
								click: function() {
									CompleteOrder();
									return false;
								}
							}
						]
					});
				},
				error: function(data)
				{
					console.log(data);
				}
			});
			$("#errorText").html(errors);
		},
		error: function(data)
		{
			console.log(data);
		}
	});
	
	

}

function OrderHistory(OrderID)
{
	checkLogin();
	var message= "<tr><th style=\"width:20px\"></th><th>Item</th><th>Type</th><th>Quantity</th></tr>";
	var errors = "";
	var errorLine = [];

	$.ajax({
		type: "POST",
		url: url,
		async:false,
		data: "action=getCartLTI&orderID=" + OrderID, 
		success: function(data)
		{
			for (var key in data) {
					if (data.hasOwnProperty(key)) {
					var actualData = data[key];
					//console.log(actualData["intTrueQuantity"]);
					message += "<tr><td><img src=\"../images/products/big/" + actualData["vchImageURL"] + "\" height=\"90px\" /></td><td>" + actualData["vchItemName"] + "</td><td>Inventory</td><td>" + actualData["intQuantity"] + "</td></tr>";
				}
			}
			//console.log(message);
			$.ajax({
				type: "POST",
				url: url,
				async:true,
				data: "action=getCart&orderID=" + OrderID,
				success: function(data)
				{
					for (var key in data) {
							if (data.hasOwnProperty(key)) {
							var actualData = data[key];
							//console.log(actualData["intTrueQuantity"]);
							message += "<tr><td><img src=\"data:image/png;base64," + actualData["txtImage64"].substring(0, actualData["txtImage64"].length - 1) + "\" height=\"90px\" /></td><td>" + actualData["vchName"] + "<br /><a href=\"#\" onclick=\"getProofFrame(" + actualData["intProofJobTicketID"] + "); return false\">View Proof</a></td><td>Customized Print</td>";
							console.log( actualData["intMinQty"] + " " + actualData["intMaxQty"]);
							message += "<td>" +actualData["intTrueQuantity"];
							message += "</td></tr>";
						}
					}
					//console.log(message);
					$("#HistoryTable").html(message);
				},
				error: function(data)
				{
					console.log(data);
				}
			});
		},
		error: function(data)
		{
			console.log(data);
		}
	});
	
	

}

function updateItems()
{
	var quantityData = ""
	quantityData = $("#CartForm").serialize();
	$.ajax({
		type: "POST",
		url: url,
		async:true,
		data: "action=updateItems&" + quantityData, // serializes the form's elements.
		success: function(data)
		{
			console.log(quantityData);
			if ($("#shoppingCart").hasClass("ui-dialog-content") && $("#shoppingCart").dialog( "isOpen" ))
			{
				$( "#shoppingCart" ).dialog("close");
				DisplayCart();
			}
			else
			{
				var message= "<tr><th style=\"width:20px\"></th><th>Item</th><th>Type</th><th>Price</th><th>Print Quantity</th></tr>";
				var errors = "";
				var errorLine = [];
				var totalCost = 0;
				var podCost = 0;
	
				$.ajax({
					type: "POST",
					url: url,
					async:false,
					data: "action=validateLTICart", 
					success: function(data)
					{
						if (data)
						{
							for (var key in data) {
								if (data.hasOwnProperty(key)) {
									var actualData = data[key];
									errors += actualData["errMessage"] + "<br />"
									errorLine.push(actualData["lineItemID"]);
								}
							}
						}
					},
					error: function(data)
					{
						console.log(data);
					}
				});
				
				$.ajax({
					type: "POST",
					url: url,
					async:false,
					data: "action=getCartLTI", 
					success: function(data)
					{
						for (var key in data) {
								if (data.hasOwnProperty(key)) {
								var actualData = data[key];
								var errorBackground = "";
								if (errorLine.indexOf(actualData["intID"].toString()) > -1)
								{
									errorBackground = "style=\"color: red;\"";
								}
								//console.log(actualData["intTrueQuantity"]);
								message += "<tr " + errorBackground + "><td " + errorBackground + "><img src=\"../images/products/big/" + actualData["vchImageURL"] + "\" height=\"90px\" /></td><td " + errorBackground + ">" + actualData["vchItemName"] + "<br /><a href=\"#\" onclick=\"removeItem('LTI', " + actualData["intID"] + "); return false;\" style=\"color: red;\">Remove</a></td><td " + errorBackground + ">Inventory</td><td>$" + actualData["mnyPrice"].toFixed(2) + "</td><td><input type=\"text\" name=\"intQty_" + actualData["intOrderID"] + "_" + actualData["intID"] + "\" value=\"" + actualData["intQuantity"] + "\" size=\"10\" /><input type=\"submit\" onclick=\"updateItems(); return false;\" value=\"Update Qty\" /></td></tr>";
								errorBackground = "";
								if (isNaN(actualData["mnyPrice"]) == false)
								{
									totalCost += parseFloat(actualData["mnyPrice"]);
								}
							}
						}
						//console.log(message);
						$.ajax({
							type: "POST",
							url: url,
							async:true,
							data: "action=getCart",
							success: function(data)
							{
								for (var key in data) {
										if (data.hasOwnProperty(key)) {
										var actualData = data[key];
										if (errorLine.indexOf(actualData["intID"].toString()) > -1)
										{
											errorBackground = "style=\"color: red;\"";
										}
										//console.log(actualData["intTrueQuantity"]);
										message += "<tr><td><img src=\"data:image/png;base64," + actualData["txtImage64"].substring(0, actualData["txtImage64"].length - 1) + "\" height=\"90px\" /></td><td>" + actualData["vchName"] + "<br /><a href=\"#\" onclick=\"getProofFrame(" + actualData["intProofJobTicketID"] + "); return false\">View Proof</a>&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick=\"removeItem('POD', " + actualData["intTicketID"] + "); return false;\" style=\"color: red;\">Remove</a></td><td>Customized Print</td><td>$" + actualData["mnyPrice"].toFixed(2) + "</td>";
										console.log( actualData["intMinQty"] + " " + actualData["intMaxQty"]);
										if (actualData["intMinQty"] == actualData["intMaxQty"] && actualData["intMaxQty"] != null && actualData["intMinQty"] != null)
										{
											message += "<td>" +actualData["intTrueQuantity"];
										}
										else
										{
											message += "<td><input type=\"number\" name=\"intQty_" + actualData["intOrderID"] + "_" + actualData["intID"] + "\" id=\"copies-" + actualData["intID"] + "\" style=\"width:50px\" onkeyup=\"CalculateTotalCart(" + actualData["intID"] + ")\" onchange=\"CalculateTotalCart(" + actualData["intID"] + ")\" value=\"" + actualData["intQuantity"] + "\"> x <span id=\"bundleQuantity-" + actualData["intID"] + "\">" + actualData["vchBundleQuantity"] + "</span> = <span id=\"bundleTotal-" + actualData["intID"] + "\">" + actualData["intTrueQuantity"] + "</span> printed pieces&nbsp;&nbsp;<input type=\"submit\" onclick=\"updateItems(); return false;\" value=\"Update Qty\" />";
										}
										message += "</td></tr>";
										errorBackground = "";
										if (isNaN(actualData["mnyPrice"]) == false)
										{
											totalCost += parseFloat(actualData["mnyPrice"]);
											podCost += parseFloat(actualData["mnyPrice"]);
										}
									}
								}
								if (message.indexOf("<img") == -1)
								{
									message = "<tr><td colspan=\"5\"><center>Your cart is empty.</center></td></tr>";
								}
								else
								{
									message += "<tr><td colspan=\"3\">&nbsp;</td><td><b>Cart total: $" + totalCost.toFixed(2) + "</b></td><td>&nbsp;</td></tr>";
									message += "<tr><td colspan=\"3\">&nbsp;</td><td><b>Charged to store: $" + podCost.toFixed(2) + "</b></td><td>&nbsp;</td></tr>";
								}
								message += "<br />";
								$("#CPCartFull").html(message);
							},
							error: function(data)
							{
								console.log(data);
							}
						});
						$("#errorTextFull").html(errors);
						
						$("#topBreadcrumbArea").html("");
						$("#topBreadcrumbArea").hide();
						$("#productGroups").hide();
						$("#dialArea").hide();
						$("#products").hide();
						$("#cartPage").show();
						$("#productPreview").hide();
						$("#fullwidth").hide();
					},
					error: function(data)
					{
						console.log(data);
					}
				});
			}
		},
		error: function(data)
		{
			console.log(data);
		}
	});
}

function removeItem(strType, intTicketID)
{
	checkLogin();
	var IDType ="";
	if ( strType == "POD" )
	{
		IDType = "ticketID";
	}
	else
	{
		IDType = "lineID";
	}
	$.ajax({
		type: "POST",
		url: url,
		async:true,
		data: "action=removeItem&" + IDType + "=" + intTicketID, 
		success: function(data)
		{
			if ($("#shoppingCart").hasClass("ui-dialog-content") && $("#shoppingCart").dialog( "isOpen" ))
			{
				$( "#shoppingCart" ).dialog("close");
				DisplayCart();
			}
			else
			{
				var message= "<tr><th style=\"width:20px\"></th><th>Item</th><th>Type</th><th>Price</th><th>Print Quantity</th></tr>";
				var errors = "";
				var errorLine = [];
				var totalCost = 0;
				var podCost = 0;
	
				$.ajax({
					type: "POST",
					url: url,
					async:false,
					data: "action=validateLTICart", 
					success: function(data)
					{
						if (data)
						{
							for (var key in data) {
								if (data.hasOwnProperty(key)) {
									var actualData = data[key];
									errors += actualData["errMessage"] + "<br />"
									errorLine.push(actualData["lineItemID"]);
								}
							}
						}
					},
					error: function(data)
					{
						console.log(data);
					}
				});
				
				$.ajax({
					type: "POST",
					url: url,
					async:false,
					data: "action=getCartLTI", 
					success: function(data)
					{
						for (var key in data) {
								if (data.hasOwnProperty(key)) {
								var actualData = data[key];
								var errorBackground = "";
								if (errorLine.indexOf(actualData["intID"].toString()) > -1)
								{
									errorBackground = "style=\"color: red;\"";
								}
								//console.log(actualData["intTrueQuantity"]);
								message += "<tr " + errorBackground + "><td " + errorBackground + "><img src=\"../images/products/big/" + actualData["vchImageURL"] + "\" height=\"90px\" /></td><td " + errorBackground + ">" + actualData["vchItemName"] + "<br /><a href=\"#\" onclick=\"removeItem('LTI', " + actualData["intID"] + "); return false;\" style=\"color: red;\">Remove</a></td><td " + errorBackground + ">Inventory</td><td>$" + actualData["mnyPrice"].toFixed(2) + "</td><td><input type=\"text\" name=\"intQty_" + actualData["intOrderID"] + "_" + actualData["intID"] + "\" value=\"" + actualData["intQuantity"] + "\" size=\"10\" /><input type=\"submit\" onclick=\"updateItems(); return false;\" value=\"Update Qty\" /></td></tr>";
								if (isNaN(actualData["mnyPrice"]) == false)
								{
									totalCost += parseFloat(actualData["mnyPrice"]);
								}
							}
						}
						//console.log(message);
						$.ajax({
							type: "POST",
							url: url,
							async:true,
							data: "action=getCart",
							success: function(data)
							{
								for (var key in data) {
										if (data.hasOwnProperty(key)) {
										var actualData = data[key];
										//console.log(actualData["intTrueQuantity"]);
										message += "<tr><td><img src=\"data:image/png;base64," + actualData["txtImage64"].substring(0, actualData["txtImage64"].length - 1) + "\" height=\"90px\" /></td><td>" + actualData["vchName"] + "<br /><a href=\"#\" onclick=\"getProofFrame(" + actualData["intProofJobTicketID"] + "); return false\">View Proof</a>&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick=\"removeItem('POD', " + actualData["intTicketID"] + "); return false;\" style=\"color: red;\">Remove</a></td><td>Customized Print</td><td>$" + actualData["mnyPrice"].toFixed(2) + "</td>";
										console.log( actualData["intMinQty"] + " " + actualData["intMaxQty"]);
										if (actualData["intMinQty"] == actualData["intMaxQty"] && actualData["intMaxQty"] != null && actualData["intMinQty"] != null)
										{
											message += "<td>" +actualData["intTrueQuantity"];
										}
										else
										{
											message += "<td><input type=\"number\" name=\"intQty_" + actualData["intOrderID"] + "_" + actualData["intID"] + "\" id=\"copies-" + actualData["intID"] + "\" style=\"width:50px\" onkeyup=\"CalculateTotalCart(" + actualData["intID"] + ")\" onchange=\"CalculateTotalCart(" + actualData["intID"] + ")\" value=\"" + actualData["intQuantity"] + "\"> x <span id=\"bundleQuantity-" + actualData["intID"] + "\">" + actualData["vchBundleQuantity"] + "</span> = <span id=\"bundleTotal-" + actualData["intID"] + "\">" + actualData["intTrueQuantity"] + "</span> printed pieces&nbsp;&nbsp;<input type=\"submit\" onclick=\"updateItems(); return false;\" value=\"Update Qty\" />";
										}
										message += "</td></tr>";
										if (isNaN(actualData["mnyPrice"]) == false)
										{
											totalCost += parseFloat(actualData["mnyPrice"]);
											podCost += parseFloat(actualData["mnyPrice"]);
										}
									}
								}
								if (message.indexOf("<img") == -1)
								{
									message = "<tr><td colspan=\"5\"><center>Your cart is empty.</center></td></tr>";
								}
								else
								{
									message += "<tr><td colspan=\"3\">&nbsp;</td><td><br>Cart total: $" + totalCost.toFixed(2) + "</b></td><td>&nbsp;</td></tr>";
									message += "<tr><td colspan=\"3\">&nbsp;</td><td><b>Charged to store: $" + podCost.toFixed(2) + "</b></td><td>&nbsp;</td></tr>";
								}
								message += "<br />";
								$("#CPCartFull").html(message);
							},
							error: function(data)
							{
								console.log(data);
							}
						});
						$("#errorTextFull").html(errors);
					},
					error: function(data)
					{
						console.log(data);
					}
				});
			}
		},
		error: function(data)
		{
			console.log(data);
		}
	});

}

function editFromCart(ticketID)
{
	var uStoreID = 0;
	var productName = "";
	var breadcumbs = "";
	var image64 = "";
	checkLogin();
	
	$.ajax({
		type: "POST",
		url: url,
		async:true,
		data: "action=getTicketInfo&ticketID=" + ticketID, 
		success: function(data)
		{
			for (var key in data) {
				if (data.hasOwnProperty(key)) {
					var actualData = data[key];
					breadCrumb = "<li id=\"bcProductGroup\" style=\"font-weight:bold\"><i class=\"fa fa-angle-right\"></i><a href=\"#\" onclick=\"javascript:loadProductsInGroup(" + actualData["GroupNum"] + ",'" + actualData["GroupName"] + "'); return false\">" + actualData["GroupName"] + "</a></li>";
					uStoreID = actualData["intUStoreID"];
					productName = actualData["ProductName"];
					image64 = actualData["txtImage64"];
					var images = image64.split("|");
					loadProductStep(uStoreID, productName, images[0])
					//alter the save dials function to look to see if it is being passed a ticket to save to
					
				}
			}
		},
		error: function(data)
		{
			console.log(data);
		}
	});
	$("#topBreadcrumbArea").hide();
}

function getProofFrame(intProofTicketID)
{
	checkLogin();
	var message = "";
	var iframeURL = "";

	$.ajax({
		type: "POST",
		url: url,
		async:true,
		data: "action=getProof&intProofTicketID=" + intProofTicketID, 
		success: function(data)
		{
			var actualData = data[0];
			iframeURL = actualData["proofURL"];
			//window.open(iframeURL, "_blank");
			message = "<iframe src=\"" + window.location.protocol + "//" + window.location.host + "/ustoretesting/js/web/viewer.html?proofURL=" + encodeURIComponent(iframeURL) + "\" width=\"100%\" height=\"800px\"></iframe>";
			$("#viewProof").html(message);
			
			$( "#viewProof" ).dialog({
				modal: true,
				draggable: false,
				resizable: false,
				width: "45%",
				height: 700
			});
			
		},
		error: function(data)
		{
			console.log(data);
		}
	});
}

function CalculateTotal()
{

	var total;
	var priceTotal;
	var quantity = $("#copies").val();
	var price = $("#price").val();
	var bundle = $("#bundleQuantity").html();
	
	total = quantity * bundle;
	priceTotal = quantity * price;
	$("#bundleTotal").html(total);
	$("#pricing").html(priceTotal);
	//console.log(quantity);
	//console.log(bundle);
	//console.log(total);

}

function CalculateTotalCart(id)
{

	var total;
	var quantity = $("#copies-" + id).val();
	var bundle = $("#bundleQuantity-" +id).html();
	
	total = quantity * bundle;
	$("#bundleTotal-" + id).html(total);
	//console.log(quantity);
	//console.log(bundle);
	//console.log(total);

}

function checkLogin()
{

	$.ajax({
		type: "POST",
		url: url,
		async:true,
		data: "action=checkLogin", 
		success: function(data)
		{
			var actualData = data[0];
			if (actualData["login"] == "false")
			{
				window.location.assign("/default.asp");
			}
		},
		error: function(data)
		{
			console.log(data)
		}
	});

}

function show(divToShow)
{
	$( "div" ).each(function() {
		if( $(this).attr("id") != "header")
		{
			$(this).hide();
		}
	});

}
