<%@ LANGUAGE="VBScript" %>
<!--#include file="config/incInit.asp"-->
<%

' =====================================================================================
' = File: default.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2000 SacWeb, Inc. All rights reserved.
' = Description:
' =   GoCart Customer Service Center
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

'resetting this test here
Session("DummyUser")=""
OpenConn
dim strAction, strReturnURL, gtfError, gstrHintdiv, gstrHint

strReturnURL = Request("return")
if strReturnURL = "" then
	strReturnURL = "default.asp"
end if

strAction = lcase(Request("action"))

if strAction&""="logout" then
Session.Abandon
Response.Cookies(SESSION_PUB_USER_ID) = ""
Response.Cookies(SESSION_MERCHANT_UID) = ""
gintUserID=0
Response.Redirect "default.asp"
end if

if Request.Cookies(SESSION_PUB_USER_ID)&""<>"" Then 
    Session(SESSION_PUB_USER_ID) = Request.Cookies(SESSION_PUB_USER_ID)
    Session(SESSION_PUB_USER_NAME) = Request.Cookies(SESSION_PUB_USER_NAME)
	MakeAuditRecord "S", "login", Session(SESSION_PUB_USER_ID), "User: " & Request("email") & " (" & Session(SESSION_MERCHANT_UID) & ")"
    response.redirect virtualbase & "store/dashboard.asp"
End if

If Request.Cookies(SESSION_MERCHANT_UID)&""<>"" Then
    Session(SESSION_MERCHANT_UID) = Request.Cookies(SESSION_MERCHANT_UID)
    Session(SESSION_MERCHANT_ULOGIN) = Request.Cookies(SESSION_MERCHANT_ULOGIN)
    Session(SESSION_MERCHANT_UNAME) = Request.Cookies(SESSION_MERCHANT_UNAME)
    Session(SESSION_MERCHANT_UTIME) = Request.Cookies(SESSION_MERCHANT_UTIME)
    Session(SESSION_MERCHANT_ACCESS) = Request.Cookies(SESSION_MERCHANT_ACCESS)
    Session(SESSION_MERCHANT_ACCESS_STATUS) = Request.Cookies(SESSION_MERCHANT_ACCESS_STATUS)
	MakeAuditRecord "S", "login", Session(SESSION_PUB_USER_ID), "User: " & Request("email") & " (" & Session(SESSION_MERCHANT_UID) & ")"
    response.redirect virtualbase & "store/dashboard.asp"
End if

SelectCurrentOrder


select case strAction
	case "reset":
		' for debugging - abandon session and redirect to home page
		Session.Abandon
		response.redirect nonsecurebase & "default.asp"
	case "loginsubmit": ' login form submission
		if LoginUser(Request("email"), Request("password")) then
			'response.write strreturnurl
			strSQL = "SELECT dtmFirstLogin"
	        strSQL = strSQL & " FROM " & STR_TABLE_SHOPPER
	        strSQL = strSQL & " WHERE vchemail='" & SQLEncode(Request("email")) & "'"
	        strSQL = strSQL & " AND vchPassword='" & SQLEncode(Request("password")) & "'"
	        strSQL = strSQL & " AND chrStatus<>'D'"
	        set rsData = gobjConn.execute(strSQL)

            If rsData(0)&""="" Then
                strSQL = "UPDATE " & STR_TABLE_SHOPPER & " SET dtmFirstLogin=GETDATE()"
                strSQL = strSQL & " WHERE vchemail='" & SQLEncode(Request("email")) & "'"
	            strSQL = strSQL & " AND vchPassword='" & SQLEncode(Request("password")) & "'"

                gobjConn.execute(strSQL)
                Session("FirstLogin") = Now()
            Else
                Session("FirstLogin") = rsData(0)&""
            End If           



            'Check to see if they are also an admin
            dim strSQL, rsData
	        strSQL = "SELECT intID, chrStatus, vchUsername, vchFullName, vchCanViewStatus, vchAccess, intBrand"
	        strSQL = strSQL & " FROM " & STR_TABLE_USER
	        strSQL = strSQL & " WHERE vchUsername='" & SQLEncode(Request("email")) & "'"
	        strSQL = strSQL & " AND vchPassword='" & SQLEncode(Request("password")) & "'"
	        strSQL = strSQL & " AND chrStatus<>'D'"
	        set rsData = gobjConn.execute(strSQL)
	        if not rsData.eof then
		    
		        Session(SESSION_MERCHANT_UID) = rsData("intID")
		        Session(SESSION_MERCHANT_ULOGIN) = rsData("vchUsername")
		        Session(SESSION_MERCHANT_UNAME) = rsData("vchFullName")
		        Session(SESSION_MERCHANT_UTIME) = Now()
		        Session(SESSION_MERCHANT_ACCESS) = rsData("vchAccess")
		        Session(SESSION_MERCHANT_ACCESS_STATUS) = rsData("vchCanViewStatus")
		        if isValid(rsData("intBrand")) then
		        Session(SESSION_MERCHANT_BID) = rsData("intBrand")
		        end if

                if Request("remember")&""<>"" then
                    Response.Cookies(SESSION_MERCHANT_UID) = Session(SESSION_MERCHANT_UID)
                    Response.Cookies(SESSION_MERCHANT_ULOGIN) = Session(SESSION_MERCHANT_ULOGIN)
                    Response.Cookies(SESSION_MERCHANT_UNAME) = Session(SESSION_MERCHANT_UNAME)
                    Response.Cookies(SESSION_MERCHANT_UTIME) = Session(SESSION_MERCHANT_UTIME)
                    Response.Cookies(SESSION_MERCHANT_ACCESS) = Session(SESSION_MERCHANT_ACCESS)
                    Response.Cookies(SESSION_MERCHANT_ACCESS_STATUS) = Session(SESSION_MERCHANT_ACCESS_STATUS)

                    Response.Cookies(SESSION_MERCHANT_UID).Expires=DateAdd("D",30,Now)
                    Response.Cookies(SESSION_MERCHANT_ULOGIN).Expires=DateAdd("D",30,Now)
                    Response.Cookies(SESSION_MERCHANT_UNAME).Expires=DateAdd("D",30,Now)
                    Response.Cookies(SESSION_MERCHANT_UTIME).Expires=DateAdd("D",30,Now)
                    Response.Cookies(SESSION_MERCHANT_ACCESS).Expires=DateAdd("D",30,Now)
                    Response.Cookies(SESSION_MERCHANT_ACCESS_STATUS).Expires=DateAdd("D",30,Now)
                end if

		        rsData.close
		        set rsData = nothing
		        strSQL = "UPDATE " & STR_TABLE_USER & " SET dtmLastLogin=GETDATE() WHERE intID=" & Session(SESSION_MERCHANT_UID)
		        gobjConn.execute(strSQL)
		        MakeAuditRecord "S", "login", Session(SESSION_MERCHANT_UID), "User: " & Request("email") & " (" & Session(SESSION_MERCHANT_UID) & ")"

               

		        response.redirect virtualbase & "store/dashboard.asp"
	        end if
			
            if Request("remember")&""<>"" then
                Response.Cookies(SESSION_PUB_USER_ID) = Session(SESSION_PUB_USER_ID)
                Response.Cookies(SESSION_PUB_USER_NAME) = Session(SESSION_PUB_USER_NAME)

                Response.Cookies(SESSION_PUB_USER_ID).Expires=DateAdd("D",30,Now)
                Response.Cookies(SESSION_PUB_USER_NAME).Expires=DateAdd("D",30,Now)

            end if

			MakeAuditRecord "S", "login", Session(SESSION_PUB_USER_ID), "User: " & Request("email") & " (" & Session(SESSION_MERCHANT_UID) & ")"
			
             if Session("chrStatus") ="C" Then
             Session("Message") = "Please change your password"
                    response.Redirect "store/custserv.asp?action=accountinfo"
            else

			response.redirect virtualbase & "store/dashboard.asp"
            end if
		else
            dim intAuth
	
	        ' attempt to validate client user
	        strSQL = "SELECT intID, chrStatus, vchUsername, vchFullName, vchCanViewStatus, vchAccess, intBrand"
	        strSQL = strSQL & " FROM " & STR_TABLE_USER
	        strSQL = strSQL & " WHERE vchUsername='" & SQLEncode(Request("email")) & "'"
	        strSQL = strSQL & " AND vchPassword='" & SQLEncode(Request("password")) & "'"
	        strSQL = strSQL & " AND chrStatus<>'D'"
	        set rsData = gobjConn.execute(strSQL)
	        if rsData.eof then
		        rsData.close
		        set rsData = nothing
		        
		        MakeAuditRecord "S", "loginfail", lcase(Request("email")), "User: " & Request("email")
            elseif rsData("chrStatus") <> "A" and rsData("chrStatus") <> "C" then
		        rsData.close
		        set rsData = nothing
		        
		        MakeAuditRecord "S", "loginfail", lcase(Request("email")), "(DISABLED) User: " & Request("email")
	        else
				'if we get here then there was no shopper record
                CreateNewUser Request("email"), Request("password"), Request("strHint"), Request("email"), Request("strLastname"), Request("strAddress"), Request("strCity"), Request("strState"), Request("strZip"), Request("strCountry")
				
				strSQL = "update " & STR_TABLE_SHOPPER & " set vchGroups = '1' where vchusername = '" & Request("email") & "'"
				gobjConn.execute(strSQL)
				
		        Session(SESSION_MERCHANT_UID) = rsData("intID")
		        Session(SESSION_MERCHANT_ULOGIN) = rsData("vchUsername")
		        Session(SESSION_MERCHANT_UNAME) = rsData("vchFullName")
		        Session(SESSION_MERCHANT_UTIME) = Now()
		        Session(SESSION_MERCHANT_ACCESS) = rsData("vchAccess")
		        Session(SESSION_MERCHANT_ACCESS_STATUS) = rsData("vchCanViewStatus")
		        if isValid(rsData("intBrand")) then
		        Session(SESSION_MERCHANT_BID) = rsData("intBrand")
		        end if

                if Request("remember")&""<>"" then
                    Response.Cookies(SESSION_MERCHANT_UID) = Session(SESSION_MERCHANT_UID)
                    Response.Cookies(SESSION_MERCHANT_ULOGIN) = Session(SESSION_MERCHANT_ULOGIN)
                    Response.Cookies(SESSION_MERCHANT_UNAME) = Session(SESSION_MERCHANT_UNAME)
                    Response.Cookies(SESSION_MERCHANT_UTIME) = Session(SESSION_MERCHANT_UTIME)
                    Response.Cookies(SESSION_MERCHANT_ACCESS) = Session(SESSION_MERCHANT_ACCESS)
                    Response.Cookies(SESSION_MERCHANT_ACCESS_STATUS) = Session(SESSION_MERCHANT_ACCESS_STATUS)

                    Response.Cookies(SESSION_MERCHANT_UID).Expires=DateAdd("D",30,Now)
                    Response.Cookies(SESSION_MERCHANT_ULOGIN).Expires=DateAdd("D",30,Now)
                    Response.Cookies(SESSION_MERCHANT_UNAME).Expires=DateAdd("D",30,Now)
                    Response.Cookies(SESSION_MERCHANT_UTIME).Expires=DateAdd("D",30,Now)
                    Response.Cookies(SESSION_MERCHANT_ACCESS).Expires=DateAdd("D",30,Now)
                    Response.Cookies(SESSION_MERCHANT_ACCESS_STATUS).Expires=DateAdd("D",30,Now)
                end if

		        rsData.close
		        set rsData = nothing
		        strSQL = "UPDATE " & STR_TABLE_USER & " SET dtmLastLogin=GETDATE() WHERE intID=" & Session(SESSION_MERCHANT_UID)
		        gobjConn.execute(strSQL)
		        MakeAuditRecord "S", "login", Session(SESSION_MERCHANT_UID), "User: " & Request("email") & " (" & Session(SESSION_MERCHANT_UID) & ")"

               

		        response.redirect virtualbase & "store/dashboard.asp"
	        end if

			gtfError = true
			DrawHeader "Checkout Center", "custserv"
			DrawPage
			DrawFooter "custserv"
		end if
	case "hint": ' lookup hint
		DrawHeader "Lost Password", "custserv"
		DrawHintPage
		DrawFooter "custserv"
	case "newcustomer": ' create a new customer
		DrawHeader "New Customer", "custserv"
		DrawNewCustomerPage
		DrawFooter "custserv"
	case "newcustomersubmit": ' new customer form submission
		
        if Request("strEmail")&""="" then FormErrors.Add "strEmail","Email is Required"
        if Request("strPassword")&""="" then FormErrors.Add "strPassword","Password is Required"

        if Request("strFirstname")&""="" then FormErrors.Add "strFirstname","First Name is Required"
        if Request("strLastname")&""="" then FormErrors.Add "strLastname","Last Name is Required"

        'if Request("strAddress")&""="" then FormErrors.Add "strAddress","Address is Required"
        'if Request("strCity")&""="" then FormErrors.Add "strCity","City is Required"
        'if Request("strState")&""="" then FormErrors.Add "strState","State/Provence is Required"


		if not FormErrors.Exists("strConfirmPassword") and not FormErrors.Exists("strPassword") then
			if Request("strPassword") <> Request("strConfirmPassword") then
				FormErrors.Add "strConfirmPassword", "Please confirm your password by retyping it exactly."
			end if
		end if
		if FormErrors.count = 0 then
			if CreateNewUser(Request("strEmail"), Request("strPassword"), Request("strHint"), Request("strFirstname"), Request("strLastname"), Request("strAddress"), Request("strCity"), Request("strState"), Request("strZip"), Request("strCountry")) then
				
                if InStr(Request("strEmail"),"HBO.com")= 0 Then
                
                    gobjConn.execute "UPDATE " & STR_TABLE_SHOPPER & " SET chrStatus='I' WHERE intID=" & gintUserID
                    Session.Abandon
                    response.redirect "Default.asp?message=" & Server.URLEncode("Your account is awaiting approval.  You may log in once that is done.")
                Else
                    
                    response.redirect virtualbase & "store/dashboard.asp"
                End If
			else
				FormErrors.Add "strEmail", "An account already exists with this email address."
			end if
		end if
		DrawHeader "New Customer", "custserv"
		DrawNewCustomerPage
		DrawFooter "custserv"
		case else:
		if lcase(request("hint")) = "y" then
			if LookupHint(Request("email"), gstrHint) = false then
				gstrHint = "No account could be found with this email address."
			end if
		else
			gstrHint = ""
		end if
		DrawHeader "Checkout Service Center", "custserv"
		DrawPage
		DrawReturnPolicy
		DrawFooter "custserv"
end select
response.end


function GetSacWebUser(strUser, strPassword, intMinLevel)
	' minimum levels:
	'   15 = programmer admin
	'   25 = system admin (normal)  <-- ***
	'   35 = guest admin (read-only)
	'   45 = guest user (read-only, but not to admin pages)
	dim objAuth, intResult
	set objAuth = Server.CreateObject("IIS4Utils.SacWebSecurity")
	intResult = objAuth.Authenticate(strUser, strPassword)
	'if (intResult > 0) and (intResult <= intMinLevel) then
	'	GetSacWebUser = intResult
	'else
	'	GetSacWebUser = intResult
	'end if
	set objAuth = nothing
end function

sub DrawPage

	if gintUserID > 0 then
		DrawMenu
	else
		DrawLogin
	end if

end sub



sub DrawLogin

gintOrderID = Session(SESSION_PUB_ORDER_ID)




%>

<div class="content col-md-8 col-lg-6 col-xl-5 col-sm-12 mx-auto center">

	<!-- BEGIN LOGIN FORM -->
	<form class="login-form border border-dark p-3 bg-light mx-auto" action="default.asp" method="post" style="border-radius: 15px;max-width:500px">    
<% HiddenInput "action", "loginsubmit" %>
<% HiddenInput "return", strReturnURL %>
		<div class="row">
			<div class="page-logo d-flex justify-content-center">
				<a href="store/dashboard.asp">
				<img src="<%= virtualbase %>images/logo.svg" alt="logo" class="ml-auto" width="80px">
				</a>
			</div>		
		</div>
		<div class="row">
			<span class="h3 font-weight-light mx-auto">Login to your account</span>
		</div>
        <% if Request("Message")&""<>"" Then %>
        <div class="alert alert-danger">
			<button class="close" data-close="alert"></button>
			<span>
			<%= Request("Message")&"" %> </span>
		</div>
        <%end if %>
        <%if gtfError then %>
		<div class="alert alert-danger">
			<button class="close" data-close="alert"></button>
			<span>
			The username or password you entered is incorrect. Please try again. </span>
		</div>
        <%end if %>
        <%
        if gstrHint <> "" then
	        if gstrHint = "No account could be found with this email address." then
		     %>
		    <div class="alert alert-danger">
			    <button class="close" data-close="alert"></button>
			    <span>
			    <%= gstrHint %> </span>
		    </div>
            <%
	        elseif gstrHint = "none" then
		    %>
		    <div class="alert alert-danger">
			    <button class="close" data-close="alert"></button>
			    <span>
			    We are sorry, there is no hint on file for this account. </span>
		    </div>
            <% 
	        else
	         %>
		    <div class="alert alert-danger">
			    <button class="close" data-close="alert"></button>
			    <span>
			    <%response.write "The hint for your password is: <B>" & gstrHint  %> </span>
		    </div>
            <%
	        end if
        end if
         %>
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<div class="row">
				<div class="col-2 d-none d-md-block">
					<label>Username:</label>
				</div>
				<div class="col-xs-12 col-md-10 input-group">
					<div class="input-group-prepend bg-light"><i class="fa fa-user input-group-text"></i></div>
					<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="email"/>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-2 d-none d-md-block">
					<label>Password: </label>
				</div>
				<div class="col-xs-12 col-md-10 input-group">
					<div class="input-group-prepend bg-light"><i class="fa fa-lock input-group-text"></i></div>
					<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>
				</div>
			</div>
		</div>
		<div class="form-actions row w-100">
			<button type="submit" class="btn btn-dark ml-auto">
			Login <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>
		
		<!--
        <div class="create-account">
			<p>Need help logging in?<br />Contact Technical Support at
<a href="emailto:HBOsupport@domeprinting.com">HBOsupport@domeprinting.com</a><br /> or call 866-213-7402.

			</p>
		</div>
		-->
	</form>
</div>
<%
	


end sub

sub DrawNewCustomerPage
%>
<div class="content">
    <form  action="default.asp" method="post">
    <%
	    HiddenInput "action", "newcustomersubmit"
	    HiddenINput "return", strReturnURL
    %>
        <%
        If FormErrors.Count>0 Then
        %>
        <div>Please correct the following errors:</div>
        <ul style="color:Red;font-weight:bold;list-style:none;">
            <%
            Dim key
            For Each key in FormErrors.Keys
                Response.Write "<li>" & FormErrors(key) & "</li>"
            Next
            %>
        </ul>
        <%
        End If
         %>
        <h3>Sign Up</h3>
	    <p>
			    Enter your personal details below:
	    </p>
        <div class="form-group">
	        <label class="control-label visible-ie8 visible-ie9">Full Name</label>
	        <div class="row">
		        <div class="col-md-6">
			        <div class="input-icon">
				        <i class="fa fa-font"></i>
				        <input class="form-control placeholder-no-fix" type="text" placeholder="First Name" name="strFirstName"/>
			        </div>
		        </div>
		        <div class="col-md-6">
			        <div class="input-icon">
				        <i class="fa fa-font"></i>
				        <input class="form-control placeholder-no-fix" type="text" placeholder="Last Name" name="strLastName"/>
			        </div>
		        </div>
	        </div>
	    </div>
        <div class="form-group">
		    <label class="control-label visible-ie8 visible-ie9">Address</label>
		    <div class="input-icon">
			    <i class="fa fa-check"></i>
			    <input class="form-control placeholder-no-fix" type="text" placeholder="Address" name="strAddress"/>
		    </div>
	    </div>
	    <div class="form-group">
		    <label class="control-label visible-ie8 visible-ie9">City/Town</label>
		    <div class="input-icon">
			    <i class="fa fa-location-arrow"></i>
			    <input class="form-control placeholder-no-fix" type="text" placeholder="City/Town" name="strCity"/>
		    </div>
	    </div>
        <div class="form-group">
		    <label class="control-label visible-ie8 visible-ie9">State/Provence</label>
		    <div class="input-icon">
			    <i class="fa fa-location-arrow"></i>
			    <input class="form-control placeholder-no-fix" type="text" placeholder="State/Provence" name="strState"/>
		    </div>
	    </div>
        <div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Country</label>
			<select name="strCountry" id="select2_sample4" class="select2 form-control">
				<option value="US">United States</option>
				<option value="AF">Afghanistan</option>
				<option value="AL">Albania</option>
				<option value="DZ">Algeria</option>
				<option value="AS">American Samoa</option>
				<option value="AD">Andorra</option>
				<option value="AO">Angola</option>
				<option value="AI">Anguilla</option>
				<option value="AQ">Antarctica</option>
				<option value="AR">Argentina</option>
				<option value="AM">Armenia</option>
				<option value="AW">Aruba</option>
				<option value="AU">Australia</option>
				<option value="AT">Austria</option>
				<option value="AZ">Azerbaijan</option>
				<option value="BS">Bahamas</option>
				<option value="BH">Bahrain</option>
				<option value="BD">Bangladesh</option>
				<option value="BB">Barbados</option>
				<option value="BY">Belarus</option>
				<option value="BE">Belgium</option>
				<option value="BZ">Belize</option>
				<option value="BJ">Benin</option>
				<option value="BM">Bermuda</option>
				<option value="BT">Bhutan</option>
				<option value="BO">Bolivia</option>
				<option value="BA">Bosnia and Herzegowina</option>
				<option value="BW">Botswana</option>
				<option value="BV">Bouvet Island</option>
				<option value="BR">Brazil</option>
				<option value="IO">British Indian Ocean Territory</option>
				<option value="BN">Brunei Darussalam</option>
				<option value="BG">Bulgaria</option>
				<option value="BF">Burkina Faso</option>
				<option value="BI">Burundi</option>
				<option value="KH">Cambodia</option>
				<option value="CM">Cameroon</option>
				<option value="CA">Canada</option>
				<option value="CV">Cape Verde</option>
				<option value="KY">Cayman Islands</option>
				<option value="CF">Central African Republic</option>
				<option value="TD">Chad</option>
				<option value="CL">Chile</option>
				<option value="CN">China</option>
				<option value="CX">Christmas Island</option>
				<option value="CC">Cocos (Keeling) Islands</option>
				<option value="CO">Colombia</option>
				<option value="KM">Comoros</option>
				<option value="CG">Congo</option>
				<option value="CD">Congo, the Democratic Republic of the</option>
				<option value="CK">Cook Islands</option>
				<option value="CR">Costa Rica</option>
				<option value="CI">Cote d'Ivoire</option>
				<option value="HR">Croatia (Hrvatska)</option>
				<option value="CU">Cuba</option>
				<option value="CY">Cyprus</option>
				<option value="CZ">Czech Republic</option>
				<option value="DK">Denmark</option>
				<option value="DJ">Djibouti</option>
				<option value="DM">Dominica</option>
				<option value="DO">Dominican Republic</option>
				<option value="EC">Ecuador</option>
				<option value="EG">Egypt</option>
				<option value="SV">El Salvador</option>
				<option value="GQ">Equatorial Guinea</option>
				<option value="ER">Eritrea</option>
				<option value="EE">Estonia</option>
				<option value="ET">Ethiopia</option>
				<option value="FK">Falkland Islands (Malvinas)</option>
				<option value="FO">Faroe Islands</option>
				<option value="FJ">Fiji</option>
				<option value="FI">Finland</option>
				<option value="FR">France</option>
				<option value="GF">French Guiana</option>
				<option value="PF">French Polynesia</option>
				<option value="TF">French Southern Territories</option>
				<option value="GA">Gabon</option>
				<option value="GM">Gambia</option>
				<option value="GE">Georgia</option>
				<option value="DE">Germany</option>
				<option value="GH">Ghana</option>
				<option value="GI">Gibraltar</option>
				<option value="GR">Greece</option>
				<option value="GL">Greenland</option>
				<option value="GD">Grenada</option>
				<option value="GP">Guadeloupe</option>
				<option value="GU">Guam</option>
				<option value="GT">Guatemala</option>
				<option value="GN">Guinea</option>
				<option value="GW">Guinea-Bissau</option>
				<option value="GY">Guyana</option>
				<option value="HT">Haiti</option>
				<option value="HM">Heard and Mc Donald Islands</option>
				<option value="VA">Holy See (Vatican City State)</option>
				<option value="HN">Honduras</option>
				<option value="HK">Hong Kong</option>
				<option value="HU">Hungary</option>
				<option value="IS">Iceland</option>
				<option value="IN">India</option>
				<option value="ID">Indonesia</option>
				<option value="IR">Iran (Islamic Republic of)</option>
				<option value="IQ">Iraq</option>
				<option value="IE">Ireland</option>
				<option value="IL">Israel</option>
				<option value="IT">Italy</option>
				<option value="JM">Jamaica</option>
				<option value="JP">Japan</option>
				<option value="JO">Jordan</option>
				<option value="KZ">Kazakhstan</option>
				<option value="KE">Kenya</option>
				<option value="KI">Kiribati</option>
				<option value="KP">Korea, Democratic People's Republic of</option>
				<option value="KR">Korea, Republic of</option>
				<option value="KW">Kuwait</option>
				<option value="KG">Kyrgyzstan</option>
				<option value="LA">Lao People's Democratic Republic</option>
				<option value="LV">Latvia</option>
				<option value="LB">Lebanon</option>
				<option value="LS">Lesotho</option>
				<option value="LR">Liberia</option>
				<option value="LY">Libyan Arab Jamahiriya</option>
				<option value="LI">Liechtenstein</option>
				<option value="LT">Lithuania</option>
				<option value="LU">Luxembourg</option>
				<option value="MO">Macau</option>
				<option value="MK">Macedonia, The Former Yugoslav Republic of</option>
				<option value="MG">Madagascar</option>
				<option value="MW">Malawi</option>
				<option value="MY">Malaysia</option>
				<option value="MV">Maldives</option>
				<option value="ML">Mali</option>
				<option value="MT">Malta</option>
				<option value="MH">Marshall Islands</option>
				<option value="MQ">Martinique</option>
				<option value="MR">Mauritania</option>
				<option value="MU">Mauritius</option>
				<option value="YT">Mayotte</option>
				<option value="MX">Mexico</option>
				<option value="FM">Micronesia, Federated States of</option>
				<option value="MD">Moldova, Republic of</option>
				<option value="MC">Monaco</option>
				<option value="MN">Mongolia</option>
				<option value="MS">Montserrat</option>
				<option value="MA">Morocco</option>
				<option value="MZ">Mozambique</option>
				<option value="MM">Myanmar</option>
				<option value="NA">Namibia</option>
				<option value="NR">Nauru</option>
				<option value="NP">Nepal</option>
				<option value="NL">Netherlands</option>
				<option value="AN">Netherlands Antilles</option>
				<option value="NC">New Caledonia</option>
				<option value="NZ">New Zealand</option>
				<option value="NI">Nicaragua</option>
				<option value="NE">Niger</option>
				<option value="NG">Nigeria</option>
				<option value="NU">Niue</option>
				<option value="NF">Norfolk Island</option>
				<option value="MP">Northern Mariana Islands</option>
				<option value="NO">Norway</option>
				<option value="OM">Oman</option>
				<option value="PK">Pakistan</option>
				<option value="PW">Palau</option>
				<option value="PA">Panama</option>
				<option value="PG">Papua New Guinea</option>
				<option value="PY">Paraguay</option>
				<option value="PE">Peru</option>
				<option value="PH">Philippines</option>
				<option value="PN">Pitcairn</option>
				<option value="PL">Poland</option>
				<option value="PT">Portugal</option>
				<option value="PR">Puerto Rico</option>
				<option value="QA">Qatar</option>
				<option value="RE">Reunion</option>
				<option value="RO">Romania</option>
				<option value="RU">Russian Federation</option>
				<option value="RW">Rwanda</option>
				<option value="KN">Saint Kitts and Nevis</option>
				<option value="LC">Saint LUCIA</option>
				<option value="VC">Saint Vincent and the Grenadines</option>
				<option value="WS">Samoa</option>
				<option value="SM">San Marino</option>
				<option value="ST">Sao Tome and Principe</option>
				<option value="SA">Saudi Arabia</option>
				<option value="SN">Senegal</option>
				<option value="SC">Seychelles</option>
				<option value="SL">Sierra Leone</option>
				<option value="SG">Singapore</option>
				<option value="SK">Slovakia (Slovak Republic)</option>
				<option value="SI">Slovenia</option>
				<option value="SB">Solomon Islands</option>
				<option value="SO">Somalia</option>
				<option value="ZA">South Africa</option>
				<option value="GS">South Georgia and the South Sandwich Islands</option>
				<option value="ES">Spain</option>
				<option value="LK">Sri Lanka</option>
				<option value="SH">St. Helena</option>
				<option value="PM">St. Pierre and Miquelon</option>
				<option value="SD">Sudan</option>
				<option value="SR">Suriname</option>
				<option value="SJ">Svalbard and Jan Mayen Islands</option>
				<option value="SZ">Swaziland</option>
				<option value="SE">Sweden</option>
				<option value="CH">Switzerland</option>
				<option value="SY">Syrian Arab Republic</option>
				<option value="TW">Taiwan, Province of China</option>
				<option value="TJ">Tajikistan</option>
				<option value="TZ">Tanzania, United Republic of</option>
				<option value="TH">Thailand</option>
				<option value="TG">Togo</option>
				<option value="TK">Tokelau</option>
				<option value="TO">Tonga</option>
				<option value="TT">Trinidad and Tobago</option>
				<option value="TN">Tunisia</option>
				<option value="TR">Turkey</option>
				<option value="TM">Turkmenistan</option>
				<option value="TC">Turks and Caicos Islands</option>
				<option value="TV">Tuvalu</option>
				<option value="UG">Uganda</option>
				<option value="UA">Ukraine</option>
				<option value="AE">United Arab Emirates</option>
				<option value="GB">United Kingdom</option>
				<option value="UM">United States Minor Outlying Islands</option>
				<option value="UY">Uruguay</option>
				<option value="UZ">Uzbekistan</option>
				<option value="VU">Vanuatu</option>
				<option value="VE">Venezuela</option>
				<option value="VN">Viet Nam</option>
				<option value="VG">Virgin Islands (British)</option>
				<option value="VI">Virgin Islands (U.S.)</option>
				<option value="WF">Wallis and Futuna Islands</option>
				<option value="EH">Western Sahara</option>
				<option value="YE">Yemen</option>
				<option value="ZM">Zambia</option>
				<option value="ZW">Zimbabwe</option>
			</select>
		</div>
        <p>
				 Enter your account details below:
			</p>
			<div class="form-group">
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<label class="control-label visible-ie8 visible-ie9">Email</label>
				<div class="input-icon">
					<i class="fa fa-envelope"></i>
					<input class="form-control placeholder-no-fix" type="text" placeholder="Email" name="strEmail"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label visible-ie8 visible-ie9">Password</label>
				<div class="input-icon">
					<i class="fa fa-lock"></i>
					<input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Password" name="strPassword"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
				<div class="controls">
					<div class="input-icon">
						<i class="fa fa-check"></i>
						<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Re-type Your Password" name="strConfirmPassword"/>
					</div>
				</div>
			</div>
            <div class="form-actions">
				
				<button type="submit" id="register-submit-btn" class="btn btn-danger pull-right">
				Sign Up <i class="m-icon-swapright m-icon-white"></i>
				</button>
			</div>
		</form>
		<!-- END REGISTRATION FORM -->
	</div>
  <%
end sub

sub DrawLogoutPage
%>
  If you wish, you may save the order you are working on and finish it later. 
  Orders will be saved for up to two weeks.<BR>
</div>
<FORM ACTION="default.asp" METHOD="POST">
<% HiddenInput "action", "logoutsubmit" %>
<% RadioButton "saveorder", "Y", "Y" %> <B>Save my order, I'll finish it later.</B><BR>
<% RadioButton "saveorder", "N", "Y" %> Cancel my order<BR>
<BR>
<INPUT TYPE=IMAGE SRC="<%= g_imagebase %>menu/continue.gif" WIDTH="64" HEIGHT="16" ALT="Continue" BORDER="0">
</FORM>
<BR>
<A HREF="<%= strReturnURL %>"><IMG SRC="<%= g_imagebase %>menu/go-back.gif" WIDTH="62" HEIGHT="16" ALT="Go Back" BORDER="0"></A>
<%
end sub

' =============================
sub DrawMenu
dim aryTemp
	response.redirect "store/dashboard.asp"
		dim strSQL, rsTemp, strEmail
		strSQL = "SELECT U.vchEmail"
		strSQL = strSQL & " FROM " & STR_TABLE_SHOPPER & " AS U WHERE U.intID=" & gintUserID
		set rsTemp = gobjConn.execute(strSQL)
		if not rsTemp.eof then
			strEmail = Server.URLEncode(rsTemp(0))
		end if
		rsTemp.close
		set rsTemp = nothing
%>
	<!-- BEGIN LOGIN -->
	<div class="content">
<%= font(2) %><B>SAVED ORDERS</B></FONT><BR>
<%
dim dctSavedOrders, i, k, rsData
set dctSavedOrders = GetSavedOrders()
if dctSavedOrders.count > 0 then
	%>
	To continue shopping with a saved order, click the Select button for that order.<br />
	&nbsp;
	<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%">
	<TR id="cart">
		<TD>&nbsp;</TD>
		<TD>Order #</TD>
		<TD>Order Created</TD>
		<TD>Order Last Updated</TD>
	</TR>
	<%
		dim strColor
		for each k in dctSavedOrders
			aryTemp = split(dctSavedOrders(k),"|")
			if k = gintOrderID then
				strColor = cWhite
				%>
				<TR BGCOLOR="<%= cGoCart_CSSelectBG %>"><TD><%= fontx(1,1,cWhite) %><B>Currently Selected</B></TD>
				<%
			else
				strColor = cBlack
				%>
				<TR><TD><%= font(1) %><A HREF="default.asp?action=selectorder&id=<%= k %>&return=<%= Server.URLEncode("cartlist.asp") %>"><IMG SRC="<%= g_imagebase %>menu/select.gif" WIDTH="50" HEIGHT="16" ALT="Select" BORDER="0"></A></TD>
				<%
			end if
			%>
				<TD><%= fontx(1,1,strColor) %>#<%= aryTemp(0) %></font></TD>
				<TD><%= fontx(1,1,strColor) %><%= aryTemp(1) %></font></TD>
				<TD><%= fontx(1,1,strColor) %><%= aryTemp(2) %></font></TD>
			</TR>
			<TR>
				<TD COLSPAN=4><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cGoCart_CSHdrBG %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
			</TR>
			<%
		next
	%>
	</TABLE>
	<%
else
	%>
	You do not have any past orders saved.<BR>
	<%
end if
%>
<BR>
<%= font(2) %><B>ORDER HISTORY</B></FONT><BR>
<%
set dctSavedOrders = GetOrderHistory()
if dctSavedOrders.count > 0 then
	%>
	<%= font(1) %>To view a completed order, click the View button</FONT><BR>
	&nbsp;
	<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%">
	<TR BGCOLOR="<%= cGoCart_CSHdrBG %>">
		<TD><%= fontx(1,1,cWhite) %>&nbsp;</font></TD>
		<TD><%= fontx(1,1,cWhite) %>Order #</font></TD>
		<TD><%= fontx(1,1,cWhite) %>Order Created</font></TD>
		<TD><%= fontx(1,1,cWhite) %>Order Last Updated</font></TD>
		<TD><%= fontx(1,1,cWhite) %>Status</font></TD>
	</TR>
	<%
	for each k in dctSavedOrders
		aryTemp = split(dctSavedOrders(k),"|")
		aryTemp(2) = replace(aryTemp(2), ":00 ", " ")
		aryTemp(3) = replace(aryTemp(3), ":00 ", " ")
		
		%>
		<TR>
			<TD><A HREF="invoice.asp?order=<%= k %>"><IMG SRC="<%= g_imagebase %>menu/vieworder.gif" WIDTH="78" HEIGHT="16" ALT="View Order" BORDER="0"></A></TD>
			<TD><%= font(1) %>#<%= aryTemp(0) %></font></TD>
			<TD><%= font(1) %><%= aryTemp(2) %></font></TD>
			<TD><%= font(1) %><%= aryTemp(3) %></font></TD>
			<TD><%= font(1) %><%= GetArrayValue(aryTemp(1), dctOrderStatusInvoiceValues) %></font></TD>
		</TR>
		<TR>
			<TD COLSPAN=5><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cBeige %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
		</TR>
		<%
		'response.write "<A HREF=""invoice.asp?order=" & k & """>" & dctSavedOrders(k) & "</A><BR>"
	next
	%>
	</TABLE>
	<%
else
	%>
	You do not have any submitted orders.<BR>
	<%
end if
%>
</div>
<%
end sub

sub DrawAccountPage
	dim strPassword, strHint, strFirstName, strLastName, strCompany, strEmail, strAddress, strCity, strState, strZip
	if FormErrors.count = 0 then
		' load saved information
		GetUserInfo strHint, strFirstName, strLastName, strCompany, strEmail, strAddress, strCity, strState, strZip
		strPassword = ""
	else
		' form submitted with errors
		' use information from last form submission
		strHint = Request("strHint")
		strPassword = Request("strPassword")
		strFirstName = Request("strFirstName")
		strLastName = Request("strLastName")
		strCompany = Request("strCompany")
		strEmail = Request("strEmail")
		stlAddress = Request("strAddress")
		strCity = Request("strCity")
		strState = Request("strState")
		strZip = Request("strZip")
	end if
%>
<%= fontx(2,1,cBlack) %>You are currently logged in as <B><%= gintUserName %></B>. You may leave the password field blank if you do not wish to change it.
<br /><br />
<FORM ACTION="<%= virtualbase %>store/default.asp" METHOD="POST">
<%
	HiddenInput "action", "accountinfosubmit"
	HiddenInput "return", strReturnURL

	stlBeginStdTable "100%"
	response.write "<TR><TD>"
	
	stlBeginFormSection "100%", 2
	stlTextInput "strEmail", 32, 50, strEmail, "Email Address", "IsEmail", "Please provide your email address."
	stlTextInput "strPassword", 20, 20, strPassword, "Password", "optislength4%20", "Please provide a Password that is at least 4 characters long."
	stlTextInput "strHint", 32, 32, strHint, "Hint", "IsEmpty", "Please provide a hint."
	stlTextInput "strFirstName", 32, 32, strFirstName, "First Name", "IsEmpty", "Please provde your first name."
	stlTextInput "strLastName", 32, 32, strLastName, "Last Name", "IsEmpty", "Please provide your last name."
%>
	<TR>
		<TD colspan="5"><%= spacer(1,4) %></TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
		<TD><INPUT TYPE=IMAGE SRC="<%= g_imagebase %>menu/continue.gif" WIDTH="64" HEIGHT="16" ALT="Continue" BORDER="0"></TD>
	</TR>
<%
	stlEndFormSection
	response.write "</TD></TR>"
	stlEndStdTable
%>
</FORM>
<br />
<A HREF="<%= strReturnURL %>"><IMG SRC="<%= g_imagebase %>menu/go-back.gif" WIDTH="62" HEIGHT="16" ALT="Go Back" BORDER="0"></A>
<%
end sub

sub DrawConfirmPage(strMessage)
%>
<B>Thank you, <%= split(gintUserName," ")(0) %>.</B><BR>
<BR>
<%= strMessage %><BR>
<BR>
<A HREF="<%= strReturnURL %>"><IMG SRC="<%= g_imagebase %>menu/continue.gif" WIDTH="64" HEIGHT="16" ALT="Continue" BORDER="0"></A>
<%
end sub

sub DrawHintPage()
%>
When you created your account, you provided a hint to help you remember your password. Please enter your email address to find out your hint. If you still can not remember your password after viewing your hint, please contact customer service at <%= STR_MERCHANT_CS_PHONE %> or email <A HREF="mailto:<%= STR_MERCHANT_CS_EMAIL %>"><%= STR_MERCHANT_CS_EMAIL %></A>.<BR>
<BR>
<FORM ACTION="default.asp" METHOD="POST">
<%
	HiddenInput "action", "login"
	HiddenInput "hint", "y"
%>
Email Address: <% TextInput "email", 32, 50, Request %> <INPUT TYPE="submit" VALUE="Display My Hint">
</FORM>
<%
end sub

sub DrawReturnPolicy
%>


<%
end sub
%>