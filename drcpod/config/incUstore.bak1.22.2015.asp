<!--#include file="../../config/incInit.asp"-->
<!--#include file="../../config/incVeraCore.asp"-->
<!--#include file="Json_2.0.4.asp"-->
<%
'Response.ContentType = "application/json; charset=utf-8"
Server.ScriptTimeout = 2147483647
dim reqAction

CONST STR_USTORE_USERNAME = "ustorewebservice@domeprinting.com"
CONST STR_USTORE_PASSWORD = "dothething"

CONST STR_UPRODUCE_USERNAME = "ustore"
CONST STR_UPRODUCE_PASSWORD = "password"

CONST INT_STORE_ID = 5 ' Graton 12 ' Google 11 'Athleta 5

'CONST UPRODUCE_IP = "10.0.10.91"
CONST UPRODUCE_IP = "10.0.10.185"
CONST USTORE_IP = "10.0.10.185"
'CONST USTORE_IP = "10.0.10.93"


reqAction = request("action")

select case reqAction
	case "getProductGroupList"
		getProductGroupList
	case "getProductList"
		getProductList
	case "getProduct"
		getProduct
	case "getProductThumbnails"
		getProductThumbnails
	case "getDocumentByProduct"
		getDocumentByProduct
	case "getProductPropertyList"
		getProductPropertyList
	case "getCustomizationStepList"
		getCustomizationStepList
	case "getCustomizationDialList"
		getCustomizationDialList
	case "getRecipientList"
		getRecipientList
	case "addRecipientList"
		addRecipientList
	case "resync"
		getProductGroupList
		getProductList
		getDocumentByProduct
		getProductThumbnails
		getCustomizationStepList
		getCustomizationDialList
	case "getAllProperties"
		getAllProperties
	case "addPODToVeracore"
		addPODToVeracore
	case "getUProduceStatus"
		getUProduceStatus
	case "testLoadTicket"
		testLoadTicket
	case "testRecipientStuff"
		testRecipientStuff
end select

sub addPODToVeracore
	Dim oXmlHTTP, oXmlResp,oXMlNodes, oXmlProductAvailabilityResult, oXmlOfferResultId, oXmlOfferResultDescription, oXmlOfferResultAvailable
	
	Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
	oXmlHTTP.Open "POST", "http://www.drcinventory.com/pmomsws/order.asmx", False 

	oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
	oXmlHTTP.setRequestHeader "SOAPAction", "http://sma-promail/AddProduct"
	'On Error Resume Next

	Response.Write AddPODProductXml("ATH_POD_Test7","test name",0)
	'Response.End


	oXmlHTTP.send AddPODProductXml("ATH_POD_Test7","test name",0)
	Response.Write oXmlHTTP.responseText
	
	'Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
	'oXmlHTTP.Open "POST", "http://www.drcinventory.com/pmomsws/order.asmx", False 
	'oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
	'oXmlHTTP.setRequestHeader "SOAPAction", "http://sma-promail/SaveOffer"
	'Response.Write AddPODSaveOfferXml("ATH_POD_Test6","test name1",1)
	'oXmlHTTP.send AddPODSaveOfferXml("ATH_POD_Test6","test name1",1)
	
	'Response.Write oXmlHTTP.responseText
    Response.End
end sub

Function QueryToJSON(dbcomm, params)
        Dim rs, jsa
        Set rs = dbcomm.Execute(,params,1)
        Set jsa = jsArray()
        Do While Not (rs.EOF Or rs.BOF)
                Set jsa(Null) = jsObject()
                For Each col In rs.Fields
                        jsa(Null)(col.Name) = col.Value
                Next
        rs.MoveNext
        Loop
        Set QueryToJSON = jsa
        rs.Close
End Function

sub SQLExampleASPJson
	strConn = "Provider=SQLNCLI10;Server=localhost;Database=NorthWind;Trusted_Connection=Yes;"
	Set conn = Server.CreateObject("ADODB.Connection")
	conn.Open strConn
	query = "SELECT * FROM Customers WHERE CustomerID = ?"
	CustomerID = Request.QueryString("CustomerID")
	arParams = array(CustomerID)
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = query
	Set cmd.ActiveConnection = conn
	QueryToJSON(cmd, arParams).Flush
end sub

sub getProductPropertyList
	Dim oXmlHTTP, oXmlResp,oXMlNodes, indivNode, indivNodeList, node, nodeList, fstElmnt, nameList, nameElement, dataNode, oXmlProductAvailabilityResult, oXmlOfferResultId, oXmlOfferResultDescription, oXmlOfferResultAvailable, dctReturnValue, i, rsTemp,strSQL
	Dim ProductGroupID, Name, Description, ParentGroupID, Status, DisplayOrder

	Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
    oXmlHTTP.Open "POST", "http://" & USTORE_IP & "/uStorewsapi/ProductPropertyWS.asmx", False 

    oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
    oXmlHTTP.setRequestHeader "SOAPAction", "uStoreWSAPI/GetProductPropertyList"
    'On Error Resume Next

    oXmlHTTP.send getProductPropertyListXML("58")    
    Response.Write getProductPropertyListXML("58")  
	Response.Write oXmlHTTP.responseText
    Response.End

	
end sub

sub getProductGroupList
	Dim oXmlHTTP, oXmlResp,oXMlNodes, indivNode, indivNodeList, node, nodeList, fstElmnt, nameList, nameElement, dataNode, oXmlProductAvailabilityResult, oXmlOfferResultId, oXmlOfferResultDescription, oXmlOfferResultAvailable, dctReturnValue, i, rsTemp,strSQL
	Dim ProductGroupID, Name, Description, ParentGroupID, Status, DisplayOrder

    Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
    oXmlHTTP.Open "POST", "http://" & USTORE_IP & "/uStorewsapi/ProductGroupWS.asmx", False 

    oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
    oXmlHTTP.setRequestHeader "SOAPAction", "uStoreWSAPI/GetProductGroupList"
    'On Error Resume Next

    oXmlHTTP.send getProductGroupListXML()    
    'Response.Write getProductGroupListXML()  
	'Response.Write oXmlHTTP.responseText
    'Response.End
    
    Set oXmlResp = oXmlHTTP.responseXML
    
    Set oXmlNodes = oXmlResp.selectNodes("//ProductGroup")
	
	openConn
	for i = 0 to oXmlNodes.length - 1
	
		set indivNode = oXmlNodes.item(i)

		Set dataNode = indivNode.getElementsByTagName("ProductGroupID")
		ProductGroupID = dataNode.item(0).text
		
		Set dataNode = indivNode.getElementsByTagName("Name")
		Name = dataNode.item(0).text
		
		Set dataNode = indivNode.getElementsByTagName("Description")
		Description = dataNode.item(0).text
		
		Set dataNode = indivNode.getElementsByTagName("ParentGroupID")
		ParentGroupID = dataNode.item(0).text
		
		Set dataNode = indivNode.getElementsByTagName("StatusID")
		Status = dataNode.item(0).text
		
		Set dataNode = indivNode.getElementsByTagName("DisplayOrder")
		DisplayOrder = dataNode.item(0).text
		
		if ParentGroupID = "-1" then
			ParentGroupID = "0"
		end if
		
		if Status = "1" then
			Status = "A"
		else
			Status = "I"
		end if
		
		strSQL = "select intID from " & TABLE_PRODUCT_GROUP & " where intID = " & ProductGroupID
		set rsTemp = gobjConn.execute(strSQL)
		
		if rsTemp.eof then
			strSQL = "Insert into " & TABLE_PRODUCT_GROUP & " (intID, chrStatus, intDisplayOrder, intParentID, vchName, txtDescription) values (" & ProductGroupID & ", '" & Status & "', " & DisplayOrder & ", " & ParentGroupID & ", '" & Name & "', '" & Description & "')"
		else
			strSQL = "Update " & TABLE_PRODUCT_GROUP & " set vchName = '" & Name & "' , txtDescription = '" & Description & "', intParentID = " & ParentGroupID & ", chrStatus = '" & Status & "', intDisplayOrder = " & DisplayOrder & " where intID = " & ProductGroupID
		end if
		'response.write strSQL
		'rsTemp.Close
		gobjConn.execute(strSQL)
		'rsTemp.Close
		
	next
	CloseConn
 
	
end sub

sub getProductList
	Dim oXmlHTTP, oXmlResp,oXMlNodes, indivNode, indivNodeList, node, fstElmnt, nameList, nameElement, dataNode, oXmlProductAvailabilityResult, oXmlOfferResultId, oXmlOfferResultDescription, oXmlOfferResultAvailable, dctReturnValue, strSQL, rsTemp, rsTemp2, i, strSKUName
	dim uStoreProductID, ProductGroupID, Name, SDesc, Desc, HasPricing, EnableRecipientList, CreatedDate, ModifiedDate, DisplayOrder, StatusID, DefaultProofType, EnableProofGeneration, ShowShortDesc, Keywords

    'On Error Resume Next
	OpenConn
	strSQL = "select intID from " & TABLE_PRODUCT_GROUP
	set rsTemp = gobjConn.execute(strSQL)

	while not rsTemp.EOF
		Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
		oXmlHTTP.Open "POST", "http://" & USTORE_IP & "/uStorewsapi/ProductWS.asmx", False 

		oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
		oXmlHTTP.setRequestHeader "SOAPAction", "uStoreWSAPI/GetProductList"
		
		oXmlHTTP.send getProductListXML(rsTemp("intID"))
		Response.Write getProductListXML(rsTemp("intID"))
		Response.Write oXmlHTTP.responseText
		'Response.End
		
		
		Set oXmlResp = oXmlHTTP.responseXML
    
		Set oXmlNodes = oXmlResp.selectNodes("//Product")
		
		ProductGroupID = rsTemp("intID")
	
		for i = 0 to oXmlNodes.length-1
			set indivNode = oXmlNodes.item(i)
			
			Set dataNode = indivNode.getElementsByTagName("ProductID")
			uStoreProductID = dataNode.item(0).text
		
			Set dataNode = indivNode.getElementsByTagName("Name")
			Name = dataNode.item(0).text
		
			Set dataNode = indivNode.getElementsByTagName("ShortDescrition")
			SDesc = dataNode.item(0).text
		
			Set dataNode = indivNode.getElementsByTagName("Description")
			Desc = dataNode.item(0).text
		
			Set dataNode = indivNode.getElementsByTagName("HasPricing")
			HasPricing = dataNode.item(0).text
		
			Set dataNode = indivNode.getElementsByTagName("EnableRecipientListModels")
			EnableRecipientList = dataNode.item(0).text
			
			Set dataNode = indivNode.getElementsByTagName("ModifiedDate")
			ModifiedDate = dataNode.item(0).text
			
			Set dataNode = indivNode.getElementsByTagName("DisplayOrder")
			DisplayOrder = dataNode.item(0).text
			
			Set dataNode = indivNode.getElementsByTagName("StatusID")
			StatusID = dataNode.item(0).text
			
			Set dataNode = indivNode.getElementsByTagName("DefaultProofType")
			DefaultProofType = dataNode.item(0).text
			
			Set dataNode = indivNode.getElementsByTagName("EnableProofGeneration")
			EnableProofGeneration = dataNode.item(0).text
			
			Set dataNode = indivNode.getElementsByTagName("ShowShortDesc")
			ShowShortDesc = dataNode.item(0).text
			
			Set dataNode = indivNode.getElementsByTagName("KeyWords")
			Keywords = dataNode.item(0).text
		
			if StatusID = "1" then
				StatusID = "A"
			else
				StatusID = "I"
			end if
		
			strSQL = "select intID, dtmModifiedDate from " & TABLE_PRODUCT_TABLE & " where intUStoreID = " & uStoreProductID
			set rsTemp2 = gobjConn.execute(strSQL)
		
			if rsTemp2.eof then
				strSQL = "Insert into " & TABLE_PRODUCT_TABLE & " (intUStoreID, intProductGroupID, vchName, txtShortDescription, txtDescription, boolHasPricing, boolRecipientList, dtmCreatedDate, dtmModifiedDate, intDisplayOrder, chrStatus, intProofType, boolGenerateProof, boolShowShortDesc, txtKeywords) values (" & uStoreProductID & ", " & ProductGroupID & ", '" & Name & "', '" & SDesc & "', '" & Desc & "', '" & HasPricing & "', '" & EnableRecipientList & "', '" & CreatedDate & "', '" & ModifiedDate & "', " & DisplayOrder & ", '" & StatusID & "', " & DefaultProofType & ", '" & EnableProofGeneration & "', '" & ShowShortDesc & "','" & Keywords & "')"
				gobjConn.execute(strSQL)
			elseif rsTemp2("dtmModifiedDate") <> ModifiedDate then
				strSQL = "Update " & TABLE_PRODUCT_TABLE & " set intProductGroupID = " & ProductGroupID & " , vchName = '" & Name & "', txtShortDescription = '" & SDesc & "', txtDescription = '" & Desc & "', boolHasPricing = '" & HasPricing & "', boolRecipientList = '" & EnableRecipientList & "', dtmCreatedDate = '" & CreatedDate & "', dtmModifiedDate = '" & ModifiedDate & "', intDisplayOrder = " & DisplayOrder & ", chrStatus = '" & StatusID & "', intProofType = " & DefaultProofType & ", boolGenerateProof = '" & EnableProofGeneration & "', boolShowShortDesc = '" & ShowShortDesc & "', txtKeywords = '" & Keywords & "' where intUStoreID = " & uStoreProductID
				gobjConn.execute(strSQL)
			end if
			'response.write strSQL
			
			strSKUName = UCase(left(TABLE_PRODUCT_TICKETS, 3)) & "_POD_" & uStoreProductID
			
			strSQL = "select intID from " & STR_TABLE_INVENTORY_LONG_TERM & " where vchPartNumber = '" & strSKUName & "'"
			set rsTemp2 = gobjConn.execute(strSQL)
			
			if rsTemp2.eof then
				strSQL = "insert into " & STR_TABLE_INVENTORY_LONG_TERM & " (chrType, chrStatus, dtmCreated, dtmUpdated, vchCreatedByUser, vchUpdatedByUser, vchCreatedByIP, vchUpdatedByIP, intStock, vchItemName, vchPartNumber, vchGroupingIDs) values ('I','A',getdate(),getdate(),'uStore','uStore','" & USTORE_IP & "','" & USTORE_IP & "',0,'" & Name & "','" & strSKUName & "', '1')"
				gobjConn.execute(strSQL)
				
				Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
				oXmlHTTP.Open "POST", "http://www.drcinventory.com/pmomsws/order.asmx", False 

				oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
				oXmlHTTP.setRequestHeader "SOAPAction", "http://sma-promail/AddProduct"
				'On Error Resume Next

				'Response.Write AddPODProductXml(strSKUName,Name,0)
				'Response.End


				oXmlHTTP.send AddPODProductXml(strSKUName,Name,0)
				
				Response.Write oXmlHTTP.responseText
			end if
			
		next
		
		
		rsTemp.MoveNext
	wend
	CloseConn
end sub


sub getDocumentByProduct
	Dim oXmlHTTP, oXmlResp,oXMlNodes, oXmlProductAvailabilityResult, oXmlOfferResultId, oXmlOfferResultDescription, oXmlOfferResultAvailable, dctReturnValue, strSQL,rsTemp,indivNode,dataNode,i
	dim uStoreProductID, DocTypeID, TemplateType, ProofCountMax, ProofCountDefault, UproduceDocID, UproduceProofJobID, UproduceProcessJobID, StaticDocLocation, StaticDocProofLocation, RecipientListSchemaXml

    
    'On Error Resume Next
    
    OpenConn
	strSQL = "select intUStoreID from " & TABLE_PRODUCT_TABLE
	set rsTemp = gobjConn.execute(strSQL)
	
	while not rsTemp.EOF
		Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
		oXmlHTTP.Open "POST", "http://" & USTORE_IP & "/uStorewsapi/DocumentWS.asmx", False 

		oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
		oXmlHTTP.setRequestHeader "SOAPAction", "uStoreWSAPI/GetDocumentByProduct"
		oXmlHTTP.send getDocumentByProductXML(rsTemp("intUStoreID"))    
		'Response.Write getDocumentByProductXML(rsTemp("intUStoreID"))  
		'Response.Write oXmlHTTP.responseText
		
		Set oXmlResp = oXmlHTTP.responseXML
    
		Set oXmlNodes = oXmlResp.selectNodes("//GetDocumentByProductResult")
		
		uStoreProductID = rsTemp("intUStoreID")
	
		for i = 0 to oXmlNodes.length-1
			set indivNode = oXmlNodes.item(i)
			
			Set dataNode = indivNode.getElementsByTagName("DocTypeID")
			DocTypeID = dataNode.item(0).text
		
			Set dataNode = indivNode.getElementsByTagName("TemplateType")
			TemplateType = dataNode.item(0).text
		
			Set dataNode = indivNode.getElementsByTagName("ProofCountMax")
			ProofCountMax = dataNode.item(0).text
		
			Set dataNode = indivNode.getElementsByTagName("ProofCountDefault")
			ProofCountDefault = dataNode.item(0).text
		
			Set dataNode = indivNode.getElementsByTagName("UproduceDocID")
			UproduceDocID = dataNode.item(0).text
		
			Set dataNode = indivNode.getElementsByTagName("UproduceProofJobID")
			UproduceProofJobID = dataNode.item(0).text
			
			Set dataNode = indivNode.getElementsByTagName("UproduceProcessJobID")
			UproduceProcessJobID = dataNode.item(0).text
			
			Set dataNode = indivNode.getElementsByTagName("StaticDocLocation")
			StaticDocLocation = dataNode.item(0).text
			
			Set dataNode = indivNode.getElementsByTagName("StaticDocProofLocation")
			StaticDocProofLocation = dataNode.item(0).text
			
			Set dataNode = indivNode.getElementsByTagName("RecipientListSchemaXml")
			RecipientListSchemaXml = dataNode.item(0).text
		
			strSQL = "Update " & TABLE_PRODUCT_TABLE & " set intDocType = " & DocTypeID & " , intTemplateType = " & TemplateType & ", intProofCountMax = " & ProofCountMax & ", intProofCountDefault = " & ProofCountDefault & ", intUProduceID = " & UproduceDocID & ", intUProduceProofID = " & UproduceProofJobID & ", intUProduceProccessJobID = " & UproduceProcessJobID & ", vchStaticDocLocation = '" & StaticDocLocation & "', vchStaticProofLocation = '" & StaticDocProofLocation & "', txtRecipientListSchemaXml = '" & RecipientListSchemaXml & "' where intUStoreID = " & uStoreProductID
			'response.write strSQL
			gobjConn.execute(strSQL)
		next
		
		rsTemp.MoveNext
	wend
	CloseConn
	
end sub

sub getProductThumbnails
	Dim oXmlHTTP, oXmlResp,oXMlNodes, oXmlProductAvailabilityResult, oXmlOfferResultId, oXmlOfferResultDescription, oXmlOfferResultAvailable, dctReturnValue, strSQL,rsTemp,indivNode,indivNodeChild,dataNode,i, h
	dim uStoreProductID, base64Image
    base64Image = ""
    'On Error Resume Next
    
    OpenConn
	strSQL = "select intUStoreID from " & TABLE_PRODUCT_TABLE
	set rsTemp = gobjConn.execute(strSQL)
	
	while not rsTemp.EOF
		Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
		oXmlHTTP.Open "POST", "http://" & USTORE_IP & "/uStorewsapi/ProductWS.asmx", False 

		oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
		oXmlHTTP.setRequestHeader "SOAPAction", "uStoreWSAPI/GetProductThumbnails"
		oXmlHTTP.send getProductThumbnailsXML(rsTemp("intUStoreID"))    
		'Response.Write getProductThumbnailsXML(rsTemp("intUStoreID"))  
		'Response.Write oXmlHTTP.responseText
		'response.end
		
		Set oXmlResp = oXmlHTTP.responseXML
    
		Set oXmlNodes = oXmlResp.selectNodes("//GetProductThumbnailsResult")
		
		uStoreProductID = rsTemp("intUStoreID")
		base64Image = ""
		for i = 0 to oXmlNodes.length-1
			set indivNode = oXmlNodes.item(i)
			base64Image = base64Image & indivNode.text & "|"
			
		next
		
		'response.write base64Image
		'response.end
		
		strSQL = "Update " & TABLE_PRODUCT_TABLE & " set txtImage64 = '" & base64Image & "'  where intUStoreID = " & uStoreProductID
		'response.write strSQL
		gobjConn.execute(strSQL)
		
		rsTemp.MoveNext
	wend
	CloseConn

end sub

sub getCustomizationStepList
	Dim oXmlHTTP, oXmlResp,oXMlNodes, oXmlProductAvailabilityResult, oXmlOfferResultId, oXmlOfferResultDescription, oXmlOfferResultAvailable, dctReturnValue, strSQL,rsTemp,rsTemp2,indivNode,dataNode,i
	dim uStoreProductID, CustomizationStepID, CustomizationWizardID, Name, Description, DisplayOrder

    OpenConn
	strSQL = "select intUStoreID from " & TABLE_PRODUCT_TABLE
	set rsTemp = gobjConn.execute(strSQL)
	
	while not rsTemp.EOF
		Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
		oXmlHTTP.Open "POST", "http://" & USTORE_IP & "/uStorewsapi/CustomizationStepWS.asmx", False 

		oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
		oXmlHTTP.setRequestHeader "SOAPAction", "uStoreWSAPI/GetCustomizationStepList"
		'On Error Resume Next
		
		oXmlHTTP.send getCustomizationStepListXML(rsTemp("intUStoreID"))
		'Response.Write getCustomizationStepListXML(rsTemp("intUStoreID"))
		
		Set oXmlResp = oXmlHTTP.responseXML
    
		Set oXmlNodes = oXmlResp.selectNodes("//CustomizationStep")
		
		uStoreProductID = rsTemp("intUStoreID")
	
		for i = 0 to oXmlNodes.length-1
			set indivNode = oXmlNodes.item(i)
			
			Set dataNode = indivNode.getElementsByTagName("CustomizationStepID")
			CustomizationStepID = dataNode.item(0).text
		
			Set dataNode = indivNode.getElementsByTagName("CustomizationWizardID")
			CustomizationWizardID = dataNode.item(0).text
		
			Set dataNode = indivNode.getElementsByTagName("Name")
			Name = dataNode.item(0).text
		
			Set dataNode = indivNode.getElementsByTagName("Description")
			Description = dataNode.item(0).text
		
			Set dataNode = indivNode.getElementsByTagName("DisplayOrder")
			DisplayOrder = dataNode.item(0).text
			
			strSQL = "select intStepID from " & TABLE_PRODUCT_CUSTOM_STEPS & " where intStepID = " & CustomizationStepID
			set rsTemp2 = gobjConn.execute(strSQL)
			
			if rsTemp2.EOF then
				strSQL = "insert into " & TABLE_PRODUCT_CUSTOM_STEPS & " (intUStoreID, intStepID, intWizardID, intDisplayOrder, vchName, txtDescription) values (" & uStoreProductID & ", " & CustomizationStepID & " , " & CustomizationWizardID & " ," & DisplayOrder & " ,'" & Name & "' ,'" & Description & "')"
			else
				strSQL = "Update " & TABLE_PRODUCT_CUSTOM_STEPS & " set intWizardID = " & CustomizationWizardID & " , intDisplayOrder = " & DisplayOrder & ", vchName = '" & Name & "', txtDescription = '" & Description & "' where intStepID = " & CustomizationStepID
			end if
			'response.write strSQL
			gobjConn.execute(strSQL)
		
		next
		
		
		rsTemp.MoveNext
	wend
	CloseConn
 
	
end sub

sub getCustomizationDialList
	
	Dim oXmlHTTP, oXmlResp,oXMlNodes, oXmlSubNodes, oXmlProductAvailabilityResult, oXmlOfferResultId, oXmlOfferResultDescription, oXmlOfferResultAvailable, dctReturnValue, strSQL,rsTemp,rsTemp2,indivNode, indivNode2,dataNode,i, h, g, paramXML, paramXML2, dctParamsToSave, dctParamsToUpdate, subXML
	dim intStepID, intDialID, Name, Description, DocID, DialTypeID, UproduceDialName, IsVariable, DefaultValue, Options, OptionID, OptionText, OptionValue, UIControlTypeID, UIControlParamsXml, UIControlParam, UIControlParamValue, AssetSelectionSourceID, AssetUploadSourceID, DisplayOrder, DialValueAutoFillTypeID, StatusID, DependentOnFieldOptionID, key
    'On Error Resume Next
    
    OpenConn
	strSQL = "select intStepID from " & TABLE_PRODUCT_CUSTOM_STEPS
	set rsTemp = gobjConn.execute(strSQL)
	
	while not rsTemp.EOF
		Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
		oXmlHTTP.Open "POST", "http://" & USTORE_IP & "/uStorewsapi/CustomizationDialWS.asmx", False 

		oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
		oXmlHTTP.setRequestHeader "SOAPAction", "uStoreWSAPI/GetCustomizationDialList"
		'On Error Resume Next
		
		oXmlHTTP.send getCustomizationDialListXML(rsTemp("intStepID"))
		'Response.Write getCustomizationDialListXML(rsTemp("intStepID"))
		Response.Write oXmlHTTP.responseText
		
		Set oXmlResp = oXmlHTTP.responseXML
    
		Set oXmlNodes = oXmlResp.selectNodes("//CustomizationDial")
		
		intStepID = rsTemp("intStepID")
	
		for i = 0 to oXmlNodes.length-1
			set indivNode = oXmlNodes.item(i)
			
			Set dataNode = indivNode.getElementsByTagName("ID")
			intDialID = dataNode.item(0).text
		
			Set dataNode = indivNode.getElementsByTagName("Name")
			Name = dataNode.item(0).text
		
			Set dataNode = indivNode.getElementsByTagName("Description")
			Description = dataNode.item(0).text
		
			Set dataNode = indivNode.getElementsByTagName("DocID")
			DocID = dataNode.item(0).text
		
			Set dataNode = indivNode.getElementsByTagName("DialTypeID")
			DialTypeID = dataNode.item(0).text
			
			Set dataNode = indivNode.getElementsByTagName("UproduceDialName")
			UproduceDialName = dataNode.item(0).text
			
			Set dataNode = indivNode.getElementsByTagName("IsVariable")
			IsVariable = dataNode.item(0).text
			
			Set dataNode = indivNode.getElementsByTagName("DefaultValue")
			DefaultValue = dataNode.item(0).text
			
			Set dataNode = indivNode.getElementsByTagName("Options")
			Options = dataNode.item(0).text
			
			Set dataNode = indivNode.getElementsByTagName("UIControlTypeID")
			UIControlTypeID = dataNode.item(0).text
			
			Set dataNode = indivNode.getElementsByTagName("UIControlParamsXml")
			UIControlParamsXml = dataNode.item(0).text
			
			Set dataNode = indivNode.getElementsByTagName("AssetSelectionSourceID")
			AssetSelectionSourceID = dataNode.item(0).text
			
			Set dataNode = indivNode.getElementsByTagName("AssetUploadSourceID")
			AssetUploadSourceID = dataNode.item(0).text
			
			Set dataNode = indivNode.getElementsByTagName("DisplayOrder")
			DisplayOrder = dataNode.item(0).text
			
			Set dataNode = indivNode.getElementsByTagName("DialValueAutoFillTypeID")
			DialValueAutoFillTypeID = dataNode.item(0).text
			
			Set dataNode = indivNode.getElementsByTagName("StatusID")
			StatusID = dataNode.item(0).text
			
			Set dataNode = indivNode.getElementsByTagName("DependentOnFieldOptionID")
			DependentOnFieldOptionID = dataNode.item(0).text
			
			if StatusID = "1" then
				StatusID = "A"
			else
				StatusID = "I"
			end if
			
			strSQL = "select intDialID from " & TABLE_PRODUCT_CUSTOM_DIALS  & " where intDialID = " & intDialID
			set rsTemp2 = gobjConn.execute(strSQL)
			'select/save dial before trying to save params and drop downs
			if rsTemp2.EOF then
				strSQL = "insert into " & TABLE_PRODUCT_CUSTOM_DIALS  & " (intDialID, chrStatus ,intStepID, intDocID, intDialTypeID, intControlType, intAssetSelectionSourceID, intAssestUploadSourceID, intDisplayOrder, intDialValueAutoFillTypeID, intDependentOnFieldOptionID, vchName, vchUProduceDialName, vchDefaultValue, blnIsVariable, txtDescription) values (" & intDialID & ", '" & StatusID & "', " & intStepID & " ," & DocID & " ," & DialTypeID & " ," & UIControlTypeID & " ," & AssetSelectionSourceID & " ," & AssetUploadSourceID & " ," & DisplayOrder & " ," & DialValueAutoFillTypeID & " ," & DependentOnFieldOptionID & " ,'" & Name & "' ,'" & UproduceDialName & "' ,'" & DefaultValue & "' ,'" & IsVariable & "' ,'" & Description & "')"
			else
				strSQL = "Update " & TABLE_PRODUCT_CUSTOM_DIALS  & " set chrStatus = '" & StatusID & "', intDocID = " & DocID & ", intDialTypeID = " & DialTypeID & ", intControlType = " & UIControlTypeID & ", intAssetSelectionSourceID = " & AssetSelectionSourceID & ", intAssestUploadSourceID = " & AssetUploadSourceID & ", intDisplayOrder = " & DisplayOrder & ", intDialValueAutoFillTypeID = " & DialValueAutoFillTypeID & ", intDependentOnFieldOptionID = " & DependentOnFieldOptionID & ", vchName = '" & Name & "', vchUProduceDialName = '" & UproduceDialName & "', vchDefaultValue = '" & DefaultValue & "', blnIsVariable = '" & IsVariable & "', txtDescription = '" & Description & "'  where intDialID = " & intDialID
			end if
			'response.write strSQL
			gobjConn.execute(strSQL)
			
			'do options now
			if Options <> "" then
				Set oXmlSubNodes = indivNode.SelectNodes(".//FieldOption")
				for h = 0 to oXmlSubNodes.length-1
					set indivNode2 = oXmlSubNodes.item(h)
					
					Set dataNode = indivNode2.getElementsByTagName("ID")
					OptionID = dataNode.item(0).text
					
					Set dataNode = indivNode2.getElementsByTagName("Text")
					OptionText = dataNode.item(0).text
					
					Set dataNode = indivNode2.getElementsByTagName("Value")
					OptionValue = dataNode.item(0).text
					
					strSQL = "select intOptionID from " & TABLE_PRODUCT_CUSTOM_DROPDOWNS  & " where intOptionID = " & OptionID & " and intDialID = " & intDialID
					set rsTemp2 = gobjConn.execute(strSQL)
					'select/save dial before trying to save params and drop downs
					if rsTemp2.EOF then
						strSQL = "insert into " & TABLE_PRODUCT_CUSTOM_DROPDOWNS  & " (intDialID, intOptionID ,vchText, vchValue) values (" & intDialID & ", " & OptionID & ", '" & OptionText & "' ,'" & OptionValue & "')"
					else
						strSQL = "Update " & TABLE_PRODUCT_CUSTOM_DROPDOWNS  & " set vchText = '" & OptionText & "', vchValue = '" & OptionValue & "' where intDialID = " & intDialID & " and intOptionID = " & OptionID
					end if
					'response.write strSQL
					gobjConn.execute(strSQL)
				Next
			end if
			
			if UIControlParamsXml <> "" then
				set dctParamsToSave = CreateObject("Scripting.Dictionary")
				set dctParamsToUpdate = CreateObject("Scripting.Dictionary")
				'response.write UIControlParamsXml
				UIControlParamsXml = replace(replace(replace(replace(UIControlParamsXml, "&amp;", "&"), "&lt;", "<"), "&gt;", ">"), "&quot;", """")
				UIControlParamsXml = "<?xml version='1.0' encoding='UTF-8' standalone='yes'?>"  & UIControlParamsXml
				set subXML = Server.CreateObject("Msxml2.DOMDocument")
				subXML.LoadXml(UIControlParamsXml)
				set paramXML = subXML
				if instr(UIControlParamsXml, "TextboxDucParams") > 0 then
				' no useful Params
				elseif instr(UIControlParamsXml, "Parameters") > 0 then
				' used for checkbox, get order properties, datetime picker
					set paramXML = paramXML.selectNodes("//Parameters")
					set paramXML = paramXML.item(0).childNodes
					
					for g = 0 to paramXML.length-1
						set indivNode = paramXML.item(g)
						if indivNode.nodeName = "TimePicker" or indivNode.nodeName = "DatePickerNode" then
							set paramXML2 =  indivNode.selectNodes(".//Parameters")
							set paramXML2 = paramXML2.item(0).childNodes
							
							for h = 0 to paramXML2.length-1
								set indivNode = paramXML2.item(h)
								'response.write indivNode.nodeName & ":" & indivNode.text & "here i am"  & vbNewLine
								dctParamsToSave.add intDialID & "|" &indivNode.nodeName, indivNode.text
								
							next
						else
							dctParamsToSave.add intDialID & "|" &indivNode.nodeName, indivNode.text
							'response.write indivNode.nodeName & ":" & indivNode.text  & vbNewLine
						end if
					next
				elseif instr(UIControlParamsXml, "params") > 0 then
				' used for dropdown list, radio button, gallery list, gallery grid
					set paramXML = paramXML.selectNodes("//params")
					set paramXML = paramXML.item(0).childNodes
					
					for g = 0 to paramXML.length-1
						set indivNode = paramXML.item(g)
						
						dctParamsToSave.add intDialID & "|" &indivNode.nodeName, indivNode.text
						'response.write indivNode.nodeName & ":" & indivNode.text  & vbNewLine
					next
				
				elseif instr(UIControlParamsXml, "<xml>") > 0 then
				' used for HTML generic
					set paramXML = paramXML.selectNodes("//xml")
					set paramXML = paramXML.item(0).childNodes
					
					for g = 0 to paramXML.length-1
						set indivNode = paramXML.item(g)
						
						dctParamsToSave.add intDialID & "|" &indivNode.nodeName, indivNode.text
						'response.write indivNode.nodeName & ":" & indivNode.text  & vbNewLine
					next
				end if
				
				if dctParamsToSave.Count > 0 then
					strSQL = "select DialParam, intDialID, vchParamType, vchParamValue from " & TABLE_PRODUCT_CUSTOM_PARAMS  & " where DialParam in ( "
					for each key in dctParamsToSave.Keys
						UIControlParam = split(key, "|")
						strSQL = strSQL & "'" & intDialID & UIControlParam(1) & "',"
					next
					strSQL = left(strSQL,len(strSQL)-1)
					strSQL = strSQL & ")"
					'response.write strSQL & "initialselect"
					set rsTemp2 = gobjConn.execute(strSQL)
					
					if rsTemp2.EOF then
						strSQL = "insert into " & TABLE_PRODUCT_CUSTOM_PARAMS  & " (DialParam, intDialID ,vchParamType, vchParamValue) values "
						for each key in dctParamsToSave.Keys
							UIControlParam = split(key, "|")
							strSQL = strSQL & "('" & intDialID & UIControlParam(1) & "', " & intDialID & ", '" & UIControlParam(1) & "' ,'" & dctParamsToSave(key) & "'),"
						next
						strSQL = left(strSQL,len(strSQL)-1)
						gobjConn.execute(strSQL)
					else
						while not rsTemp2.eof
							dctParamsToUpdate.Add rsTemp2("intDialID") & "|" & rsTemp2("vchParamType"), dctParamsToSave(rsTemp2("intDialID") & "|" & rsTemp2("vchParamType"))
							dctParamsToSave.Remove( rsTemp2("intDialID") & "|" & rsTemp2("vchParamType"))
							rsTemp2.MoveNext
						wend
						if dctParamsToUpdate.Count > 0 then
							strSQL = ""
							for each key in dctParamsToUpdate.Keys
								UIControlParam = split(key, "|")
								strSQL = strSQL & "Update " & TABLE_PRODUCT_CUSTOM_PARAMS  & " set vchParamValue = '" & dctParamsToUpdate(key) & "' where DialParam = '" & intDialID & UIControlParam(1) &"';"
							next
							'response.write strSQL &"thisistheupdate"
							gobjConn.execute(strSQL)
							
							
						end if
						if dctParamsToSave.Count > 0 then
							strSQL = "insert into " & TABLE_PRODUCT_CUSTOM_PARAMS  & " (DialParam, intDialID ,vchParamType, vchParamValue) values "
							for each key in dctParamsToSave.Keys
								UIControlParam = split(key, "|")
								strSQL = strSQL & "('" & intDialID & UIControlParam(1) & "', " & intDialID & ", '" & UIControlParam(1) & "' ,'" & dctParamsToSave(key) & "'),"
							next
							strSQL = left(strSQL,len(strSQL)-1)
							'response.write strSQL&"thisisthesave"
							gobjConn.execute(strSQL)
						end if
					end if
				end if
			end if
		
		next
		
		
		rsTemp.MoveNext
	wend
	CloseConn
end sub

sub getRecipientList 
	Dim oXmlHTTP, oXmlResp,oXMlNodes, oXmlProductAvailabilityResult, oXmlOfferResultId, oXmlOfferResultDescription, oXmlOfferResultAvailable, dctReturnValue

    Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
    oXmlHTTP.Open "POST", "http://" & USTORE_IP & "/uStorewsapi/RecipientListWS.asmx", False 

    oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
    oXmlHTTP.setRequestHeader "SOAPAction", "uStoreWSAPI/GetRecipientList"
    'On Error Resume Next
	
    oXmlHTTP.send getRecipientListXML(request("recipientListId"))
    Response.Write getRecipientListXML(request("recipientListId"))  
	Response.Write oXmlHTTP.responseText
    Response.End
    
    
    Set oXmlResp = oXmlHTTP.responseXML
    
    set dctReturnValue = Server.CreateObject("Scripting.Dictionary")
    
    Set oXmlNodes = oXmlResp.getElementsByTagName("Unapproved")
    dctReturnValue("Unapproved") = oXmlNodes(0).Text
 
	
end sub

sub addRecipientList 
	'TO DO
	'finish translastion off xls to xml
	Dim oXmlHTTP, oXmlResp,oXMlNodes, oXmlProductAvailabilityResult, oXmlOfferResultId, oXmlOfferResultDescription, oXmlOfferResultAvailable, dctReturnValue

    Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
    oXmlHTTP.Open "POST", "http://" & USTORE_IP & "/uStorewsapi/RecipientListWS.asmx", False 

    oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
    oXmlHTTP.setRequestHeader "SOAPAction", "uStoreWSAPI/AddRecipientList"
    'On Error Resume Next
	'readXLS(request("fileName"))
	'response.end
	
    oXmlHTTP.send addRecipientListXML(request("filePath"), request("fileName"))
    Response.Write addRecipientListXML(request("filePath"), request("fileName"))  
	Response.Write oXmlHTTP.responseText
    Response.End
    
    
    Set oXmlResp = oXmlHTTP.responseXML
    
    set dctReturnValue = Server.CreateObject("Scripting.Dictionary")
    
    Set oXmlNodes = oXmlResp.getElementsByTagName("Unapproved")
    dctReturnValue("Unapproved") = oXmlNodes(0).Text
 
end sub

sub getAllProperties 
	'TO DO
	'finish translastion off xls to xml
	Dim oXmlHTTP, oXmlResp,oXMlNodes, oXmlProductAvailabilityResult, oXmlOfferResultId, oXmlOfferResultDescription, oXmlOfferResultAvailable, dctReturnValue

    Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
    oXmlHTTP.Open "POST", "http://" & UPRODUCE_IP & "/XMPieWSAPI/Document_SSP.asmx", False 

    oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
    oXmlHTTP.setRequestHeader "SOAPAction", "XMPieWSAPI/GetSettings"
    'On Error Resume Next
	'readXLS(request("fileName"))
	'response.end
	
    oXmlHTTP.send getAllPropertiesXML(request("uProduceID"))
    Response.Write getAllPropertiesXML(request("uProduceID"))
	Response.Write oXmlHTTP.responseText
    Response.End
    
    
    Set oXmlResp = oXmlHTTP.responseXML
    
    set dctReturnValue = Server.CreateObject("Scripting.Dictionary")
    
    Set oXmlNodes = oXmlResp.getElementsByTagName("Unapproved")
    dctReturnValue("Unapproved") = oXmlNodes(0).Text
 
	
end sub

function ProcessTicket(ticketTableID, uProduceID, ticketType, ticketID)
	Dim oXmlHTTP, oXmlResp,oXMlNodes, oXmlProductAvailabilityResult, oXmlOfferResultId, oXmlOfferResultDescription, oXmlOfferResultAvailable, dctReturnValue, saveType, strSQL, rsTemp, columnName, jobTicketID, fs, downloadURL, processJobID, strFileName, intQuantity, strFolderName
	
	if ticketType = "proof" then
		columnName = "intProofJobTicketID"
	else
		columnName = "intPrintJobTicketID"
	end if
	
	if ticketID = "" or isNull(ticketID) then
		if ticketType = "proof" then
			Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
			oXmlHTTP.Open "POST", "http://" & UPRODUCE_IP & "/XMPieWSAPI/JobTicket_SSP.asmx", False 

			oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
			oXmlHTTP.setRequestHeader "SOAPAction", "XMPieWSAPI/CreateNewTicketForDocument"
			'On Error Resume Next
			  
			oXmlHTTP.send createNewTicketForDocumentXML(uProduceID)
			'Response.Write createNewTicketForDocumentXML(uProduceID)
			
			'Response.Write oXmlHTTP.responseText
			'Response.End
			
			
			Set oXmlResp = oXmlHTTP.responseXML
			
			set dctReturnValue = Server.CreateObject("Scripting.Dictionary")
			
			Set oXmlNodes = oXmlResp.getElementsByTagName("CreateNewTicketForDocumentResult")
			ticketID = oXmlNodes(0).Text
		else
			strSQL = "select intUProduceProccessJobID from " & TABLE_PRODUCT_TABLE & " where intUProduceID = " & uProduceID
			set rsTemp = gobjConn.execute(strSQL)
			
			processJobID = rsTemp("intUProduceProccessJobID")
			Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
			oXmlHTTP.Open "POST", "http://" & UPRODUCE_IP & "/XMPieWSAPI/JobTicket_SSP.asmx", False 

			oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
			oXmlHTTP.setRequestHeader "SOAPAction", "XMPieWSAPI/LoadJobTicket"
			'On Error Resume Next
			  
			oXmlHTTP.send loadJobTicketXML(rsTemp("intUProduceProccessJobID"))
			'Response.Write loadJobTicketXML(rsTemp("intUProduceProccessJobID"))
			
			'Response.Write oXmlHTTP.responseText
			'Response.End
			
			Set oXmlResp = oXmlHTTP.responseXML
			
			set dctReturnValue = Server.CreateObject("Scripting.Dictionary")
			
			Set oXmlNodes = oXmlResp.getElementsByTagName("LoadJobTicketResult")
			ticketID = oXmlNodes(0).Text
		end if
		
		if ticketType = "proof" then
			saveType = "intProofTicketID"
		else
			saveType = "intPrintTicketID"
		end if 
		
		strSQL = "update " & TABLE_PRODUCT_TICKETS & " set " & saveType & " = " & ticketID & " where intID = " & ticketTableID
		gobjConn.execute(strSQL)
		 
	end if
	
		Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
		oXmlHTTP.Open "POST", "http://" & UPRODUCE_IP & "/XMPieWSAPI/JobTicket_SSP.asmx", False 
	
		oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
		oXmlHTTP.setRequestHeader "SOAPAction", "XMPieWSAPI/SetOutputInfo"
		
		oXmlHTTP.send setOutputInfoXML(ticketTableID, uProduceID, ticketType, ticketID)
		'Response.Write setOutputInfoXML(ticketTableID, uProduceID, ticketType, ticketID)  
		'Response.Write oXmlHTTP.responseText
		strFolderName = split(TABLE_PRODUCT_TICKETS , "_")
		
		if ticketType <> "proof" then
			strSQL = "Select chrStatus from " & TABLE_PRODUCT_TICKETS  & " where intID = " & ticketTableID
			rsTemp = gobjConn.execute(strSQL)
			if rsTemp("chrStatus")&"" = "W" then
				
				strSQL = "select T.intUStoreID, T.intID as intTicketID, T.intProofJobTicketID, P.vchName, (L.intQuantity * (select I.vchBundleQuantity from " & STR_TABLE_INVENTORY_LONG_TERM  & " I where vchPartNumber = '" & left(STR_TABLE_LINEITEM_LONG_TERM, 3) & "_POD_'+ CONVERT(varchar,T.intUStoreID))) as intTrueQuantity, P.txtImage64 from " & TABLE_PRODUCT_TICKETS  & " T left join " & TABLE_PRODUCT_TABLE & " P on T.intUStoreID = P.intUStoreID left join " & STR_TABLE_LINEITEM_LONG_TERM & " L on T.intID = L.intInternalID where T.chrStatus = 'W' and T.intShopperID = " & Session(SESSION_PUB_USER_ID)
				'response.write strSQL
				rsTemp = gobjConn.execute(strSQL)
				
				intQuantity = rsTemp("intTrueQuantity")
				
				
				Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
				oXmlHTTP.Open "POST", "http://" & UPRODUCE_IP & "/XMPieWSAPI/JobTicket_SSP.asmx", False 
			
				oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
				oXmlHTTP.setRequestHeader "SOAPAction", "XMPieWSAPI/SetRI"
				
				strSQL = "Select intUProduceProccessJobID from " & TABLE_PRODUCT_TABLE & " where intUStoreID = " & ticketTableID
				gobjConn.execute(strSQL)
				'oXmlHTTP.send SQLAddRIXML(ticketID)
				strFileName = createRecipientList(strFolderName(0), ticketTableID, intQuantity)
				oXmlHTTP.send AddRIXML(ticketID, strFileName, strFolderName(0))
				'Response.Write AddRIXML(ticketID, strFileName, strFolderName(0))  
				'oXmlHTTP.setRequestHeader "SOAPAction", "XMPieWSAPI/SetDataSource"
				
				'oXmlHTTP.send setDataSourceXML(ticketID)
				'Response.Write setDataSourceXML(ticketID)  
				Response.Write oXmlHTTP.responseText
			end if
		end if
		
		Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
		oXmlHTTP.Open "POST", "http://" & UPRODUCE_IP & "/XMPieWSAPI/JobTicket_SSP.asmx", False 
	
		oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
		oXmlHTTP.setRequestHeader "SOAPAction", "XMPieWSAPI/SetCustomizations"
		
		oXmlHTTP.send SetCustomizationsXML(strFolderName(0), ticketTableID, uProduceID, ticketType, ticketID)
		'Response.Write SetCustomizationsXML(strFolderName(0), ticketTableID, uProduceID, ticketType, ticketID)
		'Response.Write oXmlHTTP.responseText
	
		Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
		oXmlHTTP.Open "POST", "http://" & UPRODUCE_IP & "/XMPieWSAPI/JobTicket_SSP.asmx", False 
	
		oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
		oXmlHTTP.setRequestHeader "SOAPAction", "XMPieWSAPI/SetJobType"
		
		oXmlHTTP.send setJobTypeXML(ticketType, ticketID)
		'Response.Write setJobTypeXML(ticketType, ticketID)
		'Response.Write oXmlHTTP.responseText
	
		Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
		oXmlHTTP.Open "POST", "http://" & UPRODUCE_IP & "/XMPieWSAPI/Production_SSP.asmx", False 
	
		oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
		oXmlHTTP.setRequestHeader "SOAPAction", "XMPieWSAPI/SubmitJob"
		
		oXmlHTTP.send submitJobXML(ticketTableID, uProduceID, ticketType, ticketID)
		'Response.Write submitJobXML(ticketTableID, uProduceID, ticketType, ticketID)  
		'Response.Write oXmlHTTP.responseText
		
		Set oXmlResp = oXmlHTTP.responseXML
		
		Set oXmlNodes = oXmlResp.getElementsByTagName("SubmitJobResult")
		jobTicketID = oXmlNodes(0).Text
		
		strSQL = "update " & TABLE_PRODUCT_TICKETS & " set " & columnName & " = " & jobTicketID & " where intID = " & ticketTableID
		gobjConn.execute(strSQL)
		'response.write strSQL
		
		
		if ticketType = "proof" then
			'loop get status until 3
			dim currentStatus
			currentStatus = "0"
			do while currentStatus <> "3"
				currentStatus = getUProduceStatus(jobTicketID)
			loop
			ProcessTicket = getDownloadURL(jobTicketID)
			'response.end
			exit function
			
		end if
		
	
 
	ProcessTicket = jobTicketID
end function

sub testLoadTicket
	dim oXmlHTTP

	Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
	oXmlHTTP.Open "POST", "http://" & UPRODUCE_IP & "/XMPieWSAPI/JobTicket_SSP.asmx", False 

	oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
	oXmlHTTP.setRequestHeader "SOAPAction", "XMPieWSAPI/LoadJobTicket"
	'On Error Resume Next
			  
	oXmlHTTP.send loadJobTicketXML("332519")
	Response.Write loadJobTicketXML("332519")
			
	'Response.Write oXmlHTTP.responseText
	Set oXmlResp = oXmlHTTP.responseXML
		
	Set oXmlNodes = oXmlResp.getElementsByTagName("SubmitJobResult")
	jobTicketID = oXmlNodes(0).Text

	Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
	oXmlHTTP.Open "POST", "http://" & UPRODUCE_IP & "/XMPieWSAPI/JobTicket_SSP.asmx", False 

	oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
	oXmlHTTP.setRequestHeader "SOAPAction", "XMPieWSAPI/LoadTicketFromTicket"
	'On Error Resume Next
			  
	oXmlHTTP.send loadTicketFromTicketXML("385")
	Response.Write loadTicketFromTicketXML("385")
			
	Response.Write oXmlHTTP.responseText
	Response.End

end sub

sub testRecipientStuff
	dim oXmlHTTP, oXmlResp, oXmlNodes, jobTicketID

	Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
	oXmlHTTP.Open "POST", "http://" & UPRODUCE_IP & "/XMPieWSAPI/Job_SSP.asmx", False 

	oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
	oXmlHTTP.setRequestHeader "SOAPAction", "XMPieWSAPI/GetTicket"
	'On Error Resume Next
			  
	oXmlHTTP.send getOriginalTicketXML(request("ticket"))
	Response.Write getOriginalTicketXML(request("ticket"))
			
	Response.Write oXmlHTTP.responseText
	
	Response.End

end sub

function getDownloadURL(ticketID)
	dim oXmlHTTP, downloadURL, oXmlResp, oXmlNodes
	
	Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
	oXmlHTTP.Open "POST", "http://" & UPRODUCE_IP & "/XMPieWSAPI/Job_SSP.asmx", False
	
	oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8"
	oXmlHTTP.setRequestHeader "SOAPAction", "XMPieWSAPI/GetOutputResultDownloadURL"
	'oXmlHTTP.setRequestHeader "SOAPAction", "XMPieWSAPI/GetOutputResults"
		
	oXmlHTTP.send getOutputResultDownloadURLXML(ticketID)
	'oXmlHTTP.send getOutputResultsXML(jobTicketID)
	'Response.Write getOutputResultDownloadURLXML(jobTicketID)  
	'Response.Write oXmlHTTP.responseText
			
	Set oXmlResp = oXmlHTTP.responseXML
		
	Set oXmlNodes = oXmlResp.getElementsByTagName("GetOutputResultDownloadURLResult")
	'Set oXmlNodes = oXmlResp.getElementsByTagName("string")
	downloadURL = oXmlNodes(0).Text
			
	getDownloadURL = downloadURL
	
end function

function checkCustomizations(ticketID)
	Dim oXmlHTTP, oXmlResp,oXMlNodes, oXmlProductAvailabilityResult, oXmlOfferResultId, oXmlOfferResultDescription, oXmlOfferResultAvailable
		Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
		oXmlHTTP.Open "POST", "http://" & UPRODUCE_IP & "/XMPieWSAPI/JobTicket_SSP.asmx", False 
	
		oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
		oXmlHTTP.setRequestHeader "SOAPAction", "XMPieWSAPI/GetCustomizations"
		
		oXmlHTTP.send getCustomizationsXML(ticketID)
		Response.Write getCustomizationsXML(ticketID)  
		Response.Write oXmlHTTP.responseText
		response.end

end function

function getUProduceStatus(ticketID)
	
	Dim oXmlHTTP, oXmlResp,oXMlNodes, oXmlProductAvailabilityResult, oXmlOfferResultId, oXmlOfferResultDescription, oXmlOfferResultAvailable, statusResult
		Set oXmlHTTP = CreateObject("Microsoft.XMLHTTP")
		oXmlHTTP.Open "POST", "http://" & UPRODUCE_IP & "/XMPieWSAPI/Job_SSP.asmx", False 
	
		oXmlHTTP.setRequestHeader "Content-Type", "text/xml; charset=utf-8" 
		oXmlHTTP.setRequestHeader "SOAPAction", "XMPieWSAPI/GetStatus"
		
		oXmlHTTP.send getStatusXML(ticketID)
		'Response.Write getStatusXML(ticketID)  
		'Response.Write oXmlHTTP.responseText
		'response.end
		
		Set oXmlResp = oXmlHTTP.responseXML
		
		Set oXmlNodes = oXmlResp.getElementsByTagName("GetStatusResult")
		statusResult = oXmlNodes(0).Text
			
		getUProduceStatus = statusResult	
end function


function formatDate(dateToFormat)
	dim newDate, splitDate
	
	splitDate = Split(dateToFormat, "-")
	newDate = splitDate(1) & "-" & splitDate(2) & "-" & splitDate(0)
	
	formatDate = newDate
end function

Function GetUStoreXml(strBodyXML)

GetUStoreXml = ""
GetUStoreXml = GetUStoreXml & "<?xml version=""1.0"" encoding=""utf-8""?>" &vbNewLine
GetUStoreXml = GetUStoreXml & "<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">" &vbNewLine
'GetUStoreXml = GetUStoreXml & SoapHeader

GetUStoreXml = GetUStoreXml & "<soap:Body>" &vbNewLine

GetUStoreXml = GetUStoreXml & strBodyXML

GetUStoreXml = GetUStoreXml & "</soap:Body>" &vbNewLine
GetUStoreXml = GetUStoreXml & "</soap:Envelope>" &vbNewLine

End Function



function getProductGroupListXML()
	dim strXml

	strXml = ""
	strXml = strXML & "<GetProductGroupList xmlns=""uStoreWSAPI"">" & vbNewLine
	strXml = strXML & "<username>" & STR_USTORE_USERNAME & "</username>" & vbNewLine
	strXml = strXML & "<password>" & STR_USTORE_PASSWORD & "</password>" & vbNewLine
	strXml = strXML & "<storeId>" & INT_STORE_ID & "</storeId>" & vbNewLine
	strXml = strXML & "<cultureId>1</cultureId>" & vbNewLine
	strXml = strXML & "</GetProductGroupList>" & vbNewLine


	getProductGroupListXML = GetUStoreXml(strXml)
	
end function

function getProductListXML(productGroupId)
	dim strXml

	strXml = ""
	strXml = strXML & "<GetProductList xmlns=""uStoreWSAPI"">" & vbNewLine
	strXml = strXML & "<username>" & STR_USTORE_USERNAME & "</username>" & vbNewLine
	strXml = strXML & "<password>" & STR_USTORE_PASSWORD & "</password>" & vbNewLine
	strXml = strXML & "<productGroupId>" & productGroupId & "</productGroupId>" & vbNewLine
	strXml = strXML & "<cultureId>1</cultureId>" & vbNewLine
	strXml = strXML & "<isReturnInactive>true</isReturnInactive>" & vbNewLine
	strXml = strXML & "</GetProductList>" & vbNewLine

	getProductListXML = GetUStoreXml(strXml)
	
end function

function getProductXML(productID)
	dim strXml

	strXml = ""
	strXml = strXML & "<GetProduct xmlns=""uStoreWSAPI"">" & vbNewLine
	strXml = strXML & "<username>" & STR_USTORE_USERNAME & "</username>" & vbNewLine
	strXml = strXML & "<password>" & STR_USTORE_PASSWORD & "</password>" & vbNewLine
	strXml = strXML & "<productId>" & productID & "</productId>" & vbNewLine
	strXml = strXML & "<cultureId>1</cultureId>" & vbNewLine
	strXml = strXML & "</GetProduct>" & vbNewLine


	getProductXML = GetUStoreXml(strXml)
	
end function

function getProductPropertyListXML(productID)
	dim strXml

	strXml = ""
	strXml = strXML & "<GetProductPropertyList xmlns=""uStoreWSAPI"">" & vbNewLine
	strXml = strXML & "<username>" & STR_USTORE_USERNAME & "</username>" & vbNewLine
	strXml = strXML & "<password>" & STR_USTORE_PASSWORD & "</password>" & vbNewLine
	strXml = strXML & "<productId>" & productID & "</productId>" & vbNewLine
	strXml = strXML & "<cultureId>1</cultureId>" & vbNewLine
	strXml = strXML & "</GetProductPropertyList>" & vbNewLine


	getProductPropertyListXML = GetUStoreXml(strXml)
	
end function

function getProductThumbnailsXML(productID)
	dim strXml

	strXml = ""
	strXml = strXML & "<GetProductThumbnails xmlns=""uStoreWSAPI"">" & vbNewLine
	strXml = strXML & "<username>" & STR_USTORE_USERNAME & "</username>" & vbNewLine
	strXml = strXML & "<password>" & STR_USTORE_PASSWORD & "</password>" & vbNewLine
	strXml = strXML & "<productId>" & productID & "</productId>" & vbNewLine
	strXml = strXML & "</GetProductThumbnails>" & vbNewLine


	getProductThumbnailsXML = GetUStoreXml(strXml)
	
end function

function getDocumentByProductXML(productID)
	dim strXml

	strXml = ""
	strXml = strXML & "<GetDocumentByProduct xmlns=""uStoreWSAPI"">" & vbNewLine
	strXml = strXML & "<username>" & STR_USTORE_USERNAME & "</username>" & vbNewLine
	strXml = strXML & "<password>" & STR_USTORE_PASSWORD & "</password>" & vbNewLine
	strXml = strXML & "<productId>" & productID & "</productId>" & vbNewLine
	strXml = strXML & "</GetDocumentByProduct>" & vbNewLine


	getDocumentByProductXML = GetUStoreXml(strXml)
	
end function


function getCustomizationStepListXML(productid)
	dim strXml

	strXml = ""
	strXml = strXML & "<GetCustomizationStepList xmlns=""uStoreWSAPI"">" & vbNewLine
	strXml = strXML & "<username>" & STR_USTORE_USERNAME & "</username>" & vbNewLine
	strXml = strXML & "<password>" & STR_USTORE_PASSWORD & "</password>" & vbNewLine
	strXml = strXML & "<productId>" & productid & "</productId>" & vbNewLine
	strXml = strXML & "<cultureId>1</cultureId>" & vbNewLine
	strXml = strXML & "</GetCustomizationStepList>" & vbNewLine


	getCustomizationStepListXML = GetUStoreXml(strXml)
	
end function

function getCustomizationDialListXML(customizationStepId)
	dim strXml

	strXml = ""
	strXml = strXML & "<GetCustomizationDialList xmlns=""uStoreWSAPI"">" & vbNewLine
	strXml = strXML & "<username>" & STR_USTORE_USERNAME & "</username>" & vbNewLine
	strXml = strXML & "<password>" & STR_USTORE_PASSWORD & "</password>" & vbNewLine
	strXml = strXML & "<customizationStepId>" & customizationStepId & "</customizationStepId>" & vbNewLine
	strXml = strXML & "<cultureId>1</cultureId>" & vbNewLine
	strXml = strXML & "</GetCustomizationDialList>" & vbNewLine


	getCustomizationDialListXML = GetUStoreXml(strXml)
	
end function

function getRecipientListXML(recipientListId)
	dim strXml

	strXml = ""
	strXml = strXML & "<GetRecipientList xmlns=""uStoreWSAPI"">" & vbNewLine
	strXml = strXML & "<username>" & STR_USTORE_USERNAME & "</username>" & vbNewLine
	strXml = strXML & "<password>" & STR_USTORE_PASSWORD & "</password>" & vbNewLine
	strXml = strXML & "<recipientListId>" & recipientListId & "</recipientListId>" & vbNewLine
	strXml = strXML & "</GetRecipientList>" & vbNewLine


	getRecipientListXML = GetUStoreXml(strXml)
	
end function

function GetCustomizationsXML(ticketID)
	dim strXml

	strXml = ""
	strXml = strXML & "<GetCustomizations xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD & "</inPassword>" & vbNewLine
	strXml = strXML & "<inTicketID>" & ticketID & "</inTicketID>" & vbNewLine
	strXml = strXML & "</GetCustomizations>" & vbNewLine


	GetCustomizationsXML = GetUStoreXml(strXml)
	
end function

function SetCustomizationsXML(strFolderName, ticketTableID, uProduceID, ticketType, ticketID)
	dim strXml, strParamSQL, rsParamSQL, XMPType

	strXml = ""
	strXml = strXML & "<SetCustomizations xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD & "</inPassword>" & vbNewLine
	strXml = strXML & "<inTicketID>" & ticketID & "</inTicketID>" & vbNewLine
	strXml = strXML & "<inCustomizations>" & vbNewLine
	
	strParamSQL = "select D.vchUProduceDialName, SD.vchDialValue, D.blnIsVariable, D.intControlType from " & TABLE_PRODUCT_SAVED_DIALS & " SD left join " & TABLE_PRODUCT_TICKETS & " T on SD.intTicketTableID = T.intID left join " & TABLE_PRODUCT_CUSTOM_DIALS & " D on T.intUstoreID = D.intDocID and SD.vchDialName = D.vchUProduceDialName where SD.intTicketTableID = " & ticketTableID
	'response.write strParamSQL
	set rsParamSQL = gobjConn.execute(strParamSQL)
	
	while not rsParamSQL.EOF
		strXml = strXML & "<Customization>" & vbNewLine
		strXml = strXML & "<m_Name>" & rsParamSQL("vchUProduceDialName") & "</m_Name>" & vbNewLine
		strXml = strXML & "<m_Type>"
		if rsParamSQL("blnIsVariable") then
			strXml = strXML & "VAR"
		else
			strXml = strXML & "ADOR"
		end if
		strXml = strXML &"</m_Type>" & vbNewLine
		'response.write rsParamSQL("vchDialValue")&"here"
		if rsParamSQL("intControlType") = 18 then
			if rsParamSQL("vchDialValue")&"" <> "" then
				strXml = strXML & "<m_Expression>""\\\\10.0.10.70\\output\\" & strFolderName & "\\images\\" & rsParamSQL("vchDialValue") & """</m_Expression>" & vbNewLine
			else
				strXml = strXML & "<m_Expression>""""</m_Expression>" & vbNewLine
			end if
		else
			strXml = strXML & "<m_Expression>AsString(""" & rsParamSQL("vchDialValue") & """)</m_Expression>" & vbNewLine
		end if
		
		strXml = strXML & "</Customization>" & vbNewLine
		rsParamSQL.MoveNext
	wend
	strXml = strXML & "</inCustomizations>" & vbNewLine
	strXml = strXML & "</SetCustomizations>" & vbNewLine
	
	SetCustomizationsXML = GetUStoreXml(strXml)
	
end function

function createNewTicketForDocumentXML(uProduceID)
	dim strXml

	strXml = ""
	strXml = strXML & "<CreateNewTicketForDocument xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME  & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD  & "</inPassword>" & vbNewLine
	strXml = strXML & "<inDocumentID>" & uProduceID & "</inDocumentID>" & vbNewLine
	strXml = strXML & "<inRITableName></inRITableName>" & vbNewLine
	strXml = strXML & "<inFlatOriented>false</inFlatOriented>" & vbNewLine
	strXml = strXML & "</CreateNewTicketForDocument>" & vbNewLine


	createNewTicketForDocumentXML = GetUStoreXml(strXml)
	
end function

function getRecipientTableIDXML(tableName)
	dim strXml

	strXml = ""
	strXml = strXML & "<GetID xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME  & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD  & "</inPassword>" & vbNewLine
	strXml = strXML & "<inDataSourceID>4552</inDataSourceID>" & vbNewLine
	strXml = strXML & "<inTableName>" & tableName & "</inTableName>" & vbNewLine
	strXml = strXML & "</GetID>" & vbNewLine

	getRecipientTableIDXML = GetUStoreXml(strXml)
	
end function

function getRecipientTablesXML(tableName)
	dim strXml

	strXml = ""
	strXml = strXML & "<GetRecipientTablesDataSet xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME  & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD  & "</inPassword>" & vbNewLine
	strXml = strXML & "<inDataSourceID>4552</inDataSourceID>" & vbNewLine
	strXml = strXML & "</GetRecipientTablesDataSet>" & vbNewLine

	getRecipientTablesXML = GetUStoreXml(strXml)
	
end function

function getOriginalTicketXML(ticketID)
	dim strXml

	strXml = ""
	strXml = strXML & "<GetTicket xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME  & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD  & "</inPassword>" & vbNewLine
	strXml = strXML & "<inJobID>" & ticketID & "</inJobID>" & vbNewLine
	strXml = strXML & "</GetTicket>" & vbNewLine

	getOriginalTicketXML = GetUStoreXml(strXml)
	
end function

function getNthRIInfoXML(ticketID)
	dim strXml

	strXml = ""
	strXml = strXML & "<GetRecipientTablesDataSet xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME  & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD  & "</inPassword>" & vbNewLine
	strXml = strXML & "<inTicketID>" & ticketID & "</inTicketID>" & vbNewLine
	strXml = strXML & "<inRIIndex>1</inRIIndex>" & vbNewLine
	strXml = strXML & "</GetRecipientTablesDataSet>" & vbNewLine

	getNthRIInfoXML = GetUStoreXml(strXml)
	
end function

function getDataSourceIDXML(tableName)
	dim strXml

	strXml = ""
	strXml = strXML & "<GetID xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME  & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD  & "</inPassword>" & vbNewLine
	strXml = strXML & "<inCampaignID>2648</inCampaignID>" & vbNewLine
	strXml = strXML & "<inDataSourceName>" & tableName & "</inDataSourceName>" & vbNewLine
	strXml = strXML & "</GetID>" & vbNewLine

	getDataSourceIDXML = GetUStoreXml(strXml)
	
end function


function createTicketDocumentXML()
	dim strXml

	strXml = ""
	strXml = strXML & "<CreateNewTicket xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME  & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD  & "</inPassword>" & vbNewLine
	'strXml = strXML & "<inDocumentID>" & uProduceID & "</inDocumentID>" & vbNewLine
	'strXml = strXML & "<inRITableName></inRITableName>" & vbNewLine
	'strXml = strXML & "<inFlatOriented>false</inFlatOriented>" & vbNewLine
	strXml = strXML & "</CreateNewTicket>" & vbNewLine


	createTicketDocumentXML = GetUStoreXml(strXml)
	
end function

function getAllPropertiesXML(uProduceID)
	dim strXml

	strXml = ""
	strXml = strXML & "<GetSettings xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME  & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD  & "</inPassword>" & vbNewLine
	strXml = strXML & "<inDocumentID>" & uProduceID & "</inDocumentID>" & vbNewLine
	strXml = strXML & "<inSettingsNames>"& vbNewLine
	strXml = strXML & "<string>Margin</string>" & vbNewLine
	strXml = strXML & "</inSettingsNames>"& vbNewLine
	strXml = strXML & "<inIncludeInheritable>true</inIncludeInheritable>" & vbNewLine
	strXml = strXML & "</GetSettings>" & vbNewLine


	getAllPropertiesXML = GetUStoreXml(strXml)
	
end function

function createOnDemandOutputXML(ticketTableID, uProduceID, ticketType, ticketID)
	dim strXml, strType, strParamSQL, rsParamSQL, strProductSQL, rsProduct

	if ticketType = "proof" then
		strType = "PDF"
	end if
	
	strXml = ""
	strXml = strXML & "<CreateOnDemandOutput xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME  & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD  & "</inPassword>" & vbNewLine
    strXml = strXML & "<inProps>" & vbNewLine
	
	strParamSQL = "select vchDialName, vchDialValue from " & TABLE_PRODUCT_SAVED_DIALS & " where intTicketTableID = " & ticketTableID
	set rsParamSQL = gobjConn.execute(strParamSQL)
	
	while not rsParamSQL.EOF
		strXml = strXML & "<Property>" & vbNewLine
		strXml = strXML & "<m_Name>" & rsParamSQL("vchDialName") & "</m_Name>" & vbNewLine
		strXml = strXML & "<m_Value>AsString(""" & rsParamSQL("vchDialValue") & """)</m_Value>" & vbNewLine
		strXml = strXML & "</Property>" & vbNewLine
		rsParamSQL.MoveNext
	wend
    
	strXml = strXML & "</inProps>" & vbNewLine
	strXml = strXML & "</CreateOnDemandOutput>" & vbNewLine

	createOnDemandOutputXML = GetUStoreXml(strXml)
	
end function

function setOutputInfoXML(ticketTableID, uProduceID, ticketType, ticketID)
	dim strXml, strType, strParamSQL, rsParamSQL, strProductSQL, rsProduct

	if ticketType = "proof" then
		strType = "PDF"
	else
		strType = "PDFO"
	end if
	
	strProductSQL = " select * from " & TABLE_PRODUCT_TABLE & " where intUProduceID = " & uProduceID
	set rsProduct = gobjConn.execute(strProductSQL)
	
	strXml = ""
	strXml = strXML & "<SetOutputInfo xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME  & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD  & "</inPassword>" & vbNewLine
	strXml = strXML & "<inTicketID>" & ticketID & "</inTicketID>" & vbNewLine
    strXml = strXML & "<inType>" & strType & "</inType>" & vbNewLine
    strXml = strXML & "<inMedia>" & rsProduct("intTemplateType") & "</inMedia>" & vbNewLine
    strXml = strXML & "<inFolder></inFolder>" & vbNewLine
    strXml = strXML & "<inFileName>" & rsProduct("vchName") & "</inFileName>" & vbNewLine
    'strXml = strXML & "<inFileName>Test File- DO NOT PRINT</inFileName>" & vbNewLine
    strXml = strXML & "<inParams>" & vbNewLine
	
	'strParamSQL = "select vchDialName, vchDialValue from " & TABLE_PRODUCT_SAVED_DIALS & " where intTicketTableID = " & ticketTableID
	'set rsParamSQL = gobjConn.execute(strParamSQL)
	
	'while not rsParamSQL.EOF
		strXml = strXML & "<Parameter>" & vbNewLine
		strXml = strXML & "<m_Name>COPY_TYPE</m_Name>" & vbNewLine
		strXml = strXML & "<m_Value>0</m_Value>" & vbNewLine
		strXml = strXML & "</Parameter>" & vbNewLine
		strXml = strXML & "<Parameter>" & vbNewLine
		strXml = strXML & "<m_Name>COPY_NUM</m_Name>" & vbNewLine
		strXml = strXML & "<m_Value>1</m_Value>" & vbNewLine
		strXml = strXML & "</Parameter>" & vbNewLine
	'	rsParamSQL.MoveNext
	'wend
    
	strXml = strXML & "</inParams>" & vbNewLine
	strXml = strXML & "</SetOutputInfo>" & vbNewLine


	setOutputInfoXML = GetUStoreXml(strXml)
	
end function

function setDocumentByIDXML(uProduceID, ticketType, ticketID)
	dim strXml, strParamSQL, rsParamSQL, strProductSQL, rsProduct
		
	strXml = ""
	strXml = strXML & "<SetDocumentByID xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME  & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD  & "</inPassword>" & vbNewLine
	strXml = strXML & "<inTicketID>" & ticketID & "</inTicketID>" & vbNewLine
    strXml = strXML & "<inDocumentID>" & uProduceID & "</inDocumentID>" & vbNewLine
	strXml = strXML & "</SetDocumentByID>" & vbNewLine

	setDocumentByIDXML = GetUStoreXml(strXml)
	
end function

function setJobTypeXML(ticketType, ticketID)
	dim strXml, strType, strParamSQL, rsParamSQL, strProductSQL, rsProduct

	if ticketType = "proof" then
		strType = "PROOF"
	else
		strType = "PRINT"
	end if
		
	strXml = ""
	strXml = strXML & "<SetJobType xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME  & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD  & "</inPassword>" & vbNewLine
	strXml = strXML & "<inTicketID>" & ticketID & "</inTicketID>" & vbNewLine
    strXml = strXML & "<inJobType>" & strType & "</inJobType>" & vbNewLine
	strXml = strXML & "</SetJobType>" & vbNewLine

	setJobTypeXML = GetUStoreXml(strXml)
	
end function

function submitJobXML(ticketTableID, uProduceID, ticketType, ticketID)
	dim strXml, strParamSQL, rsParamSQL, strProductSQL, rsProduct
	
	strProductSQL = " select * from " & TABLE_PRODUCT_TABLE & " where intUProduceID = " & uProduceID
	set rsProduct = gobjConn.execute(strProductSQL)
	
	strXml = ""
	strXml = strXML & "<SubmitJob xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME  & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD  & "</inPassword>" & vbNewLine
	strXml = strXML & "<inJobTicket>" & ticketID & "</inJobTicket>" & vbNewLine
    strXml = strXML & "<inPriority>3</inPriority>" & vbNewLine
    strXml = strXML & "<inTouchPointID></inTouchPointID>" & vbNewLine
    strXml = strXML & "<inProps>" & vbNewLine
    strXml = strXML & "<Property>" & vbNewLine
    strXml = strXML & "</Property>" & vbNewLine
	strXml = strXML & "</inProps>" & vbNewLine
	strXml = strXML & "</SubmitJob>" & vbNewLine

	submitJobXML = GetUStoreXml(strXml)
	
end function

function getOutputResultsXML(ticketID)
	dim strXml
		
	strXml = ""
	strXml = strXML & "<GetOutputResults xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME  & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD  & "</inPassword>" & vbNewLine
	strXml = strXML & "<inJobID>" & ticketID & "</inJobID>" & vbNewLine
	strXml = strXML & "</GetOutputResults>" & vbNewLine

	getOutputResultsXML = GetUStoreXml(strXml)
	
end function

function getOutputResultDownloadURLXML(ticketID)
	dim strXml
		
	strXml = ""
	strXml = strXML & "<GetOutputResultDownloadURL xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME  & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD  & "</inPassword>" & vbNewLine
	strXml = strXML & "<inJobID>" & ticketID & "</inJobID>" & vbNewLine
	strXml = strXML & "<inResultIndex>0</inResultIndex>" & vbNewLine
	strXml = strXML & "<inMimeType>application/pdf</inMimeType>" & vbNewLine
	strXml = strXML & "<inIsInline>false</inIsInline>" & vbNewLine
	strXml = strXML & "<inMacTypeHex></inMacTypeHex>" & vbNewLine
	strXml = strXML & "<inMacCreatorHex></inMacCreatorHex>" & vbNewLine
	strXml = strXML & "<inReturnInternalURL>false</inReturnInternalURL>" & vbNewLine
	strXml = strXML & "</GetOutputResultDownloadURL>" & vbNewLine

	getOutputResultDownloadURLXML = GetUStoreXml(strXml)
	
end function

function getOutputResultsXML(ticketID)
	dim strXml
		
	strXml = ""
	strXml = strXML & "<GetOutputResults xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME  & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD  & "</inPassword>" & vbNewLine
	strXml = strXML & "<inJobID>" & ticketID & "</inJobID>" & vbNewLine
	strXml = strXML & "</GetOutputResults>" & vbNewLine

	getOutputResultsXML = GetUStoreXml(strXml)
	
end function

function loadTicketFromTicketXML(ticketID)
	dim strXml
		
	strXml = ""
	strXml = strXML & "<LoadTicketFromTicket xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME  & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD  & "</inPassword>" & vbNewLine
	strXml = strXML & "<inTicket>" & ticketID & "</inTicket>" & vbNewLine
	strXml = strXML & "</LoadTicketFromTicket>" & vbNewLine

	loadTicketFromTicketXML = GetUStoreXml(strXml)
	
end function

function SQLAddRIXML(ticketID)
	dim strXml
		
	strXml = ""
	strXml = strXML & "<SetRI xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME  & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD  & "</inPassword>" & vbNewLine
	strXml = strXML & "<inTicketID>" & ticketID & "</inTicketID>" & vbNewLine
	strXml = strXML & "<inRIInfo>" & vbNewLine
	strXml = strXML & "<m_From></m_From>" & vbNewLine
	strXml = strXML & "<m_To></m_To>" & vbNewLine
	strXml = strXML & "<m_FilterType>1</m_FilterType>" & vbNewLine
	strXml = strXML & "<m_Filter>select * from (select vchDialName, vchDialValue from [Athleta_POD_SavedDials] where intTicketTableID = 45) src pivot ( min(vchDialValue) for vchDialName in ([Description],[Heading],[Image],[Store Information])) piv</m_Filter>" & vbNewLine
	'strXml = strXML & "<m_Filter>select * from PODSendTest</m_Filter>" & vbNewLine
	strXml = strXML & "<m_SubFilter></m_SubFilter>" & vbNewLine
	strXml = strXML & "<m_SelectOneQuery></m_SelectOneQuery>" & vbNewLine
	strXml = strXML & "<m_InsertQuery></m_InsertQuery>" & vbNewLine
	strXml = strXML & "<m_UpdateQuery></m_UpdateQuery>" & vbNewLine
	strXml = strXML & "<m_DeleteQuery></m_DeleteQuery>" & vbNewLine
	strXml = strXML & "<m_recipientIDListFileName></m_recipientIDListFileName>" & vbNewLine
	strXml = strXML & "<m_recipientIDListMergeType>0</m_recipientIDListMergeType>" & vbNewLine
	strXml = strXML & "</inRIInfo>" & vbNewLine
	strXml = strXML & "<inConnection>" & vbNewLine
	'strXml = strXML & "<m_Type>TXT</m_Type>" & vbNewLine
	'strXml = strXML & "<m_ConnectionString>Provider=Microsoft.Jet.OLEDB.4.0;Data Source=D:\XMPie\;Extended Properties=text;</m_ConnectionString>" & vbNewLine
	'strXml = strXML & "<m_AdditionalInfo>xmpietest.txt@,@,</m_AdditionalInfo>" & vbNewLine 'file:////ServerName/data/myXML.xml
	strXml = strXML & "<m_Type>MSQL</m_Type>" & vbNewLine
	strXml = strXML & "<m_ConnectionString>Provider=SQLOLEDB.1;Data source=10.0.10.70;Initial Catalog=WebData2;User id=webuser2;Password=welcome</m_ConnectionString>" & vbNewLine
	strXml = strXML & "<m_AdditionalInfo></m_AdditionalInfo>" & vbNewLine
	strXml = strXML & "</inConnection>" & vbNewLine
	strXml = strXML & "</SetRI>" & vbNewLine

	SQLAddRIXML = GetUStoreXml(strXml)
	
end function

function AddRIXML(ticketID, strFileName, strFolderName)
	dim strXml, strJustName
	
	strJustName = split(strFileName, ".")
		
	strXml = ""
	strXml = strXML & "<SetRI xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME  & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD  & "</inPassword>" & vbNewLine
	strXml = strXML & "<inTicketID>" & ticketID & "</inTicketID>" & vbNewLine
	strXml = strXML & "<inRIInfo>" & vbNewLine
	strXml = strXML & "<m_From>0</m_From>" & vbNewLine
	strXml = strXML & "<m_To>0</m_To>" & vbNewLine
	strXml = strXML & "<m_FilterType>3</m_FilterType>" & vbNewLine
	strXml = strXML & "<m_Filter>" & strJustName(0) & "</m_Filter>" & vbNewLine
	strXml = strXML & "<m_SubFilter></m_SubFilter>" & vbNewLine
	strXml = strXML & "<m_SelectOneQuery></m_SelectOneQuery>" & vbNewLine
	strXml = strXML & "<m_InsertQuery></m_InsertQuery>" & vbNewLine
	strXml = strXML & "<m_UpdateQuery></m_UpdateQuery>" & vbNewLine
	strXml = strXML & "<m_DeleteQuery></m_DeleteQuery>" & vbNewLine
	strXml = strXML & "<m_recipientIDListFileName></m_recipientIDListFileName>" & vbNewLine
	strXml = strXML & "<m_recipientIDListMergeType>0</m_recipientIDListMergeType>" & vbNewLine
	strXml = strXML & "</inRIInfo>" & vbNewLine
	strXml = strXML & "<inConnection>" & vbNewLine
	'strXml = strXML & "<m_Type>XLS</m_Type>" & vbNewLine
	strXml = strXML & "<m_Type>XML</m_Type>" & vbNewLine
	'strXml = strXML & "<m_ConnectionString>Provider=Microsoft.Jet.OLEDB.4.0;Data Source=D:\XMPie\xmpietest.xls;Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1;"";</m_ConnectionString>" & vbNewLine
	strXml = strXML & "<m_ConnectionString>\\10.0.10.70\output\" & strFolderName & "\" & strFileName & "</m_ConnectionString>" & vbNewLine
	'strXml = strXML & "<m_Type>TXT</m_Type>" & vbNewLine
	'strXml = strXML & "<m_ConnectionString>Provider=Microsoft.Jet.OLEDB.4.0;Data Source=D:\XMPie\;Extended Properties=text;</m_ConnectionString>" & vbNewLine
	'strXml = strXML & "<m_AdditionalInfo>xmpietest.txt@,@,</m_AdditionalInfo>" & vbNewLine 'file:////ServerName/data/myXML.xml
	'strXml = strXML & "<m_Type>XML</m_Type>" & vbNewLine
	'strXml = strXML & "<m_ConnectionString>\\" & UPRODUCE_IP & "\XMPie\484203_Recipient.xml</m_ConnectionString>" & vbNewLine
	strXml = strXML & "<m_AdditionalInfo></m_AdditionalInfo>" & vbNewLine
	strXml = strXML & "</inConnection>" & vbNewLine
	strXml = strXML & "</SetRI>" & vbNewLine

	AddRIXML = GetUStoreXml(strXml)
	
end function

function setDataSourceXML(ticketID)
	dim strXml
		
	strXml = ""
	strXml = strXML & "<SetDataSource xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME  & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD  & "</inPassword>" & vbNewLine
	strXml = strXML & "<inTicketID>" & ticketID & "</inTicketID>" & vbNewLine
	strXml = strXML & "<inSchemaName>843845_Recipient</inSchemaName>" & vbNewLine
	strXml = strXML & "<inConnection>" & vbNewLine
	strXml = strXML & "<m_Type>XML</m_Type>" & vbNewLine
	strXml = strXML & "<m_ConnectionString>10.0.10.88\AccuV30Assets\mtest\843845_Recipient.xml</m_ConnectionString>" & vbNewLine
	strXml = strXML & "<m_AdditionalInfo></m_AdditionalInfo>" & vbNewLine
	strXml = strXML & "</inConnection>" & vbNewLine
	
	strXml = strXML & "</SetDataSource>" & vbNewLine

	setDataSourceXML = GetUStoreXml(strXml)
	
end function

function loadJobTicketXML(ticketID)
	dim strXml
		
	strXml = ""
	strXml = strXML & "<LoadJobTicket xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME  & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD  & "</inPassword>" & vbNewLine
	strXml = strXML & "<inJobID>" & ticketID & "</inJobID>" & vbNewLine
	strXml = strXML & "</LoadJobTicket>" & vbNewLine

	loadJobTicketXML = GetUStoreXml(strXml)
	
end function

function getFolderPathXML(ticketID)
	dim strXml
		
	strXml = ""
	strXml = strXML & "<GetFolderPath xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME  & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD  & "</inPassword>" & vbNewLine
	strXml = strXML & "<inJobID>" & ticketID & "</inJobID>" & vbNewLine
	strXml = strXML & "</GetFolderPath>" & vbNewLine

	getFolderPathXML = GetUStoreXml(strXml)
	
end function

function getStatusXML(ticketID)
	dim strXml
		
	strXml = ""
	strXml = strXML & "<GetStatus xmlns=""XMPieWSAPI"">" & vbNewLine
	strXml = strXML & "<inUsername>" & STR_UPRODUCE_USERNAME  & "</inUsername>" & vbNewLine
	strXml = strXML & "<inPassword>" & STR_UPRODUCE_PASSWORD  & "</inPassword>" & vbNewLine
	strXml = strXML & "<inJobID>" & ticketID & "</inJobID>" & vbNewLine
	strXml = strXML & "</GetStatus>" & vbNewLine

	getStatusXML = GetUStoreXml(strXml)
	
end function

function addRecipientListXML(filePath, fileName)
	dim strXml

	strXml = ""
	strXml = strXML & "<AddRecipientList xmlns=""uStoreWSAPI"">" & vbNewLine
	strXml = strXML & "<username>" & STR_USTORE_USERNAME & "</username>" & vbNewLine
	strXml = strXML & "<password>" & STR_USTORE_PASSWORD & "</password>" & vbNewLine
	strXml = strXML & "<recipientList>" & vbNewLine
	
	strXml = strXML & "<recipientListId></recipientListId>" & vbNewLine
	strXml = strXML & "<FilePath>" & filePath & "</FilePath>" & vbNewLine
	strXml = strXML & "<FileName>" & fileName & "</FileName>" & vbNewLine
	strXml = strXML & "<SourceTable>>&amp;apos;Graton BC$&amp;apos;</SourceTable>" & vbNewLine
	strXml = strXML & "<Format>1</Format>" & vbNewLine
	strXml = strXML & "<ItemCount>1</ItemCount>" & vbNewLine
	'TO DO
	'finish translastion off xls to xml
	strXml = strXML & "<SourceXml>" & readXLS(request("fileName")) & "</SourceXml>" & vbNewLine
	strXml = strXML & "<MallID>1</MallID>" & vbNewLine
	strXml = strXML & "<UserID>75</UserID>" & vbNewLine
	strXml = strXML & "<RecipientType>1</RecipientType>" & vbNewLine
	strXml = strXML & "<DateCreated>2015-08-21T08:27:43.55</DateCreated>" & vbNewLine
	strXml = strXML & "<IsRepository>true</IsRepository>" & vbNewLine
	strXml = strXML & "<CampaignID>13</CampaignID>" & vbNewLine
	strXml = strXML & "<StatusID>1</StatusID>" & vbNewLine
	strXml = strXML & "<RecipientListModelID>4</RecipientListModelID>" & vbNewLine
	strXml = strXML & "<Description>t</Description>" & vbNewLine
	
	strXml = strXML & "</recipientList>" & vbNewLine
	
	strXml = strXML & "</AddRecipientList>" & vbNewLine


	addRecipientListXML = GetUStoreXml(strXml)
	
end function

function readXLS(fileName)
	'TO DO
	'finish translastion off xls to xml
	'initialize variables
	Dim objConn, strSQL, objrs, dctNames, newXML
	Dim x
	
	set dctNames = Server.CreateObject("Scripting.Dictionary")
	Set objConn = Server.CreateObject("ADODB.Connection")
	response.write Server.MapPath("../" &fileName)
	objConn.Open "DRIVER={Microsoft Excel Driver (*.xls)}; READONLY=1; IMEX=1; HDR=NO; "&_
	 "Excel 8.0; DBQ=" & Server.MapPath("../" &fileName) & "; "
	strSQL = "SELECT * FROM A1:G1000"
	set objRS = objConn.Execute(strSQL)
	
	'save all columnNames
	For x=0 To objRS.Fields.Count-1
	  dctNames.Add x, objRS.Fields(x).Name
	Next
	newXML =  "<?xml version=""1.0"" encoding=""utf-16""?>"
	newXML = newXML & "<NewDataSet>"
	
	while not objRS.EOF
		newXML = newXML & "<RecipitentList>"
		For x=0 To objRS.Fields.Count-1
			newXML = newXML & "<" & dctNames(x) & ">" & objRS.Fields(x).Value & "</" & dctNames(x) & ">"
		next
		newXML = newXML & "</RecipitentList>"
		objRS.MoveNext
	wend
	objRS.Close
	
	newXML = newXML & "</NewDataSet>"
	newXML = replace(replace(replace(replace(newXML, """", "&quote"), "<", "&lt;"), ">", "&gt;"), "&", "&amp;")
	Response.Write newXML
	 
	Set objRS=Nothing

	readXLS = newXML
end function

function createRecipientList(strFolderName, intTicketID, intQuantity)
	'TO DO
	'finish translastion off xls to xml
	'initialize variables
	Dim objConn, strParamSQL, rsParamSQL, dctNames, dctValues, newXML, FSO, strTimestamp,myFile, subXML
	Dim x, y
	
	set dctNames = Server.CreateObject("Scripting.Dictionary")
	set dctValues = Server.CreateObject("Scripting.Dictionary")
	
	'strParamSQL = "select vchUProduceDialName from " & TABLE_PRODUCT_CUSTOM_DIALS & " where intDocID = " & uStoreID
	strParamSQL = "select D.vchUProduceDialName, SD.vchDialValue, D.blnIsVariable, D.intControlType from " & TABLE_PRODUCT_SAVED_DIALS & " SD left join " & TABLE_PRODUCT_TICKETS & " T on SD.intTicketTableID = T.intID left join " & TABLE_PRODUCT_CUSTOM_DIALS & " D on T.intUstoreID = D.intDocID and SD.vchDialName = D.vchUProduceDialName where SD.intTicketTableID = " & intTicketID
	OpenConn
	set rsParamSQL = gobjConn.execute(strParamSQL)
	
	x = 0
	y = 0
	while not rsParamSQL.EOF
		dctNames.Add x, rsParamSQL("vchUProduceDialName") & ""
		if rsParamSQL("intControlType")&"" = "18" then
			dctValues.Add x, "\\10.0.10.70\output\" & strFolderName & "\images\" & rsParamSQL("vchDialValue") & ""
		else
			dctValues.Add x, rsParamSQL("vchDialValue") & ""
		end if
		x = x + 1
		rsParamSQL.MoveNext
	wend
	
	newXML =  "<?xml version=""1.0"" encoding=""UTF-8""?><xml xmlns:s=""uuid:BDC6E3F0-6DA3-11d1-A2A3-00AA00C14882"" xmlns:dt=""uuid:C2F41010-65B3-11d1-A29F-00AA00C14882"" xmlns:rs=""urn:schemas-microsoft-com:rowset"" xmlns:z=""#RowsetSchema"">"
	newXML = newXML & "<s:Schema id=""RowsetSchema"">"
	newXML = newXML & "<s:ElementType name=""row"" content=""eltOnly"" rs:updatable=""true"">"
	'save all columnNames
	For x=0 To dctNames.Count-1
		newXML = newXML & "<s:AttributeType name=""" & replace(replace(dctNames(x), " ", "_x0020_"), "#", "_x0023_") & """ rs:name=""" & dctNames(x) & """ rs:number=""" & (x+1) & """ rs:baseCatalog=""test"" rs:baseTable=""mapping"" rs:keycolumn=""False"" rs:autoincrement=""False"">"
			newXML = newXML & "<s:datatype dt:type="""" dt:maxlength=""-1"" rs:maybenull=""True"" />"
        newXML = newXML & "</s:AttributeType>"
	Next
	
	newXML = newXML & "</s:ElementType></s:Schema><rs:data>"
	
	For x=0 to (intQuantity - 1)
		newXML = newXML & "<z:row "
		For y=0 To dctNames.Count-1
			newXML = newXML & replace(replace(dctNames(y), " ", "_x0020_"), "#", "_x0023_") & "=""" & dctValues(y) & """ "
		next
		newXML = newXML & " />"
	next
	
	newXML = newXML & "</rs:data></xml>"
	Response.Write newXML
	
	set subXML = Server.CreateObject("Msxml2.DOMDocument")
	subXML.LoadXml(newXML)
	strTimestamp = year(now) & right("0" & month(now),2) & right("0" & day(now),2) & "_" & right("0" & hour(now),2) & right("0" & minute(now),2) & right("0" & second(now),2)
	'subXML.Save "C:\test\a-" & strTimestamp & ".xml"
	subXML.Save "C:\output\" & strFolderName & "\" & intTicketID & "-" & strTimestamp & ".xml"
	

	'myFile.WriteLine(newXML)
	'myFile.Close
	'Set objRS=Nothing

	createRecipientList = intTicketID & "-" & strTimestamp & ".xml"
end function

sub CreatePrintFinal
	dim strSQL, strSQL2, rsTemp, rsTemp2, proofURL, ustoreID, proofTicketID, uProduceID, checkType, jobTicketID
	
	OpenConn
	
	strSQL = "select P.intUProduceID as intUProduceID, T.intID as intID, T.intProofTicketID as ProofID from " & TABLE_PRODUCT_TICKETS & " T left join " & TABLE_PRODUCT_TABLE & " P on P.intUStoreID = T.intUStoreID where T.intShopperID = " & Session(SESSION_PUB_USER_ID)
	strSQL = strSQL & " and T.chrStatus = 'W'"
	'response.write "1"
	set rsTemp =  gobjConn.execute(strSQL)
	
	while not rsTemp.EOF
		proofTicketID = rsTemp("intID")&""
		uProduceID = rsTemp("intUProduceID")&""
		jobTicketID = ProcessTicket(proofTicketID, uProduceID, "final", "")
		strSQL2 = strSQL2 & "Update " & TABLE_PRODUCT_TICKETS & " set chrStatus = 'P' where intID = " & proofTicketID & "; update " & STR_TABLE_LINEITEM_LONG_TERM & " set intUProduceTicket = " & jobTicketID & " where intInternalId = " & proofTicketID & "; "
		rsTemp.MoveNext
	wend
	if strSQL2 <> "" then
		gobjConn.execute(strSQL2)
	end if

end sub


Sub Pause(intSeconds)
dim startTime
startTime = Time()
Do Until DateDiff("s", startTime, Time(), 0, 0) > intSeconds
Loop
End Sub
%>