<!--#include file="incUstore.asp"-->
<%
dim strSQL, cmd, arParams, col
Response.ContentType = "application/json; charset=utf-8"

reqAction = request("action")

select case reqAction
	case "loadProductGroups"
		loadProductGroups
	case "loadGroupTemplates"
		loadGroupTemplates
	case "loadProductSteps"
		loadProductSteps
	case "getImage"
		getImage
	case "loadProductDialsByStep"
		loadProductDialsByStep
	case "loadDialOptions"
		loadDialOptions
	case "loadDialParams"
		loadDialParams
	case "checkForRecipientList"
		checkForRecipientList
	case "loadRecipientLists"
		loadRecipientLists
	case "checkForDials"
		checkForDials
	case "upload"
		upload
	case "CreateProof"
		CreateProof
	case "SaveDials"
		SaveDials
	case "checkCustom"
		checkCustom
	case "CreateFinal"
		CreateFinal
	case "loadAddressBook"
		loadAddressBook
	case "checkType"
		checkType
	case "loadExistingDialValues"
		loadExistingDialValues
	case "checkMinMax"
		checkMinMax
	case "validateMinMax"
		validateMinMax
	case "SavePODToCart"
		SavePODToCart
	case "getCart"
		getCart
	case "removeItem"
		removeItem
	case "updateItems"
		updateItems
	case "getProof"
		getProof
	case "testFinal"
		testFinal
	case "getCartLTI"
		getCartLTI
	case "testRI"
		testRI
	case "checkLogin"
		checkLogin
	case "sendMailTest"
		sendMailTest
	case "addLTItoCart"
		addLTItoCart
	case "validateLTICart"
		validateLTICart
	case "loadDistinctFilter"
		loadDistinctFilter
	case "getPODPrice"
		getPODPrice
	case "getTicketInfo"
		getTicketInfo
	case "loadStoreNumber"
		loadStoreNumber
	case "getInvID"
		getInvID
    case "checkAdmin"
        checkAdmin
    case else
        ' nothing
end select

Function QueryToJSON(dbcomm, params)
        Dim rs, jsa
        Set rs = dbcomm.Execute(,params,1)
        Set jsa = jsArray()
        Do While Not (rs.EOF Or rs.BOF)
                Set jsa(Null) = jsObject()
                For Each col In rs.Fields
                        jsa(Null)(col.Name) = col.Value
                Next
        rs.MoveNext
        Loop
        Set QueryToJSON = jsa
        rs.Close
End Function

sub SQLExampleASPJson
	strConn = "Provider=SQLNCLI10;Server=localhost;Database=NorthWind;Trusted_Connection=Yes;"
	Set conn = Server.CreateObject("ADODB.Connection")
	conn.Open strConn
	query = "SELECT * FROM Customers WHERE CustomerID = ?"
	CustomerID = Request.QueryString("CustomerID")
	arParams = array(CustomerID)
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = query
	Set cmd.ActiveConnection = conn
	QueryToJSON(cmd, arParams).Flush
end sub

sub loadProductGroups

	strSQL = "Select intID, vchName, txtDescription from " & TABLE_PRODUCT_GROUP & "  where chrStatus = 'A' order by intDisplayOrder"
	
	'response.write strSQL
	
	OpenConn
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()

end sub

sub loadGroupTemplates
	dim groupNum
	Response.Buffer = False
	groupNum = request("groupNum")
	
	strSQL = "select vchGroups from " & STR_TABLE_SHOPPER & " WHERE intID=" & gintUserID
	OpenConn
	dim rsUser, strSplitGroups, x, i
    set rsUser = gobjConn.execute(strSQL)
	
	strSQL = "Select P.intUStoreID, P.vchName, P.boolShowShortDesc, P.vchStaticDocLocation, P.txtShortDescription, P.txtDescription, P.txtImage64 from " & TABLE_PRODUCT_TABLE & " P left join " & STR_TABLE_INVENTORY_LONG_TERM & " I on I.vchPartNumber = '" & left(STR_TABLE_INVENTORY_LONG_TERM, 3) &"_POD_' + cast(P.intUstoreID as varchar(50)) where P.chrStatus = 'A' and P.intProductGroupID = " & groupNum
	if not rsUser.eof then
		strSplitGroups = split(rsUser("vchGroups")&"",",")
		if rsUser("vchGroups")&"" <> "" then
		strSQL = strSQL & " And ("
			for each x in strSplitGroups
				if i > 0 then
					strSQL = strSQL & " OR "
				end if
				strSQL = strSQL & " I.vchGroupingIDs LIKE '%, " & trim(x) & ",%' " '--middle
				strSQL = strSQL & " OR "
				strSQL = strSQL & " I.vchGroupingIDs LIKE '" & trim(x) & ",%'" ' --start
				strSQL = strSQL & " OR "
				strSQL = strSQL & " I.vchGroupingIDs LIKE '%, " & trim(x) & "' " '--end
				strSQL = strSQL & " OR "
				strSQL = strSQL & " I.vchGroupingIDs =  '" & trim(x) & "'  "
				i = i + 1
			next
			strSQL = strSQL & " OR I.vchGroupingIDs is null ) "
		else
			strSQL = strSQL & " And I.vchGroupingIDs is null  "
		end if
	else
		strSQL = strSQL & " And I.vchGroupingIDs is null  "
	end if

	strSQL = strSQL & " Order by P.vchName"
	
	'response.write strSQL
	'response.end
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()

end sub

sub loadProductSteps
	dim productNum, rsTemp, i
	
	productNum = request("productNum")
	
	strSQL = "Select intUStoreID, intStepID, vchName, txtDescription from " & TABLE_PRODUCT_CUSTOM_STEPS & "  where intUstoreID = " & productNum & " order by intDisplayOrder"
	
	OpenConn
	
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()

end sub

sub loadProductDialsByStep
	dim stepNum
	
	stepNum = request("stepNum")
	
	strSQL = "Select * from " & TABLE_PRODUCT_CUSTOM_DIALS  & "  where intStepID = " & stepNum & " order by intDisplayOrder"
	
	OpenConn
	
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()

end sub

sub loadDialOptions
	dim dialID
	
	dialID = request("dialID")
	
	strSQL = "Select vchText, vchValue from " & TABLE_PRODUCT_CUSTOM_DROPDOWNS & "  where intDialID = " & dialID  & " order by intDisplayOrder, intOptionID"
	
	OpenConn
	
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()

end sub

sub loadDialParams
	dim dialID
	
	dialID = request("dialID")
	
	strSQL = "Select vchParamType, vchParamValue from " & TABLE_PRODUCT_CUSTOM_PARAMS  & "  where intDialID = " & dialID
	
	OpenConn
	
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()

end sub

sub checkForRecipientList
	dim productID
	
	productID = request("productID")
	
	strSQL = "Select boolRecipientList from " & TABLE_PRODUCT_TABLE  & "  where intUStoreID = " & productID
	
	OpenConn
	
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()

end sub

sub checkForDials
	dim productID
	
	productID = request("productID")
	
	strSQL = "Select D.intDialID from " & TABLE_PRODUCT_CUSTOM_STEPS  & " S left join " & TABLE_PRODUCT_CUSTOM_DIALS & " D on D.intStepID = S.intStepID where intUStoreID = " & productID
	
	OpenConn
	
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()

end sub

sub checkType
	dim productID
	
	productID = request("productID")
	
	strSQL = "Select txtKeywords from " & TABLE_PRODUCT_TABLE  & " where intUStoreID = " & productID
	
	OpenConn
	
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()

end sub

sub upload
	dim objUpload, rsRequestData, strFolderName
	
	
	Set objUpload = Server.CreateObject("SoftArtisans.FileUp")
	Set rsRequestData = objUpload.Form
	
	strFolderName = split(TABLE_PRODUCT_TICKETS , "_")
	
	dim fImage, uploadPath, IsImageGood, fImageOrient
	uploadPath = "C:\output\" & strFolderName(0) & "\images\"
	objUpload.Form("uploadItem").SaveAs uploadPath & objUpload.form("uploadItem").UserFilename
	

end sub

sub SaveDials
	dim rsRequestData, uStoreID, uProduce, rsTemp, rsTemp2, ticketTableID,strSQLUpdate, item, key, value
	
	Set rsRequestData = Request.Form
	
	ustoreID = request("ustoreID")
	ticketTableID = request("ticketID")&""
	
	strSQLUpdate = ""
	
	OpenConn
	strSQL = "select intUProduceID from " & TABLE_PRODUCT_TABLE & " where intUStoreID = " & ustoreID
	
	set rsTemp = gobjConn.execute(strSQL)
	
	uProduce = rsTemp("intUProduceID")
	if ticketTableID = "" then
		strSQL = "select * from " & TABLE_PRODUCT_TICKETS & " where intShopperID = " & Session(SESSION_PUB_USER_ID) & " and intUStoreID = " & ustoreID & " and chrStatus = '0'"
		'response.write strSQL
		set rsTemp = gobjConn.execute(strSQL)
		
		if rsTemp.EOF then
			strSQL = "insert into " & TABLE_PRODUCT_TICKETS & " (dtmCreated, dtmUpdated, intShopperID, intUStoreID, intUProduceID, chrStatus) values ('" & Now() & "','" & Now() & "', " & Session(SESSION_PUB_USER_ID) & ", " & ustoreID & ", " & uProduce & ", '0')"
			'response.write strSQL
			gobjConn.execute(strSQL)
			
			strSQL = "select intID from " & TABLE_PRODUCT_TICKETS & " where intShopperID = " & Session(SESSION_PUB_USER_ID) & " and intUStoreID = " & ustoreID & " and chrStatus = '0'"
			set rsTemp2 = gobjConn.execute(strSQL)
			'response.write strSQL
			ticketTableID = rsTemp2("intID")
		else
			ticketTableID = rsTemp("intID")
		end if
	end if
	
	strSQL = "select * from " & TABLE_PRODUCT_SAVED_DIALS & " where intTicketTableID = " & ticketTableID
	'response.write strSQL
	set rsTemp = gobjConn.execute(strSQL)
	
	if rsTemp.EOF then
		strSQL = "insert into " & TABLE_PRODUCT_SAVED_DIALS & " (intTicketTableID, vchDialName, vchDialValue) values "
		
		for each key in request.Form
			'response.write key &":"& request.Form(key)
			if key <> "action" and key <> "ustoreID" and key <> "ticketID" then
				strSQL =  strSQL & "(" & ticketTableID &", '" & key &"', '" & SQLEncode(request(key)) & "')," 
			end if
		Next
		'response.end
		strSQL = left(strSQL,len(strSQL)-1)
		'response.write strSQL
		gobjConn.execute(strSQL)
	else
		while not rsTemp.EOF
			strSQLUpdate = strSQLUpdate & " update " & TABLE_PRODUCT_SAVED_DIALS  & " set vchDialValue = '" & SQLEncode(Request(rsTemp("vchDialName"))) & "' where intID = " & rsTemp("intID") & ";"
			rsTemp.MoveNext
		wend
		'response.write strSQLUpdate
		gobjConn.execute(strSQLUpdate)
	end if
		
	CloseConn()

end sub

sub CreateProof
	dim strSQL, rsTemp, proofURL, ustoreID, proofTicketID, ticketID
	
	ustoreID = request("ustoreID")
	ticketID = request("ticketID")&""
	Session(SESSION_PUB_USER_ID)
	OpenConn
	strSQL = "select P.intUProduceID as intUProduceID, T.intID as intID, T.intProofTicketID as ProofID from " & TABLE_PRODUCT_TICKETS & " T left join " & TABLE_PRODUCT_TABLE & " P on P.intUStoreID = T.intUStoreID where T.intShopperID = " & Session(SESSION_PUB_USER_ID) & "  and T.intUStoreID = " & ustoreID
	if ticketID <> "" then 
		strSQL = strSQL & " and t.intID = " & ticketID
	else
		strSQl = strSQL & " and T.chrStatus = '0'"
	end if
	set rsTemp =  gobjConn.execute(strSQL)
	'response.write strSQL
	proofURL = ProcessTicket(rsTemp("intID")&"", rsTemp("intUProduceID")&"", "proof", "")
	CloseConn
	
	'response.write "here"
	response.write "[{""proofURL"": """ & proofURL & """}]"
	response.flush
	response.end

end sub

sub CreateFinal
	dim strSQL, rsTemp, rsTemp2, proofURL, ustoreID, proofTicketID, uProduceID, checkType, jobTicketID
	
	ustoreID = request("ustoreID")
	Session(SESSION_PUB_USER_ID)
	OpenConn
	
	strSQL = "select txtKeywords from " & TABLE_PRODUCT_TABLE & " where intUStoreID = " & ustoreID
	set rsTemp = gobjConn.execute(strSQL)
	checkType = rsTemp("txtKeywords") &""
	
	strSQL = "select P.intUProduceID as intUProduceID, T.intID as intID, T.intProofTicketID as ProofID from " & TABLE_PRODUCT_TICKETS & " T left join " & TABLE_PRODUCT_TABLE & " P on P.intUStoreID = T.intUStoreID where T.intShopperID = " & Session(SESSION_PUB_USER_ID) & "  and T.intUStoreID = " & ustoreID
	if instr(checkType,"Download") > 0 then
		strSQL = strSQL & " and T.chrStatus = '0'"
	else
		strSQL = strSQL & " and T.chrStatus = 'W'"
	end if
	'response.write strSQL
	set rsTemp =  gobjConn.execute(strSQL)
	proofTicketID = rsTemp("intID")&""
	uProduceID = rsTemp("intUProduceID")&""
	if instr(checkType,"Download") > 0 then
		jobTicketID = ProcessTicket(proofTicketID, uProduceID, "final", "")
	else
		while not rsTemp.EOF
			proofTicketID = rsTemp("intID")&""
			jobTicketID = ProcessTicket(proofTicketID, uProduceID, "final", "")
			strSQL = "Update " & TABLE_PRODUCT_TICKETS & " set chrStatus = 'P' where intID = " & proofTicketID & "; update " & STR_TABLE_LINEITEM_LONG_TERM & " set intUProduceTicket = " & jobTicketID & " where intInternalId = " & proofTicketID
			gobjConn.execute(strSQL)
			rsTemp.MoveNext
		wend
	end if
	OpenConn
	if instr(checkType,"Download") > 0 then
		strSQL = "Update " & TABLE_PRODUCT_TICKETS & " set chrStatus = 'L' where intID = " & proofTicketID
		gobjConn.execute(strSQL)
		dim currentStatus
		currentStatus = "0"
		do while currentStatus <> "3"
			currentStatus = getUProduceStatus(jobTicketID)
		loop
		proofURL = getDownloadURL(jobTicketID)
		CloseConn
		response.write "[{""proofURL"": """ & proofURL & """}]"
		response.flush
		response.end
	end if
	
	'response.write "here"
	
	CloseConn
	response.write "[{""proofURL"": """ & proofURL & """}]"
	response.flush
	response.end

end sub

sub testFinal

	dim jobTicketID
	OpenConn
	jobTicketID = ProcessTicket(60, 7582, "proof", "")
	CloseConn

end sub

sub checkCustom
	dim strSQL, rsTemp, proofURL, ustoreID, proofTicketID
	
	ustoreID = request("ustoreID")
	
	OpenConn
	checkCustomizations("363776")
	CloseConn
	
	'response.write "here"
	response.write "[{""proofURL"": """ & proofURL & """}]"
	response.flush
	response.end

end sub

sub getInvID
	dim productID
	
	productID = request("productID")
	
	strSQL = "Select intID from " & STR_TABLE_INVENTORY_LONG_TERM   & " where vchPartNumber = '" & left(STR_TABLE_INVENTORY_LONG_TERM, 3) &"_POD_" & productID & "'"
	
	OpenConn
	
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()

end sub

sub loadStoreNumber
	dim strSQL, rsTemp, proofURL, ustoreID, proofTicketID, vchState, vchCity, strStoreSearch
	
	vchState = request("State")
	vchCity = request("City")
	strStoreSearch = request("Search")
	
	OpenConn
	strSQL = "SELECT L.intID, L.vchLabel FROM " & STR_TABLE_SHOPPER & " C INNER JOIN " & STR_TABLE_SHOPPER & " L ON L.intShopperID = C.intID  WHERE C.chrStatus='A' AND L.chrType='S'"
	strSQL = strSQL & " AND (C.vchFirstName LIKE '%" & strStoreSearch & "%' OR C.vchLastName LIKE '%" & strStoreSearch & "%' OR L.vchAddress1 LIKE '%" & strStoreSearch & "%' OR L.vchState LIKE '%" & strStoreSearch & "%' OR L.vchCity LIKE '%" & strStoreSearch & "%' OR L.vchZip LIKE '%" & strStoreSearch & "%')"
    ' response.write strsql
    ' response.end
    
	if vchState <> "" then
		strSQL = strSQL & " AND L.vchState = '" & vchState & "'"
	end if
	
	if vchCity <> "" then
		strSQL = strSQL & " AND L.vchCity = '" & vchCity & "'"
	end if

	strSQL = strSQL & " ORDER BY L.intid"
	
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()

end sub


sub loadDistinctFilter
	dim strSQL, rsTemp, vchFilter
	
	vchFilter = request("vchFilter")
	OpenConn
    strSQL = "SELECT distinct(" & vchFilter & ") FROM " & STR_TABLE_SHOPPER & " WHERE chrStatus='A' AND " & vchFilter & " is not NULL"
	'response.write strSQL
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()


end sub

sub loadAddressBook
	dim strSQL, rsTemp, proofURL, ustoreID, proofTicketID, strStoreSearch, intStoreID, intArea, intRegion, intDistrict, intOther, vchState, vchCity
	
	ustoreID = request("ustoreID")
	strStoreSearch = request("store_search")
	
	intStoreID = Request("storenumber")
	if intStoreID<>"" then
		intStoreID = intStoreID
	else
		intStoreID = 0
	end if
	
	intArea = Request("area")
	if IsNumeric(intArea) and intArea<>"" then
		intArea = clng(intArea)
	else
		intArea = 0
	end if
	
	intRegion = Request("region")
	if IsNumeric(intRegion) and intRegion<>"" then
		intRegion = clng(intRegion)
	else
		intRegion = 0
	end if
	
	intDistrict = Request("district")
	if IsNumeric(intDistrict) and intDistrict<>"" then
		intDistrict = clng(intDistrict)
	else
		intDistrict = 0
	end if

	intOther = Request("other")
	if IsNumeric(intOther) and intOther<>"" then
		intOther = clng(intOther)
	else
		intOther = 0
	end if
	
	vchState = Request("state")
	if vchState<>"" then
		vchState = vchState
	else
		vchState = ""
	end if
	
	vchCity = Request("city")
	if vchCity<>"" then
		vchCity = vchCity
	else
		vchCity = ""
	end if
	
	OpenConn
	strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE chrStatus='A' and chrType='S' "
	if Session(SESSION_MERCHANT_ULOGIN) = "" then
		strSQL = strSQL & " and intShopperID = " & Session(SESSION_PUB_USER_ID)
	end if
    strSQL = strSQL & " AND (vchAddress1 LIKE '%" & strStoreSearch & "%' OR vchState LIKE '%" & strStoreSearch & "%' OR vchCity LIKE '%" & strStoreSearch & "%' OR vchZip LIKE '%" & strStoreSearch & "%')"

    if intStoreID <> "" and intStoreID <> "0" then
        strSQL = strSQL & " AND intID in (" & intStoreID & ")"
    end if

    if vchState <> "" then
		strSQL = strSQL & " AND vchState = '" & vchState & "'"
    end if

	if vchCity <> "" then
		strSQL = strSQL & " AND vchCity = '" & vchCity & "'"
    end if

	strSQL = strSQL & " ORDER BY intid"
    'response.write strSQL
	
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()

end sub

sub loadExistingDialValues

	dim productNo, ticketID, strSQL, cmd, arParams
	
	productNo = request("productID")
	ticketID = request("ticketID")&""
	
	strSQL = "select D.intDialID, V.vchDialValue, D.intControlType from " & TABLE_PRODUCT_TICKETS  & " T left join " & TABLE_PRODUCT_SAVED_DIALS & " V on T.intID = V.intTicketTableID left join " & TABLE_PRODUCT_CUSTOM_DIALS  & " D on D.vchName = V.vchDialName where D.intDocID = " & productNo & " and T.intUStoreID = " & productNo & " and T.intShopperID = " & Session(SESSION_PUB_USER_ID)
	if ticketID <> "" then
		strSQL = strSQL & " and T.intID = " & ticketID
	else
		strSQL = strSQL & " and T.chrStatus = '0' "
	end if
	'response.write strSQL
	OpenConn
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()

end sub

sub getCart

	dim productNo, strSQL, cmd, arParams, OrderID
	
	OrderID = request("OrderID")
	
	strSQL = "select T.intUStoreID, T.intID as intTicketID, T.intProofJobTicketID, P.vchName, L.intQuantity, I.vchBundleQuantity, (L.intQuantity * I.vchBundleQuantity) as intTrueQuantity, (I.mnyItemPrice * L.intQuantity) AS mnyPrice, I.intMinQty, I.intMaxQty, L.intOrderID, L.intID, P.txtImage64 from " & TABLE_PRODUCT_TICKETS  & " T left join " & TABLE_PRODUCT_TABLE & " P on T.intUStoreID = P.intUStoreID left join " & STR_TABLE_LINEITEM_LONG_TERM & " L on T.intID = L.intInternalID JOIN " & STR_TABLE_ORDER_LONG_TERM & " O on O.intID=L.intOrderID JOIN " & STR_TABLE_INVENTORY_LONG_TERM & " I on L.intInvId=I.intID where T.intShopperID = " & Session(SESSION_PUB_USER_ID)
	if OrderID <> "" then 
		strSQL = strSQL & " and T.chrStatus = 'P' and O.intID = " & OrderID
	else
		strSQL = strSQL & " and T.chrStatus = 'W' " 
	end if 
	strSQL = strSQL & " and L.chrStatus = 'A' " 
	strSQL = strSQL & " ORDER BY L.intID"
	'response.write strSQL
	OpenConn
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()

end sub

sub getCartLTI

	dim strSQL,cmd,arParams, OrderID
	
	OrderID = request("OrderID")

    strSQL = strSQL & "SELECT S.vchLabel AS [distributor], O.intID AS [order], L.intID, L.intOrderID, L.intInvID, L.vchItemName, L.intQuantity, (I.mnyItemPrice * L.intQuantity) AS mnyPrice, L.intQuantity * I.vchBundleQuantity AS [intTrueQuantity], I.vchImageURL, I.vchBundleQuantity, I.intMinQty, I.intMaxQty, P.txtImage64, P.vchName, T.intUStoreID, T.intID AS intTicketID, T.intProofJobTicketID "
    strSQL = strSQL & "FROM " & STR_TABLE_ORDER_LONG_TERM & " AS O "
    strSQL = strSQL & "INNER JOIN " & STR_TABLE_LINEITEM_LONG_TERM & " AS L ON O.intID = L.intOrderID AND O.chrStatus = '0' AND L.chrStatus <> 'D' AND O.intShopperID = " & Session(SESSION_PUB_USER_ID) & " "
    strSQL = strSQL & "INNER JOIN " & STR_TABLE_INVENTORY_LONG_TERM & " AS I ON I.intID = L.intInvId "
    strSQL = strSQL & "INNER JOIN " & STR_TABLE_SHOPPER & " AS S ON O.intShipShopperID = S.intID "
    strSQL = strSQL & "LEFT JOIN " & TABLE_PRODUCT_TICKETS & " AS T ON T.intID = L.intInternalID "
    strSQL = strSQL & "LEFT JOIN " & TABLE_PRODUCT_TABLE & " AS P ON P.intUStoreID = T.intUStoreID "
    if orderid <> "" then
		strSql = replace(strsql, "'0'", "'P'")
		strSQL = strSQL & "WHERE o.intid = " & orderid & " "
        strsql = replace(strsql, "AND O.intShopperID = 227", " ")
	end if 
    strSQL = strSQL & "ORDER BY s.intid "
	' response.write strsql : response.end
	OpenConn
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()

end sub

' to remove a POD that exists for multiple distributors
' 1 - check to see if a POD exists in more than one lineitem
sub removeItem
    OpenConn
	dim ticketID, lineID, strSQL, cmd, arParams, order, rsremove
	
	ticketID = request("ticketID")
	lineID = request("lineID")
    order = request("order")
	    
    if ticketID <> "" then
        
        ' get the amount of orders that have the same POD
        strSQL = "SELECT COUNT(intID) AS [amt] FROM " & STR_TABLE_LINEITEM_LONG_TERM & " WHERE chrStatus = 'A' AND intInternalID = " & ticketID 
        set rsremove = gobjConn.execute(strSQL)
        strSQL = ""
        
        if not rsremove.eof then
            if rsremove("amt") = 1 then
                strSQL = "Update " & TABLE_PRODUCT_TICKETS  & " set chrStatus = 'D' where chrStatus = 'W' and intID = " & ticketID & " and intShopperID = " & Session(SESSION_PUB_USER_ID) & ";"
            end if
        end if
    
        strSQL = strSQL & "UPDATE " & STR_TABLE_LINEITEM_LONG_TERM & " SET chrStatus = 'D' WHERE intInternalID = " & ticketID & " AND intOrderID = " & order & ";"
	else
		strSQL = strSQL & " Update " & STR_TABLE_LINEITEM_LONG_TERM   & " set chrStatus = 'D' where intID = " & lineID
	end if
    
    gobjConn.execute(strSQL)
	CloseConn

end sub

sub getProof
	dim strSQL, rsTemp, proofURL, ustoreID, proofTicketID
	
	proofTicketID = request("intProofTicketID")
	
	'response.write "here"
	response.write "[{""proofURL"": """ & getDownloadURL(proofTicketID) & """}]"
	response.flush
	response.end

end sub

sub SavePODToCart

	dim productNo, strSQL, cmd, arParams, orderID, intItemInvID, shipID, quantity, rsTemp
	
	productNo = request("productID")
	shipID = request("shipID")
	quantity = request("Copies")
	OpenConn
	strSql = "select intID from " & STR_TABLE_ORDER_LONG_TERM & " where intShopperID = "  & Session(SESSION_PUB_USER_ID) & " and intShipShopperID = " & shipID & " and chrStatus= '0'"
	' response.write strSql
	set rsTemp = gobjConn.execute(strSQL)
	
	if rsTemp.EOF then
		CreateNewOrderLt
		orderID = gintOrderID
		'response.write orderID
		strSQL = "Update " & STR_TABLE_ORDER_LONG_TERM  & " set intShipShopperID = " & shipID & ", intBillShopperID = " & shipID & " where intID = " & orderID
		'response.write strSql
		gobjConn.execute(strSQL)
	else
		orderID = rsTemp("intID")
		'response.write orderID
	end if
	
	strSQL = "select intID from " & STR_TABLE_INVENTORY_LONG_TERM & " where vchPartNumber like '%POD_" & productNo & "%'"
	' response.write strSql
	
	set rsTemp = gobjConn.execute(strSQL)
	
	intItemInvID =  rsTemp("intID")
	
	AddItemToOrder_OtherLt orderID, intItemInvID, quantity
	
	strSQL = "update " & TABLE_PRODUCT_TICKETS & " set chrStatus = 'W' where intUStoreID = " & productNo & " and intShopperID = " & Session(SESSION_PUB_USER_ID) & " and chrStatus = '0'"
	gobjConn.execute(strSQL)
	
	strSQL = "select top 1 intID from " & TABLE_PRODUCT_TICKETS & " where chrStatus = 'W' and  intUStoreID = " & productNo & " and intShopperID = " & Session(SESSION_PUB_USER_ID) & " order by dtmUpdated desc"
	set rsTemp = gobjConn.execute(strSQL)
	
	strSQL = "update " & STR_TABLE_LINEITEM_LONG_TERM  & " set intInternalID = " & rsTemp("intID") & " where vchPartNumber = '" & left(STR_TABLE_LINEITEM_LONG_TERM, 3) & "_POD_" & productNo & "' and intInternalID is Null"
	gobjConn.execute(strSQL)
	'response.write intItemInvID
	CloseConn
	'response.write strSQL

end sub

sub addLTItoCart
    
    dim dctOrders, dctShoppers, dctItemQTY, key, key2, debugString, intItemInvID, rsTemp, Item, intShipShopperID, intOrderId, fieldName, fieldValue
    set dctOrders = Server.CreateObject("Scripting.Dictionary")
    set dctShoppers = Server.CreateObject("Scripting.Dictionary")
    set dctItemQTY = Server.CreateObject("Scripting.Dictionary")
		OpenConn
		
		strSQL = "SELECT intBillShopperID, intID FROM " & STR_TABLE_ORDER_LONG_TERM & " WHERE chrStatus='0' and intShopperID = " & Session(SESSION_PUB_USER_ID)
		set rsTemp = gobjConn.execute(strSQL)
		while not rsTemp.EOF
			if not dctOrders.Exists(rsTemp(0)&"") Then dctOrders.Add rsTemp(0)&"",rsTemp(1)&""
			rsTemp.MoveNext
		wend
		
		for each Item in Request.Form
			
			fieldName = Item
			fieldValue = Request.Form(fieldName)

            ' response.write "Item: " & Item & " Item Value: " & fieldValue & " | "

			if Right(fieldName,4) = "_QTY" then
				if fieldValue&"" <> "" and IsNumeric(fieldValue) then 
					dctItemQTY.Add fieldName, fieldValue
				end if
			elseif Left(fieldName,6) = "Store_" then
				if fieldValue = "true" then
					dctShoppers.Add fieldName, fieldValue
				end if
			end if
            
		Next
		
		Set rsTemp = Nothing
		
		if dctShoppers.Count = 0 then
			strSQL = "select intID from " & STR_TABLE_SHOPPER & " where intShopperID = " & Session(SESSION_PUB_USER_ID)
			set rsTemp = gobjConn.execute(strSQL)
			'response.write strSQL
			dctShoppers.add rsTemp("intID"), "true"
		end if
		
		for each key in dctShoppers.Keys
			
            intShipShopperID = CLNG(Replace(key,"Store_",""))
			
			If dctOrders.Exists(intShipShopperID&"") Then
				intOrderId = dctOrders(intShipShopperID&"")
			Else
				CreateNewOrderLt
				intOrderId = gintOrderID
			End If
					
            strSQL = "UPDATE " & STR_TABLE_ORDER_LONG_TERM & " SET intBillShopperId=" & intShipShopperID & ", intShipShopperId=" & intShipShopperID & " WHERE intID=" & intOrderId
                       
			call gobjConn.execute(strSQL)
			
			for each key2 in dctItemQTY.Keys
                intItemInvID = CLNG(Replace(key2,"_QTY",""))
				AddItemToOrder_OtherLt intOrderId, intItemInvID, dctItemQTY(key2)  
			next
			
		next
		
		CloseConn
        
end sub

sub validateLTICart

	dim strInvSQL, rsInvData, strQtySQL, rsQtySQL, dctTotalOfItems, dctErrors, dctOrderItemError, output, x, a, b, i
	set dctTotalOfItems = CreateObject("Scripting.Dictionary")
	set dctErrors = CreateObject("Scripting.Dictionary")
	set dctOrderItemError = CreateObject("Scripting.Dictionary")
    
	OpenConn
    
	strInvSQL = "SELECT O.intID as OrderID, L.IntId as Item_ID, L.intInvID as Inv_ID, L.vchItemName as Item_Name, L.intQuantity as Quantity FROM " & STR_TABLE_ORDER_LONG_TERM & " O"
	strInvSQL = strInvSQL & " INNER JOIN " & STR_TABLE_LINEITEM_LONG_TERM & " AS L ON O.intID=L.intOrderID AND (L.chrStatus='A' or L.chrStatus='C') "
	strInvSQL = strInvSQL & " INNER JOIN " & STR_TABLE_INVENTORY_LONG_TERM & " AS I ON I.intID=L.intInvID AND I.chrStatus='A' "
	strInvSQL = strInvSQL & " WHERE O.intShopperId=" & Session(SESSION_PUB_USER_ID) & " AND O.chrStatus in ('0','9') AND O.intBillShopperID is not null"

	set rsInvData = gobjConn.execute(strInvSQL)

	i = 0
	while not rsInvData.EOF 
    
		strQtySQL = "Select * From " & STR_TABLE_INVENTORY_LONG_TERM & " where intID = " & rsInvData("Inv_ID")
		set rsQtySQL = gobjConn.execute(strQtySQL)
		
		if not rsQtySQL.EOF then
			
			if dctTotalOfItems.Exists(rsInvData("Item_Name")&"") = true then 
				dctTotalOfItems.Item(rsInvData("Item_Name")&"") = CLNG(dctTotalOfItems.Item(rsInvData("Item_Name")&"")) + CLNG(rsInvData("Quantity"))
			else 
				dctTotalOfItems.Add rsInvData("Item_Name")&"", CLNG(rsInvData("Quantity"))
			end if
			
			if rsQtySQL("intMinQty") > rsInvData("Quantity") then
				dctErrors.Add rsInvData("Item_ID") & "", "You must order at least " & rsQtySQL("intMinQty") &  " of " & replace(rsInvData("Item_Name"), """", "\""") & "."
				dctOrderItemError.Add i, rsInvData("OrderID") & " " & rsInvData("Inv_ID")
			elseif rsQtySQL("intMaxQty") < rsInvData("Quantity") then
				dctErrors.Add rsInvData("Item_ID") & "", "You cannot order more than " & rsQtySQL("intMaxQty") &  " of " & replace(rsInvData("Item_Name"), """", "\""") & "."
				dctOrderItemError.Add i, rsInvData("OrderID") & " " & rsInvData("Inv_ID")
			end if
			
			if CLNG(dctTotalOfItems.Item(rsInvData("Item_Name")&"")) > rsQtySQL("intStock") and instr(rsQtySQL("vchPartNumber"), "_POD_") = 0 then
				if not dctErrors.Exists(rsInvData("Item_ID")&"") then 
					dctErrors.Add rsInvData("Item_ID")&"", "There is/are only " & rsQtySQL("intStock") &  " left of " & replace(rsInvData("Item_Name"), """", "\""") & " in stock." 
				end if 
			end if
			
			rsQtySQL.MoveNext
			'response.write "<div style=""display:none"">" & rsInvData("Item_Name") & " " & dctTotalOfItems.Item(rsInvData("Item_Name")&"") & "</div>"
		end if
		
		rsQtySQL.close
		set rsQtySQL = nothing
		
		i = i + 1
		rsInvData.MoveNext
	wend
	
	
	rsInvData.close
	set rsInvData = nothing
	
	'Debug leftovers
	'response.write "errors: "
	'a = dctErrors.Items
	'for x = 0 to dctErrors.count - 1
	'	response.write a(x) & "<br/>"
	'next
	
	'response.write "errors: "
	'a = dctOrderItemError.Items
	'for x = 0 to dctOrderItemError.count - 1
	'	response.write a(x) & "<br/>"
	'next
	if dctErrors.count > 0 then
		'dim a, b, x, output
		a = dctErrors.Items
		b = dctErrors.Keys
		output = "["
		for x = 0 to dctErrors.count - 1
			output = output & "{""lineItemID"": """ & b(x) & """, ""errMessage"": """ & a(x) & """}"
			if x <> dctErrors.count - 1 then
				output = output & ","
			end if
		next
		output = output & "]"
		response.write output
	end if

	
end sub

sub validateMinMax

	dim productID, amount, strSQL, rsTemp
 
	productID = request("productID")
	amount = request("amount")
	OpenConn
	strSQL = "select intMinQty, intMaxQty, vchBundleQuantity from " & STR_TABLE_INVENTORY_LONG_TERM  & " where vchPartNumber = '" & left(STR_TABLE_INVENTORY_LONG_TERM, 3) & "_POD_" & productID & "'"
	'response.write strSQL
	set rsTemp = gobjConn.execute(strSQL)
	'response.write amount
	'response.write rsTemp("intMinQty")&" " & rsTemp("intMaxQty")
	if rsTemp("intMinQty")&"" <> rsTemp("intMaxQty")&"" then
		if (clng(amount) < clng(rsTemp("intMinQty"))) then
			response.write "[{""minLimit"": """ & (cint(rsTemp("intMinQty")) * cint(rsTemp("vchBundleQuantity"))) & """}]"
		elseif (clng(amount) > clng(rsTemp("intMaxQty"))) then
			response.write "[{""maxLimit"": """ & (cint(rsTemp("intMaxQty")) * cint(rsTemp("vchBundleQuantity"))) & """}]"
		else
			response.write "[{}]"
		end if
	else 
		response.write "[{}]"
	end if
	CloseConn
end sub

sub checkMinMax

	dim productID, amount, strSQL, rsTemp
 
	productID = request("productID")
	strSQL = "select intMinQty, intMaxQty, vchBundleQuantity, mnyItemPrice from " & STR_TABLE_INVENTORY_LONG_TERM  & " where vchPartNumber = '" & left(STR_TABLE_INVENTORY_LONG_TERM, 3) & "_POD_" & productID & "'"
	' response.write strSQL
	OpenConn
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()
end sub

sub updateItems

	dim i, intQty, intItemID, intOrderID
	OpenConn
	for each i in request.Form
		if lcase(left(i, 7)) = "intqty_" then
        'Response.Write mid(i,8)
			intOrderID = Split(mid(i,8),"_")(0)
            intItemID = Split(mid(i,8),"_")(1)
			if IsNumeric(intItemID) then
				intItemID = CLng(intItemID)
				intQty = request(i)
				if IsNumeric(intQty) then
					intQty = CLng(intQty)
				else
					intQty = -1
				end if
				if intQty = 0 then
					' remove item from cart
					'response.write "order id: " & intOrderID & " itemid: " & intItemID
					'previously: RemoveLineFromOrderLT_Other intOrderID, intItemID
					RemoveLineFromOrderLT_Other intItemID
				elseif intQty > 0 then
					' change qty in cart
					EditOrderItemQtyLT_Other intOrderID,intItemID, intQty
				end if
			end if
		end if
	next
	CloseConn

end sub

sub getTicketInfo

	dim ticketID, amount, strSQL, rsTemp
 
	ticketID = request("ticketID")
	strSQL = "select T.intUStoreID, P.vchName as ProductName, G.vchName as GroupName, G.intID as GroupNum, P.txtImage64 from " & TABLE_PRODUCT_TICKETS & " T join " & TABLE_PRODUCT_TABLE & " P on T.intUstoreID = P.intUStoreID join " & TABLE_PRODUCT_GROUP & " G on P.intProductGroupID = G.intID where T.intID = " & ticketID
	'response.write strSQL
	OpenConn
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()

end sub

sub testRI

	dim thing

	thing = createRecipientList("test", 56, 45, 50)

end sub

sub checkLogin

	If gintUserName&"" = ""  Then
		response.write "[{""login"": ""false""}]"
	else
		response.write "[{""login"": ""true""}]"
	end if

end sub

sub MailerDetails

	dim recno, strSql, cmd, arParams
	
	recno = request("recno")
	
	strSQL = "select [full name] from " & mainTable & "  where [recno] = " & recno
	
	OpenConn
	Set cmd = Server.CreateObject("ADODB.Command")
	cmd.CommandText = strSQL
	arParams = array("")
	Set cmd.ActiveConnection = gobjConn
	QueryToJSON(cmd, arParams).Flush
	CloseConn()

end sub

sub sendMailTest

	SendMail "tetstfrom@test.com", "mdelellis@domeprinting.com", "", "", "test sending mail", "supah test"

end sub

sub OpenConn()
	if not gblnConnOpen then
		set gobjConn = Server.CreateObject("ADODB.Connection")
		gobjConn.Open gstrConnectString
		gblnConnOpen = true
	end if
end sub

sub CloseConn()
	if gblnConnOpen and IsObject(gobjConn) then
		gobjConn.Close
		set gobjConn = nothing
		gblnConnOpen = false
	end if
end sub

sub ConnExecute(strSQL)
	OpenConn
	'response.write strsql
	gobjConn.execute(strSQL)
end sub

function ConnOpenRS(strSQL)
	OpenConn
	set ConnOpenRS = gobjConn.execute(strSQL)
end function

function formatDate(dateToFormat)
	dim newDate, splitDate
	
	splitDate = Split(dateToFormat, "-")
	newDate = splitDate(1) & "-" & splitDate(2) & "-" & splitDate(0)
	
	formatDate = newDate
end function

sub checkAdmin

	If Session(SESSION_MERCHANT_ULOGIN) <> ""  Then
		response.write "[{""admin"": ""true""}]"
	else
		response.write "[{""admin"": ""false""}]"
	end if

end sub
%>