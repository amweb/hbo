<!--#include file="config/incUstore.asp"-->

<%
reqAction = request("action")
select case reqAction
	case "CreateOutput"
		CreateOutput
end select

sub CreateOutput()
	'response.Write "in DrawExport"
	dim intTicketID, RecipientListSchemaXml, intQuantity
	dim strFieldSep, strFieldQuote, strFieldAltQuote, strFileName, strData, strEOL, strGiftMessage, XML, fullXML, oXmlNodes, indivNode, dataNode, dctHeaderName, dctValues, resultsString, objTextStream, objFileSystemObject, strSQL, rsTemp
	dim strLine, strItem, strNote, i
	
	intTicketID = request("intTicketID")
	intQuantity = request("intQuantity")
	
	set dctHeaderName = Server.CreateObject("Scripting.Dictionary")
	set dctValues = Server.CreateObject("Scripting.Dictionary")
	
	strSQL = "select vchDialName,vchDialValue from " & TABLE_PRODUCT_SAVED_DIALS  & " where intTicketTableID = " & intTicketID
	'response.write strSQL
	openConn
	set rsTemp = gobjConn.execute(strSQL)
	
	'Response.ContentType = "application/vnd.ms-excel"
	'Response.AddHeader "Content-Disposition", "attachment; filename=RecipientListTemplate_.xls"
	
	i = 0	
	while not rsTemp.EOF
		dctHeaderName.Add i, rsTemp("vchDialName") & ""
		dctValues.Add i, rsTemp("vchDialValue") & ""
		i = i + 1
		rsTemp.MoveNext
	wend
	'if strFieldQuote <> "" then
        
    Dim intLoop, intLoop2
	resultsString = "<html>"
    resultsString = resultsString & "<head>"
    resultsString = resultsString & "<meta http-equiv=""Content-Type"" "
    resultsString = resultsString & "content=""text/html; charset=UTF-8"">"
    resultsString = resultsString & "<style type=""text/css"">"
    resultsString = resultsString & "html, body, table {"
    resultsString = resultsString & "    margin: 0;"
    resultsString = resultsString & "    padding: 0;"
    resultsString = resultsString & "    font-size: 11pt;"
    resultsString = resultsString & "}"
    resultsString = resultsString & "table, th, td { "
    resultsString = resultsString & "    border: 0.1pt solid #D0D7E5;"
    resultsString = resultsString & "    border-collapse: collapse;"
    resultsString = resultsString & "    border-spacing: 0;"
    resultsString = resultsString & "}"
    resultsString = resultsString & "</style>"
    resultsString = resultsString & "</head>"
    resultsString = resultsString & "<body>"
    resultsString = resultsString & "<table><tr>"
    For intLoop = 0 To (dctHeaderName.Count - 1)
		'response.write intLoop & " value: " & dctHeaderName(intLoop)
        resultsString = resultsString & "<td>" & dctHeaderName(intLoop) & "</td>"
    Next
	resultsString = resultsString & "</tr>"
	For intLoop = 0 To (intQuantity - 1)
		resultsString = resultsString & "<tr>"
        For intLoop2 = 0 To (dctValues.Count - 1)
			resultsString = resultsString & "<td>" & dctValues(intLoop2) & "</td>"
		Next
		resultsString = resultsString & "</tr>"
    Next
    resultsString = resultsString & "</tr></table></body></html>"
	'response.write resultsString
	'response.end
		
	'Set objFileSystemObject = Server.CreateObject("Scripting.FileSystemObject")
 
    ' Create a text file and dump the contents of the string to the file,
    ' using the file name export with the current date and time as a way to seperate out differing users
	'response.write Server.MapPath("\drcpod") & "\" & strFileName & ".xls"
	'Set objTextStream = objFileSystemObject.CreateTextFile(Server.MapPath("\drcpod") & "\" & strFileName & ".xls", 2, True)
    'objTextStream.Write(ResultsString)
 
    ' Clean up
    'objTextStream.Close
    'Set objTextStream = Nothing
    'Set objFileSystemObject = Nothing
	
	
	'start from here for excel
	Set objExcel = CreateObject("Excel.Application") 
	objExcel.Visible = True 
	objExcel.DisplayAlerts = FALSE 

	Set objWorkbook = objExcel.Workbooks.Add 
	Set objWorksheet = objWorkbook.Worksheets(1) 

	For intLoop = 0 To (dctHeaderName.Count - 1)
		objWorksheet.Cells(intLoop, 1).Value = dctHeaderName(intLoop)
	next
	
	objWorkbook.SaveAs(Server.MapPath("\drcpod") & "\" & strFileName & ".xls") 
	objExcel.Quit 

	'end if
	'CreateRecipientListTemplate = strFileName & ".csv"
end sub
%>