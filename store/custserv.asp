<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: default.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2000 SacWeb, Inc. All rights reserved.
' = Description:
' =   GoCart Customer Service Center
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

'resetting this
Session("DummyUser")=""
OpenConn

If gintUserName&"" = ""  Then
Response.Redirect virtualbase
End if

SelectCurrentOrder

dim strAction, strReturnURL, gtfError, gstrHint

strReturnURL = Request("return")
if strReturnURL = "" then
	strReturnURL = "default.asp"
end if
strAction = lcase(Request("action"))

select case strAction
	case "reset":
		' for debugging - abandon session and redirect to home page
		Session.Abandon
		response.redirect nonsecurebase & "default.asp"
	case "loginsubmit": ' login form submission
		if LoginUser(Request("email"), Request("password")) then
			'response.write strreturnurl
			response.redirect strReturnURL
		else
			gtfError = true
			DrawHeader "Checkout Center", "custserv"
			DrawPage
			DrawFooter "custserv"
		end if
	case "hint": ' lookup hint
		DrawHeader "Lost Password", "custserv"
		DrawHintPage
		DrawFooter "custserv"
	case "newcustomer": ' create a new customer
		DrawHeader "New Customer", "custserv"
		DrawNewCustomerPage
		DrawFooter "custserv"
	case "newcustomersubmit": ' new customer form submission
		AutoCheck Request, ""
		if not FormErrors.Exists("strConfirmPassword") and not FormErrors.Exists("strPassword") then
			if Request("strPassword") <> Request("strConfirmPassword") then
				FormErrors.Add "strConfirmPassword", "Please confirm your password by retyping it exactly."
			end if
		end if
		if FormErrors.count = 0 then
			if CreateNewUser(Request("strEmail"), Request("strPassword"), Request("strHint"), Request("strFirstname"), Request("strLastname"), Request("strAddress"), Request("strCity"), Request("strState"), Request("strZip"), Request("strCountry")) then
				response.redirect strReturnURL
			else
				FormErrors.Add "strEmail", "An account already exists with this email address."
			end if
		end if
		DrawHeader "New Customer", "custserv"
		DrawNewCustomerPage
		DrawFooter "custserv"
	case "logout": ' user logout
		if gintOrderID > 0 then
			' there is an order in progress--let's confirm the logout request
			DrawHeader "Logout", "custserv"
			DrawLogoutPage
			DrawFooter "custserv"
		else
			LogoutUser
			response.redirect homebase
		end if
	case "logoutsubmit": ' logout form submission
		if gintOrderID > 0 then
			if lcase(Request("saveorder")) = "n" then
				CancelOrder
			end if
		end if
		LogoutUser
		response.redirect homebase
	case "accountinfo": ' account information form
		DrawHeader "Account Information", "custserv"
		DrawAccountPage
		DrawFooter "custserv"
         Session("Message") = ""
	case "accountinfosubmit": ' account form submission
		AutoCheck Request, ""
		if FormErrors.count = 0 then
			ChangeUserInfoEx  Request("strEmail"), Request("strPassword"), Request("strHint"), Request("strFirstName"), Request("strLastName"), Request("strAddress"),Request("strCity"),Request("strState"),Request("strZip"),Request("strCountry"),Request("strBusinessUnit"),Request("strDivision"), Request("strDayPhone"), Request("strNightPhone"), Request("strFax")
			DrawHeader "Account Information", "custserv"
			DrawConfirmPage "Your account information has been updated."
			DrawFooter "custserv"

		else
			DrawHeader "Account Information", "custserv"
			DrawAccountPage
			DrawFooter "custserv"
		end if
	case "selectorder":
		if IsNumeric(Request("id")) then
			SelectSavedOrder Request("id")
		end if
		response.redirect strReturnURL
	case "save":
		if gintOrderID > 0 then
			if gintUserID > 0 then
				AbandonOrder
				DrawHeader "Order Saved", "custserv"
				DrawConfirmPage "Your order has been saved. You may return to complete it at anytime during the next two weeks."
				DrawFooter "custserv"
			else
				response.redirect "default.asp?action=login&return=" & Server.URLEncode("default.asp?action=save")
			end if
		else
			response.redirect strReturnURL
		end if
	case else:
		if lcase(request("hint")) = "y" then
			if LookupHint(Request("email"), gstrHint) = false then
				gstrHint = "No account could be found with this email address."
			end if
		else
			gstrHint = ""
		end if
		DrawHeader "Checkout Service Center", "custserv"
		DrawPage
		DrawReturnPolicy
		DrawFooter "custserv"
end select
response.end

sub DrawPage

	if gintUserID > 0 then
		DrawMenu
	else
		DrawLogin
	end if

end sub

sub DrawLogin

gintOrderID = Session(SESSION_PUB_ORDER_ID)



%>
<div class="content">
	<!-- BEGIN LOGIN FORM -->
	<form class="login-form" action="default.asp" method="post">    
<% HiddenInput "action", "loginsubmit" %>
<% HiddenInput "return", strReturnURL %>
		<h3 class="form-title">Login to your account</h3>
        <%if gtfError then %>
		<div class="alert alert-danger">
			<button class="close" data-close="alert"></button>
			<span>
			The username or password you entered is incorrect. Please try again. </span>
		</div>
        <%end if %>
        <%
        if gstrHint <> "" then
	        if gstrHint = "No account could be found with this email address." then
		     %>
		    <div class="alert alert-danger">
			    <button class="close" data-close="alert"></button>
			    <span>
			    <%= gstrHint %> </span>
		    </div>
            <%
	        elseif gstrHint = "none" then
		    %>
		    <div class="alert alert-danger">
			    <button class="close" data-close="alert"></button>
			    <span>
			    We are sorry, there is no hint on file for this account. </span>
		    </div>
            <% 
	        else
	         %>
		    <div class="alert alert-danger">
			    <button class="close" data-close="alert"></button>
			    <span>
			    <%response.write "The hint for your password is: <B>" & gstrHint  %> </span>
		    </div>
            <%
	        end if
        end if
         %>
		<div class="form-group">
			<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
			<label class="control-label visible-ie8 visible-ie9">Username</label>
			<div class="input-icon">
				<i class="fa fa-user"></i>
				<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="email"/>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Password</label>
			<div class="input-icon">
				<i class="fa fa-lock"></i>
				<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>
			</div>
		</div>
		<div class="form-actions">
			<label class="checkbox">
			<input type="checkbox" name="remember" value="1"/> Remember me </label>
			<button type="submit" class="btn red pull-right">
			Login <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>
		<div class="forget-password">
			<h4>Forgot your password ?</h4>
			<p>click <a href="default.asp?action=hint&return=<%= strReturnURL %>" id="forget-password">here </a>
				for your password hint.
			</p>
		</div>
		<div class="create-account">
			<p>Don't have an account yet ?&nbsp; <a href="default.asp?action=newcustomer&return=<%= server.urlencode(request("return")) %>" id="register-btn">Create an account </a>
			</p>
		</div>
	</form>
</div>
<%
	


end sub

sub DrawNewCustomerPage
%>
<div class="content">
    <form  action="default.asp" method="post">
    <%
	    HiddenInput "action", "newcustomersubmit"
	    HiddenINput "return", strReturnURL
    %>
        <h3>Sign Up</h3>
	    <p>
			    Enter your personal details below:
	    </p>
        <div class="form-group">
	        <label class="control-label visible-ie8 visible-ie9">Full Name</label>
	        <div class="row">
		        <div class="col-md-6">
			        <div class="input-icon">
				        <i class="fa fa-font"></i>
				        <input class="form-control placeholder-no-fix" type="text" placeholder="First Name" maxlength="15" name="strFirstName"/>
			        </div>
		        </div>
		        <div class="col-md-6">
			        <div class="input-icon">
				        <i class="fa fa-font"></i>
				        <input class="form-control placeholder-no-fix" type="text" placeholder="Last Name" maxlength="100" name="strLatName"/>
			        </div>
		        </div>
	        </div>
	    </div>
        <div class="form-group">
		    <label class="control-label visible-ie8 visible-ie9">Address</label>
		    <div class="input-icon">
			    <i class="fa fa-check"></i>
			    <input class="form-control placeholder-no-fix" type="text" placeholder="Address" maxlength="255" name="strAddress"/>
		    </div>
	    </div>
	    <div class="form-group">
		    <label class="control-label visible-ie8 visible-ie9">City/Town</label>
		    <div class="input-icon">
			    <i class="fa fa-location-arrow"></i>
			    <input class="form-control placeholder-no-fix" type="text" placeholder="City/Town" maxlength="50" name="strCity"/>
		    </div>
	    </div>
        <div class="form-group">
		    <label class="control-label visible-ie8 visible-ie9">State/Provence</label>
		    <div class="input-icon">
			    <i class="fa fa-location-arrow"></i>
			    <input class="form-control placeholder-no-fix" type="text" placeholder="State/Provence" name="strState"/>
		    </div>
	    </div>
        <div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Country</label>
			<select name="strCountry" id="select2_sample4" class="select2 form-control">
				<option value="US">United States</option>
				<option value="AF">Afghanistan</option>
				<option value="AL">Albania</option>
				<option value="DZ">Algeria</option>
				<option value="AS">American Samoa</option>
				<option value="AD">Andorra</option>
				<option value="AO">Angola</option>
				<option value="AI">Anguilla</option>
				<option value="AQ">Antarctica</option>
				<option value="AR">Argentina</option>
				<option value="AM">Armenia</option>
				<option value="AW">Aruba</option>
				<option value="AU">Australia</option>
				<option value="AT">Austria</option>
				<option value="AZ">Azerbaijan</option>
				<option value="BS">Bahamas</option>
				<option value="BH">Bahrain</option>
				<option value="BD">Bangladesh</option>
				<option value="BB">Barbados</option>
				<option value="BY">Belarus</option>
				<option value="BE">Belgium</option>
				<option value="BZ">Belize</option>
				<option value="BJ">Benin</option>
				<option value="BM">Bermuda</option>
				<option value="BT">Bhutan</option>
				<option value="BO">Bolivia</option>
				<option value="BA">Bosnia and Herzegowina</option>
				<option value="BW">Botswana</option>
				<option value="BV">Bouvet Island</option>
				<option value="BR">Brazil</option>
				<option value="IO">British Indian Ocean Territory</option>
				<option value="BN">Brunei Darussalam</option>
				<option value="BG">Bulgaria</option>
				<option value="BF">Burkina Faso</option>
				<option value="BI">Burundi</option>
				<option value="KH">Cambodia</option>
				<option value="CM">Cameroon</option>
				<option value="CA">Canada</option>
				<option value="CV">Cape Verde</option>
				<option value="KY">Cayman Islands</option>
				<option value="CF">Central African Republic</option>
				<option value="TD">Chad</option>
				<option value="CL">Chile</option>
				<option value="CN">China</option>
				<option value="CX">Christmas Island</option>
				<option value="CC">Cocos (Keeling) Islands</option>
				<option value="CO">Colombia</option>
				<option value="KM">Comoros</option>
				<option value="CG">Congo</option>
				<option value="CD">Congo, the Democratic Republic of the</option>
				<option value="CK">Cook Islands</option>
				<option value="CR">Costa Rica</option>
				<option value="CI">Cote d'Ivoire</option>
				<option value="HR">Croatia (Hrvatska)</option>
				<option value="CU">Cuba</option>
				<option value="CY">Cyprus</option>
				<option value="CZ">Czech Republic</option>
				<option value="DK">Denmark</option>
				<option value="DJ">Djibouti</option>
				<option value="DM">Dominica</option>
				<option value="DO">Dominican Republic</option>
				<option value="EC">Ecuador</option>
				<option value="EG">Egypt</option>
				<option value="SV">El Salvador</option>
				<option value="GQ">Equatorial Guinea</option>
				<option value="ER">Eritrea</option>
				<option value="EE">Estonia</option>
				<option value="ET">Ethiopia</option>
				<option value="FK">Falkland Islands (Malvinas)</option>
				<option value="FO">Faroe Islands</option>
				<option value="FJ">Fiji</option>
				<option value="FI">Finland</option>
				<option value="FR">France</option>
				<option value="GF">French Guiana</option>
				<option value="PF">French Polynesia</option>
				<option value="TF">French Southern Territories</option>
				<option value="GA">Gabon</option>
				<option value="GM">Gambia</option>
				<option value="GE">Georgia</option>
				<option value="DE">Germany</option>
				<option value="GH">Ghana</option>
				<option value="GI">Gibraltar</option>
				<option value="GR">Greece</option>
				<option value="GL">Greenland</option>
				<option value="GD">Grenada</option>
				<option value="GP">Guadeloupe</option>
				<option value="GU">Guam</option>
				<option value="GT">Guatemala</option>
				<option value="GN">Guinea</option>
				<option value="GW">Guinea-Bissau</option>
				<option value="GY">Guyana</option>
				<option value="HT">Haiti</option>
				<option value="HM">Heard and Mc Donald Islands</option>
				<option value="VA">Holy See (Vatican City State)</option>
				<option value="HN">Honduras</option>
				<option value="HK">Hong Kong</option>
				<option value="HU">Hungary</option>
				<option value="IS">Iceland</option>
				<option value="IN">India</option>
				<option value="ID">Indonesia</option>
				<option value="IR">Iran (Islamic Republic of)</option>
				<option value="IQ">Iraq</option>
				<option value="IE">Ireland</option>
				<option value="IL">Israel</option>
				<option value="IT">Italy</option>
				<option value="JM">Jamaica</option>
				<option value="JP">Japan</option>
				<option value="JO">Jordan</option>
				<option value="KZ">Kazakhstan</option>
				<option value="KE">Kenya</option>
				<option value="KI">Kiribati</option>
				<option value="KP">Korea, Democratic People's Republic of</option>
				<option value="KR">Korea, Republic of</option>
				<option value="KW">Kuwait</option>
				<option value="KG">Kyrgyzstan</option>
				<option value="LA">Lao People's Democratic Republic</option>
				<option value="LV">Latvia</option>
				<option value="LB">Lebanon</option>
				<option value="LS">Lesotho</option>
				<option value="LR">Liberia</option>
				<option value="LY">Libyan Arab Jamahiriya</option>
				<option value="LI">Liechtenstein</option>
				<option value="LT">Lithuania</option>
				<option value="LU">Luxembourg</option>
				<option value="MO">Macau</option>
				<option value="MK">Macedonia, The Former Yugoslav Republic of</option>
				<option value="MG">Madagascar</option>
				<option value="MW">Malawi</option>
				<option value="MY">Malaysia</option>
				<option value="MV">Maldives</option>
				<option value="ML">Mali</option>
				<option value="MT">Malta</option>
				<option value="MH">Marshall Islands</option>
				<option value="MQ">Martinique</option>
				<option value="MR">Mauritania</option>
				<option value="MU">Mauritius</option>
				<option value="YT">Mayotte</option>
				<option value="MX">Mexico</option>
				<option value="FM">Micronesia, Federated States of</option>
				<option value="MD">Moldova, Republic of</option>
				<option value="MC">Monaco</option>
				<option value="MN">Mongolia</option>
				<option value="MS">Montserrat</option>
				<option value="MA">Morocco</option>
				<option value="MZ">Mozambique</option>
				<option value="MM">Myanmar</option>
				<option value="NA">Namibia</option>
				<option value="NR">Nauru</option>
				<option value="NP">Nepal</option>
				<option value="NL">Netherlands</option>
				<option value="AN">Netherlands Antilles</option>
				<option value="NC">New Caledonia</option>
				<option value="NZ">New Zealand</option>
				<option value="NI">Nicaragua</option>
				<option value="NE">Niger</option>
				<option value="NG">Nigeria</option>
				<option value="NU">Niue</option>
				<option value="NF">Norfolk Island</option>
				<option value="MP">Northern Mariana Islands</option>
				<option value="NO">Norway</option>
				<option value="OM">Oman</option>
				<option value="PK">Pakistan</option>
				<option value="PW">Palau</option>
				<option value="PA">Panama</option>
				<option value="PG">Papua New Guinea</option>
				<option value="PY">Paraguay</option>
				<option value="PE">Peru</option>
				<option value="PH">Philippines</option>
				<option value="PN">Pitcairn</option>
				<option value="PL">Poland</option>
				<option value="PT">Portugal</option>
				<option value="PR">Puerto Rico</option>
				<option value="QA">Qatar</option>
				<option value="RE">Reunion</option>
				<option value="RO">Romania</option>
				<option value="RU">Russian Federation</option>
				<option value="RW">Rwanda</option>
				<option value="KN">Saint Kitts and Nevis</option>
				<option value="LC">Saint LUCIA</option>
				<option value="VC">Saint Vincent and the Grenadines</option>
				<option value="WS">Samoa</option>
				<option value="SM">San Marino</option>
				<option value="ST">Sao Tome and Principe</option>
				<option value="SA">Saudi Arabia</option>
				<option value="SN">Senegal</option>
				<option value="SC">Seychelles</option>
				<option value="SL">Sierra Leone</option>
				<option value="SG">Singapore</option>
				<option value="SK">Slovakia (Slovak Republic)</option>
				<option value="SI">Slovenia</option>
				<option value="SB">Solomon Islands</option>
				<option value="SO">Somalia</option>
				<option value="ZA">South Africa</option>
				<option value="GS">South Georgia and the South Sandwich Islands</option>
				<option value="ES">Spain</option>
				<option value="LK">Sri Lanka</option>
				<option value="SH">St. Helena</option>
				<option value="PM">St. Pierre and Miquelon</option>
				<option value="SD">Sudan</option>
				<option value="SR">Suriname</option>
				<option value="SJ">Svalbard and Jan Mayen Islands</option>
				<option value="SZ">Swaziland</option>
				<option value="SE">Sweden</option>
				<option value="CH">Switzerland</option>
				<option value="SY">Syrian Arab Republic</option>
				<option value="TW">Taiwan, Province of China</option>
				<option value="TJ">Tajikistan</option>
				<option value="TZ">Tanzania, United Republic of</option>
				<option value="TH">Thailand</option>
				<option value="TG">Togo</option>
				<option value="TK">Tokelau</option>
				<option value="TO">Tonga</option>
				<option value="TT">Trinidad and Tobago</option>
				<option value="TN">Tunisia</option>
				<option value="TR">Turkey</option>
				<option value="TM">Turkmenistan</option>
				<option value="TC">Turks and Caicos Islands</option>
				<option value="TV">Tuvalu</option>
				<option value="UG">Uganda</option>
				<option value="UA">Ukraine</option>
				<option value="AE">United Arab Emirates</option>
				<option value="GB">United Kingdom</option>
				<option value="UM">United States Minor Outlying Islands</option>
				<option value="UY">Uruguay</option>
				<option value="UZ">Uzbekistan</option>
				<option value="VU">Vanuatu</option>
				<option value="VE">Venezuela</option>
				<option value="VN">Viet Nam</option>
				<option value="VG">Virgin Islands (British)</option>
				<option value="VI">Virgin Islands (U.S.)</option>
				<option value="WF">Wallis and Futuna Islands</option>
				<option value="EH">Western Sahara</option>
				<option value="YE">Yemen</option>
				<option value="ZM">Zambia</option>
				<option value="ZW">Zimbabwe</option>
			</select>
		</div>
        <p>
				 Enter your account details below:
			</p>
			<div class="form-group">
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<label class="control-label visible-ie8 visible-ie9">Email</label>
				<div class="input-icon">
					<i class="fa fa-envelope"></i>
					<input class="form-control placeholder-no-fix" type="text" placeholder="Email" maxlength="255" name="strEmail"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label visible-ie8 visible-ie9">Password</label>
				<div class="input-icon">
					<i class="fa fa-lock"></i>
					<input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Password" name="strPassword"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
				<div class="controls">
					<div class="input-icon">
						<i class="fa fa-check"></i>
						<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Re-type Your Password" name="strConfirmPassword"/>
					</div>
				</div>
			</div>
            <div class="form-actions">
				
				<button type="submit" id="register-submit-btn" class="btn red pull-right">
				Sign Up <i class="m-icon-swapright m-icon-white"></i>
				</button>
			</div>
		</form>
		<!-- END REGISTRATION FORM -->
	</div>
  <%
end sub

sub DrawLogoutPage
%>
  If you wish, you may save the order you are working on and finish it later. 
  Orders will be saved for up to two weeks.<BR>
</div>
<FORM ACTION="default.asp" METHOD="POST">
<% HiddenInput "action", "logoutsubmit" %>
<% RadioButton "saveorder", "Y", "Y" %> <B>Save my order, I'll finish it later.</B><BR>
<% RadioButton "saveorder", "N", "Y" %> Cancel my order<BR>
<BR>
<INPUT TYPE=IMAGE SRC="<%= g_imagebase %>menu/continue.gif" WIDTH="64" HEIGHT="16" ALT="Continue" BORDER="0">
</FORM>
<BR>
<A HREF="<%= strReturnURL %>"><IMG SRC="<%= g_imagebase %>menu/go-back.gif" WIDTH="62" HEIGHT="16" ALT="Go Back" BORDER="0"></A>
<%
end sub

' =============================
sub DrawMenu
dim aryTemp
		dim strSQL, rsTemp, strEmail
		strSQL = "SELECT U.vchEmail"
		strSQL = strSQL & " FROM " & STR_TABLE_SHOPPER & " AS U WHERE U.intID=" & gintUserID
		set rsTemp = gobjConn.execute(strSQL)
		if not rsTemp.eof then
			strEmail = Server.URLEncode(rsTemp(0))
		end if
		rsTemp.close
		set rsTemp = nothing
%>
	<!-- BEGIN LOGIN -->
	<div class="content">
<%= font(2) %><B>SAVED ORDERS</B></FONT><BR>
<%
dim dctSavedOrders, i, k, rsData
set dctSavedOrders = GetSavedOrders()
if dctSavedOrders.count > 0 then
	%>
	To continue shopping with a saved order, click the Select button for that order.<br />
	&nbsp;
	<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%">
	<TR id="cart">
		<TD>&nbsp;</TD>
		<TD>Order #</TD>
		<TD>Order Created</TD>
		<TD>Order Last Updated</TD>
	</TR>
	<%
		dim strColor
		for each k in dctSavedOrders
			aryTemp = split(dctSavedOrders(k),"|")
			if k = gintOrderID then
				strColor = cWhite
				%>
				<TR BGCOLOR="<%= cGoCart_CSSelectBG %>"><TD><%= fontx(1,1,cWhite) %><B>Currently Selected</B></TD>
				<%
			else
				strColor = cBlack
				%>
				<TR><TD><%= font(1) %><A HREF="default.asp?action=selectorder&id=<%= k %>&return=<%= Server.URLEncode("cartlist.asp") %>"><IMG SRC="<%= g_imagebase %>menu/select.gif" WIDTH="50" HEIGHT="16" ALT="Select" BORDER="0"></A></TD>
				<%
			end if
			%>
				<TD><%= fontx(1,1,strColor) %>#<%= aryTemp(0) %></font></TD>
				<TD><%= fontx(1,1,strColor) %><%= aryTemp(1) %></font></TD>
				<TD><%= fontx(1,1,strColor) %><%= aryTemp(2) %></font></TD>
			</TR>
			<TR>
				<TD COLSPAN=4><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cGoCart_CSHdrBG %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
			</TR>
			<%
		next
	%>
	</TABLE>
	<%
else
	%>
	You do not have any past orders saved.<BR>
	<%
end if
%>
<BR>
<%= font(2) %><B>ORDER HISTORY</B></FONT><BR>
<%
set dctSavedOrders = GetOrderHistory()
if dctSavedOrders.count > 0 then
	%>
	<%= font(1) %>To view a completed order, click the View button</FONT><BR>
	&nbsp;
	<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%">
	<TR BGCOLOR="<%= cGoCart_CSHdrBG %>">
		<TD><%= fontx(1,1,cWhite) %>&nbsp;</font></TD>
		<TD><%= fontx(1,1,cWhite) %>Order #</font></TD>
		<TD><%= fontx(1,1,cWhite) %>Order Created</font></TD>
		<TD><%= fontx(1,1,cWhite) %>Order Last Updated</font></TD>
		<TD><%= fontx(1,1,cWhite) %>Status</font></TD>
	</TR>
	<%
	for each k in dctSavedOrders
		aryTemp = split(dctSavedOrders(k),"|")
		aryTemp(2) = replace(aryTemp(2), ":00 ", " ")
		aryTemp(3) = replace(aryTemp(3), ":00 ", " ")
		
		%>
		<TR>
			<TD><A HREF="invoice.asp?order=<%= k %>"><IMG SRC="<%= g_imagebase %>menu/vieworder.gif" WIDTH="78" HEIGHT="16" ALT="View Order" BORDER="0"></A></TD>
			<TD><%= font(1) %>#<%= aryTemp(0) %></font></TD>
			<TD><%= font(1) %><%= aryTemp(2) %></font></TD>
			<TD><%= font(1) %><%= aryTemp(3) %></font></TD>
			<TD><%= font(1) %><%= GetArrayValue(aryTemp(1), dctOrderStatusInvoiceValues) %></font></TD>
		</TR>
		<TR>
			<TD COLSPAN=5><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cBeige %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
		</TR>
		<%
		'response.write "<A HREF=""invoice.asp?order=" & k & """>" & dctSavedOrders(k) & "</A><BR>"
	next
	%>
	</TABLE>
	<%
else
	%>
	You do not have any submitted orders.<BR>
	<%
end if
%>
</div>
<%
end sub

sub DrawAccountPage
	dim strPassword, strHint, strFirstName, strLastName, strCompany, strEmail, strAddress, strCity, strState, strZip, strCountry, strBusinessUnit, strDivision, strDayPhone, strNightPhone, strFax
	if FormErrors.count = 0 then
		' load saved information
		GetUserInfoEx strHint, strFirstName, strLastName, strCompany, strEmail, strAddress, strCity, strState, strZip, strCountry, strBusinessUnit, strDivision, strDayPhone, strNightPhone, strFax
		strPassword = ""
	else
		' form submitted with errors
		' use information from last form submission
		strHint = Request("strHint")
		strPassword = Request("strPassword")
		strFirstName = Request("strFirstName")
		strLastName = Request("strLastName")
		strCompany = Request("strCompany")
		strEmail = Request("strEmail")
		stlAddress = Request("strAddress")
		strCity = Request("strCity")
		strState = Request("strState")
        strCountry = Request("strCountry")
		strZip = Request("strZip")
        strBusinessUnit = Request("strBusinessUnit")
        strDivision = Request("strDivision")
        strDayPhone = Request("strDayPhone")
        strNightPhone = Request("strNightPhone")
        strFax = Request("strFax")

	end if
%>

<div class="content">
    <form  action="custserv.asp" method="post">
    <%
	    HiddenInput "action", "accountinfosubmit"
	    HiddenINput "return", strReturnURL
    %>
        <h3><%= Session("Message") %></h3>
	    <p>
			    Enter your personal details below:
	    </p>
        <div class="form-group">
	        <label class="control-label visible-ie8 visible-ie9">Full Name</label>
	        <div class="row">
		        <div class="col-md-6">
			        <div class="input-icon">
				        <i class="fa fa-font"></i>
				        <input class="form-control placeholder-no-fix" type="text" placeholder="First Name" name="strFirstName" maxlength="15" value="<%= strFirstName %>"/>
			        </div>
		        </div>
		        <div class="col-md-6">
			        <div class="input-icon">
				        <i class="fa fa-font"></i>
				        <input class="form-control placeholder-no-fix" type="text" placeholder="Last Name" name="strLastName" maxlength="100" value="<%= strLastName %>"/>
			        </div>
		        </div>
	        </div>
	    </div>
        <div class="form-group">
		    <label class="control-label visible-ie8 visible-ie9">Day Phone</label>
		    <div class="input-icon">
			    <i class="fa fa-phone"></i>
			    <input class="form-control placeholder-no-fix" type="text" placeholder="Day Phone" name="strDayPhone" maxlength="20" value="<%= strDayPhone %>" />
		    </div>
	    </div>
        <div class="form-group">
		    <label class="control-label visible-ie8 visible-ie9">Night Phone</label>
		    <div class="input-icon">
			    <i class="fa fa-phone"></i>
			    <input class="form-control placeholder-no-fix" type="text" placeholder="Night Phone" name="strNightPhone" maxlength="20" value="<%= strNightPhone %>" />
		    </div>
	    </div>
        <div class="form-group">
		    <label class="control-label visible-ie8 visible-ie9">Fax</label>
		    <div class="input-icon">
			    <i class="fa fa-phone"></i>
			    <input class="form-control placeholder-no-fix" type="text" placeholder="Fax" name="strFax" maxlength="20" value="<%= strFax %>" />
		    </div>
	    </div>

        <!--<div class="form-group">
		    <label class="control-label visible-ie8 visible-ie9">Business Unit</label>
		    <div class="input-icon">
			    <i class="fa fa-check"></i>
			    <input disabled class="form-control placeholder-no-fix" type="text" placeholder="Business Unit" name="strBusinessUnit" value="<%= strBusinessUnit %>" />
		    </div>
	    </div>-->

        <div class="form-group">
		    <label class="control-label visible-ie8 visible-ie9">Division</label>
		    <div class="input-icon">
			    <i class="fa fa-check"></i>
			    <input class="form-control placeholder-no-fix" type="text" placeholder="Division" name="strDivision" value="<%= strDivision %>" />
		    </div>
	    </div>
        <div class="form-group">
		    <label class="control-label visible-ie8 visible-ie9">Address</label>
		    <div class="input-icon">
			    <i class="fa fa-check"></i>
			    <input class="form-control placeholder-no-fix" type="text" placeholder="Address" name="strAddress" maxlength="255" value="<%= strAddress %>"/>
		    </div>
	    </div>
	    <div class="form-group">
		    <label class="control-label visible-ie8 visible-ie9">City/Town</label>
		    <div class="input-icon">
			    <i class="fa fa-location-arrow"></i>
			    <input class="form-control placeholder-no-fix" type="text" placeholder="City/Town" name="strCity" maxlength="50" value="<%= strCity %>"/>
		    </div>
	    </div>
        <div class="form-group">
		    <label class="control-label visible-ie8 visible-ie9">State/Provence</label>
		    <div class="input-icon">
			    <i class="fa fa-location-arrow"></i>
			    <input class="form-control placeholder-no-fix" type="text" placeholder="State/Provence" name="strState" maxlength="20" value="<%= strState %>"/>
		    </div>
	    </div>
        <div class="form-group">
			<label class="control-label visible-ie8 visible-ie9">Country</label>
			<select name="strCountry" id="select1" class="select2 form-control">
				<%
                dim key
                    for each key in dctCountryCodes.Keys
                    %>
                    <option value="<%=key %>" <%= iif(strCountry = key," selected= ""Selected""","") %>><%= dctCountryCodes(key) %></option>
                    <%
                    next
                 %>
			</select>
		</div>
        <p>
				 Enter your account details below:
			</p>
			<div class="form-group">
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<label class="control-label visible-ie8 visible-ie9">Email</label>
				<div class="input-icon">
					<i class="fa fa-envelope"></i>
					<input class="form-control placeholder-no-fix" type="text" placeholder="Email" name="strEmail" maxlength="255" value="<%= strEmail %>"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label visible-ie8 visible-ie9">Password</label>
				<div class="input-icon">
					<i class="fa fa-lock"></i>
					<input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="Password1" placeholder="Password" name="strPassword"/>
				</div>
			</div>
			<div class="form-group">
				<label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
				<div class="controls">
					<div class="input-icon">
						<i class="fa fa-check"></i>
						<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Re-type Your Password" name="strConfirmPassword"/>
					</div>
				</div>
			</div>
            <div class="form-group">
				<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
				<label class="control-label visible-ie8 visible-ie9">Password Hint</label>
				<div class="input-icon">
					<i class="fa fa-lock"></i>
					<input class="form-control placeholder-no-fix" type="text" placeholder="Password Hint" name="strHint" value="<%= strHint %>"/>
				</div>
			</div>
            <div class="form-actions">
				
				<button type="submit" id="Button1" class="btn red pull-right">
				Update Account <i class="m-icon-swapright m-icon-white"></i>
				</button>
			</div>
		</form>
		<!-- END REGISTRATION FORM -->
	</div>
<%
end sub

sub DrawConfirmPage(strMessage)
%>
<div class="content">
<B>Thank you, <%= split(gintUserName," ")(0) %>.</B><BR>
<BR>
<%= strMessage %><BR>
<BR>
<form action="dashboard.asp">

<div class="form-actions">
				
				<button type="submit" id="Button2" class="btn red pull-right">
				Continue <i class="m-icon-swapright m-icon-white"></i>
				</button>
			</div>
</form>
</div>
<%
end sub

sub DrawHintPage()
%>
When you created your account, you provided a hint to help you remember your password. Please enter your email address to find out your hint. If you still can not remember your password after viewing your hint, please contact customer service at <%= STR_MERCHANT_CS_PHONE %> or email <A HREF="mailto:<%= STR_MERCHANT_CS_EMAIL %>"><%= STR_MERCHANT_CS_EMAIL %></A>.<BR>
<BR>
<FORM ACTION="default.asp" METHOD="POST">
<%
	HiddenInput "action", "login"
	HiddenInput "hint", "y"
%>
Email Address: <% TextInput "email", 32, 50, Request %> <INPUT TYPE="submit" VALUE="Display My Hint">
</FORM>
<%
end sub

sub DrawReturnPolicy
%>


<%
end sub
%>