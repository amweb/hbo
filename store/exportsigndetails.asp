<!--#include file="../admin/config/incInit.asp"-->
<%
OpenConn

If gintUserName&"" = ""  Then
	Response.Redirect virtualbase
End if

dim strSQL, rsTableInfo, rsCampaignInfo, intcampaignid, strFileName
	
intcampaignid = cint(request.form("campaign"))
set rsCampaignInfo = gobjconn.execute("SELECT [vchItemName] FROM " & STR_TABLE_INVENTORY & " WHERE [intID]="  & intcampaignid & " AND [chrType]='C' AND [chrStatus]='A'")


response.ContentType = "text/comma-separated-values"
response.AddHeader "Content-transfer-encoding", "binary"
strFileName = Day(Date) & "_" & Month(Date) & "_" & Year(Date) & "_" & Replace(rsCampaignInfo("vchitemname") & "_StoreExport", " ", "_") & ".csv"
response.AddHeader "Content-Disposition", "attachment; filename=" & strFileName
'response.write strFileName

'get items for csv
strSQL = "SELECT F.vchitemname AS FixtureName, I.vchitemname AS ItemName, I.vchsize AS vchSize, I.vchnumsides AS NumSides, I.vchsubstrate AS substrate, LS.vchslotname, I.vchcolorprocess "
strSQL = strSQL & " FROM " & STR_TABLE_PROFILE_STORE_PACKAGE & " SP LEFT JOIN HBO_inv I ON I.intid = SP.intitemid LEFT JOIN " & STR_TABLE_INVENTORY & " F "
strSQL = strSQL & " ON F.intid = I.intparentid LEFT JOIN " & STR_TABLE_LAYOUTS_SLOTS & " LS ON LS.intid = SP.intslotid WHERE  SP.intlayoutid=" & request.form("layout")
strSQL = strSQL & " AND SP.intcampaignid=" & intcampaignid

set rsTableInfo = gobjconn.execute(strSQL)

response.write """Fixture Type"",""Item name"",""Item Size"",""Substrate"",""Number of sides"",""Location"",""Color""" & vbCrLf

if not rsTableInfo.eof then
	while not rsTableInfo.eof
		
		response.write """" & rsTableInfo("FixtureName") & """,""" & rsTableInfo("ItemName") & """"
		response.write ",""" & rsTableInfo("vchSize") & """,""" & rsTableInfo("substrate") & """"
		response.write "," & rsTableInfo("NumSides") & ",""" & rsTableInfo("vchslotname") & """,""" & rsTableInfo("vchcolorprocess") & """" & vbCrLf
		
		rsTableInfo.moveNext()
	wend
end if
%>