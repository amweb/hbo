<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%
OpenConn

If gintUserName&"" = ""  Then
Response.Redirect virtualbase
End if

DrawHeader "Dashboard", "custserv"
%>
    <div class="container-fluid mt-3">
        <div class="row justify-content-center">
            <div class="col-lg-4 col-md-6 col-xs-12">
                <div class="row">
                    <div class="col-3">
                        <label for="">Username:</label>
                    </div>
                    <div class="col-9">
                        <strong>
                            <%=gintUserName%>            
                        </strong>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <label for="">Password:</label>
                        <input type="text" class="form-control">                
                    </div>
                    <div class="col-6">              
                        <label for="">Confirm Password:</label>
                        <input type="text" class="form-control">                
                    </div>
                </div>
            </div>
        </div>
    </div>
<%
DrawFooter "custserv"
%>