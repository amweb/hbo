
<!--#include file="../drcpod/config/incUStore.asp"-->
<%

' =====================================================================================
' = File: default.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Main store-front, inventory browser
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: code cleanup/documentation
' = Description of Customizations:
' =   (none)
' =====================================================================================
OpenConn

If gintUserName&"" = ""  Then
Response.Redirect virtualbase
End if

SelectCurrentOrder
dim intParentID, strKeywords, intBrandID, intProgramID, intCampaignID, intCategoryID, intprofileid
dim dctErrors, dctOrderItemError, dctAddressBook
set dctErrors = CreateObject("Scripting.Dictionary")
set dctOrderItemError = CreateObject("Scripting.Dictionary")
set dctAddressBook = CreateObject("Scripting.Dictionary")
dim dctCheckoutErrors
set dctCheckoutErrors = CreateObject("Scripting.Dictionary")
strKeywords = Request("keywords")
intParentID = Request("parentid")
if IsNumeric(intParentID) then
	intParentID = CLng(intParentID)
else
	intParentID = 0
end if

intBrandID = Request("brandid")


intProgramID = Request("programid")
if IsNumeric(intProgramID) then
	intProgramID = CLng(intProgramID)
else
	intProgramID = 0
end if

intprofileid = Request("profileid")
if IsNumeric(intprofileid) then
	intprofileid = CLng(intprofileid)
else
	intprofileid = 0
end if


intCampaignID = Request("campaignid")
if IsNumeric(intCampaignID) then
	intCampaignID = CLng(intCampaignID)
else
	intCampaignID = 0
end if

if Request("action")="Return" Then
    Session("Catalog") = ""
end if

intCategoryID = Request("categoryid")
if IsNumeric(intCategoryID) then
	intCategoryID = CLng(intCategoryID)
else
	intCategoryID = 0
end if

if Request("action")="Return" Then
    Session("Catalog") = ""
end if

dim strPageTitle, strPageText, rsData

dim intItemCount, intFolderCount, blnLeftImage
blnLeftImage = false

if strKeywords = "" then
	if intParentID = 0 then
		gintInvParentUpID = -1
		' ** code-change **
		strPageTitle = "Online Store"
	else
		Inventory_GetFolderInfo intParentID, strPageTitle, strPageText, gintInvParentUpID
	end if

    if Request("lt") <> "" Then
        if Request("vchItemName") <> "" Then
            set rsData = Inventory_GetItemsFromFolderLtByNumber(Request("vchItemName"), request("lt"))
        elseif intBrandID <> "" and intCategoryID>0 then
            set rsData = Inventory_GetItemsFromFolderLtEx3(intParentID,intBrandID,intCategoryID, true, true)
        elseif intBrandID <> "" then
            set rsData = Inventory_GetItemsFromFolderLtEx2(intParentID,intBrandID, true, true)
        elseif intCategoryID > 0 then
            set rsData = Inventory_GetItemsFromFolderLtEx4(intParentID,intCategoryID, true, true)
        else
            set rsData = Inventory_GetItemsFromFolderLt(-1, true, true)
        end if
    else
        if intBrandID <> "" and intCampaignID > 0 and intCategoryID > 0 then
            set rsData = Inventory_GetItemsFromFolderEx6(intParentID,intCampaignID,intBrandId,intCategoryID, true, true)
        elseif intBrandID <> "" and intCampaignID > 0 then
            set rsData = Inventory_GetItemsFromFolderEx5(intParentID,intCampaignID,intBrandId, true, true)
        elseif intProgramID = "" and intCampaignID > 0 then
            set rsData = Inventory_GetItemsFromCampaign(intParentID,intCampaignID, true, true)
        elseif intBrandID <> "" and intProgramID > 0 then
            set rsData = Inventory_GetItemsFromFolderEx3(intParentID,intProgramId,intBrandId, true, true)
        elseif intBrandID <> "" then
            set rsData = Inventory_GetItemsFromFolderEx(intParentID,intBrandId, true, true)
		elseif intProgramID > 0 and intCampaignID > 0 then
            set rsData = Inventory_GetItemsFromFolderEx7(intParentID,intProgramId,intCampaignId, true, true)
        elseif intProgramID > 0 then
            set rsData = Inventory_GetItemsFromFolderEx2(intParentID,intProgramId, true, true)
		elseif intCampaignID > 0 then
            set rsData = Inventory_GetItemsFromCampaign(intParentID,intCampaignID, intprofileid, true, true)
        else
            set rsData = Inventory_GetItemsFromFolder(-1, true, true)
        end if
    end if
else
	intParentID = 0
	strPageTitle = "Search Results: " & strKeywords
	set rsData = Inventory_GetItemsByKeyword(strKeywords)
end if

if Request("action") = "save-doc" Then
dim strSQL
if request("color") <> "" then
	strSQL = "UPDATE Athleta_GoDoc SET caption = '" & request("color") & "', Description='" & SQLEncode(Request("editor1")&"") & "' WHERE ID=" & Request("id")
else
	strSQL = "UPDATE Athleta_GoDoc SET Description='" & SQLEncode(Request("editor1")&"") & "' WHERE ID=" & Request("id")
end if
gobjConn.execute(strSQL)

Response.Redirect "../admin/main.asp"
End If



'Custom_GetGlobalInventoryFolders intParentID
Custom_GetGlobalInventoryFolders 0
if Left(Request("action"),5) = "edit_" Then
    DrawWYSIWYG
elseif Request("action") = "checkout" Then
	CheckInventoryAmt(gintUserID)
	if dctErrors.count > 0 then
		response.redirect "default.asp?lt=0"
	else
		DrawHeader strPageTitle, "storemain"
		DrawCheckout
		DrawFooter "storemain"
	end if 
elseif Request("action") = "history" Then 
    DrawHeader strPageTitle, "storemain"
    DrawHistory
    DrawFooter "storemain"
elseif Request("action") = "historydetail" Then 
    DrawHeader strPageTitle, "storemain"
    DrawHistoryDetail
    DrawFooter "storemain"
elseif Request("action") = "PDFhistorydetail" Then 
    DrawHeader strPageTitle, "storemain"
    DrawPDFHistoryDetail
    DrawFooter "storemain"
elseif Request("action") = "PODhistorydetail" Then 
    DrawHeader strPageTitle, "storemain"
    DrawPODHistoryDetail
    DrawFooter "storemain"
elseif Request("action") = "historyexport" Then 
    dim strFieldSep, strFieldQuote, strFieldAltQuote, strFileName, strData, strEOL, strGiftMessage
	dim strLine, strItem, strNote

    if Request("lt") = "1" or Request("lt") = "2" then
        strSQL = "SELECT O.intID,O.chrStatus,O.mnyGrandTotal,O.dtmSubmitted,B.vchLastName AS B_vchLastName,B.vchFirstName AS B_vchFirstName,B.vchCompany AS B_vchCompany,B.vchBusinessUnit AS [Business Unit],B.vchDivision AS B_vchDivision,B.vchEmail AS B_vchEmail,C.vchItemName as Campaign, Br.vchItemName as [Brand], Pr.vchItemName as [Program], Cat.vchItemName as [Category],L.vchPartNumber as L_vchPartNumber,L.vchItemName as L_vchItemName,L.mnyUnitPrice as L_mnyUnitPrice, L.intQuantity as L_intQuantity, (IsNull(L.intQuantity,0)* IsNull(L.mnyUnitPrice,0.0)) as L_mnySubTotal,H.intID AS H_intID,H.vchLastName AS H_vchLastName,H.vchFirstName AS H_vchFirstName,H.vchLabel AS H_vchCompany,H.vchEmail AS H_vchEmail,H.vchAddress1 AS H_vchAddress1,H.vchAddress2 AS H_vchAddress2,H.vchCity AS H_vchCity,H.vchState AS H_vchState,H.vchZip AS H_vchZip,H.vchDayPhone AS H_vchDayPhone,H.vchBusinessUnit AS H_vchBusinessUnit,H.vchDivision AS H_vchDivision,I.vchSpecifications AS [Item Specifications] FROM " & STR_TABLE_ORDER_LONG_TERM & " AS O LEFT JOIN " & STR_TABLE_LINEITEM_LONG_TERM & " AS L ON O.intID=L.intOrderID LEFT JOIN " & STR_TABLE_INVENTORY_LONG_TERM & " AS I ON I.intID=L.intInvID LEFT JOIN Athleta_Inv AS C ON C.intID=I.intCampaign LEFT JOIN Athleta_Inv AS Br ON Br.intID=I.intBrand LEFT JOIN Athleta_Inv AS Pr ON Pr.intID=I.intProgram LEFT JOIN Athleta_Inv AS Cat ON Cat.intID=I.intCategory LEFT OUTER JOIN Athleta_Shopper AS B ON O.intShopperID=B.intID LEFT OUTER JOIN Athleta_Shopper AS P ON O.intBillShopperID=P.intID LEFT OUTER JOIN Athleta_Shopper AS H ON O.intShipShopperID=H.intID WHERE O.intShopperID=" & gintUserID & " AND O.chrStatus IN ('P','S','H') ORDER BY O.intID DESC"
    else
        strSQL = "SELECT O.intID,O.chrStatus,O.mnyGrandTotal,O.dtmSubmitted,B.vchLastName AS B_vchLastName,B.vchFirstName AS B_vchFirstName,B.vchCompany AS B_vchCompany,B.vchBusinessUnit AS [Business Unit],B.vchDivision AS B_vchDivision,B.vchEmail AS B_vchEmail,C.vchItemName as Campaign, Br.vchItemName as [Brand], Pr.vchItemName as [Program], Cat.vchItemName as [Category],L.vchPartNumber as L_vchPartNumber,L.vchItemName as L_vchItemName,L.mnyUnitPrice as L_mnyUnitPrice, L.intQuantity as L_intQuantity, (IsNull(L.intQuantity,0)* IsNull(L.mnyUnitPrice,0.0)) as L_mnySubTotal,H.intID AS H_intID,H.vchLastName AS H_vchLastName,H.vchFirstName AS H_vchFirstName,H.vchLabel AS H_vchCompany,H.vchEmail AS H_vchEmail,H.vchAddress1 AS H_vchAddress1,H.vchAddress2 AS H_vchAddress2,H.vchCity AS H_vchCity,H.vchState AS H_vchState,H.vchZip AS H_vchZip,H.vchDayPhone AS H_vchDayPhone,H.vchBusinessUnit AS H_vchBusinessUnit,H.vchDivision AS H_vchDivision,I.vchSpecifications AS [Item Specifications] FROM Athleta_Order AS O LEFT JOIN Athleta_LineItem AS L ON O.intID=L.intOrderID LEFT JOIN Athleta_Inv AS I ON I.intID=L.intInvID LEFT JOIN Athleta_Inv AS C ON C.intID=I.intCampaign LEFT JOIN Athleta_Inv AS Br ON Br.intID=I.intBrand LEFT JOIN Athleta_Inv AS Pr ON Pr.intID=I.intProgram LEFT JOIN Athleta_Inv AS Cat ON Cat.intID=I.intCategory LEFT OUTER JOIN Athleta_Shopper AS B ON O.intShopperID=B.intID LEFT OUTER JOIN Athleta_Shopper AS P ON O.intBillShopperID=P.intID LEFT OUTER JOIN Athleta_Shopper AS H ON O.intShipShopperID=H.intID WHERE O.intShopperID=" & gintUserID & " AND O.chrStatus IN ('P','S','H') ORDER BY O.intID DESC"
    end if

    set rsData = gobjConn.execute(strSQL)
    strFileName = "orderexport.csv"
	strFieldSep = ","
	strFieldQuote = """"
	strFieldAltQuote = "'"
	strEOL = vbCrLf
    response.ContentType = "text/csv"
	response.AddHeader "Content-transfer-encoding", "binary"
	response.AddHeader "Content-Disposition", "attachment; filename=" & strFileName

	if strFieldQuote <> "" then
        
       Response.Write """POS #"",""Description"",""Program"",""Brand"",""Type,User"",""Qty"",""Cost"",""Total Cost"",""Distributor"",""Contact"",""Address"",""City"",""State"",""Zip"",""Dist#"",""Zone"",""BU""" & vbCrLf

		while not rsData.eof
			Response.Write """" & rsData("intID") & ""","
            Response.Write """" & rsData("L_vchItemName") & ""","
            Response.Write """" & rsData("Program") & ""","
            Response.Write """" & rsData("Brand") & ""","
            Response.Write """" & rsData("Category") & ""","
            Response.Write """" & rsData("B_vchEmail") & ""","
            Response.Write """" & rsData("L_intQuantity") & ""","
            Response.Write """" & rsData("L_mnyUnitPrice") & ""","
            Response.Write """" & rsData("L_mnySubTotal") & ""","
            Response.Write """" & rsData("H_vchCompany") & ""","
            Response.Write """" & rsData("H_vchFirstName") & " " & rsData("H_vchLastName") & ""","
            Response.Write """" & rsData("H_vchAddress1") & ""","
            Response.Write """" & rsData("H_vchCity") & ""","
            Response.Write """" & rsData("H_vchState") & ""","
            Response.Write """" & rsData("H_vchZip") & ""","
            Response.Write """" & rsData("H_intID") & ""","
            Response.Write """" & rsData("H_vchDivision") & ""","
            Response.Write """" & rsData("H_vchBusinessUnit") & ""","
            Response.Write vbCrLf
			rsData.MoveNext
		wend
	end if
    Response.End
elseIf request("pdf")&""<>"" Then
        DrawPdf
elseIf request("pdf-full")&""<>"" Then
        DrawPdfFull
elseIf request("pdf-order")&""<>"" Then
        DrawOrderPdf
elseif Request("action") = "training" Then 
    DrawHeader strPageTitle, "storemain"
    DrawTraining
    DrawFooter "storemain"
elseif Request("action") = "help" Then 
    DrawHeader strPageTitle, "storemain"
    DrawHelp
    DrawFooter "storemain"
elseif Request("action") ="checkoutthankyou" Then
    DrawHeader strPageTitle, "storemain"
    DrawThankYou
    DrawFooter "storemain"
elseif Request("action")="checkoutsubmit" Then
	
	if request("orderingName") = "" or request("orderingEmail") = "" then
		if request("orderingName") = ""  then
			dctCheckoutErrors.add "Name Missing", "Please add your name to this order"
		end if
		if request("orderingEmail") = ""  then
			dctCheckoutErrors.add "Email Missing", "Please add your email to this order"
		end if
		DrawHeader strPageTitle, "storemain"
		DrawCheckout
		DrawFooter "storemain"
		response.end
	end if

	CheckInventoryAmt(gintUserID)
	
	if dctErrors.count > 0 then
		response.redirect "default.asp?lt=" & request("lt")
	else
    
		
		' CreatePrintFinal
		strSQL = "UPDATE " & STR_TABLE_ORDER_LONG_TERM & " Set intShipOption=" & Request("shipping") & ",txtGiftMessage='" & SQLEncode(request("orderingName")) & " - " & request("orderingEmail") & "  " & SQLEncode(Request("notes")) & "'  WHERE intShopperID=" & gintUserID & " AND chrStatus NOT IN ('P','S','H','1')"
		'response.write strSQL
		'response.end
		gobjConn.execute(strSQL)
		
		CheckForApprovalItems(gintUserID)
	
		strSQL = "SELECT intID FROM " & STR_TABLE_ORDER_LONG_TERM & " WHERE intShopperID=" & gintUserID & " AND chrStatus NOT IN ('P','S','H','B','D','1')"
   
		set rsData = gobjConn.execute(strSQL)
		' if instr(request.servervariables("URL"), "/www/") = 0 and Session(SESSION_MERCHANT_UID)&"" <> "1" then
            ' While not rsData.Eof
                ' call AddOrder(rsData("intID")) 
                ' rsData.MoveNext
            ' Wend
        ' end if
		
		UpdateCheckedOutItems(gintUserID)

		strSQL = "UPDATE " & STR_TABLE_ORDER_LONG_TERM & " Set chrStatus='P', intShipOption=" & Request("shipping") & ",txtGiftMessage='" & SQLEncode(request("orderingName")) & " - " & request("orderingEmail") & " - " & SQLEncode(Request("notes")) & "'  WHERE intShopperID=" & gintUserID & " AND chrStatus NOT IN ('P','S','H','B','D','1')"
		gobjConn.execute(strSQL)
		
		' mred: added update to Abandon orders with no items to clean up admin lists
		strSQL = "UPDATE " & STR_TABLE_ORDER_LONG_TERM & " Set chrStatus='B' WHERE intShopperID=" & gintUserID & " AND chrStatus NOT IN ('P','S','H') AND intID NOT IN (SELECT DISTINCT intOrderID FROM " & STR_TABLE_LINEITEM_LONG_TERM & ")"
		gobjConn.execute(strSQL)

		Response.Redirect "?lt=" & request("lt") & "&action=checkoutthankyou"
	end if
else
    DrawHeader strPageTitle, "storemain"
	GetAddressBook(gintUserID)
    If request("print")&""<>"" Or request("print-full")&""<>"" Then
        DrawPrint
    ElseIf Request("lt")&""<>"" Then
        DrawLTPage
    Else
        DrawPage
    end if
    DrawFooter "storemain"
end if
rsData.close
set rsData = nothing
response.end

Sub DrawPDFFull
    Response.Redirect "../AspPdf/CatalogPDf.asp?campaignid=" & intCampaignID &"&CatalogId=all"
End Sub

Sub DrawPDF
    Response.Redirect "../AspPdf/CatalogPDf.asp?CatalogId=" & Session("Catalog")&""
End Sub

Sub DrawPDFOrder
    Response.Redirect "../AspPdf/CatalogPDf.asp?CatalogId=" & Session("Catalog")&""
End Sub



Sub DrawThankYou
%>
<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">
					Resource Center
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.asp">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Thank You</a>
						</li>
					</ul>
				</div>
			</div>
			
			<!-- BEGIN PUSH MESSAGES-->
			<div class="row">
			<div style="width:600px;margin:0 auto;">
                <h3>Thank You</h3>
                <p>
                We have received your order and will process it shortly.  If you have any questions, or need to change your order, please contact us.
                </p>
            <center><p>For Technical Support contact 866-213-7402 or <a href="mailto:support@domeprinting.com">technicalsupport@domeprinting.com</a><br>
				
				For Ordering Support contact Mr.Dome at <a href="mailto:support@domeprinting.com">orderingsupport@domeprinting.com</a></p></center>
</div>
</div>
            </div>
        </div>
    </div>

    
                    <%
    
End Sub

Sub DrawCheckout
%>
<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">
					Resource Center
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.asp">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Checkout</a>
						</li>
					</ul>
				</div>
			</div>
			
			<!-- BEGIN PUSH MESSAGES-->
			<div class="row">
                <div style="width:400px;margin:0 auto;">
					<% dim totalCost, podCost, rsTemp 
					strSQL = "select sum((I.mnyItemPrice * L.intQuantity)) as total FROM " & STR_TABLE_LINEITEM_LONG_TERM & " L JOIN " & STR_TABLE_ORDER_LONG_TERM & " O on O.intID=intOrderID JOIN " & STR_TABLE_INVENTORY_LONG_TERM & " I on L.intInvId=I.intID LEFT JOIN " & STR_TABLE_INVENTORY_LONG_TERM & " Pr ON Pr.intID=I.intProgram LEFT JOIN " & STR_TABLE_PROMO & " P on L.intInvId=P.intProductID  AND P.chrPromoCode=O.chrPromoCode WHERE O.intShopperID=" & Session(SESSION_PUB_USER_ID) & " AND L.chrStatus<>'D' and O.chrStatus = '0';"
					'response.write strSQL
					set rsTemp =  gobjConn.execute(strSQL)
					
					response.write "<p style=""font-size: 1.2em"">"
					
					if rsTemp("total")&"" <> "" then
                        if session(SESSION_MERCHANT_ULOGIN) <> "" then
                            response.write "<strong>Cart total: " & FormatCurrency(rsTemp("total")) & "</strong><br />"
                        else 
                            response.write "<strong>Cart total: " & FormatCurrency(rsTemp("total")) & "</strong><br />"
                        end if
					end if
					
					strSQL = "select sum((I.mnyItemPrice * L.intQuantity)) as total FROM " & STR_TABLE_LINEITEM_LONG_TERM & " L JOIN " & STR_TABLE_ORDER_LONG_TERM & " O on O.intID=intOrderID JOIN " & STR_TABLE_INVENTORY_LONG_TERM & " I on L.intInvId=I.intID LEFT JOIN " & STR_TABLE_INVENTORY_LONG_TERM & " Pr ON Pr.intID=I.intProgram LEFT JOIN " & STR_TABLE_PROMO & " P on L.intInvId=P.intProductID  AND P.chrPromoCode=O.chrPromoCode WHERE L.intInternalID is not null and O.intShopperID=" & Session(SESSION_PUB_USER_ID) & " AND L.chrStatus<>'D' and O.chrStatus = '0'"
					set rsTemp =  gobjConn.execute(strSQL)
					
					if rsTemp("total")&"" <> "" then
                        if session(SESSION_MERCHANT_ULOGIN) <> "" then
                            response.write "<strong>POD Total: " & FormatCurrency(rsTemp("total")) & "</strong><br />"
                        else 
                            response.write "<strong>Chargeable to store: " & FormatCurrency(rsTemp("total")) & "</strong><br />"
                        end if
                    else 
                        if session(SESSION_MERCHANT_ULOGIN) <> "" then
                            response.write "<strong>POD Total: $0.00</strong><br />"
                        else 
                            response.write "<strong>Chargeable to store: $0.00</strong><br />"
                        end if
					end if
					%>
                    <form action="default.asp">
                        <input type="hidden" name="action" value="checkoutsubmit" />
                        <div>
							<input type="text" name="orderingName" placeholder="Your Name" style="width:400px;margin-bottom:10px" maxlength="35" /><br />
							<input type="text" name="orderingEmail" placeholder="Your Email" style="width:400px;margin-bottom:5px" maxlength="35" /><br />
                            <label style="display:inline-block;margin-right:10px;font-size:larger;">Shipping Method</label>
                            <select name="shipping">
                                
                                <option value="1">FedEx Express Saver</option>
                            </select>
                        </div>
                        <div>
                            <div style="font-size:larger;">Special Instructions</div>
							<textarea name="notes" style="width:400px;height:200px;" onkeyup="countChar(this)"></textarea>
							<div id="charNum"></div>
                        </div>
						
						<% if dctCheckoutErrors.count > 0 then %> 
							<div id=errors" style="color: red"><strong>
							<%
								dim a, x
								a = dctCheckoutErrors.Items
								for x = 0 to dctCheckoutErrors.count - 1
									response.write a(x) & "<br/>"
								next %></strong>
							</div>
						<% end if %>
                       
                        <div>
                            <input type="submit" value="Submit Order" />
                        </div> 
                        <!--<div style="text-align:center;margin-top:10px;">
                            If this order is placed and approved prior to 1:00 pm PST are eligible for same day shipping. Orders placed and approved after 1:00 pm PST will ship the following business day.
                        </div>-->
                    </form>
                </div>
            </div>
        </div>
    </div>
<script>
	function countChar(val) {
		var len = val.value.length;
        if (len >= 180) {
          val.value = val.value.substring(0, 180);
		  $('#charNum').text('0 of 180 char left');
        } else {
          $('#charNum').text((180 - len) + ' of 180 char left');
        }
    };
</script>
    
                    <%
End Sub

Sub DrawHelp
%>
<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">
					Resource Center
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.asp">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Help</a>
						</li>
					</ul>
				</div>
			</div>
			
			<!-- BEGIN PUSH MESSAGES-->
			<div class="row">
				<div style="width:600px;margin:0 auto;">
					<table width="500">
						<tbody><tr>
						<td><img src="../images/email.png" width="50"></td>
						<td colspan="3" style="font-size:12pt;font-weight:bold;text-align: center;">E-mail & Phone Support</td>
						<td><img src="../images/phone.png" width="50"></td>
						</tr>

								
						</tbody></table>
					<br>
					<p>For Technical Support contact 866-213-7402 or <a href="mailto:technicalsupport@domeprinting.com">technicalsupport@domeprinting.com</a><br>
					For Ordering Support contact Mr.Dome at <a href="orderingsupport@domeprinting.com">orderingsupport@domeprinting.com</a></p>
				</div>
			</div>

        </div>
    </div>

    
                    <%
    
End Sub

Sub DrawTraining
%>
<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">
					Training and FAQs
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.asp">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Training and FAQs</a>
						</li>
					</ul>
				</div>
			</div>
			<script>
			    function vimeoLoadingThumb(id) {
			        var url = "http://vimeo.com/api/v2/video/" + id + ".json?callback=showThumb";

			        var id_img = "#vimeo-" + id;

			        var script = document.createElement('script');
			        script.type = 'text/javascript';
			        script.src = url;

			        $(id_img).before(script);
			    }


			    function showThumb(data) {
			        var id_img = "#vimeo-" + data[0].id;
			        $(id_img).attr('src', data[0].thumbnail_medium);
			    }

            </script>
			<!-- BEGIN PUSH MESSAGES-->
			<div class="row">
                <!--<h3>Training and FAQs</h3>-->
                <table width="100%">
                    <tr>
                        <td width="75%">
                          
              
                        </td>
						<td valign="top" style="padding:20px;" width="25%" rowspan="2">
                            <table>
                                
                                  
</tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="75%" style="width:75%;padding:20px;padding-bottom:100px;border-right:1px solid #ccc;" valign="top">
                        <%
                        strSQL  = "SELECT *   FROM Athleta_GODOC WHERE STITLE='FAQ'" 
                        dim rsorders
                        set rsOrders = gobjConn.execute(strSQL)
                        %>  
                        <%= rsOrders("Description") %>
                        </td>
                        
                    </tr>
                </table>
            </div>
        </div>
    </div>

    
                    <%
    
End Sub


Sub DrawFiles
%>
<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">
					Resource Center
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.asp">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">FAQ & Training</a>
						</li>
					</ul>
				</div>
			</div>
			
			<!-- BEGIN PUSH MESSAGES-->
			<div class="row">
                

                <%
                    Dim rsOrders, strSQL

                    strSQL  = "SELECT *   FROM " & STR_TABLE_INVENTORY & " WHERE chrType='L' AND chrStatus='A'"
                    set rsOrders = gobjConn.execute(strSQL)

                    %>
                   
                    <%

                    if rsOrders.eof then
                    %>
                    <tr>
                        <td colspan="3">No files Found</td>
                    </tr>
                    <%
                    end if

                    while not rsOrders.Eof


                    %>
                        <div style="width:100px;height:80px;float:left;">
					        <div class="dashboard-stat">
                            <h2><%= rsOrders("vchItemName") %></h2>
                            <% 
                                dim strPhotoURL

                                strPhotoURL = "../images/products/big/" & rsOrders("vchImageURL")
                                %>
                    
                                  <% if strPhotoURL&""="../images/products/big/" then strPhotoURL = virtualbase & "images/icon-no-image-512.png" %>
                       
                                <div style="background-image:url(<%= strPhotoURL %>);margin-bottom:20px;width:100px;height:80px;background-repeat:no-repeat;background-size:cover;boder:1px solid #ccc;">
                
                                </div>
						       <a href="../files/<%=rsOrders("vchSoftURL") %>">Download</a>
					        </div>
				        </div>
                    <%
                        rsOrders.MoveNext
                    Wend
                    %>
            </div>
        </div>
    </div>

    
                    <%
    
End Sub


Sub DrawHistory
%>
<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">
					Resource Center
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.asp">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Order History</a>
						</li>
					</ul>
				</div>
			</div>
			<!-- END PAGE HEADER-->
			

			
			 <div class="row">
				<div>
				
					
				    <h2 style="margin-left:140px;">Custom PDF Downloads</h2>
					<!-- BEGIN BANNER-->
					<div class="portlet-body" style="text-align:center">
						<% 'PODOrderHistory 
						Dim rsOrders, strSQL, rsCount, rsPODOrders, UPStatus
						
						strSQL = "Select T.*, P.vchName from " & TABLE_PRODUCT_TICKETS & " T left join " & TABLE_PRODUCT_TABLE & " P on T.intUStoreID = P.intUStoreID WHERE T.chrStatus = 'L' and intShopperID = " & Session(SESSION_PUB_USER_ID) & " ORDER BY T.dtmUpdated DESC"
						'response.write strSQL
						set rsPODOrders = gobjConn.execute(strSQL)
						%>
						
						 <table class="CSSTableGenerator">
                    <tr>
                        <th style="width"20px"></th><th>ID</th><th>Item Name</th><th>Create Date</th>
                    </tr>
                    <%

                    if rsPODOrders.eof then
                    %>
                    <tr>
                        <td colspan="4">No Orders Found</td>
                    </tr>
                    <%
                    end if

                    while not rsPODOrders.Eof
                    %>
                        <tr>
                            <td><a href="default.asp?action=PDFhistorydetail&id=<%=rsPODOrders("intID") %>&lt=1">View</a></td>
                            <td><%=rsPODOrders("intID") %></td>
							<td><%=rsPODOrders("vchName") %></td>
                            <td><%= rsPODOrders("dtmUpdated") %></td>
                        </tr>

                    <%
                        rsPODOrders.MoveNext
                    Wend
                    %>
                    </table>
					</div>
					</br>
					<!-- END BANNER-->
				</div>
			</div>

					
			
            <!--<div class="row">
				<div>
				
					
				    <h2 style="margin-left:140px;">Customized Print/Print On Demand Orders</h2>
					<!-- BEGIN BANNER-
					<div class="portlet-body" style="text-align:center">
						<% 'PODOrderHistory 
						
						strSQL = "Select L.vchItemName, O.intID, O.dtmUpdated, S.vchLabel, S.vchFirstName, S.vchLastName, S.vchCity, S.vchState,L.intuProduceTicket from " & STR_TABLE_LINEITEM_LONG_TERM & " L left join " & STR_TABLE_ORDER & " O on L.intOrderID = O.intID left join " & STR_TABLE_SHOPPER & " S ON O.intShipShopperID=S.intID WHERE O.intid = 17473 ORDER BY O.dtmCreated DESC"
						'L.intInternalID is not null and O.intShopperID = " & Session(SESSION_PUB_USER_ID) & " and O.chrStatus= 'S'
						'response.write strSQL
						set rsPODOrders = gobjConn.execute(strSQL)
						%>
						
						 <table class="CSSTableGenerator">
                    <tr>
                        <th style="width"20px"></th><th>ID</th><th>Store</th><th>Order Date</th><th>Print Status</th><th>Status</th><th>Tracking Number</th>
                    </tr>
                    <%

                    if rsPODOrders.eof then
                    %>
                    <tr>
                        <td colspan="7">No Orders Found</td>
                    </tr>
                    <%
                    end if

                    while not rsPODOrders.Eof
                    %>
                        <tr>
                            <td><a href="default.asp?action=PODhistorydetail&id=<%=rsPODOrders("intID") %>&lt=1">View</a></td>
                            <td><%=rsPODOrders("intID") %></td>
                            <td><%= rsPODOrders("vchLabel") %><br /><%= rsPODOrders("vchFirstName") %>&nbsp;<%= rsPODOrders("vchLastName") %><br /><%= rsPODOrders("vchCity") %>,&nbsp;<%= rsPODOrders("vchState") %></td>
                            <td><%= rsPODOrders("dtmUpdated") %></td>
							<td>
							<% 
							if true = false then
							UPStatus = getUProduceStatus(rsPODOrders("intuProduceTicket")&"")
								select case UPStatus
									case "1"
										response.write "Waiting"
									case "2"
										response.write "In Progress"
									case "3"
										response.write "Done"
									case "4"
										response.write "Fail"
									case "5"
										response.write "Suspended"
									case "6"
										response.write "Aborted"
									case "7"
										response.write "Online"
								end select	
							end if
							%>
							</td>
                            <td>
                            <%
                                set dctOrder = GetOrder(rsPODOrders("intID"))
                                If dctOrder("Unapproved")&""="true" Then
                                    Response.Write "[Unapproved] "
                                End If
                                If dctOrder("Unprocessed")&""="true" Then
                                    Response.Write "[Unprocessed] "
                                End If
                                If dctOrder("Accepted")&""="true" Then
                                    Response.Write "[Accepted] "
                                End If
                                If dctOrder("Hold")&""="true" Then
                                    Response.Write "[Hold] "
                                End If
                                If dctOrder("DeniedCredit")&""="true" Then
                                    Response.Write "[Denied Credit] "
                                End If
                                If dctOrder("Processed")&""="true" Then
                                    Response.Write "[Processed] "
                                End If
                                If dctOrder("Pending")&""="true" Then
                                    Response.Write "[Pending] "
                                End If
                                If dctOrder("Picked")&""="true" Then
                                    Response.Write "[Picked] "
                                End If
                                If dctOrder("Backordered")&""="true" Then
                                    Response.Write "[Backordered] "
                                End If
                                If dctOrder("Shipped")&""="true" Then
                                    Response.Write "[Shipped] "
                                End If
                                If dctOrder("Canceled")&""="true" Then
                                    Response.Write "[Canceled] "
                                End If
                                If dctOrder("Complete")&""="true" Then
                                    Response.Write "[Complete]"
                                End If
                            
                             %>
                            </td>
                            <td>
                                 <%= iif(dctOrder("TrackingId")&""<>"", dctOrder("TrackingId"),"None Provided or Not Shipped Yet")%>
                            </td>
                        </tr>

                    <%
                        rsPODOrders.MoveNext
                    Wend
                    %>
                    </table>
					</div>
					</br>
					<!-- END BANNER-
				</div>
			</div>-->
 <!--PREBUY ORDER HISTORY SECTION START-->
            <!--<div class="row">
				<div>
					
				    <h2 style="margin-left:140px;">Pre-Buy Orders</h2>
					<!-- BEGIN BANNER--
					<div class="portlet-body" style="text-align:center">
						Orders for an open pre-buy will not be shown. To see current pre-buy items, <a href="default.asp">click here</a>.
					</div>
					<!-- END BANNER--
				</div>
			</div>
            <div class="row" style="width:876px;margin:0 auto;margin-bottom:-25px;text-align:right;">
                <a href="default.asp?action=historyexport"><img src="../images/export.png" width="30" />Export All</a>
            </div>
			<!-- BEGIN PUSH MESSAGES--
			<div class="row">
                

                <%
                    Dim rsPBOrders

                    strSQL  = "SELECT O.intID,O.dtmUpdated, S.vchLabel, S.vchFirstName, S.vchLastName, S.vchCity, S.vchState   FROM " & STR_TABLE_ORDER & " O LEFT JOIN " & STR_TABLE_SHOPPER & " S ON O.intShipShopperID=S.intID WHERE O.intShopperId=" & gintUserID & " AND O.chrStatus in ('S') AND intShipShopperID is not null ORDER BY O.dtmCreated DESC"
                    set rsPBOrders = gobjConn.execute(strSQL)

                    
                    %>
                    
                    <table class="CSSTableGenerator">
                    <tr>
                        <th></th><th>ID</th><th>Distributor</th><th>Date Ordered</th>
                    </tr>
                    <%

                    if rsPBOrders.eof then
                    %>
                    <tr>
                        <td colspan="4">No Orders Found</td>
                    </tr>
                    <%
                    end if

                    while not rsPBOrders.Eof
                        strSQL = "SELECT count(*) FROM " & STR_TABLE_LINEITEM  & " WHERE intOrderID=" & rsPBOrders("intID")
                        set rsCount = gobjConn.execute(strSQL)
                        if rsCount(0)>0 then
                    %>
                        <tr>
                            <td><a href="default.asp?action=historydetail&id=<%=rsPBOrders("intID") %>">View</a></td>
                            <td><%=rsPBOrders("intID") %></td>
                            <td><%= rsPBOrders("vchLabel") %><br /><%= rsPBOrders("vchFirstName") %>&nbsp;<%= rsPBOrders("vchLastName") %><br /><%= rsPBOrders("vchCity") %>,&nbsp;<%= rsPBOrders("vchState") %></td>
                            <td><%= rsPBOrders("dtmUpdated") %></td>
                        </tr>

                    <%
                        end if
                        rsPBOrders.MoveNext
                    Wend
                    %>
                    </table>
            </div>
<!--PREBUY ORDER HISTORY END-->	          
			<!-- BEGIN PUSH MESSAGES-->
			<div class="row">
                

                <%
                    strSQL  = "SELECT O.intID,O.dtmUpdated, S.vchLabel, S.vchFirstName, S.vchLastName, S.vchCity, S.vchState   FROM " & STR_TABLE_ORDER & " O LEFT JOIN " & STR_TABLE_SHOPPER & " S ON O.intShipShopperID=S.intID WHERE O.intShopperId=" & gintUserID & " AND O.chrStatus in ('S') AND intShipShopperID is not null ORDER BY O.dtmCreated DESC"
                    set rsOrders = gobjConn.execute(strSQL)

                    
                    %>
                    
                    
            </div>

            <!-- END PAGE HEADER-->
            <div class="row">
				<div>
					
				    <h2 style="margin-left:140px;">Inventory Orders</h2>
					<!-- BEGIN BANNER-->
					<div class="portlet-body" style="text-align:center">
						Inventory Orders that have not been completed will not be shown. To see your current shopping cart, <a href="default.asp?lt=1">click here</a>.
					</div>
					<!-- END BANNER-->
				</div>
			</div>
            <div class="row" style="width:876px;margin:0 auto;margin-bottom:-25px;text-align:right;">
                <a href="default.asp?action=historyexport&lt=1"><img src="../images/export.png" width="30" />Export All</a>
            </div>
			<br clear="all" />
			<br clear="all" />
			<!-- BEGIN PUSH MESSAGES-->
			<div class="row">
                

                <%
                   Dim dctOrder
                    strSQL  = "SELECT TOP 10 O.intID,O.dtmUpdated, S.vchLabel, S.vchFirstName, S.vchLastName, S.vchCity, S.vchState FROM " & STR_TABLE_ORDER_LONG_TERM & " O LEFT JOIN " & STR_TABLE_SHOPPER & " S ON O.intShipShopperID=S.intID WHERE O.chrStatus in ('S','P') AND intShipShopperID is not null and O.intID not in (17473, 500369) ORDER BY o.dtmCreated DESC"
                    ' response.write strsql
                    set rsOrders = gobjConn.execute(strSQL)

                    %>
                    
                    <table class="CSSTableGenerator">
                    <tr>
                        <th style="width"20px"></th><th>ID</th><th>Store</th><th>Order Date</th><th>Status</th><th>Tracking Number</th>
                    </tr>
                    <%

                    if rsOrders.eof then
                    %>
                    <tr>
                        <td colspan="6">No Orders Found</td>
                    </tr>
                    <%
                    end if

                    while not rsOrders.Eof
                     strSQL = "SELECT count(*) FROM " & STR_TABLE_LINEITEM_LONG_TERM  & " WHERE intOrderID=" & rsOrders("intID")
                      ' response.write strsql : response.end
                        set rsCount = gobjConn.execute(strSQL)
                        if rsCount(0)>0 then
                    %>
                        <tr>
                            <td><a href="default.asp?action=historydetail&id=<%=rsOrders("intID") %>&lt=1">View</a></td>
                            <td><%=rsOrders("intID") %></td>
                            <td><%= rsOrders("vchLabel") %><br /><%= rsOrders("vchFirstName") %>&nbsp;<%= rsOrders("vchLastName") %><br /><%= rsOrders("vchCity") %>,&nbsp;<%= rsOrders("vchState") %></td>
                             <%
                                set dctOrder = GetOrder(rsOrders("intID")) %>
							
							<td><%= rsOrders("dtmUpdated") %></td>
                            <td>
                            <%
                                
                                If dctOrder("Unapproved")&""="true" Then
                                    Response.Write "[Unapproved] "
                                End If
                                'If dctOrder("Unprocessed")&""="true" Then
								if rsOrders("intID") = "500420" or rsOrders("intID") = "500418" or rsOrders("intID") = "500419" or rsOrders("intID") = "500416" or rsOrders("intID") = "500417" or rsOrders("intID") = "500414" or rsOrders("intID") = "500415" or rsOrders("intID") = "500411" or rsOrders("intID") = "500412" or rsOrders("intID") = "500413" then
									  Response.Write "[Shipped] "
								else
                                    Response.Write "[Unprocessed] "
                                End If
                                If dctOrder("Accepted")&""="true" Then
                                    Response.Write "[Accepted] "
                                End If
                                If dctOrder("Hold")&""="true" Then
                                    Response.Write "[Hold] "
                                End If
                                If dctOrder("DeniedCredit")&""="true" Then
                                    Response.Write "[Denied Credit] "
                                End If
                                If dctOrder("Processed")&""="true" Then
                                    Response.Write "[Processed] "
                                End If
                                If dctOrder("Pending")&""="true" Then
                                    Response.Write "[Pending] "
                                End If
                                If dctOrder("Picked")&""="true" Then
                                    Response.Write "[Picked] "
                                End If
                                If dctOrder("Backordered")&""="true" Then
                                    Response.Write "[Backordered] "
                                End If
                                If dctOrder("Shipped")&""="true" Then
                                    Response.Write "[Shipped] "
                                End If
                                If dctOrder("Canceled")&""="true" Then
                                    Response.Write "[Canceled] "
                                End If
                                If dctOrder("Complete")&""="true" Then
                                    Response.Write "[Complete]"
                                End If
                            
                             %>
                            </td>
                            <td>
								<% if rsOrders("intID") = "500420" then %>
									<a href="#" style="text-decoration: underline">1Z416546545676574667</a>
								<% elseif rsOrders("intID") = "500418" then %>
									<a href="#" style="text-decoration: underline">1Z416546545676575677</a>
								<% elseif rsOrders("intID") = "500419" then %>
									<a href="#" style="text-decoration: underline">1Z418546545676574678</a>
								<% elseif rsOrders("intID") = "500416" then %>
									<a href="#" style="text-decoration: underline">1Z616546545676574686</a>
								<% elseif rsOrders("intID") = "500417" then %>
									<a href="#" style="text-decoration: underline">1Z466546545676594676</a>
								<% elseif rsOrders("intID") = "500414" then %>
									<a href="#" style="text-decoration: underline">1Z916545545676574676</a>
								<% elseif rsOrders("intID") = "500415" then %>
									<a href="#" style="text-decoration: underline">1Z416546545876576676</a>
								<% elseif rsOrders("intID") = "500411" then %>
									<a href="#" style="text-decoration: underline">1Z416546545676524736</a>
								<% elseif rsOrders("intID") = "500412" then %>
									<a href="#" style="text-decoration: underline">1Z247536545676574676</a>
								<% elseif rsOrders("intID") = "500413" then %>
									<a href="#" style="text-decoration: underline">1Z416546545676574676</a>
								<% else %>
                                 <%= iif(dctOrder("TrackingId")&""<>"", dctOrder("TrackingId"),"None Provided or Not Shipped Yet")%>
								<% end if %>
                            </td>
                        </tr>

                    <%
                        end if
                        rsOrders.MoveNext
                    Wend
                    %>
                    </table>
            </div>
        </div>
    </div>

    
                    <%
    
End Sub

Sub DrawHistoryDetail
    Dim rsData, orderID,mnySubtotal, rsOrders

    orderID = Clng(Request("id"))
    if Request("lt")="1" then
        set rsData = GetOrderLineItems_OtherLt(orderID)
    else
        set rsData = GetOrderLineItems_Other(orderID)
    end if
	
	Dim dctOrder
	if request("lt") = "1" or request("lt") = "2" then
		strSQL  = "SELECT O.intID,O.dtmUpdated, S.vchLabel, S.vchFirstName, S.vchLastName, S.vchCity, S.vchState   FROM " & STR_TABLE_ORDER_LONG_TERM & " O LEFT JOIN " & STR_TABLE_SHOPPER & " S ON O.intShipShopperID=S.intID WHERE O.chrStatus in ('S','P') AND intShipShopperID is not null AND O.IntID = "& orderID & " ORDER BY o.dtmCreated DESC"
	else
		strSQL  = "SELECT O.intID,O.dtmUpdated, S.vchLabel, S.vchFirstName, S.vchLastName, S.vchCity, S.vchState   FROM " & STR_TABLE_ORDER & " O LEFT JOIN " & STR_TABLE_SHOPPER & " S ON O.intShipShopperID=S.intID WHERE O.intShopperId=" & gintUserID & " AND O.chrStatus in ('S') AND intShipShopperID is not null  AND O.IntID = "& orderID & " ORDER BY O.dtmCreated DESC"
    end if
    set rsOrders = gobjConn.execute(strSQL)
%>
<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">
					Resource Center
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.asp">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="default.asp?action=history">Order Detail</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Order ID <%= orderID %></a>
						</li>
					</ul>
				</div>
			</div>
            <div class="row">
            <table border="0" cellpadding="0" cellspacing="0" style="margin:0 auto;width:800px;">
	        <tr>
                <td colspan="3"><a href="default.asp?action=history"><strong>Return to list</strong></a>
            </td>
			<tr>
				<td colspan="3"> &nbsp; </td>
			</tr>
			
			<tr>
				<td colspan="3"><strong>Order Id:</strong> <%= orderID %><br />
				<strong>Store:</strong><br /><%= rsOrders("vchLabel") %><br /><%= rsOrders("vchFirstName") %>&nbsp;<%= rsOrders("vchLastName") %><br /><%= rsOrders("vchCity") %>,&nbsp;<%= rsOrders("vchState") %><br / >
                <strong>Order Date:</strong> <%= rsOrders("dtmUpdated") %><br />
				<% if request("lt") = "1" then %>
					<strong>Status:</strong>
					<%
					set dctOrder = GetOrder(rsOrders("intID"))
					If dctOrder("Unapproved")&""="true" Then
						Response.Write "[Unapproved] "
					End If
					If dctOrder("Unprocessed")&""="true" Then
						Response.Write "[Unprocessed] "
					End If
					If dctOrder("Accepted")&""="true" Then
						Response.Write "[Accepted] "
					End If
					If dctOrder("Hold")&""="true" Then
						Response.Write "[Hold] "
					End If
					If dctOrder("DeniedCredit")&""="true" Then
						Response.Write "[Denied Credit] "
					End If
					If dctOrder("Processed")&""="true" Then
						Response.Write "[Processed] "
					End If
					If dctOrder("Pending")&""="true" Then
						Response.Write "[Pending] "
					End If
					If dctOrder("Picked")&""="true" Then
						Response.Write "[Picked] "
					End If
					If dctOrder("Backordered")&""="true" Then
						Response.Write "[Backordered] "
					End If
					If dctOrder("Shipped")&""="true" Then
						Response.Write "[Shipped] "
					End If
					If dctOrder("Canceled")&""="true" Then
						Response.Write "[Canceled] "
					End If
					If dctOrder("Complete")&""="true" Then
						Response.Write "[Complete]"
					End If
								
					%>
					<br />
					<strong>Tracking:</strong> <%= iif(dctOrder("TrackingId")&""<>"", dctOrder("TrackingId"),"None Provided or Not Shipped Yet")%>
				<% end if %>
				</td>
			<tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
	       <!-- <%
	        mnySubtotal = 0
	       
	        while not rsData.eof
                dim strPhotoURL, vchPartNumber, vchProgram, vchItemName, txtDescription, mnyUnitPrice, vchValidStates, vchBundleQuantity, vchExpireDate, chrStatus
                    txtDescription = rsData("txtDescription")
                    vchBundleQuantity = rsData("vchBundleQuantity")
                    vchExpireDate = rsData("vchExpireDate")
                    vchValidStates = rsData("vchValidStates")
                    vchPartNumber = rsData("vchPartNumber")
                    vchProgram = rsData("vchProgram")
                    vchItemName = rsData("vchItemName")
                    chrStatus= rsData("chrStatus")
	                mnyUnitPrice = "0"&rsData("mnyUnitPrice")
                    

		        %>
	        <tr class="cartitem">
		        <td valign="top" class="cartitem-image">
                    <% 
                    
                    
                   

                    strPhotoURL = "../images/products/big/" & rsData("vchImageURL")
                    %>
                    
                      <% if strPhotoURL&""="../images/products/big/" then strPhotoURL = virtualbase & "images/icon-no-image-512.png" %>
                       
                    <div style="background-image:url(<%= strPhotoURL %>);margin-bottom:20px;width:100px;height:80px;background-repeat:no-repeat;background-size:cover;">
                        <%=iif(chrStatus="C","<img src=""../images/cancelled.jpg"" height=""80"" />","") %>
                    </div>
                    
                </td>
                <td valign="top" class="cartitem-data">
                    <table cellpadding="1" cellspacing="1">
                        <tr align="left">
                            <td colspan="2" style="font-weight:bold;font-size:larger;"><%= vchItemName %></td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr align="left">
                            <td>Part Number:</td>
                            <td><%= vchPartNumber %></td>
                        </tr>
                        <tr align="left">
                            <td>Program:</td>
                            <td><%= vchProgram %></td>
                        </tr>
                        
                        <!--
                         <tr align="left">
                            <td>Cost/Bundle:</td>
                            <td><%= FormatCurrency(mnyUnitPrice) %></td>
                        </tr>
                        <tr align="left">
                            <td>Bundle Quantity:</td>
                            <td><%= vchBundleQuantity %></td>
                        </tr>
                        <tr align="left">
                            <td>Valid States:</td>
                            <td><%= vchValidStates %></td>
                        </tr>
                        
                        <tr align="left">
                            <td>Expire Date:</td>
                            <td><%= vchExpireDate  %></td>
                        </tr>
                        
                        <tr class="no-print">
                            <td>Quantity</td>
                            <td>
                                    <%= rsData("intQuantity") %>
                               
                            </td>
                        </tr>
                        
                    </table>
                </td>
                <td> 
                    <%if chrStatus = "C" then %>
                    This item was cancelled.
                    <% end if %>
                    <%if chrStatus = "S" then %>
                    This item was substituted for <%= rsData("vchShippingNUmber") %> .
                    <% end if %>
                </td>
	        </tr>
	            <%
                if chrStatus<>"C" then 
                    mnySubTotal = mnySubTotal + (CDBL("0"&rsData("mnyUnitPrice"))* rsData("intQuantity"))
                end if
                rsData.MoveNext
	        wend
	        %>-->
	        <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
	        <!--<tr class="no-print">
                <td style="font-weight:bold;font-size:larger;padding-bottom:20px;border-bottom:1px solid #ccc;">Sub Total:</td><td style="font-weight:bold;font-size:larger;border-bottom:1px solid #ccc;padding-bottom:20px;"><%= FormatCurrency(mnySubTotal) %></td><td style="font-weight:bold;font-size:larger;border-bottom:1px solid #ccc;padding-bottom:20px;"></td>
            </tr>-->
	        
	        </table>

			<table class="CSSTableGenerator" id="HistoryTable">
                    
            </table>
			<script type="text/javascript">
				$(document).ready(function () {
					OrderHistory(<%=orderID%>);
				});
			</script>
            </div>
        </div>
    </div>
<%
End Sub

Sub DrawPDFHistoryDetail
    Dim rsData, orderID,mnySubtotal, rsOrders

    orderID = Clng(Request("id"))
	
	strSQL = "Select * from " & TABLE_PRODUCT_TICKETS & " where intID = " & orderID
	'response.write strSQL
	set rsData = gobjConn.execute(strSQL)
	
%>
<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">
					Resource Center
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.asp">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="default.asp?action=history">PDF Download Detail</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Order ID <%= orderID %></a>
						</li>
					</ul>
				</div>
			</div>
            <div class="row">
            <table border="0" cellpadding="0" cellspacing="0" style="margin:0 auto;width:1100px;">
	        <tr>
                <td colspan="3"><a href="default.asp?action=history"><strong>Return to list</strong></a>
            </td>
			<tr>
				<td colspan="3"> &nbsp; </td>
			</tr>
			
			<tr>
				<td colspan="3" style="background-color:#f7f7f7;padding:15px;"><strong>Order Id:</strong> <%= orderID %><br />
				<strong>Created Date:</strong> <%= rsData("dtmUpdated") %><br />
				</td>
			<tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
	        <%
	        mnySubtotal = 0
	       
	        while not rsData.eof
                dim intPrintJobTicketID
                intPrintJobTicketID = rsData("intPrintJobTicketID")
	                

		        %>
	        <tr class="cartitem">
			
		        <td valign="top" class="cartitem-image" width="33%">
                    <% 
                    
                    
                   

                    'strPhotoURL = "../images/products/big/" & rsData("vchImageURL")
                    %>
                    
                      <% 'if strPhotoURL&""="../images/products/big/" then strPhotoURL = virtualbase & "images/icon-no-image-512.png" 
					  %>
                    <iframe src="../drcpod/js/web/viewer.html?proofURL=<%=Server.URLEncode(getDownloadURL(intPrintJobTicketID))%>" width="100%" height="300px"></iframe><br />
					<center><a href="#" onclick="window.open('../drcpod/js/web/viewer.html?proofURL=<%=Server.URLEncode(getDownloadURL(intPrintJobTicketID))%>', '_blank'); return false">View Large Proof</a></center>
                    <!--<div style="margin:0px auto;padding:0px; position:relative;width:100px;height:80px;background-repeat:no-repeat;background-size:cover;">
					
					
                        <%=iif(chrStatus="C","<img src=""../images/cancelled.jpg"" height=""80"" />","") %>
                    </div>-->
                    
                </td>
                <td> 
					&nbsp;
                </td>
	        </tr>
	            <%
                rsData.MoveNext
	        wend
	        %>
	        <tr>
                <td colspan="3"></td>
            </tr>
	        
	        
	        </table>


            </div>
        </div>
    </div>
<%
End Sub

Sub DrawPODHistoryDetail
    Dim rsData, orderID,mnySubtotal, rsOrders

    orderID = Clng(Request("id"))
    if Request("lt")="1" then
        set rsData = GetOrderLineItems_OtherLt(orderID)
    else
        set rsData = GetOrderLineItems_Other(orderID)
    end if
	
	strSQL = "Select  100 as vchBundleQuantity, L.vchPartNumber, L.vchItemName, 'A' as chrStatus, T.intProofJobTicketID from " & STR_TABLE_LINEITEM_LONG_TERM & " L left join " & TABLE_PRODUCT_TICKETS & " T on L.intInternalID = T.intID where intOrderID = " & orderID
	'response.write strSQL
	set rsData = gobjConn.execute(strSQL)
	
	Dim dctOrder
	if request("lt") = "1" then
		strSQL  = "SELECT O.intID,O.dtmUpdated, S.vchLabel, S.vchFirstName, S.vchLastName, S.vchCity, S.vchState   FROM " & STR_TABLE_ORDER_LONG_TERM & " O LEFT JOIN " & STR_TABLE_SHOPPER & " S ON O.intShipShopperID=S.intID WHERE O.intShopperId=" & gintUserID & " AND O.chrStatus in ('S','P','1') AND O.IntID = "& orderID & " ORDER BY o.dtmCreated DESC"
		'intShipShopperID is not null AND 
	else
		strSQL  = "SELECT O.intID,O.dtmUpdated, S.vchLabel, S.vchFirstName, S.vchLastName, S.vchCity, S.vchState   FROM " & STR_TABLE_ORDER & " O LEFT JOIN " & STR_TABLE_SHOPPER & " S ON O.intShipShopperID=S.intID WHERE O.intShopperId=" & gintUserID & " AND O.chrStatus in ('S') AND intShipShopperID is not null  AND O.IntID = "& orderID & " ORDER BY O.dtmCreated DESC"
    end if
	'response.write strSQL
    set rsOrders = gobjConn.execute(strSQL)
%>
<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">
					Resource Center
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.asp">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="default.asp?action=history">POD Order Detail</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Order ID <%= orderID %></a>
						</li>
					</ul>
				</div>
			</div>
            <div class="row">
            <table border="0" cellpadding="0" cellspacing="0" style="margin:0 auto;width:1100px;">
	        <tr>
                <td colspan="3"><a href="default.asp?action=history"><strong>Return to list</strong></a>
            </td>
			<tr>
				<td colspan="3"> &nbsp; </td>
			</tr>
			
			<tr>
				<td colspan="3" style="background-color:#f7f7f7;padding:15px;"><strong>Order Id:</strong> <%= orderID %><br />
				<strong>Order Date:</strong> <%= rsOrders("dtmUpdated") %><br />
				<% if request("lt") = "1" then %>
					<strong>Status:</strong>
					<%
					set dctOrder = GetOrder(rsOrders("intID"))
					If dctOrder("Unapproved")&""="true" Then
						Response.Write "[Unapproved] "
					End If
					If dctOrder("Unprocessed")&""="true" Then
						Response.Write "[Unprocessed] "
					End If
					If dctOrder("Accepted")&""="true" Then
						Response.Write "[Accepted] "
					End If
					If dctOrder("Hold")&""="true" Then
						Response.Write "[Hold] "
					End If
					If dctOrder("DeniedCredit")&""="true" Then
						Response.Write "[Denied Credit] "
					End If
					If dctOrder("Processed")&""="true" Then
						Response.Write "[Processed] "
					End If
					If dctOrder("Pending")&""="true" Then
						Response.Write "[Pending] "
					End If
					If dctOrder("Picked")&""="true" Then
						Response.Write "[Picked] "
					End If
					If dctOrder("Backordered")&""="true" Then
						Response.Write "[Backordered] "
					End If
					If dctOrder("Shipped")&""="true" Then
						Response.Write "[Shipped] "
					End If
					If dctOrder("Canceled")&""="true" Then
						Response.Write "[Canceled] "
					End If
					If dctOrder("Complete")&""="true" Then
						Response.Write "[Complete]"
					End If
								
					%>
					<br />
					<strong>Tracking:</strong> <%= iif(dctOrder("TrackingId")&""<>"", dctOrder("TrackingId"),"None Provided or Not Shipped Yet")%><br />
					<strong>Total:</strong><%= FormatCurrency(mnySubTotal) %>
				<% end if %>
				</td>
			<tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
	        <%
	        mnySubtotal = 0
	       
	        while not rsData.eof
                dim strPhotoURL, vchPartNumber, vchProgram, vchItemName, txtDescription, mnyUnitPrice, vchValidStates, vchBundleQuantity, vchExpireDate, chrStatus
                    vchBundleQuantity = rsData("vchBundleQuantity")
                    vchPartNumber = rsData("vchPartNumber")
                    vchItemName = rsData("vchItemName")
                    chrStatus= rsData("chrStatus")
	                

		        %>
	        <tr class="cartitem">
			
		        <td valign="top" class="cartitem-image" width="33%">
                    <% 
                    
                    
                   

                    'strPhotoURL = "../images/products/big/" & rsData("vchImageURL")
                    %>
                    
                      <% 'if strPhotoURL&""="../images/products/big/" then strPhotoURL = virtualbase & "images/icon-no-image-512.png" 
					  %>
                    <iframe src="../drcpod/js/web/viewer.html?proofURL=<%=Server.URLEncode(getDownloadURL(rsData("intProofJobTicketID")))%>" width="100%" height="300px"></iframe><br />
					<center><a href="#" onclick="window.open('../drcpod/js/web/viewer.html?proofURL=<%=Server.URLEncode(getDownloadURL(rsData("intProofJobTicketID")))%>', '_blank'); return false">View Large Proof</a></center>
                    <!--<div style="margin:0px auto;padding:0px; position:relative;width:100px;height:80px;background-repeat:no-repeat;background-size:cover;">
					
					
                        <%=iif(chrStatus="C","<img src=""../images/cancelled.jpg"" height=""80"" />","") %>
                    </div>-->
                    
                </td>
				<td valign="top" class="cartitem-data" style="padding-left: 50px">
                    <table cellpadding="1" cellspacing="1">
                        <tr align="left">
                            <td colspan="2" style="font-weight:bold;font-size:larger;"><%= vchItemName %></td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr align="left">
                            <td>Part Number:</td>
                            <td><%= vchPartNumber %></td>
                        </tr>
                        <!--<tr align="left">
                            <td>Program:</td>
                            <td><%= vchProgram %></td>
                        </tr>-->
                        
                        <!--
                         <tr align="left">
                            <td>Cost/Bundle:</td>
                            <td><%= FormatCurrency(mnyUnitPrice) %></td>
                        </tr>
                        <tr align="left">
                            <td>Bundle Quantity:</td>
                            <td><%= vchBundleQuantity %></td>
                        </tr>
                        <tr align="left">
                            <td>Valid States:</td>
                            <td><%= vchValidStates %></td>
                        </tr>
                        
                        <tr align="left">
                            <td>Expire Date:</td>
                            <td><%= vchExpireDate  %></td>
                        </tr>
                        -->
                        <tr class="no-print">
                            <td>Quantity</td>
                            <td>
                                    <% 'rsData("intQuantity") 
									%>
                               50
                            </td>
                        </tr>
                        
                    </table>
                </td>
                <td> 
                    <%if chrStatus = "C" then %>
                    This item was cancelled.
                    <% end if %>
                    <%if chrStatus = "S" then %>
                    This item was substituted for <%= rsData("vchShippingNUmber") %> .
                    <% end if %>
					&nbsp;
                </td>
	        </tr>
	            <%
                rsData.MoveNext
	        wend
	        %>
	        <tr>
                <td colspan="3"></td>
            </tr>
	        
	        
	        </table>


            </div>
        </div>
    </div>
<%
End Sub

Sub DrawWYSIWYG

    Dim rsData, strSQL, intDocId, txtText

    strSQL = "SELECT TOP 1 * FROM Athleta_GoDoc"

    if Request("action") = "edit_prebuy" Then
        strSQL = strSQL  & " WHERE STitle='Pre-buy page text'"
    elseif Request("action") = "edit_cover" Then
        strSQL = strSQL  & " WHERE STitle='Cover Page Text'"
    elseif Request("action") = "edit_faq" Then
        strSQL = strSQL  & " WHERE STitle='FAQ'"
    elseif Request("action") = "edit_scroll" Then
        strSQL = strSQL  & " WHERE STitle='Scroll Text'"
	elseif Request("action") = "edit_top_dasboard_text" Then
        strSQL = strSQL  & " WHERE STitle='Top Dashboard Text'"
	elseif Request("action") = "edit_image_text" Then
        strSQL = strSQL  &  " WHERE STitle='Image Text'"
	elseif Request("action") = "edit_lower_dashboard_text" Then
        strSQL = strSQL & " WHERE STitle='Lower Dashboard Text'"
    end if

    set rsData = gobjConn.execute(strSQL)

    if rsData.Eof Then
        if Request("action") = "edit_prebuy" Then
            strSQL = "INSERT INTO Athleta_GoDoc (STitle,Description) VALUES ('Pre-buy page text','Enter Text for pre-buy page')"
        elseif Request("action") = "edit_cover" Then
            strSQL = "INSERT INTO Athleta_GoDoc (STitle,Description) VALUES ('Cover Page Text','Enter Text for cover page')"
        elseif Request("action") = "edit_faq" Then
            strSQL = "INSERT INTO Athleta_GoDoc (STitle,Description) VALUES ('FAQ','Enter Text for FAQs')"
        elseif Request("action") = "edit_scroll" Then
            strSQL = "INSERT INTO Athleta_GoDoc (STitle,Description) VALUES ('Scroll Text','Enter Text for scroll box')"
		elseif Request("action") = "edit_top_dasboard_text" Then
            strSQL = "INSERT INTO Athleta_GoDoc (STitle,Description) VALUES ('Top Dashboard Text','Enter Text for top dashboard message')"
		elseif Request("action") = "edit_image_text" Then
            strSQL = "INSERT INTO Athleta_GoDoc (STitle,Description) VALUES ('Image Text','Enter Text for dashboard image')"
		elseif Request("action") = "edit_lower_dashboard_text" Then
            strSQL = "INSERT INTO Athleta_GoDoc (STitle,Description) VALUES ('Lower Dashboard Text','Enter Text for lower dashboard message')"
        end if

        set rsData = gobjConn.execute(strSQL)

    End If

    strSQL = "SELECT TOP 1 * FROM Athleta_GoDoc"
    if Request("action") = "edit_prebuy" Then
        strSQL = strSQL  & " WHERE STitle='Pre-buy page text'"
    elseif Request("action") = "edit_cover" Then
        strSQL = strSQL  & " WHERE STitle='Cover Page Text'"
    elseif Request("action") = "edit_faq" Then
        strSQL = strSQL  & " WHERE STitle='FAQ'"
    elseif Request("action") = "edit_scroll" Then
        strSQL = strSQL  & " WHERE STitle='Scroll Text'"
	elseif Request("action") = "edit_top_dasboard_text" Then
        strSQL = strSQL  & " WHERE STitle='Top Dashboard Text'"
	elseif Request("action") = "edit_image_text" Then
        strSQL = strSQL  & " WHERE STitle='Image Text'"
	elseif Request("action") = "edit_lower_dashboard_text" Then
        strSQL = strSQL  & " WHERE STitle='Lower Dashboard Text'"
    end if

    set rsData = gobjConn.execute(strSQL)

    Response.Write "<h1>"
    if Request("action") = "edit_prebuy" Then
       Response.Write "Pre-buy page text"
    elseif Request("action") = "edit_cover" Then
        Response.Write  "Cover Page Text"
    elseif Request("action") = "edit_faq" Then
        Response.Write  "FAQs"
    elseif Request("action") = "edit_scroll" Then
        Response.Write  "Welcome Message"
	elseif Request("action") = "edit_top_dasboard_text" Then
        Response.Write  "Top Dashboard Text"
	elseif Request("action") = "edit_image_text" Then
        Response.Write  "Image Text"
	elseif Request("action") = "edit_lower_dashboard_text" Then
        Response.Write  "Lower Dashboard Text"
    end if
    Response.Write "</h1>"

%>


<script src="../ckeditor/ckeditor.js"></script>
<% if Request("action") = "edit_lower_dashboard_text" then %>
<script type="text/javascript" src="jscolor/jscolor.js"></script>
<% end if %>
<form action="default.asp" method="post">
<input type="hidden" name="action" value="save-doc" />
<input type="hidden" name="id" value="<%= rsData("ID") %>" />
<% if Request("action") = "edit_lower_dashboard_text" then %>
Background Color: <input type="color" id="color" name="color" style="width: 150px" value =<% =rsData("caption")%>><br />
<% end if %>
            <textarea name="editor1" id="editor1" rows="10" cols="80"><%= rsData("Description")&"" %></textarea>
			<% if Request("action") <> "edit_top_dasboard_text" and Request("action") <> "edit_image_text" then %> 
				<script>
					// Replace the <textarea id="editor1"> with a CKEditor
					// instance, using default configuration.
					CKEDITOR.config.allowedContent = true;
					CKEDITOR.replace('editor1');
				</script>
			<% end if %>
            <input type="submit" value="Save Text" />
        </form>
<%
End Sub

Sub DrawPrint

%><center>
<form action="default.asp?cc=1">
<input type="hidden" name="cc" value="1" />
<input type="submit" name="action" value="Return" class="no-print" />
</form>
<style>
    table {text-align:left;}
    .title {text-align:center;font-size:larger;font-weight:bold;}
    .label {font-weight:bold;color:#000;display:inline-block;margin-right:15px;border:0px !important;}
    .data {display:inline-block;margin-right:15px;}
    .page-break {page-break-before:always;}
    .image {margin:10px;width:100px;height:100px;background-repeat:no-repeat;background-size:cover;overflow:hidden;}
    .part-number {background-color:#ccc;color:#fff;}
    td {vertical-align:top;}
    .item {page-break-inside: avoid;}
@media print
{
    .no-print {display:none;}   
}

</style>

<%
    Dim intCatalogID, strSQL,rsTemp,strPhotoURL, currentBrand
   If request("print-full")&""<>"" Then
        strSQL = "SELECT TOP 1 * FROM Athleta_GoDoc WHERE STitle='Cover Page Text'"
    else
        strSQL = "SELECT TOP 1 * FROM Athleta_GoDoc WHERE STitle='Cover Page Text'"
    end if
    set rsTemp = gobjConn.execute(strSQL)

    %>
    <div><%=rsTemp("description") %></div>
    <%

    If request("print-full")&""<>"" Then
        strSQL = "SELECT B.vchItemName as BrandName, P.vchItemName as ProgramName, C.vchItemName as CategoryName,I.vchBundleQuantity, I.mnyItemPrice, I.vchImageURL, I.vchExpireDate, I.vchPartNumber,"
        strSQL = strSQL & "I.vchValidStates,I.vchItemName as InvItemName,I.txtDescription,I.txtShortDescription  FROM " & STR_TABLE_INVENTORY & " I " 
        strSQL = strSQL & "LEFT JOIN " & STR_TABLE_INVENTORY & " B ON I.intBrand = B.intID  "
        strSQL = strSQL & "LEFT JOIN " & STR_TABLE_INVENTORY & " C ON I.intCategory = C.intID  "    
        strSQL = strSQL & "LEFT JOIN " & STR_TABLE_INVENTORY & " CM ON I.intCampaign = CM.intID  "
        strSQL = strSQL & "INNER JOIN " & STR_TABLE_INVENTORY & " P ON I.intProgram = P.intID  "

        strSQL = strSQL & "WHERE  I.chrStatus='A' and I.chrType='I' and I.vchPartNumber!='' "
        If intCampaignID > 0 Then
            strSQL = strSQL & "AND  I.intCampaign=" & intCampaignID & " "
        End If
        strSQL = strSQL & "ORDER BY B.vchItemName,I.vchPartNumber "

        set rsTemp = gobjConn.execute(strSQL)
    else
        intCatalogID =  Session("Catalog")&""

        if intCatalogID&""="" Then
            Response.Redirect "default.asp?cc=1"
        Else
            intCatalogID = Clng(intCatalogID)
        End If

        strSQL = "SELECT B.vchItemName as BrandName, P.vchItemName as ProgramName, C.vchItemName as CategoryName,I.vchBundleQuantity, I.mnyItemPrice, I.vchImageURL, I.vchExpireDate, I.vchPartNumber, I.vchValidStates,I.vchItemName as InvItemName,I.txtDescription,I.txtShortDescription  FROM " & STR_TABLE_LINEITEM & " L "
	    strSQL = strSQL & "INNER JOIN " & STR_TABLE_INVENTORY & " I ON  L.intInvId = I.intID " 
        strSQL = strSQL & "LEFT JOIN " & STR_TABLE_INVENTORY & " B ON I.intBrand = B.intID  "
        strSQL = strSQL & "LEFT JOIN " & STR_TABLE_INVENTORY & " C ON I.intCategory = C.intID  "    
        strSQL = strSQL & "LEFT JOIN " & STR_TABLE_INVENTORY & " CM ON I.intCampaign = CM.intID  "
        strSQL = strSQL & "LEFT JOIN " & STR_TABLE_INVENTORY & " P ON I.intProgram = P.intID  "

        strSQL = strSQL & "WHERE  L.intOrderID=" & intCatalogID & " "
        strSQL = strSQL & "ORDER BY B.vchItemName,I.vchPartNumber "
        set rsTemp = gobjConn.execute(strSQL)

    end if
    
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"
    response.write "<br style=""page-break-after: always"" >"

    while not rsTemp.eof

        Dim vchPartNumber,BrandName, ProgramName, CategoryName, vchValidStates, mnyItemPrice, vchBundleQuantity, vchExpireDate,vchItemName, txtDescription,txtShortDescription
        txtDescription = rsTemp("txtDescription")        
        txtShortDescription = rsTemp("txtShortDescription")

        vchBundleQuantity = rsTemp("vchBundleQuantity")
        BrandName = rsTemp("BrandName")
         mnyItemPrice = rsTemp("mnyItemPrice")
		 vchItemName = rsTemp("InvItemName")
        
        vchExpireDate = rsTemp("vchExpireDate") 
        vchPartNumber =  rsTemp("vchPartNumber")
        ProgramName = rsTemp("ProgramName")
        CategoryName = rsTemp("CategoryName")
        vchValidStates = rsTemp("vchValidStates")
       

        strPhotoURL = "images/products/big/" & rsTemp("vchImageURL")&""
        if rsTemp("vchImageURL")&""="" then strPhotoURL = "images/icon-no-image-512.png" 

        %>
        <% If BrandName<> currentBrand Then  %>
            <% If currentBrand&""<>"" Then %>
        </table>
            <% End If %>
        <table class="page-break" cellpadding="3" cellspacing="3" width="100%">            
            <tr>
                <td colspan="3" class="title"><%= rsTemp("BrandName") %></td>
            </tr>
            <tr>
                <td colspan="3" class="title">&nbsp;</td>
            </tr>
        <% currentBrand =BrandName %>
        <% End If %>
            <tr>
                <td colspan="3" valign="top" align="center" class="item">
                    <table  style="border-bottom:1px solid #000;width:700px;margin:0 auto;">
						<tr>
                            <td rowspan="4" width="100"><div class="image" style="overflow:hidden;height:100px;"><img src="<%=  virtualbase & strPhotoURL %>" width="100" /></div></td>
                            <td class="label" width="100">Item Name:</td>
                            <td align="left" width="500"><%= vchItemName %></td>
                        </tr>
                        <tr>
                            <td class="label">Part Number:</td>
                            <td class="part-number" align="left"><%= vchPartNumber %></td>
                        </tr>
                        <tr>
                            <td class="label">Size:</td><td align="left"><%= ProgramName %></td>
                        </tr>
 <!--                       <tr>
                            <td class="label">Short Desc:</td><td align="left"><%= txtShortDescription %></td>
                        </tr>
                        <tr class="last">
                            <td class="label">Description</td><td align="left"><%= txtDescription %></td>
                        </tr>
                        <tr>
                            <td colspan="3"><span class="label">Valid States:</span><span class="data"><%= vchValidStates %></span></td>
                        </tr>
                        <tr class="last">
                            <td colspan="3"><span class="label">Cost / Bundle:</span><span class="data"><%' FormatCurrency(mnyItemPrice) 
							%></span><span class="label">Bundle Qty:</span><span class="data"><%= vchBundleQuantity %></span><span class="label">Expire Date:</span><span class="data"><%= vchExpireDate %></span> </td>
                        </tr>-->
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="title">&nbsp;</td>
            </tr>  
        <%
        rsTemp.MoveNext
    wend
    %>
    </table>
    <script>
        window.print();
    </script>
    </center>
    <%
End Sub

sub DrawPage		'Online Store
%>
<div class="page-content-wrapper">
		<div class="page-content">
        <!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">
                        <% if Request("cc")<>"" Then %>
                        File Tracker
                        <% else %>
                        Pre-Buy Items
                        <% end if %>
					</h3>
					
                    <ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.asp">Home</a>
							<% if Request("cc")<>"" Then %>
							<i class="fa fa-angle-right"></i><a href="#"> File Tracker</a>
							<% else %>
							<i class="fa fa-angle-right"></i><a href="#"> Pre-buy</a>
							<% end if %>
							<% 
                            Dim rsTemp
                            if intProgramID > 0 then
                            strSQL = "SELECT *  FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intProgramID & " AND chrStatus='A'"
	                        set rsTemp = gobjConn.execute(strSQL)

                                if Not rsTemp.Eof Then
                                %>
                                    <i class="fa fa-angle-right"></i>

                                <%
                                End If
                            elseif intCampaignID > 0 then
                            strSQL = "SELECT *  FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intCampaignID & " AND chrStatus='A'"
	                        set rsTemp = gobjConn.execute(strSQL)

                                if Not rsTemp.Eof Then
                                %>
                                    <i class="fa fa-angle-right"></i>

                                <%
                                End If

                            end if
                            %>

						</li>
                        <% 
                        if  intCampaignId >0 Or intProgramId then
                            if Not rsTemp.Eof then

                        %>
                        <li>
	                        <a href="#"><%= rsTemp("vchItemName") %></a>
                        </li>

                        <%
                            end if
                        
                        end if
                        %>
						
					</ul>
				</div>
			</div>
			<!-- END PAGE HEADER-->
	<% if intCampaignID = 0 and request("cc")&""="" then %>
        <div id="createform">
                <% 
                strSQL = "SELECT intID, vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE chrType='C' and chrStatus='A'"
                set rsTemp = gobjConn.execute(strSQL)
                if not rsTemp.eof then
                %>
                    <form action="default.asp" method="post" style="text-align:left;padding:20px;">
						<input type="hidden" name="programid" value="">
                        <fieldset>
                            <legend>Load existing campaign</legend>
                            <select name="campaignid" id="campaignid" onchange="this.form.submit()">
                                <option>Select Existing Campaign</option>
                                <% 
                                strSQL = "SELECT intID, vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE chrType='C' and chrStatus='A'"
                                set rsTemp = gobjConn.execute(strSQL)
                                while not rsTemp.eof
                                %>
                                <option value="<%= rsTemp("intID") %>"><%= rsTemp("vchItemName") %></option>
                                <%
                                    rsTemp.MoveNext
                                wend
                                %>
                            </select>
                        </fieldset>
                    </form>
                <% end if %>
        </div>
        <script>
            $(document).ready(function () {
                $.blockUI(
                    {
                        message: $('#createform'),
                    });

            }); 
        </script>
        <% end if %>
        <table id="tblMain" width="100%">
        <tr>
            <td valign="top">
	        <% If request("cc")&""="" Then %>
            <form action="default.asp">
                <input type="hidden" name="campaignID" value="<%= Request("campaignID") %>">
                <!--<select name="programid" id="programid" class="multiselect">
                <option>Select Size</option>
                <%
                    if intCampaignID <> 0 Then
						strSQL = "SELECT DISTINCT P.intID, P.vchItemName FROM Athleta_Inv P INNER JOIN Athleta_Inv I ON I.intProgram = P.intID WHERE P.chrType = 'P' AND P.chrStatus = 'A' AND I.intCampaign = " & intCampaignID
					else
						strSQL = "SELECT intID, vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE chrType='P' AND chrStatus='A'"
					end if

                    set rsTemp = gobjConn.execute(strSQL)
                                    
                    While not rsTemp.Eof
                        %>
                        <option value="<%= rsTemp("intID") %>"<%= iif(rsTemp("intID")=intProgramId,"SELECTED","") %>><%= rsTemp("vchItemName") %></option>
                        <%
                        rsTemp.MoveNext
                    Wend

                %>
                </select>
                <script>
                    jQuery(document).ready(function () {
                        jQuery("#programid").multiselect({
                            nonSelectedText: "Select Size",
                            onChange: function (event) {
                                document.forms[0].submit();
                            }
                        });
                    });
                 </script>-->
            </form>
        <% else %>
            
            <form id="campaign-selection" action="default.asp" method="get">
                <input type="hidden" name="cc" value="1" />
                
                <select name="campaignid" id="campaignid" class="multiselect">
                <option>Select a Campaign</option>
                <%
                    strSQL = "SELECT * FROM " & STR_TABLE_INVENTORY & " WHERE chrType='C' AND chrStatus='A'"

                    set rsTemp = gobjConn.execute(strSQL)
                                    
                    While not rsTemp.Eof
                        %>
                        <option value="<%= rsTemp("intID") %>"<%= iif(rsTemp("intID")=intCampaignId," SELECTED","") %>><%= rsTemp("vchItemName") %></option>
                        <%
                        rsTemp.MoveNext
                    Wend

                %>
                </select>
                
                <% if intCampaignID > 0 then %>
                    <select name="profileid" id="profileid" class="multiselect">
                    <option>Select a Profile</option>
                    <%
                        strSQL = "SELECT intid, vchprofilename FROM " & STR_TABLE_PROFILES & " WHERE chrStatus='A'"

                        set rsTemp = gobjConn.execute(strSQL)
                                        
                        While not rsTemp.Eof
                            %>
                            <option value="<%= rsTemp("intID") %>"<%= iif(rsTemp("intID")=intprofileid," SELECTED","") %>><%= rsTemp("vchprofilename") %></option>
                            <%
                            rsTemp.MoveNext
                        Wend

                    %>
                    </select>
                <% end if %>
                
                <input type="text" placeholder="SKU" name="search" class="btn-group-vertical" style="height:34px;width:200px;border:1px solid #ccc;padding:5px;" /><input type="submit" value="Submit" class="btn-group-vertical" style="border:1px solid #ccc;height:34px;margin-left:2px;width:80px;font-weight:bold;background-color:#f0f0f0;" />
                <script>
                    jQuery(document).ready(function () {
                        jQuery("#campaignid").multiselect({
                            nonSelectedText: "Select a Campaign",
                            onDropdownHide: function (event) {
                                $("#campaign-selection").submit();
                            }
                        });
                        jQuery("#profileid").multiselect({
                            nonSelectedText: "Select a Profile",
                            onDropdownHide: function (event) {
                                $("#campaign-selection").submit();
                            }
                        });
                    });
                 </script>
            </form>

        <% End If %>
	<%
	dim rsDesc, Description, intID, vchItemName
	rsDesc = "SELECT intParentID, intID, chrStatus,mnyItemPrice, txtDescription"
	rsDesc = rsDesc & " FROM " & STR_TABLE_INVENTORY


	set rsDesc = gobjConn.execute(rsDesc)	

	if not rsDesc.eof then
		Description = rsDesc("txtDescription")
		intID = rsDesc("intID")
	end if

	%>
	<%
	if rsData.eof then
        if request("cc")&""<>"" then
            response.write "Select a campaign from above to start the file tracking process."
        end if
	else
		blnLeftImage = true	'intItemCount mod 2 = 0
		intFolderCount = 0
		intItemCount = 0
        %>
       <style>
       .CSSTableGenerator {
	margin:0px;padding:0px;
	width:100%;
	
}.CSSTableGenerator table{
    border-collapse: collapse;
        border-spacing: 0;
	width:100%;
	height:100%;
	margin:0px;padding:0px;
}.CSSTableGenerator tr:last-child td:last-child {
	-moz-border-radius-bottomright:0px;
	-webkit-border-bottom-right-radius:0px;
	border-bottom-right-radius:0px;
}
.CSSTableGenerator table tr:first-child td:first-child {
	-moz-border-radius-topleft:0px;
	-webkit-border-top-left-radius:0px;
	border-top-left-radius:0px;
}
.CSSTableGenerator table tr:first-child td:last-child {
	-moz-border-radius-topright:0px;
	-webkit-border-top-right-radius:0px;
	border-top-right-radius:0px;
}.CSSTableGenerator tr:last-child td:first-child{
	-moz-border-radius-bottomleft:0px;
	-webkit-border-bottom-left-radius:0px;
	border-bottom-left-radius:0px;
}.CSSTableGenerator tr:hover td{
	
}
.blockMsg  {border:0px !important;}
.CSSTableGenerator td{
	vertical-align:middle;
	
	text-align:left;
	padding:7px;
	font-size:10pt;
	font-family: 'Open Sans', sans-serif;
	font-weight:normal;
	color:#000000;
.fancybox-iframe {
  position: absolute;
  top: 100px !important;
}
       </style>

        <div style="clear:left;" id="boxes">
        <%
		while not rsData.eof
			DrawItem rsData, blnLeftImage
			rsData.MoveNext
		wend
        %>
        </div>
        
        <%
	end if
%>
        </td>
   
        </tr>
        </table>
    </div>
</div>
<div id="file-tracker"></div>
<script>
    $(".fancybox").fancybox({
        afterClose: function() {
            location.reload();
        }
    });
    
    $(".iframe").fancybox({
        'hideOnContentClick': false,
        'type'				: 'iframe',
        afterClose : function() {
            location.reload();
            return;
        }
    });
    
    $(".inline").fancybox({
        'hideOnContentClick': true,
        afterClose : function() {
            location.reload();
            return;
        }
    });
</script>
<%
end sub


sub DrawLTPage		'Online Store
%>
<div class="page-content-wrapper">
	<div class="page-content">
    <!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<h3 class="page-title">
					<% if request("lt") = "1" then %>
						Inventory
					<% else %>
						Marketing Signage
					<% end if %>
                </h3>
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="dashboard.asp">Home</a>
						<i class="fa fa-angle-right"></i>
					<li> <a href="default.asp?lt=<%=request("lt")%>">
						<% if request("lt") = "1" then %>
							Inventory
						<% else %>
							Marketing Signage
						<% end if %></a> </li><li>
						<% 
                        Dim rsTemp
                        if intBrandID&"" <> "" then
                            strSQL = "SELECT *  FROM " & STR_TABLE_INVENTORY_LONG_TERM & " WHERE intID in (" & intBrandID & ") AND chrStatus='A'"
	                        set rsTemp = gobjConn.execute(strSQL)

                                if Not rsTemp.Eof Then
                                %>
                                    <i class="fa fa-angle-right"></i>

                                <%
                                End If
                        elseif intCategoryID > 0 then
                            strSQL = "SELECT *  FROM " & STR_TABLE_INVENTORY_LONG_TERM & " WHERE intID=" & intCategoryID & " AND chrStatus='A'"
	                        set rsTemp = gobjConn.execute(strSQL)

                                if Not rsTemp.Eof Then
                                %>
                                    <i class="fa fa-angle-right"></i>

                                <%
                                End If

                            end if
                            %>

						</li>
                        <% 
                        if  intCategoryID >0 Or intBrandID&"" <> "" then
                            if Not rsTemp.Eof then

                        %>
                        <li>
	                        <a href="#"><%= rsTemp("vchItemName") %></a>
                        </li>

                        <%
                            end if
                        
                        end if
                        %>
						
					</ul>
				</div>
			</div>
			<!-- END PAGE HEADER-->
        <table id="Table1" width="100%">
        <tr>
            <td valign="top">
				<div style="line-height:30px;height:40px;">
				<form action="default.asp">
					<input type="hidden" name="lt" value="<%= request("lt") %>" />
					<input type="hidden" name="campaignID" value="<%= Request("campaignID") %>">
					<select name="brandid" id="brandid"  multiple="multiple" class="multiselect">
					<%
					strSQL = "select vchGroups from " & STR_TABLE_SHOPPER & " WHERE intID=" & gintUserID
					dim rsUser, strSplitGroups, x, i 
					set rsUser = gobjConn.execute(strSQL)
                     strSQL = "SELECT * FROM " & STR_TABLE_INVENTORY_LONG_TERM & " WHERE chrType='B' AND chrStatus='A' AND intID in ( select distinct intBrand from " & STR_TABLE_INVENTORY_LONG_TERM
					 if not rsUser.eof then
						strSplitGroups = split(rsUser("vchGroups")&"",",")
						if rsUser("vchGroups")&"" <> "" then
						strSQL = strSQL & " where ("
						i = 0
							for each x in strSplitGroups
								if i > 0 then
									strSQL = strSQL & " OR "
								end if
								strSQL = strSQL & " vchGroupingIDs LIKE '%, " & x & ",%' " '--middle
								strSQL = strSQL & " OR "
								strSQL = strSQL & " vchGroupingIDs LIKE '" & x & ",%'" ' --start
								strSQL = strSQL & " OR "
								strSQL = strSQL & " vchGroupingIDs LIKE '%, " & x & "' " '--end
								strSQL = strSQL & " OR "
								strSQL = strSQL & " vchGroupingIDs =  '" & x & "'  "
								i = i + 1
							next
							strSQL = strSQL & " OR vchGroupingIDs is null and chrStatus = 'A' ) )"
						else
							strSQL = strSQL & " where vchGroupingIDs is null and chrStatus = 'A' )"
						end if
					else
						strSQL = strSQL & " where vchGroupingIDs is null and chrStatus = 'A' ) "
					end if
					
					strSQL = strSQL & " and intItemSuperCategory = " & request("lt") 
					 
					strSQL = strSQL & " ORDER BY vchItemName"
					
                    set rsTemp = gobjConn.execute(strSQL)
					
					response.write strSQL
                    
                    While not rsTemp.Eof
                        %>
                        <option value="<%= rsTemp("intID") %>"<%= iif(InStr(intBrandId,rsTemp("intID"))>0," SELECTED","") %>><%= rsTemp("vchItemName") %></option>
                        <%
                        rsTemp.MoveNext
                    Wend
                %>
                 </select>
                 
                <!--<select name="categoryid" id="categoryid" class="multiselect">
                <option>Select a Subcategory</option>
                <%
                     strSQL = "SELECT * FROM " & STR_TABLE_INVENTORY_LONG_TERM & " WHERE chrType='G' AND chrStatus='A' ORDER BY vchItemName"

                    set rsTemp = gobjConn.execute(strSQL)
                                    
                    While not rsTemp.Eof
                        %>
                        <option value="<%= rsTemp("intID") %>"  <%= iif(rsTemp("intID")=intCategoryID,"SELECTED","") %>><%= rsTemp("vchItemName") %></option>
                        <%
                        rsTemp.MoveNext
                    Wend
                %>
                 </select>-->
                 <input type="text" name="vchItemName" style="height:33px;width:180px;display:inline-block;position:relative;display:inline-block;vertical-align:middle;" placeholder="Search by item name" value="<%=Request("vchItemName")&"" %>" />
                 <input type="submit" value="Submit" style="height:33px;display:inline-block;position:relative;display:inline-block;vertical-align:middle;" />
                <script>
                    jQuery(document).ready(function () {
                        
                        jQuery("#brandid").multiselect({
                            selectedText: "# of # selected",
                            nonSelectedText: "Select a Category"
                        });
                        jQuery("#categoryid").multiselect({
                            selectedText: "# of # selected",
                            nonSelectedText: "Select a Sub-category"
                        });

                    });
                 </script>
            </form>
            </div>
	<%
	dim rsDesc, Description, intID, vchItemName
	rsDesc = "SELECT intParentID, intID, chrStatus,mnyItemPrice, txtDescription"
	rsDesc = rsDesc & " FROM " & STR_TABLE_INVENTORY_LONG_TERM
	rsDesc = rsDesc & " WHERE chrStatus='A' " 
    if intBrandId&""<>"" then
        rsDesc = rsDesc & " AND intBrand in (" & intBrandId &")"
    end if
	'response.write rsDesc
	set rsDesc = gobjConn.execute(rsDesc)	

	if not rsDesc.eof then
		Description = rsDesc("txtDescription")
		intID = rsDesc("intID")
	end if

	if rsData.eof then
        response.write "<b>Select a category from above and click the ""Submit"" button</b>"
      
	else
		response.write "<b>Enter quantities for the desired items and click on the ""Add to cart"" button the right.</b>"
		blnLeftImage = true	'intItemCount mod 2 = 0
		intFolderCount = 0
		intItemCount = 0
        %>
       <style>
       .CSSTableGenerator {
	margin:0px;padding:0px;
	width:100%;
	
}.CSSTableGenerator table{
    border-collapse: collapse;
        border-spacing: 0;
	width:100%;
	height:100%;
	margin:0px;padding:0px;
}.CSSTableGenerator tr:last-child td:last-child {
	-moz-border-radius-bottomright:0px;
	-webkit-border-bottom-right-radius:0px;
	border-bottom-right-radius:0px;
}
.CSSTableGenerator table tr:first-child td:first-child {
	-moz-border-radius-topleft:0px;
	-webkit-border-top-left-radius:0px;
	border-top-left-radius:0px;
}
.CSSTableGenerator table tr:first-child td:last-child {
	-moz-border-radius-topright:0px;
	-webkit-border-top-right-radius:0px;
	border-top-right-radius:0px;
}.CSSTableGenerator tr:last-child td:first-child{
	-moz-border-radius-bottomleft:0px;
	-webkit-border-bottom-left-radius:0px;
	border-bottom-left-radius:0px;
}.CSSTableGenerator tr:hover td{
	
}
.blockMsg  {border:0px !important;}
.CSSTableGenerator td{
	vertical-align:middle;
	
	text-align:left;
	padding:7px;
	font-size:10pt;
	font-family: 'Open Sans', sans-serif;
	font-weight:normal;
	color:#000000;
       </style>
       
	<form id="massInvAddToCart" name="massInvAddToCart">
        <div style="clear:left;" id="Div1">
        <%
		while not rsData.eof
            
            DrawItemLt rsData, blnLeftImage
            
            rsData.MoveNext
		wend
        %>
              
        </div>
    </form>
        <%
	end if
%>
        </td>
		<script>
			$(document).ready(function () {
				positionAddToCart();
				$(window).resize(function() {
					positionAddToCart();
				});
				
			});
			
			function searchSubmit () 
			{
				var vchStoreSearch = "";
				if ($("#store_search").val())
				{
					vchStoreSearch = $("#store_search").val();
				}
				var vchState = $("#State").val();
				var vchCity = $("#city").val();
				var intStoreID = $("#storenumber").val();
				$.ajax({
					type: "POST",
					url: url,
					data: "action=loadAddressBook&" + $("#storeSearch").serialize(),
					dataType: "json",
					success: function (data) 
					{
						var message = "";
						
						message += "<tr>";
						$("#massInvAddToCart input").each(function () {
							if ($(this).val() != "")
							{
								message += "<input type=\"hidden\" name=\"" + $(this).attr('name') + "\" value=\"" + $(this).attr('value') + "\" />";
							}
						});
						message += "<td colspan=\"4\">";
						message += "<select class=\"multiselect\" name=\"storenumber\" id=\"storenumber\" multiple=\"multiple\" onchange=\"searchSubmit();\">";
						$.ajax({
							type: "POST",
							url: url,
							async:false,
							data: "action=loadStoreNumber", 
							success: function(data2)
							{
								//console.log(data2);
								for (var key in data2) {
									var actualData2 = data2[key];
									
									message += "<option value=\"" + actualData2["intID"] + "\"";
									//console.log(intStoreID);
									//console.log(actualData2["intID"]);
									if ($.inArray(actualData2["intID"].toString(), intStoreID) > -1 )
									{
										message += " SELECTED";
									}
									message += " >" + actualData2["vchLabel"] + "</option>";
								}
							},
							error: function(data2)
							{
								console.log(data2);
							}
						});
						message += "</select>";
						
						message += "<select class=\"multiselect\" name=\"city\" id=\"city\" multiple=\"multiple\" onchange=\"searchSubmit();\">";
						$.ajax({
							type: "POST",
							url: url,
							async:false,
							data: "action=loadDistinctFilter&vchFilter=vchCity",
                            success: function(data2) {
								for (var key in data2) {
									var actualData2 = data2[key];
								
									message += "<option value=\"" + actualData2["vchCity"].trim() + "\"";
									if ($.inArray(actualData2["vchCity"].toString(), vchCity) > -1 )
									{
										message += " SELECTED";
									}
									message += " >" + actualData2["vchCity"].trim() + "</option>";
								}
							}
						});
						message += "</select>";
						
						message += "<select class=\"multiselect\" name=\"State\" id=\"State\" multiple=\"multiple\" onchange=\"searchSubmit();\">";
						$.ajax({
							type: "POST",
							url: url,
							async:false,
							data: "action=loadDistinctFilter&vchFilter=vchState",
							 // serializes the form's elements.
							success: function(data2)
							{
								for (var key in data2) {
									var actualData2 = data2[key];
									message += "<option value=\"" + actualData2["vchState"] + "\"";
									if ( $.inArray(actualData2["vchState"].toString(), vchState) > -1 )
									{
										message += " SELECTED";
									}
									message += " >" + actualData2["vchState"] + "</option>";
								}
							},
							error: function(data2)
							{
								console.log(data2);
							}
						});
						message += "</select>";
						
						message += "<input type=\"text\" placeholder=\"Enter Search Details\" name=\"store_search\" id=\"store_search\" value=\"" + vchStoreSearch + "\" class=\"btn-group-vertical\" style=\"height:34px;width:200px;border:1px solid #ccc;padding:5px;color: black;\" />";
						message += "<input type=\"submit\" value=\"Search\" class=\"btn-group-vertical\" style=\"color:#000;border:1px solid #ccc;height:34px;margin-left:2px;width:80px;font-weight:bold;background-color:#f0f0f0;\" onclick=\"searchSubmit(); return false;\" />";
						message += "<input type=\"submit\" value=\"Add to cart\" name=\"action\" class=\"btn-group-vertical\" style=\"color:#000;border:1px solid #ccc;height:34px;margin-left:2px;width:180px;font-weight:bold;background-color:#f0f0f0;\" onclick=\"addLTItoCart('multi'); return false;\" /></td></tr>"; 
						
						for (var key in data) {
							if (data.hasOwnProperty(key)) {
								var actualData = data[key];
								
								message += "<tr>";
								message += "<td>" + actualData["vchLabel"] + "</td>";
								message += "<td>";
								message += actualData["vchAddress2"] + "<br />";
								message += actualData["vchCity"] + "," + actualData["vchState"] + "&nbsp;" + actualData["vchZip"];
								message += "</td>";
								message += "<td><input type=\"checkbox\" style=\"width:60px;\" name=\"Store_" + actualData["intID"] + "\" value=\"true\" /></td>";
								message += "</tr>";
							}
						}
						
						$( "#storeSelection" ).dialog({
                            title: 'Add to Cart',
							modal: true,
							draggable: false,
							resizable: false,
							width: "55%",
							minHeight: 400
						});
						$("#storeSelector").html(message);
						
						jQuery("#storenumber").multiselect({
							selectedText: "# of # selected",
							nonSelectedText: "Store"
						});
						jQuery("#city").multiselect({
							selectedText: "# of # selected",
							nonSelectedText: "City"
						});
						jQuery("#State").multiselect({
							selectedText: "# of # selected",
							nonSelectedText: "State"
						});
					
					}
                });

				return false; // avoid to execute the actual submit of the form.
			}
			
			function addLTItoCart(type) {
				var form = "";
				if (type == "single")
				{
					form = "#massInvAddToCart";
				}
				else
				{
					form = "#storeSearch";
				}
				$.ajax({
					type: "POST",
					url: url,
					async:false,
					data: "action=addLTItoCart&" + $(form).serialize(), // serializes the form's elements.
					success: function(data) {
						$('#massInvAddToCart').trigger("reset");
						DisplayCart();
					},
					error: function(data) {
						console.log(data);
					}
				});
			}
			
			function positionAddToCart() {
				var contentWidth = $(".page-content").width();
				var windowWidth = $(document).width();
				var newWidth;
				newWidth = ((windowWidth - contentWidth) / 2) - 5;
				$("#newAddToCart").attr("style", "width: 10px !important; position: fixed; right: " + newWidth +"px;");
				return false;
			}
			
			function getQueryVariable(variable)
			{
				   var query = $("#storeSelector").serialize();
				   var vars = query.split("&");
				   for (var i=0;i<vars.length;i++) {
						   var pair = vars[i].split("=");
						   if(pair[0] == variable){return pair[1];}
				   }
				   return(false);
			}
			
			function newSingleAddToCartMass()
			{
				$.ajax({
					type: "POST",
					url: url,
					async:true,
					data: "action=loadAddressBook", // serializes the form's elements.
					success: function(data)
					{
						// console.log(data);
						if (data.length == 1)
						{
							addLTItoCart("single");
						}
						else
						{
							searchSubmit();
						}
					},
					error: function(data)
					{
						console.log(data);
					}
				});
			}
		</script>
        <% If request("cc")&""="" and Request("action") <> "edit_prebuy" Then %>
        
		<div id="newAddToCart" style="width: 10px !important; position: fixed; right: 886px;">
            <a href="#" onclick="newSingleAddToCartMass(); return false" style="font-weight: 900; color: white; text-decoration: none; display:inline-block;">
                <div style="font-size:1.4em; border-radius: 0px 10px 10px 0px !important; background-color: #13436D; padding: 20px 35px 20px 30px;">
				A D D<br><br>T O<br><br>C A R T
                </div>
            </a>
		</div>
        
		 <% Elseif Request("action") = "edit_prebuy" Then %>
         <script type="text/javascript">



         </script>
        <% End If %>
        </tr>
        </table>
    </div>
	<div id="storeSelection" style="display:none">
			<form method="post" id="storeSearch">
				<table id="storeSelector" align="left" class="CSSTableGenerator"></table>
			</form>
		</div>
</div>
<%
end sub

sub DrawItem(rsInput, blnLeftImage)
	dim strPhotoURL, strURL, strImgURL, strDescription, strQtyBreaks
	dim strOptionList1, strOptionList2, strOptionList3

	strPhotoURL = "../images/products/" & rsInput("vchImageURL") & ""
	if rsInput("chrType") = "I" then 
		intItemCount = intItemCount + 1
		strURL = "itemdetail.asp?id=" & rsInput("intID")
	else
		intFolderCount = intFolderCount + 1
		strURL = "default.asp?parentid=" & rsInput("intID")
	end if
	%>
	<% If request("cc")&""="" Then %>
       <div  style="float:left;width:200px;margin-right:20px;margin-top:10px;padding:10px;min-height:500px;">
        <table width="100%">
            
            <tr>
                <td align="center" valign="middle">
                <% if rsInput("vchImageURL")&""="" then strPhotoURL = virtualbase & "images/icon-no-image-512.png" %>
                <div style="background-image:url(<%= strPhotoURL %>);margin-bottom:5px;width:150px;height:130px;background-repeat:no-repeat;background-size:cover;">
                </div>
                  <a class="iframe" rel="group" href="<%= strPhotoURL %>" style="background-color:#fff;"><img src="<%= virtualbase & "images/magnifier_medium_left.png"  %>" height="25" width="25"></a>
                
                </td>
            </tr>
            <tr>
                <td style="height:40px;vertical-align:top;font-weight:bold;"><%= rsInput("vchItemName") %></td>
            </tr>
            <tr>
                <td><%= FormatCurrency(rsInput("mnyItemPrice")) %> pack of <%= rsInput("vchBundleQuantity") %><%= strQtyBreaks %></td>
            </tr>
            <tr>
                <td style="padding:10px 0px;"><a href="#" onclick="$.blockUI({message: $('#popup_<%= rsInput("intID") %>'),css: { top: '10%' }});$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);">Add to Cart</a></td>
            </tr>
            <tr>
                <td><%= strDescription %></td>
            </tr>
            
        </table>

        <style>
            .blockOverlay { cursor:pointer !important;background-color:#f0f0f0 !important;}
            #table_<%= rsInput("intID") %> th,#table_<%= rsInput("intID") %> td
            {
            padding:10px !important;
            }
       
        </style>
        <div style="display:none;width:600px;height:500px;overflow-y:scroll;cursor:text" id="popup_<%= rsInput("intID") %>" class="CSSTableGenerator">
            <form action="cartlist.asp" target="cartlist" method="post">
            <input type="hidden" name="action" value="multiadd_dist" />
            <table id="table_<%= rsInput("intID") %>" width="600">
                 <tr>
                    <td colspan="5" align="center" style="background-color:white;"><input type="submit" value="Add Item to Carts" onclick="$.unblockUI()" /><img src="../images/close_button.png" style="cursor:pointer;float:right;" onclick="$.unblockUI()" width="25" /></td>
                </tr>
                <tr>
                    <th>Distributor</th>
                    <th>Item</th>
                    <th nowrap>Pack Size</th>
                    <th nowrap>Price per Pack</th>
                    <th>Quantity</th>
                </tr>
                <%
                Dim strSQL, rsTemp, even
                strSQL = "SELECT *  FROM " & STR_TABLE_SHOPPER & " WHERE intShopperId=" & gintUserID & " AND chrStatus='A'"
	            set rsTemp = gobjConn.execute(strSQL)
                
                even = false         
                            
                while not rsTemp.EOF
                    %>
                    <tr>
                        <td><%= rsTemp("vchLabel") %><br /><%= rsTemp("vchFirstName") %>&nbsp;<%= rsTemp("vchLastName") %><br /><%= rsTemp("vchCity") %>,&nbsp;<%= rsTemp("vchState") %> </td>
                        <td><%= rsInput("vchItemName") %></td>
                        <td><%= rsInput("vchBundleQuantity") %></td>
                        <td><%= FormatCurrency(rsInput("mnyItemPrice")&"") %></td>
                        <td><input name="Item_<%= rsInput("intID") %>_<%= rsTemp("intID") %>" style="width:20px;" value="0" /></td>
                    </tr>
                    <%
                    rsTemp.MoveNext
                wend


            %>
                <tr>
                    <td colspan="5" align="center"><input type="submit" value="Add Item to Carts" onclick="$.unblockUI()" /><img src="../images/close_button.png" style="cursor:pointer;float:right;" onclick="$.unblockUI()" width="25" /></td>
                </tr>
            </table>
            

            </form>
            
            

        </div>

    </div>
    
    <%Else %>
    

    <div  style="float:left;width:262px;margin-bottom:30px;padding:20px;height:320px;">
        <table width="100%">
            <tr>
                <td align="center" valign="middle" style="width:200px">
                <% if rsInput("vchImageURL")&""="" then strPhotoURL = virtualbase & "images/icon-no-artwork-512.png" %>
                <div style="background-image:url(<%= strPhotoURL %>);width:150px;height:130px;margin-bottom:5px;background-repeat:no-repeat;background-size:cover;">
                
                </div>
                   <a class="fancybox" rel="group" href="<%= strPhotoURL %>" style="background-color:#fff;"><img src="<%= virtualbase & "images/magnifier_medium_left.png" %>" style="margin-bottom:20px;height:25px;width:25px;" ></a>
                
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <strong>Slot:</strong> <%= rsinput("vchslotname") %><br>
				<strong>Name:</strong> <%= rsInput("vchItemName") %><br>
                <strong>Size:</strong> <%= rsInput("vchSize") %><br>
                <strong>Sides:</strong> <%= rsInput("vchNumSides") %><br>
                <strong>Artwork Status:</strong> <% if isnull(rsInput("vchImageURL")) then response.write "Not Received" else response.write "Received" %><br>
				<div style="display:none"><div id="spec<%= rsInput("intID") %>">
				</div></div>
				<% if Session(SESSION_MERCHANT_UID) <> "" then %>
					<% if rsInput("vchImageURL")&""="" then %>
						<input class="iframe" type="button" data-fancybox-type="iframe" data-fancybox-href="../admin/merchant/inv_uploader.asp?id=<%= rsInput("intID") %>" value="Upload Thumbnail" /> 
					<% Else %>
						<% if rsInput("chrStatus")&""="A" then %>
							<input class="iframe" type="button" data-fancybox-type="iframe" data-fancybox-href="../admin/merchant/inv_uploader.asp?id=<%= rsInput("intID") %>" value="Upload Thumbnail" style="margin-bottom:5px" /> <br />
							<input type="button" id="approve_<%= rsInput("intID") %>" value="Mark As Approved" />
						<% End If %>
					
						<% if rsInput("chrStatus")&""="S" then %>
							<input type="button" id="print_<%= rsInput("intID") %>" value="Mark As Printed" />
						<% End If %>
					<% End If %>
                <% End If %>
				</td>
            </tr>
        </table>
        
					<% if not rsInput("vchImageURL")&""="" then %>
         <script>
            			<% if rsInput("chrStatus")&""="A" then %>
            $("#approve_<%= rsInput("intID") %>").click(function() {
				$.ajax({
					type: "POST",
					url: "ajaxcallback.asp",
					data: { action: "approve", id: "<%= rsInput("intID") %>" }
				})
				.done(function (msg) {
				});
				location.reload(); 
            });
						<% End If %>
						<% if rsInput("chrStatus")&""="S" then %>
            $("#print_<%= rsInput("intID") %>").click(function() {
				$.ajax({
					type: "POST",
					url: "ajaxcallback.asp",
					data: { action: "print", id: "<%= rsInput("intID") %>" }
				})
				.done(function (msg) {
				});
				location.reload(); 
            });
						<% End If %>
         </script>
					<% End If %>
      </div>

    <%end if %>
<%
end sub


sub DrawItemLt(rsInput, blnLeftImage)
	dim strPhotoURL, strURL, strImgURL, strDescription, strQtyBreaks, vchSize
	dim intMinQty, intMaxQty
	strDescription = rsInput("txtDescription")
	vchSize = rsInput("vchSpecifications")
	intMinQty = rsInput("intMinQty")
	intMaxQty = rsInput("intMaxQty")
	if IsNumeric(intMinQty) and intMinQty > 1 then
	else
		intMinQty = 1
	end if
	dim strOptionList1, strOptionList2, strOptionList3
	strOptionList1 = rsInput("vchOptionList1")
	strOptionList2 = rsInput("vchOptionList2")
	strOptionList3 = rsInput("vchOptionList3")
    strQtyBreaks = rsInput("vchQtyBreaks")&""

    if strQtyBreaks<>"" then strQtyBreaks = "<br />" & strQtyBreaks

	strPhotoURL = "../images/products/big/" & rsInput("vchImageURL") & ""
	if rsInput("chrType") = "I" then
		intItemCount = intItemCount + 1
		strURL = "itemdetail.asp?id=" & rsInput("intID")
	else
		intFolderCount = intFolderCount + 1
		strURL = "default.asp?parentid=" & rsInput("intID")
	end if
	%>
    <div  style="float:left;width:200px;margin-right:20px;margin-top:10px;padding:10px;min-height:400px;">
        <table width="100%">
            
            <tr>
                <td align="center" valign="middle">
                <% if rsInput("vchImageURL")&""="" then strPhotoURL = virtualbase & "images/icon-no-image-512.png" %>
                <div style="background-image:url(<%= strPhotoURL %>);margin-bottom:5px;width:180px;height:130px;background-repeat:no-repeat;background-size:cover;">
                </div>
                  <a class="fancybox" rel="group" href="<%= strPhotoURL %>" style="background-color:#fff;"><img src="<%= virtualbase & "images/magnifier_medium_left.png"  %>" height="25" width="25" alt="" /></a>                
                </td>
            </tr>
            
            <tr>
                <td style="height:40px;vertical-align:top;font-weight:bold;"><%= rsInput("vchItemName") %></td>
            </tr>
            
            <tr>
                <% if rsInput("intStock") > 0 then %>
                    <td>Qty Available: <%= rsInput("intStock") %></td>
                <% else %>
                    <td>Qty Available:  Out of stock</td>
                <% end if %>
            </tr>
            
			<% if intMaxQty <> "" then %>
                <tr>
                    <td>Max Order Qty: <%= intMaxQty %></td>
                </tr>
			<% end if %>
            
			<% if vchSize&""<>"" Then %>
                <tr>
                    <td>Size: <%= vchSize %></td>
                </tr>
			<% end if %>
            
            <tr>
                <td>Item Number: <%= rsInput("vchPartNumber") %></td>
            </tr>
            
            <% if strDescription <> "" then %>
                <tr>
                    <td><%= strDescription %></td>
                </tr>
            <% end if %> 
            
			<% If rsInput("intStock") > 0 Then %>
                <tr>
                    <td>Qty: <input type="number" name="<%= rsInput("intID") %>_QTY" size="3" style="width: 80px !important" /></td>
                </tr>
			<% end if %>
            
        </table>
        
        <style>
            .blockOverlay { cursor:pointer !important;background-color:#f0f0f0 !important;}
            #table_<%= rsInput("intID") %> th,#table_<%= rsInput("intID") %> td
            {
            padding:10px !important;
            }
       
        </style>
    </div>
<%
	response.flush
end sub


sub DrawItemX(rsInput, blnLeftImage)
	response.write "<TR><TD><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=""100%"">" & vbCRLF
	response.write "<TR>" & vbCRLF
	DrawItemCell_Aligned rsInput, blnLeftImage
	response.write "</TR>" & vbCRLF
	if rsInput("chrType") = "I" then
		DrawItemCell_PriceInfo rsInput
	end if
	DrawItemCell_Divider rsInput
	response.write "</TABLE></TD></TR>" & vbCRLF
end sub

sub DrawItemCell_Image(rsInput)
	dim strPhotoURL, strURL
	
	strPhotoURL = rsInput("vchImageURL") & ""
	if rsInput("chrType") = "I" then
		strURL = "itemdetail.asp?id=" & rsInput("intID")
	else
		strURL = "default.asp?parentid=" & rsInput("intID")
	end if
	response.write "<TD>"
	if strPhotoURL <> "" then
		response.write "<A HREF=""" & strURL & """>"
		response.write "<IMG SRC=""" & imagebase & "products/small/" & strPhotoURL & """"
		if not IsNull(rsInput("intSmImageWidth")) then
			response.write " WIDTH=""" & rsInput("intSmImageWidth") & """ HEIGHT=""" & rsInput("intSmImageHeight") & """"
		end if
		response.write " BORDER=0>"
		response.write "</A>"
	end if
	response.write "</TD>" & vbCRLF
end sub

sub DrawItemCell_Space(rsInput)
%>
			<img class="hr" src="<%= g_imagebase %>hr.gif" width="528" height="3" alt="" /><br />
<%
end sub

sub DrawItemCell_Details(rsInput)
	dim strURL
	if rsInput("chrType") = "I" then
		strURL = "itemdetail.asp?id=" & rsInput("intID")
	else
		strURL = "default.asp?parentid=" & rsInput("intID")
	end if
	response.write "<TD>" & font(1) & "<B><A HREF=""" & strURL & """>" & fontx(3,3,cHeading) & rsInput("vchItemName") & "</FONT></A></B><BR>" & vbCRLF
	response.write Server.HTMLEncode(rsInput("txtDescription") & "") & "<BR><BR>" & vbCRLF
	response.write "<B><A HREF=""" & strURL & """><IMG SRC=""" & imagebase & "new_buttons/btn_details.gif"" WIDTH=57 HEIGHT=15 BORDER=0 ALT=""Product Details""></A></B>"
	response.write "</FONT></TD>" & vbCRLF
end sub

sub DrawItemCell_Aligned(rsInput, blnLeftImage)
	dim strPhotoURL, strURL
	
	if rsInput("chrType") = "I" then
		strURL = "itemdetail.asp?id=" & rsInput("intID")
	else
		strURL = "default.asp?parentid=" & rsInput("intID")
	end if
	strPhotoURL = rsInput("vchImageURL") & ""
	
	response.write "<TD>" & font(1)
	if strPhotoURL <> "" then
		response.write "<A HREF=""" & strURL & """>"
		response.write "<IMG SRC=""" & imagebase & "products/small/" & strPhotoURL & """"
		if not IsNull(rsInput("intSmImageWidth")) then
			response.write " WIDTH=""" & rsInput("intSmImageWidth") & """ HEIGHT=""" & rsInput("intSmImageHeight") & """"
		end if
		response.write " HSPACE=""10"" BORDER=0 ALIGN=""" & iif(blnLeftImage, "LEFT", "RIGHT") & """>"
		response.write "</A>"
	end if
	response.write "<B><A HREF=""" & strURL & """>" & fontx(3,3,cHeading) & rsInput("vchItemName") & "</FONT></A></B><BR>" & vbCRLF
	response.write Server.HTMLEncode(rsInput("txtDescription") & "") & "<BR><BR>" & vbCRLF
	response.write "</FONT></TD>" & vbCRLF
end sub

sub DrawItemCell_PriceInfo(rsInput)
	dim strURL, intMinQty
	
	if rsInput("chrType") = "I" then
		strURL = "itemdetail.asp?id=" & rsInput("intID")
	else
		strURL = "default.asp?parentid=" & rsInput("intID")
	end if
	intMinQty = rsInput(intMinQty)
	if intMinQty < 1 then
		intMinQty = 1
	end if
%>
	<TR>
		<TD COLSPAN=3><BR>
			<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%">
			<FORM ACTION="cartlist.asp?action=additem" METHOD="POST">
			<INPUT TYPE="hidden" NAME="id" VALUE="<%= rsInput("intID") %>">
			<INPUT TYPE="hidden" NAME="parentid" VALUE="<%= rsInput("intParentID") %>">
			<TR BGCOLOR="#F4E9D2">
				<TD>
					<%= fontx(2,2,cRed) %><B>Price: <%= SafeFormatCurrency("$0.00", rsInput("mnyItemPrice"), 2) %></B></FONT>
					<%= font(1) %><B><A HREF="<%= strURL %>"><IMG SRC="<%= imagebase %>new_buttons/btn_details.gif" WIDTH=57 HEIGHT=15 BORDER=0 ALT=""Product Details"" ALIGN=ABSMIDDLE></A></B></FONT>
				</TD>
				<TD ALIGN="right"><%= font(1) %><B>Qty:</B> <INPUT TYPE="text" NAME="qty" VALUE="<%= intMinQty %>" SIZE=2 MAXLENGTH=5> <INPUT TYPE="image" SRC="<%= imagebase %>new_buttons/btn_additem.gif" WIDTH="57" HEIGHT="15" ALT="Add to Cart" BORDER="0" ALIGN=ABSMIDDLE VSPACE=3></TD>
			</TR>
			</FORM>
		<%
			if not IsNull(rsInput("intForceShipMethod")) then
				response.write "<TR><TD COLSPAN=2 ALIGN=""center"">" & fontx(2,2,cRed) & "Special Shipping Required</TD></TR>"
				'response.write "<TR><TD COLSPAN=2 ALIGN=""center"">" & fontx(2,2,cRed) & "This item must be shipped <B>" & GetArrayValue(rsInput("intForceShipMethod"), dctShipOption) & "</B>.</TD></TR>"
			end if
			if rsInput("chrForceSoloItem") = "Y" then
				response.write "<TR><TD COLSPAN=2 ALIGN=""center"">" & fontx(2,2,cRed) & "This item must be <B>ordered separately</B>.</TD></TR>"
			end if
			if rsInput("intMinQty") > 1 then
				response.write "<TR><TD COLSPAN=2 ALIGN=""center"">" & fontx(2,2,cRed) & "Minimum <B>ordered quanity</B> required.</TD></TR>"
			end if
		%>
			</TABLE>
		</TD>
	</TR>
<%
end sub

sub DrawItemCell_Divider(rsInput)
	'response.write "<TR><TD COLSPAN=3>" & spacer(1,10) & "<BR><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH=""100%"" BGCOLOR=""" & cGoCart_InvDivider & """><TR><TD>" & spacer(1,1) & "</TD></TR></TABLE>" & spacer(1,10) & "</TD></TR>"
	response.write "<TR><TD COLSPAN=3>&nbsp;<BR>&nbsp;</TD></TR>"
end sub

sub CheckInventoryAmt(ShopperID)
	dim strInvSQL, rsInvData, strQtySQL, rsQtySQL, dctTotalOfItems, x, a, i
	set dctTotalOfItems = CreateObject("Scripting.Dictionary")
	
	strInvSQL = "SELECT O.intID as OrderID, L.IntId as Item_ID, L.intInvID as Inv_ID, L.vchItemName as Item_Name, L.intQuantity as Quantity FROM " & STR_TABLE_ORDER_LONG_TERM & " O"
	strInvSQL = strInvSQL & " INNER JOIN " & STR_TABLE_LINEITEM_LONG_TERM & " AS L ON O.intID=L.intOrderID AND (L.chrStatus='A' or L.chrStatus='C') "
	strInvSQL = strInvSQL & " INNER JOIN " & STR_TABLE_INVENTORY_LONG_TERM & " AS I ON I.intID=L.intInvID AND I.chrStatus='A' "
	strInvSQL = strInvSQL & " WHERE O.intShopperId=" & Session(SESSION_PUB_USER_ID) & " AND O.chrStatus in ('0','9') AND O.intBillShopperID is not null"
	'response.write "<div style=""display:none"">" & strInvSQL & "</div>"
	set rsInvData = gobjConn.execute(strInvSQL)
	i = 0
	while not rsInvData.EOF
		strQtySQL = "Select * From " & STR_TABLE_INVENTORY_LONG_TERM & " where intID = " & rsInvData("Inv_ID")
		set rsQtySQL = gobjConn.execute(strQtySQL)
		
		if not rsQtySQL.EOF then
			
			if dctTotalOfItems.Exists(rsInvData("Item_Name")&"") = true then 
				dctTotalOfItems.Item(rsInvData("Item_Name")&"") = CLNG(dctTotalOfItems.Item(rsInvData("Item_Name")&"")) + CLNG(rsInvData("Quantity"))
			else 
				dctTotalOfItems.Add rsInvData("Item_Name")&"", CLNG(rsInvData("Quantity"))
			end if
			
			if rsQtySQL("intMinQty") > rsInvData("Quantity") then
				dctErrors.Add rsInvData("Item_ID") & "", "You must order at least " & rsQtySQL("intMinQty") &  " of " & replace(rsInvData("Item_Name"), """", "\""") & "."
				dctOrderItemError.Add i, rsInvData("OrderID") & " " & rsInvData("Inv_ID")
			elseif rsQtySQL("intMaxQty") < rsInvData("Quantity") then
				dctErrors.Add rsInvData("Item_ID") & "", "You cannot order more than " & rsQtySQL("intMaxQty") &  " of " & replace(rsInvData("Item_Name"), """", "\""") & "."
				dctOrderItemError.Add i, rsInvData("OrderID") & " " & rsInvData("Inv_ID")
			end if
			
			if CLNG(dctTotalOfItems.Item(rsInvData("Item_Name")&"")) > rsQtySQL("intStock") and instr(rsQtySQL("vchPartNumber"), "_POD_") = 0 then
				if not dctErrors.Exists(rsInvData("Item_ID")&"") then 
					dctErrors.Add rsInvData("Item_ID")&"", "There is/are only " & rsQtySQL("intStock") &  " left of " & replace(rsInvData("Item_Name"), """", "\""") & " in stock." 
				end if 
			end if
			
			rsQtySQL.MoveNext
			'response.write "<div style=""display:none"">" & rsInvData("Item_Name") & " " & dctTotalOfItems.Item(rsInvData("Item_Name")&"") & "</div>"
		end if
		
		rsQtySQL.close
		set rsQtySQL = nothing
		
		i = i + 1
		rsInvData.MoveNext
	wend
	
	
	rsInvData.close
	set rsInvData = nothing
	
	'Debug leftovers
	'response.write "errors: "
	'a = dctErrors.Items
	'for x = 0 to dctErrors.count - 1
	'	response.write a(x) & "<br/>"
	'next
	
	'response.write "errors: "
	'a = dctOrderItemError.Items
	'for x = 0 to dctOrderItemError.count - 1
	'	response.write a(x) & "<br/>"
	'next
	
end sub

sub AddItemsToCart
'For Each item In Request.Form
'    Response.Write "Key: " & item & " - Value: " & Request.Form(item) & "<BR />"
'Next
'response.end
    intMassQty = Request("mass_qty")

    if IsNumeric(intMassQty) and intMassQty<>"" then
        intMassQty = clng(intMassQty)
    else
        intMassQty = 0
    end if

	' mred: optimizing queries outside the loop
	dim dctOrders, dctShoppers, dctItemQTY, key, key2, debugString, intItemInvID
    set dctOrders = Server.CreateObject("Scripting.Dictionary")
    set dctShoppers = Server.CreateObject("Scripting.Dictionary")
    set dctItemQTY = Server.CreateObject("Scripting.Dictionary")
	
	strSQL = "SELECT intBillShopperID, intID FROM " & STR_TABLE_ORDER_LONG_TERM & " WHERE chrStatus='0'"
	set rsTemp = gobjConn.execute(strSQL)
	while not rsTemp.EOF
		if not dctOrders.Exists(rsTemp(0)&"") Then dctOrders.Add rsTemp(0)&"",rsTemp(1)&""
		rsTemp.MoveNext
	wend
	
	for each Item in Request.Form
		
		fieldName = Item
		fieldValue = Request.Form(fieldName)
        if Right(fieldName,4)="_QTY" then
		'response.write fieldValue
			if fieldValue&"" <> "" and IsNumeric(fieldValue) then 
				dctItemQTY.Add fieldName, fieldValue
			end if
        elseif Left(fieldName,6)="Store_" then
			if fieldValue = "true" then
				dctShoppers.Add fieldName, fieldValue
			end if
        end if
    Next
	
	Set rsTemp = Nothing
	
    for each key in dctShoppers.Keys
        intShipShopperID = CLNG(Replace(key,"Store_",""))
        
				
		If dctOrders.Exists(intShipShopperID&"") Then
			intOrderId = dctOrders(intShipShopperID&"")
		Else
            CreateNewOrderLt
            intOrderId = gintOrderID
		End If
				
        strSQL = "UPDATE " & STR_TABLE_ORDER_LONG_TERM & " SET intShopperID=(SELECT intShopperID FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & intShipShopperID & "), intBillShopperId=" & intShipShopperID & ", intShipShopperId=" & intShipShopperID & " WHERE intID=" & intOrderId
	    debugString = debugString & strSQL
		call gobjConn.execute(strSQL)
		
		for each key2 in dctItemQTY.Keys
			intItemInvID = CLNG(Replace(key2,"_QTY",""))
            AddItemToOrder_OtherLt intOrderId, intItemInvID, dctItemQTY(key2)  
			debugString = debugString & "<br />" & intItemInvID & "," & dctItemQTY(key2)  
        next
        
    Next
	'response.write debugString
	'response.end
    %><script>parent.location = 'default.asp?lt=1&campaignid=<%=intCampaignId %>';</script> <%
end sub

sub AddToProfilePackage
	intItemInvID
	intCampaignId
	dim dctProfiles, strInsertSQL, intProfileID, intCount
	
	set dctProfiles = Server.CreateObject("Scripting.Dictionary")
	
	for each Item in Request.Form
		
		fieldName = Item
		fieldValue = Request.Form(fieldName)
        if Left(fieldName,8)="profile-" then
			if fieldValue = "true" then
				dctProfiles.Add fieldName, fieldValue
			end if
        end if
    Next
	
	strInsertSQL = "insert into " & STR_TABLE_PROFILE_PACKAGES & " (intCampaignID, intProfileID, intItemID) values "
	intCount = 0 
	for each key in dctProfiles.Keys
		intCount = intCount + 1
		intProfileID = CLNG(Replace(key,"profile-",""))
		strInsertSQL = strInsertSQL & "(" & intCampaignId & "," & intProfileID & ", " & intItemInvID & ")"
		if intCount <> dctProfiles.Count then
			strInsertSQL = strInsertSQL & ","
		end if
	next
end sub

sub CheckPreBuyInventoryAmt(ShopperID)
	dim strInvSQL, rsInvData, strQtySQL, rsQtySQL, dctTotalOfItems, x, a, i
	set dctTotalOfItems = CreateObject("Scripting.Dictionary")
	
	strInvSQL = "SELECT O.intID as OrderID, L.IntId as Item_ID, L.intInvID as Inv_ID, L.vchItemName as Item_Name, L.intQuantity as Quantity FROM " & STR_TABLE_ORDER & " O"
	strInvSQL = strInvSQL & " LEFT JOIN " & STR_TABLE_LINEITEM & " AS L ON O.intID=L.intOrderID AND (L.chrStatus='A' or L.chrStatus='C') "
	strInvSQL = strInvSQL & " LEFT JOIN " & STR_TABLE_INVENTORY & " AS I ON I.intID=L.intInvID AND I.chrStatus='A' "
	strInvSQL = strInvSQL & " WHERE O.intShopperId=" & ShopperID & " AND O.chrStatus in ('0','9') AND O.intBillShopperID is not null"
	'response.write "<div style=""display:none"">" & strInvSQL & "</div>"
	set rsInvData = gobjConn.execute(strInvSQL)
	i = 0
	while not rsInvData.EOF
		strQtySQL = "Select * From " & STR_TABLE_INVENTORY & " where intID = " & rsInvData("Inv_ID")
		set rsQtySQL = gobjConn.execute(strQtySQL)
		
		if not rsQtySQL.EOF then
			
			if dctTotalOfItems.Exists(rsInvData("Item_Name")&"") = true then 
				dctTotalOfItems.Item(rsInvData("Item_Name")&"") = cint(dctTotalOfItems.Item(rsInvData("Item_Name")&"")) + cint(rsInvData("Quantity"))
			else 
				dctTotalOfItems.Add rsInvData("Item_Name")&"", cint(rsInvData("Quantity"))
			end if
			
			if rsQtySQL("intMinQty") >= rsInvData("Quantity") then
				dctErrors.Add rsInvData("Item_Name") & "-min", "You must order at least " & rsQtySQL("intMinQty") &  " of " & rsInvData("Item_Name") & "."
				dctOrderItemError.Add i, rsInvData("OrderID") & " " & rsInvData("Inv_ID")
			elseif rsQtySQL("intMaxQty") <= rsInvData("Quantity") then
				dctErrors.Add rsInvData("Item_Name") & "-max", "You cannot order more than " & rsQtySQL("intMaxQty") &  " of " & rsInvData("Item_Name") & "."
				dctOrderItemError.Add i, rsInvData("OrderID") & " " & rsInvData("Inv_ID")
			end if
			
			if cint(dctTotalOfItems.Item(rsInvData("Item_Name")&"")) > rsQtySQL("intStock")  then
				if not dctErrors.Exists(rsInvData("Item_Name")&"") then 
					dctErrors.Add rsInvData("Item_Name")&"", "There is/are only " & rsQtySQL("intStock") &  " left of " & rsInvData("Item_Name") & " in stock." 
				end if 
			end if
			
			rsQtySQL.MoveNext
			'response.write "<div style=""display:none"">" & rsInvData("Item_Name") & " " & dctTotalOfItems.Item(rsInvData("Item_Name")&"") & "</div>"
		end if
		
		rsQtySQL.close
		set rsQtySQL = nothing
		
		i = i + 1
		rsInvData.MoveNext
	wend
	
	
	rsInvData.close
	set rsInvData = nothing
	
	'Debug leftovers
	'response.write "errors: "
	'a = dctErrors.Items
	'for x = 0 to dctErrors.count - 1
	'	response.write a(x) & "<br/>"
	'next
	
	'response.write "errors: "
	'a = dctOrderItemError.Items
	'for x = 0 to dctOrderItemError.count - 1
	'	response.write a(x) & "<br/>"
	'next
	
end sub

sub CheckForApprovalItems(ShopperID)
	dim strInvSQL, rsInvData, strApprovalFolderSQL, rsApprovalFolder, strOrderSQL, rsOrderData, dctApprovalA, dctApprovalB, dctApprovalC, dctApprovalD, dctApprovalE, dctApprovalF, dctApprovalG, dctApprovalH, dctApprovalI, dctApprovalJ, dctApprovalFolderIDs, intOrderID, x, a, i
	set dctApprovalA = CreateObject("Scripting.Dictionary")
	set dctApprovalB = CreateObject("Scripting.Dictionary")
	set dctApprovalC = CreateObject("Scripting.Dictionary")
	set dctApprovalD = CreateObject("Scripting.Dictionary")
	set dctApprovalE = CreateObject("Scripting.Dictionary")
	set dctApprovalF = CreateObject("Scripting.Dictionary")
	set dctApprovalG = CreateObject("Scripting.Dictionary")
	set dctApprovalH = CreateObject("Scripting.Dictionary")
	set dctApprovalI = CreateObject("Scripting.Dictionary")
	set dctApprovalJ = CreateObject("Scripting.Dictionary")
	set dctApprovalFolderIDs = CreateObject("Scripting.Dictionary")
	
	strApprovalFolderSQL = "select intID, vchItemName from " & STR_TABLE_INVENTORY_LONG_TERM & " where chrType = 'A' and vchItemName like '%Approval%'"
	'response.write strApprovalFolderSQL & "<br />"
	set rsApprovalFolder = gobjConn.execute(strApprovalFolderSQL)
	i = 0
	while not rsApprovalFolder.EOF
		dctApprovalFolderIDs.Add  rsApprovalFolder("intID")&"", i
		'response.write rsApprovalFolder("intID")&"<br / >"
		rsApprovalFolder.MoveNext 
		i = i + 1
	wend
	
	strOrderSQL = "Select intID as OrderID, intBillShopperID as BillID From " & STR_TABLE_ORDER_LONG_TERM & " WHERE intShopperId=" & ShopperID & " AND chrStatus in ('0','9') AND intBillShopperID is not null"
	set rsOrderData = gobjConn.execute(strOrderSQL)
	while not rsOrderData.EOF
		strInvSQL = "SELECT O.intID as OrderID, L.IntId as Item_ID, L.intInvID as Inv_ID, L.vchItemName as Item_Name, L.intQuantity as Quantity, I.intParentID as Parent, I.vchPartNumber as PartNumber, O.intBillShopperID as BillID FROM " & STR_TABLE_ORDER_LONG_TERM & " O"
		strInvSQL = strInvSQL & " LEFT JOIN " & STR_TABLE_LINEITEM_LONG_TERM & " AS L ON O.intID=L.intOrderID AND (L.chrStatus='A' or L.chrStatus='C') "
		strInvSQL = strInvSQL & " LEFT JOIN " & STR_TABLE_INVENTORY_LONG_TERM & " AS I ON I.intID=L.intInvID AND I.chrStatus='A' "
		strInvSQL = strInvSQL & " WHERE O.intID=" & rsOrderData("OrderID") & " AND L.intQuantity is not null"
		'response.write strInvSQL & "<br />"
		set rsInvData = gobjConn.execute(strInvSQL)
		while not rsInvData.EOF
		
			if dctApprovalFolderIDs.Exists(rsInvData("Parent")&"") = true then 
				'response.write "BillID: " & rsInvData("BillID") & " instr: " & instr(rsInvData("PartNumber")&"", "A") & " len: " & Len(rsInvData("PartNumber")&"") & "<br />"
				if dctApprovalFolderIDs.Item(rsInvData("Parent")&"") = 0 then
					dctApprovalA.Add rsInvData("Item_ID")&"", rsInvData("BillID")&""
				elseif dctApprovalFolderIDs.Item(rsInvData("Parent")&"") = 1 then
					dctApprovalB.Add rsInvData("Item_ID")&"", rsInvData("BillID")&""
				elseif dctApprovalFolderIDs.Item(rsInvData("Parent")&"") = 2 then
					dctApprovalC.Add rsInvData("Item_ID")&"", rsInvData("BillID")&""
				elseif dctApprovalFolderIDs.Item(rsInvData("Parent")&"") = 3 then
					dctApprovalD.Add rsInvData("Item_ID")&"", rsInvData("BillID")&""
				elseif dctApprovalFolderIDs.Item(rsInvData("Parent")&"") = 4 then
					dctApprovalE.Add rsInvData("Item_ID")&"", rsInvData("BillID")&""
				elseif dctApprovalFolderIDs.Item(rsInvData("Parent")&"") = 5 then
					dctApprovalF.Add rsInvData("Item_ID")&"", rsInvData("BillID")&""
				elseif dctApprovalFolderIDs.Item(rsInvData("Parent")&"") = 6 then
					dctApprovalG.Add rsInvData("Item_ID")&"", rsInvData("BillID")&""
				elseif dctApprovalFolderIDs.Item(rsInvData("Parent")&"") = 7 then
					dctApprovalH.Add rsInvData("Item_ID")&"", rsInvData("BillID")&""
				elseif dctApprovalFolderIDs.Item(rsInvData("Parent")&"") = 8 then
					dctApprovalI.Add rsInvData("Item_ID")&"", rsInvData("BillID")&""
				elseif dctApprovalFolderIDs.Item(rsInvData("Parent")&"") = 9 then
					dctApprovalJ.Add rsInvData("Item_ID")&"", rsInvData("BillID")&""
				end if
			end if
			
			'response.write "item: " & rsInvData("Item_ID") & " parent: " & rsInvData("Parent") & "<br />"
			'response.write dctApprovalFolderIDs.Item(rsInvData("Parent")&"") & "<br />"
			
			rsInvData.MoveNext
		wend
		'response.end
		
		rsInvData.close
		set rsInvData = nothing
		
		'Debug leftovers
		'response.write "count A: " & dctApprovalA.count & " Approval A Items: "
		'a = dctApprovalA.Keys
		'for x = 0 to dctApprovalA.count - 1
		'	response.write a(x) & "<br/>"
		'next
	
		'response.write "<br />count B: " & dctApprovalB.count & " Approval B Items: "
		'a = dctApprovalB.Keys
		'for x = 0 to dctApprovalB.count - 1
		'	response.write a(x) & "<br/>"
		'next
		
		if dctApprovalA.count >0 then
			CreateNewOrderLt
			intOrderId = gintOrderID
			a = dctApprovalA.Keys
			strSQL = "Update " & STR_TABLE_LINEITEM_LONG_TERM & " set intOrderID = " & intOrderId & " where intID in ("
			for x = 0 to dctApprovalA.count - 1
				strSQL = strSQL & a(x)
				if x <> dctApprovalA.count -1 then
					strSQL = strSQL & ","
				end if
			next
			strSQL = strSQL & "); Update " & STR_TABLE_ORDER_LONG_TERM & " set intBillShopperID = " & rsOrderData("BillID") & ", intShipShopperID = " & rsOrderData("BillID") & " where intID = " & intOrderId
			'response.write strSQL
			'response.end
			gobjConn.execute(strSQL)
		end if 
	
		if dctApprovalB.count >0 then
			CreateNewOrderLt
			intOrderId = gintOrderID
			a = dctApprovalB.Keys
			strSQL = "Update " & STR_TABLE_LINEITEM_LONG_TERM & " set intOrderID = " & intOrderId & " where intID in ("
			for x = 0 to dctApprovalB.count - 1
				strSQL = strSQL & a(x)
				if x <> dctApprovalB.count -1 then
					strSQL = strSQL & ","
				end if
			next
			strSQL = strSQL & "); Update " & STR_TABLE_ORDER_LONG_TERM & " set intBillShopperID = " & rsOrderData("BillID") & ", intShipShopperID = " & rsOrderData("BillID") & " where intID = " & intOrderId
			gobjConn.execute(strSQL)
		end if 
	
		if dctApprovalC.count >0 then
			CreateNewOrderLt
			intOrderId = gintOrderID
			a = dctApprovalC.Keys
			strSQL = "Update " & STR_TABLE_LINEITEM_LONG_TERM & " set intOrderID = " & intOrderId & " where intID in ("
			for x = 0 to dctApprovalC.count - 1
				strSQL = strSQL & a(x)
				if x <> dctApprovalC.count -1 then
					strSQL = strSQL & ","
				end if
			next
			strSQL = strSQL & "); Update " & STR_TABLE_ORDER_LONG_TERM & " set intBillShopperID = " & rsOrderData("BillID") & ", intShipShopperID = " & rsOrderData("BillID") & " where intID = " & intOrderId
			gobjConn.execute(strSQL)
		end if 
		
		if dctApprovalD.count >0 then
			CreateNewOrderLt
			intOrderId = gintOrderID
			a = dctApprovalD.Keys
			strSQL = "Update " & STR_TABLE_LINEITEM_LONG_TERM & " set intOrderID = " & intOrderId & " where intID in ("
			for x = 0 to dctApprovalD.count - 1
				strSQL = strSQL & a(x)
				if x <> dctApprovalD.count -1 then
					strSQL = strSQL & ","
				end if
			next
			strSQL = strSQL & "); Update " & STR_TABLE_ORDER_LONG_TERM & " set intBillShopperID = " & rsOrderData("BillID") & ", intShipShopperID = " & rsOrderData("BillID") & " where intID = " & intOrderId
			gobjConn.execute(strSQL)
		end if 
		
		if dctApprovalE.count >0 then
			CreateNewOrderLt
			intOrderId = gintOrderID
			a = dctApprovalE.Keys
			strSQL = "Update " & STR_TABLE_LINEITEM_LONG_TERM & " set intOrderID = " & intOrderId & " where intID in ("
			for x = 0 to dctApprovalE.count - 1
				strSQL = strSQL & a(x)
				if x <> dctApprovalE.count -1 then
					strSQL = strSQL & ","
				end if
			next
			strSQL = strSQL & "); Update " & STR_TABLE_ORDER_LONG_TERM & " set intBillShopperID = " & rsOrderData("BillID") & ", intShipShopperID = " & rsOrderData("BillID") & " where intID = " & intOrderId
			gobjConn.execute(strSQL)
		end if 
		
		if dctApprovalF.count >0 then
			CreateNewOrderLt
			intOrderId = gintOrderID
			a = dctApprovalF.Keys
			strSQL = "Update " & STR_TABLE_LINEITEM_LONG_TERM & " set intOrderID = " & intOrderId & " where intID in ("
			for x = 0 to dctApprovalF.count - 1
				strSQL = strSQL & a(x)
				if x <> dctApprovalF.count -1 then
					strSQL = strSQL & ","
				end if
			next
			strSQL = strSQL & "); Update " & STR_TABLE_ORDER_LONG_TERM & " set intBillShopperID = " & rsOrderData("BillID") & ", intShipShopperID = " & rsOrderData("BillID") & " where intID = " & intOrderId
			gobjConn.execute(strSQL)
		end if 
		
		if dctApprovalG.count >0 then
			CreateNewOrderLt
			intOrderId = gintOrderID
			a = dctApprovalG.Keys
			strSQL = "Update " & STR_TABLE_LINEITEM_LONG_TERM & " set intOrderID = " & intOrderId & " where intID in ("
			for x = 0 to dctApprovalG.count - 1
				strSQL = strSQL & a(x)
				if x <> dctApprovalG.count -1 then
					strSQL = strSQL & ","
				end if
			next
			strSQL = strSQL & "); Update " & STR_TABLE_ORDER_LONG_TERM & " set intBillShopperID = " & rsOrderData("BillID") & ", intShipShopperID = " & rsOrderData("BillID") & " where intID = " & intOrderId
			gobjConn.execute(strSQL)
		end if 
		
		if dctApprovalH.count >0 then
			CreateNewOrderLt
			intOrderId = gintOrderID
			a = dctApprovalH.Keys
			strSQL = "Update " & STR_TABLE_LINEITEM_LONG_TERM & " set intOrderID = " & intOrderId & " where intID in ("
			for x = 0 to dctApprovalH.count - 1
				strSQL = strSQL & a(x)
				if x <> dctApprovalH.count -1 then
					strSQL = strSQL & ","
				end if
			next
			strSQL = strSQL & "); Update " & STR_TABLE_ORDER_LONG_TERM & " set intBillShopperID = " & rsOrderData("BillID") & ", intShipShopperID = " & rsOrderData("BillID") & " where intID = " & intOrderId
			gobjConn.execute(strSQL)
		end if 
		
		if dctApprovalI.count >0 then
			CreateNewOrderLt
			intOrderId = gintOrderID
			a = dctApprovalI.Keys
			strSQL = "Update " & STR_TABLE_LINEITEM_LONG_TERM & " set intOrderID = " & intOrderId & " where intID in ("
			for x = 0 to dctApprovalI.count - 1
				strSQL = strSQL & a(x)
				if x <> dctApprovalI.count -1 then
					strSQL = strSQL & ","
				end if
			next
			strSQL = strSQL & "); Update " & STR_TABLE_ORDER_LONG_TERM & " set intBillShopperID = " & rsOrderData("BillID") & ", intShipShopperID = " & rsOrderData("BillID") & " where intID = " & intOrderId
			gobjConn.execute(strSQL)
		end if 
		
		if dctApprovalJ.count >0 then
			CreateNewOrderLt
			intOrderId = gintOrderID
			a = dctApprovalJ.Keys
			strSQL = "Update " & STR_TABLE_LINEITEM_LONG_TERM & " set intOrderID = " & intOrderId & " where intID in ("
			for x = 0 to dctApprovalJ.count - 1
				strSQL = strSQL & a(x)
				if x <> dctApprovalJ.count -1 then
					strSQL = strSQL & ","
				end if
			next
			strSQL = strSQL & "); Update " & STR_TABLE_ORDER_LONG_TERM & " set intBillShopperID = " & rsOrderData("BillID") & ", intShipShopperID = " & rsOrderData("BillID") & " where intID = " & intOrderId
			gobjConn.execute(strSQL)
		end if 
		
		dctApprovalA.RemoveAll
		dctApprovalB.RemoveAll
		dctApprovalC.RemoveAll
		dctApprovalD.RemoveAll
		dctApprovalE.RemoveAll
		dctApprovalF.RemoveAll
		dctApprovalG.RemoveAll
		dctApprovalH.RemoveAll
		dctApprovalI.RemoveAll
		dctApprovalJ.RemoveAll
		'response.end
		
		rsOrderData.MoveNext
		
	wend
	'response.end
	rsOrderData.close
	set rsOrderData = nothing
	
	set rsOrderData = gobjConn.execute(strOrderSQL)
	while not rsOrderData.EOF
		ReCalcOrderLT_Other rsOrderData("OrderID")
		rsOrderData.MoveNext
	wend
	
end sub


sub PODOrderHistory
%>

<a href="pod_redirect.asp"><b>Click here to view your Print on Demand Order History</b></a>


<%
end sub

sub UpdateCheckedOutItems(ShopperID)
	dim strInvSQL, rsInvData, strQtySQL, rsQtySQL, dctTotalOfItems, x, a, i
	
	strInvSQL = "Insert into " & STR_TABLE_INVENTORY_LONG_TERM_UPDATE & " (dteCheckoutTime, intInvID, vchPartNumber, intStock) "
	strInvSQL = strInvSQL & " SELECT distinct GetDate(), I.intID as intID, L.vchPartNumber as vchPartNumber, I.intStock as intStock FROM " & STR_TABLE_ORDER_LONG_TERM & " O"
	strInvSQL = strInvSQL & " LEFT JOIN " & STR_TABLE_LINEITEM_LONG_TERM & " AS L ON O.intID=L.intOrderID AND (L.chrStatus='A' or L.chrStatus='C') "
	strInvSQL = strInvSQL & " LEFT JOIN " & STR_TABLE_INVENTORY_LONG_TERM & " AS I ON I.intID=L.intInvID AND I.chrStatus='A' "
	strInvSQL = strInvSQL & " WHERE O.intShopperId=" & ShopperID & " AND O.chrStatus in ('0','9') AND O.intBillShopperID is not null AND L.intQuantity is not null"
	gobjConn.execute(strInvSQL)
	
end sub

sub GetAddressBook(ShopperID)
	dim i, rsTemp
	
	strSQL = "SELECT *  FROM " & STR_TABLE_SHOPPER & " WHERE "
	if Session(SESSION_MERCHANT_ULOGIN)&"" <> "" then
		strSQL = strSQL & " intshopperid is not null and chrStatus='A'"
	else
		strSQL = strSQL & " intShopperId=" & gintUserID & " AND chrStatus='A'"
	end if
	set rsTemp = gobjConn.execute(strSQL)

	i = 0
	
    while not rsTemp.EOF
        dctAddressBook.Add i, rsTemp("vchLabel") & "|" & rsTemp("vchFirstName") & "|" & rsTemp("vchLastName") & "|" & rsTemp("vchCity") & "|" & rsTemp("vchState") & "|" & rsTemp("intID")
        rsTemp.MoveNext
		i = i + 1
    wend
	
end sub
%>