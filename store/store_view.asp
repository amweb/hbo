<!--#include file="../admin/config/incInit.asp"-->

<%
OpenConn

If gintUserName&"" = ""  Then
	Response.Redirect virtualbase
End if

DrawHeader "", ""
DrawLanding
AddToFooter

function DrawLanding
	dim strSQL, rsCampaigns, rsProfiles
	strSQL = "SELECT * FROM "
	%>
	<div class="page-content-wrapper">
        <!-- BEGIN PAGE HEADER-->
		<div class="page-content">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">Store View</h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<%= virtualbase %>store/dashboard.asp">Home</a>
                            <i class="fa fa-angle-right"></i>
                            <a href="#">Store View</a>
                        </li>
                    </ul>
                </div>
            </div>
			<%
			if Request.Form("sv_campaigndropdown_submitted")="" then
				strSQL = "SELECT COUNT([intID]) AS [numcampaigns] FROM " & STR_TABLE_INVENTORY & " WHERE  [chrtype] = 'C' AND [chrstatus] <> 'D' "
				strSQL = strSQL & "AND [intid] IN (SELECT DISTINCT [intcampaignid] FROM " & STR_TABLE_PROFILE_STORE_PACKAGE & " WHERE  [intlayoutid] IN ((SELECT DISTINCT [intid] FROM "
				strSQL = strSQL & STR_TABLE_LAYOUTS & " WHERE [intID]=(SELECT [intlayout] FROM " & STR_TABLE_SHOPPER & " WHERE [chrtype] = 'S' AND "
				strSQL = strSQL & "[intshopperid]=" & gintUserID & "))))"
				set rsCampaigns = gobjconn.execute(strSQL)
				if cint(rsCampaigns("numcampaigns")) > 0 then
			%>
			<form action="<%= virtualbase %>store/store_view.asp" id="sv_campaigndropdown" method="post">
				<label for="campaignid">Select campaign:</label>
				<select name="campaignid" id="sv_campaignid">
					<option value="">Select a campaign</option>
					<%
						strSQL = "SELECT [intID],[vchItemName] FROM " & STR_TABLE_INVENTORY & " WHERE  [chrtype] = 'C' AND [chrstatus] <> 'D' "
						strSQL = strSQL & "AND [intid] IN (SELECT DISTINCT [intcampaignid] FROM " & STR_TABLE_PROFILE_STORE_PACKAGE & " WHERE  [intlayoutid] IN ((SELECT DISTINCT [intid] FROM "
						strSQL = strSQL & STR_TABLE_LAYOUTS & " WHERE [intID]=(SELECT [intlayout] FROM " & STR_TABLE_SHOPPER & " WHERE [chrtype] = 'S' AND "
						strSQL = strSQL & "[intshopperid]=" & gintUserID & "))))"
						set rsCampaigns = gobjconn.execute(strSQL)
						while not rsCampaigns.eof
					%>
					<option value="<%= rsCampaigns("intID") %>" <%= iif(StrComp(Request.Form("campaignid"),rsCampaigns("intID"), 1)=0,"selected='true'", "") %>><%= rsCampaigns("vchItemName") %></option>
					<%
							rsCampaigns.movenext()
						wend
					%>
				</select>
				<!--
				<label for="profileid">Select profile:</label>
				<select name="profileid" id="sv_profileid">
					<option value="">Select a profile</option>
				</select>
				-->
				<input type="hidden" name="sv_campaigndropdown_submitted" value="true">
				<button type="submit" name="sv_campaigndropdown_submit">Submit</button>
			</form>
			<%
				else
			%>
			<div id="sv_nocampaigns"><p class="ui-widget"><span class="ui-icon ui-icon-alert"></span> No Available Campaigns</p></div>
			<%
				end if
			else
				dim strStoreLayoutSQL, rsLayoutInfo
				strStoreLayoutSQL = "SELECT [intID],[vchImageLocation] FROM [" & STR_TABLE_LAYOUTS & "] WHERE [intID]=(SELECT [intLayout] FROM " & STR_TABLE_SHOPPER & " WHERE [chrType]='S' AND [intShopperID]=" & gintUserID & ")"

				set rsLayoutInfo = gobjconn.execute(strStoreLayoutSQL)
				if not rsLayoutInfo.eof then
			%>
			<span id="campaignID"><%= Request.Form("campaignid") %></span>
			<span id="profileDropdown"><%= Request.Form("profileid") %></span>
			<span id="layoutsvgurl"><%= rsLayoutInfo("vchImageLocation") %></span>
			<script type="text/javascript">
				var vchLayoutImage = "<%= rsLayoutInfo("vchImageLocation") %>";
				var intLayoutID = "<%= rsLayoutInfo("intID") %>";
			</script>
			<div id="leftSide">
				<select id="zoomDD">
				</select>
				<label for="departmentToggler">Toggle Departments</label>
				<input name="departmentToggler" id="departmentToggler" value="departmentToggler" type="checkbox">
				<div id="printbuttons">
					<button type="button" name="printplanningdiagram"><i class="fa fa-print" aria-hidden="true"></i> Print Planning Diagram</button>
					<form action="<%= virtualbase %>store/exportsigndetails.asp" method="post">
						<input type="hidden" name="campaign" value="<%= Request.Form("campaignid") %>">
						<input type="hidden" name="layout" value="<%= rsLayoutInfo("intID") %>">
						<button type="submit"><i class="fa fa-download" aria-hidden="true"></i> Export Sign Details</button>
					</form>
				</div>
				<div id="stage" class="col-md-8"></div>
			</div>
			<div id="rightSide" class="col-md-4"></div>
			<div id="itemDialog" title="Create Signage"></div>
			<%
				else
					response.write "<p>You don't have any stores associated with your account.</p>"
					response.write "<p>ID number: " & gintUserID & "</p>"
				end if
			end if
			%>
		</div>
	</div>
	<%
end function

function AddToFooter
%>
	<script type="text/javascript">
		var AJAXurl = "<%= virtualbase %>config/ajax_functions-ft_sv_rsrcs.asp";
	</script>
	<script src="<%= virtualbase %>assets/admin/layout/scripts/file-tracker.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.6/d3.min.js" type="text/javascript"></script>
	<script src="<%= virtualbase %>assets/admin/layout/scripts/svg-handler.js?v=0.3.1a" type="text/javascript"></script>
	<!-- <link rel="stylesheet" href="<%= virtualbase %>assets/global/css/reset.css" type="text/css"> -->
	<link rel="stylesheet" href="<%= virtualbase %>assets/global/css/svg-colors.css" type="text/css">
<%
	DrawFooter ""
end function
%>
