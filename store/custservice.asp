<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: custserv.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2000 SacWeb, Inc. All rights reserved.
' = Description:
' =   GoCart Customer Service Center
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

'reseting this
Session("DummyUser")=""
OpenConn

SelectCurrentOrder

dim strAction, strReturnURL, gtfError, gstrHint


DrawHeader "Customer Service Center", "custserv"
DrawPage
DrawFooter "custserv"

response.end

sub DrawPage
%>

<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 ALIGN=RIGHT>
<TR>
	<TD><IMG SRC="<%= imagebase %>spacer.gif" WIDTH=15 HEIGHT=1></TD>
	<TD><TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 WIDTH=120 BGCOLOR="#CC9933">
	<TR><TD><TABLE BORDER=0 CELLPADDING=10 CELLSPACING=0 WIDTH=118 BGCOLOR="#F4E9D2">
	<TR>
		<TD NOWRAP><%= font(1) %>
			<A HREF="custservice.asp#online">Online Customer Service</A><BR>
			<IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=4><BR>
			<A HREF="custservice.asp#policy">Customer Service Policy</A><BR>
			<IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=4><BR>
			<A HREF="custservice.asp#how">How to Order</A><BR>
			<IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=4><BR>
			<A HREF="custservice.asp#return">Return Policy</A><BR>
			<IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=4><BR>
			<A HREF="custservice.asp#shipping">Shipping &amp; Handling</A><BR>
			<IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=4><BR>
			<A HREF="custservice.asp#gift">Gift Messages</A><BR>
			<IMG SRC="images/spacer.gif" WIDTH=1 HEIGHT=4><BR>
			<A HREF="custservice.asp#methods">Shipping Methods</A><BR>
		</TD>
	</TR>
	</TABLE></TD></TR>
	</TABLE><FONT SIZE=1>&nbsp;</TD>
</TR>
</TABLE>
<A NAME="online"><H1>Online Customer Service</H1></A>
<%= font(2) %>
<%
	if gintUserID > 0 then
		DrawMenu
	else
		'------------------JRMCodeChange---------
		'this is a new page without the login
		'DrawLogin
	end if
%>
<BR>
<A NAME="policy"><%= font(2) %><B>CUSTOMER SERVICE POLICY</B></FONT></A><BR>
Welcome to The Campaign Store's customer service information page. 
It is our mission to provide the highest quality service possible to our customers. 
 If you have any questions, please contact our Customer Service Center at 
<A HREF="mailto:custserve@thecampaignstore.com">custserve@thecampaignstore.com</A>
or call 916-441-3734.<BR>


<BR>
<A NAME="guarantee"><%= font(2) %><B>OUR COMMITMENT and GUARANTEE</B></FONT><BR>
Your shopping at the The Campaign Store is 100% Safe and we 
guarantee your complete Satisfaction 100%! If for any reason, 
you are not completely satisfied with your purchase, we will 
gladly replace it or give you a full refund.<BR>

<BR>

<A NAME="how"><%= font(2) %><B>HOW TO ORDER</B></FONT></A><BR>
TheCampaignStore uses the highest standard of security available. 
We guarantee absolutely, the security of your information. (<A HREF="<%= aspbase %>store/default.asp">click here to order</A>)<br>
To purchase any product(s) click the "Add Item" button next to 
the desired item and it will be added to your shopping cart. 
The items you have chosen to order will display at the top of 
the products page. If you wish to delete an item please click 
the trash can icon next to the product.<br>

<BR>
When you have finished shopping, click the checkout link and 
proceed to TheCampaignStore's checkout page. Fill out the checkout form. 
The credit card, and shipping information you enter for your purchases are 
securely submitted to our server. We will send you an electronic receipt 
confirming your order. This receipt will include a tracking number for your 
future reference. In the event that you have to contact us by telephone or email, 
please indicate your tracking number for faster response. <BR>
<BR>
<CENTER><B>WE WILL NEVER SELL YOUR NAME, ADDRESS <br>
OR INFORMATION TO ANYONE.</B></CENTER><BR>
<BR>
<A NAME="return"><%= font(2) %><B>RETURN POLICY</B></FONT></A><BR>
In the unlikely event that you are not satisfied or a product arrives
damaged, you may return the merchandise in its original condition 
directly to our store within 5 days of receipt of the merchandise for a 
full refund. We can only refund shipping costs if the return is as a result of our error. 
To return any product, please call our store 916-441-3734 and request a 
"Return Merchandise Authorization Number". Please save all boxes and packing materials and 
maintain any product in your refrigerator or freezer as appropriate, pending resolution. 
Please do not return the merchandise without authorization by our Customer Service Department.<BR>
<br>
<A NAME="shipping"><%= font(2) %><B>SHIPPING AND HANDLING</B></FONT></A><BR>
The Campaign Store ships throughout the United States, including Alaska, Hawaii, 
Puerto Rico and U.S. Virgin Islands. We also deliver in the European Union and the UK, 
however we can only deliver in Europe those products found in our section "GIFTS FOR DELIVERY IN EUROPE".
<br> <br>
In some instances orders will be shipped directly by our suppliers. 
We monitor these orders to ensure that they have been sent to you in a 
timely manner. Should you ever have any questions please contact our 
Customer Service Department call 916-441-3734 or email to 
<A HREF="mailto:custserve@thecampaignstore.com">custserve@thecampaignstore.com</a>.
<BR><br>
Our standard shipping in the United States is UPS Ground. 
In Europe it is EXPRESS. In the U.S. we will on occasion ship via 
USPS Priority Mail. Some items require Overnight Delivery or Second 
Day Air, Delivery. Orders outside of Continental United States are shipped 
by UPS Second Day. Overnight and Second Day delivery are at your option with a 
surcharge. Shipping to CANADA may require additional costs over our published rates. 
We will contact you for authorization before shipping. 
See the chart below for rates and charges in U.S. and Possessions as well as Europe.
<BR><br>
Shipping charges are calculated and are based upon the total dollar amount of your 
purchase. When you check out, you will be given the various shipping options available 
and will be shown the shipping charge based upon your requested option. The applicable 
shipping charges will be added when you submit the order for final credit card approval.
<BR><br>
<A NAME="gift"><%= font(2) %><B>GIFT MESSAGES</B></FONT></A><BR>
If you like, we can include a brief message with your gift. There is a position for your message at the checkout.<BR>
<BR>
<A NAME="methods"><%= font(2) %><B>SHIPPING METHODS</B></FONT></A><BR>
<B>Overnight Delivery</B>, add $19.97 to the delivery charge per shipping address<BR>
<B>Second Day Air Delivery</B>, add $15.97 to the delivery charge per shipping address.<BR>
<b>To HI, AK, must be Second Day Air only</b>, add $15.97. 
<b>To PR and USVI</b>, add $21.97 to the delivery charge per shipping address. (All shipments are sent Second Day Air or USPS Priority Mail) Items that require Overnight Delivery cannot be ordered.<BR>
<b>Saturday Delivery</b>, add $27.97 to the delivery charge. This service is not universally 
available in the U.S. <br>
All Shipments from our EUROPEAN WAREHOUSE ARE SHIPPED EXPRESS and will be delivered anywhere 
on the continent within THREE BUSINESS DAYS. 
<BR><br>
All Shipping charges apply per shipping address 

<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%">
<TR>
	<TD NOWRAP WIDTH="50%"><%= font(1) %><B>If your order totals:</B></TD>
	<TD NOWRAP WIDTH="50%"><%= font(1) %><B>Your Delivery Cost is:</B></TD>
</TR>
<TR>
	<TD NOWRAP><%= font(1) %>$0 - $25</TD>
	<TD NOWRAP><%= font(1) %>$7.97</TD>
</TR>
<TR>
	<TD NOWRAP><%= font(1) %>$25.01 -  $ 50.00</TD>
	<TD NOWRAP><%= font(1) %>$9.97</TD>
</TR>
<TR>
	<TD NOWRAP><%= font(1) %>$50.01 -  $ 75.00</TD>
	<TD NOWRAP><%= font(1) %>$14.97</TD>
</TR>
<TR>
	<TD NOWRAP><%= font(1) %>$75.01 -  $100.00</TD>
	<TD NOWRAP><%= font(1) %>$19.97</TD>
</TR>
<TR>
	<TD NOWRAP><%= font(1) %>$100.01 - $150.00</TD>
	<TD NOWRAP><%= font(1) %>$24.97</TD>
</TR>
<TR>
	<TD NOWRAP><%= font(1) %>$150.01 - $200.00</TD>
	<TD NOWRAP><%= font(1) %>$29.97</TD>
</TR>
<TR>
	<TD NOWRAP><%= font(1) %>$200.00 or more</TD>
	<TD NOWRAP><%= font(1) %>17% of total</TD>
</TR>
</TABLE>
<BR>
<%= font(2) %><B>SHIPPING OPTIONS</B></FONT><BR>
<b>Standard Delivery</b> is UPS Ground Track, estimated delivery time 5-10 business days 
and are shipped Monday to Fridays. Orders received after 2PM Pacific Time, will be 
shipped the following day.<BR>
<b>Overnight Delivery</b>, add $19.97 to the Standard Delivery Charge per shipping address. Not Delivered on Saturday. <b>Estimated Delivery Time 2 - 3 business days</b>. Shipped Monday to Thursday. Orders received, after 1PM Pacific Time Thursday, will be shipped Monday.<BR>
<b>Second Day Air Delivery</b>, add $15.97 to the Standard Delivery Charge per shipping address. 
<b>Estimated Delivery Time  3 business days</b>.  Shipped Monday to Wednesday.
<u>Not Delivered on Saturdays.</u> Orders received after 2PM Pacific Time Wednesday, will be shipped Monday.
<BR>
<BR>
To <b>HI &amp; AK must be Second Day air please. OVERNIGHT DELIVERY IS NOT IS AVAILABLE.</b><br> 
To <b>PR and USVI</b>, add $21.97 to the standard delivery charge per shipping address. All shipments are sent Second Day Air or USPS Priority Mail.<br>
<b>OVERNIGHT DELIVERY IS NOT AVAILABLE</b>.
<BR>
<BR>
<b>Saturday Delivery</b>, add $27.97 per recipient to the standard shipping rate.
Orders must be received by Friday at 11 AM Pacific Time. Orders will ship on Friday only,
 for Saturday delivery. Must ship to a residence or to a business open on Saturdays.
 Please note that Saturday Delivery is not available throughout the U.S.  <BR>
<BR>
<CENTER><B>NO SUNDAY DELIVERY SERVICE IS AVAILABLE</B></CENTER>

We cannot ship to P.O. Boxes. APO, or FPO addresses.<BR>
<BR>
<%= font(2) %><B>SHIPPING FROM OUR EUROPEAN WAREHOUSE</B></FONT><BR>
All orders are shipped daily EXPRESS DELIVERY Monday to Thursday. 
We will deliver your order throughout the Continent within 3 Business days. 
No Saturday Delivery is available. 
<br><br>

<%= font(2) %><B>MULTIPLE SHIPPING ADDRESSES</B></FONT><BR>
In general, Multiple Shipping Addresses are treated as separate orders on our system. 
Therefore a new order has to be entered for each address. Check out with your first order 
and return to the store to initiate your other orders. If you have more than Five (5) separate orders, 
please call and place your order with our Customer Service Department 916-441-3734. 
If you prefer, send us by email your list, including names, addresses, telephone numbers and 
gift message to <A HREF="mailto:custserve@thecampaignstore.com">custserve@thecampaignstore.com</a>
 and we will call you to confirm and finish the 
order process. We will send you an electronic receipt and tracking number for your records.   <BR>
<BR>

<%
end sub

sub DrawLogin

if gtfError then
	response.write fontx(1,1,cRed) & "<B>The username or password you entered is incorrect. Please try again.</B></FONT><BR>"
else
	response.write "Existing customers, enter your email address and personal password below:<BR>"
end if

if gstrHint <> "" then
	response.write "The hint for your password is: <B>" & gstrHint & "</B><BR>"
end if
%>
<FORM ACTION="custserv.asp" METHOD="POST">
<% HiddenInput "action", "loginsubmit" %>
<% HiddenInput "return", strReturnURL %>

<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=3>
<TR>
	<TD BGCOLOR="<%= cGoCart_CSLoginBG %>"><%= font(1) %>&nbsp;<B>Email&nbsp;Address:&nbsp;&nbsp;</B></TD>
	<TD><% TextInput "email", 20, 50, Request %></TD>
</TR>
<TR>
	<TD BGCOLOR="<%= cGoCart_CSLoginBG %>"><%= font(1) %>&nbsp;<B>Password:&nbsp;&nbsp;</B></TD>
	<TD><% PasswordInput "password", 20, 20, "" %></TD>
</TR>
<TR>
	<TD COLSPAN=2 ALIGN=CENTER><INPUT TYPE="image" SRC="<%= imagebase %>btn_continue.gif" WIDTH="79" HEIGHT="15" BORDER=0><BR>
	<%= font(1) %><A HREF="custserv.asp?action=hint&return=<%= strReturnURL %>">Forgot your password?</A></FONT>
</TD>
</TR>
</TABLE>
</CENTER>
</FORM>
<BR>
<B>New Customers</B><BR>
If you'd like to checkout without creating an account, 	<a href="checkout.asp?NoUser=1">click here</a>.<BR>
<A HREF="checkout.asp?NoUser=1"><IMG SRC="<%= imagebase %>btn_expresscheckout.gif" WIDTH="132" HEIGHT="17" BORDER=0 VSPACE=5 ALT="Express Checkout"></a><BR><BR>

If you'd like to creat an account for future use on our site, <A HREF="custserv.asp?action=newcustomer&return=<%= server.urlencode(request("return")) %>">create an account here</A>.<BR>
<A HREF="custserv.asp?action=newcustomer&return=<%= server.urlencode(request("return")) %>"><IMG SRC="<%= imagebase %>btn_createaccount.gif" WIDTH="132" HEIGHT="17" BORDER=0 VSPACE=5 ALT="Create an Account"></A><BR>
<BR>
<%
end sub

sub DrawNewCustomerPage
%>
To create your customer account, please provide the following information. Your password should be at least 4 characters long.

<FORM ACTION="custserv.asp" METHOD="POST">
<%
	HiddenInput "action", "newcustomersubmit"
	HiddenINput "return", strReturnURL

	stlBeginStdTable "100%"
	response.write "<TR><TD>"
	
	stlBeginFormSection "100%", 2
	stlTextInput "strEmail", 32, 50, Request, "Email Address", "IsEmail", "Please provide your email address."
	stlPasswordInput "strPassword", 20, 20, Request, "Password", "islength4%20", "Please provide a Password that is at least 4 characters long."
	stlPasswordInput "strConfirmPassword", 20, 20, Request, "Confirm", "", ""
	stlTextInput "strHint", 32, 32, Request, "Hint", "IsEmpty", "Please provide a hint."
	stlTextInput "strFirstName", 32, 32, Request, "First Name", "IsEmpty", "Please provde your first name."
	stlTextInput "strLastName", 32, 32, Request, "Last Name", "IsEmpty", "Please provide your last name."
	stlEndFormSection

	response.write "</TD></TR>"
	stlSubmit "Create Account"

	stlEndStdTable
	
%>
</FORM>
<%= font(1) %>
<BR>
<B>About Passwords and Hints</B><BR>
Your email address and password will provide you access to the Customer Service Center section of our website. Here you can view your order history and order status. You may also choose to save payment information with your account profile for use on future orders.<BR>
<BR>
To help protect your privacy and the security of your payment information, your password should be kept secure. For your password, avoid the obvious: your maiden name, your social security number, and your birthdate are generally bad ideas.<BR>
<BR>
You can provide a hint for your password, to help you remember it if you lose it.  Your hint should not be too obvious that anyone would be able to figure it out.<BR>
<BR>
If you have any questions, please contact our customer service staff at <%= STR_MERCHANT_CS_PHONE %> or email <A HREF="mailto:<%= STR_MERCHANT_CS_EMAIL %>"><%= STR_MERCHANT_CS_EMAIL %></A>.
</FONT><BR><BR>
<%
end sub

sub DrawLogoutPage
%>
If you wish, you may save the order you are working on and finish it later. Orders will be saved for up to two weeks.<BR>
<FORM ACTION="custserv.asp" METHOD="POST">
<% HiddenInput "action", "logoutsubmit" %>
<% RadioButton "saveorder", "Y", "Y" %> <B>Save my order, I'll finish it later.</B><BR>
<% RadioButton "saveorder", "N", "Y" %> Cancel my order<BR>
<BR>
<INPUT TYPE=IMAGE SRC="<%= imagebase %>btn_continue.gif" WIDTH="79" HEIGHT="15" ALT="Continue" BORDER="0">
</FORM>
<BR>
<A HREF="<%= strReturnURL %>"><IMG SRC="<%= imagebase %>btn_back.gif" WIDTH="66" HEIGHT="15" ALT="Go Back" BORDER="0"></A>
<%
end sub

' =============================
sub DrawMenu
dim aryTemp
%>
You are currently logged in as <B><%= gintUserName %></B>.
<BR><BR>
&nbsp;<B><A HREF="custserv.asp?action=logout"><IMG SRC="<%= imagebase %>btn_arrow.gif" WIDTH="14" HEIGHT="13" BORDER="0" ALIGN=ABSMIDDLE HSPACE=5 VSPACE=5>Logout</A></B><BR>
&nbsp;<B><A HREF="custserv.asp?action=accountinfo"><IMG SRC="<%= imagebase %>btn_arrow.gif" WIDTH="14" HEIGHT="13" BORDER="0" ALIGN=ABSMIDDLE HSPACE=5 VSPACE=5>View or Change Account Information</A></B><BR>
<!--&nbsp;<B><xA HREF="#"><IMG SRC="<%= imagebase %>btn_arrow.gif" WIDTH="14" HEIGHT="13" BORDER="0" ALIGN=ABSMIDDLE HSPACE=5 VSPACE=5>View or Change Billing and Shipping Addresses</A></B> <FONT SIZE="1">(not available)</FONT><BR>-->
<BR>
<%= font(2) %><B>SAVED ORDERS</B></FONT><BR>
<%
dim dctSavedOrders, i, k
set dctSavedOrders = GetSavedOrders()
if dctSavedOrders.count > 0 then
	%>
	<%= font(1) %>To continue shopping with a saved order, click the Select button for that order.</FONT><BR>
	&nbsp;
	<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%">
	<TR BGCOLOR="<%= cGoCart_CartListRowBG %>">
		<TD><%= font(1) %>&nbsp;</TD>
		<TD><%= font(1) %>Order #</TD>
		<TD><%= font(1) %>Order Created</TD>
		<TD><%= font(1) %>Order Last Updated</TD>
	</TR>
	<%
		dim strColor
		for each k in dctSavedOrders
			aryTemp = split(dctSavedOrders(k),"|")
			if k = gintOrderID then
				strColor = cWhite
				%>
				<TR BGCOLOR="<%= cGoCart_CSSelectBG %>"><TD><%= fontx(1,1,cWhite) %><B>Currently Selected</B></TD>
				<%
			else
				strColor = cBlack
				%>
				<TR><TD><%= font(1) %><A HREF="custserv.asp?action=selectorder&id=<%= k %>&return=<%= Server.URLEncode("cartlist.asp") %>"><IMG SRC="<%= imagebase %>btn_select.gif" WIDTH="52" HEIGHT="17" ALT="Select" BORDER="0"></A></TD>
				<%
			end if
			%>
				<TD><%= fontx(1,1,strColor) %>#<%= aryTemp(0) %></TD>
				<TD><%= fontx(1,1,strColor) %><%= aryTemp(1) %></TD>
				<TD><%= fontx(1,1,strColor) %><%= aryTemp(2) %></TD>
			</TR>
			<TR>
				<TD COLSPAN=4><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cGoCart_CSHdrBG %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
			</TR>
			<%
		next
	%>
	</TABLE>
	<%
else
	%>
	You do not have any past orders saved.<BR>
	<%
end if
%>
<BR>
<%= font(2) %><B>ORDER HISTORY</B></FONT><BR>
<%
set dctSavedOrders = GetOrderHistory()
if dctSavedOrders.count > 0 then
	%>
	<%= font(1) %>To view a completed order, click the View button</FONT><BR>
	&nbsp;
	<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%">
	<TR BGCOLOR="<%= cGoCart_CSHdrBG %>">
		<TD><%= font(1) %>&nbsp;</TD>
		<TD><%= font(1) %>Order #</TD>
		<TD><%= font(1) %>Order Created</TD>
		<TD><%= font(1) %>Order Last Updated</TD>
		<TD><%= font(1) %>Status</TD>
	</TR>
	<%
	for each k in dctSavedOrders
		aryTemp = split(dctSavedOrders(k),"|")
		aryTemp(2) = replace(aryTemp(2), ":00 ", " ")
		aryTemp(3) = replace(aryTemp(3), ":00 ", " ")
		
		%>
		<TR>
			<TD><A HREF="invoice.asp?order=<%= k %>"><IMG SRC="<%= imagebase %>btn_vieworder.gif" WIDTH="81" HEIGHT="17" ALT="View Order" BORDER="0"></A></TD>
			<TD><%= font(1) %>#<%= aryTemp(0) %></TD>
			<TD><%= font(1) %><%= aryTemp(2) %></TD>
			<TD><%= font(1) %><%= aryTemp(3) %></TD>
			<TD><%= font(1) %><%= GetArrayValue(aryTemp(1), dctOrderStatusInvoiceValues) %></TD>
		</TR>
		<TR>
			<TD COLSPAN=5><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cBeige %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
		</TR>
		<%
		'response.write "<A HREF=""invoice.asp?order=" & k & """>" & dctSavedOrders(k) & "</A><BR>"
	next
	%>
	</TABLE>
	<%
else
	%>
	You do not have any submitted orders.<BR>
	<%
end if

end sub

sub DrawAccountPage
	dim strPassword, strHint, strFirstName, strLastName, strCompany, strEmail, strAddress, strCity, strState, strZip
	if FormErrors.count = 0 then
		' load saved information
		GetUserInfo strHint, strFirstName, strLastName, strCompany, strEmail, strAddress, strCity, strState, strZip
		strPassword = ""
	else
		' form submitted with errors
		' use information from last form submission
		strHint = Request("strHint")
		strPassword = Request("strPassword")
		strFirstName = Request("strFirstName")
		strLastName = Request("strLastName")
		strCompany = Request("strCompany")
		strEmail = Request("strEmail")
		stlAddress = Request("strAddress")
		strCity = Request("strCity")
		strState = Request("strState")
		strZip = Request("strZip")
	end if
%>
<%= fontx(2,1,cBlack) %>You are currently logged in as <B><%= gintUserName %></B>. You may leave the password field blank if you do not wish to change it.

<FORM ACTION="<%= absbase %>store/custserv.asp" METHOD="POST">
<%
	HiddenInput "action", "accountinfosubmit"
	HiddenInput "return", strReturnURL

	stlBeginStdTable "100%"
	response.write "<TR><TD>"
	
	stlBeginFormSection "100%", 2
	stlTextInput "strEmail", 32, 50, strEmail, "Email Address", "IsEmail", "Please provide your email address."
	stlTextInput "strPassword", 20, 20, strPassword, "Password", "optislength4%20", "Please provide a Password that is at least 4 characters long."
	stlTextInput "strHint", 32, 32, strHint, "Hint", "IsEmpty", "Please provide a hint."
	stlTextInput "strFirstName", 32, 32, strFirstName, "First Name", "IsEmpty", "Please provde your first name."
	stlTextInput "strLastName", 32, 32, strLastName, "Last Name", "IsEmpty", "Please provide your last name."
%>
	<TR>
		<TD></TD>
		<TD><INPUT TYPE=IMAGE SRC="<%= imagebase %>btn_continue.gif" WIDTH="79" HEIGHT="15" ALT="Continue" BORDER="0"></TD>
	</TR>
<%
	stlEndFormSection
	response.write "</TD></TR>"
	stlEndStdTable
%>
</FORM>
<BR>
<A HREF="<%= strReturnURL %>"><IMG SRC="<%= imagebase %>btn_back.gif" WIDTH="66" HEIGHT="15" ALT="Go Back" BORDER="0"></A>
<%
end sub

sub DrawConfirmPage(strMessage)
%>
<B>Thank you, <%= split(gintUserName," ")(0) %>.</B><BR>
<BR>
<%= strMessage %><BR>
<BR>
<A HREF="<%= strReturnURL %>"><IMG SRC="<%= imagebase %>btn_continue.gif" WIDTH="79" HEIGHT="15" ALT="Continue" BORDER="0"></A>
<%
end sub

sub DrawHintPage()
%>
When you created your account, you provided a hint to help you remember your password. Please enter your email address to find out your hint. If you still can not remember your password after viewing your hint, please contact customer service at <%= STR_MERCHANT_CS_PHONE %> or email <A HREF="mailto:<%= STR_MERCHANT_CS_EMAIL %>"><%= STR_MERCHANT_CS_EMAIL %></A>.<BR>
<BR>
<FORM ACTION="custserv.asp" METHOD="POST">
<%
	HiddenInput "action", "login"
	HiddenInput "hint", "y"
%>
Email Address: <% TextInput "email", 32, 50, Request %> <INPUT TYPE="submit" VALUE="Display My Hint">
</FORM>
<%
end sub
%>