<!--#include file="../admin/config/incInit.asp"-->
<%
OpenConn

dim intcampaignid
intcampaignid = cint(request.form("campaignid"))

'DrawPrint

sub DrawPrint
%>
<!DOCTYPE html>
<html>
	<head>
		<title>In-Store Signage Quote Request</title>
		<link href="<%= virtualbase %>assets/global/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
	</head>
	<body>
		<header>
			<div class="col-md-2">
				<img src="<%= virtualbase %>assets/admin/layout/img/logo.png" alt="HBO logo">
			</div>
			<h3 class="col-md-10">In-Store Signage Quote Request</h3>
		</header>
<%
	dim strSQL, rsItems, rsCampaignInfo, strFileName, rsUserInfo, rsItemCount, strCampaignName
	
	'intcampaignid = cint(request.form("campaignid"))
	'intcampaignid = 143
	set rsCampaignInfo = gobjconn.execute("SELECT [vchItemName] FROM " & STR_TABLE_INVENTORY & " WHERE [intID]="  & intcampaignid & " AND [chrType]='C' AND [chrStatus]='A'")
	set rsUserInfo = gobjconn.execute("SELECT [vchFirstName], [vchLastName] FROM " & STR_TABLE_SHOPPER & " WHERE [intID]=" & gintuserID)
	%>
		<section class="col-md-12">
	<%
	if not rsUserInfo.eof then
		while not rsUserInfo.eof
	%>
			<p><span class="bold">Date: </span><%= Date %></p>
			<p><span class="bold">Vendor name: </span>Dome Printing</p>
			<p><span class="bold">Request By: </span><%= rsUserInfo("vchFirstName") & " " & rsUserInfo("vchLastName") %></p>
	<%
			if not rsCampaignInfo.eof then
				while not rsCampaignInfo.eof
					strCampaignName = rsCampaignInfo("vchitemname")
					rsCampaignInfo.movenext()
				wend
			end if
			rsUserInfo.movenext()
		wend
	end if
	'strCampaignName = Replace(strCampaignName, " ", "_", 1, -1, 1)
	%>
			<p><span class="bold">Job name: </span><%= strCampaignName %></p>
	<%
	strSQL =  "SELECT COUNT(PSP.[intitemid]) AS [TotalItems] FROM " & STR_TABLE_PROFILE_STORE_PACKAGE & " PSP LEFT JOIN " & STR_TABLE_INVENTORY
	strSQL = strSQL & " I ON PSP.[intitemid] = I.[intid] WHERE [intitemid] IN (SELECT DISTINCT [intitemid] FROM "
	strSQL = strSQL & STR_TABLE_PROFILE_STORE_PACKAGE & " WHERE [intcampaignid] = 143) AND I.[chrStatus] <> 'D' AND I.[chrStatus] <> 'I'"
	set rsItemCount = gobjconn.execute(strSQL)
	if not rsItemCount.eof then
		while not rsItemCount.eof
	%>
			<p><span class="bold">Quantity of Kit #1: </span><%= rsItemCount("TotalItems") %></p>
	<%
			rsItemCount.movenext()
		wend
	end if
	'response.ContentType = "text/comma-separated-values"
	'response.AddHeader "Content-transfer-encoding", "binary"
	'strFileName = Day(Date) & "_" & Month(Date) & "_" & Year(Date) & "_" & Replace(rsCampaignInfo("vchitemname"), " ", "_") & ".csv"
	'response.AddHeader "Content-Disposition", "attachment; filename=" & strFileName
	

	strSQL = "SELECT Count(PSP.[intitemid]) AS [numitems] ,PSP.[intitemid], I.[vchitemname], I.[vchsize], I.[vchnumsides], I.[vchsubstrate], I.[vchcolorprocess], I.[intparentid], F.[vchitemname] AS [FixtureName] "
	strSQL = strSQL & " FROM " & STR_TABLE_PROFILE_STORE_PACKAGE & " PSP LEFT JOIN " & STR_TABLE_INVENTORY & " I ON PSP.[intitemid] = I.[intid] "
	strSQL = strSQL & " LEFT JOIN [HBO_inv] F ON I.[intparentid] = F.[intid]"
	strSQL = strSQL & " WHERE [intitemid] IN (SELECT DISTINCT [intitemid] FROM " & STR_TABLE_PROFILE_STORE_PACKAGE & " WHERE  [intcampaignid] =" & intcampaignid & ") "
	strSQL = strSQL & " AND I.[chrStatus]<>'D' AND I.[chrStatus]<>'I' "
	strSQL = strSQL & " GROUP  BY PSP.[intitemid], I.[vchitemname], F.[vchitemname], I.[vchsize], I.[vchnumsides], I.[vchsubstrate], I.[vchcolorprocess], I.[intparentid] "
	strSQL = strSQL & " ORDER  BY F.[vchitemname], I.[vchitemname] "
	
	set rsItems = gobjconn.execute(strSQL)
	'response.write strSQL
	%>
			<table class="table">
				<thead>
					<tr>
						<th>Graphics - Kit #1</th>
						<th>Versions</th>
						<th>Sets</th>
						<th>#/kit</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
				<%
				if not rsItems.eof then
					while not rsItems.eof
				%>
					<tr>
						<td><%= rsItems("vchitemname") %></td>
						<td></td>
						<td></td>
						<td></td>
						<td><%= rsItems("numitems") %></td>
					</tr>
				<%
						rsItems.movenext()
					wend
				end if
				%>
				</tbody>
			</table>
		</section>
		<section class="col-md-12">
			
		</section>
	</body>
</html>
<%
end sub

function GetHTMLForPDF
	dim outputstring
	outputstring = "<!DOCTYPE html>"
	outputstring = outputstring & "<html>"
	outputstring = outputstring & "<head>"
	outputstring = outputstring & "<title>In-Store Signage Quote Request</title>"
	outputstring = outputstring & "<link href='http://HBO.drcportal.com/assets/global/plugins/bootstrap/css/bootstrap.css' rel='stylesheet' type='text/css' />"
	outputstring = outputstring & "<style type='text/css'>"
	outputstring = outputstring & ".bold { font-weight: bold; }"
	outputstring = outputstring & ".table thead tr { background-color: #94c073; }"
	outputstring = outputstring & "</style>"
	outputstring = outputstring & "</head>"
	outputstring = outputstring & "<body>"
	outputstring = outputstring & "<header>"
	outputstring = outputstring & "<div class='col-md-2 pull-left'>"
	outputstring = outputstring & "<img src='http://HBO.drcportal.com/assets/admin/layout/img/logo.png'>"
	outputstring = outputstring & "</div>"
	outputstring = outputstring & "<h3 class='col-md-10'>In-Store Signage Quote Request</h3>"
	outputstring = outputstring & "</header>"
	outputstring = outputstring & "<div class='clearfix'></div>"
	
	dim strSQL, rsItems, rsCampaignInfo, strFileName, rsUserInfo, rsItemCount, rsHardware
	
	set rsCampaignInfo = gobjconn.execute("SELECT [vchItemName] FROM " & STR_TABLE_INVENTORY & " WHERE [intID]="  & intcampaignid & " AND [chrType]='C' AND [chrStatus]='A'")
	set rsUserInfo = gobjconn.execute("SELECT [vchFirstName], [vchLastName] FROM " & STR_TABLE_SHOPPER & " WHERE [intID]=" & gintuserID)
	outputstring = outputstring & "<section>"
	if not rsUserInfo.eof then
		while not rsUserInfo.eof
		
			outputstring = outputstring & "<p><span class='bold'>Date: </span>" & Date & "</p>"
			outputstring = outputstring & "<p><span class='bold'>Vendor name: </span>Dome Printing</p>"
			outputstring = outputstring & "<p><span class='bold'>Request By: </span>" & rsUserInfo("vchFirstName") & " " & rsUserInfo("vchLastName") & "</p>"
			
			if not rsCampaignInfo.eof then
				while not rsCampaignInfo.eof
					outputstring = outputstring & "<p><span class='bold'>Job name: </span>" & rsCampaignInfo("vchitemname") & "</p>"
					rsCampaignInfo.movenext()
				wend
			end if
			rsUserInfo.movenext()
		wend
	end if
	strSQL =  "SELECT COUNT(PSP.[intitemid]) AS [TotalItems] FROM " & STR_TABLE_PROFILE_STORE_PACKAGE & " PSP LEFT JOIN " & STR_TABLE_INVENTORY
	strSQL = strSQL & " I ON PSP.[intitemid] = I.[intid] WHERE [intitemid] IN (SELECT DISTINCT [intitemid] FROM "
	strSQL = strSQL & STR_TABLE_PROFILE_STORE_PACKAGE & " WHERE [intcampaignid] = " & intcampaignid & ") AND I.[chrStatus] <> 'D' AND I.[chrStatus] <> 'I'"
	set rsItemCount = gobjconn.execute(strSQL)
	if not rsItemCount.eof then
		while not rsItemCount.eof
			outputstring = outputstring & "<p><span class='bold'>Quantity of Kit #1: </span>" &  rsItemCount("TotalItems") & "</p>"
			rsItemCount.movenext()
		wend
	end if
	
	strSQL = "SELECT Count(PSP.[intitemid]) AS [numitems] ,PSP.[intitemid], I.[vchitemname], I.[vchsize], I.[vchnumsides], I.[vchColor], I.[vchsubstrate], I.[vchcolorprocess], I.[intparentid], F.[vchitemname] AS [FixtureName] "
	strSQL = strSQL & " FROM " & STR_TABLE_PROFILE_STORE_PACKAGE & " PSP LEFT JOIN " & STR_TABLE_INVENTORY & " I ON PSP.[intitemid] = I.[intid] "
	strSQL = strSQL & " LEFT JOIN [HBO_inv] F ON I.[intparentid] = F.[intid]"
	strSQL = strSQL & " WHERE [intitemid] IN (SELECT DISTINCT [intitemid] FROM " & STR_TABLE_PROFILE_STORE_PACKAGE & " WHERE  [intcampaignid] =" & intcampaignid & ") "
	strSQL = strSQL & " AND I.[chrStatus]<>'D' AND I.[chrStatus]<>'I' AND I.[intParentID] <> 314 "
	strSQL = strSQL & " GROUP  BY PSP.[intitemid], I.[vchitemname], F.[vchitemname], I.[vchsize], I.[vchnumsides], I.[vchsubstrate], I.[vchcolorprocess], I.[intparentid], I.[vchColor] "
	strSQL = strSQL & " ORDER  BY F.[vchitemname], I.[vchitemname] "
	
	set rsItems = gobjconn.execute(strSQL)
	outputstring = outputstring & "<table class='table'>"
	outputstring = outputstring & "<thead>"
	outputstring = outputstring & "<tr>"
	outputstring = outputstring & "<th>Graphics - Kit #1</th>"
	outputstring = outputstring & "<th>Size</th>"
	outputstring = outputstring & "<th>Substrate</th>"
	outputstring = outputstring & "<th>Process</th>"
	outputstring = outputstring & "<th>Color</th>"
	outputstring = outputstring & "<th>Sides</th>"
	outputstring = outputstring & "<th>Total</th>"
	outputstring = outputstring & "</tr>"
	outputstring = outputstring & "</thead>"
	outputstring = outputstring & "<tbody>"
	if not rsItems.eof then
		while not rsItems.eof
			outputstring = outputstring & "<tr>"
			outputstring = outputstring & "<td>" & rsItems("vchitemname") & "</td>"
			outputstring = outputstring & "<td>" & rsItems("vchsize") & "</td>"
			outputstring = outputstring & "<td>" & rsItems("vchsubstrate") & "</td>"
			outputstring = outputstring & "<td>" & rsItems("vchcolorprocess") & "</td>"
			outputstring = outputstring & "<td>" & rsItems("vchColor") & "</td>"
			outputstring = outputstring & "<td>" & rsItems("vchnumsides") & "</td>"
			outputstring = outputstring & "<td>" & rsItems("numitems") & "</td>"
			outputstring = outputstring & "</tr>"
			rsItems.movenext()
		wend
	end if
	outputstring = outputstring & "</tbody>"
	outputstring = outputstring & "</table>"
	outputstring = outputstring & "</section>"
	strSQL = "SELECT I.vchPartNumber, (PP.[intQty] * (select count(intStoreShopperID) from " & STR_TABLE_PROFILE_MAPPER & " P2 where P2.intProfileID in (select distinct intProfileID from " & STR_TABLE_PROFILE_PACKAGES & " where intCampaignID = " & intcampaignid & "))) AS [numitems], PP.[intitemid], I.[vchitemname], I.[vchsize], I.[vchnumsides], I.[vchsubstrate], I.[vchcolorprocess], I.[intparentid], F.[vchitemname] AS FixtureName"
	strSQL = strSQL & " FROM " & STR_TABLE_PROFILE_PACKAGES & " PP LEFT JOIN " & STR_TABLE_INVENTORY & " I ON PP.[intitemid] = I.[intid] LEFT JOIN [HBO_inv] F ON I.[intparentid] = F.[intid]"
	strSQL = strSQL & " WHERE  "
	strSQL = strSQL & " I.[chrStatus]<>'D' AND I.[chrStatus]<>'I' And I.[intParentID] = 314 "
	strSQL = strSQL & " GROUP  BY PP.[intitemid], PP.intQty, I.vchPartNumber, I.[vchitemname], F.[vchitemname], I.[vchsize], I.[vchnumsides], I.[vchsubstrate], I.[vchcolorprocess], I.[intparentid] "
	strSQL = strSQL & " ORDER  BY F.[vchitemname], I.[vchitemname] "
	'response.write strSQL
	'response.end
	set rsHardware = gobjconn.execute(strSQL)
	outputstring = outputstring & "<section class='col-md-12'>"
	outputstring = outputstring & "<table class='table'>"
	outputstring = outputstring & "<thead>"
	outputstring = outputstring & "<tr>"
	outputstring = outputstring & "<th>SKU</th>"
	outputstring = outputstring & "<th>Item name</th>"
	outputstring = outputstring & "<th>Total # of pieces</th>"
	outputstring = outputstring & "<tr>"
	outputstring = outputstring & "</thead>"
	outputstring = outputstring & "<tbody>"
	if not rsHardware.eof then
		while not rsHardware.eof
			outputstring = outputstring & "<tr>"
			outputstring = outputstring & "<td>" & rsItems("vchPartNumber") & "</td>"
			outputstring = outputstring & "<td>" & rsItems("vchitemname") & "</td>"
			outputstring = outputstring & "<td>" & rsItems("numitems") & "</td>"
			outputstring = outputstring & "</tr>"
			rsHardware.movenext()
		wend
	end if
	outputstring = outputstring & "</tbody>"
	outputstring = outputstring & "</table>"
	outputstring = outputstring & "</section>"
	outputstring = outputstring & "</body>"
	outputstring = outputstring & "</html>"
	
	GetHTMLForPDF = outputstring
end function

Dim Pdf, Doc, Filename, strPdfFile, strCampaignName, rsCampaignInfo

'intcampaignid =  Request("CatalogId")&""

' mred: consolidated PDF file location (todo: move to incInit)
set rsCampaignInfo = gobjconn.execute("SELECT [vchItemName] FROM " & STR_TABLE_INVENTORY & " WHERE [intID]="  & intcampaignid & " AND [chrType]='C' AND [chrStatus]='A'")
if not rsCampaignInfo.eof then
	while not rsCampaignInfo.eof
		strCampaignName = rsCampaignInfo("vchItemName")
		rsCampaignInfo.movenext()
	wend
end if

strPdfFile = Replace(strCampaignName, " ", "_", 1, -1, 1) & ".pdf"

Set Pdf = Server.CreateObject("Persits.Pdf")
Set Doc = Pdf.CreateDocument
Doc.ImportFromUrl GetHTMLForPDF
Doc.SaveHTTP "attachment;filename=" & strPdfFile
CloseConn


Private Sub DownloadFile(file)
    '--declare variables
    Dim strAbsFile
    Dim strFileExtension
    Dim objFSO
    Dim objFile
    Dim objStream
    '-- set absolute file location
    strAbsFile = Server.MapPath(file)
    '-- create FSO object to check if file exists and get properties
    Set objFSO = Server.CreateObject("Scripting.FileSystemObject")
    '-- check to see if the file exists
    If objFSO.FileExists(strAbsFile) Then
        Set objFile = objFSO.GetFile(strAbsFile)
        '-- first clear the response, and then set the appropriate headers
        Response.Clear
        '-- the filename you give it will be the one that is shown
        ' to the users by default when they save
        Response.AddHeader "Content-Disposition", "attachment; filename=" & objFile.Name
        Response.AddHeader "Content-Length", objFile.Size
        Response.ContentType = "application/octet-stream"
        Set objStream = Server.CreateObject("ADODB.Stream")
        objStream.Open
        '-- set as binary
        objStream.Type = 1
        Response.CharSet = "UTF-8"
        '-- load into the stream the file
        objStream.LoadFromFile(strAbsFile)
        '-- send the stream in the response
        Response.BinaryWrite(objStream.Read)
        objStream.Close
        Set objStream = Nothing
        Set objFile = Nothing
    Else 'objFSO.FileExists(strAbsFile)
        Response.Clear
        Response.Write("No such file exists.")
    End If
    Set objFSO = Nothing
End Sub

private function readBytes(file)
    dim inStream
    ' ADODB stream object used
    set inStream = Server.CreateObject("ADODB.Stream")
    ' open with no arguments makes the stream an empty container
    inStream.Open
    inStream.type= 1 
    'Response.Write file
    inStream.LoadFromFile(file)
    readBytes = inStream.Read()
  end function

  private function encodeBase64(bytes)
    dim DM, EL
    Set DM = Server.CreateObject("Microsoft.XMLDOM")
    ' Create temporary node with Base64 data type
    Set EL = DM.createElement("tmp")
    EL.DataType = "bin.base64"
    ' Set bytes, get encoded String
    EL.NodeTypedValue = bytes
    encodeBase64 = Replace(Replace(Replace(Replace(EL.Text,vbNewLine,""),vbCrLf,""), vbCR,""), vbLF,"")
  end function

  private function decodeBase64(base64)
    dim DM, EL
    Set DM = Server.CreateObject("Microsoft.XMLDOM")
    ' Create temporary node with Base64 data type
    Set EL = DM.createElement("tmp")
    EL.DataType = "bin.base64"
    ' Set encoded String, get bytes
    EL.Text = base64
    decodeBase64 = EL.NodeTypedValue
  end function

  private Sub writeBytes(file, bytes)
    Dim binaryStream
    Set binaryStream = Server.CreateObject("ADODB.Stream")
    binaryStream.Type = adTypeBinary
    'Open the stream and write binary data
    binaryStream.Open
    binaryStream.Write bytes
    'Save binary data to disk
    binaryStream.SaveToFile file, adSaveCreateOverWrite
  End Sub
  
  CloseConn()
  %>