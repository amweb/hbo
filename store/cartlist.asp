<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: cartlist.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Displays current shopping cart
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================
dim dctErrors, dctOrderItemError
set dctErrors = CreateObject("Scripting.Dictionary")
set dctOrderItemError = CreateObject("Scripting.Dictionary")

If Session("DummyUser")<>"" then
	Session("DummyUser")=""
	Session(SESSION_PUB_USER_ID) = ""
	Session(SESSION_PUB_USER_NAME) = ""
end if 
OpenConn

If gintUserName&"" = ""  Then
%>
<script type="javascript">
window.frames[0].location = "<%= virtualbase %>"; 
</script>
<%
End if
%>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>HBO Resource Center | Dome Printing</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<style type="text/css">
body
{
    font-family: 'Open Sans', sans-serif;
    font-size:8pt;
}
@media screen
{
.print, .print * {display:none !important;}
}

.header  {text-align:center;font-weight:bold;font-size:larger;}
@media print {
    body 
    {
        background-color: #fff !important;
    }
    .no-print, .no-print * {display:none !important;}
    
    .page-break { page-break-after:always !important;}
    
    .cartitem-image div {width:300px !important; height:240px !important;}
    
    .cartitem-image, .cartitem-data {border-top:1px solid #000;padding-top:10px;padding-bottom:10px;}
    
    .cartitem-data { padding-left:10px;vertical-align:top;}   
}
</style>
<body class="page-header-fixed login">

<%

SelectCurrentOrder

dim strAction
strAction = lcase(Request("action"))
select case strAction
	case "additem": ' add item to cart
		if Request("special") <> "y" then
			if not CanAddItem(Request("id")) then
				response.redirect "specialitem.asp?id=" & Request("id") & "&qty=" & Server.URLEncode(Request("qty")) & "&parentid=" & Request("parentid") & "&option1=" & Request("Option1")& "&option2=" & Request("Option2")& "&option3=" &Request("Option3")
			end if
		end if
'		AddItemToOrder Request("id"), Request("qty")
		AddItemToOrderWithOption Request("id"), Request("qty"), Request("Option1"), Request("Option2"), Request("Option3")
		response.redirect "cartlist.asp?parentid=" & Request("parentid")
	case "multiadd": ' add multiple items to cart
		AddMultipleItemsToOrder	Request
		response.redirect "cartlist.asp?parentid=" & Request("parentid")

    case "multiadd_dist": ' add multiple items to cart
		AddMultipleItemsToOrderDist	Request
		response.redirect "cartlist.asp?parentid=" & Request("parentid")
	case "delitem": ' delete item from cart
		RemoveLineFromOrder Request("id")
		response.redirect "cartlist.asp?parentid=" & Request("parentid")
	case "cancel": ' cancel order
		CancelOrder
		SelectCurrentOrder	' get updated statistical info
	case "update": ' update quantities
		UpdateQuantities
		SelectCurrentOrder	' get updated statistical info
	case "shippreview" 
		SelectCurrentOrder	' get updated statistical info
end select

if gintUserID = 0 and gintOrderID = 0 then
	response.redirect "default.asp"
end if

dim intParentID
intParentID = Request("parentid")
if IsNumeric(intParentID) then
	intParentID = CLng(intParentID)
else
	intParentID = 0
end if

'remove empty orders
Dim strSQL,rsOrders,rsLineItems
 strSQL = "SELECT *  FROM " & STR_TABLE_ORDER & " WHERE intShopperId=" & gintUserID & " AND chrStatus in ('0') AND intBillShopperID is not null ORDER BY (SELECT vchFirstName + ' ' + vchLastName FROM " & STR_TABLE_SHOPPER & " AS S WHERE S.intId=intBillShopperID AND chrStatus='A')"
set rsOrders = gobjConn.execute(strSQL)

While not rsOrders.Eof

    strSQL = "SELECT * FROM " & STR_TABLE_LINEITEM & " WHERE chrStatus='A' AND intOrderID=" & rsOrders("intID")
    set rsLineItems = gobjConn.execute(strSQL)

    if rsLineItems.Eof Then
        strSQL = "UPDATE " & STR_TABLE_ORDER & " SET chrStatus='D' WHERE intID=" & rsOrders("intID")
        gobjConn.execute strSQL
    End If

    rsOrders.MoveNext
Wend


dim strPageTitle, intParentUpID, rsDAta
strPageTitle = "Your Shopping Cart"
Custom_GetGlobalInventoryFolders 0

DrawPage

CloseConn
response.end

sub UpdateQuantities
	dim i, intQty, intItemID, intOrderID
	for each i in Request.Form
		if lcase(left(i, 7)) = "intqty_" then
        'Response.Write mid(i,8) DEBUG
			intOrderID = Split(mid(i,8),"_")(0)
            intItemID = Split(mid(i,8),"_")(1)
			if IsNumeric(intItemID) then
				intItemID = CLng(intItemID)
				intQty = Request.Form(i)
				if IsNumeric(intQty) then
					intQty = CLng(intQty)
				else
					intQty = -1
				end if
				if intQty = 0 then
					' remove item from cart
					RemoveLineFromOrder_Other intOrderID,intItemID
				elseif intQty > 0 then
					' change qty in cart
					EditOrderItemQty_Other intOrderID,intItemID, intQty
				end if
			end if
		end if
	next
end sub

sub DrawPage
    %>
    <style>
        a, a:link, a:visited, a:active, a:hover  {color:#5D319D !important;text-decoration:none;font-weight:bold;}
    </style>
    <%if Request("onlyid")&""="" then %>
	
    <hr style="color:#ccc;" class="no-print" />
    <% end if %>
    <%
	
	CheckPreBuyInventoryAmt(gintUserID)
	
	if dctErrors.count > 0 then %>
		<div id=errors" style="color: red"><strong>
		<%
			dim a, x
			a = dctErrors.Items
			for x = 0 to dctErrors.count - 1
				response.write a(x) & "<br/>"
			next %></strong>
		</div>
	<% end if
	
	dim rsData, mnySubtotal,mnyTotal, intWeight, item, rsTemp, strSQL, rsOrders, intCounter 

    strSQL = "SELECT *  FROM " & STR_TABLE_ORDER & " WHERE intShopperId=" & gintUserID & " AND chrStatus in ('0','9') AND intBillShopperID is not null "
    
    if Request("onlyid")&""<>"" then
    strSQL = strSQL & " AND intid=" & Request("onlyid")&""

    end if
    strSQL = strSQL & "ORDER BY (SELECT vchFirstName + ' ' + vchLastName FROM " & STR_TABLE_SHOPPER & " AS S WHERE S.intId=intBillShopperID AND chrStatus='A')"
	set rsOrders = gobjConn.execute(strSQL)

    intCounter = 1
    mnyTotal = 0
    While not rsOrders.Eof
        strSQL = "SELECT *  FROM " & STR_TABLE_SHOPPER & " WHERE intId=" & rsOrders("intShipShopperID") & " AND chrStatus='A'"

	    set rsTemp = gobjConn.execute(strSQL)
        mnySubtotal = 0
        if not rsTemp.Eof then
            
	        set rsData = GetOrderLineItems_Other(rsOrders("intID"))
	        %>
        <div class="page-break" style="margin-bottom:20px;margin-top:10px;">
	        <table border="0" cellpadding="0" cellspacing="0" width="<%= INT_PAGE_WIDTH %>" bgcolor="<%= cWhite %>">
	        <tr>
                <td colspan="2" class="header" style="font-size:11pt;">Store <%= rsTemp("vchLabel") %> |<%= rsTemp("vchCity") %>, <%= rsTemp("vchState") %></td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            
	        <%
	       
	        while not rsData.eof
		        %>
	        <tr class="cartitem">
		        <td valign="top" class="cartitem-image">
                    <% 
                    dim strPhotoURL

                    strPhotoURL = "../images/products/big/" & rsData("vchImageURL")
                    %>
                    
                      <% if strPhotoURL&""="../images/products/big/" then strPhotoURL = virtualbase & "images/icon-no-image-512.png" %>
                       
                    <div style="background-image:url(<%= strPhotoURL %>);margin-bottom:20px;width:100px;height:80px;background-repeat:no-repeat;background-size:cover;boder:1px solid #ccc;">
                
                    </div>
                </td>
                <td valign="top" class="cartitem-data">
                <form action="cartlist.asp" method="post">
	            <%
		            HiddenInput "action", "update"
		            HiddenInput "parentid", intParentID

                    Dim vchPartNumber, vchProgram, vchItemName, txtDescription, mnyUnitPrice,intQuantity, vchValidStates, vchBundleQuantity, vchExpireDate
                    vchBundleQuantity = rsData("vchBundleQuantity")
                    vchExpireDate = rsData("vchExpireDate")
                    vchValidStates = rsData("vchValidStates")
                    vchPartNumber = rsData("vchPartNumber")
                    vchProgram = rsData("vchProgram")
                    vchItemName = rsData("vchItemName")
                    
	                mnyUnitPrice = CDBL("0"&rsData("mnyUnitPrice"))
                    intQuantity = CDBL("0"&rsData("intQuantity"))
                    
                    txtDescription = rsData("txtDescription")
                %>
	        
                    <table cellpadding="1" cellspacing="1">
                        <tr align="left">
                            <td colspan="2" style="font-weight:bold;font-size:larger;"><%= vchItemName %></td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                        <tr align="left">
                            <td>Part Number:</td>
                            <td><%= vchPartNumber %></td>
                        </tr>
                        
						<tr align="left">
                            <td>Size:</td>
                            <td><%= vchProgram %></td>
                        </tr>
                        
                        <!--
                         <tr align="left">
                            <td>Cost/Bundle:</td>
                            <td><%= FormatCurrency(mnyUnitPrice) %></td>
                        </tr>
						
						
                        <tr align="left">
                            <td>Bundle Quantity:</td>
                            <td><%= vchBundleQuantity %></td>
                        </tr>
						-->
						
                        <tr class="print">
                            <td>Quantity:</td>
                            <td>
                                <%= rsData("intQuantity") %>
                            </td>
                        </tr>
						<!--
                        <tr align="left">
                            <td>Valid States:</td>
                            <td><%= vchValidStates %></td>
                        </tr>
                        
                        <tr align="left">
                            <td>Expire Date:</td>
                            <td><%= vchExpireDate  %></td>
                        </tr>
						-->
                        <%if Request("onlyid")&""="" then %>
                        <tr class="no-print">
                            <td>Quantity:</td>
                            <td>
                                <input type="text" name="intQty_<%= rsOrders("intID") %>_<%= rsData("intID") %>" value="<%= rsData("intQuantity") %>" size="2" /><input type="submit" value="Update Qty" />
                            </td>
                        </tr>
                        
                        <tr class="no-print">
                            <td colspan="2" style="padding:10px 0px 20px 0px;">
                                <a href="cartlist.asp?id=<%= rsData("intID") %>&action=delitem&parentid=<%= intParentID %>" onClick="return confirm('Are you sure you want to remove this item from your order?');" >Remove Item From Cart</a>
                            </td>
                        </tr>
                        <% end if %>
                    </table>
                    </form>
                </td>
	        </tr>
	            <%

                mnySubTotal = mnySubTotal + (mnyUnitPrice * intQuantity)
                
		        rsData.MoveNext
	        wend
            

	        %>
	        
	        
            <%if Request("onlyid")&""="" then %>
            <!--
			<tr class="no-print">
                <td style="font-weight:bold;font-size:larger;padding-bottom:20px;">Sub Total:</td><td style="font-weight:bold;font-size:larger;padding-bottom:20px;"><%= FormatCurrency(mnySubTotal) %></td>
            </tr>
			-->
	        <tr class="no-print">
                <td style="font-weight:bold;font-size:larger;padding-bottom:20px;border-bottom:1px solid #ccc;"><form action="cartlist.asp" target="_blank"><input type="hidden" name="onlyid" value="<%= rsOrders("intID") %>" /><input type="submit" value="Print this Order" /></form> </td><td style="font-weight:bold;font-size:larger;padding-bottom:20px;border-bottom:1px solid #ccc;"> <form target="_blank" action="../asppdf/OrderPdf.asp"><input type="hidden" name="CatalogId" value="<%= rsOrders("intID") %>" /><input type="submit" value="Save this Order as PDF" /></form></td>
            </tr>
            
            <% end if %>
	        </table>
	    </div>
            <%
	       
	        rsData.close
	        set rsData = nothing

        End if
        mnyTotal = mnyTotal + mnySubTotal
        rsOrders.MoveNext
    Wend
    %>

    <%
end sub

sub DrawShipPreview(strAction, intWeight)
	
	if strAction = "shippreview" then
	%>
	<table cellspacing=0 cellpadding=2 border=0 align="right">
	<tr>
		<td align="center" colspan="2"><b>Shipping Prices for current shopping cart:</b></td>
	</tr>
	<tr bgcolor="<%= cRow2 %>">
		<td>Ground:</td>
		<td align="right"><%= SafeFormatCurrency("n/a",PollShippingPrice(1,intWeight),2) %></td>
	</tr>
	<tr bgcolor="<%= cRow1 %>">
		<td>3 Day:</td>
		<td align="right"><%= SafeFormatCurrency("n/a",PollShippingPrice(2,intWeight),2) %></td>
	</tr>
	<tr bgcolor="<%= cRow2 %>">
		<td>Next Day Air:</td>
		<td align="right"><%= SafeFormatCurrency("n/a",PollShippingPrice(3,intWeight),2) %></td>
	</tr>
	</table>
	<%
	else
	%>
	<div align="right"><a id="blueButton" href="cartlist.asp?action=shippreview">&nbsp;View Shipping Costs&nbsp;</a></div>
	<%
	end if
end sub

sub CheckPreBuyInventoryAmt(ShopperID)
	dim strInvSQL, rsInvData, strQtySQL, rsQtySQL, dctTotalOfItems, x, a, i
	set dctTotalOfItems = CreateObject("Scripting.Dictionary")
	
	strInvSQL = "SELECT O.intID as OrderID, L.IntId as Item_ID, L.intInvID as Inv_ID, L.vchItemName as Item_Name, L.intQuantity as Quantity FROM " & STR_TABLE_ORDER & " O"
	strInvSQL = strInvSQL & " LEFT JOIN " & STR_TABLE_LINEITEM & " AS L ON O.intID=L.intOrderID AND (L.chrStatus='A' or L.chrStatus='C') "
	strInvSQL = strInvSQL & " LEFT JOIN " & STR_TABLE_INVENTORY & " AS I ON I.intID=L.intInvID AND I.chrStatus='A' "
	strInvSQL = strInvSQL & " WHERE O.intShopperId=" & ShopperID & " AND O.chrStatus in ('0','9') AND O.intBillShopperID is not null"
	'response.write "<div style=""display:none"">" & strInvSQL & "</div>"
	set rsInvData = gobjConn.execute(strInvSQL)
	i = 0
	while not rsInvData.EOF
		strQtySQL = "Select * From " & STR_TABLE_INVENTORY & " where intID = " & rsInvData("Inv_ID")
		set rsQtySQL = gobjConn.execute(strQtySQL)
		
		if not rsQtySQL.EOF then
			
			if dctTotalOfItems.Exists(rsInvData("Item_Name")&"") = true then 
				dctTotalOfItems.Item(rsInvData("Item_Name")&"") = cint(dctTotalOfItems.Item(rsInvData("Item_Name")&"")) + cint(rsInvData("Quantity"))
			else 
				dctTotalOfItems.Add rsInvData("Item_Name")&"", cint(rsInvData("Quantity"))
			end if
			
			if rsQtySQL("intMinQty") > rsInvData("Quantity") then
				dctErrors.Add rsInvData("Item_Name") & "-min", "You must order at least " & rsQtySQL("intMinQty") &  " of " & rsInvData("Item_Name") & "."
				dctOrderItemError.Add i, rsInvData("OrderID") & " " & rsInvData("Inv_ID")
			elseif rsQtySQL("intMaxQty") < rsInvData("Quantity") then
				dctErrors.Add rsInvData("Item_Name") & "-max", "You cannot order more than " & rsQtySQL("intMaxQty") &  " of " & rsInvData("Item_Name") & "."
				dctOrderItemError.Add i, rsInvData("OrderID") & " " & rsInvData("Inv_ID")
			end if
			
			if cint(dctTotalOfItems.Item(rsInvData("Item_Name")&"")) > rsQtySQL("intStock")  then
				if not dctErrors.Exists(rsInvData("Item_Name")&"") then 
					dctErrors.Add rsInvData("Item_Name")&"", "There is/are only " & rsQtySQL("intStock") &  " left of " & rsInvData("Item_Name") & " in stock." 
				end if 
			end if
			
			rsQtySQL.MoveNext
			'response.write "<div style=""display:none"">" & rsInvData("Item_Name") & " " & dctTotalOfItems.Item(rsInvData("Item_Name")&"") & "</div>"
		end if
		
		rsQtySQL.close
		set rsQtySQL = nothing
		
		i = i + 1
		rsInvData.MoveNext
	wend
	
	
	rsInvData.close
	set rsInvData = nothing
	
	'Debug leftovers
	'response.write "errors: "
	'a = dctErrors.Items
	'for x = 0 to dctErrors.count - 1
	'	response.write a(x) & "<br/>"
	'next
	
	'response.write "errors: "
	'a = dctOrderItemError.Items
	'for x = 0 to dctOrderItemError.count - 1
	'	response.write a(x) & "<br/>"
	'next
	
end sub
%>

</body>
<!-- END BODY -->
</html>
