<!--#include file="../admin/config/incInit.asp"-->
<%
OpenConn

If gintUserName&"" = ""  Then
	Response.Redirect virtualbase
End if

DrawCSV

function DrawCSV
	dim strSQL, rsItems, rsCampaignInfo, intcampaignid, strFileName
	
	intcampaignid = cint(request.form("campaignid"))
	set rsCampaignInfo = gobjconn.execute("SELECT [vchItemName] FROM " & STR_TABLE_INVENTORY & " WHERE [intID]="  & intcampaignid & " AND [chrType]='C' AND [chrStatus]='A'")
	
	response.ContentType = "text/comma-separated-values"
	response.AddHeader "Content-transfer-encoding", "binary"
	strFileName = Day(Date) & "_" & Month(Date) & "_" & Year(Date) & "_" & Replace(rsCampaignInfo("vchitemname"), " ", "_") & ".csv"
	response.AddHeader "Content-Disposition", "attachment; filename=" & strFileName
	

	strSQL = "SELECT Count(PSP.[intitemid]) AS [numitems], PSP.[intitemid], I.[vchitemname], I.[vchsize], I.[vchnumsides], I.[vchsubstrate], I.[vchcolorprocess], I.[intparentid], F.[vchitemname] AS FixtureName"
	strSQL = strSQL & " FROM " & STR_TABLE_PROFILE_STORE_PACKAGE & " PSP LEFT JOIN " & STR_TABLE_INVENTORY & " I ON PSP.[intitemid] = I.[intid] LEFT JOIN [HBO_inv] F ON I.[intparentid] = F.[intid]"
	strSQL = strSQL & " WHERE [intitemid] IN (SELECT DISTINCT [intitemid] FROM " & STR_TABLE_PROFILE_STORE_PACKAGE & " WHERE  [intcampaignid] =" & intcampaignid & ") "
	strSQL = strSQL & " AND I.[chrStatus]<>'D' AND I.[chrStatus]<>'I' And I.[intParentID] <> 314 "
	strSQL = strSQL & " GROUP  BY PSP.[intitemid], I.[vchitemname], F.[vchitemname], I.[vchsize], I.[vchnumsides], I.[vchsubstrate], I.[vchcolorprocess], I.[intparentid] "
	strSQL = strSQL & " ORDER  BY F.[vchitemname], I.[vchitemname] "
	
	set rsItems = gobjconn.execute(strSQL)
	response.write """Date"",""" & Date & """" & vbCrLf
	response.write """Vendor name"",""Dome""" & vbCrLf
	response.write """Job name"",""" & rsCampaignInfo("vchItemName") & """" & vbCrLf & vbCrLf
	
	response.write """Fixture Type"",""Item name"",""Total # of pieces"",""Item Size"",""Substrate"",""Number of sides"",""Color""" & vbCrLf
	if not rsItems.eof then
		while not rsItems.eof
			
			response.write """" & rsItems("FixtureName") & """,""" & rsItems("vchitemname") & """," & rsItems("numitems")
			response.write ",""" & rsItems("vchsize") & """,""" & rsItems("vchsubstrate") & """"
			response.write "," & rsItems("vchnumsides") & ",""" & rsItems("vchcolorprocess") & """" & vbCrLf
			
			rsItems.moveNext()
		wend
	end if
	
	strSQL = "SELECT I.vchPartNumber, (PP.[intQty] * (select count(intStoreShopperID) from " & STR_TABLE_PROFILE_MAPPER & " P2 where P2.intProfileID in (select distinct intProfileID from " & STR_TABLE_PROFILE_PACKAGES & " where intCampaignID = " & request.form("campaignid") & "))) AS [numitems], PP.[intitemid], I.[vchitemname], I.[vchsize], I.[vchnumsides], I.[vchsubstrate], I.[vchcolorprocess], I.[intparentid], F.[vchitemname] AS FixtureName"
	strSQL = strSQL & " FROM " & STR_TABLE_PROFILE_PACKAGES & " PP LEFT JOIN " & STR_TABLE_INVENTORY & " I ON PP.[intitemid] = I.[intid] LEFT JOIN [HBO_inv] F ON I.[intparentid] = F.[intid]"
	strSQL = strSQL & " WHERE  "
	strSQL = strSQL & " I.[chrStatus]<>'D' AND I.[chrStatus]<>'I' And I.[intParentID] = 314 "
	strSQL = strSQL & " GROUP  BY PP.[intitemid], PP.intQty, I.vchPartNumber, I.[vchitemname], F.[vchitemname], I.[vchsize], I.[vchnumsides], I.[vchsubstrate], I.[vchcolorprocess], I.[intparentid] "
	strSQL = strSQL & " ORDER  BY F.[vchitemname], I.[vchitemname] "
	
	'response.write strSQL
	'response.end
	set rsItems = gobjconn.execute(strSQL)
	
	if not rsItems.eof then
		response.write vbCrLf & vbCrLf & """Hardware""" & vbCrLf & vbCrLf
		response.write """SKU"",""Item name"",""Total # of pieces""" & vbCrLf
		while not rsItems.eof
			
			response.write """" & rsItems("vchPartNumber") & """,""" & rsItems("vchitemname") & """," & rsItems("numitems") & vbCrLf
			
			rsItems.moveNext()
		wend
	end if
	
	response.write vbCrLf

end function

CloseConn()
%>