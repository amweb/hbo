select distinct LS.vchSlotName, 
case when PP.intItemID is null 
    then (
		select vchItemName from poddemo_Inv 
		where intID =  LS.intItemID) 
	else (
		select vchItemName from poddemo_Inv 
		where intID =  PP.intItemID) 
end as ItemName, 
PP.intItemID as ItemID, 
case when PP.intItemID is not null 
	then (
		select vchNumSides from poddemo_Inv where intID =  PP.intItemID) 
	else null 
end as vchSides 
from poddemo_LayoutSlots LS 
left join poddemo_ProfilePackages PP 
ON LS.vchSlotName = PP.vchSlotName and PP.intCampaignID = 117 and PP.intProfileID in (select intProfileID from poddemo_ProfileMapper where intStoreShopperID = 127) 


-- showing items for store
SELECT TOP 1 
    PP.vchslotname, 
    I.vchitemname, 
    I.vchnumsides
FROM poddemo_profilepackages AS PP
    INNER JOIN poddemo_profilemapper AS PM
        ON PP.intprofileid = PM.intprofileid
    INNER JOIN poddemo_inv AS I
        ON PP.intitemid = I.intid
WHERE PP.intcampaignid = 123
AND PM.intstoreshopperid = 146
ORDER BY I.intid DESC

-- first profile in a campaign for a particular store
SELECT TOP 1 *
FROM poddemo_profilepackages AS PP
INNER JOIN poddemo_inv AS I
ON PP.intcampaignid = I.intid 

WHERE 
ORDER BY P.intid ASC