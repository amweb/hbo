<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: default.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Main store-front, inventory browser
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: code cleanup/documentation
' = Description of Customizations:
' =   (none)
' =====================================================================================
OpenConn

If gintUserName&"" = ""  Then
Response.Redirect virtualbase
End if

DrawHeader "Dashboard", "custserv"

If Request("p")&""="2" Then
    DrawPreBuyPage
Else
    DrawPage
End if
DrawFooter "custserv"
%>
	<script src="../assets/global/plugins/vue.js"></script>
	<script src="../assets/global/plugins/bootstrap-vue.umd.min.js"></script>
	<script src="../assets/js/order.js"></script>
<%
Sub DrawPage
%>
	<link rel="stylesheet" href="../assets/global/plugins/bootstrap-vue.css">
	<style>
		[v-cloak]{
			display:none;
		}
	</style>
	<div class="container-fluid cont-mar mt-3" id="dashboard" v-cloak>
		<h3>Guide Book Reorder</h3>
		<div class="row">
			<div class="col-lg-3">
				<form @submit.prevent="addOrder">
					<label for="">Property Name</label>
					<input v-model="obj.propName" type="text" class="form-control" required>
					<label for="">Address 1</label>
					<input v-model="obj.address1" type="text" class="form-control" required>
					<label for="">Address 2</label>
					<input v-model="obj.address2" type="text" class="form-control">
					<div class="row">
						<div class="col-6">
							<label for="">City</label>
							<input v-model="obj.city" type="text" class="form-control" required>
						</div>
						<div class="col-6">
							<label for="">State</label>
							<input v-model="obj.state" type="text" list="states" class="form-control" required>
							<datalist id="states">
								<option v-for="s in stateList" :value="s.value">{{s.text}}</option>
							</datalist>	
						</div>
						<div class="col-6">
							<label for="">Zip</label>
							<input v-model="obj.zip" class="form-control" required>
						</div>
					</div>
					<label for="">Quantity</label>
					<select v-model.number="obj.qty" class="form-control" required>
						<option value="1">50</option>
						<option value="2">100</option>
						<option value="3">150</option>
						<option value="4">200</option>
						<option value="5">250</option>
						<option value="6">300</option>
						<option value="7">350</option>
						<option value="8">400</option>
						<option value="9">450</option>
						<option value="10">500</option>
						<option value="11">550</option>
						<option value="12">600</option>
						<option value="13">650</option>
						<option value="14">700</option>
						<option value="15">750</option>
						<option value="16">800</option>
						<option value="17">850</option>
						<option value="18">900</option>
						<option value="19">950</option>
						<option value="20">1000</option>
					</select>
					<label for="">Guide Version Code</label>
					<select v-model="obj.guide" class="form-control" required>
						<option :value="null">--Select Guide Code--</option>
						<option v-for="ver in versionCode" :value="ver.intID" :key="ver.intID">{{ver.vchCode}} - {{ver.vchVersion}}</option>
					</select>
					<div class="text-muted">{{obj.guide?versionCode.find(i=>i.intID===obj.guide).vchPartNumber:''}}</div>
					<label for="">Notes</label>
					<select v-model="obj.note" class="form-control">
						<option v-for="nL in noteList" :value="nL">{{nL}}</option>
						<option :value="null">Other</option>
					</select>
					<div v-if="obj.note===null" class="ml-3 mb-3">
						<label>Other Notes</label>
						<input v-model="obj.otherNote" type="text" maxlength="60" class="form-control">
						<small class="text-muted pull-right">{{obj.otherNote.length}}/60</small>
					</div>
					<button class="btn btn-success mt-2 pull-right" type="submit">Add to queue</button>
				</form>
			</div>
			<div class="col-lg-9">
				<div class="row my-3">
					<div class="w-100">
						<span class="h4">Orders in Queue</span>
						<b-btn variant="success" :disabled="orders.length<1||sending" @click.prevent="sendOrder()" class="mx-3">
							<b-spinner small v-if="sending" label="Loading..."></b-spinner>
							Process orders in queue
						</b-btn>
					</div>
				</div>
				<b-table :items="orders"
						 :fields="ordersField"
						 sticky-header="500px"
						 striped
						 show-empty
						 empty-text="There are no orders in queue"
						 small>
					<template v-slot:cell(guideVersion)="data">
						{{formatVC(data.item)}}
					</template>
					<template v-slot:cell(intQuantity)="data">
						{{data.item.intQuantity*50}}
					</template>
					<template v-slot:cell(button)="data">
						<a @click.prevent="editOrder(data.item)" href="" class="mx-1 text-dark"><i class="fa fa-pencil"></i></a>
						<a @click.prevent="deleteOrder(data.item.intID)" href="" class="mx-1 text-danger"><i class="fa fa-trash"></i></a>
					</template>
					<template v-slot:empty="scope">
						<h6>{{ scope.emptyText }}</h6>
					</template>
				</b-table>
				<hr>
				<b-row class="mt-3">
					<b-col>
						<b-table 
							:items="invList"
							small
							:busy="stockUpdate"
							:fields="invFields"
							bordered>
							<template v-slot:table-busy>
								<div class="text-center text-danger my-2">
								<b-spinner class="align-middle"></b-spinner>
								<strong>Gathering Inventory Stock...</strong>
								</div>
							</template>
						</b-table>
					</b-col>
					<b-col></b-col>
				</b-row>
				<b-modal id="edit"
						 size="lg"
						 title="Edit Order"
						 hide-footer
						 @hidden="selOrder=null">
					<form @submit.prevent="sendEdit()" v-if="selOrder">
						<label for="">Property Name</label>
						<input v-model="selOrder.vchCompany" type="text" class="form-control" required>
						<label for="">Address 1</label>
						<input v-model="selOrder.vchAddress1" type="text" class="form-control" required>
						<label for="">Address 2</label>
						<input v-model="selOrder.vchAddress2" type="text" class="form-control">
						<div class="row">
							<div class="col-6">
								<label for="">City</label>
								<input v-model="selOrder.vchCity" type="text" class="form-control" required>
							</div>
							<div class="col-6">
								<label for="">State</label>
								<input v-model="selOrder.vchState" type="text" list="states" class="form-control" required>
								<datalist id="states">
									<option v-for="s in stateList" :value="s.value">{{s.text}}</option>
								</datalist>	
							</div>
							<div class="col-6">
								<label for="">Zip</label>
								<input v-model="selOrder.vchZip" class="form-control" required>
							</div>
						</div>
						<label for="">Quantity</label>
						<select v-model.number="selOrder.intQuantity" class="form-control" required>
							<option value="1">50</option>
							<option value="2">100</option>
							<option value="3">150</option>
							<option value="4">200</option>
							<option value="5">250</option>
							<option value="6">300</option>
							<option value="7">350</option>
							<option value="8">400</option>
							<option value="9">450</option>
							<option value="10">500</option>
							<option value="11">550</option>
							<option value="12">600</option>
							<option value="13">650</option>
							<option value="14">700</option>
							<option value="15">750</option>
							<option value="16">800</option>
							<option value="17">850</option>
							<option value="18">900</option>
							<option value="19">950</option>
							<option value="20">1000</option>
						</select>
						<label for="">Guide Version Code</label>
						<select v-model.number="selOrder.intInvID" class="form-control" @change="invChange()" required>
							<option :value="null">--Select Guide Code--</option>
							<option v-for="ver in versionCode" :value="ver.intInvID" :key="ver.intID">{{ver.vchCode}} - {{ver.vchVersion}}</option>
						</select>
						<div class="text-muted">{{selOrder?versionCode.find(i=>i.intInvID===selOrder.intInvID).vchPartNumber:''}}</div>
						<label>Notes</label>
						<select v-model="selOrder.vchNote" @change="selOrder.otherNote=null" class="form-control">
							<option v-for="nL in noteList" :value="nL">{{nL}}</option>
							<option :value="selOrder.otherNote">Other</option>
						</select>
						<div v-if="!noteList.includes(selOrder.vchNote)"  class="ml-3 mb-3">
							<label>Other Notes</label>
							<input v-model="selOrder.otherNote" type="text" maxlength="60" class="form-control">
						</div>
						<button type="submit" :disabled="editing" class="btn btn-success pull-right my-3">Save</button>
					</form>				
				</b-modal>
			</div>
		</div>
	</div>
<%
End Sub

%>