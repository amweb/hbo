<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: itemdetail.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Display details on an item from the inventory.
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

OpenConn
SelectCurrentOrder

dim intID
intID = Request("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if

Inventory_UpdateHitCount intID

dim intParentID, strItemName, strPartNumber, intStock, strImageURL, strImageURL2, mnyItemPrice, mnyShipPrice, blnTaxFlag, blnSoftFlag, fltShipWeight, intForceShipMethod, blnForceSoloItem, strOptionList1, strOptionList2, strOptionList3, intMinQty, strDescription
' intStock = quantity in stock (if supported)
' mnyItemPrice = unit price
' mnyShipPrice = special shipping price for this item
' blnTaxFlag = true if item is taxable
' blnSoftFlag = true if item is a software item (downloable after order is authorized)
' fltShipWeight = weight of item in lbs (if supported)
Inventory_GetItemInfo intID, intParentID, strItemName, strPartNumber, intStock, strImageURL, strImageURL2, mnyItemPrice, mnyShipPrice, blnTaxFlag, blnSoftFlag, fltShipWeight, intForceShipMethod, blnForceSoloItem, strOptionList1, strOptionList2, strOptionList3, intMinQty, strDescription

if strItemName & "" = "" then
	' item not found--user probably bookmarked this page
	response.redirect "default.asp"
end if

dim strPageTitle, strParentName
strPageTitle = strItemName

Custom_GetGlobalInventoryFolders 0

DrawHeader strPageTitle, "storemain"
DrawPage
DrawFooter "storemain"

sub DrawPage
	dim strImageTag, strImageTag2, strQtyMsg
	strImageTag2 = ""
	if strImageURL & "" = "" then
		strImageTag	= "<img src=""" & virtualbase & "images/icon-no-image-512.png"" width=""150"" height=""168"" border=""0"" alt=""" & strItemName & """ />"
	else
		strImageTag = "<img src=""" & imagebase & "products/big/" & strImageURL & """ border=""0"" alt=""" & strItemName & """ />"
	end if
	if strImageURL2 & "" <> "" then
		strImageTag2 = "<img src=""" & imagebase & "products/big/" & strImageURL2 & """ border=""0"" alt=""" & strItemName & """ />"
	end if
%>		
		<h1><%= strItemName %></h1>
		<br />
		<div style="width:100%">
			<div style="float:left">
			<a href="default.asp?parentid=<%= intParentID %>"><img src="<%= g_imagebase %>menu/go-back.gif" width="62" height="16" border="0" alt="Go Back"></a>
			</div>
			<% if gintOrderItemCount > 0 and false then %>
			<div style="float:right">
			<a href="cartlist.asp?parentid="<%= intParentID %>"><img src="<%= g_imagebase %>menu/view-cart.gif" width="70" height="16" border="0" alt="View Cart" align="right"></a>&nbsp;&nbsp;
			</div>
			<% end if %>
		</div>

		<HR COLOR="#663366">

		<div style="float:left">
			<h4>Item# <%= strPartNumber %></h4>

			<h2><%= strItemName %></h2>

		<div id="productimage">
			<p align="center"><%= strImageTag %><%= strImageTag2 %><br />
			<a href="itemphoto.asp?id=<%= intID %>" target="_blank"><img src="<%= g_imagebase %>menu/view-larger.gif" width="86" height="20" alt="<%= strItemName %>" /></a></p>
		</div>

			<h3><%= SafeFormatCurrency("$0.00", mnyItemPrice, 2) %></h3>
			<FORM ACTION="cartlist.asp?action=additem" METHOD="POST">
			<INPUT TYPE="hidden" NAME="id" VALUE="<%= intID %>">
			<INPUT TYPE="hidden" NAME="parentid" VALUE="<%= intParentID %>">
			<%
		dim dctOptions
		if (not (strOptionList1 = "")) or (not (strOptionList2 = "")) then
			if not (strOptionList1 = "") then
				set dctOptions = GetOptionDct(strOptionList1)
				response.write "<div style=""width:37%;padding:2px;border-bottom:1px;"">Please choose one: "
				ArrayPulldown "Option1", "", Request, dctOptions
				response.write "<br /></div>"
			end if
			if not (strOptionList2 = "") then
				set dctOptions = GetOptionDct(strOptionList2)
				response.write "<div style=""width:37%;padding:2px;border-bottom:1px;"">Please choose one: "
				ArrayPulldown "Option2", "", Request, dctOptions
				response.write "<br /></div>"
			end if
			if not (strOptionList3 = "") then
				set dctOptions = GetOptionDct(strOptionList3)
				response.write "<div style=""width:37%;padding:2px;border-bottom:1px;"">Please choose one: "
				ArrayPulldown "Option3", "", Request, dctOptions
				response.write "<br /></div>"
			end if
		end if
			%>
			<div class="buybuttons">
			<INPUT TYPE="image" src="<%= g_imagebase %>menu/add-to-cart.gif" width="80" height="16" border="0" alt="Add to Cart" /></a>&nbsp;
			<B>Qty:</B> <INPUT TYPE="text" NAME="qty" VALUE="<%= intMinQty %>" SIZE=2 MAXLENGTH=5>
			</div>
			</FORM>
			<p>
		<% if false then ' not IsNull(intForceShipMethod) %>
			<% 'This item must be shipped by <B><x%= GetArrayValue(intForceShipMethod, dctShipOption) %x></B>.<BR> %>
			<b>Special Shipping Required.</b><br />
		<% end if %>
		<% if blnForceSoloItem then %>
			This item must be <b>ordered seperately</b>.<br />
		<% end if %>
			</p>

			<p><%= strDescription %></p>
		</div>

		<p id="top" align="center"><a href="#top"><strong>^ Top</strong></a></p>
<%
end sub
%>