<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: default.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Main store-front, inventory browser
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: code cleanup/documentation
' = Description of Customizations:
' =   (none)
' =====================================================================================
OpenConn

If gintUserName&"" = ""  Then
Response.Redirect virtualbase
End if

DrawHeader "Dashboard", "custserv"

If Request("p")&""="2" Then
    DrawPreBuyPage
Else
    DrawPage
End if
DrawFooter "custserv"
%>
	<script src="../assets/global/plugins/vue.js"></script>
	<script src="../assets/global/plugins/bootstrap-vue.umd.min.js"></script>
	<script src="../assets/js/inv.js"></script>
<%
Sub DrawPage
%>
	<link rel="stylesheet" href="../assets/global/plugins/bootstrap-vue.css">
	<style>
		[v-cloak]{
			display:none;
		}
	</style>
	<div class="container-fluid cont-mar mt-3" id="inv" v-cloak>
        <b-row>
            <b-col cols="1"><label>New SKU:</label></b-col>
            <b-col cols="3">
                <b-input v-model="name" placeholder="ex. March 2020"></b-input>
            </b-col>
        </b-row>
        <div v-if="name!=''">
            <h6>The following SKU will be created and activate</h6>
            <ul>
                <li v-for="(v,i) in version">{{name+' - '+v}}</li>
            </ul>
        </div>
        <b-row v-if="name!=''">
            <b-col cols="4">
                <b-btn variant="success" @click="createActivate()" class="pull-right">Create and Activate</b-btn>
            </b-col>
        </b-row>
	</div>
<%
End Sub

%>