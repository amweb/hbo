<!--#include file="../drcpod/config/incUStore.asp"-->
<%
OpenConn
Dim strSQL,rs
Response.ContentType = "application/json; charset=utf-8"
if Session(SESSION_MERCHANT_UID)&"" = "" then
	response.redirect virtualbase & "store/dashboard.asp"
end if
Select Case Request("action")
    Case "addOrder"
        addOpenOrder
    Case "getOrders"
        getOrders
    Case "getVersionCode"
        getVersionCode
    Case "deleteOrder"
        deleteOrder
    Case "editOrder"
        editOrder
    Case "sendVera"
        sendVera
    Case "getProductAvail"
        getProductAvail
    Case "createActivate"
        createActivate
End Select
Sub addOpenOrder
    Dim location,inv
    Set location=Server.CreateObject("Scripting.Dictionary")
    location.Add "company",Request("propName")
    location.Add "address1",Request("address1")
    location.Add "address2",Request("address2")
    location.Add "city",Request("city")
    location.Add "state",Request("state")
    location.Add "zip",Request("zip")

	Set inv = Server.CreateObject("Scripting.Dictionary")
	inv.Add "intInvID",Request("intInvID")
	inv.Add "vchItemName",Request("vchItemName")
	inv.Add "vchPartNumber",Request("vchPartNumber")
	inv.Add "intQuantity",Request("intQuantity")
	inv.Add "vchNote",Request("vchNote")

    call CreateOrder_WShip(inv,location,"0")
End Sub
Sub getOrders
    strSQL = "SELECT * FROM "&STR_TABLE_ORDER&" WHERE chrStatus='0' "
    
    Response.Write QueryToJSON(strSQL).flush
End Sub
Sub getVersionCode
	strSQL = "SELECT C.intID,vchVersion,vchCode,vchItemName,intInvID,vchPartNumber,vchTimeZone,intBundleQuantity,I.intStock FROM "&_
	STR_TABLE_CODE&" C JOIN "&STR_TABLE_INVENTORY&" I ON I.intID=C.intInvID AND I.chrStatus='A' WHERE C.chrStatus='A'"

	Response.Write QueryToJSON(strSQL).flush
End Sub
Sub deleteOrder
	strSQL = "Update "&STR_TABLE_ORDER&" SET chrStatus='D' WHERE intID="&clng(Request("orderID"))
	gobjConn.execute(strSQL)
	Response.write "{""proccess"":""complete""}"
End Sub
Sub sendVera
	Dim orders,o
	set orders = request("orders[]")
	for each o in orders
		call AddOrder(clng(o))
		strSQL = "Update "&STR_TABLE_ORDER&" SET chrStatus='P',dtmSubmitted=GETDATE() WHERE intID="&o
		gobjConn.execute(strSQL)
	next
	Response.write "{""proccess"":""complete""}"
End Sub
Sub createActivate
	Dim basename,code,c
	basename = trim(Request("name")&"")
	code = Split(Request("code"),",")
	For each c in code
		strSQL = "INSERT INTO "&STR_TABLE_INVENTORY&" (chrType,chrStatus,dtmCreated,dtmUpdated,vchCreatedByUser,vchUpdatedByUser,vchCreatedByIP,vchUpdatedByIP,vchItemName,vchPartNumber,intBundleQuantity) "&_	
				 "VALUES "&_
				 "('I','A',GETDATE(),GETDATE(),'pub','pub','::1','::1','"&basename&"','"&basename&" - "&trim(c)&"',50);"&_
				 "UPDATE "&STR_TABLE_CODE&" SET intInvID=SCOPE_IDENTITY() WHERE vchVersion='"&trim(c)&"'"
		gobjConn.execute(strSQL)
	Next
	Response.Write "{""proccess"":""complete""}"
End Sub
Sub editOrder
	strSQL = "Update "&STR_TABLE_ORDER&" SET "
	strSQL = strSQL & "vchCompany='"& ProtectSQL(Request("order[vchCompany]")) &"',"
	strSQL = strSQL & "vchAddress1='"& ProtectSQL(Request("order[vchAddress1]")) &"',"
	If Request("order[vchAddress2]")<>"" Then
		strSQL = strSQL & "vchAddress2='"& ProtectSQL(Request("order[vchAddress2]")) &"',"
	End if
	strSQL = strSQL & "vchCity='"& ProtectSQL(Request("order[vchCity]")) &"',"
	strSQL = strSQL & "vchState='"& ProtectSQL(Request("order[vchState]")) &"',"
	strSQL = strSQL & "vchZip='"& ProtectSQL(Request("order[vchZip]")) &"',"
	strSQL = strSQL & "vchItemName='"& ProtectSQL(Request("order[vchItemName]")) &"',"
	strSQL = strSQL & "vchPartNumber='"& ProtectSQL(Request("order[vchPartNumber]")) &"',"
	strSQL = strSQL & "vchNote='"& ProtectSQL(Request("order[vchNote]")) &"',"
	strSQL = strSQL & "intQuantity="& clng(Request("order[intQuantity]")) &","
	strSQL = strSQL & "intInvID="& clng(Request("order[intInvID]"))
	strSQL = strSQL & " WHERE intID = "& clng(Request("order[intID]"))
	gobjConn.execute(strSQL)
	Response.Write "{""proccess"":""complete""}"
End Sub
function CreateOrder_WShip(inv,location,strStatus)
	' creates a new order
	dim dctSaveList, intOrderID
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "*@dtmCreated", "GETDATE()"
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "*vchCreatedByUser", gintUserName
		dctSaveList.Add "vchUpdatedByUser", gintUserName
		dctSaveList.Add "*vchCreatedByIP", gstrUserIP
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "chrType", "R"
		dctSaveList.Add "chrStatus", strStatus
		dctSaveList.Add "vchCompany", location.Item("company")
		dctSaveList.Add "vchAddress1", location.Item("address1")
		dctSaveList.Add "vchAddress2", location.Item("address2")
		dctSaveList.Add "vchCity", location.Item("city")
		dctSaveList.Add "vchState", location.Item("state")
		dctSaveList.Add "vchZip", location.Item("zip")
		dctSaveList.Add "#intInvID", inv.Item("intInvID")
		dctSaveList.Add "vchItemName", inv.Item("vchItemName")
		dctSaveList.Add "vchPartNumber", inv.Item("vchPartNumber")
		dctSaveList.Add "#intQuantity", inv.Item("intQuantity")
		dctSaveList.Add "vchNote", inv.Item("vchNote")
	intOrderID = SaveDataRecord("" & STR_TABLE_ORDER, Request, 0, dctSaveList)
	CreateOrder_WShip = intOrderID
end function
Function QueryToJSON(SQL)
    Dim jsa, col
    Set rs = gobjConn.execute(SQL)
    On Error GoTo 0
    Set jsa = jsArray()
    Do While Not (rs.EOF Or rs.BOF)
            Set jsa(Null) = jsObject()
        For Each col In rs.Fields
                jsa(Null)(col.Name) = col.Value
        Next
    rs.MoveNext
    Loop
    Set QueryToJSON = jsa
    rs.Close
End Function
Function getProductAvail
	strSQL = "SELECT intID,vchPartNumber FROM "&STR_TABLE_INVENTORY&" WHERE intID in (SELECT DISTINCT intInvID FROM "&STR_TABLE_CODE&") and chrStatus='A'"
	set rs = gobjConn.execute(strSQL)
	if not rs.eof then
		Do Until rs.eof
			Dim stock
			stock = GetProductAvailabilites(rs("vchPartNumber"))
			strSQL = "UPDATE "&STR_TABLE_INVENTORY&" SET intStock="&stock&" ,dtmUpdated=GETDATE(), vchUpdatedByUser='"&gintUserName&"'"
			strSQL = strSQL &" WHERE intID="&rs("intID")
			gobjConn.execute(strSQL)

			rs.movenext
		Loop
	end if
	Response.Write "{""proccess"",""complete""}"
End Function
%>