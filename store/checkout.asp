<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<!--#include file="../config/incEcho.asp"-->
<%

OpenConn

Session.Timeout = 60

SelectCurrentOrder

'------------------JRMCodeChange---------
'For Users that do not want to register.  Any name will do here, since on the front end if requires
'an @ sign.  You'd need to disable this in incGoCartAPI if they change their minds.
' SWS: modified to always call LoginDummyUser() if no user is logged-in.
If Request("NoUser") = 1 or gintUserID = 0 then
	LoginDummyUser
end if

'response.write "order: " & gIntOrderID
'response.write "<BR>user: " & gIntuserID

const STR_PAGE_TYPE = "checkout"

dim strAction, intStep, intMaxStep, TmpBillRecID
strAction = lcase(Request("action"))
if left(strAction, 4) <> "step" then
	strAction = ""
end if
intStep = mid(strAction, 5, 1)
if IsNumeric(intStep) then
	intStep = clng(intStep)
else
	intStep = 1
end if

if strAction = "" then
	strAction = "step1"
	intStep = 1
end if

dim dctErrors
set dctErrors = Server.CreateObject("Scripting.Dictionary")
dctErrors.Add "paymentok", true
ValidateOrder dctErrors
ValidateInput dctErrors

dctErrors.Remove "paymentok"
if dctErrors.Exists("giftmsg") then
	dctErrors.Remove "giftmsg"
end if
if dctErrors.Exists("account") or dctErrors.Exists("order") then
	response.redirect "custserv.asp"
end if
if dctErrors.Exists("billing") then
	intMaxStep = 1
elseif dctErrors.Exists("shipping") then
	intMaxStep = 1
elseif dctErrors.Exists("shipoption") then
	intMaxStep = 2
elseif dctErrors.Exists("payment") then
	intMaxStep = 2
else
	intMaxStep = 4
end if
'set dctErrors = nothing
if intStep > intMaxStep then
	strAction = "step" & intMaxStep
	intStep = intMaxStep
end if
'response.write "*ID: " & gintUserID
'response.write "<BR>action: " & strAction
dim dctList, strPageTitle, intID
select case strAction
	case "step0":
		' user wants to modify their shopping cart
		UpdateOrderStatus "0"
		response.redirect nonsecurebase & "store/cartlist.asp"
	case "step1": ' STEP 1: Order Form
		strPageTitle = "Step 1: Order Form"
		
		
		DrawCheckoutStatusBar intStep
		DrawPage_Step1
	case "step1submit"
		strPageTitle = "Step 1: Order Form"
		AutoCheck Request, ""
		if gintUserID = -1 and Request("strPassword") <> ""  and Request("strPassword") <> " " then
			dim strSQL, rsTemp
			strSQL = "SELECT intID,vchFirstName,vchLastName,vchPassword FROM " & STR_TABLE_SHOPPER & " WHERE chrStatus = 'A' AND chrType = 'A' AND vchEmail = '" & SQLEncode(Request("bill_strEmail")) & "'"
			set rsTemp = ConnOpenRS(strSQL)
			if not rsTemp.eof then
				if rsTemp("vchPassword") <> Request("strPassword") then
					FormErrors.Add "strPassword", "An account already exists for the address '" & Request("bill_strEmail") & "'."
				else	' login this user
					gintUserID = CLng(rsTemp("intID"))
					Session(SESSION_PUB_USER_ID) = gintUserID
					Session(SESSION_PUB_USER_NAME) = rsTemp("vchFirstName") & " " & rsTemp("vchLastName")
					'Session(SESSION_PUB_USER_NAME) = Request("bill_strFirstName") & " " & Request("bill_strLastName")
					gintUserName = Session(SESSION_PUB_USER_NAME)
					'------------------JRMCodeChange---------
					Session("DummyUser") = false
			
					' change order to be this account
					strSQL = "UPDATE " & STR_TABLE_ORDER & " SET intShopperID=" & gintUserID & " WHERE intID=" & gintOrderID
					gobjConn.execute(strSQL)
				end if
			end if
			rsTemp.close
			set rsTemp = nothing
		end if
		if Request("chkUseSame") <> "" then
			' remove all shipping errors
			dim e
			for each e in FormErrors
				if left(e,5) = "ship_" then
					FormErrors.Remove e
				end if
			next
		end if
		if FormErrors.count = 0 then
			' submit form and proceed to step 2
			SubmitOrderForm
			UpdateOrderStatus 2
			response.redirect "checkout.asp?action=step2"
		else
			
			DrawCheckoutStatusBar intStep
			DrawPage_Step1
		end if
	case "step2": ' STEP 2 - Shipping Options
		dim mnySubtotal, strShipCountry, strShipState, intShipOption
		GetOrderShippingZone mnySubtotal, strShipCountry, strShipState, intShipOption
		
		DrawCheckoutStatusBar 2
		DrawPage_Step2 mnySubtotal, strShipCountry, strShipState, intShipOption
		
	case "step2submit":
		AutoCheck Request, ""
		intShipOption = Request("intShipOption")
		if IsNumeric(intShipOption) then
			intShipOption = CLng(intShipOption)
		else
			intShipOption = 0
		end if
		if intShipOption = 0 then
			FormErrors.Add "intShipOption", "Please select a shipping option:"
		end if
		CheckField "chrPaymentMethod", "IsEmpty", Request, "Please select a payment method."
		if Request("chrPaymentMethod") = "OCC" then
			CheckField "chrPaymentCardExpMonth~chrPaymentCardExpYear", "CCExp", Request, "Invalid Expiration"
		end if

		if FormErrors.count = 0 then
			SetOrderShipOption intShipOption
			SetOrderPaymentMethod Request("chrPaymentMethod")
			if Request("chrPaymentMethod") = "OCC" then
				SetOrderPaymentInfo_OCC GetOrderBillName(), Request("vchPaymentCardType"), Request("vchPaymentCardNumber"), Request("vchPaymentCardExtended"), Request("chrPaymentCardExpMonth"), Request("chrPaymentCardExpYear")
				if lcase(Request("strSaveProfile")) = "y" then
					UpdateBillRecordPayment "OCC", Request("vchPaymentCardType"), Request("vchPaymentCardNumber"), Request("vchPaymentCardExtended"), Request("chrPaymentCardExpMonth"), Request("chrPaymentCardExpYear")
				end if
				RecalcOrder
			end if
			if Request("rr") <> "" then
				UpdateOrderStatus "4"
				response.redirect "checkout.asp?action=step4"
			else
				UpdateOrderStatus "3"
				response.redirect "checkout.asp?action=step3"
			end if
		else
			' use a temp. variable for the call to GetOrderShippingZone -- we want to use the current Request("intShipOption")
			' value for the call to DrawPage
			dim t
			GetOrderShippingZone mnySubtotal, strShipCountry, strShipState, t
			
			DrawCheckoutStatusBar 2
			DrawPage_Step2 mnySubtotal, strShipCountry, strShipState, intShipOption
			
		end if
	case "step3"
		
		DrawCheckoutStatusBar 3
		DrawPage_Step3
		
	case "step3submit":
		UpdateOrderStatus "4"
		response.redirect "checkout.asp?action=step4"
	case "step4":
		ProcessOrder
	case else:
		response.write "unimplemented action: " & strAction
end select
response.end

sub SubmitOrderForm()
	dim strSQL, rsTemp, dctSaveList, intTaxZone
	
	dim intBillID, intShipID, intNewBillID, intNewShipID
	if gintUserID = -1 then
		if Request("strPassword") <> "" and Request("strPassword") <> " " then
			' create an account
			set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			intTaxZone = iif((ucase(Request("bill_strCountry")) = "US") and (ucase(Request("bill_strState")) = "CA"), 1, 0)
			dctSaveList.Add "*@dtmCreated", "GETDATE()"
			dctSaveList.Add "@dtmUpdated", "GETDATE()"
			dctSaveList.Add "*vchCreatedByUser", "pub"
			dctSaveList.Add "vchUpdatedByUser", "pub"
			dctSaveList.Add "*vchCreatedByIP", gstrUserIP
			dctSaveList.Add "vchUpdatedByIP", gstrUserIP
			dctSaveList.Add "chrType", "A"
			dctSaveList.Add "*chrStatus", "A"
			dctSaveList.Add "#intShopperID", 0
			dctSaveList.Add "vchLabel", "unlabelled"
			dctSaveList.Add "vchEmail", Request("bill_strEmail")
			dctSaveList.Add "vchPassword", Request("strPassword")
			dctSaveList.Add "vchFirstName", Request("bill_strFirstName")
			dctSaveList.Add "vchLastName", Request("bill_strLastName")
			gintUserID = SaveDataRecord(STR_TABLE_SHOPPER, Request, intBillID, dctSaveList)
			set dctSaveList = nothing
			
			' login this user
			Session(SESSION_PUB_USER_ID) = gintUserID
			Session(SESSION_PUB_USER_NAME) = Request("bill_strFirstName") & " " & Request("bill_strLastName")
			gintUserName = Session(SESSION_PUB_USER_NAME)
			'------------------JRMCodeChange---------
			Session("DummyUser") = false
			
			' change order to be this account
			strSQL = "UPDATE " & STR_TABLE_ORDER & " SET intShopperID=" & gintUserID & " WHERE intID=" & gintOrderID
			gobjConn.execute(strSQL)
			
			intBillID = 0
			intShipID = 0
		else
			' lookup existing billing and shipping record IDs, if any
			strSQL = "SELECT B.intID AS intBillID, S.intID AS intShipID"
			strSQL = strSQL & " FROM " & STR_TABLE_ORDER & " AS O"
			strSQL = strSQL & " LEFT JOIN " & STR_TABLE_SHOPPER & " AS B"
			strSQL = strSQL & " ON O.intBillShopperID = B.intID AND B.chrStatus = 'A'"
			strSQL = strSQL & " LEFT JOIN " & STR_TABLE_SHOPPER & " AS S"
			strSQL = strSQL & " ON O.intShipShopperID = S.intID AND S.chrStatus = 'A'"
			strSQL = strSQL & " WHERE O.intID = " & gintOrderID
			set rsTemp = ConnOpenRS(strSQL)
			if not IsNull(rsTemp("intBillID")) then
				intBillID = rsTemp("intBillID")
			else
				intBillID = 0
			end if
			if not IsNull(rsTemp("intShipID")) then
				intShipID = rsTemp("intShipID")
			else
				intShipID = 0
			end if
			rsTemp.close
			set rsTemp = nothing
		end if
	else
		intBillID = Request("bill_abentry")
		if IsNumeric(intBillID) then
			intBillID = CLng(intBillID)
		else
			intBillID = 0
		end if
		intShipID = Request("ship_abentry")
		if IsNumeric(intShipID) then
			intShipID = CLng(intShipID)
		else
			intShipID = 0
		end if
		' verify that it's ok to modify the existing billing and shipping records (i.e. no pre-existing orders)
		if intBillID <> 0 then
			if not CheckIsBillID_Other(gintUserID, false, intBillID) then
				intBillID = 0
			elseif CheckIsBillIDProtected(false, intBillID) then
				gobjConn.execute("UPDATE " & STR_TABLE_SHOPPER & " SET chrStatus = 'I' WHERE intID = " & intBillID)
				intBillID = 0
			end if
		end if
		if intShipID <> 0 then
			if not CheckIsBillID_Other(gintUserID, true, intShipID) then
				intShipID = 0
			elseif CheckIsBillIDProtected(false, intShipID) then
				gobjConn.execute("UPDATE " & STR_TABLE_SHOPPER & " SET chrStatus = 'I' WHERE intID = " & intShipID)
				intShipID = 0
			end if
		end if
	end if
	
	' special situation:
	'   if billing and shipping addresses are not an exact match, but are pointing to the same record ID,
	'   then change the shipping ID to zero (to create a new one)
	if intBillID <> 0 and intBillID = intShipID then
		if Request("bill_strFirstName") <> Request("ship_strFirstName") or Request("bill_strLastName") <> Request("ship_strLastName") or Request("bill_strCompany") <> Request("ship_strCompany") or Request("bill_strAddress1") <> Request("ship_strAddress1") or Request("bill_strAddress2") <> Request("ship_strAddress2") or Request("bill_strCity") <> Request("ship_strCity") or Request("bill_strState") <> Request("ship_strState") or Request("bill_strZip") <> Request("ship_strZip") or Request("bill_strCountry") <> Request("ship_strCountry") or Request("bill_strDayPhone") <> Request("ship_strDayPhone") or Request("bill_strNightPhone") <> Request("ship_strNightPhone") then
			intShipID = 0
		end if
	end if
	
	' save billing info
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
	intTaxZone = iif((ucase(Request("bill_strCountry")) = "US") and (ucase(Request("bill_strState")) = "CA"), 1, 0)
	dctSaveList.Add "*@dtmCreated", "GETDATE()"
	dctSaveList.Add "@dtmUpdated", "GETDATE()"
	dctSaveList.Add "*vchCreatedByUser", "pub"
	dctSaveList.Add "vchUpdatedByUser", "pub"
	dctSaveList.Add "*vchCreatedByIP", gstrUserIP
	dctSaveList.Add "vchUpdatedByIP", gstrUserIP
	dctSaveList.Add "chrType", "B"
	dctSaveList.Add "*chrStatus", "A"
	dctSaveList.Add "#intShopperID", gintUserID
	dctSaveList.Add "vchLabel", "unlabelled"
	dctSaveList.Add "vchEmail", Request("bill_strEmail")
	dctSaveList.Add "vchFirstName", Request("bill_strFirstName")
	dctSaveList.Add "vchLastName", Request("bill_strLastName")
	dctSaveList.Add "vchCompany", Request("bill_strCompany")
	dctSaveList.Add "vchAddress1", Request("bill_strAddress1")
	dctSaveList.Add "vchAddress2", Request("bill_strAddress2")
	dctSaveList.Add "vchCity", Request("bill_strCity")
	dctSaveList.Add "vchState", Request("bill_strState")
	dctSaveList.Add "vchZip", Request("bill_strZip")
	dctSaveList.Add "vchCountry", Request("bill_strCountry")
	dctSaveList.Add "vchDayPhone", Request("bill_strDayPhone")
	dctSaveList.Add "vchNightPhone", Request("bill_strNightPhone")
	dctSaveList.Add "vchFax", Request("bill_strFax")
	
	dctSaveList.Add "#intTaxZone", intTaxZone
	intNewBillID = SaveDataRecord(STR_TABLE_SHOPPER, Request, intBillID, dctSaveList)
	' save shipping info
	dctSaveList.RemoveAll
	if Request("chkUseSame") = "" then
		intTaxZone = iif((ucase(Request("ship_strCountry")) = "US") and (ucase(Request("ship_strState")) = "CA"), 1, 0)
		dctSaveList.Add "*@dtmCreated", "GETDATE()"
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "*vchCreatedByUser", "pub"
		dctSaveList.Add "vchUpdatedByUser", "pub"
		dctSaveList.Add "*vchCreatedByIP", gstrUserIP
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "chrType", "B"
		dctSaveList.Add "*chrStatus", "A"
		dctSaveList.Add "#intShopperID", gintUserID
		dctSaveList.Add "vchLabel", "unlabelled"
		'dctSaveList.Add "vchEmail", Request("ship_strEmail")
		dctSaveList.Add "vchFirstName", Request("ship_strFirstName")
		dctSaveList.Add "vchLastName", Request("ship_strLastName")
		dctSaveList.Add "vchCompany", Request("ship_strCompany")
		dctSaveList.Add "vchAddress1", Request("ship_strAddress1")
		dctSaveList.Add "vchAddress2", Request("ship_strAddress2")
		dctSaveList.Add "vchCity", Request("ship_strCity")
		dctSaveList.Add "vchState", Request("ship_strState")
		dctSaveList.Add "vchZip", Request("ship_strZip")
		dctSaveList.Add "vchCountry", Request("ship_strCountry")
		dctSaveList.Add "vchDayPhone", Request("ship_strDayPhone")
		dctSaveList.Add "vchNightPhone", Request("ship_strNightPhone")
		dctSaveList.Add "vchFax", Request("ship_strFax")
		dctSaveList.Add "#intTaxZone", intTaxZone
		intNewShipID = SaveDataRecord(STR_TABLE_SHOPPER, Request, intShipID, dctSaveList)
	else
		' use billing info
		intNewShipID = intNewBillID
	end if
	
	' order info
	dctSaveList.RemoveAll
	dctSaveList.Add "@dtmUpdated", "GETDATE()"
	dctSaveList.Add "vchUpdatedByUser", "pub"
	dctSaveList.Add "vchUpdatedByIP", gstrUserIP
	dctSaveList.Add "#intBillShopperID", intNewBillID
	dctSaveList.Add "#intShipShopperID", intNewShipID
	dctSaveList.Add "#intTaxZone", intTaxZone
	dctSaveList.Add "#intShipOption", Request("intShipOption")
	dctSaveList.Add "chrPaymentMethod", Request("chrPaymentMethod")
	dctSaveList.Add "vchPaymentCardType", Request("vchPaymentCardType")
	dctSaveList.Add "vchPaymentCardNumber", Request("vchPaymentCardNumber")
	dctSaveList.Add "chrPaymentCardExpMonth", Request("chrPaymentCardExpMonth")
	dctSaveList.Add "chrPaymentCardExpYear", Request("chrPaymentCardExpYear")
	dctSaveList.Add "vchPaymentCardName", Request("bill_strFirstName") & " " & Request("bill_strLastName")
	dctSaveList.Add "chkRecurringOrder", Request("chkRecurringOrder")
	dctSaveList.Add "chkMonthlyOrderInfo", Request("chkMonthlyOrderInfo")
	
	dctSaveList.Add "txtGiftMessage", Request("txtGiftMessage")
	SaveDataRecord STR_TABLE_ORDER, Request, gintOrderID, dctSaveList
	
	RecalcOrder
end sub

' ========== Status Bar ==========
sub DrawCheckoutStatusBar(intThisStep)
	dim aryStepNames
	dim i, cOnStep, cOffStep, max
	cOnStep = cDkRed
	cOffStep = cVDkBlue
	aryStepNames = array("","Order Form","Shipping Options","Review","Submit Order")
	max = 4
%>
<BR>
<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="0" WIDTH="100%">
<TR>
<%
	for i = 1 to max
		if intThisStep = i then
			%>
			<TD BGCOLOR="<%= cOnStep %>"><%= spacer(1,1) %></TD>
			<%
		else
			%>
			<TD></TD>
			<%
		end if
	next
%>
</TR>
<TR BGCOLOR="<%= cOffStep %>">
<%
	dim strLinkTag1, strLinkTag2
	for i = 1 to max
		if i < intMaxStep and i < 6 then
			strLinkTag1 = "<A HREF=""checkout.asp?action=step" & i & "&rr=1"">"
			strLinkTag2 = "</A>"
		else
			strLinkTag1 = ""
			strLinkTag2 = ""
		end if
		if intThisStep = i then
			%>
			<TD ALIGN=CENTER WIDTH="14%" BGCOLOR="<%= cOnStep %>"><%= strLinkTag1 %><%= fontx(1,1,cWhite) %>Step <%= i %><BR><B><%= aryStepNames(i) %></B></FONT><%= strLinkTag2 %></TD>
			<%
		else
			%>
			<TD ALIGN=CENTER WIDTH="14%"><%= strLinkTag1 %><%= fontx(1,1,cWhite) %>Step <%= i %><BR><B><%= aryStepNames(i) %></B></FONT><%= strLinkTag2 %></TD>
			<%
		end if
	next
%>
</TR>
<TR>
<%
	for i = 1 to max
		if intThisStep = i then
			%>
			<TD BGCOLOR="<%= cOnStep %>"><%= spacer(1,1) %></TD>
			<%
		else
			%>
			<TD></TD>
			<%
		end if
	next
%>
</TR>
</TABLE>
<%
end sub

' ---------------------------------------
' ========== STEP 1 OR 2 (NEW) ==========
' ---------------------------------------

sub DrawPage_Step1()
	dim strFormMode
	
	dim dctCardExpMonthValues, dctCardExpYearValues, c
	set dctCardExpMonthValues = Server.CreateObject("Scripting.Dictionary")
	for c = 1 to 9
		dctCardExpMonthValues.Add "0" & c, "0" & c
	next
	dctCardExpMonthValues.Add "10", "10"
	dctCardExpMonthValues.Add "11", "11"
	dctCardExpMonthValues.Add "12", "12"
	set dctCardExpYearValues = Server.CreateObject("Scripting.Dictionary")
	for c = Year(Date()) to Year(Date()) + 10
		dctCardExpYearValues.Add c & "", c & ""
	next
%>
<div style="padding: 8px;">
<FORM ACTION="checkout.asp" METHOD="POST" NAME="frmOrder">
<%
	HiddenInput "action", "step1submit"
	'HiddenInput "iform", Request("iform") & ""
	'HiddenInput "rr", Request("rr") & ""
	'HiddenInput "id", intID
	
	'dim strLabel, strEmail, strFirstName, strLastName, strCompany, strAddress1, strAddress2, strCity, strState, strZip, strCountry, strDayPhone, strNightPhone, strFax, intTaxZone
	'response.write "<BR>intID: " & intID
	'response.write "<BR>intstep: " & intStep
	
	dim intBillID, intShipID
	dim strBillEmail, strBillFirstName, strShipFirstName, strBillLastName, strShipLastName
	dim strBillCompany, strShipCompany, strBillAddress1, strShipAddress1, strBillAddress2, strShipAddress2
	dim strBillCity, strShipCity, strBillState, strShipState, strBillZip, strShipZip, strBillCountry, strShipCountry
	dim strBillDayPhone, strShipDayPhone, strBillNightPhone, strShipNightPhone, strBillFax, strShipFax
	dim mnySubtotal, intShipOption, chkUseSame
	dim strPaymentMethod, strCardType, strCardNumber, strCardExpMonth, strCardExpYear
	dim strGiftMessage
	
	dim strSQL, rsData, rsInput
	if FormErrors.count <> 0 then
		strSQL = "SELECT"
		strSQL = strSQL & " ISNULL(O.mnyTaxSubtotal,0) + ISNULL(O.mnyNonTaxSubtotal,0) - ISNULL(O.mnyShipAmount,0) AS mnySubtotal"
		strSQL = strSQL & " FROM " & STR_TABLE_ORDER & " AS O"
		strSQL = strSQL & " WHERE O.intID = " & gintOrderID & " AND O.chrStatus <> 'D'"
		set rsData = ConnOpenRS(strSQL)
		set rsInput = Request
		chkUseSame = Request("chkUseSame")
		intBillID = Request("bill_abentry")
		if IsNumeric(intBillID) then
			intBillID = CLng(intBillID)
		else
			intBillID = 0
		end if
		intShipID = Request("ship_abentry")
		if IsNumeric(intShipID) then
			intShipID = CLng(intShipID)
		else
			intShipID = 0
		end if
	else
		strSQL = "SELECT B.intID AS bill_abentry, S.intID AS ship_abentry,"
		strSQL = strSQL & " B.vchEmail AS bill_strEmail, B.vchFirstName AS bill_strFirstName, B.vchLastName AS bill_strLastName,"
		strSQL = strSQL & " B.vchCompany AS bill_strCompany, B.vchAddress1 AS bill_strAddress1, B.vchAddress2 AS bill_strAddress2,"
		strSQL = strSQL & " B.vchCity AS bill_strCity, B.vchState AS bill_strState, B.vchZip AS bill_strZip, B.vchCountry AS bill_strCountry,"
		strSQL = strSQL & " B.vchDayPhone AS bill_strDayPhone, B.vchNightPhone AS bill_strNightPhone, B.vchFax AS bill_strFax,"
		strSQL = strSQL & " S.vchEmail AS ship_strEmail, S.vchFirstName AS ship_strFirstName, S.vchLastName AS ship_strLastName,"
		strSQL = strSQL & " S.vchCompany AS ship_strCompany, S.vchAddress1 AS ship_strAddress1, S.vchAddress2 AS ship_strAddress2,"
		strSQL = strSQL & " S.vchCity AS ship_strCity, S.vchState AS ship_strState, S.vchZip AS ship_strZip, S.vchCountry AS ship_strCountry,"
		strSQL = strSQL & " S.vchDayPhone AS ship_strDayPhone, S.vchNightPhone AS ship_strNightPhone, S.vchFax AS ship_strFax,"
		strSQL = strSQL & " ISNULL(O.mnyTaxSubtotal,0) + ISNULL(O.mnyNonTaxSubtotal,0) - ISNULL(O.mnyShipAmount,0) AS mnySubtotal,"
		strSQL = strSQL & " O.intShipOption,"
		strSQL = strSQL & " O.chrPaymentMethod, O.vchPaymentCardType, O.vchPaymentCardNumber, O.chrPaymentCardExpMonth, O.chrPaymentCardExpYear"
		strSQL = strSQL & ", O.txtGiftMessage"
		strSQL = strSQL & " FROM " & STR_TABLE_ORDER & " AS O"
		strSQL = strSQL & " LEFT JOIN " & STR_TABLE_SHOPPER & " AS B"
		strSQL = strSQL & " ON O.intBillShopperID = B.intID AND B.chrStatus <> 'D'"
		strSQL = strSQL & " LEFT JOIN " & STR_TABLE_SHOPPER & " AS S"
		strSQL = strSQL & " ON O.intShipShopperID = S.intID AND S.chrStatus <> 'D'"
		strSQL = strSQL & " WHERE O.intID = " & gintOrderID & " AND O.chrStatus <> 'D'"
		set rsData = ConnOpenRS(strSQL)
		set rsInput = rsData
		intBillID = rsInput("bill_abentry")
		if not IsNumeric(intBillID) then
			intBillID = 0
		end if
		intShipID = rsInput("ship_abentry")
		if not IsNumeric(intShipID) then
			intShipID = 0
		end if
		chkUseSame = iif((intBillID = intShipID) and (intBillID <> 0), "Y", "")
	end if
	strBillEmail = rsInput("bill_strEmail") & ""
	strBillFirstName = rsInput("bill_strFirstName") & ""		: strShipFirstName = rsInput("ship_strFirstName") & ""
	strBillLastName = rsInput("bill_strLastName") & ""			: strShipLastName = rsInput("ship_strLastName") & ""
	strBillCompany = rsInput("bill_strCompany") & ""			: strShipCompany = rsInput("ship_strCompany") & ""
	strBillAddress1 = rsInput("bill_strAddress1") & ""			: strShipAddress1 = rsInput("ship_strAddress1") & ""
	strBillAddress2 = rsInput("bill_strAddress2") & ""			: strShipAddress2 = rsInput("ship_strAddress2") & ""
	strBillCity = rsInput("bill_strCity") & ""					: strShipCity = rsInput("ship_strCity") & ""
	strBillState = rsInput("bill_strState") & ""				: strShipState = rsInput("ship_strState") & ""
	strBillZip = rsInput("bill_strZip") & ""					: strShipZip = rsInput("ship_strZip") & ""
	strBillCountry = rsInput("bill_strCountry") & ""			: strShipCountry = rsInput("ship_strCountry") & ""
	strBillDayPhone = rsInput("bill_strDayPhone") & ""			: strShipDayPhone = rsInput("ship_strDayPhone") & ""
	strBillNightPhone = rsInput("bill_strNightPhone") & ""		: strShipNightPhone = rsInput("ship_strNightPhone") & ""
	strBillFax = rsInput("bill_strFax") & ""					: strShipFax = rsInput("ship_strFax") & ""
	mnySubtotal = rsData("mnySubTotal")
	if IsNull(mnySubtotal) then
		mnySubtotal = 0
	end if
	intShipOption = rsInput("intShipOption") & ""
	strPaymentMethod = rsInput("chrPaymentMethod") & ""
	strCardType = rsInput("vchPaymentCardType") & ""
	strCardNumber = rsInput("vchPaymentCardNumber") & ""
	strCardExpMonth = rsInput("chrPaymentCardExpMonth") & ""
	strCardExpYear = rsInput("chrPaymentCardExpYear") & ""
	strGiftMessage = rsInput("txtGiftMessage") & ""
	set rsInput = nothing
	rsData.close
	set rsData = nothing
	
	'response.write "i=" & Request("iform") & ";b=" & strBillCountry & ";s=" & strShipCountry & "<br>" & vbcrlf
	strFormMode = ""
	if Request("iform") <> "" then
		strFormMode = Request("iform")
	else
		if strBillCountry <> "" or strShipCountry <> "" then
			'response.write ucase(strShipCountry) & ";" & ucase(strBillCountry)
			'response.write ";" & (ucase(strShipCountry)="US") & ";" & (ucase(strBillCountry)="US") & ";" & ((ucase(strShipCountry)="US") and (ucase(strBillCountry)="US")) & "<br>" & vbcrlf
			strFormMode = iif((ucase(strShipCountry)="US") and (ucase(strBillCountry)="US"), "n", "y")
		else
			strFormMode = "n"
		end if
	end if
	'response.write "i=" & strFormMode & ";b=" & strBillCountry & ";s=" & strShipCountry & "<br>" & vbcrlf
	if strFormMode = "y" then
		if strBillCountry = "US" and strShipCountry = "US" then
			strBillCountry = ""
			strShipCountry = ""
		end if
	else
		strBillCountry = "US"
		strShipCountry = "US"
	end if
	
	'if intID = 0 then
	'	' drawing a new form -- use values from acount or billing record
	'	dim strDummy		
	'	if intStep = 1 then		
	'		GetUserInfo strDummy, strFirstName, strLastName, strCompany, strEmail, strAddress1, strCity, strState, strZip
	'		strAddress2 = ""
	'		strCountry = "US"
	'		strDayPhone = ""
	'		strNightPhone = ""
	'		strFax = ""
	'		strFormMode = ""
	'	else					
	'		GetOrderAddress true, GetOrderBillID(false), strLabel, strEmail, strFirstName, strLastName, strCompany, strAddress1, strAddress2, strCity, strState, strZip, strCountry, strDayPhone, strNightPhone, strFax, intTaxZone
	'		if Request("iform") <> "" then
	'			strFormMode = Request("iform")
	'		else
	'			strFormMode = iif(ucase(strCountry)="US", "n", "y")
	'		end if			
	'	end if		
	'elseif FormErrors.count > 0 then
	'	' either redrawing a form with errors -- use values from form data
	'	strLabel = Request("strLabel")
	'	strEmail = Request("strEmail")
	'	strFirstName = Request("strFirstName")
	'	strLastName = Request("strLastName")
	'	strCompany = Request("strCompany")
	'	strAddress1 = Request("strAddress1")
	'	strAddress2 = Request("strAddress2")
	'	strCity = Request("strCity")
	'	strState = Request("strState")
	'	strZip = Request("strZip")
	'	strCountry = Request("strCountry")
	'	strDayPhone = Request("strDayPhone")
	'	strNightPhone = Request("strNightPhone")
	'	strFax = Request("strFax")
	'	strFormMode = lcase(Request("iform"))
	'	intTaxZone = Request("intTaxZone")
	'else		
	'	' load values from record
	'	GetOrderAddress (intStep=2), intID, strLabel, strEmail, strFirstName, strLastName, strCompany, strAddress1, strAddress2, strCity, strState, strZip, strCountry, strDayPhone, strNightPhone, strFax, intTaxZone
	'	' to determine the appropriate form to use (domestic vs. international), check the country field ONLY IF
	'	' Request("iform") doesn't specify otherwise (if the user is editing an existing record, but clicks on the link
	'	' to switch forms, the country field in the data will still have the old value while the "iform" specifies the correct
	'	' form to use)
	'	strFormMode = Request("iform")
	'	if strFormMode = "" then
	'		' select form based on country field
	'		strFormMode = iif(ucase(strCountry)="US", "n", "y")
	'	end if
	'end if
	
	'DrawFormHeader "100%", iif(intID=0, "New ", "Edit ") & strPageLabel & " Information", ""
	
	'if Request("iform") <> "" then
	'	strFormMode = Request("iform")
	'end if
	'strFormMode = ""
	
	stlBeginStdTable "100%"
	response.write "<TR><TD>" & font(1)

%>
<style type="text/css">
<!--
	.section td.colheader { font: 11px Arial, Verdana, Geneva, Sans Serif; font-weight: bold; }
	.section td.label { font: 11px Arial, Verdana, Geneva, Sans Serif; padding-right: 10px; }
	.section td.labelreq { font: 11px Arial, Verdana, Geneva, Sans Serif; font-weight: bold; padding-right: 10px; }
	.section td.labelerr { font: 11px Arial, Verdana, Geneva, Sans Serif; padding-right: 10px; }
	.section td.labelreqerr { font: 11px Arial, Verdana, Geneva, Sans Serif; font-weight: bold; padding-right: 10px; }
	.section td.field { font: 11px Arial, Verdana, Geneva, Sans Serif; padding-right: 10px; }
	.section td.fieldreq { font: 11px Arial, Verdana, Geneva, Sans Serif; padding-right: 10px; }
	.section td.fielderr { font: 11px Arial, Verdana, Geneva, Sans Serif; background: #ff0000; padding-right: 10px; }
	
	.section { margin-bottom: 15px; font: 11px Arial, Verdana, Geneva, Sans Serif; }
	.section .header { font: 14px Arial, Verdana, Geneva, Sans Serif; font-weight: bold; margin-bottom: 10px; }
	.section .subheader { text-align: center; font: 14px Arial, Verdana, Geneva, Sans Serif; font-weight: bold; margin-bottom: 4px; }
	.section td { font: 11px Arial, Verdana, Geneva, Sans Serif; }
	
	.errors { font: 14px Arial, Verdana, Geneva, Sans Serif; color: #660000; border-top: 2px solid #ff0000; border-bottom: 2px solid #ff0000; padding: 4px; text-align: center; font-weight: bold;}
	.errors td { font: 11px Arial, Verdana, Geneva, Sans Serif; font-weight: bold; color: #ff0000; text-align: center; }
-->
</style>
<%

	' domestic address form
	'HiddenInput "strCountry", "US"
	
	if FormErrors.count = 0 and Request("action") <> "" and Request("action") <> strAction then
		for each e in dctErrors
			FormErrors.Add e & "", dctErrors(e) & ""
		next
	end if
	
	'stlBeginFormSection "100%", 2
	'This is not needed for dummy users
	if FormErrors.count > 0 then
		dim intHalfwayPoint
		intHalfwayPoint = FormErrors.count / 2 + 1
		if intHalfwayPoint <> Fix(intHalfwayPoint) then
			' round up
			intHalfwayPoint = Fix(intHalfwayPoint) + 1
		end if
%>

		<div class="errors">
		Please correct the following errors:<br>
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td valign=""top"">
<%
		dim e
		for each e in FormErrors
			intHalfwayPoint = intHalfwayPoint - 1
			if intHalfwayPoint = 0 then
				response.write "</td><td>&nbsp;&nbsp;&nbsp;</td><td valign=""top"">" & vbcrlf
			end if
			response.write "<b>" & FormErrors(e) & "</b><br>" & vbcrlf
		next
%>
			</td>
		</tr>
		</table>
		</div>
		<br>
<%
	end if
%>

<div class="section">
<div class="header">Address Information</div>

<%
	if strFormMode = "y" then
%>
<div class="subheader">International Order Form</div>
<div style="font-size: 12px;">
If you are in the US and want to send a gift to Europe or if you are overseas and want to send a gift to someone in the US, use this form. Otherwise, please use our <b><a href="checkout.asp?iform=n">US DOMESTIC ORDER FORM</a></b>.
</div>
<%
	else
%>
<div class="subheader">Domestic Order Form</div>
<div style="font-size: 12px;">
If you are located outside the United States, please switch to the <b><a href="checkout.asp?iform=y">INTERNATIONAL ORDER FORM</a></b>.<br>
</div>
<%
	end if
%>
<input type="checkbox" ID="chkUseSame" NAME="chkUseSame" VALUE="Y" onClick="UpdateUseSame();" <% if chkUseSame <> "" then %>CHECKED<% end if %>> Please check here if your billing and shipping addresses are the same.<br>

<script language="javascript" type="text/javascript">
<!--
function SymError() 
{ 
return true; 
} 

window.onerror = SymError; 


function abookentry(p, v)
{
	var a = v.split('~');
	eval("document.frmOrder." + p + "abentry.value = a[0];");
	if (a[0] != '0')
	{
		if (p != 'ship_')
		{
			eval("document.frmOrder." + p + "strEmail.value = a[1];");
		}
		eval("document.frmOrder." + p + "strFirstName.value = a[2];");
		eval("document.frmOrder." + p + "strLastName.value = a[3];");
		eval("document.frmOrder." + p + "strCompany.value = a[4];");
		eval("document.frmOrder." + p + "strAddress1.value = a[5];");
		eval("document.frmOrder." + p + "strAddress2.value = a[6];");
		eval("document.frmOrder." + p + "strCity.value = a[7];");
		eval("document.frmOrder." + p + "strState.value = a[8];");
		eval("document.frmOrder." + p + "strZip.value = a[9];");
		eval("document.frmOrder." + p + "strDayPhone.value = a[10];");
		eval("document.frmOrder." + p + "strNightPhone.value = a[11];");
		eval("document.frmOrder." + p + "strFax.value = a[12];");
<%
	if strFormMode = "y" then
%>
		eval("document.frmOrder." + p + "strCountry.value = a[13];");
<%
	end if
%>
	}
}
// -->
</script>

<table border="0" cellpadding="2" cellspacing="0">
<tr>
	<td></td>
	<td class="colheader">Bill To:</td>
	<td class="colheader">Ship To:</td>
</tr>

<input type="hidden" name="bill_abentry" value="<%= intBillID %>" ID="Hidden1"/>
<input type="hidden" name="ship_abentry" value="<%= intShipID %>" ID="Hidden2"/>

<%
	dim rsTemp
	if gintUserID <> -1 then
%>
<tr>
	<td>Address Book:</td>
	<td>
		<select name="bill_abook" onChange="abookentry('bill_', this.value);">
			<option value="0">New...</option>
<%
		strSQL = "SELECT S.intID, S.vchEmail, S.vchFirstName, S.vchLastName, S.vchCompany, S.vchAddress1, S.vchAddress2, S.vchCity, S.vchState,"
		strSQL = strSQL & " S.vchZip, S.vchDayPhone, S.vchNightPhone, S.vchFax, S.vchCountry"
		strSQL = strSQL & " FROM " & STR_TABLE_SHOPPER & " AS S"
		strSQL = strSQL & " INNER JOIN " & STR_TABLE_SHOPPER & " AS A"
		strSQL = strSQL & " ON S.intShopperID = A.intID AND S.chrType IN ('B','S') AND S.chrStatus = 'A'"
		strSQL = strSQL & " WHERE A.intID = " & gintUserID
		if strFormMode <> "y" then
			strSQL = strSQL & " AND S.vchCountry = 'US'"
		end if
		strSQL = strSQL & " ORDER BY S.vchFirstName, S.vchLastName, S.vchCity, S.intID"
		set rsTemp = ConnOpenRS(strSQL)
		while not rsTemp.eof
			response.write "<option value="""
			for c = 0 to 13
				if c > 0 then
					response.write "~"
				end if
				response.write replace(rsTemp(c) & "", "~", " ")
			next
			response.write """"
			if rsTemp(0) & "" = intBillID & "" then
				response.write " selected"
			end if
			response.write ">" & Server.HTMLEncode(rsTemp("vchFirstName") & " " & rsTemp("vchLastName") & ", " & rsTemp("vchCity")) & "</option>" & vbcrlf
			rsTemp.MoveNext
		wend
		rsTemp.close
		set rsTemp = nothing
%>
		</select>
	</td>
	<td>
		<select name="ship_abook" onChange="abookentry('ship_', this.value);">
			<option value="0">New...</option>
<%
		strSQL = "SELECT S.intID, S.vchEmail, S.vchFirstName, S.vchLastName, S.vchCompany, S.vchAddress1, S.vchAddress2, S.vchCity, S.vchState,"
		strSQL = strSQL & " S.vchZip, S.vchDayPhone, S.vchNightPhone, S.vchFax, S.vchCountry"
		strSQL = strSQL & " FROM " & STR_TABLE_SHOPPER & " AS S"
		strSQL = strSQL & " INNER JOIN " & STR_TABLE_SHOPPER & " AS A"
		strSQL = strSQL & " ON S.intShopperID = A.intID AND S.chrType IN ('B','S') AND S.chrStatus = 'A'"
		strSQL = strSQL & " WHERE A.intID = " & gintUserID
		if strFormMode <> "y" then
			strSQL = strSQL & " AND S.vchCountry = 'US'"
		end if
		strSQL = strSQL & " ORDER BY S.vchFirstName, S.vchLastName, S.vchCity, S.intID"
		set rsTemp = ConnOpenRS(strSQL)
		while not rsTemp.eof
			response.write "<option value="""
			for c = 0 to 13
				if c > 0 then
					response.write "~"
				end if
				response.write replace(rsTemp(c) & "", "~", " ")
			next
			response.write """"
			if rsTemp(0) & "" = intShipID & "" then
				response.write " selected"
			end if
			response.write ">" & Server.HTMLEncode(rsTemp("vchFirstName") & " " & rsTemp("vchLastName") & ", " & rsTemp("vchCity")) & "</option>" & vbcrlf
			rsTemp.MoveNext
		wend
		rsTemp.close
		set rsTemp = nothing
%>
		</select>
	</td>
</tr>
<%
	end if
	
	if strFormMode = "n" then
%>
<input type="hidden" name="bill_strCountry" value="US">
<input type="hidden" name="ship_strCountry" value="US">
<%
	end if
	stlSpecialTextInput "strEmail", 20, 50, strBillEmail, "", "Email Address*", "IsEmpty", "Missing billing email address.", "-", 1,14
	stlSpecialTextInput "strFirstName", 20, 32, strBillFirstName, strShipFirstName, "First Name*", "IsEmpty", "Missing billing first name.", "Missing shipping first name.", 2, 15
	stlSpecialTextInput "strLastName", 20, 32, strBillLastName, strShipLastName, "Last Name*", "IsEmpty", "Missing billing last name.", "Missing shipping last name.", 3, 16
	if session("g_StoreFrontStr") = "sap" then
		stlSpecialTextInput "strCompany", 20, 50, strBillCompany, strShipCompany, "Church", "", "", "", 4, 17
	else
		stlSpecialTextInput "strCompany", 20, 50, strBillCompany, strShipCompany, "Company", "", "", "", 4, 17
	end if
	stlSpecialTextInput "strAddress1", 20, 50, strBillAddress1, strShipAddress1, "Address Line 1*", "IsEmpty", "Missing billing address.", "Missing shipping address.", 5, 18
	stlSpecialTextInput "strAddress2", 20, 50, strBillAddress2, strShipAddress2, "Address Line 2", "", "", "", 6, 19
	if strFormMode = "n" then
		stlSpecialTextInput "strCity", 20, 32, strBillCity, strShipCity, "City*", "IsEmpty", "Missing billing city.", "Missing shipping city.", 7,20
		stlSpecialArrayPulldown "strState", "Select...", strBillState, strShipState, dctUSStateCodes, "State*", "IsEmpty", "Missing billing state.", "Missing shipping state.", 8,21
		stlSpecialTextInput "strZip", 12, 12, strBillZip, strShipZip, "Zip*", "IsEmpty", "Missing billing zip-code.", "Missing shipping zip-code.", 9,22
	else
		stlSpecialTextInput "strCity", 20, 32, strBillCity, strShipCity, "City/Province*", "IsEmpty", "Missing billing city/province.", "Missing shipping city/province.", 7,20
		stlSpecialTextInput "strZip", 12, 12, strBillZip, strShipZip, "Postal Code*", "IsEmpty", "Missing billing postal code.", "Missing shipping postal code.", 8,21
		stlSpecialTextInput "strState", 20, 32, strBillState, strShipState, "State/Region*", "IsEmpty", "Missing billing state/region.", "Missing shipping state/region.", 9,22
		stlSpecialArrayPulldown "strCountry", "Select...", strBillCountry, strShipCountry, dctCountryCodes, "Country*", "IsEmpty", "Missing billing country.", "Missing shipping country.", 10,23
	end if
	stlSpecialTextInput "strDayPhone", 20, 20, strBillDayPhone, strShipDayPhone, "Daytime Phone*", "IsEmpty", "Missing billing daytime phone number.", "Missing shipping daytime phone number.", 11,24
	stlSpecialTextInput "strNightPhone", 20, 20, strBillNightPhone, strShipNightPhone, "Evening Phone", "", "", "", 12,25
	stlSpecialTextInput "strFax", 20, 20, strBillFax, strShipFax, "Fax Number", "", "", "", 13,26
%>
</table>

<%
	if NOT GetExpressCheckOut(gIntOrderID) then
%>
You need to create an online account by choosing a password. You can access your Online Subscriptions once payment is received. In addition, your account will keep an address book to make
future purchases faster, and allow you to track your orders online.
<table border="0" cellpadding="2" cellspacing="0">
<%
	stlTextInput "strPassword", 32, 32, Request, "Password", "IsEmpty", "Missing account password for online subscription."
%>
</table>
<%
	elseif gintUserID = -1 then
%>
If you would like to create an online account, please choose a password.  Your account will keep an address book to make
future purchases faster, and allow you to track your orders online.
<table border="0" cellpadding="2" cellspacing="0">
<%
	stlTextInput "strPassword", 32, 32, Request, "Password", "", ""
%>
</table>
<%
	end if
%>
</div>

<%
' Moved Shipping and Billing Options to Step 2 from here...
%>
<input type="checkbox" value="Y" name="chkRecurringOrder" id="chkRecurringOrder"> I want to sign up for the Monthly Smiles Club, making my order automatically generated every month. I understand that by checking this box I authorize The Campaign Store to generate an order and charge my credit card in the same amount every month. I understand I may cancel or change the frequency or amount of this order at any time by calling 916-441-6197 or emailing <a href='mailto:LisaBaron@moveamericaforward.org'>LisaBaron@moveamericaforward.org</a>.<br /><br />

<input type="checkbox" value="Y" name="chkMonthlyOrderInfo" id="chkMonthlyOrderInfo"> I would like more information about the Monthly Smiles Club and making my order a monthly, recurring order.<br /><br />
<div class="section">
<% 
	if true then 
	' *** BEGIN GIFT MESSAGE ***
%>
<div class="header">Comments</div>
Please include your brief message here:<br>
<br>
<textarea name="txtGiftMessage" rows="4" cols="50" wrap="virtual" class="textarea"><%= Server.HTMLEncode(strGiftMessage) %></textarea><br>
</div>
<% 
	' *** END GIFT MESSAGE ***
	end if 
%>

<script language="JavaScript" type="text/javascript">
<!--

document.frmOrder.bill_strEmail.focus();

function UpdateUseSame()
{
	if (document.frmOrder)
	{
		if (document.frmOrder.chkUseSame.checked)
		{
			document.frmOrder.ship_abook.disabled = true; document.frmOrder.ship_abook.value = '(same)';
			document.frmOrder.ship_strFirstName.disabled = true; document.frmOrder.ship_strFirstName.value = '(same)';
			document.frmOrder.ship_strLastName.disabled = true; document.frmOrder.ship_strLastName.value = '(same)';
			document.frmOrder.ship_strCompany.disabled = true; document.frmOrder.ship_strCompany.value = '(same)';
			document.frmOrder.ship_strAddress1.disabled = true; document.frmOrder.ship_strAddress1.value = '(same)';
			document.frmOrder.ship_strAddress2.disabled = true; document.frmOrder.ship_strAddress2.value = '(same)';
			document.frmOrder.ship_strCity.disabled = true; document.frmOrder.ship_strCity.value = '(same)';
			document.frmOrder.ship_strState.disabled = true; document.frmOrder.ship_strState.value = '(same)';
			document.frmOrder.ship_strZip.disabled = true; document.frmOrder.ship_strZip.value = '(same)';
<%
	if strFormMode = "y" then
%>
			document.frmOrder.ship_strCountry.disabled = true; document.frmOrder.ship_strCountry.value = '(same)';
<%
	end if
%>
			document.frmOrder.ship_strDayPhone.disabled = true; document.frmOrder.ship_strDayPhone.value = '(same)';
			document.frmOrder.ship_strNightPhone.disabled = true; document.frmOrder.ship_strNightPhone.value = '(same)';
			document.frmOrder.ship_strFax.disabled = true; document.frmOrder.ship_strFax.value = '(same)';
		}
		else
		{
			document.frmOrder.ship_abentry.value = '0';
			document.frmOrder.ship_abook.disabled = false; document.frmOrder.ship_abook.value = '0';
			document.frmOrder.ship_strFirstName.disabled = false; document.frmOrder.ship_strFirstName.value = '';
			document.frmOrder.ship_strLastName.disabled = false; document.frmOrder.ship_strLastName.value = '';
			document.frmOrder.ship_strCompany.disabled = false; document.frmOrder.ship_strCompany.value = '';
			document.frmOrder.ship_strAddress1.disabled = false; document.frmOrder.ship_strAddress1.value = '';
			document.frmOrder.ship_strAddress2.disabled = false; document.frmOrder.ship_strAddress2.value = '';
			document.frmOrder.ship_strCity.disabled = false; document.frmOrder.ship_strCity.value = '';
			document.frmOrder.ship_strState.disabled = false; document.frmOrder.ship_strState.value = '';
			document.frmOrder.ship_strZip.disabled = false; document.frmOrder.ship_strZip.value = '';
<%
	if strFormMode = "y" then
%>
			document.frmOrder.ship_strCountry.disabled = false; document.frmOrder.ship_strCountry.value = '';
<%
	end if
%>
			document.frmOrder.ship_strDayPhone.disabled = false; document.frmOrder.ship_strDayPhone.value = '';
			document.frmOrder.ship_strNightPhone.disabled = false; document.frmOrder.ship_strNightPhone.value = '';
			document.frmOrder.ship_strFax.disabled = false; document.frmOrder.ship_strFax.value = '';
		}
	}
}

<%
	if chkUseSame <> "" then
%>
UpdateUseSame();
<%
	end if
%>

// -->
</script>

<%
	response.write "</TD></TR>"
	stlSubmit "Continue to Step " & (intStep + 1) & " -->"
	stlEndStdTable
	response.write "</FORM>"
	response.write "</div>" 'closes <div style="padding: 5px;">
end sub

' ----------------------------
' ========== STEP 2 ==========
' ----------------------------

sub DrawPage_Step2 (mnySubtotal, strShipCountry, strShipState, intShipOption)
	dim strFormMode
	
	dim dctCardExpMonthValues, dctCardExpYearValues, x, c
	set dctCardExpMonthValues = Server.CreateObject("Scripting.Dictionary")
	for c = 1 to 9
		dctCardExpMonthValues.Add "0" & c, "0" & c
	next
	dctCardExpMonthValues.Add "10", "10"
	dctCardExpMonthValues.Add "11", "11"
	dctCardExpMonthValues.Add "12", "12"
	set dctCardExpYearValues = Server.CreateObject("Scripting.Dictionary")
	for c = Year(Date()) to Year(Date()) + 10
		dctCardExpYearValues.Add c & "", c & ""
	next

	dim strPaymentMethod, strCardType, strCardNumber, strCardExpMonth, strCardExpYear
	
	if FormErrors.count > 0 then
		strPaymentMethod = Request("chrPaymentMethod")
	else
		strPaymentMethod = GetOrderPaymentMethod()
	end if
%>
<div style="padding: 8px;">
<FORM ACTION="checkout.asp" METHOD="POST" NAME="frmOrder">
<%
	HiddenInput "action", "step2submit"
	HiddenInput "iform", Request("iform") & ""
	HiddenInput "rr", Request("rr") & ""
	'HiddenInput "id", intID
%>
<%
	stlBeginStdTable "100%"
	response.write "<TR><TD>" & font(1)

%>
<style type="text/css">
<!--
	.section td.colheader { font: 11px Arial, Verdana, Geneva, Sans Serif; font-weight: bold; }
	.section td.label { font: 11px Arial, Verdana, Geneva, Sans Serif; padding-right: 10px; }
	.section td.labelreq { font: 11px Arial, Verdana, Geneva, Sans Serif; font-weight: bold; padding-right: 10px; }
	.section td.labelerr { font: 11px Arial, Verdana, Geneva, Sans Serif; padding-right: 10px; }
	.section td.labelreqerr { font: 11px Arial, Verdana, Geneva, Sans Serif; font-weight: bold; padding-right: 10px; }
	.section td.field { font: 11px Arial, Verdana, Geneva, Sans Serif; padding-right: 10px; }
	.section td.fieldreq { font: 11px Arial, Verdana, Geneva, Sans Serif; padding-right: 10px; }
	.section td.fielderr { font: 11px Arial, Verdana, Geneva, Sans Serif; background: #ff0000; padding-right: 10px; }
	
	.section { margin-bottom: 15px; font: 11px Arial, Verdana, Geneva, Sans Serif; }
	.section .header { font: 14px Arial, Verdana, Geneva, Sans Serif; font-weight: bold; margin-bottom: 10px; }
	.section .subheader { text-align: center; font: 14px Arial, Verdana, Geneva, Sans Serif; font-weight: bold; margin-bottom: 4px; }
	.section td { font: 11px Arial, Verdana, Geneva, Sans Serif; }
	
	.errors { font: 14px Arial, Verdana, Geneva, Sans Serif; color: #660000; border-top: 2px solid #ff0000; border-bottom: 2px solid #ff0000; padding: 4px; text-align: center; font-weight: bold;}
	.errors td { font: 11px Arial, Verdana, Geneva, Sans Serif; font-weight: bold; color: #ff0000; text-align: center; }
-->
</style>
<%
	dim e
	if FormErrors.count = 0 and Request("action") <> "" and Request("action") <> strAction then
		for each e in dctErrors
			FormErrors.Add e & "", dctErrors(e) & ""
		next
	end if
	
	'stlBeginFormSection "100%", 2
	'This is not needed for dummy users
	if FormErrors.count > 0 then
		dim intHalfwayPoint
		intHalfwayPoint = FormErrors.count / 2 + 1
		if intHalfwayPoint <> Fix(intHalfwayPoint) then
			' round up
			intHalfwayPoint = Fix(intHalfwayPoint) + 1
		end if
%>

		<div class="errors">
		Please correct the following errors:<br>
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td valign=""top"">
<%
		for each e in FormErrors
			intHalfwayPoint = intHalfwayPoint - 1
			if intHalfwayPoint = 0 then
				response.write "</td><td>&nbsp;&nbsp;&nbsp;</td><td valign=""top"">" & vbcrlf
			end if
			response.write "<b>" & FormErrors(e) & "</b><br>" & vbcrlf
		next
%>
			</td>
		</tr>
		</table>
		</div>
		<br>
<%
	end if
%>
<%
	' **** BEGIN SHIPPING OPTIONS ****
	
	dim aryShipBaseMatrix, aryShipUPS_Matrix, aryShipOption, mnyBaseShipping
	GetShipPriceMatrix intShipOption, mnySubTotal, aryShipBaseMatrix, aryShipUPS_Matrix, aryShipOption, mnyBaseShipping
	
	if IsNumeric(intShipOption) then
		intShipOption = CLng(intShipOption)
	else
		intShipOption = 1
	end if
	dim maxCnt
	strFormMode = ""
	if Request("iform") <> "" then
		strFormMode = Request("iform")
	else
		if strShipCountry <> "" then
			strFormMode = iif((ucase(strShipCountry)="US"), "n", "y")
		else
			strFormMode = "n"
		end if
	end if

' make sure we have valid shipping options
if false then	' debug code
	maxCnt = ubound(aryShipOption)
	response.write "-------------------------------CHECKOUT----------------------------<br />"
	for c = 0 to maxCnt
		response.write aryShipOption(c,0) & " - " & aryShipOption(c,1) & ": " & aryShipOption(c,2)  & "<br />"
	next
	response.write "----------------------------------------------------------<br />"
	'response.end
end if 			' end debug code

'	 new method - test for international shipping
	maxCnt = ubound(aryShipOption) -3
	if strFormMode = "y" then
		for c = 1 to maxCnt
			aryShipOption(c,0) = -1
		next
	else
		aryShipOption(maxCnt+1,0) = -1
	end if

'	' verify which shipping options are available based on delivery address
'	if strShipCountry <> "US" and strShipCountry <> "CA" then
'		' international must choose option 5
'		aryShipOption(0,0) = -1
'		aryShipOption(1,0) = -1
'		aryShipOption(2,0) = -1
'		aryShipOption(3,0) = -1
'	elseif (strShipCountry = "US" and (strShipState = "HI" OR strShipState = "AK" OR strShipState = "VI")) then
'		' Hawaii, Alaska, Virgin Islands must choose option 4
'		aryShipOption(0,0) = -1
'		aryShipOption(2,0) = -1
'		aryShipOption(3,0) = -1
'		aryShipOption(4,0) = -1
'	else
'		' Non-international can not choose option 3
'		aryShipOption(4,0) = -1
'	end if

'	 old method for international shipping
'	aryShipOption(4,0) = -1
'	if strFormMode = "y" then
'	'	aryShipOption(0,0) = -1
'		aryShipOption(1,0) = -1
'		aryShipOption(2,0) = -1
'		aryShipOption(3,0) = -1
'	else
'		aryShipOption(4,0) = -1
'	end if
	
	x = ubound(aryShipOption)
	'maxCnt = ubound(aryShipOption)

	' verify which shipping options are available based on special product requirements
	dim rsLineItem, blnSpecialItems, blnOptionValid, intFirstOption, intForceShip
	blnSpecialItems = false
	intForceShip = -1
	set rsLineItem = GetOrderLineItems_Direct()
	while not rsLineItem.eof
		if not IsNull(rsLineItem("intForceShipMethod")) then
			blnSpecialItems = true
			intForceShip = rsLineItem("intForceShipMethod")
		else
			blnSpecialItems = false
			intForceShip = -1
		end if
		rsLineItem.MoveNext
	wend
	if blnSpecialItems = true AND intForceShip > -1 then
		for c = 0 to x
			' response.write aryShipOption(c,0) & " - " & aryShipOption(c,1) & ": " & aryShipOption(c,2)  & "<br />"
			if aryShipOption(c,0) <> intForceShip then
				aryShipOption(c,0) = -1
			end if
		next
	end if
	rsLineItem.close
	set rsLineItem = nothing
	
	' verify that there is an available option, and that the currently selected option is available
	blnOptionValid = false
	intFirstOption = -1
	for c = 0 to x
		if aryShipOption(c,0) <> -1 then
			if intFirstOption = -1 then
				intFirstOption = aryShipOption(c,0)
			end if
			if aryShipOption(c,0) = intShipOption then
				blnOptionValid = true
			end if
		end if
	next
	
	if blnOptionValid = false and intFirstOption <> -1 then
		intShipOption = intFirstOption
	end if
    	
	if intFirstOption = -1 then
		' there are no available shipping options
		
		response.write "<B>Problem with Your Order</B><BR><BR>"
		response.write "Unfortunately, the items you have ordered require special shipping methods that are not available based on your shipping address. Please contact our customer service staff to discuss arrangements, and refer to order " & STR_MERCHANT_TRACKING_PREFIX & gintOrderID & ".<BR><BR>"
		response.write STR_MERCHANT_NAME & "<BR>"
		response.write STR_MERCHANT_ADDRESS1 & "<BR>"
		if STR_MERCHANT_ADDRESS2 <> "" then
			response.write STR_MERCHANT_ADDRESS2 & "<BR>"
		end if
		response.write STR_MERCHANT_CITY & ", " & STR_MERCHANT_STATE & " " & STR_MERCHANT_ZIP & "<BR>"
		response.write STR_MERCHANT_PHONE & "<BR>"
		response.write "Fax: " & STR_MERCHANT_FAX & "<BR>"
		response.write "Email: <A HREF=""mailto:" & STR_MERCHANT_EMAIL & """>" & STR_MERCHANT_EMAIL & "</A><BR><BR>"
		response.write "&nbsp;"
	else
		if strFormMode = "y" then
            dim i
            i = intShipOption-1
            response.write "<div class=""section"">"
                response.write "<div class=""header"">Shipping Options</div>"
                response.write aryShipOption(i,1) & "<br />"
            response.write "</div>"
		    if not blnSpecialItems then i = 0
%>
<input type="hidden" name="intShipOption" value="<%= aryShipOption(i,0) %>">
<%
		else
%>
<div class="section">
<div class="header">Shipping Options</div>
<div class="header">Address Information</div>
<%
		'if blnSpecialItems then
		'	response.write "<B>Due to the nature of the items in your order, your order must be shipped by one of the methods shown.</B><BR><BR>"
		'end if
%>
Please select one of the following shipping options:<br><br>
<table border="0" cellpadding="2" cellspacing="0">
<%
	dim y
	if blnSpecialItems = false then
		y = maxCnt
	else
		y = x
	end if
			for c = 0 to y	'4
				if aryShipOption(c,0) <> -1 then
%>
<tr>
	<td valign="top"><input type="radio" name="intShipOption" value="<%= aryShipOption(c,0) %>" <% if intShipOption = aryShipOption(c,0) then %>CHECKED<% end if %>></td>
	<td valign="top"><%= aryShipOption(c,1) %></td>
	<td valign="top" align="right"><b><%= SafeFormatCurrency("", aryShipOption(c,2), 2) %></b></td>
</tr>
<%
				else
					' uncomments the next three lines to display unavailable shipping options as greyed out options
					stlRawCell ""
					stlRawCellX "<FONT COLOR=""#999999"">" & aryShipOption(c,1) & "</FONT>", "", "", "top", 1
					stlRawCellX "<FONT COLOR=""#999999"">" & FormatCurrency(aryShipOption(c,2),2) & "</FONT>", "", "right", "top", 1
				end if
			next
%>
</table>
</div>
<%
		end if
	end if
	
	' **** END SHIPPING OPTIONS ***
	
	' *** BEGIN PAYMENT OPTIONS ***

	strPaymentMethod = Request("chrPaymentMethod")
	
%>
<div class="section">
<div class="header">Payment Options</div>
Major Credit Cards Accepted: <%= join(dctCreditCardTypeValues.items, ", ") %><br>
<br>
Please indicate how you would like to pay for your purchase:<br>
<table border="0" cellpadding="2" cellspacing="0">
<tr>
	<td valign="top"><input type="radio" name="chrPaymentMethod" value="OCC" <% if strPaymentMethod <> "MEC" then %>CHECKED<% end if %>></td>
	<td valign="top">
	Using the following credit card:<br>
	Type of Card: <% SpecialArrayPulldown "vchPaymentCardType", "Select...", strCardType, dctCreditCardTypeValues, 0 %><br>
	Card Number: <% TextInput "vchPaymentCardNumber", 32, 32, strCardNumber %><br>
	Expiration Date: <% SpecialArrayPulldown "chrPaymentCardExpMonth", "", strCardExpMonth, dctCardExpMonthValues, 0 %>
	  <% SpecialArrayPulldown "chrPaymentCardExpYear", "", strCardExpYear, dctCardExpYearValues, 0 %>
	</td>
</tr>

<% if true = false then %>
<tr>
	<td valign="top"><input type="radio" name="chrPaymentMethod" value="MCC" <% if strPaymentMethod = "MCC" then %>CHECKED<% end if %>></td>
	<td valign="top">
	Mail-in Credit Card.
	</td>
</tr>
<tr>
	<td valign="top"><input type="radio" name="chrPaymentMethod" value="FCC" <% if strPaymentMethod = "FCC" then %>CHECKED<% end if %>></td>
	<td valign="top">
	I would prefer to provide my credit card information by fax.<br>
	Please fax your payment information to <%= STR_MERCHANT_NAME %> at <%= STR_MERCHANT_FAX %> after you have submitted your order.
	</td>
</tr>
<tr>
	<td valign="top"><input type="radio" name="chrPaymentMethod" value="FMO" <% if strPaymentMethod = "FMO" then %>CHECKED<% end if %>></td>
	<td valign="top">
	Fax-in Money Order.
	</td>
</tr>
<tr>
	<td valign="top"><input type="radio" name="chrPaymentMethod" value="FPO" <% if strPaymentMethod = "FPO" then %>CHECKED<% end if %>></td>
	<td valign="top">
	Fax-in Purchase Order.
	</td>
</tr>
<% end if %>
<tr>
	<td valign="top"><input type="radio" name="chrPaymentMethod" value="MEC" <% if strPaymentMethod = "MEC" then %>CHECKED<% end if %>></td>
	<td valign="top">
	Mail-in Check or Money Order.
	</td>
</tr>
<tr>
	<td valign="top">&nbsp;</td>
	<td valign="top">
		Please make the check or money order payable to "The Campaign Store<br />
		Please mail the check or money order to:<br /><br />
		The Campaign Store<br />
		8795 Folsom Blvd., Suite 103<br />
		Sacramento, CA 95826<br />

	</td>
</tr>
<% if true = false then %>
<tr>
	<td valign="top"><input type="radio" name="chrPaymentMethod" value="MMO" <% if strPaymentMethod = "MMO" then %>CHECKED<% end if %>></td>
	<td valign="top">
	Mail-in Money Order.
	</td>
</tr>
<tr>
	<td valign="top"><input type="radio" name="chrPaymentMethod" value="MPO" <% if strPaymentMethod = "MPO" then %>CHECKED<% end if %>></td>
	<td valign="top">
	Mail-in Purchase Order.
	</td>
</tr>
<% end if %>
<tr>
	<td></td>
	<td valign="top">
		<b>PLEASE NOTE:</b> For pay-by-mail orders, you will be reminded to send your payment information at the end of this checkout.
		Your order can NOT be processed until payment is received.
	</td>
</tr>
</table>
</div>
<%
	
	' *** END PAYMENT OPTIONS ***
	%>

<%
	response.write "</TD></TR>"
	stlSubmit "Continue to Step " & (intStep + 1) & " -->"
	stlEndStdTable
	response.write "</FORM>"
	response.write "</div>" 'closes <div style="padding: 5px;">
end sub

' ----------------------------
' ========== STEP 3 ==========
' ----------------------------

sub DrawPage_Step3
	
	Dim strPromoCode
	
	strPromoCode = Request("PromoCode")
	
	RecalcOrder_WithPromo(strPromoCode)
	
	dim rsOrder, rsLineItem, rsShopper
	set rsOrder = GetOrderRecord_Direct()

	'DRAWFORMHEADER "100%", "ORDER REVIEW", ""
%>
<div style="padding: 8px;">
<FORM ACTION="checkout.asp" METHOD="POST">
<%
	stlbeginstdtable "100%"
	response.write "<TR><TD>" & font(2)
%>

<div align="center">
	<A HREF="checkout.asp?action=step2"><IMG SRC="<%= g_imagebase %>menu/modify.gif" WIDTH="51" HEIGHT="16" ALT="Modify Order Form" BORDER="0" ALIGN=ABSMIDDLE VSPACE=3 HSPACE=4></A>
</div>

<%= fontx(2,2,cHeading) %><B>Payment Information</B></FONT><BR>
<%= GetArrayValue(rsOrder("chrPaymentMethod"), dctPaymentMethod) %>
<%
if right(rsOrder("chrPaymentMethod"), 2) = "CC" then
	if not IsNull(rsOrder("vchPaymentCardNumber")) then
		%>
		&nbsp;
		<%= rsOrder("vchPaymentCardType") %>
		&nbsp;
		<%= GetProtectedCardNumber(rsOrder("vchPaymentCardNumber")) %>
		(exp <%= rsOrder("chrPaymentCardExpMonth") %>/<%= rsOrder("chrPaymentCardExpyear") %>)
		<%
	end if
elseif right(rsOrder("chrPaymentMethod"), 2) = "EC" then
	response.write " " & rsOrder("vchPaymentBankName") & " " & rsOrder("vchPaymentRtnNumber") & "-" & GetProtectedCardNumber(rsOrder("vchPaymentAcctNumber")) & " " & rsOrder("vchPaymentCheckNumber")
end if
%>
<BR>

<%
set rsLineItem = GetOrderTrans_Direct()
if not rsLineItem.eof then
	%>
	<TABLE BORDER=1 CELLPADDING=2 CELLSPACING=2 WIDTH="100%">
	<%
end if
while not rsLineItem.eof
	if rsLineItem("chrStatus") = "A" then
		%>
		<TR>
			<TD><%= font(1) %><%= GetArrayValue(rsLineItem("chrTransType"), dctTransType) %></TD>
			<TD><%= font(1) %><%= SafeFormatCurrency("", rsLineItem("mnyTransAmount"), 2) %></TD>
			<TD><%= font(1) %><%= replace(rsLineItem("dtmTransDate"),":00 "," ") %></TD>
		<%
		if rsLineItem("chrStatus") = "A" then
			if not IsNull(rsLineItem("vchAuthCode")) then
				%>
				<TD><%= font(1) %>A<%= rsLineItem("vchAuthCode") %></TD>
				<%
			else
				%>
				<TD></TD>
				<%
			end if
			if not IsNull(rsLineItem("vchAVCode")) then
				%>
				<TD><%= font(1) %>AV&nbsp;<%= rsLineItem("vchAVCode") %></TD>
				<%
			else
				%>
				<TD></TD>
				<%
			end if
			if not IsNull(rsLineItem("vchRefCode")) then
				%>
				<TD><%= font(1) %>Ref&nbsp;<%= rsLineItem("vchRefCode") %></TD>
				<%
			else
				%>
				<TD></TD>
				<%
			end if
			if not IsNull(rsLineItem("vchDeclineCode")) then
				%>
				<TD><%= font(1) %>Decline&nbsp;<%= rsLineItem("vchDeclineCode") %></TD>
				<%
			else
				%>
				<TD></TD>
				<%
			end if
			if not IsNull(rsLineItem("vchOrderCode")) then
				%>
				<TD><%= font(1) %>Order&nbsp;<%= rsLineItem("vchOrderCode") %></TD>
				<%
			else
				%>
				<TD></TD>
				<%
			end if
		else
			%>
			<TD><%= font(1) %><B><%= GetArrayValue(rsLineItem("chrStatus"), dctTransStatusValues) %></B></TD>
			<%
		end if
	end if
	rsLineItem.MoveNext
	if rsLineItem.eof then
		%>
		
		</TABLE>
		<BR>
		<%
	end if
wend
rsLineItem.close
set rsLineItem = nothing
%>
<HR SIZE=1 COLOR="<%= cRule %>">
<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%">
<TR VALIGN=TOP>
	<TD WIDTH="50%"><%= font(2) %>
	<%= fontx(2,2,cHeading) %><B>Bill To</B></FONT><BR>
	<%
		set rsShopper = GetOrderShopper_Direct(false)
		if rsShopper.eof then
			response.write "<B>System error</B>--unable to retrieve shopper information." & rsOrder("intbillShopperID")
		else
			response.write rsShopper("vchLastName") & ", " & rsShopper("vchFirstName") & "<BR>"
			if not IsNull(rsShopper("vchCompany")) then
				response.write rsShopper("vchCompany") & "<BR>"
			end if
			if not IsNull(rsShopper("vchAddress1")) then
				response.write rsShopper("vchAddress1") & "<BR>"
			end if
			if not IsNull(rsShopper("vchAddress2")) then
				response.write rsShopper("vchAddress2") & "<BR>"
			end if
			response.write rsShopper("vchCity") & ", " & rsShopper("vchState") & " " & rsShopper("vchZip") & "<BR>"
			if not IsNull(rsShopper("vchCountry")) then
				response.write rsShopper("vchCountry") & "<BR>"
			end if
			if not IsNull(rsShopper("vchDayPhone")) then
				response.write "Day: " & rsShopper("vchDayPhone") & "<BR>"
			end if
			if not IsNull(rsShopper("vchNightPhone")) then
				response.write "Night: " & rsShopper("vchNightPhone") & "<BR>"
			end if
			if not IsNull(rsShopper("vchFax")) then
				response.write "Fax: " & rsShopper("vchFax") & "<BR>"
			end if
			if not IsNull(rsShopper("vchEmail")) then
				response.write "Email: " & rsShopper("vchEmail") & "<BR>"
			end if
		end if
		rsShopper.close
		set rsShopper = nothing
%>
	</TD>
	<TD><%= spacer(15,1) %></TD>
	<TD WIDTH="50%"><%= font(2) %>
		<%= fontx(2,2,cHeading) %><B>Ship To</B></FONT><BR>
<%
		set rsShopper = GetOrderShopper_Direct(true)
		if rsShopper.eof then
			response.write "Same as Billing"
		else
			response.write rsShopper("vchLastName") & ", " & rsShopper("vchFirstName") & "<BR>"
			if not IsNull(rsShopper("vchCompany")) then
				response.write rsShopper("vchCompany") & "<BR>"
			end if
			if not IsNull(rsShopper("vchAddress1")) then
				response.write rsShopper("vchAddress1") & "<BR>"
			end if
			if not IsNull(rsShopper("vchAddress2")) then
				response.write rsShopper("vchAddress2") & "<BR>"
			end if
			response.write rsShopper("vchCity") & ", " & rsShopper("vchState") & " " & rsShopper("vchZip") & "<BR>"
			if not IsNull(rsShopper("vchCountry")) then
				response.write rsShopper("vchCountry") & "<BR>"
			end if
			if not IsNull(rsShopper("vchDayPhone")) then
				response.write "Day: " & rsShopper("vchDayPhone") & "<BR>"
			end if
			if not IsNull(rsShopper("vchNightPhone")) then
				response.write "Night: " & rsShopper("vchNightPhone") & "<BR>"
			end if
			if not IsNull(rsShopper("vchFax")) then
				response.write "Fax: " & rsShopper("vchFax") & "<BR>"
			end if
			if not IsNull(rsShopper("vchEmail")) then
				response.write "Email: " & rsShopper("vchEmail") & "<BR>"
			end if
		end if
		rsShopper.close
		set rsShopper = nothing
%>
	</TD>
</TR>
</TABLE>
<% if true then ' - kill gift message%>

<HR SIZE=1 COLOR="<%= cRule %>">
<%= fontx(2,2,cHeading) %><B>Comments</B></FONT><BR>
<%
	dim txtGiftMessage
	txtGiftMessage = rsOrder("txtGiftMessage") & ""
	if txtGiftMessage = "none" then
%>
None - You may include a message at no additional charge.
<%
	else
		response.write replace(Server.HTMLEncode(txtGiftMessage), vbCR, "<BR>")
	end if
%>
<BR>
<% end if %>
<%
	set rsLineItem = GetOrderLineItems_WithPromoCode(strPromoCode)'GetOrderLineItems_Direct()
%>

<HR SIZE=1 COLOR="<%= cRule %>">
<%= fontx(2,2,cHeading) %><B>Order Details</B></FONT>
<A HREF="checkout.asp?action=step0"><IMG SRC="<%= g_imagebase %>menu/modify.gif" WIDTH="51" HEIGHT="16" ALT="Modify Your Order" BORDER="0" ALIGN=ABSMIDDLE VSPACE=3 HSPACE=4></A><BR>
<BR>

<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%">
<TR BGCOLOR="<%= cGoCart_CSHdrBG %>">
	<TD><%= fontx(1,1,cWhite) %>Item #</TD>
	<TD><%= fontx(1,1,cWhite) %>Product</TD>
	<TD ALIGN=CENTER><%= fontx(1,1,cWhite) %>Quantity</TD>
	<TD ALIGN=RIGHT><%= fontx(1,1,cWhite) %>Price</TD>
	<TD ALIGN=RIGHT><%= fontx(1,1,cWhite) %>Total</TD>
</TR>
<%
while not rsLineItem.eof
	%>
	<TR>
		<TD><%= font(2) %><%= rsLineItem("vchPartNumber") %>&nbsp;</TD>
		<TD><%= font(2) %><%= rsLineItem("vchItemName") %>&nbsp;</TD>
		<TD ALIGN=CENTER><%= font(2) %><%= iif(IsNull(rsLineItem("intQuantity")), "&nbsp;", rsLineItem("intQuantity")) %></TD>
		<TD ALIGN=RIGHT><%= font(2) %><%= SafeFormatCurrency("&nbsp;", rsLineItem("mnyUnitPrice"), 2) %></TD>
		<TD ALIGN=RIGHT><%= font(2) %><%= SafeFormatCurrency("&nbsp;", rsLineItem("mnyUnitPrice") *rsLineItem("intQuantity"), 2) %></TD>
	</TR>
	<TR>
		<TD COLSPAN=5><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cGreen %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
	</TR>
<%
		rsLineItem.MoveNext
	wend
	rsLineItem.close
	set rsLineItem = nothing

	' Shipping: xx
	' Nontaxable Subtotal: xx
	' Taxable Subtotal: xx
	' Tax: xx
	' Total: xx
	if not IsNull(rsOrder("mnyShipAmount")) then
%>
<TR>
	<TD NOWRAP COLSPAN="4" ALIGN=RIGHT><%= font(2) %><B>(<%= GetArrayValue(rsOrder("intShipOption"), dctShipOption) %>) Shipping:</B>&nbsp;</TD>
	<TD ALIGN=RIGHT><%= font(2) %><%= SafeFormatCurrency("$0.00", rsOrder("mnyShipAmount"), 2) %></TD>
</TR>
<%
	end if
	dim mnyTaxSubTotal, mnyNonTaxSubTotal
	if not IsNull(rsOrder("mnyNonTaxSubtotal")) then
		mnyNonTaxSubTotal = rsOrder("mnyNonTaxSubtotal")
	else
		mnyNonTaxSubTotal = 0
	end if
	if not IsNull(rsOrder("mnyTaxSubtotal")) then
		mnyTaxSubTotal = rsOrder("mnyTaxSubTotal")
	else
		mnyTaxSubTotal = 0
	end if
	if mnyTaxSubTotal = 0 or mnyNonTaxSubTotal = 0 then
		' only one subtotal--we'll list it as "Subtotal"
%>
<TR>
	<TD COLSPAN=4 ALIGN=RIGHT><%= font(2) %><B>Subtotal:</B>&nbsp;</TD>
	<TD ALIGN=RIGHT><%= font(2) %><%= SafeFormatCurrency("$0.00", mnyTaxSubTotal + mnyNonTaxSubTotal, 2) %></TD>
</TR>
<%
	else
%>
<TR>
	<TD COLSPAN=4 ALIGN=RIGHT><%= font(2) %><B>Taxable Subtotal:</B>&nbsp;</TD>
	<TD ALIGN=RIGHT><%= font(2) %><%= SafeFormatCurrency("n/a", mnyTaxSubtotal, 2) %></TD>
</TR>
<TR>
	<TD COLSPAN=4 ALIGN=RIGHT><%= font(2) %><B>Non-Taxable Subtotal:</B>&nbsp;</TD>
	<TD ALIGN=RIGHT><%= font(2) %><%= SafeFormatCurrency("n/a", mnyNonTaxSubtotal, 2) %></TD>
</TR>
<%
	end if
	if rsOrder("mnyTaxAmount") <> 0 and not IsNull(rsOrder("mnyTaxAmount")) then
%>
<TR>
	<TD COLSPAN=4 ALIGN=RIGHT><%= font(2) %><B>(<%= GetArrayValue(rsOrder("intTaxZone"), GetTaxZoneList()) %>) Tax @ <%= SafeFormatNumber("0", rsOrder("fltTaxRate") * 100, 2) %>%:</B>&nbsp;</TD>
	<TD ALIGN=RIGHT><%= font(2) %><%= SafeFormatCurrency("n/a", rsOrder("mnyTaxAmount"), 2) %></TD>
</TR>
<%
	end if
	if rsOrder("mnyDiscountAmount") <> 0 and not IsNull(rsOrder("mnyDiscountAmount")) then
%>
<TR>
	<TD COLSPAN=4 ALIGN=RIGHT><%= font(2) %><B><%= rsOrder("vchReferalName") & " " & rsOrder("fltReferalDiscount") & "% Discount:" %></B>&nbsp;</TD>
	<TD ALIGN=RIGHT><%= font(2) %>-<%= SafeFormatCurrency("n/a", rsOrder("mnyDiscountAmount"), 2) %></TD>
</TR>
<%
	end if
%>
<TR BGCOLOR="<%= cGoCart_CSTotalRowBG %>">
	<TD COLSPAN=4 ALIGN=RIGHT><%= fontx(2,2,cDkRed) %><B>Total:</B>&nbsp;</TD>
	<TD ALIGN=RIGHT><%= fontx(2,2,cDkRed) %><B><%= SafeFormatCurrency("$0.00", rsOrder("mnyGrandTotal"), 2) %></B></TD>
</TR>


</TABLE>
&nbsp;
<%
	rsOrder.close
	set rsOrder = nothing
	
	response.write "</TD></TR>"
%>
<% HiddenInput "action", "step3submit" %>
<%
	stlSubmit "Submit Your Order -->"
	stlEndStdTable
	response.write "</FORM>"
	response.write "</div>" 'closes <div style="padding: 8px;">
	
%>
<div align="right">If you have a Promo Code please enter it here:
		
			<form action="checkout.asp" method="get" ID="Form1">
				<input type="hidden" name="action" value="step3" />
				<input type="text" name="PromoCode" size="20" maxlength="18" />
				<input type="submit" value="Recalculate"  />
			</form>
</div>
<%
end sub

' -----------------------------------
' ========== PROCESS ORDER ==========
' -----------------------------------

sub ProcessOrder
	' process flow:
	'  (1) set order status to 'submitted'
	'  (2) if OCC or OEC, process transaction
	'  (3) display/email invoice (unless declined or rejected)
	
	' SWSCodeChange - 7/20 - added merchant email
	dim strMerchantMessage, strSubject
	strMerchantMessage = "An order has been submitted. Please review the order by visiting your merchant website." & vbcrlf
	strMerchantMessage = strMerchantMessage & vbcrlf
	strMerchantMessage = strMerchantMessage & "Order #" & STR_MERCHANT_TRACKING_PREFIX & gintOrderID & vbcrlf
	strMerchantMessage = strMerchantMessage & "Merchant Website: " & nonsecurebase & "admin/" & vbcrlf
	strMerchantMessage = strMerchantMessage & vbcrlf
	if lcase(strServerHost) = "awsdev" then
		AddStdEmailSecurity strMerchantMessage
	end if
	
	dim strMethod, strStatus, blnUseOEC
	strMethod = GetOrderPaymentMethod()
	blnUseOEC = (BLN_SUPPORT_OEC and (strMethod = "OCC" or strMethod = "OEC"))
	if blnUseOEC then
		' if shipping internationally, card auth must wait for admin approval
		if GetOrderShipOption_Other(gintOrderID) = 5 then
			blnUseOEC = false
		end if
	end if
	UpdateOrderStatus "S"
	SetOrderDateSubmitted Now()		' SWSCodeChange: added 7/19/00 to set 'date submitted' field
	strStatus = "S"

	if blnUseOEC then
		Response.buffer = true
		Server.ScriptTimeout = 60
		
		DrawProcessHeader
		strStatus = ProcessOrderTransaction()
		select case strStatus
			case "A": ' approved
				strStatus = "A"
			case "Z": '	Z = authorized
				strStatus = "Z"
			case "K": ' declined
				strStatus = "K"
			case "R": ' rejected (fraud check)
				strStatus = "K"
			case "V": ' voice-auth required
				strStatus = "V"
			case else:
				strStatus = "E"
		end select
	 else
		' don't send merchant email (only admin) when order is submitted
		strSubject = "Order # " & STR_MERCHANT_TRACKING_PREFIX & gintOrderID & " - Submitted"

		SendOrderEmail_Other gintOrderID, "Thank you for your order. If you have any questions, please contact our customer service staff at the address or phone number provided above. This communication may also serve as your �tax receipt� as proof of your donation."
		SendMerchantEmail strSubject, strMerchantMessage		' SWSCodeChange - 7/20 - added merchant email
		response.redirect "invoice.asp?order=" & gintOrderID
		response.end
	end if
	
	' possible status at this point:
	'   S = submitted, no processing done
	'	V = Voice Authorization Required
	'   A = approved
	'	Z = authorized
	'   E = system error
	'   K or R = declined
	strSubject = "Order # " & STR_MERCHANT_TRACKING_PREFIX & gintOrderID
	select case strStatus
		case "S":
			' don't send merchant email (only admin) when order is submitted
			SendOrderEmail_Other gintOrderID, "Thank-you for your order. If you have any questions, please contact our customer service staff at the address or phone number provided above. This communication may also serve as your �tax receipt� as proof of your donation."
			SendMerchantEmail strSubject & " - Submitted", strMerchantMessage		' SWSCodeChange - 7/20 - added merchant email
			DrawSubmitResult
		case "V":
			' don't send merchant email (only admin) when voice authorization is required
			SendOrderEmail_Other gintOrderID, "Thank-you for your order. If you have any questions, please contact our customer service staff at the address or phone number provided above. This communication may also serve as your �tax receipt� as proof of your donation."
			SendMerchantEmail strSubject & " - Voice Auth. Req.", strMerchantMessage		' SWSCodeChange - 7/20 - added merchant email
			DrawSubmitResult
		case "A":
			' send merchant and admin email when order is approved
			strSubject = "Order Notification # " & STR_MERCHANT_TRACKING_PREFIX & gintOrderID & ""
			SendOrderEmail_Other gintOrderID, "Thank-you for your order. If you have any questions, please contact our customer service staff at the address or phone number provided above. This communication may also serve as your �tax receipt� as proof of your donation."
			SendMerchantEmail strSubject, strMerchantMessage		' SWSCodeChange - 7/20 - added merchant email
			DrawApprovedResult
		case "Z":
			' don't send merchant email (only admin) when OCC or OEC authorizated
			SendOrderEmail_Other gintOrderID, "Thank-you for your order. If you have any questions, please contact our customer service staff at the address or phone number provided above. This communication may also serve as your �tax receipt� as proof of your donation."
			SendMerchantEmail strSubject & " - OCC or OEC Authorized", strMerchantMessage		' SWSCodeChange - 7/20 - added merchant email
			DrawApprovedResult
		case "K", "R":
			' don't send merchant or admin email if declined has occured
			'SendMerchantEmail "Order Notification - Declined", strMerchantMessage		' SWSCodeChange - 7/20 - added merchant email
			DrawDeclineResult
		case "E":
			' don't send merchant email (only admin) if error has occured
			SendMerchantEmail strSubject & " - System Error", strMerchantMessage		' SWSCodeChange - 7/20 - added merchant email
			DrawErrorResult
	end select
end sub

function ProcessOrderTransaction()
	' valid return codes:
	'   "V" = voice-auth required
	'   "A" = approved
	'	"Z" = authorized
	'   "K" = declined
	'   "R" = rejected
	'   "E" = system error
	dim mnyAmount, intTransID, intResult
	mnyAmount = GetOrderGrandTotal()
	
	' configure ASP and display header
	
	' prepare transaction
	'intTransID = Trans_Prepare(gintOrderID, "AV", mnyAmount)
	intTransID = Trans_Prepare(gintOrderID, "ES", mnyAmount)	'changed by dde, 10/27/2011
	' perform transaction
	intResult = Trans_Run(intTransID)
	' display result msg
	select case intResult
		case 0:	' transaction successful
			ProcessOrderTransaction = "A"
			'UpdateOrderStatus "Z"
			UpdateOrderStatus "H"	'shipped, updated by dde per Ed's email, 11/02/2011
			UpdateOrderShippedDate gintOrderID
		case 1:	' transaction failed
			ProcessOrderTransaction = "K"
			UpdateOrderStatus "5"
		case 2:	' transaction requires verbal auth
			ProcessOrderTransaction = "V"
			UpdateOrderStatus "V"
		case 3:	' system error
			ProcessOrderTransaction = "E"
			UpdateOrderStatus "E"
		case else:	' unknown result - system error
			ProcessOrderTransaction = "E"
			UpdateOrderStatus "E"
	end select
end function

' ------------------------------------
' ========== PROCESS HEADER ==========
' ------------------------------------

sub DrawProcessHeader
%>
<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="<%= INT_PAGE_WIDTH %>">
<TR>
	<TD><%= font(5) %>Order Submission</TD>
	<TD ALIGN=RIGHT><%= font(2) %><B>Tracking # <%= STR_MERCHANT_TRACKING_PREFIX & gintOrderID %></B></TD>
</TR>
</TABLE>
<%= spacer(1,5) %><BR>
<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="<%= INT_PAGE_WIDTH %>" BGCOLOR="<%= cPurple %>">
<TR>
	<TD><%= fontx(1,1, cWhite) %>&nbsp;</TD>
</TR>
</TABLE>
<%= spacer(1,5) %><BR>
<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="<%= INT_PAGE_WIDTH %>">
<TR>
	<TD><%= font(2) %>Please wait while your order is processed. This may take up to two minutes. If you have any difficulties or need assistance, please contact customer service at <%= STR_MERCHANT_CS_PHONE %> or email <A HREF="mailto:<%= STR_MERCHANT_CS_EMAIL %>"><%= STR_MERCHANT_CS_EMAIL %></A></TD>
</TR>
</TABLE>
<%= spacer(1,10) %><BR>
<%
	response.flush
end sub

' -----------------------------
' ========== RESULTS ==========
' -----------------------------

sub DrawSubmitResult()
%>
&nbsp;
<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="<%= INT_PAGE_WIDTH %>">
<TR>
	<TD ALIGN=CENTER><%= font(2) %><B>** Submitted **</B></TD>
</TR>
<TR>
	<TD><%= font(2) %>&nbsp;<BR>
	Your order has been submitted. Please <a href="invoice.asp?order=<%= gintOrderID %>">click here</a> to continue with your receipt.</TD>
</TR>
</TABLE>
<%= spacer(1,10) %><BR>
<%
	DrawJavaScriptRedirect "invoice.asp?order=" & gintOrderID & "&msg=S"
	response.flush
end sub

sub DrawApprovedResult
	dim strTagLine, strAuthCode, strRefCode, strOrderCode
	GetOrderAuthCode strAuthCode, strRefCode, strOrderCode
	strTagLine = "Auth " & strAuthCode
	if strRefCode <> "" then
		strTagLine = strTagLine & " Ref " & strRefCode
	end if
	if strOrderCode <> "" then
		strTagLine = strTagLine & " Order " & strOrderCode
	end if
	strTagLine = strTagLine & " " & SafeFormatCurrency("$0.00", GetOrderGrandTotal(), 2)
%>
<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="<%= INT_PAGE_WIDTH %>">
<TR>
	<TD ALIGN=CENTER><%= font(2) %><B>** Transaction Approved **</B></FONT></TD>
</TR>
<TR>
	<TD ALIGN=CENTER><%= font(1) %><FONT SIZE="3"><TT><%= strTagLine %></TT></FONT></TD>
</TR>
<TR>
	<TD><%= font(2) %>&nbsp;<BR>
	Your payment has been approved. Please <a href="invoice.asp?order=<%= gintOrderID %>">click here</a> to continue with your receipt.</TD>
</TR>
</TABLE>
<%= spacer(1,10) %><BR>
<%
	DrawJavaScriptRedirect "invoice.asp?order=" & gintOrderID & "&msg=S"
	response.flush
end sub

sub DrawDeclineResult
	' SWSCodeChange - 26jul2000 added support for https:
%>
<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="<%= INT_PAGE_WIDTH %>">
<TR>
	<TD ALIGN=CENTER><%= font(2) %><B>** Transaction Declined **</B></TD>
</TR>
<TR>
	<TD><%= font(2) %>&nbsp;<BR>
	The bank could not authorize your card. <a href="checkout.asp?action=step6">Click here</a> to review or change your payment information.<BR>
	<BR>
	If you need assistance, please contact customer service at <%= STR_MERCHANT_CS_PHONE %> or email <a href="mailto:<%= STR_MERCHANT_CS_EMAIL %>"><%= STR_MERCHANT_CS_EMAIL %></a>.</TD>
</TR>
</TABLE>
<%= spacer(1,10) %><BR>
<%
	response.flush
end sub

sub DrawErrorResult
%>
<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="<%= INT_PAGE_WIDTH %>">
<TR>
	<TD ALIGN=CENTER><%= font(2) %><B>** Technical Difficulties **</B></TD>
</TR>
<TR>
	<TD><%= font(2) %>&nbsp;<BR>
	A system error has prevented your transaction from being processed. Our technical support staff has been notified.<BR>
	<BR>
	Please contact customer service at <%= STR_MERCHANT_CS_PHONE %> or email <A HREF="mailto:<%= STR_MERCHANT_CS_EMAIL %>"><%= STR_MERCHANT_CS_EMAIL %></A> and refer to tracking number: <%= STR_MERCHANT_TRACKING_PREFIX & gintOrderID%>.</TD>
</TR>
</TABLE>
<%= spacer(1,10) %><BR>
<%
	response.flush
end sub

sub DrawJavaScriptRedirect(strURL)
%>
<SCRIPT LANGUAGE="JavaScript">
<!--
	document.location.replace('<%= strURL %>');
// -->
</SCRIPT>
<%
end sub

sub DebugDct(dct)
	dim c, x, i, k
	x = dct.count - 1
	i = dct.items
	k = dct.keys
	for c = 0 to x
		response.write "item #" & c & ": " & k(c) & " - " & i(c) & "<BR>"
	next
end sub

sub stlSpecialTextInput(strName, intSize, intMax, strValueB, strValueS, strLabel, strCheck, strErrorB, strErrorS, intTabB, intTabS)
	dim strClassLabel, strClassFieldB, strClassFieldS
	
	if strLabel <> "" then
		strLabel = strLabel & ":"
	end if
	
	strClassLabel = "label"
	strClassFieldB = "field"
	strClassFieldS = "field"
	
	if strCheck <> "" then
		strClassLabel = strClassLabel & "req"
		strClassFieldB = strClassFieldB & "req"
		strClassFieldS = strClassFieldS & "req"
	end if
	
	if FormErrors.Exists("bill_" & strName) then
		strClassLabel = strClassLabel & "err"
		strClassFieldB = strClassFieldB & "err"
	end if
	if FormErrors.Exists("ship_" & strName) then
		if right(strClassLabel,3) <> "err" then
			strClassLabel = strClassLabel & "err"
		end if
		strClassFieldS = strClassFieldS & "err"
	end if
	
	response.Write "<tr>" & vbcrlf
	response.Write "<td class=""" & strClassLabel & """>" & strLabel & "</td>" & vbcrlf
	if strErrorB <> "-" then
		response.Write GetAutoCheckStr("bill_" & strName, strCheck, strErrorB)
		response.Write "<td class=""" & strClassFieldB & """><input type=""text"" name=""bill_" & strName & """ size=""" & intSize & """ maxlength=""" & intMax & """ value=""" & Server.HTMLEncode(strValueB) & """ tabindex=""" & intTabB & """></td>" & vbcrlf
	else
		response.Write "<td></td>" & vbcrlf
	end if
	if strErrorS <> "-" then
		response.Write GetAutoCheckStr("ship_" & strName, strCheck, strErrorS)
		response.Write "<td class=""" & strClassFieldS & """><input type=""text"" name=""ship_" & strName & """ size=""" & intSize & """ maxlength=""" & intMax & """ value=""" & Server.HTMLEncode(strValueS) & """ tabindex=""" & intTabS & """></td>" & vbcrlf
	else
		response.Write "<td></td>" & vbcrlf
	end if
	response.Write "</tr>"
end sub

Sub stlSpecialArrayPulldown(strName, strFirstOption, strValueB, strValueS, dctList, strLabel, strCheck, strErrorB, strErrorS, intTabB, intTabS)
	dim strClassLabel, strClassFieldB, strClassFieldS
	
	if strLabel <> "" then
		strLabel = strLabel & ":"
	end if
	
	strClassLabel = "label"
	strClassFieldB = "field"
	strClassFieldS = "field"
	
	if strCheck <> "" then
		strClassLabel = strClassLabel & "req"
		strClassFieldB = strClassFieldB & "req"
		strClassFieldS = strClassFieldS & "req"
	end if
	
	if FormErrors.Exists("bill_" & strName) then
		strClassLabel = strClassLabel & "err"
		strClassFieldB = strClassFieldB & "err"
	end if
	if FormErrors.Exists("ship_" & strName) then
		if right(strClassLabel,3) <> "err" then
			strClassLabel = strClassLabel & "err"
		end if
		strClassFieldS = strClassFieldS & "err"
	end if
	
	response.Write "<tr>" & vbcrlf
	response.Write "<td class=""" & strClassLabel & """>" & strLabel & "</td>" & vbcrlf
	if strErrorB <> "-" then
		response.Write GetAutoCheckStr("bill_" & strName, strCheck, strErrorB)
		response.Write "<td class=""" & strClassFieldB & """>" & PulldownFromArray("bill_" & strName, strFirstOption, strValueB, dctList, " tabindex=""" & intTabB & """") & "</td>" & vbcrlf
	else
		response.Write "<td></td>" & vbcrlf
	end if
	if strErrorS <> "-" then
		response.Write GetAutoCheckStr("ship_" & strName, strCheck, strErrorS)
		response.Write "<td class=""" & strClassFieldS & """>" & PulldownFromArray("ship_" & strName, strFirstOption, strValueS, dctList, " tabindex=""" & intTabS & """") & "</td>" & vbcrlf
	else
		response.Write "<td></td>" & vbcrlf
	end if
	response.Write "</tr>"
End Sub

sub SpecialArrayPulldown(strName, strFirstOption, strValue, dctList, intTab)
	dim k
	response.write "<select name=""" & Server.HTMLEncode(strName) & """ tabindex=""" & intTab & """>" & vbcrlf
	if strFirstOption <> "" then
		response.write "<option value="""">" & Server.HTMLEncode(strFirstOption) & "</option>" & vbcrlf
	end if
	for each k in dctList
		response.write "<option value=""" & Server.HTMLEncode(k & "") & """ "
		if k & "" = strValue & "" then
			response.write "selected"
		end if
		response.write ">" & Server.HTMLEncode(dctList(k) & "") & "</option>" & vbcrlf
	next
	response.write "</select>" & vbcrlf
end sub

sub ValidateInput(dctFormErrors)
	Dim objRegEx 
	
	set objRegEx = new RegExp
	
	with objRegEx
		.Pattern = "[a-zA-z@0-9]*"
		.IgnoreCase = True
		.Global = True
	end with
	
	Dim item
	
	for each item in request.Form
	
		if objRegEx.Test(Request(item)) then
			'Response.Write "inside"
			if not dctFormErrors.Exists(item) then
			
				dctFormErrors.Add item,"Invalid entry"
			end if
			
		end if
	next

end sub
%>