<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: default.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Main store-front, inventory browser
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: code cleanup/documentation
' = Description of Customizations:
' =   (none)
' =====================================================================================
OpenConn

If gintUserName&"" = ""  Then
Response.Redirect virtualbase
End if

Dim rsData,strSQL, intID

intID = Request("id")&""

if IsNumeric(intID) and intID<>"" Then 

    intID = CINT(intID)

else

    intID = -1

end if

if Request("action")&"" = "submit" then

    if intID=0 then

    strSQL = "INSERT INTO  " & STR_TABLE_SHOPPER & " ([chrType],[chrStatus],[dtmCreated],[dtmUpdated],[vchCreatedByUser],[vchUpdatedByUser],[vchCreatedByIP],[vchUpdatedByIP],[intShopperID],[vchLabel],[vchFirstName],[vchLastName],[vchAddress1],[vchCity],[vchState],[vchZip],[vchDayPhone],[vchNightPhone],[vchFax],[vchEmail])"
    strSQL = strSQL & " VALUES ('S','A',GETDATE(),GETDATE(),'system','system','','', "  & gintUserID & ",'" & SqlEncode(Request("strLabel")) & "','" & SqlEncode(Request("strFirstName")) & "','" &  SqlEncode(Request("strLastName")) & "','" & SqlEncode(Request("strAddress1")) & "',' " & SqlEncode(Request("strCity")) & "','"  & SqlEncode(Request("strState")) & "','"  & SqlEncode(Request("strZip")) & "','"  & SqlEncode(Request("strDayPhone")) & "','"  & SqlEncode(Request("strNightPhone")) & "','"  & SqlEncode(Request("strFax")) & "','"  & SqlEncode(Request("strEmail")) & "') "        
                    
    set rsData = gobjConn.execute(strSQL)
    Response.Redirect "distributors.asp"
    else
        strSQL = "UPDATE " & STR_TABLE_SHOPPER & " set vchLabel='" & SqlEncode(Request("strLabel")) & "',vchFirstName='" & SqlEncode(Request("strFirstName")) & "',vchLastName='" & SqlEncode(Request("strLastName")) & "',vchAddress1='" & SqlEncode(Request("strAddress1")) & "', vchCity=' " & SqlEncode(Request("strCity")) & "',vchState='"  & SqlEncode(Request("strState")) & "',vchZip='"  & SqlEncode(Request("strZip")) & "',vchDayPhone='"  & SqlEncode(Request("strDayPhone")) & "',vchNightPhone='"  & SqlEncode(Request("strNightPhone")) & "',vchFax='"  & SqlEncode(Request("strFax")) & "',vchEmail='"  & SqlEncode(Request("strEmail")) & "'    WHERE intID=" & intid & ""
        set rsData = gobjConn.execute(strSQL)

        Response.Redirect "distributors.asp"
    end if

elseif Request("action") = "delete" then
    strSQL = "UPDATE " & STR_TABLE_SHOPPER & " SET chrStatus='D' WHERE intID=" & intID & ""
    set rsData = gobjConn.execute(strSQL)
    Response.Redirect "distributors.asp"

end if


DrawHeader "Stores", "storemain"
DrawPage
DrawFooter "storemain"


response.end

sub DrawPage		
%>
<style>
    .CSSTableGenerator {
        margin:0px;padding:0px;
        width:1200px;
	
    }.CSSTableGenerator table{
        border-collapse: collapse;
        border-spacing: 0;
        width:100%;
        height:100%;
        margin:0px;padding:0px;
    }.CSSTableGenerator tr:last-child td:last-child {
        -moz-border-radius-bottomright:0px;
        -webkit-border-bottom-right-radius:0px;
        border-bottom-right-radius:0px;
    }
    .CSSTableGenerator table tr:first-child td:first-child {
        -moz-border-radius-topleft:0px;
        -webkit-border-top-left-radius:0px;
        border-top-left-radius:0px;
    }
    .CSSTableGenerator table tr:first-child td:last-child {
        -moz-border-radius-topright:0px;
        -webkit-border-top-right-radius:0px;
        border-top-right-radius:0px;
    }.CSSTableGenerator tr:last-child td:first-child{
        -moz-border-radius-bottomleft:0px;
        -webkit-border-bottom-left-radius:0px;
        border-bottom-left-radius:0px;
    }.CSSTableGenerator tr:hover td{
	
    }
    .blockMsg  {border:0px !important;}
    
    .CSSTableGenerator tr:nth-child(odd){ background-color:#f0f0f0; }
    .CSSTableGenerator tr:nth-child(even)    { background-color:#ffffff; }
    .CSSTableGenerator tr:nth-child(2), .CSSTableGenerator tr:nth-child(2) td{ background-color:#393333;color:#fff !important; }
    .CSSTableGenerator td{vertical-align:middle;text-align:left;
        padding:7px;
        font-size:10pt;
        font-family: 'Open Sans', sans-serif;
        font-weight:normal;
        color:#000000;
        }
</style>
<div class="page-content-wrapper">
		<div class="page-content">
        <!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">
					Enhanced Storeview Simulator
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.asp">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
                        
                        <li>
	                        <a href="#">ESS</a>
                        </li>

					</ul>
				</div>
			</div>
           
			<!-- END PAGE HEADER-->

            <div id="forms" style="padding:10px;margin:auto;width:750px;background-color:#F7F7F7;">
					<select class="input-medium" name="campaignSelection" id="campaignSelection" onchange="storeSetCampaign();">
						<option value="">Select a Campaign</option>
					<%
						strSQL = "SELECT intID, vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE chrType='C' and chrStatus='A'"
						set rsData = gobjConn.execute(strSQL)
							While not rsData.Eof
							%>
								<option value="<%= rsData("intID") %>"><%= rsData("vchItemName") %></option>
							<%
								rsData.MoveNext
							Wend
							%>
					</select>
					<select class="input-medium" name="storeSelection" id="storeSelection" onchange="loadProfilesWithStoreDropdown();" style="display:none;">
						<option value="">Select a Store</option>
					<%
						strSQL = "SELECT intid, vchlabel FROM " & STR_TABLE_SHOPPER & " WHERE chrStatus='A' and chrtype='S'"
						set rsData = gobjConn.execute(strSQL)
							While not rsData.Eof
							%>
								<option value="<%= rsData("intID") %>"><%= rsData("vchLabel") %></option>
							<%
								rsData.MoveNext
							Wend
							%>
					</select>
					<select class="input-medium" name="profileSelection" id="profileSelection" onchange="storeSetStore();" style="display:none;"></select>
            </div>

                <script src="../assets/admin/layout/scripts/svg-handler.js" type="text/javascript"></script>
		
				<span id="campaignID" style="display:none"></span>
				<span id="storeID" style="display:none"></span>
				<span id="layoutID" style="display:none"></span>
				<div  id="leftSide" style="width:75%;margin:auto;">
					<div id="stage"></div>
				</div>
		
				<div id="itemDialog"></div>
    </div>
</div>
<%
end sub
%>