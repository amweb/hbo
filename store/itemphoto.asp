<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: itemdetail.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Display details on an item from the inventory.
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

OpenConn

SelectCurrentOrder

dim intID
intID = Request("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if

Inventory_UpdateHitCount intID

dim intParentID, strItemName, strPartNumber, intStock, strImageURL, strImageURL2, mnyItemPrice, mnyShipPrice, blnTaxFlag, blnSoftFlag, fltShipWeight, intForceShipMethod, blnForceSoloItem, strOptionList1, strOptionList2, strOptionList3, intMinQty, strDescription
' intStock = quantity in stock (if supported)
' mnyItemPrice = unit price
' mnyShipPrice = special shipping price for this item
' blnTaxFlag = true if item is taxable
' blnSoftFlag = true if item is a software item (downloable after order is authorized)
' fltShipWeight = weight of item in lbs (if supported)
Inventory_GetItemInfo intID, intParentID, strItemName, strPartNumber, intStock, strImageURL, strImageURL2, mnyItemPrice, mnyShipPrice, blnTaxFlag, blnSoftFlag, fltShipWeight, intForceShipMethod, blnForceSoloItem, strOptionList1, strOptionList2, strOptionList3, intMinQty, strDescription

if strItemName & "" = "" then
	' item not found--user probably bookmarked this page
	response.redirect "default.asp"
end if

dim strPageTitle, strParentName
strPageTitle = strItemName

Custom_GetGlobalInventoryFolders 0

'DrawHeader strPageTitle, "storemain"
DrawPage
'DrawFooter "storemain"

sub DrawPage
	dim strImageTag, strImageTag2, strQtyMsg
	strImageTag2 = ""
	if strImageURL & "" = "" then
		strImageTag	= "<img src=""" & g_imagebase & "products/image-01.jpg"" width=""250"" height=""280"" alt=""" & strItemName & """ />"
	else
		strImageTag = "<img src=""" & imagebase & "products/" & strImageURL & """ border=""0"" />"
		if strImageURL2 & "" <> "" then
			strImageTag2 = "<img src=""" & imagebase & "products/" & strImageURL2 & """ border=""0"" />"
		end if
	end if
%>
		<h2><%= strItemName %></h2>

		<div id="productimage">
			<%= strImageTag %>
			<%= strImageTag2 %><br />
		</div>
<%
end sub
%>