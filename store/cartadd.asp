<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: cartlist.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2000 SacWeb, Inc. All rights reserved.
' = Description:
' =   Displays current shopping cart
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

'if blnLive then
'	if lcase(strServerHost) <> "central4wd.com" then
'		response.redirect "http://central4wd.com" & strScriptName
'	end if
'end if

OpenConn

SelectCurrentOrder

dim strAction
strAction = lcase(Request("action"))
select case strAction
	case "additem": ' add item to cart
		'AddItemToOrder Request("id"), Request("qty")
		AddItemToOrderWithOption Request("id"), Request("qty"), Request("Option1"), Request("Option2"), Request("Option3")
		response.redirect "cartadd.asp?parentid=" & Request("parentid")
	case "multiadd":	' add multiple items to cart
		AddMultipleItemsToOrder	Request
		response.redirect "cartadd.asp?parentid=" & Request("parentid")
	case "delitem": ' delete item from cart
		RemoveLineFromOrder Request("id")
		response.redirect "cartadd.asp?parentid=" & Request("parentid")
	case "cancel": ' cancel order
		CancelOrder
		SelectCurrentOrder	' get updated statistical info
	case "update": ' update quantities
		UpdateQuantities
		SelectCurrentOrder	' get updated statistical info
	case "shippreview" 
		SelectCurrentOrder	' get updated statistical info
end select

if gintOrderID = 0 then
	response.redirect "default.asp"
end if

dim intParentID
intParentID = Request("parentid")
if IsNumeric(intParentID) then
	intParentID = CLng(intParentID)
else
	intParentID = 0
end if

dim strPageTitle, intParentUpID, rsData, verisign
verisign = "no"
strPageTitle = "Your Shopping Cart"
Custom_GetGlobalInventoryFolders 0

DrawHeader strPageTitle, "storemain"
DrawPage
DrawFooter "storemain"

CloseConn
response.end

sub UpdateQuantities
	dim i, intQty, intItemID
	for each i in Request.Form
		if lcase(left(i, 7)) = "intqty_" then
			intItemID = mid(i,8)
			if IsNumeric(intItemID) then
				intItemID = CLng(intItemID)
				intQty = Request.Form(i)
				if IsNumeric(intQty) then
					intQty = CLng(intQty)
				else
					intQty = -1
				end if
				if intQty = 0 then
					' remove item from cart
					RemoveLineFromOrder intItemID
				elseif intQty > 0 then
					' change qty in cart
					EditOrderItemQty intItemID, intQty
				end if
			end if
		end if
	next
end sub

sub DrawPage
	dim rsData, mnySubtotal, intWeight
	set rsData = GetOrderLineItems()
	%>
	<div id="contentArea">
	<table border=0 cellpadding=2 cellspacing=0 width="<%= INT_PAGE_WIDTH %>" bgcolor="<%= cWhite %>">
	<form action="cartlist.asp" method="post">
	<%
		HiddenInput "action", "update"
		HiddenInput "parentid", intParentID
	%>
	<tr id="cart">
		<td>Product</td>
		<td width="15%" align="center">Quantity</td>
		<td width="15%" align="right">Price</td>
		<td width="15%" align="right">Total</td>
		<td>&nbsp;</td>
	</tr>
	<%
	mnySubtotal = 0
	if rsData.eof then %>
	<tr>
		<td id="cartrulerow" colspan="5"><%= spacer(1,1) %></td>
	</tr>
	<% end if
	while not rsData.eof
		%>
	<tr id="cartitem">
		<td><% if not isNull(rsData("intInvID")) then %><a href="itemdetail.asp?id=<%= rsData("intInvID") %>"><% end if %><%= rsData("vchItemName") %></a></td>
		<td align="center"><input type="text" name="intQty_<%= rsData("intID") %>" value="<%= rsData("intQuantity") %>" size="2" /></td>
		<td align="right"><%= SafeFormatCurrency("$0.00", rsData("mnyUnitPrice"), 2) %></td>
		<td align="right"><%= SafeFormatCurrency("$0.00", rsData("mnyUnitPrice") * rsData("intQuantity"), 2) %></td>
		<td align="right"><a href="cartadd.asp?id=<%= rsData("intID") %>&action=delitem&parentid=<%= intParentID %>">
		<img src="<%= g_imagebase %>menu/remove3.gif" width="15" height="15" border="0" alt="Delete item(s)" onClick="return confirm('Are you sure you want to remove this item from your order?');" /></a>
		</td>
	</tr>
	<tr>
		<td id="cartrulerow" colspan="5"><%= spacer(1,1) %></td>
	</tr>
		<%
		mnySubtotal = mnySubtotal + rsData("mnyUnitPrice") * rsData("intQuantity")
		rsData.MoveNext
	wend
	%>
	<!--
	<tr id="cartsubtotal">
		<td>&nbsp;</td>
		<td align="center"><input type="image" src="<%= g_imagebase %>menu/update.gif" width="54" height="16" alt="Update Quantities" border="0" /></td>
		<td align="right" colspan="2" nowrap>Sub Total:&nbsp;<b>
		<%= SafeFormatCurrency("$0.00", mnySubtotal, 2) %></b></td>
		<td>&nbsp;</td>
	</tr>
	-->
	<tr>
		<td colspan="5"><%= spacer(1,4) %></td>
	</tr>
	<tr>
		<td align="center" colspan="5">
			<table border="0" cellpadding="3" cellspacing="1">
			<tr>
				<td align="center"><a href="default.asp?parentid=<%= intParentID %>"><img src="<%= g_imagebase %>menu/continueshopping.gif" width="121" height="16" alt="" border="0" /></a></td>
				<td align="center"><a href="<%= securebase %>store/custserv.asp"><img src="<%= g_imagebase %>menu/customerservice.gif" width="116" height="16" alt="Customer Service" border="0" /></a></td>
				<td align="center"><a href="cartlist.asp?action=cancel" onClick="return confirm('Are you sure you want to cancel your order?');"><img src="<%= g_imagebase %>menu/cancel.gif" width="52" height="16" alt="Cancel This Order" border="0" /></a></td>
				<td align="center"><a href="<%= securebase %>store/custserv.asp?action=save"><img src="<%= g_imagebase %>menu/saveforlater.gif" width="97" height="16" alt="Save for Later" border="0" /></a></td>
			</tr>
			<tr>
				<td align="center" colspan="5" valign="top"><a href="<%= securebase %>store/checkout.asp"><img src="<%= g_imagebase %>menu/checkout.gif" width="68" height="16" alt="Proceed to checkout" border="0" /></a></td>
			</tr>
			</table>
		</td>
	</tr>
	</form>
	</table>
	<%
	if 0=1 then ' deactivated (until fltshipweight is added to STR_TABLE_LINEITEM table)
		' and the rest of the kinks are worked out of the function PollShippingPrice()
		DrawShipPreview strAction, intWeight
	end if
	rsData.close
	set rsData = nothing
end sub

sub DrawShipPreview(strAction, intWeight)
	
	if strAction = "shippreview" then
	%>
	<table cellspacing=0 cellpadding=2 border=0 align="right">
	<tr>
		<td align="center" colspan="2"><b>Shipping Prices for current shopping cart:</b></td>
	</tr>
	<tr bgcolor="<%= cRow2 %>">
		<td>Ground:</td>
		<td align="right"><%= SafeFormatCurrency("n/a",PollShippingPrice(1,intWeight),2) %></td>
	</tr>
	<tr bgcolor="<%= cRow1 %>">
		<td>3 Day:</td>
		<td align="right"><%= SafeFormatCurrency("n/a",PollShippingPrice(2,intWeight),2) %></td>
	</tr>
	<tr bgcolor="<%= cRow2 %>">
		<td>Next Day Air:</td>
		<td align="right"><%= SafeFormatCurrency("n/a",PollShippingPrice(3,intWeight),2) %></td>
	</tr>
	</table>
	<%
	else
	%>
	<div align="right"><a id="blueButton" href="cartadd.asp?action=shippreview">&nbsp;View Shipping Costs&nbsp;</a></div>
	<%
	end if
end sub

sub AddMultipleItemsToOrder (rsItems)
	dim strItemList, intItemID, intQuantity, strOption1, strOption2, strOption3
	strItemList = rsItems("id")

'	response.write strItemList & "<br />"
'	response.end
	
	for each intItemID in split(strItemList, ", ")
		if IsNumeric(intItemID) then
			intQuantity = rsItems("Q_" & intItemID)
			strOption1 = rsItems("O1_" & intItemID)
			strOption2 = rsItems("O2_" & intItemID)
			strOption3 = rsItems("O3_" & intItemID)
			if IsNumeric(intQuantity) then
'				AddItemToOrder intItemID, intQuantity
				AddItemToOrderWithOption intItemID, intQuantity, strOption1, strOption2, strOption3
			else
				if intQuanity <> "" then
					AddItemToOrder intItemID, 1
				end if
			end if
		end if
	next
end sub
%>