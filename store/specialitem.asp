<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: specialitem.asp
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Display user confirmation when ordering an item that requires special handling
' = Revision History:
' =   
' =====================================================================================

OpenConn

SelectCurrentOrder

dim intID
intID = Request("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if

dim intParentID, strItemName, strPartNumber, intStock, strImageURL, strImageURL2, mnyItemPrice, mnyShipPrice, blnTaxFlag, blnSoftFlag, fltShipWeight, intForceShipMethod, blnForceSoloItem, strOptionList1, strOptionList2, strOptionList3, intMinQty, strDescription
' intStock = quantity in stock (if supported)
' mnyItemPrice = unit price
' mnyShipPrice = special shipping price for this item
' blnTaxFlag = true if item is taxable
' blnSoftFlag = true if item is a software item (downloable after order is authorized)
' fltShipWeight = weight of item in lbs (if supported)
Inventory_GetItemInfo intID, intParentID, strItemName, strPartNumber, intStock, strImageURL, strImageURL2, mnyItemPrice, mnyShipPrice, blnTaxFlag, blnSoftFlag, fltShipWeight, intForceShipMethod, blnForceSoloItem, strOptionList1, strOptionList2, strOptionList3, intMinQty, strDescription

if strItemName & "" = "" then
	' item not found--user probably bookmarked this page
	response.redirect "default.asp"
end if

dim strPageTitle
strPageTitle = strItemName

Custom_GetGlobalInventoryFolders 0

DrawHeader strPageTitle, "storemain"
DrawPage
DrawFooter "storemain"

sub DrawPage
	dim strImageTag, strMsg
	if strImageURL & "" = "" then
		strImageTag	= ""
	else
		strImageTag = "<IMG SRC=""" & imagebase & "products/big/" & strImageURL & """>"
	end if
	
	' determine why this item is special
	strMsg = ""
	if not IsNull(intForceShipMethod) then
		' it must be shipped differently than other products currently ordered
		if blnForceSoloItem then
			strMsg = "This item must be shipped by <B>" & GetArrayValue(intForceShipMethod, dctShipOption) & "</B>, and must be <B>ordered by itself</B>."
		else
			strMsg = "This item must be shipped by <B>" & GetArrayValue(intForceShipMethod, dctShipOption) & "</B>."
		end if
	elseif blnForceSoloItem then
		' it must be ordered by itself
		strMsg = "This item must be shipped separately."
	end if
%>
<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%">
<TR>
	<TD COLSPAN=2><%= font(2) %><BR>
	<%= strmsg %>
	If you add this item to your order, the following items will be removed:<BR>
	<BR>
	<%= font(1) %>
<%
	dim rsItem
	set rsItem = GetOrderLineItems_Direct()
	while not rsItem.eof
		if not IsNull(intForceShipMethod) and not IsNull(rsItem("intForceShipMethod")) and intForceShipMethod <> rsItem("intForceShipMethod") then
			response.write rsItem("vchItemName") & " - must be shipped by <B>" & GetArrayValue(rsItem("intForceShipMethod"), dctShipOption) & "</B><BR>"
		elseif blnForceSoloItem then
			response.write rsItem("vchItemName") & "<BR>"
		elseif rsItem("chrForceSoloItem") = "Y" then
			response.write rsItem("vchitemName") & " - must be <B>ordered separately</B><BR>"
		end if
		rsItem.MoveNext
	wend
	rsItem.close
	set rsItem = nothing
%>
	</FONT><BR>
	To add this item and remove the items listed above: <A HREF="cartlist.asp?special=y&id=<%= intID %>&parentid=<%= intParentID %>&action=additem&option1=<%=request("option1")%>&option2=<%=Request("option2")%>&option3=<%=Request("option3")%>">Add To Cart</A><BR>
	<BR>
	<A HREF="default.asp?parentid=<%= Request("parentid") %>">Go Back</A>
	</TD>
</TR>
<TR>
	<TD COLSPAN=2 ALIGN=center><%= strImageTag %></TD>
</TR>
</TABLE>
<%
end sub
%>