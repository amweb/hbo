<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: invoice.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2000 SacWeb, Inc. All rights reserved.
' = Description:
' =   Display order invoice or order history
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

OpenConn

dim intOrderID

intOrderID = Request("order") & Request("o")
if IsNumeric(intOrderID) then
	intOrderID = CLng(intOrderID)
else
	intOrderID = 0
end if

if intOrderID = 0 then
	response.redirect "custserv.asp"
end if

if gintUserID = 0 then
	' need the user to login or create a new account
	response.redirect "custserv.asp?action=login"
end if

dim rsOrder
set rsOrder = GetOrderRecord_Other(intOrderID)
if rsOrder.eof then
	rsOrder.close
	set rsOrder = nothing
	response.redirect "custserv.asp"
end if
if rsOrder("intShopperID") <> gintUserID then
	rsOrder.close
	set rsOrder = nothing
	response.redirect "custserv.asp"
end if
if InStr("SAZXCHRVK", rsOrder("chrStatus")) = 0 then
	rsOrder.close
	set rsOrder = nothing
	response.redirect "custserv.asp"
end if

dim strMsg
strMsg = ucase(Request("msg"))

DrawHeader "Order Invoice", "checkout"
DrawPage
DrawFooter "orderhistory"

rsOrder.close
set rsOrder = nothing
response.end

sub DrawPage
	dim rsLineItem, rsShopper
	dim strMethod, strType, strText
	strMethod = left(rsOrder("chrPaymentMethod"), 1)
	strType = mid(rsOrder("chrPaymentMethod"), 2)
'<div style="width:680px;margin-left:15px;padding:8px;border:1px solid #CCD9E5;">
%>
<div id="invoice">
	<div style="float:right"><img src="<%= g_imagebase %>banners/logo.jpg" width="250" height="118" alt="" /></div>

<%= font(1) %>
<CENTER>
<FONT SIZE="2"><B><%= STR_MERCHANT_NAME %></B></FONT><BR>
<%= STR_MERCHANT_ADDRESS1 & iif(STR_MERCHANT_ADDRESS2 <> "", ", " & STR_MERCHANT_ADDRESS2, "") %><BR>
<%= STR_MERCHANT_CITY & ", " & STR_MERCHANT_STATE & " " & STR_MERCHANT_ZIP %><BR>
<%= "Phone " & STR_MERCHANT_PHONE & "&nbsp;&nbsp;Fax " & STR_MERCHANT_FAX %><BR>
<BR>
<B>Print and save a copy of this screen for your records.</B><BR><BR>
<%= fontx(3,2,cHeading) %><A HREF="<%= aspbase %>store/default.asp"><b>Place another order</b></A></FONT><BR><BR>
</CENTER>

<HR size=1 color="<%= cGoCart_InvDivider %>" />
<%= fontx(2,2,cHeading) %><B>Payment Information</B></FONT><BR>
Tracking Number:  <%= rsOrder("intID") %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Status: <%= GetArrayValue(rsOrder("chrStatus"), dctOrderStatusInvoiceValues) %><BR>
<%
	if strMethod = "O" then
		strMethod = ""
	elseif strType = "CC" then
		if not IsNull(rsOrder("vchPaymentCardNumber")) then
			strMethod = ""
		end if
	end if
	if rsOrder("chrStatus") <> "S" or strMethod = "O" then
		strMethod = ""
	end if
	if strMethod <> "" then
%>
Your order can not be processed until payment is received.<BR>
<%
	end if
%>
&nbsp;<BR>
<%= GetArrayValue(rsOrder("chrPaymentMethod"), dctPaymentMethod) %><BR>
<%
	if strMethod <> "" then
		strText = ""
		if strMethod = "F" then
			strText = "Please print out and complete this form and fax to 916-441-6057."
		elseif strMethod = "M" then
			strText = "Please print out " & iif(strType = "CC", "and complete this form ", "") & "and mail " & iif(strType = "CC", "", "with payment payable to " & STR_MERCHANT_NAME & " ") & "to the address above."
		elseif strMethod = "P" then
			strText = "Please call " & STR_MERCHANT_PHONE & " and refer to order " & rsOrder("intID") & " to provide your payment information."
		end if
		if strText <> "" then
	%>
	<%= strText %><BR>
	<%
			if strType = "CC" and (strMethod = "M" or strMethod = "F") then
	%>
<BR>
<TABLE BORDER="0" CELLPADDING="8" CELLSPACING="0">
<TR>
	<TD><%= font(1) %>Credit Card Type:</TD>
	<TD><%= font(1) %>________________________________________</TD>
</TR>
<TR>
	<TD><%= font(1) %>Credit Card Number:</TD>
	<TD><%= font(1) %>________________________________________</TD>
</TR>
<TR>
	<TD><%= font(1) %>Credit Card Expiration Date:</TD>
	<TD><%= font(1) %>________________________________________</TD>
</TR>
<TR>
	<TD><%= font(1) %>Authorized Signature:</TD>
	<TD><%= font(1) %>________________________________________</TD>
</TR>
</TABLE>
	<%
			end if
		end if
	end if
	if right(rsOrder("chrPaymentMethod"), 2) = "CC" then
		if not IsNull(rsOrder("vchPaymentCardNumber")) then
%>
&nbsp;
<%= rsOrder("vchPaymentCardType") %>
&nbsp;
<%= GetProtectedCardNumber(rsOrder("vchPaymentCardNumber")) %>
(exp <%= rsOrder("chrPaymentCardExpMonth") %>/<%= rsOrder("chrPaymentCardExpyear") %>)
<%
		end if
	elseif rsOrder("chrPaymentMethod") = "OEC" then
%>
<%= rsOrder("vchPaymentBankName") %>
&nbsp;
<%= rsOrder("vchPaymentRtnNumber") %>
-
<%= GetProtectedCardNumber(rsOrder("vchPaymentAcctNumber")) %>
&nbsp;
<%= rsOrder("vchPaymentCheckNumber") %>
<%
	end if
%>
<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=2>
<%
	set rsLineItem = GetOrderTrans_Other(intOrderID)
	while not rsLineItem.eof
		if rsLineItem("chrStatus") = "A" then
%>
<TR>
	<TD><%= GetArrayValue(rsLineItem("chrTransType"), dctTransType) %></TD>
	<TD><%= SafeFormatCurrency("", rsLineItem("mnyTransAmount"), 2) %></TD>
	<TD><%= replace(rsLineItem("dtmTransDate"),":00 "," ") %></TD>
<%
			if rsLineItem("chrStatus") = "A" then
				if not IsNull(rsLineItem("vchAuthCode")) then
%>
	<TD>A<%= rsLineItem("vchAuthCode") %></TD>
<%
				else
%>
	<TD></TD>
<%
				end if
				if not IsNull(rsLineItem("vchAVCode")) then
%>
	<TD>AV&nbsp;<%= rsLineItem("vchAVCode") %></TD>
<%
				else
%>
	<TD></TD>
<%
				end if
				if not IsNull(rsLineItem("vchRefCode")) then
%>
	<TD>Ref&nbsp;<%= rsLineItem("vchRefCode") %></TD>
<%
				else
%>
	<TD></TD>
<%
				end if
				if not IsNull(rsLineItem("vchDeclineCode")) then
%>
	<TD>Decline&nbsp;<%= rsLineItem("vchDeclineCode") %></TD>
<%
				else
%>
	<TD></TD>
<%
				end if
				if not IsNull(rsLineItem("vchOrderCode")) then
%>
	<TD>Order&nbsp;<%= rsLineItem("vchOrderCode") %></TD>
<%
				else
%>
	<TD></TD>
<%
				end if
			else
%>
	<TD><B><%= GetArrayValue(rsLineItem("chrStatus"), dctTransStatusValues) %></B></TD>
<%
			end if
		end if
		rsLineItem.MoveNext
	wend
	rsLineItem.close
	set rsLineItem = nothing
%>
</TABLE>
<HR SIZE=1 COLOR="<%= cGoCart_InvDivider %>" />

<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%">
<TR VALIGN=TOP>
	<TD WIDTH="49%"><%= font(2) %>
	<%= fontx(2,2,cHeading) %><B>Bill To</B></FONT><BR>
	<%
	set rsShopper = GetOrderShopper_Other(rsOrder("intBillShopperID"))
	if rsShopper.eof then
		response.write "<B>System error</B>--unable to retrieve shopper information." & rsOrder("intbillShopperID")
	else
		response.write rsShopper("vchLastName") & ", " & rsShopper("vchFirstName") & "<BR>"
		if not IsNull(rsShopper("vchCompany")) then
			response.write rsShopper("vchCompany") & "<BR>"
		end if
		if not IsNull(rsShopper("vchAddress1")) then
			response.write rsShopper("vchAddress1") & "<BR>"
		end if
		if not IsNull(rsShopper("vchAddress2")) then
			response.write rsShopper("vchAddress2") & "<BR>"
		end if
		response.write rsShopper("vchCity") & ", " & rsShopper("vchState") & " " & rsShopper("vchZip") & "<BR>"
		if not IsNull(rsShopper("vchCountry")) then
			response.write rsShopper("vchCountry") & "<BR>"
		end if
		if not IsNull(rsShopper("vchDayPhone")) then
			response.write "Day: " & rsShopper("vchDayPhone") & "<BR>"
		end if
		if not IsNull(rsShopper("vchNightPhone")) then
			response.write "Night: " & rsShopper("vchNightPhone") & "<BR>"
		end if
		if not IsNull(rsShopper("vchFax")) then
			response.write "Fax: " & rsShopper("vchFax") & "<BR>"
		end if
		if not IsNull(rsShopper("vchEmail")) then
			response.write "Email: " & rsShopper("vchEmail") & "<BR>"
		end if
	end if
	rsShopper.close
	set rsShopper = nothing
%>
	</TD>
	<TD><%= spacer(15,1) %></TD>
	<TD WIDTH="48%"><%= font(2) %>
	<%= fontx(2,2,cHeading) %><B>Ship To</B></FONT><BR>
	<%
	set rsShopper = GetOrderShopper_Other(rsOrder("intShipShopperID"))
	if rsShopper.eof then
		response.write "same as billing"
	else
		response.write rsShopper("vchLastName") & ", " & rsShopper("vchFirstName") & "<BR>"
		if not IsNull(rsShopper("vchCompany")) then
			response.write rsShopper("vchCompany") & "<BR>"
		end if
		if not IsNull(rsShopper("vchAddress1")) then
			response.write rsShopper("vchAddress1") & "<BR>"
		end if
		if not IsNull(rsShopper("vchAddress2")) then
			response.write rsShopper("vchAddress2") & "<BR>"
		end if
		response.write rsShopper("vchCity") & ", " & rsShopper("vchState") & " " & rsShopper("vchZip") & "<BR>"
		if not IsNull(rsShopper("vchCountry")) then
			response.write rsShopper("vchCountry") & "<BR>"
		end if
		if not IsNull(rsShopper("vchDayPhone")) then
			response.write "Day: " & rsShopper("vchDayPhone") & "<BR>"
		end if
		if not IsNull(rsShopper("vchNightPhone")) then
			response.write "Night: " & rsShopper("vchNightPhone") & "<BR>"
		end if
		if not IsNull(rsShopper("vchFax")) then
			response.write "Fax: " & rsShopper("vchFax") & "<BR>"
		end if
		if not IsNull(rsShopper("vchEmail")) then
			response.write "Email: " & rsShopper("vchEmail") & "<BR>"
		end if
	end if
	rsShopper.close
	set rsShopper = nothing
	%>
	</TD>
</TR>
</TABLE>

<%
	set rsLineItem = GetOrderLineItems_Other(intOrderID)
%>

<HR size=1 color="<%= cGoCart_InvDivider %>" />
<%= fontx(2,2,cHeading) %><B>Order Details</B></FONT><BR>

<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%">
<TR BGCOLOR="<%= cGoCart_CSHdrBG %>">
	<TD><%= fontx(1,1,cWhite) %>Item #</TD>
	<TD><%= fontx(1,1,cWhite) %>Product</TD>
	<TD ALIGN=CENTER><%= fontx(1,1,cWhite) %>Quantity</TD>
	<TD ALIGN=RIGHT><%= fontx(1,1,cWhite) %>Price</TD>
	<TD ALIGN=RIGHT><%= fontx(1,1,cWhite) %>Total</TD>
</TR>
<%
while not rsLineItem.eof
	%>
	<TR>
		<TD><%= rsLineItem("vchPartNumber") %>&nbsp;</TD>
		<TD><%= rsLineItem("vchItemName") %>&nbsp;</TD>
		<TD ALIGN=CENTER><%= iif(IsNull(rsLineItem("intQuantity")), "&nbsp;", rsLineItem("intQuantity")) %></TD>
		<TD ALIGN=RIGHT><%= SafeFormatCurrency("&nbsp;", rsLineItem("mnyUnitPrice"), 2) %></TD>
		<TD ALIGN=RIGHT><%= SafeFormatCurrency("&nbsp;", rsLineItem("mnyPrice"), 2) %></TD>
	</TR>
	<TR>
		<TD COLSPAN=5><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cGoCart_InvDivider %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
	</TR>
<%
		rsLineItem.MoveNext
	wend
	rsLineItem.close
	set rsLineItem = nothing

	' Shipping: xx
	' Nontaxable Subtotal: xx
	' Taxable Subtotal: xx
	' Tax: xx
	' Total: xx
	if not IsNull(rsOrder("mnyShipAmount")) then
%>
<TR>
	<TD NOWRAP COLSPAN="4" ALIGN=RIGHT><%= font(2) %><B>(<%= GetArrayValue(rsOrder("intShipOption"), dctShipOption) %>) Shipping:</B>&nbsp;</TD>
	<TD ALIGN=RIGHT><%= font(2) %><%= SafeFormatCurrency("$0.00", rsOrder("mnyShipAmount"), 2) %></TD>
</TR>
<%
	end if
	dim mnyTaxSubTotal, mnyNonTaxSubTotal
	if not IsNull(rsOrder("mnyNonTaxSubtotal")) then
		mnyNonTaxSubTotal = rsOrder("mnyNonTaxSubtotal")
	else
		mnyNonTaxSubTotal = 0
	end if
	if not IsNull(rsOrder("mnyTaxSubtotal")) then
		mnyTaxSubTotal = rsOrder("mnyTaxSubTotal")
	else
		mnyTaxSubTotal = 0
	end if
	if mnyTaxSubTotal = 0 or mnyNonTaxSubTotal = 0 then
		' only one subtotal--we'll list it as "Subtotal"
%>
<TR>
	<TD COLSPAN=4 ALIGN=RIGHT><%= font(2) %><B>Subtotal:</B>&nbsp;</TD>
	<TD ALIGN=RIGHT><%= font(2) %><%= FormatCurrency(mnyTaxSubTotal + mnyNonTaxSubTotal, 2) %></TD>
</TR>
<%
	else
%>
<TR>
	<TD COLSPAN=4 ALIGN=RIGHT><%= font(2) %><B>Taxable Subtotal:</B>&nbsp;</TD>
	<TD ALIGN=RIGHT><%= font(2) %><%= FormatCurrency(mnyTaxSubtotal, 2) %></TD>
</TR>
<TR>
	<TD COLSPAN=4 ALIGN=RIGHT><%= font(2) %><B>Non-Taxable Subtotal:</B>&nbsp;</TD>
	<TD ALIGN=RIGHT><%= font(2) %><%= FormatCurrency(mnyNonTaxSubtotal, 2) %></TD>
</TR>
<%
	end if
	if rsOrder("mnyTaxAmount") <> 0 and not IsNull(rsOrder("mnyTaxAmount")) then
%>
<TR>
	<TD COLSPAN=4 ALIGN=RIGHT><%= font(2) %><B>(<%= GetArrayValue(rsOrder("intTaxZone"), GetTaxZoneList()) %>) Tax @ <%= SafeFormatNumber("0", rsOrder("fltTaxRate") * 100, 2) %>%:</B>&nbsp;</TD>
	<TD ALIGN=RIGHT><%= font(2) %><%= SafeFormatCurrency("n/a", rsOrder("mnyTaxAmount"), 2) %></TD>
</TR>
<%
	end if
	if rsOrder("mnyDiscountAmount") <> 0 and not IsNull(rsOrder("mnyDiscountAmount")) then
%>
<TR>
	<TD COLSPAN=4 ALIGN=RIGHT><%= font(2) %><B><%= rsOrder("vchReferalName") & " " & rsOrder("fltReferalDiscount") & "% Discount:" %></B>&nbsp;</TD>
	<TD ALIGN=RIGHT><%= font(2) %>-<%= SafeFormatCurrency("n/a", rsOrder("mnyDiscountAmount"), 2) %></TD>
</TR>
<%
	end if

	if rsOrder("chrPromoCode")&"" <> "" then
%>
<TR>
	<TD COLSPAN=4 ALIGN=RIGHT><%= font(2) %><b>Promo Code Used:</b></TD>
	<TD ALIGN=RIGHT><%= font(2) %><%=rsOrder("chrPromoCode")%></TD>
</TR>
<%
	end if
%>
<TR BGCOLOR="<%= cGoCart_CSTotalRowBG %>">
	<TD COLSPAN=4 ALIGN=RIGHT><%= fontx(2,2,cDkRed) %><B>Total:</B>&nbsp;</TD>
	<TD ALIGN=RIGHT><%= fontx(2,2,cDkRed) %><B><%= SafeFormatCurrency("$0.00", rsOrder("mnyGrandTotal"), 2) %></B></TD>
</TR>
</TABLE>
</div> <!--closes <div style="padding: 8px;">-->
<%
session.Abandon()

end sub
%>