<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: default.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Main store-front, inventory browser
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: code cleanup/documentation
' = Description of Customizations:
' =   (none)
' =====================================================================================
OpenConn

If gintUserName&"" = ""  Then
Response.Redirect virtualbase
End if

Dim rsData,strSQL, intID

intID = Request("id")&""

if IsNumeric(intID) and intID<>"" Then 

    intID = CINT(intID)

else

    intID = -1

end if

if Request("action")&"" = "submit" then

    if intID=0 then

    strSQL = "INSERT INTO  " & STR_TABLE_SHOPPER & " ([chrType],[chrStatus],[dtmCreated],[dtmUpdated],[vchCreatedByUser],[vchUpdatedByUser],[vchCreatedByIP],[vchUpdatedByIP],[intShopperID],[vchLabel],[vchFirstName],[vchLastName],[vchAddress1],[vchCity],[vchState],[vchZip],[vchDayPhone],[vchNightPhone],[vchFax],[vchEmail])"
    strSQL = strSQL & " VALUES ('S','A',GETDATE(),GETDATE(),'system','system','','', "  & gintUserID & ",'" & SqlEncode(Request("strLabel")) & "','" & SqlEncode(Request("strFirstName")) & "','" &  SqlEncode(Request("strLastName")) & "','" & SqlEncode(Request("strAddress1")) & "',' " & SqlEncode(Request("strCity")) & "','"  & SqlEncode(Request("strState")) & "','"  & SqlEncode(Request("strZip")) & "','"  & SqlEncode(Request("strDayPhone")) & "','"  & SqlEncode(Request("strNightPhone")) & "','"  & SqlEncode(Request("strFax")) & "','"  & SqlEncode(Request("strEmail")) & "') "        
                    
    set rsData = gobjConn.execute(strSQL)
    Response.Redirect "distributors.asp"
    else
        strSQL = "UPDATE " & STR_TABLE_SHOPPER & " set vchLabel='" & SqlEncode(Request("strLabel")) & "',vchFirstName='" & SqlEncode(Request("strFirstName")) & "',vchLastName='" & SqlEncode(Request("strLastName")) & "',vchAddress1='" & SqlEncode(Request("strAddress1")) & "', vchCity=' " & SqlEncode(Request("strCity")) & "',vchState='"  & SqlEncode(Request("strState")) & "',vchZip='"  & SqlEncode(Request("strZip")) & "',vchDayPhone='"  & SqlEncode(Request("strDayPhone")) & "',vchNightPhone='"  & SqlEncode(Request("strNightPhone")) & "',vchFax='"  & SqlEncode(Request("strFax")) & "',vchEmail='"  & SqlEncode(Request("strEmail")) & "'    WHERE intID=" & intid & ""
        set rsData = gobjConn.execute(strSQL)

        Response.Redirect "distributors.asp"
    end if

elseif Request("action") = "delete" then
    strSQL = "UPDATE " & STR_TABLE_SHOPPER & " SET chrStatus='D' WHERE intID=" & intID & ""
    set rsData = gobjConn.execute(strSQL)
    Response.Redirect "distributors.asp"

end if


DrawHeader "Stores", "storemain"
DrawPage
DrawFooter "storemain"


response.end

sub DrawPage		
%>
<style>
.CSSTableGenerator {
	margin:0px;padding:0px;
	width:100%;
	border:1px solid #000000;
	
	-moz-border-radius-bottomleft:0px;
	-webkit-border-bottom-left-radius:0px;
	border-bottom-left-radius:0px;
	
	-moz-border-radius-bottomright:0px;
	-webkit-border-bottom-right-radius:0px;
	border-bottom-right-radius:0px;
	
	-moz-border-radius-topright:0px;
	-webkit-border-top-right-radius:0px;
	border-top-right-radius:0px;
	
	-moz-border-radius-topleft:0px;
	-webkit-border-top-left-radius:0px;
	border-top-left-radius:0px;
}.CSSTableGenerator table{
    border-collapse: collapse;
        border-spacing: 0;
	width:100%;
	height:100%;
	margin:0px;padding:0px;
}.CSSTableGenerator tr:last-child td:last-child {
	-moz-border-radius-bottomright:0px;
	-webkit-border-bottom-right-radius:0px;
	border-bottom-right-radius:0px;
}
.CSSTableGenerator table tr:first-child td:first-child {
	-moz-border-radius-topleft:0px;
	-webkit-border-top-left-radius:0px;
	border-top-left-radius:0px;
}
.CSSTableGenerator table tr:first-child td:last-child {
	-moz-border-radius-topright:0px;
	-webkit-border-top-right-radius:0px;
	border-top-right-radius:0px;
}.CSSTableGenerator tr:last-child td:first-child{
	-moz-border-radius-bottomleft:0px;
	-webkit-border-bottom-left-radius:0px;
	border-bottom-left-radius:0px;
}.CSSTableGenerator tr:hover td{
	
}
.CSSTableGenerator tr:nth-child(odd){ background-color:#e5e5e5; }
.CSSTableGenerator tr:nth-child(even)    { background-color:#ffffff; }.CSSTableGenerator td{
	vertical-align:middle;
	
	
	border:1px solid #000000;
	border-width:0px 1px 1px 0px;
	text-align:left;
	padding:7px;
	font-size:10px;
	font-family:Arial;
	font-weight:normal;
	color:#000000;
}.CSSTableGenerator tr:last-child td{
	border-width:0px 1px 0px 0px;
}.CSSTableGenerator tr td:last-child{
	border-width:0px 0px 1px 0px;
}.CSSTableGenerator tr:last-child td:last-child{
	border-width:0px 0px 0px 0px;
}
.CSSTableGenerator tr:first-child td{
		background:-o-linear-gradient(bottom, #cccccc 5%, #b2b2b2 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #cccccc), color-stop(1, #b2b2b2) );
	background:-moz-linear-gradient( center top, #cccccc 5%, #b2b2b2 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#cccccc", endColorstr="#b2b2b2");	background: -o-linear-gradient(top,#cccccc,b2b2b2);

	background-color:#cccccc;
	border:0px solid #000000;
	text-align:center;
	border-width:0px 0px 1px 1px;
	font-size:14px;
	font-family:Arial;
	font-weight:bold;
	color:#000000;
}
.CSSTableGenerator tr:first-child:hover td{
	background:-o-linear-gradient(bottom, #cccccc 5%, #b2b2b2 100%);	background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #cccccc), color-stop(1, #b2b2b2) );
	background:-moz-linear-gradient( center top, #cccccc 5%, #b2b2b2 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#cccccc", endColorstr="#b2b2b2");	background: -o-linear-gradient(top,#cccccc,b2b2b2);

	background-color:#cccccc;
}
.CSSTableGenerator tr:first-child td:first-child{
	border-width:0px 0px 1px 0px;
}
.CSSTableGenerator tr:first-child td:last-child{
	border-width:0px 0px 1px 1px;
}
</style>
<div class="page-content-wrapper">
		<div class="page-content">
        <!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">
					My Address Book
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.asp">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
                        
                        <li>
	                        <a href="#">Stores</a>
                        </li>

					</ul>
				</div>
			</div>
            <div style="width:600px;margin:0 auto;">
           
			<!-- END PAGE HEADER-->
            <%
            
                if intID = -1 Then
                %>
                 <div style="margin-bottom:10px;font-weight:bold;"><a href="distributors.asp?id=0&action=edit">Add a Location</a><br /></div>
                 
                <%
                    
                    strSQL = "SELECT *  FROM " & STR_TABLE_SHOPPER & " WHERE chrStatus='A' and chrtype='S'"
                    set rsData = gobjConn.execute(strSQL)
                    
                    if not rsData.Eof Then
                        %>
                        <table class="CSSTableGenerator">
                            <tr>
                                <th>Name</th><th>Address</th><th></th><th></th>
                            </tr>
                        <%
                        While not rsData.Eof
                        %>
                        <tr>
                            <td><%= rsData("vchLabel") %></td>
                            <td><%= rsData("vchCity") %>&nbsp;<%= rsData("vchState") %>&nbsp;<%= rsData("vchZip") %>&nbsp;<%= rsData("vchCountry") %> </td>
                            <td><a href="distributors.asp?id=<%= rsData("intID") %>&action=edit">Edit</a></td>
                            <td><a href="distributors.asp?id=<%= rsData("intID") %>&action=delete" onclick=" return confirm('Are you sure you want to delete <%= rsData("vchFirstName") %> &nbsp; <%= rsData("vchLastName") %>');">Delete</a></td>
                        </tr>
                        <%
                            rsData.MoveNext
                        Wend
                        %>
                        </table>
                        <%
                    else
                        %>
                        No Locations found

                        <%
                    end if


                else
                    
                    strSQL = "SELECT *  FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & intid & " AND chrStatus='A'"
                    set rsData = gobjConn.execute(strSQL)

                    if rsData.Eof Then set rsData = Request
                    %>
                    <form  action="distributors.asp" method="post">
                        <%
	                        HiddenInput "action", "submit"
                            HiddenInput "id", intID
                        if intID>0 then
                        %>
                        <h3>Edit Location <%= rsData("vchFirstName") %>&nbsp;<%= rsData("vchLastName") %></h3>
                        <%else%>
                        <h3>Add Location</h3>

                        <%end if%>
                        <div class="form-group">
		                    <label class="control-label visible-ie8 visible-ie9">Label</label>
		                    <div class="input-icon">
			                    <i class="fa fa-check"></i>
			                    <input class="form-control placeholder-no-fix" type="text" placeholder="Company" name="strLabel" value="<%= rsData("vchLabel") %>"/>
		                    </div>
	                    </div>
	                    <div class="form-group">
	                        <label class="control-label visible-ie8 visible-ie9">Full Name</label>
	                        <div class="row">
		                        <div class="col-md-6">
			                        <div class="input-icon">
				                        <i class="fa fa-font"></i>
				                        <input class="form-control placeholder-no-fix" type="text" placeholder="First Name" name="strFirstName" maxlength="15" pattern="^[\w|\s|-]*$" onchange="try{setCustomValidity('')}catch(e){}" oninvalid="setCustomValidity('Only the following characters are allowed: A-Z,0-9,_,space,-')" value="<%= rsData("vchFirstName") %>"/>
			                        </div>
		                        </div>
		                        <div class="col-md-6">
			                        <div class="input-icon">
				                        <i class="fa fa-font"></i>
				                        <input class="form-control placeholder-no-fix" type="text" placeholder="Last Name" name="strLastName" maxlength="100" pattern="^[\w|\s|-]*$" onchange="try{setCustomValidity('')}catch(e){}" oninvalid="setCustomValidity('Only the following characters are allowed: A-Z,0-9,_,space,-')" value="<%= rsData("vchLastName") %>"/>
			                        </div>
		                        </div>
	                        </div>
	                    </div>
                        <div class="form-group">
	                        <label class="control-label visible-ie8 visible-ie9">Phones</label>
	                        <div class="row">
		                        <div class="col-md-6">
			                        <div class="input-icon">
				                        <i class="fa fa-phone"></i>
				                        <input class="form-control placeholder-no-fix" type="text" placeholder="Day Phone" name="strDayPhone" maxlength="20" value="<%= rsData("vchDayPhone") %>"/>
			                        </div>
		                        </div>
		                        <div class="col-md-6">
			                        <div class="input-icon">
				                        <i class="fa fa-phone"></i>
				                        <input class="form-control placeholder-no-fix" type="text" placeholder="Night Phone" name="strNightPhone" maxlength="20" value="<%= rsData("vchNightPhone") %>"/>
			                        </div>
		                        </div>
	                        </div>
	                    </div>
                        <div class="form-group">
		                    <label class="control-label visible-ie8 visible-ie9">Fax</label>
		                    <div class="input-icon">
			                    <i class="fa fa-phone"></i>
			                    <input class="form-control placeholder-no-fix" type="text" placeholder="Fax" name="strFax" maxlength="20" value="<%= rsData("vchFax") %>"/>
		                    </div>
	                    </div>
                        <div class="form-group">
		                    <label class="control-label visible-ie8 visible-ie9">Email</label>
		                    <div class="input-icon">
			                    <i class="fa fa-envelope"></i>
			                    <input class="form-control placeholder-no-fix" type="text" placeholder="Email Address" name="strEmail" maxlength="255" value="<%= rsData("vchEmail") %>"/>
		                    </div>
	                    </div>
                        <div class="form-group">
		                    <label class="control-label visible-ie8 visible-ie9">Address</label>
		                    <div class="input-icon">
			                    <i class="fa fa-check"></i>
			                    <input class="form-control placeholder-no-fix" type="text" placeholder="Address" name="strAddress1" maxlength="255" value="<%= rsData("vchAddress1") %>"/>
		                    </div>
	                    </div>
	                    <div class="form-group">
		                    <label class="control-label visible-ie8 visible-ie9">City/Town</label>
		                    <div class="input-icon">
			                    <i class="fa fa-location-arrow"></i>
			                    <input class="form-control placeholder-no-fix" type="text" placeholder="City/Town" name="strCity" maxlength="50" pattern="^[\w|\s|-]*$" onchange="try{setCustomValidity('')}catch(e){}" oninvalid="setCustomValidity('Only the following characters are allowed: A-Z,0-9,_,space,-')" value="<%= rsData("vchCity") %>"/>
		                    </div>
	                    </div>
                        <div class="form-group">
		                    <label class="control-label visible-ie8 visible-ie9">State/Provence</label>
		                    <div class="input-icon">
			                    <i class="fa fa-location-arrow"></i>
			                    <input class="form-control placeholder-no-fix" type="text" placeholder="State/Provence" name="strState" maxlength="20" pattern="^[\w|\s|-]*$" onchange="try{setCustomValidity('')}catch(e){}" oninvalid="setCustomValidity('Only the following characters are allowed: A-Z,0-9,_,space,-')" value="<%= rsData("vchState") %>"/>
		                    </div>
	                    </div>
                        <div class="form-group">
		                    <label class="control-label visible-ie8 visible-ie9">Zip Code/Postal Code</label>
		                    <div class="input-icon">
			                    <i class="fa fa-location-arrow"></i>
			                    <input class="form-control placeholder-no-fix" type="text" placeholder="Zip Code/Postal Code" name="strZip" maxlength="15" value="<%= rsData("vchZip") %>"/>
		                    </div>
	                    </div>
                        <div class="form-actions">
				
				            <button type="submit" id="register-submit-btn" class="btn red pull-right">
				            Save Location <i class="m-icon-swapright m-icon-white"></i>
				            </button>
			            </div>
		            </form>

                    <%
                End If


             %>
             </div>
            
    </div>
</div>
<%
end sub
%>