<%@ LANGUAGE="VBScript" %>
<!--#include file="config/incInit.asp"-->

<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/admin/layout/css/themes/light2.css" rel="stylesheet" type="text/css" />
<link href="<%= virtualbase %>assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<%

' =====================================================================================
' = File: sidebar.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Admin Frameset - sidebar frame
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

dim pageTitle, pageType
dim aryMenu(80,1), x, intExpandItem

intExpandItem = Request("select")
if CLng(intExpandItem) then
	intExpandItem = CLng(intExpandItem)
else
	intExpandItem = 0
end if

' prefix labels with a + if it's a sub-menu item with a link
' or with a * if it is a top-level link with no sub items
' or with a ! if it is not available (disabled)
' e.g.:
'  Top-Level Label
'   +sub item
'   +sub item
'  Top Level Label
'   +sub item
'   +sub item
'  *Top Level Item
'  *Top Level Item

dim TF_SIDEBAR_EXPAND_ALL
TF_SIDEBAR_EXPAND_ALL = request("expandall")

x = 0
if CheckAdminLogin_Sideframe() then
	dim strBrand
	if Session(SESSION_MERCHANT_BID) <> "" then
'		response.write "intBrandID: " & Session(SESSION_MERCHANT_BID) & "<br />"
		strBrand = "brand_"
	end if
	aryMenu(x,0) = "*System Summary" : aryMenu(x,1) = "main.asp" : x = x + 1
	if CheckUserAccess(ACC_INVENTORY_VIEW) then
		aryMenu(x,0) = "Pre-Buy Inventory Control" : x = x + 1
		aryMenu(x,0) = "+Search Inventory" : aryMenu(x,1) = "merchant/inv_search.asp" : x = x + 1
		aryMenu(x,0) = "+List by Category" : aryMenu(x,1) = "merchant/inv_list.asp" : x = x + 1
		aryMenu(x,0) = "+List by Brand" : aryMenu(x,1) = "merchant/inv_listbrands.asp" : x = x + 1
		aryMenu(x,0) = "+Flat Inventory Listing" : aryMenu(x,1) = "merchant/inv_stock.asp?all" : x = x + 1
		aryMenu(x,0) = "+Out of Stock Range" : aryMenu(x,1) = "merchant/inv_stock.asp" : x = x + 1
		aryMenu(x,0) = "+Brands" : aryMenu(x,1) = "merchant/inv_blist.asp" : x = x + 1
        aryMenu(x,0) = "+Programs" : aryMenu(x,1) = "merchant/inv_plist.asp" : x = x + 1
        aryMenu(x,0) = "+Campaigns" : aryMenu(x,1) = "merchant/inv_clist.asp" : x = x + 1
        aryMenu(x,0) = "+Categories" : aryMenu(x,1) = "merchant/inv_glist.asp" : x = x + 1
	end if

    if CheckUserAccess(ACC_INVENTORY_VIEW) then
		aryMenu(x,0) = "HBO Videos" : x = x + 1
        aryMenu(x,0) = "+Video List" : aryMenu(x,1) = "merchant/inv_vlist.asp" : x = x + 1
	end if

    if CheckUserAccess(ACC_INVENTORY_VIEW) then
		aryMenu(x,0) = "Training & FAQ files" : x = x + 1
        aryMenu(x,0) = "+File List" : aryMenu(x,1) = "merchant/inv_filist.asp" : x = x + 1
	end if
	
	if CheckUserAccess(ACC_ORDER_VIEW) then
		aryMenu(x,0) = "Order Summary" : x = x + 1
		if CheckUserAccess(ACC_ORDER_CREATE) then
			aryMenu(x,0) = "+Create New Order" : aryMenu(x,1) = "merchant/"&strBrand&"order_detail.asp?action=createnew" : x = x + 1
		end if
		aryMenu(x,0) = "+Search Orders" : aryMenu(x,1) = "merchant/"&strBrand&"order_list.asp" : x = x + 1
		aryMenu(x,0) = "+Orders by Distributor" : aryMenu(x,1) = "merchant/"&strBrand&"order_list.asp?preset=dist" : x = x + 1
		aryMenu(x,0) = "+Orders by Salesperson" : aryMenu(x,1) = "merchant/"&strBrand&"order_list.asp?preset=sales" : x = x + 1
        aryMenu(x,0) = "+Orders by Current Campaigns" : aryMenu(x,1) = "merchant/"&strBrand&"order_list.asp?preset=campaign" : x = x + 1
		aryMenu(x,0) = "+Orders by Past Campaigns" : aryMenu(x,1) = "merchant/"&strBrand&"order_list.asp?preset=pastcampaign" : x = x + 1
	end if
	
	if CheckUserAccess(ACC_REPORT_VIEW) then
		aryMenu(x,0) = "Reports" : x = x + 1
		aryMenu(x,0) = "+Select Date Range" : aryMenu(x,1) = "merchant/report_selectdate.asp" : x = x + 1
		aryMenu(x,0) = "+Summary Report" : aryMenu(x,1) = "merchant/report_summary.asp" : x = x + 1
		aryMenu(x,0) = "+Orders By State" : aryMenu(x,1) = "merchant/report_state.asp" : x = x + 1
		aryMenu(x,0) = "+Orders By City" : aryMenu(x,1) = "merchant/report_city.asp" : x = x + 1
		aryMenu(x,0) = "+Orders by Tax Zone" : aryMenu(x,1) = "merchant/report_tax.asp" : x = x + 1
		'aryMenu(x,0) = "+Orders by Shopper" : aryMenu(x,1) = "merchant/report_shopper.asp" : x = x + 1
		aryMenu(x,0) = "+Orders by Product" : aryMenu(x,1) = "merchant/report_item.asp" : x = x + 1
		aryMenu(x,0) = "+Orders by Brand" : aryMenu(x,1) = "merchant/report_brand.asp" : x = x + 1
		'aryMenu(x,0) = "+Orders by Referral" : aryMenu(x,1) = "merchant/report_referral.asp" : x = x + 1
'		aryMenu(x,0) = "+Orders by Quantity" : aryMenu(x,1) = "!merchant/report_qty.asp" : x = x + 1
	end if
	
	if CheckUserAccess(ACC_SHOPPER_VIEW) then
		aryMenu(x,0) = "Salesperson List" : x = x + 1
		if CheckUserAccess(ACC_SHOPPER_EDIT) then
			aryMenu(x,0) = "+Create New Salesperson" : aryMenu(x,1) = "merchant/shopper_detail.asp" : x = x + 1
		end if
		aryMenu(x,0) = "+Search Salespeople" : aryMenu(x,1) = "merchant/shopper_list.asp" : x = x + 1
		aryMenu(x,0) = "+All Salespeople" : aryMenu(x,1) = "merchant/shopper_list.asp?preset=all" : x = x + 1
		aryMenu(x,0) = "+Salespeople Groups" : aryMenu(x,1) = "merchant/group_list.asp" : x = x + 1
	end if
	
    if CheckUserAccess(ACC_ORDER_VIEW) then
		aryMenu(x,0) = "Long-Term Inventory Orders" : x = x + 1
		aryMenu(x,0) = "+Search Orders" : aryMenu(x,1) = "merchant/"&strBrand&"orderlt_list.asp" : x = x + 1
		aryMenu(x,0) = "+Orders by Distributor" : aryMenu(x,1) = "merchant/"&strBrand&"orderlt_list.asp?preset=dist" : x = x + 1
		aryMenu(x,0) = "+Orders by Salesperson" : aryMenu(x,1) = "merchant/"&strBrand&"orderlt_list.asp?preset=sales" : x = x + 1
        aryMenu(x,0) = "+Orders by Current Campaigns" : aryMenu(x,1) = "merchant/"&strBrand&"orderlt_list.asp?preset=campaign" : x = x + 1
		aryMenu(x,0) = "+Orders by Past Campaigns" : aryMenu(x,1) = "merchant/"&strBrand&"orderlt_list.asp?preset=pastcampaign" : x = x + 1
	end if

    if CheckUserAccess(ACC_INVENTORY_VIEW) then
		aryMenu(x,0) = "Long-Term Inventory Control" : x = x + 1
		aryMenu(x,0) = "+Search Inventory" : aryMenu(x,1) = "merchant/invlt_search.asp" : x = x + 1
		aryMenu(x,0) = "+List by Category" : aryMenu(x,1) = "merchant/invlt_list.asp" : x = x + 1
		aryMenu(x,0) = "+Categories" : aryMenu(x,1) = "merchant/invlt_blist.asp" : x = x + 1
		aryMenu(x,0) = "+Subcategories" : aryMenu(x,1) = "merchant/invlt_glist.asp" : x = x + 1
	end if

	if CheckUserAccess(ACC_TRANS_VIEW) AND false then
		aryMenu(x,0) = "Transaction Log" : x = x + 1
		aryMenu(x,0) = "+Search Transactions" : aryMenu(x,1) = "merchant/trans_search.asp" : x = x + 1
		aryMenu(x,0) = "+Pending/Review" : aryMenu(x,1) = "merchant/trans_search.asp?preset=review" : x = x + 1
		aryMenu(x,0) = "+Last 48 hours" : aryMenu(x,1) = "merchant/trans_search.asp?preset=48hours" : x = x + 1
		aryMenu(x,0) = "+This Week" : aryMenu(x,1) = "merchant/trans_search.asp?preset=thisweek" : x = x + 1
		aryMenu(x,0) = "+This Month" : aryMenu(x,1) = "merchant/trans_search.asp?preset=thismonth" : x = x + 1
		aryMenu(x,0) = "+Year To Date" : aryMenu(x,1) = "merchant/trans_search.asp?preset=thisyear" : x = x + 1
		aryMenu(x,0) = "+All Transactions" : aryMenu(x,1) = "merchant/trans_search.asp?preset=all" : x = x + 1
	end if
	
	if CheckUserAccess(ACC_ADMIN) then
		aryMenu(x,0) = "System Administration" : x = x + 1
		aryMenu(x,0) = "+Administrative Users" : aryMenu(x,1) = "merchant/user_list.asp" : x = x + 1
		arymenu(x,0) = "+Audit Log": aryMenu(x,1) = "merchant/audit_list.asp": x = x + 1
	end if
	
else
	aryMenu(x,0) = "*Login": aryMenu(x,1) = "login.asp": x = x + 1
end if

PageTitle = "Administration Website"
PageType = "sidebar"

' --------------- main ---------------

	'DrawHeader PageTitle, PageType
	DrawPage
	'DrawFooter PageType
	response.end

sub DrawPage
	dim c, intThisItem, s, tfDisable, tfInsideBlock
	tfInsideBlock = false
	%>
	<style>
	page {font-size:14pt;}
	</style>
	<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0>
	<%
	intThisItem = 0
	for c = 0 to x - 1
		if left(aryMenu(c,0),1) = "*" then
			' headings with no sub-items
			if tfInsideBlock then
				response.write "</TD></TR></TABLE></TD></TR>" & vbCRLF
				tfInsideBlock = false
			end if
			response.write "<TR><TD><IMG SRC=""" & imagebase & "bullet.gif"" WIDTH=""7"" HEIGHT=""7"" ALT="""" BORDER=""0""></TD>" & vbCRLF
			response.write "<TD>"
			s = aryMenu(c,1)
			tfDisable = (left(s,1) = "!")
			if tfDisable then
				s = mid(s,2)
				response.write "<FONT COLOR=""#AAAAAA"">"
			else
				response.write " <A HREF=""" & s & """ TARGET=""swgc5_main"">" & fontx(2,2,cDkBlue)
			end if
			response.write " <B>" & mid(aryMenu(c,0),2) & "</B>"
			response.write "</FONT>"
			if not tfDisable then
				response.write "</A>"
			end if
			response.write "</B>"
			response.write "</TD></TR>" & vbCRLF
			response.write "<TR><TD></TD><TD>" & spacer(1,1) & "</TD></TR>"
		elseif left(aryMenu(c,0),1) <> "+" then
			' headings with subitems
			if tfInsideBlock then
				response.write "</TD></TR></TABLE></TD></TR>" & vbCRLF
				tfInsideBlock = false
			end if
			response.write "<TR><TD>"
			intThisItem = c
			if intExpandItem = intThisItem or TF_SIDEBAR_EXPAND_ALL then
				response.write "<IMG SRC=""" & imagebase & "arrow_down.gif"" WIDTH=""7"" HEIGHT=""7"" ALT="""" BORDER=""0"">"
			else
				response.write "<IMG SRC=""" & imagebase & "arrow.gif"" WIDTH=""7"" HEIGHT=""7"" ALT="""" BORDER=""0"">"
			end if
			response.write "</TD>" & vbCRLF
			response.write "<TD>"
			'response.write "<BR>"
			if intThisItem = intExpandItem then
'				response.write ">"
			else
'				response.write "+"
			end if
			s = aryMenu(c,0)
			tfDisable = (left(s,1) = "!")
			if tfDisable then
				s = mid(s,2)
				response.write fontx(1,1,cDisabled)
			else
				if intThisItem = intExpandItem then
					response.write fontx(2,2,cRed)
				else
					response.write " <A HREF=""sidebar.asp?select=" & c & """>"
					response.write fontx(2,2,cDkBlue)
				end if
			end if
			response.write " <B>" & s & "</B>"
			if tfDisable then
				response.write "</FONT>"
			else
				response.write "</FONT></A>"
			end if
			response.write "</TD></TR>" & vbCRLF
			response.write "<TR><TD></TD><TD><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0><TR><TD>" & font(2)
			tfInsideBlock = true
			' ready for subitems
		else
			' subitems
			if intExpandItem = intThisItem or TF_SIDEBAR_EXPAND_ALL then
				'response.write "<TR><TD><IMG SRC=""" & imagebase & "arrow_down.gif"" WIDTH=""7"" HEIGHT=""7"" ALT="""" BORDER=""0""></TD>"
				'response.write "<TD>"
				s = aryMenu(c,1)
				tfDisable = (left(s,1) = "!")
				if tfDisable then
					s = mid(s,2)
					response.write fontx(1,1,cDisabled)
				else
					response.write "<A HREF=""" & s & """ TARGET=""swgc5_main"">" & fontx(2,2,cDkBlue)
				end if
				response.write mid(aryMenu(c,0),2)
				if tfDisable then
					response.write "</FONT>"
				else
					response.write "</FONT></A>"
				end if
				response.write "<BR>"
				'response.write "</TD></TR>" & vbCRLF
			end if
		end if
	next
	if tfInsideBlock then
		response.write "</TD></TR></TABLE></TD></TR>" & vbCRLF
		tfInsideBlock = false
	end if
	%>
	<TR>
		<TD></TD>
		<TD><%= fontx(1,1,cLight) %>
		<%
		if CheckAdminLogin_Sideframe() then
			%>
			&nbsp;<BR>
			<%
			if TF_SIDEBAR_EXPAND_ALL then
				%>
				<A HREF="sidebar.asp?expandall=0">Contract All Menus</A><BR>
				<%
			else
				%>
				<A HREF="sidebar.asp?expandall=-1">Expand All Menus</A><BR>
				<%
			end if
		end if
		%>
		
		</TD>
	</TR>
	</TABLE>	
</FONT>

</BODY>
</HTML>

<%
end sub
%>
