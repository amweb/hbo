<!--#include file="config/incInit.asp"-->

<%
OpenConn

if Session(SESSION_MERCHANT_UID)&"" = "" then
	response.redirect virtualbase & "store/dashboard.asp"
end if

DrawHeader "", ""

'do update statements here, but in profile_manager.asp change to insert imemediately when creating new profile.

dim strProfileName, intProfileID
if request.form("intProfileID") = "" then
	'check for empty string in profilename
	strProfileName = ProtectSQL(request.form("profilename"))
	CreateProfile strProfileName
else
	DrawUpdateProfile cint(ProtectSQL(request.form("intProfileID")))
end if

'update submitted profile
if request.form("updatefixtures") = "true" then
	DrawUpdateTableWithNewValues cint(request.form("intProfileID"))
end if



AddToFooter
DrawFooter ""

sub DrawBreadcrumbs
%>
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">Update Profile</h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<%= virtualbase %>store/dashboard.asp">Home</a>
                            <i class="fa fa-angle-right"></i>
                            <a href="<%= virtualbase %>admin/profile_manager.asp">Profile Manager</a>
                            <i class="fa fa-angle-right"></i>
                            <a href="#">Update profile</a>
                        </li>
                    </ul>
                </div>
            </div>
<%
end sub


sub DrawUpdateTableWithNewValues(argIntStoreID)
	dim strUpdateSQL, keyDctIndex, strCreateNewProfileSQL, strJustCreatedSQL, rsJustCreated, strMapperSQL
	
	if Request.Form("duplicateexistingprofile")="true" then
		'section to create a duplicate entry
		strCreateNewProfileSQL = "INSERT INTO " & STR_TABLE_PROFILES & " ("
		for keyDctIndex=0 to dctFixtures.Count - 1
			strCreateNewProfileSQL = strCreateNewProfileSQL & "[" & dctFixtures.Keys()(keyDctIndex) & "]"
			'& ")=" & ProtectSQL(Request.Form(dctFixtures.Keys()(keyDctIndex)))
			if keyDctIndex <> dctFixtures.Count - 1 then
				strCreateNewProfileSQL = strCreateNewProfileSQL & ","
			end if
		Next
		keyDctIndex = 0
		strCreateNewProfileSQL = strCreateNewProfileSQL & ", [vchProfileName], [chrStatus]) VALUES ("
		for keyDctIndex=0 to dctFixtures.Count - 1
			strCreateNewProfileSQL = strCreateNewProfileSQL & ProtectSQL(Request.Form(dctFixtures.Keys()(keyDctIndex)))
			if keyDctIndex <> dctFixtures.Count - 1 then
				strCreateNewProfileSQL = strCreateNewProfileSQL & ","
			end if
		Next
		strCreateNewProfileSQL = strCreateNewProfileSQL & ", '" & ProtectSQL(Request.Form("vchProfileName")) & "', 'I')"
		gobjConn.execute(strCreateNewProfileSQL)
		'get just created profile id
		strJustCreatedSQL = "SELECT [intID] FROM " & STR_TABLE_PROFILES & " WHERE [vchProfileName]='" & ProtectSQL(Request.Form("vchProfileName")) & "'"
		set rsJustCreated = gobjConn.execute(strJustCreatedSQL)
		'Set stores from parent ID to child ID
		'strMapperSQL = "SELECT [intStoreShopperID] FROM " & STR_TABLE_PROFILEMAPPER & " WHERE [intProfileID]=" & ProtectSQL(Request.Form("intParentProfileID"))
		strMapperSQL = "INSERT INTO " & STR_TABLE_PROFILEMAPPER & " ([intStoreShopperID], [intProfileID]) SELECT [intStoreShopperID], '"
		strMapperSQL = strMapperSQL & rsJustCreated(0) & "' FROM " & STR_TABLE_PROFILEMAPPER & " WHERE [intProfileID]=" & ProtectSQL(Request.Form("intParentProfileID"))
		gobjConn.execute(strMapperSQL)
		'forward to just created profile
		'response.write "Create new profile SQL: " & strCreateNewProfileSQL & "<br>"
		'response.write "Just created SQL: " & strJustCreatedSQL & "<br>"
		'response.write "Duplicate mapper SQL: " & strMapperSQL & "<br>"
		'response.end
		response.redirect(virtualbase & "admin/profile_assignment.asp?intProfileID=" & rsJustCreated(0))
	else
		'Update an existing profile
		strUpdateSQL = "UPDATE " & STR_TABLE_PROFILES & " SET "
		for keyDctIndex=0 to dctFixtures.Count - 1
			strUpdateSQL = strUpdateSQL & "[" & dctFixtures.Keys()(keyDctIndex) & "]=" & ProtectSQL(Request.Form(dctFixtures.Keys()(keyDctIndex)))
			if keyDctIndex <> dctFixtures.Count - 1 then
				strUpdateSQL = strUpdateSQL & ","
			end if
		Next
		keyDctIndex = 0
		strUpdateSQL = strUpdateSQL & ", [vchProfileName]='" & ProtectSQL(Request.Form("vchProfileName")) & "'"
		strUpdateSQL = strUpdateSQL & " WHERE [intID]=" & argIntStoreID
		gobjConn.execute(strUpdateSQL)
		'now just forward to the assignment page with the appropriate intID
		response.redirect(virtualbase & "admin/profile_assignment.asp?intProfileID=" & argIntStoreID)
		'response.write "Update SQL: " & strUpdateSQL & "<br>"
		'response.end
	end if
end Sub


sub CreateProfile(argProfileName)
	dim strSQL, strInsertSQL, rsStoreCount, rsIntAfterInsert
	'dim strProfileName
	dim dctFixturesIndex, keyDctIndex

	'DrawBreadcrumbs
	strSQL = "SELECT COUNT([intID]) AS storecount FROM " & STR_TABLE_PROFILES & " WHERE [vchProfileName]='" & argProfileName & "' AND [chrStatus]='A'"
	rsStoreCount = gobjConn.execute(strSQL)
	'strSQL = ""
	if cint(rsStoreCount("storecount"))>0 then
	%>
	<div class="page-content-wrapper">
        <!-- BEGIN PAGE HEADER-->
		<div class="page-content">
			<%
			DrawBreadcrumbs
			%>
					<div id="profilepopout">
						<p>Error: Profile <%= argProfileName %> already exists.</p>
						<p><a href="<%= virtualbase %>admin/profile_manager.asp">Return to the profile manager page</a>.</p>
					</div>
		</div>
	</div>
	<%
	else
		strInsertSQL = "INSERT INTO " & STR_TABLE_PROFILES & " ([vchProfileName], [chrStatus]) VALUES ('" & argProfileName & "', 'I')"
		gobjConn.execute(strInsertSQL)
		strInsertSQL = "SELECT * FROM " & STR_TABLE_PROFILES & " WHERE [vchProfileName]='" & argProfileName & "'"
		rsIntAfterInsert = gobjConn.execute(strInsertSQL)
				%>
		<div class="page-content-wrapper">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-content">
			<%
			DrawBreadcrumbs
			%>
				<form action="<%= virtualbase %>admin/profile_create.asp" method="post" id="creatednewprofile" class="formautosubmit">
					<%
					if request.form("createprofile")&""<>"" then
					%>
					<input type="hidden" name="createprofile" value="true">
					<%
					end if
					%>
					<input name="intProfileID" value="<%= rsIntAfterInsert("intID") %>" type="hidden">
					<button type="submit" value="Continue">Continue</button>
				</form>
			</div>
		</div>
<%
	end if
end sub

sub DrawUpdateProfile(argIntStoreID)
	dim strGetStoreInfoSQL, rsGetStoreInfo, strUpdateSQL, keyDctIndex, intParentProfileID
	strGetStoreInfoSQL = "SELECT * FROM " & STR_TABLE_PROFILES & " WHERE [intID]=" & argIntStoreID
	rsGetStoreInfo = gobjConn.execute(strGetStoreInfoSQL)
	'Update copy in header appropriately
	dim blnDuplicateProfile, blnCreateNewProfile, strProfileHeaderCopy, strMaxFixturesCopy
	strMaxFixturesCopy = ""
	strProfileHeaderCopy = ""
	blnDuplicateProfile = false
	if request.form("duplicateprofile")&""<>"" then
		strProfileHeaderCopy = "Duplicate profile "
		strMaxFixturesCopy = "Set "
		blnDuplicateProfile = true
	end if
	if request.form("createprofile")&""<>"" then
		strProfileHeaderCopy = "Create profile "
		strMaxFixturesCopy = "Set "
	end if
	if request.form("editexistingprofile")&""<>"" then
		strProfileHeaderCopy = "Update profile "
		strMaxFixturesCopy = "Update "
	end if
%>
		<div class="page-content-wrapper">
			<!-- BEGIN PAGE HEADER-->
			<div class="page-content">
			<%
			DrawBreadcrumbs
			%>
				<div id="profilepopout" data-dialog-minwidth="620">
					<h3><%= strProfileHeaderCopy %> <%= rsGetStoreInfo("vchProfileName") %></h3>
					<h4><%= strMaxFixturesCopy %> maximum number of fixtures</h4>
					<form action="<%= virtualbase %>admin/profile_create.asp" method="post" id="updateprofile">
						<input type="hidden" name="updatefixtures" value="true">
						<fieldset>
							<%
							if blnDuplicateProfile	then
							%>
							<input type="hidden" name="duplicateexistingprofile" value="true">
							<legend>
								<label for="vchProfileName">Duplicate profile number <%= argIntStoreID&"" %>: </label>
								<%
									if isNumeric(argIntStoreID) then
										intParentProfileID = ProtectSQL(argIntStoreID&"")
									end if
								%>
								<input name="vchProfileName" id="vchProfileName" value="<%= rsGetStoreInfo("vchProfileName") %>_copy" type="text" placeholder="Profile name">
							</legend>
							<input name="intParentProfileID" id="intParentProfileID" value="<%= intParentProfileID %>" type="hidden">
							<%
							else
							%>
							<legend>
								<label for="vchProfileName"><%= strMaxFixturesCopy %> name for profile number <%= argIntStoreID&"" %>: </label>
								<input name="vchProfileName" id="vchProfileName" value="<%= rsGetStoreInfo("vchProfileName") %>" type="text" placeholder="Profile name">
							</legend>
							<%
							end if
							%>
							<br>
							<p class="profilecreateerror">
								<i class="fa fa-exclamation-triangle"></i>
								A duplicate profile has been found.
							</p>
							<input name="intProfileID" value="<%= argIntStoreID %>" type="hidden">
							<div class="newprofilecontainer">
							<%
							for keyDctIndex=0 to dctFixtures.Count - 1
								Response.write "<span class='updateprofileitem'>"
								Response.Write "<label for='" & dctFixtures.Keys()(keyDctIndex) & "'>" & dctFixtures.Items()(keyDctIndex) & "</label>"
								Response.Write "<input name='" & dctFixtures.Keys()(keyDctIndex) & "'" & "id='" & dctFixtures.Keys()(keyDctIndex) & "'" & "value='" & iif(rsGetStoreInfo(dctFixtures.Keys()(keyDctIndex))&""="", 0, rsGetStoreInfo(dctFixtures.Keys()(keyDctIndex))) & "' min='0' type='number' size='3' maxlength='3'>"
								Response.write "</span>"
								if keyDctIndex = 11 then
									Response.Write "</div>"
									Response.Write "<div class='newprofilecontainer'>"
								end if
							Next
							keyDctIndex = 0
							%>
							</div>
						</fieldset>
						<div style="clear: both;">
							<button type="submit" value="Submit">Continue</button>
							<button type="reset" value="Cancel" class="redirecttodashboard">Cancel</button>
						</div>
					</form>
				</div>
			</div>
		</div>


<%
end sub

sub AddToFooter
%>
		<script type="text/javascript">
			var url = "<%= virtualbase %>/config/ajax_functions_profilebuilder.asp";
		</script>
		<script src="<%= virtualbase %>assets/admin/layout/scripts/profile-builder-ajax.js" type="text/javascript"></script>
<%
end sub
%>