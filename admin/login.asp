<%@ LANGUAGE="VBScript" %>
<!--#include file="config/incInit.asp"-->
<%

' =====================================================================================
' = File: login.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   GoCart Merchant Admin Login page
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

OpenConn
	
if Session(SESSION_MERCHANT_UID) <> "" then
	MakeAuditRecord "S", "logout", lcase(Session(SESSION_MERCHANT_ULOGIN)), "User: " & Session(SESSION_MERCHANT_ULOGIN)
	Session(SESSION_MERCHANT_UID) = ""
	Session(SESSION_MERCHANT_ULOGIN) = ""
	Session(SESSION_MERCHANT_UNAME) = ""
	Session(SESSION_MERCHANT_UTIME) = ""
	Session(SESSION_MERCHANT_BID) = ""
	Session(SESSION_REPORT_START) = ""
	Session(SESSION_REPORT_END) = ""
	Session(SESSION_REPORT_TOP) = ""
end if

dim strErr, tfNeedRefresh
strErr = ""

if Request("action") = "login" then
	dim strUser, strPass, intAuth
	strUser = Request("strUsername")
	strPass = Request("strPassword")
	
	' attempt to validate SacWeb user
	
	' attempt to validate client user
	dim strSQL, rsData
	strSQL = "SELECT intID, chrStatus, vchUsername, vchFullName, vchCanViewStatus, vchAccess, intBrand"
	strSQL = strSQL & " FROM " & STR_TABLE_USER
	strSQL = strSQL & " WHERE vchUsername='" & SQLEncode(strUser) & "'"
	strSQL = strSQL & " AND vchPassword='" & SQLEncode(strPass) & "'"
	strSQL = strSQL & " AND chrStatus<>'D'"
	set rsData = gobjConn.execute(strSQL)
	if rsData.eof then
		rsData.close
		set rsData = nothing
		strErr = "Username or password is incorrect. Please try again."
		MakeAuditRecord "S", "loginfail", lcase(strUser), "User: " & strUser
	elseif rsData("chrStatus") <> "A" then
		rsData.close
		set rsData = nothing
		strErr = "Account disabled.<BR>Please contact your system administrator."
		MakeAuditRecord "S", "loginfail", lcase(strUser), "(DISABLED) User: " & strUser
	else
		Session(SESSION_MERCHANT_UID) = rsData("intID")
		Session(SESSION_MERCHANT_ULOGIN) = rsData("vchUsername")
		Session(SESSION_MERCHANT_UNAME) = rsData("vchFullName")
		Session(SESSION_MERCHANT_UTIME) = Now()
		Session(SESSION_MERCHANT_ACCESS) = rsData("vchAccess")
		Session(SESSION_MERCHANT_ACCESS_STATUS) = rsData("vchCanViewStatus")
		if isValid(rsData("intBrand")) then
		Session(SESSION_MERCHANT_BID) = rsData("intBrand")
		end if
		rsData.close
		set rsData = nothing
		strSQL = "UPDATE " & STR_TABLE_USER & " SET dtmLastLogin=GETDATE() WHERE intID=" & Session(SESSION_MERCHANT_UID)
		gobjConn.execute(strSQL)
		MakeAuditRecord "S", "login", Session(SESSION_MERCHANT_UID), "User: " & strUser & " (" & Session(SESSION_MERCHANT_UID) & ")"
		response.redirect "main.asp?updateheader=1"
	end if
elseif Request.QueryString("direct").count = 0 then
	' the session may have expired, or the user clicked on the logout button,
	' in any case we need to refresh the entire frameset so that the other
	' frames are guaranteed to be in a "non-logged-in" state
	tfNeedRefresh = true
end if

DrawHeader "Merchant Login", "adminlogin"
DrawPage
DrawFooter "adminlogin"
response.end

sub DrawPage
%>
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
<!--
	// make sure were in the frameset
	if (top.frames.length == 0)
	{
		top.location = 'default.asp';
	}
	else
	{
		top.swgc5_title.location.reload(true);
		top.swgc5_side.location.reload(true);
	}
// -->
</SCRIPT>

<FORM ACTION="login.asp" METHOD="POST" NAME="frm">
<INPUT TYPE="hidden" NAME="action" VALUE="login">
&nbsp;<BR>
<img src="<%= imagebase %>arrowclick.gif" width="187" height="60" alt="ArrowClick Software" border="0" /><BR><BR>
<%= fontx(1,1,cGrey) %>Copyright �1993-2014 American Web Services, Inc.<BR><BR>
<%= font(5) %>Please Login</FONT><BR><BR>
This is a protected system. All access attempts are logged.<BR>
&nbsp;
<%
	'DrawFormHeader "350", "Please Login", "<B>This is a protected system. All access attempts are logged.</B>"
	
	stlBeginStdTable "350"
	if strErr <> "" then
		stlFormError strErr, 0
	end if
	response.write "<TR><TD>"
	
	stlBeginFormSection "100%", 2
	stlTextInput "strUsername", 20, 20, Request, "\Username", "", ""
	stlPasswordInput "strPassword", 20, 20, "", "\Password", "", ""
	stlEndFormSection
	
	response.write "</TD></TR>"
	
	stlSubmit "Login"
	
	stlEndStdTable
	response.write "</FORM>"
	
	SetFieldFocus "frm", "strUsername"
end sub
%>