
<%@ LANGUAGE="VBScript" %>
<!--#include file="config/incInit.asp"-->
<%

' =====================================================================================
' = File: default.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Admin Frameset
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================
OpenConn
response.expires = -1
Dim vchPartNumber, vchProgram, vchItemName, txtDescription, mnyUnitPrice,intQuantity, vchValidStates, vchBundleQuantity, vchExpireDate
Dim rsOrders,rsData,  intCounter, mnyTotal, mnySubtotal, intParentID
Dim strFixture, strSubFixture, strImageUrl                    
dim strMainURL, intFixtureId, intSubFixtureId,intCampaignID
Dim strSearch, strStoreSearch, Item, fieldName, fieldValue, intInvID, intOrderId
dim dctSaveList, strSKU, intShopperID, intShipShopperID
Dim vchSize,vchSubstrate,vchNumSides, intQty, intMassQty, vchColorProcess
dim strCampaignName, strCampaign
dim intStoreID, vchFormat, intArea, vchRegion, intDistrict, intOther
dim blnCreateCampaignError, rsCampaignDuplicateCount
dim error

if Session(SESSION_MERCHANT_UNAME)&""="" then 
        Response.Redirect "../default.asp"
end if

intCampaignID = Request("campaignid")&""

strSearch = Request("Search")
strStoreSearch = Request("store_search")

if IsNumeric(intCampaignID) and intCampaignID<>"" then
    intCampaignID = clng(intCampaignID)
else
    intCampaignID = 0
end if

intFixtureId = Request("fixtureid")&""
if IsNumeric(intFixtureId) and intFixtureId<>"" then
    intFixtureId = clng(intFixtureId)
else
    intFixtureId = 0
end if

intSubFixtureId = Request("subfixtureid")&""
if IsNumeric(intSubFixtureId) and intSubFixtureId<>"" then
    intSubFixtureId = clng(intSubFixtureId)
else
    intSubFixtureId = 0
end if

intInvID = Request("intInvID")
if IsNumeric(intInvID) and intInvID<>"" then
    intInvID = clng(intInvID)
else
    intInvID = 0
end if

intStoreID = Request("storeid")
if intStoreID<>"" then
    intStoreID = intStoreID
else
    intStoreID = 0
end if

intArea = Request("area")
if IsNumeric(intArea) and intArea<>"" then
    intArea = clng(intArea)
else
    intArea = 0
end if

intQty = Request("intQty")
if IsNumeric(intQty) and intQty<>"" then
    intQty = clng(intQty)
else
    intQty = 0
end if

vchRegion = Request("region")
if vchRegion<>"" then
    vchRegion = vchRegion
else
    vchRegion = ""
end if

intDistrict = Request("district")
if intDistrict<>"" then
    intDistrict = intDistrict
else
    intDistrict = ""
end if

intOther = Request("other")
if IsNumeric(intOther) and intOther<>"" then
    intOther = clng(intOther)
else
    intOther = 0
end if

intDistrict = Request("district")
if intDistrict<>"" then
    intDistrict = intDistrict
else
    intDistrict = ""
end if

vchFormat = Request("format")
if vchFormat<>"" then
    vchFormat = vchFormat
else
    vchFormat = ""
end if

vchItemName = Request("vchItemName")
if vchItemName<>"" then
    vchItemName = vchItemName
else
    vchItemName = ""
end if

dim strAction
strAction = lcase(Request("action"))&""
'response.write strAction
'response.end
select case strAction
	case "delitem": ' delete item from cart
		RemoveLineFromOrder Request("id")
		response.redirect "mcb.asp?campaignid=" & intCampaignID
	case "update": ' update quantities
		UpdateQuantities
		response.redirect "mcb.asp?campaignid=" & intCampaignID
    'case "submit"
    '    if Request("vchItemName") = "" then
    '        error = "Please enter a valid name to continue"
    '    end if
    case "saveorders"
        DrawHeader()
		DrawConfirmation()
		DrawFooter "custserv"
		response.end
    case "confirm"
		SaveOrders
        'Confirm   -moved into saveorders
	case "campaigncreate"
		if Request.Form("vchItemName")="" then
			blnCreateCampaignError = true
		end if
		rsCampaignDuplicateCount = gobjConn.execute("SELECT COUNT([intID]) AS [campaigncount] FROM " & STR_TABLE_INVENTORY & " WHERE [chrType]='C' AND [vchItemName]='" & ProtectSQL(Request.Form("vchItemName")) & "'")
		if cint(rsCampaignDuplicateCount("campaigncount")) > 0 then
			blnCreateCampaignError = true
		end if
		if not blnCreateCampaignError then
			CreateCampaign
			response.redirect "mcb.asp?campaignid=" & intCampaignID
		end if
    case "select"
		if request("fixtureid")&"" = "314" then
			AddHardwareToProfilePackage
		else
			CreateInventoryItem
			AddToProfilePackage
		end if		
end select

sub AddToProfilePackage
	dim dctProfiles, strInsertSQL, intProfileID, intCount, key
	
	set dctProfiles = Server.CreateObject("Scripting.Dictionary")
	
	for each Item in Request.Form
		
		fieldName = Item
		fieldValue = Request.Form(fieldName)
        if Left(fieldName,8)="profile-" then
			if fieldValue = "true" then
				dctProfiles.Add fieldName, fieldValue
			end if
        end if
    Next
	
	strInsertSQL = "insert into " & STR_TABLE_PROFILE_PACKAGES & " (intCampaignID, intProfileID, intItemID) values "
	intCount = 0 
	for each key in dctProfiles.Keys
		intCount = intCount + 1
		intProfileID = CLNG(Replace(key,"profile-",""))
		strInsertSQL = strInsertSQL & "(" & intCampaignId & "," & intProfileID & ", " & intInvID & ")"
		if intCount <> dctProfiles.Count then
			strInsertSQL = strInsertSQL & ","
		end if
	next
	gobjConn.execute(strInsertSQL)
	%><script>parent.location = 'mcb.asp?campaignid=<%=intCampaignId %>';</script><%
end sub

sub AddHardwareToProfilePackage
	dim dctProfiles, strInsertSQL, intProfileID, intCount, key
	
	set dctProfiles = Server.CreateObject("Scripting.Dictionary")
	
	for each Item in Request.Form
		
		fieldName = Item
		fieldValue = Request.Form(fieldName)
        if Left(fieldName,8)="profile-" then
			if fieldValue = "true" then
				dctProfiles.Add fieldName, fieldValue
			end if
        end if
    Next
	
	strInsertSQL = "insert into " & STR_TABLE_PROFILE_PACKAGES & " (intCampaignID, intProfileID, intItemID, intQty) values "
	intCount = 0 
	for each key in dctProfiles.Keys
		intCount = intCount + 1
		intProfileID = CLNG(Replace(key,"profile-",""))
		strInsertSQL = strInsertSQL & "(" & intCampaignId & "," & intProfileID & ", " & intInvID & ", " & intQty & ")"
		if intCount <> dctProfiles.Count then
			strInsertSQL = strInsertSQL & ","
		end if
	next
	gobjConn.execute(strInsertSQL)
	%><script>parent.location = 'mcb.asp?campaignid=<%=intCampaignId %>';</script><%
end sub

sub CreateInventoryItem
	dim arrFixtureInitials, strFixtureInitials, strCampaignName, vchImage, x
    strSKU = ""
	strFixtureInitials = ""

    strSQL = "SELECT vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intCampaignID
    'response.write strSQL
	set rsTemp = gobjConn.execute(strSQL)
    strCampaignName =  rsTemp(0) & ""

    strSQL = "SELECT vchItemName, vchImageURL FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intFixtureId
    set rsTemp = gobjConn.execute(strSQL)
	'response.write strSQL
	vchImage = rsTemp("vchImageURL")
	arrFixtureInitials = split(rsTemp("vchItemName")&"", " ")
	
	For Each x In arrFixtureInitials
		strFixtureInitials = strFixtureInitials & left(x,1)
	Next
	
    strSKU = strFixtureInitials & "_" & vchItemName & "_" & strCampaignName
	'response.write "<br />" &strSKU
	
	strSQL = "SELECT intID, vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE vchPartNumber='" & strSKU & "'"

    set rsTemp = gobjConn.execute(strSQL)
	
	'response.end
	if rsTemp.EOF then
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "*@dtmCreated", "GETDATE()"
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "*vchCreatedByUser", Session(SESSION_MERCHANT_ULOGIN)
		dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
		dctSaveList.Add "*vchCreatedByIP", gstrUserIP
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "chrType", "I"
		dctSaveList.Add "*chrStatus", "O"
		dctSaveList.Add "*#intParentID", intFixtureId
		dctSaveList.Add "*#intSortOrder", 0
		dctSaveList.Add "*#intHitCount", 0
		dctSaveList.Add "vchItemName", vchItemName
		dctSaveList.Add "vchSize", Request("vchSize")
		dctSaveList.Add "vchNumSides", Request("vchNumSides")
		dctSaveList.Add "vchColorProcess", Request("vchColorProcess")
		dctSaveList.Add "vchSubstrate", Request("vchSubstrate")
		dctSaveList.Add "vchPartNumber", strSKU
		'dctSaveList.Add "vchImageURL", vchImage
		dctSaveList.Add "#mnyItemPrice", "0"
		dctSaveList.Add "chrICFlag", "N"
		dctSaveList.Add "chrTaxFlag", "N"
		dctSaveList.Add "chrSoftFlag", "N"
		dctSaveList.Add "intCampaign", intCampaignID
		dctSaveList.Add "!#intStock", "0"
		dctSaveList.Add "!txtDescription", ""
		intInvID = SaveDataRecord(STR_TABLE_INVENTORY, Request, intInvID, dctSaveList)
	else 
		intInvID = rsTemp("intID")
	end if
end sub

sub AddItemsToProfile
	'disabled to allow custom values for items but use field to populate all others
    'intMassQty = Request("mass_qty")

    'if IsNumeric(intMassQty) and intMassQty<>"" then
    '    intMassQty = clng(intMassQty)
    'else
    '    intMassQty = 0
    'end if

	' mred: optimizing queries outside the loop
	dim dctOrders, dctShoppers
    set dctOrders = Server.CreateObject("Scripting.Dictionary")
    set dctShoppers = Server.CreateObject("Scripting.Dictionary")
	
	strSQL = "SELECT intBillShopperID, intID FROM " & STR_TABLE_ORDER & " WHERE chrStatus='0' AND intCampaignID = " & intCampaignID & ""
	set rsTemp = gobjConn.execute(strSQL)
	while not rsTemp.EOF
		if not dctOrders.Exists(rsTemp(0)&"") Then dctOrders.Add rsTemp(0)&"",rsTemp(1)&""
		rsTemp.MoveNext
	wend
	
	'strSQL = "SELECT intID, intShopperID FROM " & STR_TABLE_SHOPPER
	'set rsTemp = gobjConn.execute(strSQL)
	'while not rsTemp.EOF
	'	if not dctShoppers.Exists(rsTemp(0)&"") Then dctShoppers.Add rsTemp(0)&"",rsTemp(1)&""
	'	rsTemp.MoveNext
	'wend
	
	Set rsTemp = Nothing
	
    For Each Item In Request.Form
        fieldName = Item

        if Left(fieldName,4)="QTY_" then
            fieldValue = Request.Form(fieldName)
            
            'aleach - only continue if the store in question has quantity to add, otherwise we are creating empty orders
            if (fieldValue&""<>"" and IsNumeric(fieldValue)) OR intMassQty > 0 then
                
                intShipShopperID = CLNG(Replace(fieldName,"QTY_",""))

                'strSQL = "SELECT *  FROM " & STR_TABLE_ORDER & " WHERE intBillShopperId=" & intShipShopperID & " AND chrStatus='0' AND intCampaignID = " & intCampaignID & ""
	            'set rsTemp = gobjConn.execute(strSQL)
                'if not rsTemp.Eof Then
                '    intOrderId = rsTemp("intId")
                'else
                '    CreateNewOrder
                '    intOrderId = gintOrderID
                'End If
				
				If dctOrders.Exists(intShipShopperID&"") Then
					intOrderId = dctOrders(intShipShopperID&"")
				Else
                    CreateNewOrder
                    intOrderId = gintOrderID
				End If
            
                'strSQL = "SELECT intShopperID FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & intShipShopperID
	            'set rsTemp = gobjConn.execute(strSQL)
                'strSQL = "UPDATE " & STR_TABLE_ORDER & " SET intShopperID=" & rsTemp(0) & ", intBillShopperId=" & intShipShopperID & ", intShipShopperId=" & intShipShopperID & ", intCampaignID=" & intCampaignId & " WHERE intID=" & intOrderId
				'strSQL = "UPDATE " & STR_TABLE_ORDER & " SET intShopperID=" & dctShoppers(intShipShopperID&"") & ", intBillShopperId=" & intShipShopperID & ", intShipShopperId=" & intShipShopperID & ", intCampaignID=" & intCampaignId & " WHERE intID=" & intOrderId
				strSQL = "UPDATE " & STR_TABLE_ORDER & " SET intShopperID=(SELECT intShopperID FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & intShipShopperID & "), intBillShopperId=" & intShipShopperID & ", intShipShopperId=" & intShipShopperID & ", intCampaignID=" & intCampaignId & " WHERE intID=" & intOrderId
	            call gobjConn.execute(strSQL)

                'if intMassQty > 0 then
                '    AddItemToOrder_Other intOrderId, intInvID, intMassQty  
                'else
                    intQTY = CLNG(fieldValue)
                    AddItemToOrder_Other intOrderId, intInvID, intQTY  
                'end if
                             
            end if            
        end if
    Next

    %> <script>parent.location = 'mcb.asp?campaignid=<%=intCampaignId %>';</script> <%
end sub

if Request("show_address") = "true" then
%>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js" id="mcbaddfixturetoprofilepopout">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/global/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/global/plugins/bootstrap/css/sticky-footer.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>/assets/global/plugins/bootstrap-multiselect/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css"/>

<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="<%= virtualbase %>assets/global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/admin/pages/css/mcb.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="<%= virtualbase %>assets/global/css/components.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/admin/layout/css/themes/light2.css" rel="stylesheet" type="text/css" />
<link href="<%= virtualbase %>assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%= virtualbase %>assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<%= virtualbase %>assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<link href="<%= virtualbase %>drcpod/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
<script src="<%= virtualbase %>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js" type="text/javascript"></script>

</head>
<body>
<script>
	$(document).ready(function(){
		$('#mass_qty').keyup(function(e) {
			var massValue;
			massValue = $(this).val();
			$(".indivQTY").val(massValue);
		});
		$("input[name='selectAll']").change(function(){
			$("input[type='checkbox']").prop('checked', $(this).prop("checked"));
		});
	});
</script>
<form method="post" action="<%= virtualbase %>admin/mcb.asp?show_address=true&intInvID=<%=intInvID %>&campaignid=<%= intCampaignId %>&fixtureid=<%=intFixtureId%>&vchItemName=<%=vchItemName%>&intQty=<%=intQty%>">
<%
	'dim vchSize, vchSubstrate
	dim intNumSides
	if request("vchNumSides")&"" <> "" then
		intNumSides = cint(request("vchNumSides"))
	end if
	if request("vchSize")&"" <> "" then
		vchSize = request("vchSize")
	end if
	if request("vchSubstrate")&"" <> "" then
		vchSubstrate = ProtectSQL(request("vchSubstrate"))
	end if
	if request("vchColorProcess")&"" <> "" then
		vchColorProcess = request("vchColorProcess")
	end if
%>
<input type="hidden" name="show_address" value="true" />
<input type="hidden" name="fixtureid" value="<%=intFixtureId%>" />
<input type="hidden" name="vchItemName" value="<%=vchItemName%>" />
<input type="hidden" name="campaignid" value="<%= intCampaignId %>" />
<input type="hidden" name="intInvID" value="<%=intInvID %>" />
<input type="hidden" name="vchNumSides" value="<%= intNumSides %>" />
<input type="hidden" name="vchSize" value="<%= vchSize %>" />
<input type="hidden" name="vchSubstrate" value="<%= vchSubstrate %>" />
<input type="hidden" name="vchColorProcess" value="<%= vchColorProcess %>" >
<input type="hidden" name="intQty" value="<%= intQty %>" />
<input type="hidden" name="filtershowprofiles" value="true" />
<div class="fancybox">
	<style type="text/css">
	.btn-group {
		margin-bottom: 20px;
	}
	</style>
	<h3>Select Profiles</h3>
	<select class="multiselect" name="storeid" id="storenumber" multiple="multiple" onchange="this.form.submit()">
	<%
	'strSQL = "SELECT intID, vchFirstName + ' ' + vchLastName as vchName FROM " & STR_TABLE_SHOPPER & " WHERE intShopperID IS NOT NULL AND chrStatus='A' and chrType='A' AND (vchFirstName LIKE '%" & strStoreSearch & "%' OR vchLastName LIKE '%" & strStoreSearch & "%') AND intID <> 345 ORDER BY (vchLastName * 1), vchFirstName"
			   
	strSQL = "SELECT intID, intStoreID, vchFirstName, vchLastName FROM " & STR_TABLE_SHOPPER & " WHERE chrType='S'"
	
	
	dim save_sql
	save_sql = strSQL
	
	set rsTemp = gobjConn.execute(strSQL)
  
	while not rsTemp.Eof
		%>
		<option value="<%= rsTemp("intID") %>"<%= iif(InStr(intStoreID,rsTemp("intID"))>0," SELECTED","") %>><%=rsTemp("vchFirstName") & " " & rsTemp("vchLastName")%></option>
		<%
		rsTemp.movenext
	wend
	%>
</select>
<!-- make new onchange ajax function-->
 <select class="multiselect" name="district" id="district" onchange="this.form.submit()" multiple="multiple">
	<option value="">District</option>
	<%
	strSQL = "SELECT distinct(intDistrict) FROM " & STR_TABLE_SHOPPER & " WHERE chrStatus='A' and chrType='S' AND intDistrict is not NULL ORDER BY intDistrict"
	'response.write strSQL
	set rsTemp = gobjConn.execute(strSQL)
	while not rsTemp.Eof
		%>
		<option value="<%= rsTemp("intDistrict") %>"<%= iif(InStr(intDistrict,rsTemp("intDistrict"))>0," selected='selected'","") %>><%=rsTemp("intDistrict") %></option>
		<%
		rsTemp.movenext
	wend
	%>
</select>

<select class="multiselect" name="region" id="region" onchange="this.form.submit()" multiple="multiple">
	<option value="">Region</option>
	<%
	strSQL = "SELECT distinct(vchDivision) FROM " & STR_TABLE_SHOPPER & " WHERE chrStatus='A' and chrType='S' AND vchDivision is not NULL ORDER BY vchDivision"
	'response.write strSQL
	set rsTemp = gobjConn.execute(strSQL)
	while not rsTemp.Eof
		%>
		<option value="'<%= rsTemp("vchDivision") %>'"<%= iif(InStr(vchRegion,rsTemp("vchDivision"))>0," SELECTED='selected'","") %>><%=rsTemp("vchDivision") %></option>
		<%
		rsTemp.movenext
	wend
	%>
</select>
<div id="selectprofilessearchcontainer">
	<input type="text" placeholder="Enter Search Details" name="store_search" value="<%= strStoreSearch %>" class="btn-group">
	<button type="submit" value="Search" class="btn-group" id="addfixturetoprofilesubmit">Search</button>
</div>
<div id="mcbadditemtoprofile-wrapper">
	<div id="mcbadditemtoprofile-scroll">
		<table class="CSSTableGenerator" align="left">
			<thead>
				<tr>
					<th>
						<label for="selectAll">Select All</label>
						<br>
						<input type="checkbox" name="selectAll" value="selectAll">
					</th>
					<th>
						Profile Name
					</th>
					<th>
					</th>
					<th>
						View Profile
					</th>
				</tr>
			</thead>
			<tbody>
			<%
				dim dctInvidualRegions, intIndividualRegion
				if request("storeid") = "" and strStoreSearch = "" and request("district") = "" and request("region") = "" then
					strSQL = "Select intID, vchProfileName from " & STR_TABLE_PROFILES & " where chrStatus='A' ORDER BY vchProfileName"
				else
					strSQL = "SELECT DISTINCT P.intID as intID, P.vchProfileName as vchProfileName from " & STR_TABLE_PROFILES & " P INNER JOIN " & STR_TABLE_PROFILE_MAPPER  & " PM ON P.intID = PM.intProfileID inner join " & STR_TABLE_SHOPPER & " S on S.intID = PM.intStoreShopperID WHERE S.chrStatus='A' and P.chrStatus='A' AND S.chrType='S'"
					if strStoreSearch&"" <> "" then
						strSQL = strSQL & " AND (vchProfileName LIKE '%" & strStoreSearch & "%' OR S.vchFirstName LIKE '%" & strStoreSearch & "%' OR S.vchLastName LIKE '%" & strStoreSearch & "%' OR S.vchAddress1 LIKE '%" & strStoreSearch & "%' OR S.vchAddress2 LIKE '%" & strStoreSearch & "%' OR S.vchState LIKE '%" & strStoreSearch & "%' OR S.vchCity LIKE '%" & strStoreSearch & "%' OR S.vchZip LIKE '%" & strStoreSearch & "%')"
					end if
					if request("storeid")&""<>"" then
						if Len(request("storeid")) > 1 then
							dim dctIndividualStoreIDs, intIndividualStoreID
							dctIndividualStoreIDs = Split(ProtectSQL(request("storeid")), ", ")
							for each intIndividualStoreID in dctIndividualStoreIDs
								strSQL = strSQL & " AND S.intID=" & intIndividualStoreID
							next
						else
							strSQL = strSQL & " AND S.intID=" & cint(ProtectSQL(request("storeid")))
						end if
					end if
					if intDistrict&"" <> "" then
						if Len(intDistrict) > 1 then
							dim dctIndividualDistricts, intIndividualDistrict
							dctIndividualDistricts = Split(ProtectSQL(intDistrict), ", ")
							for each intIndividualDistrict in dctIndividualDistricts
								if intIndividualDistrict <> "" then
									strSQL = strSQL & " AND S.intDistrict=" & intIndividualDistrict
								end if
							next
						else
							strSQL = strSQL & " AND S.intDistrict=" & intDistrict
						end if
						'strSQL = strSQL & " AND S.intDistrict in (" & intDistrict & ")"
					end if
					if vchRegion&"" <> "" then
						dctInvidualRegions = Split(vchRegion, ", ")
						for each intIndividualRegion in dctInvidualRegions
							if intIndividualRegion <> "" then
								strSQL = strSQL & " AND S.vchDivision=" & intIndividualRegion
							end if
						next
					end if
					strSQL = strSQL & " ORDER BY P.vchProfileName"
				end if
				set rsTemp = gobjConn.execute(strSQL)
				dim strCheckSQL, rsDenominator, intDenominator, intNumerator, rsNumerator
				dim strFixtureNameSQL, rsFixtureName, strColumnName
				
				
				strFixtureNameSQL = "SELECT [vchItemName] FROM " & STR_TABLE_INVENTORY & " WHERE [intID]=" & intFixtureID
				set rsFixtureName = gobjConn.execute(strFixtureNameSQL)
				strColumnName = Replace(rsFixtureName(0), " ", "")
			%>
			<% while not rsTemp.Eof %>
			<tr>
				<%
					if intFixtureId = 314 then
						intDenominator = 10000000000000
						intNumerator = 0
					else
						strCheckSQL = "SELECT [" & strColumnName & "] FROM " & STR_TABLE_PROFILES & " WHERE [intID]=" & rsTemp("intID")
						set rsDenominator = gobjConn.execute(strCheckSQL)
						intDenominator = rsDenominator(0)
						strCheckSQL = "select (select count(intParentID) from " & STR_TABLE_INVENTORY & " I right join "
						strCheckSQL = strCheckSQL & STR_TABLE_PROFILE_PACKAGES & " PP on PP.intItemID = I.intID where I.intParentID = "
						strCheckSQL = strCheckSQL & intFixtureId & " and PP.intProfileID = " & rsTemp("intID") & " and PP.intCampaignID = " & intCampaignID & " ) as numerator"
						set rsNumerator = gobjConn.execute(strCheckSQL)
						intNumerator = cint(rsNumerator(0))
					end if
				%>
				<td>
					<%
					if intNumerator <> intDenominator then
						if intDenominator=0 then
						%>
						<span style="color: red;">Does not apply to this profile.</span>
						<%
						else
						%>
						<input type="checkbox" name="profile-<%=rsTemp("intID")%>" value="true">
						<%
						end if
						%>
					<%
					else
						if intDenominator=0 then
						%>
						<span style="color: red;">Does not apply to this profile.</span>
						<%
						else
						%>
						<span style="color: red;">At max for this fixture.</span>
						<%
						end if
					end if
					%>
				</td>
				<td colspan="2">
					<%= rsTemp("vchProfileName") %>
				</td>
				<td><a href="#" onclick="parent.loadStoresInProfile(<%=rsTemp("intID")%>); return false;">View Profile</a></td>
			</tr>
			<%  
				rsTemp.MoveNext
				wend
			%>
			</tbody>
		</table>
	</div>
</div>
</div>
<div id="selectprofilesubmit">
	<!-- <button type="submit" value="select">Select</button> -->
	<input type="submit" name="action" value="Select">
	<input type="button" onclick="parent.$('iframe#addresses').dialog('close')" value="Close">
</div>
</form>
    <script>
        $(document).ready(function () {
			$("#storenumber").multiselect({
                selectedText: "# of # selected",
                nonSelectedText: "Store"   
            });
			$("#district").multiselect({
                selectedText: "# of # selected",
                nonSelectedText: "District"
            });
			$("#region").multiselect({
                selectedText: "# of # selected",
                nonSelectedText: "Region"
            });
        });
    </script>
</body>
</html>
<%
response.end
end if

if strSearch&""<>"" then
    
    strSQL = "SELECT intID FROM " & STR_TABLE_INVENTORY & " WHERE chrType='A' AND chrStatus='A' AND vchItemName like '%" & SQLEncode(strSearch) & "%' AND intParentID=(SELECT TOP 1 intID FROM " & STR_TABLE_INVENTORY & " WHERE vchItemName='Fixtures')"
    set rsTemp = gobjConn.execute(strSQL)
    
    if not rsTemp.Eof then
        intFixtureId = CINT(rsTemp(0)&"")
    end if    

    strSQL = "SELECT intID, intParentID FROM " & STR_TABLE_INVENTORY & " WHERE chrType='A' AND chrStatus='A'" 
    strSQL = strSQL & " AND vchItemName like '%" & SQLEncode(strSearch) & "%' AND intParentID in (SELECT intID FROM " & STR_TABLE_INVENTORY & " WHERE intParentID=(SELECT TOP 1 intID FROM " & STR_TABLE_INVENTORY & " WHERE vchItemName='Fixtures'))"
    set rsTemp = gobjConn.execute(strSQL)
    
    if not rsTemp.Eof then
        intFixtureId = CINT(rsTemp(1)&"")
        intSubFixtureId= CINT(rsTemp(0)&"")
    end if 
    'sanity check
    
     strSQL = "SELECT intID FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intSubFixtureId & " AND intParentID=" & intFixtureId
    set rsTemp = gobjConn.execute(strSQL)
    
    if rsTemp.eof then
        intSubFixtureId = 0    
    end if                    

end if

sub SaveOrders
    strSQL = "SELECT *  FROM " & STR_TABLE_ORDER & " WHERE chrStatus in ('0','9', 'S') AND intBillShopperID is not null AND intCampaignID=" & intCampaignId

    'response.write strSQL
	set rsOrders = gobjConn.execute(strSQL)

    While not rsOrders.Eof
        UpdateOrderStatus_Other rsOrders("intID"), "S"
        rsOrders.MoveNext
    Wend
	
	Confirm
    'DrawHeader()
    'DrawConfirmation()
        
    'response.end   
end sub

sub Confirm
    strSQL = "UPDATE " & STR_TABLE_ORDER & " SET dtmSubmitted = GETDATE() WHERE intCampaignID=" & intCampaignId

    'response.write strSQL
	set rsOrders = gobjConn.execute(strSQL)

	dim strMerchantMessage, strSubject
	strMerchantMessage = "A campaign has been confirmed: " & intCampaignID & vbcrlf
	strMerchantMessage = strMerchantMessage & vbcrlf
	strSubject = "Campaign Confirmation #" & intCampaignID & ""
	SendMerchantEmail strSubject, strMerchantMessage

    DrawHeader()
    DrawConfirmation()
    DrawFooter "custserv"
    response.end   
end sub

sub CreateCampaign
    set dctSaveList = Server.CreateObject("Scripting.Dictionary")
    dctSaveList.Add "*@dtmCreated", "GETDATE()"
    dctSaveList.Add "@dtmUpdated", "GETDATE()"
    dctSaveList.Add "*vchCreatedByUser", Session(SESSION_MERCHANT_ULOGIN)
    dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
    dctSaveList.Add "*vchCreatedByIP", gstrUserIP
    dctSaveList.Add "vchUpdatedByIP", gstrUserIP
    dctSaveList.Add "chrType", "C"
    dctSaveList.Add "*chrStatus", "A"
    dctSaveList.Add "*#intParentID", 0
    dctSaveList.Add "*#intSortOrder", 0
    dctSaveList.Add "*#intHitCount", 0
    dctSaveList.Add "!vchItemName", ""
    dctSaveList.Add "#mnyItemPrice", "0"
    dctSaveList.Add "chrICFlag", "N"
    dctSaveList.Add "chrTaxFlag", "N"
    dctSaveList.Add "chrSoftFlag", "N"
    dctSaveList.Add "!vchSpecialDate", ""
    dctSaveList.Add "!#intStock", "0"
    dctSaveList.Add "!txtDescription", ""
    intCampaignID = SaveDataRecord(STR_TABLE_INVENTORY, Request, intCampaignID, dctSaveList)
end sub

sub UpdateQuantities
	dim i, intQty, intItemID, intOrderID
	for each i in Request.Form
		if lcase(left(i, 7)) = "intqty_" then
        Response.Write mid(i,8)
			intOrderID = Split(mid(i,8),"_")(0)
            intItemID = Split(mid(i,8),"_")(1)
			if IsNumeric(intItemID) then
				intItemID = CLng(intItemID)
				intQty = Request.Form(i)
				if IsNumeric(intQty) then
					intQty = CLng(intQty)
				else
					intQty = -1
				end if
				if intQty = 0 then
					' remove item from cart
					RemoveLineFromOrder_Other intOrderID,intItemID
					' also need to remove customized inventory item?
					
					' if order now has no items, delete order
					strSQL = "SELECT intID FROM " & STR_TABLE_LINEITEM & " WHERE intOrderID=" & intOrderID
					set rsTemp = gobjConn.execute(strSQL)
					if rsTemp.eof then
						UpdateOrderStatus_Other intOrderID, "D"
					end if

				elseif intQty > 0 then
					' change qty in cart
					EditOrderItemQty_Other intOrderID,intItemID, intQty
				end if
			end if
		end if
	next
end sub

DrawHeader()
%>

	<% if intCampaignID = 0 then %>

	<div id="createform">
			<% 
			strSQL = "SELECT intID, vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE chrType='C' and chrStatus='A'"
			set rsTemp = gobjConn.execute(strSQL)
			if not rsTemp.eof then
			%>
				<form action="<%= virtualbase %>admin/mcb.asp" method="post" style="text-align:left;padding:20px;">
					<label for="campaignid"><h3>Load existing campaign</h3></label>
					<br>
					<select name="campaignid" id="campaignid" onchange="this.form.submit()">
						<option value="">Select Existing Campaign</option>
						<% 
						strSQL = "SELECT intID, vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE chrType='C' and chrStatus='A'"
						set rsTemp = gobjConn.execute(strSQL)
						while not rsTemp.eof
						%>
						<option value="<%= rsTemp("intID") %>"><%= rsTemp("vchItemName") %></option>
						<%
							rsTemp.MoveNext
						wend
						%>
					</select>
				</form>
				<hr />
			<% end if %>

		<form action="mcb.asp" method="post" id="createcampaign">
			<%
			if blnCreateCampaignError then
			%>
			<p class="error">
				<i class="fa fa-exclamation-triangle" aria-hidden="true"></i> There was an error with your submission. <br>
				<%
				if cint(rsCampaignDuplicateCount("campaigncount")) > 0 then
				%>
				A duplicate campaign was found.<br>
				<%
				end if
				if Request.Form("vchItemName")="" then
				%>
				You must provide a campaign name.<br>
				<% end if %>
				<!-- <i class="fa fa-exclamation-circle" aria-hidden="true" title="Duplicate campaign found"></i> -->
			</p>
			<%
			end if
			%>
			<input type="hidden" name="action" value="CampaignCreate" />
			<fieldset>
				<legend>Create new campaign</legend>
				<label for="vchItemName">Name:</label>
				<input type="text" name="vchItemName">
				<br>
				<label for="vchSpecialDate">Date:</label>
				<input type="date" name="vchSpecialDate">
				<br>
				<div class="col-md-6 col-md-offset-6">
					<button type="submit" class="approve">Create Campaign</button>
					<button type="button" name="backtohome" class="dialogclose">Close</button>
				</div>
			</fieldset>
		</form>
	</div>
	<%
	else
	Dim strSQL, rsTemp
	%>
	<link href="<%= virtualbase %>drcpod/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
	<!-- <link href="<%= virtualbase %>assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.css" type="text/css" /> -->
	<div class="mcbinventoryleft">
		<a href="#" id="changecampaign">Change campaign</a>
		<form action="<%= virtualbase %>admin/mcb.asp" method="post" name="changecampaign">
			<label for="campaignid">Change campaign</label>
			<select name="campaignid" id="campaignid">
				<option value="">Select Existing Campaign</option>
				<% 
				strSQL = "SELECT intID, vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE chrType='C' and chrStatus='A'"
				set rsTemp = gobjConn.execute(strSQL)
				while not rsTemp.eof
					'iif(rsTemp("intID")=intCampaignId, "selected", "")
				%>
				<option value="<%= rsTemp("intID") %>"><%= rsTemp("vchItemName") %></option>
				<%
					rsTemp.MoveNext
				wend
				%>
			</select>
		</form>
		<form action="mcb.asp" class="mcbinventorysearch">
			<input name="campaignid" value="<%=intCampaignID %>" type="hidden" />
			<select name="fixtureid" id="fixtureid" class="multiselect">
				<option value="">Select a fixture</option>
				<%
				strSQL = "SELECT * FROM " & STR_TABLE_INVENTORY & " WHERE chrType='A' AND chrStatus='A' AND intParentID=(SELECT TOP 1 intID FROM "
				strSQL = strSQL & STR_TABLE_INVENTORY & " WHERE vchItemName='Fixtures') ORDER BY vchItemName ASC"
				
				set rsTemp = gobjConn.execute(strSQL)
				While not rsTemp.Eof
				%>
				<option value="<%= rsTemp("intID") %>" <%= iif(intFixtureID = rsTemp("intID"), "selected", "") %>><%= rsTemp("vchItemName") %></option>
				<%
					rsTemp.MoveNext
				Wend
				%>
			</select>
			<input type="text" placeholder="Search" name="search" class="btn-group-vertical">
			<button type="submit" value="Submit" class="btn-group-vertical storesearch">Submit</button>
		</form>
		<div class="row">
			Select a fixture and subfixture from above to start the Sign Builder
		</div>
			<%
			if intFixtureID = 314 then
				dim rsHardwareList
				strSQL = "Select intID, vchItemName, vchPartNumber from " & STR_TABLE_INVENTORY & " WHERE intParentID=" & intFixtureID
				set rsHardwareList = gobjConn.execute(strSQL)
				
				strSQL = "SELECT vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE intId=" & intFixtureID
				set rsTemp = gobjConn.execute(strSQL)
				strFixture = rsTemp("vchItemName")
				
				strSQL = "SELECT vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE intId=" & intCampaignID
				set rsTemp = gobjConn.execute(strSQL)
				strCampaign = rsTemp("vchItemName")
				
				%>
				<div class="">
			<form action="mcb.asp" method="post" class="individualitemaddtoprofile">
				<input type="hidden" name="campaignid" value="<%= intCampaignId %>" />
				<input type="hidden" name="fixtureid" value="<%= intFixtureId %>" />
				<input type="hidden" name="subfixtureid" value="<%= intSubFixtureId %>" />
				<input type="hidden" name="action" value="submit" />
				<img src="<%= strImageUrl %>">
				<div class="individualitemaddtoprofiledetail">
					<strong>Hardware Details</strong>
					<div class="specifications">
						<div>
							<select name="intInvID" id="intInvID">
								<option value="default">Select Hardware</option>
								<% if not rsHardwareList.EOF then
									while not rsHardwareList.EOF
										response.write "<option value=""" & rsHardwareList("intID") & """>"& rsHardwareList("vchItemName") & " - " & rsHardwareList("vchPartNumber") & "</option>"
										rsHardwareList.movenext
									wend
								end if %>
							</select>
							<a href="#" onclick="addNewHardware(); return false;">Add new hardware not on list</a>
						</div>
					</div>
					<div class="quantity">
						<div>
							<input type="number" name="intQty" id="intQty" style="width:60px" placeholder="QTY" min="0" /> per store
						</div>
					</div>
					<div>
						<button type="submit" class="invididualitemaddtocart invididualitemaddtocart-disabledbutton btn-group-vertical">Select Profiles</button>
					</div>
					<%
					if request("action") = "submit" then
						dim strIframeSrcURL
						strIframeSrcURL = "mcb.asp?show_address=true&intInvID=" & intInvID & "&campaignid=" & intCampaignID & "&fixtureid=" & intFixtureId & "&vchItemName=" & vchItemName
						strIframeSrcURL = strIframeSrcURL & "&vchNumSides=" & request.form("vchNumSides") & "&vchSize=" & request.form("vchSize")
						strIframeSrcURL = strIframeSrcURL & "&vchSubstrate=" & request.form("vchSubstrate") & "&vchColorProcess=" & request.form("vchColorProcess") & "&intQty=" & intQty
						%>
						<iframe src="<%= strIframeSrcURL %>" id="addresses" width="100%" height="100%" style="display:none;"></iframe>
					<% end if %>
					
				</div>
			</form>
			<div id="hardwarePopup" style="display:none">
						<h2>Add new Hardware</h2>
						<form id="hardwareForm">
							Hardware Name: <input type="text" name="hardwareName" id="hardwareName" /><br />
							Hardware SKU: <input type="text" name="hardwareSku" id="hardwareID" /><br />
							<button onclick="saveNewHardware(); return false;">Add Hardware</button>
							<button onclick="$( '#hardwarePopup' ).dialog('close'); return false;">Close</button>
						</form>
					</div>
			</div>
			<%
			elseif intFixtureID>0 Then
				strSQL = "SELECT vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE intId=" & intFixtureID
				set rsTemp = gobjConn.execute(strSQL)
				strFixture = rsTemp("vchItemName")
				
				strSQL = "SELECT vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE intId=" & intCampaignID
				set rsTemp = gobjConn.execute(strSQL)
				strCampaign = rsTemp("vchItemName")
				
				strSQL = "SELECT vchItemName,vchImageURL,vchOptionList1,vchOptionList2,vchOptionList3 FROM " & STR_TABLE_INVENTORY & " WHERE intId=" & intFixtureID
				set rsTemp = gobjConn.execute(strSQL)
				
				dim fs
				set fs=Server.CreateObject("Scripting.FileSystemObject")
				strImageUrl = virtualbase & "images/products/" & rsTemp("vchImageURL")
				'if fs.FileExists(strImageUrl) then
				'	strImageUrl = virtualbase & "images/products/" & rsTemp("vchImageURL")
				'else
				'	strImageUrl = "https://placehold.it/300x390"
				'end if
				dim vchO1, vchO2, vchO3, vchO4, o
				vchO1 = rsTemp("vchOptionList1")
				vchO2 = rsTemp("vchOptionList2")
				vchO3 = rsTemp("vchOptionList3")
				'vchO4 = rsTemp("vchOptionList4")
				strSubFixture = rsTemp("vchItemName")
			%>
		<div class="individualfixture">
			<form action="mcb.asp" method="post" class="individualitemaddtoprofile">
				<input type="hidden" name="campaignid" value="<%= intCampaignId %>" />
				<input type="hidden" name="fixtureid" value="<%= intFixtureId %>" />
				<input type="hidden" name="subfixtureid" value="<%= intSubFixtureId %>" />
				<input type="hidden" name="action" value="submit" />
				<img src="<%= strImageUrl %>">
				<div class="individualitemaddtoprofiledetail">
					<strong>Sign Details</strong>
					<p>Fixture: <%= strFixture %></p>
					<p>Campaign: <%=strCampaign %></p>
					<div class="specifications">
						<strong>Select Specifications</strong>
						<div>
							<select name="vchSize" id="vchSize" data-error-type="size">
								<option value="default">Sign Size</option>
								<% 
								For each o in Split(vchO1,",")
								%>
								<option value="<%= o %>"
								<%
								'if InStr(request("vchSize"),o)>0 then
								'if InStr(request("vchSize"),o)<>0 then
								if request("vchSize")=o then
									response.write " SELECTED"
								else
									response.write ""
								end if
								%>
								><%= o %></option>
								<%
								next
								%>
							</select>
						</div>
						<div>
							<select name="vchNumSides" id="vchNumSides" data-error-type="sides" data-fixture-id="<%= intFixtureId %>">
							<option value="default">Number of Sides</option>
							<% 
							For each o in Split(vchO2,",")
							%>
							<option value="<%= o %>"<%= iif(InStr(request("vchNumSides"),o)>0," SELECTED","") %>><%= o %></option>
							<%
							next
							%>
							</select>
						</div>
						<div>
							<select name="vchColorProcess" id="vchColorProcess" data-error-type="colorprocess">
								<option value="default">Color Process</option>
							</select>
						</div>
						<div>
							<select name="vchSubstrate" id="vchSubstrate" data-error-type="substrate">
								<option value="default">Substrate</option>
								<% 
								For each o in Split(vchO3,",")
								%>
								<option value="<%= o %>"<%= iif(InStr(request("vchSubstrate"),o)>0," SELECTED","") %>><%= o %></option>
								<%
								next
								%>
							</select>
						</div>
						<div>
							<input type="text" name="vchItemName" placeholder="Name"  <%= iif(request("vchItemName")&""<>"", "value='" & request("vchItemName") & "'", "") %> class="btn-group-vertical" data-error-type="name">
						</div>
						<div>
							<button type="submit" class="invididualitemaddtocart invididualitemaddtocart-disabledbutton btn-group-vertical" disabled>Select Profiles</button>
						</div>
					</div>
					<%
					if request("action") = "submit" then
					strIframeSrcURL = "mcb.asp?show_address=true&intInvID=" & intInvID & "&campaignid=" & intCampaignID & "&fixtureid=" & intFixtureId & "&vchItemName=" & vchItemName
					strIframeSrcURL = strIframeSrcURL & "&vchNumSides=" & request.form("vchNumSides") & "&vchSize=" & request.form("vchSize")
					strIframeSrcURL = strIframeSrcURL & "&vchSubstrate=" & request.form("vchSubstrate") & "&vchColorProcess=" & request.form("vchColorProcess")
					%>
					<iframe src="<%= strIframeSrcURL %>" id="addresses" width="100%" height="100%" style="display:none;"></iframe>
					<% end if %>
				</div>
			</form>
		</div>
		<% End If %>
	</div>
	<div class="mcbinventoryright"> 
		<form action="mcb-profile.asp">
			<input type="hidden" name="campaignid" value="<%= intCampaignID %>" />
			<button type="submit" class="continuetoconfirmation" value="Continue to Collateral Manager">Continue to Collateral Manager</button>
		</form>
		<div class="accordion"> 
		<%
		strSQL = "SELECT intID,vchProfileName FROM " & STR_TABLE_PROFILES & " WHERE chrStatus='A' and intID in ( select intProfileID from " & STR_TABLE_PROFILE_PACKAGES & " where intCampaignID = " & intCampaignID & " )"
		set rsTemp = gobjConn.execute(strSQL)
		While not rsTemp.Eof
		%>
			<h3 data-profilepackage-intid="<%=rsTemp("intID")%>" data-profilepackage-campaign="<%=intCampaignID%>" data-fixtures-loaded="false">
				<strong>Profile <%= rsTemp("intID")&""%> - <%= rsTemp("vchProfileName")&""%></strong>
			</h3>
			<div id="profile-<%= rsTemp("intID")&""%>-container"></div>
		<%
			rsTemp.MoveNext
		Wend
				%>
		</div>
	</div>
	<%
	end if
	%>
	</div>
</div>
<!-- END CONTAINER -->	
<!-- BEGIN COPYRIGHT -->
<div class="footer copyright">
	<div class="page-footer">
		&copy; <%= Now %> DOME Solutions. All rights reserved. Under license to spdemo
	</div>
</div>

<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<div id="StoreProfileDialogBox" style="display:none">
	<span id="currentProfileName"></span>
	<div class="table-wrapper">
		<div class="table-scroll">
			<table id="currentProfileTable">
			</table>
		</div>
	</div>
	
	<script type="text/javascript">
	var iconurl = "<%= virtualbase %>images/Profile-Icon.svg";
	$("a.layoutzoom").fancybox({
		arrows: false,
		type: 'iframe',
		autoSize: false,
		width: 500,
		height: 425,
		fitToView: false,
		beforeShow : function() {
			var alt = this.element.find('img').attr('alt');
			var url = this.element.attr("href");
			this.inner.find('img').attr('alt', alt);
			var externalurl = "<br><a href='" + url + "' target='_blank'>Open this image in a new tab</a>. <i class='fa fa-external-link' aria-hidden='true'></i>"
			this.title = alt + externalurl;
		}
	});
	</script>
	<input type="button" onclick="$('#StoreProfileDialogBox').dialog('close')" value="Close" />
</div>
<script src="<%= virtualbase %>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<%= virtualbase %>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%= virtualbase %>assets/global/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<%= virtualbase %>assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/admin/pages/scripts/login.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/admin/layout/scripts/profile-loader.js" type="text/javascript"></script>
<script type="text/javascript" src="<%= virtualbase %>assets/admin/pages/scripts/mcb.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
	function RUSure() {
		return confirm('Are you sure you want to remove this item from your order?');
	};
	function scrollToElement(ele) {
		$(window).scrollTop(ele.offset().top).scrollLeft(ele.offset().left);
	}
    jQuery(document).ready(function () {
		
        Metronic.init();
        QuickSidebar.init()
        Login.init();
		
    });
	/*window.load( function() {
		var hash = window.location.hash;
		if (hash != '')
		{
			scrollToElement( $('a #' + hash));
		}
	});*/
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

<%
    sub DrawHeader() %>
        <!DOCTYPE html>
        <!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
        <!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
        <!--[if !IE]><!-->
        <html lang="en" class="no-js">
        <!--<![endif]-->
        <!-- BEGIN HEAD -->
        <head>
        <meta charset="utf-8"/>
        <title></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="<%= virtualbase %>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<%= virtualbase %>assets/global/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<%= virtualbase %>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <link href="<%= virtualbase %>assets/global/plugins/bootstrap/css/sticky-footer.css" rel="stylesheet" type="text/css"/>
        <link href="<%= virtualbase %>/assets/global/plugins/bootstrap-multiselect/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
		<link rel="stylesheet" href="<%= virtualbase %>assets/global/plugins/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
		<link rel="stylesheet" href="<%= virtualbase %>assets/global/plugins/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
        <!-- END PAGE LEVEL PLUGIN STYLES -->
        <!-- BEGIN PAGE STYLES -->
        <link href="<%= virtualbase %>assets/global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
        <link href="<%= virtualbase %>assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
		<link href="<%= virtualbase %>assets/admin/pages/css/mcb.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="<%= virtualbase %>assets/global/css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= virtualbase %>assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<%= virtualbase %>assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= virtualbase %>assets/admin/layout/css/themes/light2.css" rel="stylesheet" type="text/css" />
        <link href="<%= virtualbase %>assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
        <script src="<%= virtualbase %>assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
        <script src="<%= virtualbase %>assets/global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<%= virtualbase %>assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
		<link href="<%= virtualbase %>drcpod/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
        <script src="<%= virtualbase %>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<%= virtualbase %>assets/global/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js" type="text/javascript"></script>
		<script type="text/javascript" src="<%= virtualbase %>assets/global/plugins/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>

        <!-- END THEME STYLES -->
        </head>
        <!-- END HEAD -->
        <!-- BEGIN BODY -->
        <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
        <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
        <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
        <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
        <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
        <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
        <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
        <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
        <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
        <body class="page-header-fixed login">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
	        <!-- BEGIN HEADER INNER -->
	        <div class="page-header-inner">
		        <!-- BEGIN LOGO -->
		        <div class="page-logo">
			<a href="<%= virtualbase%>store/dashboard.asp"><img src="<%= virtualbase %>assets/admin/layout/img/logo.png" alt="logo" class="logo-default" style="float:left"/></a>
		        </div>
                <% if gintUserName&""<>"" then %>
		        <!-- END LOGO -->
		        <!-- BEGIN TOP NAVIGATION MENU -->
		        <div class="top-menu">
			        <ul class="nav navbar-nav pull-right">
				        <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
					        <a href="#">Welcome <strong><%= gintUserName %></strong></a>
				        </li>
				        <li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">
					        <a href="<%= virtualbase %>store/custserv.asp?action=accountinfo"><i class="fa fa-caret-down"></i> My Account</a>
				        </li>
				        <li class="dropdown dropdown-extended dropdown-tasks" id="header_task_bar">
					        <a href="<%= virtualbase %>default.asp"><i class="fa fa-power-off"></i> Logout</a>
				        </li>
			        </ul>
		        </div>
		        <!-- END TOP NAVIGATION MENU -->
               <% elseif Session(SESSION_MERCHANT_ULOGIN)&""<>"" then %>
		        <!-- END LOGO -->
		        <!-- BEGIN TOP NAVIGATION MENU -->
		        <div class="top-menu">
			        <ul class="nav navbar-nav pull-right">
				        <li class="dropdown dropdown-extended dropdown-notification" id="Li1">
					        <a href="#">Welcome <strong><%= Session(SESSION_MERCHANT_ULOGIN)  %></strong></a>
				        </li>
				        <li class="dropdown dropdown-extended dropdown-inbox" id="Li2">
					        <a href="<%= virtualbase %>admin/default.asp"><i class="fa fa-caret-down"></i> My Account</a>
				        </li>
				        <li class="dropdown dropdown-extended dropdown-tasks" id="Li3">
					        <a href="<%= virtualbase %>default.asp"><i class="fa fa-power-off"></i> Logout</a>
				        </li>
			        </ul>
		        </div>
		        <!-- END TOP NAVIGATION MENU -->
                <% end if %>
	        </div>
	        <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <div class="page-content">
            <br />
   		        <div class="row">
			        <div class="col-md-12">
				        <h3 class="page-title">Sign Builder</h3>
				        <ul class="page-breadcrumb breadcrumb">
					        <li>
						        <i class="fa fa-home"></i>
						        <a href="../store/dashboard.asp">Home</a>
						        <i class="fa fa-angle-right"></i>
					        </li>
					        <li>
						        <a href="mcb.asp">Sign Builder</a>                        
						        <i class="fa fa-angle-right"></i>
					        </li>
                            <% if intCampaignID > 0 then 
                                strCampaignName = gobjConn.execute("SELECT vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intCampaignID )(0)                   
                            %>
                            <li>
						        <a href="#"><%= strCampaignName %></a>
					        </li>
                            <% end if %>
				        </ul>
			        </div>
		        </div>


    <% end sub

    sub DrawConfirmation()
        dim hasItems, rsLineItem, rsAggregatedLineItems
        %>
		<form action="mcb.asp">
		The Campaign has now been saved and opened. To edit this campaign, Click on the Edit Campaign button.
		<%
        strSQL = "SELECT C.vchFirstName as [ContactFirstName], C.vchLastName as [ContactLastName], L.* FROM " & STR_TABLE_SHOPPER & " C INNER JOIN " & STR_TABLE_SHOPPER & " L ON L.intShopperID = C.intID  WHERE C.chrStatus='A' AND L.chrType='S'"
        strSQL = strSQL & " ORDER BY C.vchLastName,C.vchFirstName, L.vchLastName, L.vchFirstName"
        'response.write strSQL
        set rsTemp = gobjConn.execute(strSQL)
        %>
        &nbsp;&nbsp;&nbsp;<a href="mcb.asp?campaignid=<%= intCampaignID %>">Edit Campaign</a>
        &nbsp;&nbsp;&nbsp;<a href="../store/dashboard.asp">Home</a>
		&nbsp;&nbsp;&nbsp;
		<% If strAction <> "confirm" then %>
		<input type="hidden" name="campaignid" value="<%= intCampaignID %>" />
		<input type="hidden" name="action" value="Confirm" />
		<input type="submit" value="Confirm" />
		<% Else %>
        <strong>Campaign confirmed</strong>
		<% End If %>
        </form>
        <br /><br />
		
		<script src="../assets/admin/pages/scripts/drag_n_drop.js" type="text/javascript"></script>
		
		<div id="packingSlipArea">
		
			<div id="itemSKUList">
				<center><strong>Fixture Order Overview</strong><br />(unassigned)</center>
				<table id="listArea" name="unassignedSKU">
					<tr>
                        <td><u><b>Quantity</b></u></td>
                        <td><u><b>Sign Description</b></u></td>
                    </tr>
					<% 'insert sku loop here
						strSQL = "SELECT sum(L.intQuantity) as sumQty, L.vchItemName, L.vchPartNumber, G.intPackGroup FROM " & STR_TABLE_ORDER & " O LEFT JOIN " & STR_TABLE_LINEITEM & " L ON O.intID = L.intOrderID Left Join " & STR_TABLE_PACKGROUP & " G on L.vchPartNumber = G.vchPartNumber WHERE L.chrStatus='A' AND O.intCampaignID = " & intCampaignID & " GROUP BY L.intInvID, L.vchItemName, L.vchPartNumber, G.intPackGroup"
    '           		
                        set rsAggregatedLineItems = gobjConn.execute(strSQL)
						while not rsAggregatedLineITems.eof
							if rsAggregatedLineITems("intPackGroup") & "" = "0" or rsAggregatedLineITems("intPackGroup") & "" = "" then
								 response.write "<tr draggable=""true"" class=""item"" data-value=""" & rsAggregatedLineITems("vchPartNumber") & """>"
								 response.write "<td>" & rsAggregatedLineITems("sumQty") & " </td>"
								 response.write "<td>" & rsAggregatedLineITems("vchItemName") & "</td>"
								 response.write "</tr>"   
							end if
							rsAggregatedLineITems.MoveNext()
						wend
                        
                        if rsAggregatedLineITems.eof then
                            rsAggregatedLineITems.MoveFirst()
                        end if
					%>
                </table>
			</div>
			<div id="packingContainer">
				<table id="packSlip1" class="hole" name="packSlip1">
					<tr>
						<td colspan="2"><u><b><center>Pack Group 1</center></b></u></td>
					</tr>
					<tr>
						<td><u><b>Quantity</b></u></td>
						<td><u><b>Sign Description</b></u></td>
					</tr>
					<%
						while not rsAggregatedLineITems.eof
							if rsAggregatedLineITems("intPackGroup") & "" = "1" then
								'response.write "<li draggable=""true"" class=""item"" value=""" & rsAggregatedLineITems("vchPartNumber") & """>" & rsAggregatedLineITems("vchPartNumber") & "</li>"
								 response.write "<tr draggable=""true"" class=""item"" data-value=""" & rsAggregatedLineITems("vchPartNumber") & """>"
								 response.write "<td>" & rsAggregatedLineITems("sumQty") & " </td>"
								 response.write "<td>" & rsAggregatedLineITems("vchItemName") & "</td>"
								 response.write "</tr>"   
							end if
							rsAggregatedLineITems.MoveNext()
						wend
						
                        if rsAggregatedLineITems.eof then
                            rsAggregatedLineITems.MoveFirst()
                        end if
						%>
				</table>
				<table id="packSlip2" class="hole" name="packSlip2">
					<tr>
						<td colspan="2"><u><b><center>Pack Group 2</center></b></u></td>
					</tr>
					<tr>
						<td><u><b>Quantity</b></u></td>
						<td><u><b>Sign Description</b></u></td>
					</tr>
					<%
						while not rsAggregatedLineITems.eof
							if rsAggregatedLineITems("intPackGroup") & "" = "2" then
								'response.write "<li draggable=""true"" class=""item"" value=""" & rsAggregatedLineITems("vchPartNumber") & """>" & rsAggregatedLineITems("vchPartNumber") & "</li>"
								 response.write "<tr draggable=""true"" class=""item"" data-value=""" & rsAggregatedLineITems("vchPartNumber") & """>"
								 response.write "<td>" & rsAggregatedLineITems("sumQty") & " </td>"
								 response.write "<td>" & rsAggregatedLineITems("vchItemName") & "</td>"
								 response.write "</tr>"   
							end if
							rsAggregatedLineITems.MoveNext()
						wend
						if rsAggregatedLineITems.eof then
                            rsAggregatedLineITems.MoveFirst()
                        end if
						%>
				</table>
				<table id="packSlip3" class="hole" name="packSlip3">	
					<tr>
						<td colspan="2"><u><b><center>Pack Group 3</center></b></u></td>
					</tr>
					<tr>
						<td><u><b>Quantity</b></u></td>
						<td><u><b>Sign Description</b></u></td>
					</tr>
					<%
						while not rsAggregatedLineITems.eof
							if rsAggregatedLineITems("intPackGroup") & "" = "3" then
								'response.write "<li draggable=""true"" class=""item"" value=""" & rsAggregatedLineITems("vchPartNumber") & """>" & rsAggregatedLineITems("vchPartNumber") & "</li>"
								 response.write "<tr draggable=""true"" class=""item"" data-value=""" & rsAggregatedLineITems("vchPartNumber") & """>"
								 response.write "<td>" & rsAggregatedLineITems("sumQty") & " </td>"
								 response.write "<td>" & rsAggregatedLineITems("vchItemName") & "</td>"
								 response.write "</tr>"   
							end if
							rsAggregatedLineITems.MoveNext()
						wend
						if rsAggregatedLineITems.eof then
                            rsAggregatedLineITems.MoveFirst()
                        end if
						%>
				</table>
				<table id="packSlip4" class="hole" name="packSlip4">
					<tr>
						<td colspan="2"><u><b><center>Pack Group 4</center></b></u></td>
					</tr>
					<tr>
						<td><u><b>Quantity</b></u></td>
						<td><u><b>Sign Description</b></u></td>
					</tr>
					<%
						while not rsAggregatedLineITems.eof
							if rsAggregatedLineITems("intPackGroup") & "" = "4" then
								'response.write "<li draggable=""true"" class=""item"" value=""" & rsAggregatedLineITems("vchPartNumber") & """>" & rsAggregatedLineITems("vchPartNumber") & "</li>"
								 response.write "<tr draggable=""true"" class=""item"" data-value=""" & rsAggregatedLineITems("vchPartNumber") & """>"
								 response.write "<td>" & rsAggregatedLineITems("sumQty") & " </td>"
								 response.write "<td>" & rsAggregatedLineITems("vchItemName") & "</td>"
								 response.write "</tr>"   
							end if
							rsAggregatedLineITems.MoveNext()
						wend
						if rsAggregatedLineITems.eof then
                            rsAggregatedLineITems.MoveFirst()
                        end if
						%>
				</table>
			</div>
		</div>
		<br clear="all" />
        <table width="100%" style="margin-top: 20px;">
            <tr>
                <td style="margin-bottom:-10px; border-right:3px solid #ccc;">
                    <strong>Stores Receiving Product Overview</strong>
                </td>

            </tr>			
			
            <% 
                
            %>

            <% while not rsTemp.Eof %>
                <% hasItems = false %>
                <% 
                strSQL = "SELECT O.intID, L.intQuantity, L.vchItemName FROM " & STR_TABLE_ORDER & " O LEFT JOIN " & STR_TABLE_LINEITEM & " L ON O.intID = L.intOrderID WHERE L.chrStatus='A' AND O.intShopperID = " & rsTemp("intShopperID") & " AND O.intCampaignID = " & intCampaignID 
                set rsLineItem = gobjConn.execute(strSQL)

                if not rsLineItem.eof then
                    hasItems = true
                end if
                %>
                <tr>
                    <td>
                        <%
                            if hasItems = true then
                                response.write "<img src='../images/gd.png'>"
                            else
                                response.write "&nbspt;&nbsp;&nbsp;"
                            end if
                        %>
                        &nbsp;&nbsp;<%= rsTemp("vchFirstName") %>&nbsp;<%= rsTemp("vchLastName") %> - <%= rsTemp("vchCity") %>, <%= rsTemp("vchState") %>&nbsp;<%= rsTemp("vchZip") 
						
						%>
						<%
						response.write "<br><br>"
						%>

                </tr>
                <%  
                    rsTemp.MoveNext
                    wend
                %>
        </table>
		<br clear="all"/><br clear="all"/><br clear="all"/><br clear="all"/>
    <% end sub %>