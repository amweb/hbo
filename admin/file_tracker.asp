<!--#include file="./config/incInit.asp"-->

<%
OpenConn


If gintUserName&"" = ""  Then
	response.redirect virtualbase & "store/dashboard.asp"
End if

DrawHeader "", ""
if request("submittedprofile")="" then
	DrawSelectProfile
else
	DrawPage
end if
AddToFooter
'DrawFooter ""

%>
<%
function DrawSelectProfile
%>
    <div class="page-content-wrapper">
        <!-- BEGIN PAGE HEADER-->
		<div class="page-content">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">File Tracker</h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<%= virtualbase %>store/dashboard.asp">Home</a>
                            <i class="fa fa-angle-right"></i>
                            <a href="#">File Tracker</a>
                        </li>
                    </ul>
                </div>
            </div>
			<div <%= iif(request.form("submittedprofile")="", "id='popout'", "class='invisible'") %>>
				<%
				dim rsCampaigns, intCampaignID, strSQL
				
				strSQL = "SELECT intID FROM " & STR_TABLE_INVENTORY & " WHERE chrType='C' and chrStatus='A'"
				set rsCampaigns = gobjconn.execute(strSQL)
				if not rsCampaigns.eof then
				%>
				<form action="<%= virtualbase %>admin/file_tracker.asp" id="campaigndropdown" method="post">
					<select name="campaignid" id="campaignid">
						<option>Select a campaign</option>
						<%
						strSQL = "SELECT intID, vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE chrType='C' and chrStatus='A'"
						set rsCampaigns = gobjconn.execute(strSQL)
						while not rsCampaigns.eof
						'iif(StrComp(Request.Form("campaignid"),rsCampaigns("intID"), 1)=0,"selected='true'", "")
						%>
						<option value="<%= rsCampaigns("intID") %>"><%= rsCampaigns("vchItemName") %></option>
						<%
							rsCampaigns.movenext()
						wend
						%>
					</select>
					<select name="profileid" id="profileid">
					</select>
					<div class="buttoncontainer">
						<input type='hidden' name='submittedprofile' value='true'>
						<div class='col-md-7 col-md-offset-5'>
							<button type='submit' name="continue" class="btn btn-primary disabled" disabled>Continue to File Tracker</button>
							<button type='button' name='backtohome'>Close</button>
						</div>
					</div>
				<%
				end if
				%>
				</form>
			</div>
		</div>
	</div>
<%
end function

function DrawPage
	dim strSQL, rsIndiviualItemsFromCampaign
%>
    <div class="page-content-wrapper">
        <!-- BEGIN PAGE HEADER-->
		<div class="page-content">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">File Tracker</h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<%= virtualbase %>store/dashboard.asp">Home</a>
                            <i class="fa fa-angle-right"></i>
                            <a href="#">File Tracker</a>
                        </li>
                    </ul>
                </div>
            </div>
			<%
			if request.form("submittedprofile")&""<>"" then
			%>
			<div class="filetrackercontainer">
				<form id="filetrackersearch" action="<%= virtualbase %>admin/file_tracker.asp" method="post">
					<input type="hidden" name="campaignid" value="<%= request("campaignid") %>">
					<input type="hidden" name="profileid" value="<%= request("profileid") %>">
					<input type="hidden" name="submittedprofile" value="true">
					<input type="search" name="filetrackersearch">
					<button type="submit">Submit</button>
				</form>
				<%
				dim intProfileID, strArtworkStatus, intCampaignID, strThumnailURL, strImageURL
				intCampaignID = Request.Form("campaignid")
				intProfileID = request.form("profileid")
				strSQL = "SELECT "
				strSQL = strSQL & "I.[vchItemName] as Item, I.[vchColorProcess] AS Color, "
				strSQL = strSQL & "CASE WHEN I.[chrstatus]<>'O' THEN I.[vchimageurl] ELSE NULL END AS imgURL, "
				strSQL = strSQL & "CASE WHEN I.[chrstatus]<>'O' THEN I.[vchimageurl2] ELSE NULL END AS imgThumnail, "
				strSQL = strSQL & "I.[vchSize] AS Size, I.[vchSubstrate] AS Substrate, "
				strSQL = strSQL & "I.[vchNumSides] AS Sides, I.[vchPartNumber] AS SKU, "
				strSQL = strSQL & "I.[chrStatus] AS chrStatus, I.[intID] AS InvItemID, "
				strSQL = strSQL & " I.[vchUpdatedByUser],"
				strSQL = strSQL & " F.[vchItemName] AS Fixture, PP.[intID] AS PPIntID "
				strSQL = strSQL & "FROM [" & STR_TABLE_INVENTORY & "] I "
				strSQL = strSQL & "LEFT JOIN [" & STR_TABLE_INVENTORY & "] F on I.[intParentID] = F.[intID] "
				strSQL = strSQL & "LEFT JOIN [" & STR_TABLE_PROFILE_PACKAGES & "] PP ON I.[intID]=PP.[intItemID] "
				strSQL = strSQL & "WHERE I.[chrType] = 'I' AND PP.[intCampaignID]="
				strSQL = strSQL & intCampaignID & " AND PP.[intProfileID]=" & intProfileID
				if request("filetrackersearch")<>"" then
					strSQL = strSQL & " AND (I.[vchItemName] LIKE '%" & ProtectSQL(request("filetrackersearch")) & "%' "
					strSQL = strSQL & " OR I.[vchPartNumber] LIKE '%" & ProtectSQL(request("filetrackersearch")) & "%' "
					strSQL = strSQL & " OR F.[vchItemName] LIKE '%" & ProtectSQL(request("filetrackersearch")) & "%' "
					strSQL = strSQL & ") "
				end if
				set rsIndiviualItemsFromCampaign = gobjconn.execute(strSQL)
				%>
				<div id="itemcontainer">
					<%
					if not rsIndiviualItemsFromCampaign.eof then
						while not rsIndiviualItemsFromCampaign.eof
							response.write "<div class='individualitem'>" & vbCrLf
							if rsIndiviualItemsFromCampaign("imgThumnail")&""<>"" then
								strThumnailURL = virtualbase & "images/" & rsIndiviualItemsFromCampaign("imgThumnail")
							else
								strThumnailURL = virtualbase & "images/icon-no-artwork-512.png"
							end if
							if rsIndiviualItemsFromCampaign("imgURL")&""<>"" then
								strImageURL = virtualbase & "images/" & rsIndiviualItemsFromCampaign("imgURL")
							else
								strImageURL = virtualbase & "images/icon-no-artwork-512.png"
							end if
							response.write "<div class='imgcontainer'>" & vbCrLf
							response.write "<img src=""" & strThumnailURL & """ alt='" & rsIndiviualItemsFromCampaign("Fixture") & "'>" & vbCrLf
							response.write "</div>" & vbCrLf
							response.write "<a class='imageexpand fa fa-search-plus' alt='" & rsIndiviualItemsFromCampaign("Fixture") & "' href=""" & strImageURL & """>"
							response.write "<img src=""" & strImageURL & """ alt='" & rsIndiviualItemsFromCampaign("Fixture") & "'>" & vbCrLf
							response.write "</a>" & vbCrLf
							response.write "<span><span class='bold'>SKU</span>: " & Replace(rsIndiviualItemsFromCampaign("SKU"), " ", "") & "</span>" & vbCrLf
							response.write "<span><span class='bold'>Name</span>: " & rsIndiviualItemsFromCampaign("Item") & "</span>" & vbCrLf
							if rsIndiviualItemsFromCampaign("Size")&""<>"" then
								response.write "<span><span class='bold'>Size</span>: " & rsIndiviualItemsFromCampaign("Size") & "</span>" & vbCrLf
							end if
							if rsIndiviualItemsFromCampaign("Sides")&""<>"" then
								response.write "<span><span class='bold'>Sides</span>: " & rsIndiviualItemsFromCampaign("Sides") & "</span>" & vbCrLf
							end if
							if rsIndiviualItemsFromCampaign("Substrate")&""<>"" then
								response.write "<span><span class='bold'>Substrate</span>: " & rsIndiviualItemsFromCampaign("Substrate") & "</span>" & vbCrLf
							end if
							if rsIndiviualItemsFromCampaign("Color")&""<>"" then
								response.write "<span><span class='bold'>Color Process</span>: " & rsIndiviualItemsFromCampaign("Color") & "</span>" & vbCrLf
							end if
							'Last updated by
							
							dim strLastUpdatedBy, rsLastUpdatedBy
							if isNumeric(rsIndiviualItemsFromCampaign("vchUpdatedByUser")) then
								set rsLastUpdatedBy = gobjconn.execute("SELECT [vchEmail] FROM " & STR_TABLE_SHOPPER & " WHERE [intID]=" & rsIndiviualItemsFromCampaign("vchUpdatedByUser"))
								'response.write "SELECT [vchEmail] FROM " & STR_TABLE_SHOPPER & " WHERE [intID]=" & rsIndiviualItemsFromCampaign("vchUpdatedByUser")
								'response.write rsLastUpdatedBy("vchEmail")
								strLastUpdatedBy = rsLastUpdatedBy("vchEmail")
							else
								strLastUpdatedBy = rsIndiviualItemsFromCampaign("vchUpdatedByUser")
							end if
							'set strLastUpdatedBy = gobjconn.execute("")
							%>
							<span><span class="bold">Updated By: </span><%= strLastUpdatedBy %></span>
							<%
							
							if Session(SESSION_MERCHANT_UID)<>"" then
								'admin view
								select case rsIndiviualItemsFromCampaign("chrStatus")
									case "O"
										strArtworkStatus = "Artwork not received"
									case "U"
										strArtworkStatus = "Waiting for approval"
									case "A"
										strArtworkStatus = "Approved"
									case "R"
										strArtworkStatus = "Waiting for new thumbnail"
									case "P"
										strArtworkStatus = "Printed" 'awaiting printing
								end select
							else
								'user view
								select case rsIndiviualItemsFromCampaign("chrStatus")
									case "O"
										strArtworkStatus = "Artwork not received"
									case "U"
										strArtworkStatus = "Received, waiting for approval"
									case "A"
										strArtworkStatus = "Artwork approved"
									case "R"
										strArtworkStatus = "Artwork rejected"
									case "P"
										strArtworkStatus = "Printed" 'awaiting printing
								end select
							end if
							
							response.write "<span><span class='bold'>Artwork Status</span>: " & strArtworkStatus & "</span>"
							
							if Session(SESSION_MERCHANT_UID)<>"" then
								'admin view
								%>
								<% if rsIndiviualItemsFromCampaign("chrStatus")="A" then %>
								<button type="button" name="markasprinted" data-itemid="<%= rsIndiviualItemsFromCampaign("InvItemID") %>">Mark as printed</button>
								<% end if %>
								<% if rsIndiviualItemsFromCampaign("chrStatus")="U" then %>
								<button type="button" name="rejectthumbnail" data-itemid="<%= rsIndiviualItemsFromCampaign("InvItemID") %>">Reject</button>
								<button type="button" name="approvethumbnail" data-itemid="<%= rsIndiviualItemsFromCampaign("InvItemID") %>">Approve</button>
								<% end if %>

								<% if rsIndiviualItemsFromCampaign("chrStatus")<>"P" then %>

								<button type="submit" name="uploadthumnail" data-itemid="<%= rsIndiviualItemsFromCampaign("InvItemID") %>">Upload thumbnail</button>
								<% end if %>
							<%
							else
								'user view
							%>
								<% if rsIndiviualItemsFromCampaign("chrStatus")="R" then %>
								<button type="submit" name="uploadthumnail" data-itemid="<%= rsIndiviualItemsFromCampaign("InvItemID") %>">Upload new thumbnail</button>
								<% end if %>
								<% if rsIndiviualItemsFromCampaign("chrStatus")="O" then %>
								<button type="submit" name="uploadthumnail" data-itemid="<%= rsIndiviualItemsFromCampaign("InvItemID") %>">Upload thumbnail</button>
								<% end if %>
							<%
							end if
							%>
							<form action="<%= virtualbase %>admin/file_tracker.asp" method="post" class="individualitemupload" data-itemid="<%= rsIndiviualItemsFromCampaign("InvItemID") %>" enctype="multipart/form-data">
								<input type="hidden" name="itemid" value="<%= rsIndiviualItemsFromCampaign("InvItemID") %>">
								<legend>Upload thumbnail for <%= rsIndiviualItemsFromCampaign("SKU") %></legend>
								<fieldset>
									<div class="thumnailuploadcurrentimage">
										<p>Current Image:
										<br>
										(Click to view full size)</p>
										<%
										if rsIndiviualItemsFromCampaign("imgURL")&""<>"" then
											response.write "<a href='" & strImageURL & "' class='campaignthumbnailzoom'>"
											response.write "<img src='" & strThumnailURL & "' alt='" & rsIndiviualItemsFromCampaign("Fixture") & "'>"
											response.write "</a>" & vbCrLf
										else
											response.write "<p class='noimage'>No image</p>" & vbCrLf
										end if
										%>
									</div>
									<div class="thumnailuploadimage">
										<%
										if rsIndiviualItemsFromCampaign("imgURL")&""<>"" then
											response.write "<label for='currentthmbnailimage'>Image: </label>"
											response.write "<input type='text' name='currentthmbnailimage' value='" & rsIndiviualItemsFromCampaign("imgURL") &"'>"
											response.write "<p>or </p>"
										end if
										'rsIndiviualItemsFromCampaign("imgURL")
										'strImageURL
										%>
										<label for="artwork">Upload Image: </label>
										<input type="file" name="artwork" accept="image/*">
									</div>
									<button type="button" name="uploadthumnailreset" data-itemid="<%= rsIndiviualItemsFromCampaign("InvItemID") %>">Close</button>
									<button type="submit" name="uploadthumnailsubmit" data-itemid="<%= rsIndiviualItemsFromCampaign("InvItemID") %>">Submit</button>
								</fieldset>
								<progress class="progress progress-hidden" data-progressbar-itemid="<%= rsIndiviualItemsFromCampaign("InvItemID") %>"></progress>
							</form>
							<%
							rsIndiviualItemsFromCampaign.movenext()
							response.write "</div>" & vbCrLf
						wend
					end if
					%>
				</div>
			</div>
			<%
			end if
			%>
		</div>
    </div>
<%                    
end function
function AddToFooter
%>
	<script type="text/javascript">
		var AJAXurl = "<%= virtualbase %>config/ajax_functions-ft_sv_rsrcs.asp";
	</script>
	<script src="<%= virtualbase %>assets/admin/layout/scripts/file-tracker.js?v=0.4.1" type="text/javascript"></script>
<%
	DrawFooter ""
end function
%>
