<%@ LANGUAGE="VBScript" %>
<!--#include file="config/incInit.asp"-->
<%

' =====================================================================================
' = File: default.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Main store-front, inventory browser
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: code cleanup/documentation
' = Description of Customizations:
' =   (none)
' =====================================================================================
OpenConn



If Session(SESSION_MERCHANT_UID)&"" = "" Then
Response.Redirect virtualbase
End if

DrawHeader "Dashboard", "custserv"
DrawPage
DrawFooter "custserv"

'If DateDiff("d",Session("FirstLogin")&"",Now())<14 Then
%>
<!--<div id="message" style="position:absolute;border:1px solid #000;width:600px;padding:10px;height:100px;margin:0 auto;text-align:center;background-color:#fff;z-index:10000;top:100px;">
Please take a moment to update contact phone numbers for yourself and your distributors.<br /> This will ensure there are no transit delays and orders will be received on time.<br />
<a href="custserv.asp?action=accountinfo">Your Information</a><br />
<a href="distributors.asp">Your Distributors</a>

</div>
<script>
    jQuery.fn.center = function () {
        this.css("position", "absolute");
        this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) +
                                                $(window).scrollLeft()) + "px");
        return this;
    }

    $("#message").center();

    window.setTimeout(function () {
        $("#message").fadeOut("slow", function () {
            // Animation complete.
        });
    }, 10000);

</script>-->
<%
'End If


Sub DrawPage

%>
<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">

			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">
					POS Resource Center
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="dashboard.asp">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">POS Resource Center</a>
						</li>
					</ul>
				</div>
			</div>
			<!-- END PAGE HEADER-->

			<!-- BEGIN PUSH MESSAGES-->
			<div class="row">
				<div class="col-md-4">
					<!-- BEGIN ALERTS PORTLET-->
					<div class="portlet-body">
						<div class="note note-warning">
							<div class="scroller" style="height: 140px;">
							<%
                            Dim strSQL,rsData
                            strSQL = "SELECT TOP 1 * FROM HBO_GoDoc WHERE STitle='Scroll Text'"
                            set rsData = gobjConn.execute(strSQL)
                            Response.Write rsData("Description")
                            %>
							</div>
						</div>
					</div>
					<!-- END ALERTS PORTLET-->
				</div>
				<div class="col-md-8">
					<!-- BEGIN BANNER-->
					<div class="portlet-body">
                        <% 
                            Dim bannerNumber 
                            
                            
                        %>
                        <ul id="slider1">
                        <%
                        for bannerNumber = 1 to 1
                        %>
						<li style="list-style:none;"><img order="0" src="<%= virtualbase %>images/poddemo-FA-ADMIN-Banner1.jpg" width="750" style="margin-left:-40px;" /></li>
                        <!--<li style="list-style:none;"><img order="0" src="<%= virtualbase %>images/TreasuryBanner<%= bannerNumber %>.jpg" width="750" style="margin-left:-40px;" /></li>-->
                        <%
                        next
                         %>
                        </ul>
						
					</div>
                    <script>
                        $(document).ready(function () {
                            $('#slider1').cycle({
                                fx: 'fade', // here change effect to blindX, blindY, blindZ etc 
                                speed: 'slow',
                                timeout: 5000
                            });
                        }); 
                    </script>
					</div>
					<!-- END BANNER-->
			</div>
			<!-- END PUSH MESSAGES-->
            
			<!-- BEGIN PRIMARY BANNERS -->
			<div class="row">
               <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat red-intense">
						<div class="visual">
							<i class="fa fa-calendar"></i>
						</div>
						<div class="details">
							<div class="desc">
								 <a href="default.asp">Admin</a>
							</div>
							<div class="number">
								 <a href="default.asp">Access</a>
							</div>
						</div>
					</div>
				</div>
              <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat red-intense">
						<div class="visual">
							<i class="fa fa-history"></i>
						</div>
						<div class="details">
							<div class="desc">
								<a href="default.asp">Order</a>
							</div>
							<div class="number">
								 <a href="default.asp">History</a>
							</div>
						</div>
					</div>
				</div>
             	
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat red-intense">
						<div class="visual">
							<i class="fa fa-truck"></i>
						</div>
						<div class="details">
							<div class="desc">
								 <a href="#">Tracking &amp;</a>
							</div>
							<div class="number">
								 <a href="#">Shipping</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat red-intense">
						<div class="visual">
							<i class="fa fa-print"></i>
						</div>
						<div class="details">
							<div class="desc">
								 <a href="/drcpod">Print on</a>
							</div>
							<div class="number">
								 <a href="/drcpod">Demand</a>
							</div>
						</div>
					</div>
				</div>
       	</div>
			<!-- END PRIMARY BANNERS -->
			<div class="clearfix"></div>

			<!-- BEGIN SECONDARY BANNERS -->
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat orange-intense">
						<div class="visual">
							<i class="fa fa-map-marker"></i>
						</div>
						<div class="details">
							<div class="desc">
								 <a href="../store/default.asp?cc=1">Pre-Buy Catalog</a>
							</div>
							<div class="number">
								 <a href="../store/default.asp?cc=1">Create </a>
							</div>
						</div>
					</div>
				</div>
          	
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat orange-intense">
						<div class="visual">
							<i class="fa fa-info-circle"></i>
						</div>
						<div class="details">
							<div class="desc">
								<a href="#">FAQ &</a>
							</div>
							<div class="number">
								 <a href="#">Training</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat orange-intense">
						<div class="visual">
							<i class="fa fa-play-circle"></i>
						</div>
						<div class="details">
							<div class="desc">
								 <a href="default.asp?action=videos">HBO</a>
							</div>
							<div class="number">
								 <a href="default.asp?action=videos">Videos</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat orange-intense">
						<div class="visual">
							<i class="fa fa-mobile-phone"></i>
						</div>
						<div class="details">
							<div class="desc">
								 <a href="#">Treasury</a>
							</div>
							<div class="number">
								 <a href="#">Contacts</a>
							</div>
						</div>
					</div>
				</div>
        	</div>
			<!-- END SECONDARY BANNERS -->
			<div class="clearfix"></div>
        		
			<!-- BEGIN SECONDARY BANNERS -->
			<div class="row">
				<!--
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="dashboard-stat gray-intense">
						<div class="visual">
							<i class="fa fa-files-o"></i>
						</div>
						<div class="details">
							<div class="number">
								 <a href="">TRU/Sales Portal</a>
							</div>
						</div>
					</div>
				</div>
				-->
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="dashboard-stat gray-intense">
						<div class="visual">
							<i class="fa fa-barcode"></i>
						</div>
						<div class="details">
							<div class="number">
								 <a href="http://HBO.drcportal.com/store/default.asp?lt=1">Inventory Management</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- END SECONDARY BANNERS -->
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- END CONTENT -->

<%
End Sub

%>