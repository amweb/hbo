<!--#include file="./config/incInit.asp"-->

<%

If gintUserName&"" = ""  Then
	response.redirect virtualbase & "store/dashboard.asp"
End if

OpenConn

DrawHeader "", ""
DrawPage
AddToFooter

%>
<%

function DrawPage
	dim strSQL, rsRSRCCategories
	dim strChildSQL, rsRSRCIndividualItems, rsRSRCChildCount
%>
    <div class="page-content-wrapper">
        <!-- BEGIN PAGE HEADER-->
		<div class="page-content">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">Store Resources</h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<%= virtualbase %>store/dashboard.asp">Home</a>
                            <i class="fa fa-angle-right"></i>
                            <a href="#">Store Resources</a>
                        </li>
                    </ul>
                </div>
            </div>
			<nav class="storeresourcescategories">
				<%
				strSQL = "SELECT * FROM " & STR_TABLE_RESOURCES & " WHERE [chrType]='C' AND [chrStatus]='A'"
				set rsRSRCCategories = gobjconn.execute(strSQL)
				
				while not rsRSRCCategories.eof
				%>
					<h3 data-itemid="<%= rsRSRCCategories("intID") %>"><%= rsRSRCCategories("vchItemName") %></h3>
					<div>
					<%
					strChildSQL = "SELECT COUNT([intID]) AS numItems FROM " & STR_TABLE_RESOURCES & " WHERE [chrType]='I' AND [chrStatus]='A' AND [intParentID]=" & rsRSRCCategories("intID")
					set rsRSRCChildCount = gobjconn.execute(strChildSQL)
					if rsRSRCChildCount("numItems") > 0 then
						'response.write "<ul class='children'>"
						strChildSQL = "SELECT * FROM " & STR_TABLE_RESOURCES & " WHERE [chrType]='I' AND [chrStatus]='A' AND [intParentID]=" & rsRSRCCategories("intID")
						set rsRSRCIndividualItems = gobjconn.execute(strChildSQL)
						if not rsRSRCIndividualItems.eof then
							while not rsRSRCIndividualItems.eof
								'response.write "<li>" & rsRSRCIndividualItems("vchItemName") & "</li>"
								response.write "<p>" & rsRSRCIndividualItems("vchItemName") & "</p>"
								rsRSRCIndividualItems.movenext()
							wend
						end if
						'response.write "</ul>"
					end if
					'rsRSRCCategories.movenext()
					%>
					</div>
				<%
					rsRSRCCategories.movenext()
				wend
				rsRSRCCategories.movefirst()
				%>
			</nav>
			<section id="storeresourcesfiles">
			</section>
		</div>
    </div>
<%                    
end function

function AddToFooter
%>
	<script type="text/javascript">
		var AJAXurl = "<%= virtualbase %>config/ajax_functions-ft_sv_rsrcs.asp";
	</script>
	<script src="<%= virtualbase %>assets/admin/layout/scripts/store-resources.js" type="text/javascript"></script>
	<link href="<%= virtualbase %>assets/admin/pages/css/rsrc_accordion.css" rel="stylesheet" type="text/css">
<%
	DrawFooter ""
end function
%>
