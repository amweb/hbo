<%@ LANGUAGE="VBScript" %>
<!--#include file="config/incInit.asp"-->
<%

' =====================================================================================
' = File: default.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Admin Frameset
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

response.expires = -1

dim strMainURL
if Session(SESSION_MERCHANT_UNAME)&""="" then 
        Response.Redirect "../default.asp"   
end if
DrawHeader "",""
DrawPage
DrawFooter ""
Sub DrawPage
%>
</script>

    <div class="page-content">
    <table id="tblMain" width="100%">
        <tr>
            <td width="250" style="padding-top:10px;" valign="top">
                <iframe  src="sidebar.asp" name="swgc5_side" id="swgc5_side" frameborder="0" scrolling="auto"  marginwidth="15" marginheight="0" width="100%" height="700"></iframe>	
            </td>
            <td  style="padding-top:10px;" valign="top">
                <iframe  src="main.asp" name="swgc5_main" id="swgc5_main" frameborder="0" scrolling="auto"  marginwidth="10" marginheight="0" width="100%" height="700" style="margin-bottom:100px;"></iframe>
            </td>
        </tr>
    </table>
    </div>
<%
End Sub

 %>
