<!--#include file="./config/incInit.asp"-->

<%

If gintUserName&"" = ""  Then
	response.redirect virtualbase & "store/dashboard.asp"
End if

OpenConn


DrawHeader "", ""
if Session(SESSION_MERCHANT_UID)<>"" then
	DrawAdministration
else
	response.redirect virtualbase & "store/dashboard.asp"
end if
AddToFooter

function DrawAdministration
%>
    <div class="page-content-wrapper">
        <!-- BEGIN PAGE HEADER-->
		<div class="page-content">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">Store Resources</h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<%= virtualbase %>store/dashboard.asp">Home</a>
                            <i class="fa fa-angle-right"></i>
                            <a href="<%= virtualbase %>admin/store_resources.asp">Store Resources</a>
							<i class="fa fa-angle-right"></i>
                            <a href="#">Store Resources Administration</a>
                        </li>
                    </ul>
                </div>
            </div>
			<nav>
				<a href="#storeresourcescategoryadministration">Available Categories</a>
				<a href="#uploadnewfile">Upload a new file</a>
				<a href="#storeresourcesadministration">Item administration</a>
				<a href="#storeitemsneedingattention">Items not in categories</a>
			</nav>
			<a name="storeresourcescategoryadministration"></a>
			<section id="storeresourcescategoryadministration">
				<div class="createcategory">
					<h3>Create a new category</h3>
					<form name="createcategory">
						<fieldset>
							<label for="newcategoryname">Name: </label>
							<input type="text" name="newcategoryname">
						</fieldset>
					</form>
					<button name="sendnewcategory">Create new category</button>
					<button name="resetnewcategory">Cancel</button>
				</div>
				<div class="alert">
					<div class="edit">
						<h3></h3>
						<form name="updatecategory">
							<fieldset>
								<label for='categoryname'>Name: </label>
								<input type='text' name='categoryname' value=''>
								<br>
								<label for='active'>Active: </label>
								<input type='radio' name='active' value='A' checked>Yes
								<input type='radio' name='active' value='I'>No<br>
								<input type='hidden' name='id' value=''>
							</fieldset>
						</form>
						<button name='post' data-formid=''>Update</button>
						<button name='close' data-formid=''>Close</button>
					</div>
					<div class="delete">
						<p>
						<span class='ui-icon ui-icon-alert' style='float:left; margin:12px 12px 20px 0;'></span>
						Are you sure you want to delete category <span id="strCategoryName"></span>?
						</p>
						<button name='delete'>Confirm delete</button>
						<button name='cancel'>Cancel</button>
					</div>
				</div>
				<h4>Available categories:</h4>
				<ul class="listallcategories">
				</ul>
				<button name="createcategory"><i class="fa fa-plus" aria-hidden="true"></i> Create a new category</button>
			</section>
			<a name="uploadnewfile"></a>
			<section id="uploadnewfile">
				<h4>Upload a new file</h4>
				<form name="uploadnewfile" enctype="multipart/form-data">
					<label for="newfile">Upload file</label>
					<input type="file" name="newfile"
						accept="application/pdf, application/postscript, image/*, video/*"
					>
					<label for="">Thumbnail image: </label>
					<input type="file" name="newthumbnail" accept="image/*">
					<label for="newfilename">Name: </label>
					<input type="text" name="newfilename">
					<br>
					<select name="category">
					</select>
					<progress value="0"></progress>
					<br>
					<button name="newfileupload" type="button"><i class="fa fa-upload" aria-hidden="true"></i> Upload file</button>
				</form>
			</section>
			<a name="storeresourcesadministration"></a>
			<section id="storeresourcesadministration">
				<form name="alterindividualitem">
					<fieldset>
						<legend></legend>
						<label for="itemname">Item name:</label>
						<input type="text" name="itemname" value=''>
						<br>
						<label for="active">Active:</label>
						<input type="radio" name="active" value="A" checked> Yes <input type="radio" name="active" value="I">No<br>
						<label for="changecategory">Change category: </label>
						<select name="changecategory">
						</select>
						<input type='hidden' name='itemid' value=''>
					</fieldset>
					<input type="hidden" name="tabid" value="">
					<button type="button" name="updateitem">Update</button>
					<button type="button" name="reset">Close</button>
				</form>
				<div class="deleteindividualitem">
					  <p><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> This item will be disabled.</p>
					  <button type="button" name="deleteindividualitem">Disable this item</button>
					  <button type="button" name="closedialog">Cancel</button>
				</div>
				<h3>Item administration</h3>
				<div class="tabs">
				</div>
			</section>
			<a name="storeitemsneedingattention"></a>
			<section id="storeitemsneedingattention">
				<h3>Items not in categories</h3>
				<div class="tabs">
				</div>
				<form name="attentionform">
					<fieldset>
						<legend></legend>
						<input type="hidden" name="itemid" value="">
						<label for="itemname">Name:</label>
						<input type="text" name="itemname" value="" placeholder="Item name">
						<br>
						<label for="active">Active:</label>
						<input type="radio" name="active" value="A" checked> Yes <input type="radio" name="active" value="I">No<br>
						<label for="changecategory">Change category for this item: </label>
						<select name="changecategory">
						</select>
					</fieldset>
					<button type="button" name="updateitem">Update</button>
					<button type="button" name="reset">Close</button>
				</form>
			</section>
		</div>	
	</div>

<%
end function


function AddToFooter
%>
	<script type="text/javascript">
		var AJAXurl = "<%= virtualbase %>config/ajax_functions-ft_sv_rsrcs.asp";
	</script>
	<script src="<%= virtualbase %>assets/admin/layout/scripts/store-resources.js" type="text/javascript"></script>
<%
	DrawFooter ""
end function
%>
