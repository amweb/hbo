<!--#include file="config/incInit.asp"-->
<%

' =====================================================================================
' = File: default.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Admin Frameset
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================
OpenConn
response.expires = -1
Dim vchPartNumber, vchProgram, vchItemName, txtDescription, mnyUnitPrice,intQuantity, vchValidStates, vchBundleQuantity, vchExpireDate
Dim rsOrders,rsData,  intCounter, mnyTotal, mnySubtotal, intParentID
Dim strFixture, strSubFixture, strImageUrl                    
dim strMainURL, intFixtureId, intSubFixtureId,intCampaignID
Dim strSearch, strStoreSearch, Item, fieldName, fieldValue, intInvID, intOrderId
dim dctSaveList, strSKU, intShopperID, intShipShopperID
Dim vchSize,vchSubstrate,vchNumSides, intQty, intMassQty
dim strCampaignName, strCampaign
dim intStoreID, vchFormat, intArea, intRegion, intDistrict, vchState, intOther
dim error

if Session(SESSION_MERCHANT_UNAME)&""="" then 
        Response.Redirect "../default.asp"
end if

intCampaignID = Request("campaignid")&""

strSearch = Request("Search")
strStoreSearch = Request("store_search")

if IsNumeric(intCampaignID) and intCampaignID<>"" then
    intCampaignID = clng(intCampaignID)
else
    intCampaignID = 0
end if

intFixtureId = Request("fixtureid")&""
if IsNumeric(intFixtureId) and intFixtureId<>"" then
    intFixtureId = clng(intFixtureId)
else
    intFixtureId = 0
end if

intSubFixtureId = Request("subfixtureid")&""
if IsNumeric(intSubFixtureId) and intSubFixtureId<>"" then
    intSubFixtureId = clng(intSubFixtureId)
else
    intSubFixtureId = 0
end if

intInvID = Request("intInvID")
if IsNumeric(intInvID) and intInvID<>"" then
    intInvID = clng(intInvID)
else
    intInvID = 0
end if

intStoreID = Request("storeid")
if intStoreID<>"" then
    intStoreID = intStoreID
else
    intStoreID = 0
end if

intArea = Request("area")
if IsNumeric(intArea) and intArea<>"" then
    intArea = clng(intArea)
else
    intArea = 0
end if

intRegion = Request("region")
if IsNumeric(intRegion) and intRegion<>"" then
    intRegion = clng(intRegion)
else
    intRegion = 0
end if

intDistrict = Request("district")
if IsNumeric(intDistrict) and intDistrict<>"" then
    intDistrict = clng(intDistrict)
else
    intDistrict = 0
end if

intOther = Request("other")
if IsNumeric(intOther) and intOther<>"" then
    intOther = clng(intOther)
else
    intOther = 0
end if

vchState = Request("state")
if vchState<>"" then
    vchState = vchState
else
    vchState = ""
end if

vchFormat = Request("format")
if vchFormat<>"" then
    vchFormat = vchFormat
else
    vchFormat = ""
end if

vchItemName = Request("vchItemName")
if vchItemName<>"" then
    vchItemName = vchItemName
else
    vchItemName = ""
end if

dim strAction
strAction = lcase(Request("action"))&""
select case strAction
	case "delitem": ' delete item from cart
		RemoveLineFromOrder Request("id")
		response.redirect "mcb-profile.asp?campaignid=" & intCampaignID
	case "update": ' update quantities
		UpdateQuantities
		response.redirect "mcb-profile.asp?campaignid=" & intCampaignID
    case "submit"
        if Request("vchItemName") <> "" then
            'CreateInventoryItem ---moved to add to cart to avoid creating items that dont get added
        else
            error = "Please enter a valid name to continue"
        end if
    case "saveorders"
        DrawHeader()
		DrawConfirmation()
		DrawFooter "custserv"
		response.end
    case "confirm"
		SaveOrders
        'Confirm   -moved into saveorders
    case "campaigncreate"
        CreateCampaign
        response.redirect "mcb-profile.asp?campaignid=" & intCampaignID
    case "add to cart"
		CreateInventoryItem
        AddItemsToCart    
end select

sub CreateInventoryItem
	dim arrFixtureInitials, strFixtureInitials, strCampaignName, vchImage, x
    strSKU = ""
	strFixtureInitials = ""

    strSQL = "SELECT vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intCampaignID
    'response.write strSQL
	set rsTemp = gobjConn.execute(strSQL)
    strCampaignName =  rsTemp(0) & ""

    strSQL = "SELECT vchItemName, vchImageURL FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intFixtureId
    set rsTemp = gobjConn.execute(strSQL)
	'response.write strSQL
	vchImage = rsTemp("vchImageURL")
	arrFixtureInitials = split(rsTemp("vchItemName")&"", " ")
	
	For Each x In arrFixtureInitials
		strFixtureInitials = strFixtureInitials & left(x,1)
	Next
	
    strSKU = strFixtureInitials & "_" & vchItemName & "_" & strCampaignName
	'response.write "<br />" &strSKU
	
	strSQL = "SELECT intID, vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE vchPartNumber='" & strSKU & "'"

    set rsTemp = gobjConn.execute(strSQL)
	
	'response.end
	if rsTemp.EOF then
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "*@dtmCreated", "GETDATE()"
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "*vchCreatedByUser", Session(SESSION_MERCHANT_ULOGIN)
		dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
		dctSaveList.Add "*vchCreatedByIP", gstrUserIP
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "chrType", "I"
		dctSaveList.Add "*chrStatus", "A"
		dctSaveList.Add "*#intParentID", intFixtureId
		dctSaveList.Add "*#intSortOrder", 0
		dctSaveList.Add "*#intHitCount", 0
		dctSaveList.Add "vchItemName", vchItemName
		dctSaveList.Add "vchSize", Request("vchSize")
		dctSaveList.Add "vchNumSides", Request("vchNumSides")
		dctSaveList.Add "vchSubstrate", Request("vchSubstrate")
		dctSaveList.Add "vchPartNumber", strSKU
		dctSaveList.Add "vchImageURL", vchImage
		dctSaveList.Add "#mnyItemPrice", "0"
		dctSaveList.Add "chrICFlag", "N"
		dctSaveList.Add "chrTaxFlag", "N"
		dctSaveList.Add "chrSoftFlag", "N"
		dctSaveList.Add "!#intStock", "0"
		dctSaveList.Add "!txtDescription", ""
		intInvID = SaveDataRecord(STR_TABLE_INVENTORY, Request, intInvID, dctSaveList)
	else 
		intInvID = rsTemp("intID")
	end if
end sub

sub AddItemsToCart
	'disabled to allow custom values for items but use field to populate all others
    'intMassQty = Request("mass_qty")

    'if IsNumeric(intMassQty) and intMassQty<>"" then
    '    intMassQty = clng(intMassQty)
    'else
    '    intMassQty = 0
    'end if

	' mred: optimizing queries outside the loop
	dim dctOrders, dctShoppers
    set dctOrders = Server.CreateObject("Scripting.Dictionary")
    set dctShoppers = Server.CreateObject("Scripting.Dictionary")
	
	strSQL = "SELECT intBillShopperID, intID FROM " & STR_TABLE_ORDER & " WHERE chrStatus='0' AND intCampaignID = " & intCampaignID & ""
	set rsTemp = gobjConn.execute(strSQL)
	while not rsTemp.EOF
		if not dctOrders.Exists(rsTemp(0)&"") Then dctOrders.Add rsTemp(0)&"",rsTemp(1)&""
		rsTemp.MoveNext
	wend
	
	'strSQL = "SELECT intID, intShopperID FROM " & STR_TABLE_SHOPPER
	'set rsTemp = gobjConn.execute(strSQL)
	'while not rsTemp.EOF
	'	if not dctShoppers.Exists(rsTemp(0)&"") Then dctShoppers.Add rsTemp(0)&"",rsTemp(1)&""
	'	rsTemp.MoveNext
	'wend
	
	Set rsTemp = Nothing
	
    For Each Item In Request.Form
        fieldName = Item

        if Left(fieldName,4)="QTY_" then
            fieldValue = Request.Form(fieldName)
            
            'aleach - only continue if the store in question has quantity to add, otherwise we are creating empty orders
            if (fieldValue&""<>"" and IsNumeric(fieldValue)) OR intMassQty > 0 then
                
                intShipShopperID = CLNG(Replace(fieldName,"QTY_",""))

                'strSQL = "SELECT *  FROM " & STR_TABLE_ORDER & " WHERE intBillShopperId=" & intShipShopperID & " AND chrStatus='0' AND intCampaignID = " & intCampaignID & ""
	            'set rsTemp = gobjConn.execute(strSQL)
                'if not rsTemp.Eof Then
                '    intOrderId = rsTemp("intId")
                'else
                '    CreateNewOrder
                '    intOrderId = gintOrderID
                'End If
				
				If dctOrders.Exists(intShipShopperID&"") Then
					intOrderId = dctOrders(intShipShopperID&"")
				Else
                    CreateNewOrder
                    intOrderId = gintOrderID
				End If
            
                'strSQL = "SELECT intShopperID FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & intShipShopperID
	            'set rsTemp = gobjConn.execute(strSQL)
                'strSQL = "UPDATE " & STR_TABLE_ORDER & " SET intShopperID=" & rsTemp(0) & ", intBillShopperId=" & intShipShopperID & ", intShipShopperId=" & intShipShopperID & ", intCampaignID=" & intCampaignId & " WHERE intID=" & intOrderId
				'strSQL = "UPDATE " & STR_TABLE_ORDER & " SET intShopperID=" & dctShoppers(intShipShopperID&"") & ", intBillShopperId=" & intShipShopperID & ", intShipShopperId=" & intShipShopperID & ", intCampaignID=" & intCampaignId & " WHERE intID=" & intOrderId
				strSQL = "UPDATE " & STR_TABLE_ORDER & " SET intShopperID=(SELECT intShopperID FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & intShipShopperID & "), intBillShopperId=" & intShipShopperID & ", intShipShopperId=" & intShipShopperID & ", intCampaignID=" & intCampaignId & " WHERE intID=" & intOrderId
	            call gobjConn.execute(strSQL)

                'if intMassQty > 0 then
                '    AddItemToOrder_Other intOrderId, intInvID, intMassQty  
                'else
                    intQTY = CLNG(fieldValue)
                    AddItemToOrder_Other intOrderId, intInvID, intQTY  
                'end if
                             
            end if            
        end if
    Next

    %> <script>parent.location = 'mcb-profile.asp?campaignid=<%=intCampaignId %>';</script> <%
end sub

if strSearch&""<>"" then
    
    strSQL = "SELECT intID FROM " & STR_TABLE_INVENTORY & " WHERE chrType='A' AND chrStatus='A' AND vchItemName like '%" & SQLEncode(strSearch) & "%' AND intParentID=(SELECT TOP 1 intID FROM " & STR_TABLE_INVENTORY & " WHERE vchItemName='Fixtures')"
    set rsTemp = gobjConn.execute(strSQL)
    
    if not rsTemp.Eof then
        intFixtureId = CINT(rsTemp(0)&"")
    end if    

    strSQL = "SELECT intID, intParentID FROM " & STR_TABLE_INVENTORY & " WHERE chrType='A' AND chrStatus='A'" 
    strSQL = strSQL & " AND vchItemName like '%" & SQLEncode(strSearch) & "%' AND intParentID in (SELECT intID FROM " & STR_TABLE_INVENTORY & " WHERE intParentID=(SELECT TOP 1 intID FROM " & STR_TABLE_INVENTORY & " WHERE vchItemName='Fixtures'))"
    set rsTemp = gobjConn.execute(strSQL)
    
    if not rsTemp.Eof then
        intFixtureId = CINT(rsTemp(1)&"")
        intSubFixtureId= CINT(rsTemp(0)&"")
    end if 
    'sanity check
    
     strSQL = "SELECT intID FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intSubFixtureId & " AND intParentID=" & intFixtureId
    set rsTemp = gobjConn.execute(strSQL)
    
    if rsTemp.eof then
        intSubFixtureId = 0    
    end if                    

end if

sub SaveOrders
    strSQL = "SELECT *  FROM " & STR_TABLE_ORDER & " WHERE chrStatus in ('0','9', 'S') AND intBillShopperID is not null AND intCampaignID=" & intCampaignId

    'response.write strSQL
	set rsOrders = gobjConn.execute(strSQL)

    While not rsOrders.Eof
        UpdateOrderStatus_Other rsOrders("intID"), "S"
        rsOrders.MoveNext
    Wend
	
	Confirm
    'DrawHeader()
    'DrawConfirmation()
        
    'response.end   
end sub

sub Confirm
    strSQL = "UPDATE " & STR_TABLE_ORDER & " SET dtmSubmitted = GETDATE() WHERE intCampaignID=" & intCampaignId

    'response.write strSQL
	set rsOrders = gobjConn.execute(strSQL)

	dim strMerchantMessage, strSubject
	strMerchantMessage = "A campaign has been confirmed: " & intCampaignID & vbcrlf
	strMerchantMessage = strMerchantMessage & vbcrlf
	strSubject = "Campaign Confirmation #" & intCampaignID & ""
	SendMerchantEmail strSubject, strMerchantMessage

    DrawHeader()
    DrawConfirmation()
    DrawFooter "custserv"
    response.end   
end sub

sub CreateCampaign
    set dctSaveList = Server.CreateObject("Scripting.Dictionary")
    dctSaveList.Add "*@dtmCreated", "GETDATE()"
    dctSaveList.Add "@dtmUpdated", "GETDATE()"
    dctSaveList.Add "*vchCreatedByUser", Session(SESSION_MERCHANT_ULOGIN)
    dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
    dctSaveList.Add "*vchCreatedByIP", gstrUserIP
    dctSaveList.Add "vchUpdatedByIP", gstrUserIP
    dctSaveList.Add "chrType", "C"
    dctSaveList.Add "*chrStatus", "A"
    dctSaveList.Add "*#intParentID", 0
    dctSaveList.Add "*#intSortOrder", 0
    dctSaveList.Add "*#intHitCount", 0
    dctSaveList.Add "!vchItemName", ""
    dctSaveList.Add "#mnyItemPrice", "0"
    dctSaveList.Add "chrICFlag", "N"
    dctSaveList.Add "chrTaxFlag", "N"
    dctSaveList.Add "chrSoftFlag", "N"
    dctSaveList.Add "!vchSpecialDate", ""
    dctSaveList.Add "!#intStock", "0"
    dctSaveList.Add "!txtDescription", ""
    intCampaignID = SaveDataRecord(STR_TABLE_INVENTORY, Request, intCampaignID, dctSaveList)
end sub

sub UpdateQuantities
	dim i, intQty, intItemID, intOrderID
	for each i in Request.Form
		if lcase(left(i, 7)) = "intqty_" then
        Response.Write mid(i,8)
			intOrderID = Split(mid(i,8),"_")(0)
            intItemID = Split(mid(i,8),"_")(1)
			if IsNumeric(intItemID) then
				intItemID = CLng(intItemID)
				intQty = Request.Form(i)
				if IsNumeric(intQty) then
					intQty = CLng(intQty)
				else
					intQty = -1
				end if
				if intQty = 0 then
					' remove item from cart
					RemoveLineFromOrder_Other intOrderID,intItemID
					' also need to remove customized inventory item?
					
					' if order now has no items, delete order
					strSQL = "SELECT intID FROM " & STR_TABLE_LINEITEM & " WHERE intOrderID=" & intOrderID
					set rsTemp = gobjConn.execute(strSQL)
					if rsTemp.eof then
						UpdateOrderStatus_Other intOrderID, "D"
					end if

				elseif intQty > 0 then
					' change qty in cart
					EditOrderItemQty_Other intOrderID,intItemID, intQty
				end if
			end if
		end if
	next
end sub

DrawHeader()
%>
		<div class="loader" id="loader">
			<!--<img src="../images/loader.gif" style="margin:auto; position: absolute; top: 0; left: 0; right: 0; bottom: 0;"/>-->
			<div >
				<progress class="progressloader" style="margin:auto; position: absolute; top: 0; left: 0; right: 0; bottom: 0; "></progress>
				<p style="margin:auto; position: absolute; top: 75px; left: 0; right: 0; bottom: 0;">Loading...</p>
			</div>
		</div>
		<link href="<%= virtualbase %>assets/admin/pages/css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
		<link href="<%= virtualbase %>assets/admin/pages/css/mcb.css?v=0.3.1" rel="stylesheet" type="text/css"/>
        <% if intCampaignID = 0 then %>

        <div id="createform">
			<% 
			strSQL = "SELECT intID, vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE chrType='C' and chrStatus='A'"
			set rsTemp = gobjConn.execute(strSQL)
			if not rsTemp.eof then
			%>
				<form action="mcb-profile.asp" method="post" style="text-align:left;padding:20px;">
					<fieldset>
						<legend>Load existing campaign</legend>
						<select name="campaignid" id="campaignid" class="btn btn-default dropdown-toggle" onchange="this.form.submit()">
							<option>Select Existing Campaign</option>
							<% 
							strSQL = "SELECT intID, vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE chrType='C' and chrStatus='A'"
							set rsTemp = gobjConn.execute(strSQL)
							while not rsTemp.eof
							%>
							<option value="<%= rsTemp("intID") %>"><%= rsTemp("vchItemName") %></option>
							<%
								rsTemp.MoveNext
							wend
							%>
						</select>
					</fieldset>
				</form>
			<% end if %>
        </div>
        <script>
            $(document).ready(function () {
                $("#createform").dialog({
					resizeable: false,
					height: "auto",
					width: 400,
					modal: true,
					dialogClass: "noTitleStuff"
				});
            });
        </script>
        <% else %>
        <%
        Dim strSQL, rsTemp
        
        %>
        <script src="<%= virtualbase %>assets/admin/layout/scripts/svg-handler.js" type="text/javascript"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.6/d3.min.js"></script>
		<link href="<%= virtualbase %>assets/global/css/svg-colors.css" rel="stylesheet" type="text/css"/>
		<span id="campaignID" style="display:none"><%=intCampaignID%></span>
		<span id="layoutID" style="display:none"></span>
		<div class="row">
			<select id="profileDropdown" class="btn btn-default dropdown-toggle col-lg-3" data-campaign="<%=intCampaignID%>"></select>
			<div class="input-group col-lg-3" id="searchcontainer">
				<input id="searchBox" type="text" class="form-control" placeholder="Search" />
				<span class="input-group-btn">
					<input type="button" class="btn btn-default" value="Submit" />
				</span>
			</div>
			<div id="aqr" class="col-lg-6">
				<form action="<%= virtualbase %>store/aqr.asp" method="post">
					<input type="hidden" name="campaignid" value="<%= intCampaignID %>">
					<button type="submit">Export Quote Request (CSV)</button>
				</form>
				<br /><br /><br />
				<form action="<%= virtualbase %>store/quoterequestexport.asp" method="post">
					<input type="hidden" name="campaignid" value="<%= intCampaignID %>">
					<button type="submit" style="padding-left:10px">Export Quote Request (PDF)</button>
				</form>
			</div>
		</div>
		<div class="clear" style="clear:both"></div>
		<div id="storeList"></div>
		<div id="leftSide" style="width:60%; float:left; display:none;">
			<div class="row">
				<select id="zoomDD" class="btn btn-sm btn-secondary dropdown-toggle col-md-4">
				</select>
				<div class="btn-group col-md-4" data-toggle="buttons">
					<input id="departmentToggler" name="departmentToggler" type="checkbox" onclick="toggleDepartments()">
					<label for="departmentToggler">Toggle Departments</label>
				</div>
			</div>
			<div id="stage"></div>
		</div>
		<div id="rightSide" style="width:37%; float:right;">
			
		</div>
		<div id="itemDialog" title="Create Signage"></div>
        <div id="quickView" title="Quick View" style="display:none;">
        	<div style="margin-bottom: 20px;">
            	<span id="quickViewStore"></span>
            </div>
			<div style="float:left;width:50%;">
				<img id="quickViewImage" />
			</div>
			<div id="quickViewOverView" style="float:right;"></div>
			<br clear="all" />
			<div id="quickViewButtons" style="float:right;">
				<input type="button" id="quickViewCreate" value="Show Full View" onclick="loadMap();$('#quickView').dialog('close');">
				<input type="button" id="quickViewClose" onclick="$('#quickView').dialog('close');" value="Cancel" />
			</div>
		</div>
		<div id="manualSelection" style="display:none;">
			<div id="slotName"></div>
			<form id="manualSignForm">
				<input id="vchSlotName" type="hidden">
				<select id="slotDropDown"></select>
			</form>
		</div>
        <br /><br /><br />
        <% 
        
        %>


        <%
        end if
        %>
    </div>
</div>
<!-- END CONTAINER -->	
<!-- BEGIN COPYRIGHT -->
<div class="footer copyright">
	<div class="page-footer">
		&copy; <%= Now %> DOME Solutions. All rights reserved. Under license to HBO
	</div>
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->

<script src="<%= virtualbase %>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/jquery-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<%= virtualbase %>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%= virtualbase %>assets/global/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<%= virtualbase %>assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/admin/pages/scripts/login.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
	function RUSure() {
		return confirm('Are you sure you want to remove this item from your order?');
	};
	function scrollToElement(ele) {
		$(window).scrollTop(ele.offset().top).scrollLeft(ele.offset().left);
	}
    jQuery(document).ready(function () {
		
        Metronic.init();
        QuickSidebar.init()
        Login.init();
		
    });
	/*window.load( function() {
		var hash = window.location.hash;
		if (hash != '')
		{
			scrollToElement( $('a #' + hash));
		}
	});*/
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

<%
    sub DrawHeader() %>
        <!DOCTYPE html>
        <!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
        <!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
        <!--[if !IE]><!-->
        <html lang="en" class="no-js">
        <!--<![endif]-->
        <!-- BEGIN HEAD -->
        <head>
        <meta charset="utf-8"/>
        <title></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
        <link href="<%= virtualbase %>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<%= virtualbase %>assets/global/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="<%= virtualbase %>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <link href="<%= virtualbase %>assets/global/plugins/bootstrap/css/sticky-footer.css" rel="stylesheet" type="text/css"/>
        <link href="<%= virtualbase %>/assets/global/plugins/bootstrap-multiselect/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css"/>

        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
        <!-- END PAGE LEVEL PLUGIN STYLES -->
        <!-- BEGIN PAGE STYLES -->
        <link href="<%= virtualbase %>assets/global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
        <link href="<%= virtualbase %>assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
        <!-- END PAGE STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="<%= virtualbase %>assets/global/css/components.css" rel="stylesheet" type="text/css"/>
        <link href="<%= virtualbase %>assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<%= virtualbase %>assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<%= virtualbase %>assets/admin/layout/css/themes/light2.css" rel="stylesheet" type="text/css" />
        <link href="<%= virtualbase %>assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
        <script src="<%= virtualbase %>assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
        <script src="<%= virtualbase %>assets/global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="<%= virtualbase %>assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
		<link href="<%= virtualbase %>drcpod/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
        <script src="<%= virtualbase %>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<%= virtualbase %>assets/global/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js" type="text/javascript"></script>

        <!-- END THEME STYLES -->
        <style>
        legend { 
            display: block;
            padding-left: 2px;
            padding-right: 2px;
            border: none;
        }
        .page-breadcrumb {
            border-top: 5px solid #13436D !important;
        }
        </style>
	<script>
	$(function() {
		$( "#accordion" ).accordion({
			collapsible: true,
			heightStyle: "content",
			active: false
		});
	});
	</script>
        </head>
        <!-- END HEAD -->
        <!-- BEGIN BODY -->
        <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
        <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
        <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
        <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
        <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
        <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
        <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
        <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
        <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
        <body class="page-header-fixed login">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
	        <!-- BEGIN HEADER INNER -->
	        <div class="page-header-inner">
		        <!-- BEGIN LOGO -->
		        <div class="page-logo">
			        <a href="<%= virtualbase %>store/dashboard.asp"><img src="<%= virtualbase %>assets/admin/layout/img/logo.png" alt="logo" class="logo-default" style="float:left"></a>
		        </div>
                <% if gintUserName&""<>"" then %>
		        <!-- END LOGO -->
		        <!-- BEGIN TOP NAVIGATION MENU -->
		        <div class="top-menu">
			        <ul class="nav navbar-nav pull-right">
				        <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
					        <a href="#">Welcome <strong><%= gintUserName %></strong></a>
				        </li>
				        <li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">
					        <a href="<%= virtualbase %>store/custserv.asp?action=accountinfo"><i class="fa fa-caret-down"></i> My Account</a>
				        </li>
				        <li class="dropdown dropdown-extended dropdown-tasks" id="header_task_bar">
					        <a href="<%= virtualbase %>default.asp"><i class="fa fa-power-off"></i> Logout</a>
				        </li>
			        </ul>
		        </div>
		        <!-- END TOP NAVIGATION MENU -->
               <% elseif Session(SESSION_MERCHANT_ULOGIN)&""<>"" then %>
		        <!-- END LOGO -->
		        <!-- BEGIN TOP NAVIGATION MENU -->
		        <div class="top-menu">
			        <ul class="nav navbar-nav pull-right">
				        <li class="dropdown dropdown-extended dropdown-notification" id="Li1">
					        <a href="#">Welcome <strong><%= Session(SESSION_MERCHANT_ULOGIN)  %></strong></a>
				        </li>
				        <li class="dropdown dropdown-extended dropdown-inbox" id="Li2">
					        <a href="<%= virtualbase %>admin/default.asp"><i class="fa fa-caret-down"></i> My Account</a>
				        </li>
				        <li class="dropdown dropdown-extended dropdown-tasks" id="Li3">
					        <a href="<%= virtualbase %>default.asp"><i class="fa fa-power-off"></i> Logout</a>
				        </li>
			        </ul>
		        </div>
		        <!-- END TOP NAVIGATION MENU -->
                <% end if %>
	        </div>
	        <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container" style="margin-bottom: 130px;">
            <div class="page-content">
            <br />
   		        <div class="row">
			        <div class="col-md-12">
				        <h3 class="page-title">
				        Collateral Manager
				        </h3>
				        <ul class="page-breadcrumb breadcrumb">
					        <li>
						        <i class="fa fa-home"></i>
						        <a href="<%= virtualbase %>store/dashboard.asp">Home</a>
						        <i class="fa fa-angle-right"></i>
					        </li>
					        <li>
						        <a href="">Collateral Manager</a>                        
						        <i class="fa fa-angle-right"></i>
					        </li>
                            <% if intCampaignID > 0 then 
                                strCampaignName = gobjConn.execute("SELECT vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intCampaignID )(0)                   
                            %>
                            <li>
						        <a href="#"><%= strCampaignName %></a>
					        </li>
                            <% end if %>
				        </ul>
			        </div>
		        </div>


    <% end sub

    sub DrawConfirmation()
        dim hasItems, rsLineItem, rsAggregatedLineItems
        %>
		<form action="mcb-profile.asp">
		The Campaign has now been saved and opened. To edit this campaign, Click on the Edit Campaign button.
		<%
        strSQL = "SELECT C.vchFirstName as [ContactFirstName], C.vchLastName as [ContactLastName], L.* FROM " & STR_TABLE_SHOPPER & " C INNER JOIN " & STR_TABLE_SHOPPER & " L ON L.intShopperID = C.intID  WHERE C.chrStatus='A' AND L.chrType='S'"
        strSQL = strSQL & " ORDER BY C.vchLastName,C.vchFirstName, L.vchLastName, L.vchFirstName"
        'response.write strSQL
        set rsTemp = gobjConn.execute(strSQL)
        %>
        &nbsp;&nbsp;&nbsp;<a href="mcb-profile.asp?campaignid=<%= intCampaignID %>">Edit Campaign</a>
        &nbsp;&nbsp;&nbsp;<a href="store/dashboard.asp">Home</a>
		&nbsp;&nbsp;&nbsp;
		<% If strAction <> "confirm" then %>
		<input type="hidden" name="campaignid" value="<%= intCampaignID %>" />
		<input type="hidden" name="action" value="Confirm" />
		<input type="submit" value="Confirm" />
		<% Else %>
        <strong>Campaign confirmed</strong>
		<% End If %>
        </form>
        <br /><br />
		
		<script src="../assets/admin/pages/scripts/drag_n_drop.js" type="text/javascript"></script>
		
		<div id="packingSlipArea">
		
			<div id="itemSKUList">
				<center><strong>Fixture Order Overview</strong><br />(unassigned)</center>
				<table id="listArea" name="unassignedSKU">
					<tr>
                        <td><u><b>Quantity</b></u></td>
                        <td><u><b>Sign Description</b></u></td>
                    </tr>
					<% 'insert sku loop here
						strSQL = "SELECT sum(L.intQuantity) as sumQty, L.vchItemName, L.vchPartNumber, G.intPackGroup FROM " & STR_TABLE_ORDER & " O LEFT JOIN " & STR_TABLE_LINEITEM & " L ON O.intID = L.intOrderID Left Join " & STR_TABLE_PACKGROUP & " G on L.vchPartNumber = G.vchPartNumber WHERE L.chrStatus='A' AND O.intCampaignID = " & intCampaignID & " GROUP BY L.intInvID, L.vchItemName, L.vchPartNumber, G.intPackGroup"
    '           		 response.write strSQL
						set rsAggregatedLineItems = gobjConn.execute(strSQL)
						while not rsAggregatedLineITems.eof
							if rsAggregatedLineITems("intPackGroup") & "" = "0" or rsAggregatedLineITems("intPackGroup") & "" = "" then
								'response.write "<li draggable=""true"" class=""item"" value=""" & rsAggregatedLineITems("vchPartNumber") & """>" & rsAggregatedLineITems("vchPartNumber") & "</li>"
								 response.write "<tr draggable=""true"" class=""item"" data-value=""" & rsAggregatedLineITems("vchPartNumber") & """>"
								 response.write "<td>" & rsAggregatedLineITems("sumQty") & " </td>"
								 response.write "<td>" & rsAggregatedLineITems("vchItemName") & "</td>"
								 response.write "</tr>"   
								
							end if
							rsAggregatedLineITems.MoveNext()
						wend
						rsAggregatedLineITems.MoveFirst()
					%>
                </table>
			</div>
			<div id="packingContainer">
				<table id="packSlip1" class="hole" name="packSlip1">
					<tr>
						<td colspan="2"><u><b><center>Pack Group 1</center></b></u></td>
					</tr>
					<tr>
						<td><u><b>Quantity</b></u></td>
						<td><u><b>Sign Description</b></u></td>
					</tr>
					<%
						while not rsAggregatedLineITems.eof
							if rsAggregatedLineITems("intPackGroup") & "" = "1" then
								'response.write "<li draggable=""true"" class=""item"" value=""" & rsAggregatedLineITems("vchPartNumber") & """>" & rsAggregatedLineITems("vchPartNumber") & "</li>"
								 response.write "<tr draggable=""true"" class=""item"" data-value=""" & rsAggregatedLineITems("vchPartNumber") & """>"
								 response.write "<td>" & rsAggregatedLineITems("sumQty") & " </td>"
								 response.write "<td>" & rsAggregatedLineITems("vchItemName") & "</td>"
								 response.write "</tr>"   
							end if
							rsAggregatedLineITems.MoveNext()
						wend
						rsAggregatedLineITems.MoveFirst()
						%>
				</table>
				<table id="packSlip2" class="hole" name="packSlip2">
					<tr>
						<td colspan="2"><u><b><center>Pack Group 2</center></b></u></td>
					</tr>
					<tr>
						<td><u><b>Quantity</b></u></td>
						<td><u><b>Sign Description</b></u></td>
					</tr>
					<%
						while not rsAggregatedLineITems.eof
							if rsAggregatedLineITems("intPackGroup") & "" = "2" then
								'response.write "<li draggable=""true"" class=""item"" value=""" & rsAggregatedLineITems("vchPartNumber") & """>" & rsAggregatedLineITems("vchPartNumber") & "</li>"
								 response.write "<tr draggable=""true"" class=""item"" data-value=""" & rsAggregatedLineITems("vchPartNumber") & """>"
								 response.write "<td>" & rsAggregatedLineITems("sumQty") & " </td>"
								 response.write "<td>" & rsAggregatedLineITems("vchItemName") & "</td>"
								 response.write "</tr>"   
							end if
							rsAggregatedLineITems.MoveNext()
						wend
						rsAggregatedLineITems.MoveFirst()
						%>
				</table>
				<table id="packSlip3" class="hole" name="packSlip3">	
					<tr>
						<td colspan="2"><u><b><center>Pack Group 3</center></b></u></td>
					</tr>
					<tr>
						<td><u><b>Quantity</b></u></td>
						<td><u><b>Sign Description</b></u></td>
					</tr>
					<%
						while not rsAggregatedLineITems.eof
							if rsAggregatedLineITems("intPackGroup") & "" = "3" then
								'response.write "<li draggable=""true"" class=""item"" value=""" & rsAggregatedLineITems("vchPartNumber") & """>" & rsAggregatedLineITems("vchPartNumber") & "</li>"
								 response.write "<tr draggable=""true"" class=""item"" data-value=""" & rsAggregatedLineITems("vchPartNumber") & """>"
								 response.write "<td>" & rsAggregatedLineITems("sumQty") & " </td>"
								 response.write "<td>" & rsAggregatedLineITems("vchItemName") & "</td>"
								 response.write "</tr>"   
							end if
							rsAggregatedLineITems.MoveNext()
						wend
						rsAggregatedLineITems.MoveFirst()
						%>
				</table>
				<table id="packSlip4" class="hole" name="packSlip4">
					<tr>
						<td colspan="2"><u><b><center>Pack Group 4</center></b></u></td>
					</tr>
					<tr>
						<td><u><b>Quantity</b></u></td>
						<td><u><b>Sign Description</b></u></td>
					</tr>
					<%
						while not rsAggregatedLineITems.eof
							if rsAggregatedLineITems("intPackGroup") & "" = "4" then
								'response.write "<li draggable=""true"" class=""item"" value=""" & rsAggregatedLineITems("vchPartNumber") & """>" & rsAggregatedLineITems("vchPartNumber") & "</li>"
								 response.write "<tr draggable=""true"" class=""item"" data-value=""" & rsAggregatedLineITems("vchPartNumber") & """>"
								 response.write "<td>" & rsAggregatedLineITems("sumQty") & " </td>"
								 response.write "<td>" & rsAggregatedLineITems("vchItemName") & "</td>"
								 response.write "</tr>"   
							end if
							rsAggregatedLineITems.MoveNext()
						wend
						rsAggregatedLineITems.MoveFirst()
						%>
				</table>
			</div>
		</div>
		<br clear="all" />
        <table width="100%" style="margin-top: 20px;">
            <tr>
                <td style="margin-bottom:-10px; border-right:3px solid #ccc;">
                    <strong>Stores Receiving Product Overview</strong>
                </td>

            </tr>			
			
            <% 
                
            %>

            <% while not rsTemp.Eof %>
                <% hasItems = false %>
                <% 
                strSQL = "SELECT O.intID, L.intQuantity, L.vchItemName FROM " & STR_TABLE_ORDER & " O LEFT JOIN " & STR_TABLE_LINEITEM & " L ON O.intID = L.intOrderID WHERE L.chrStatus='A' AND O.intShopperID = " & rsTemp("intShopperID") & " AND O.intCampaignID = " & intCampaignID 
                set rsLineItem = gobjConn.execute(strSQL)

                if not rsLineItem.eof then
                    hasItems = true
                end if
                %>
                <tr>
                    <td>
                        <%
                            if hasItems = true then
                                response.write "<img src='../images/gd.png'>"
                            else
                                response.write "&nbsp;&nbsp;&nbsp;"
                            end if
                        %>
                        &nbsp;&nbsp;<%= rsTemp("vchFirstName") %>&nbsp;<%= rsTemp("vchLastName") %> - <%= rsTemp("vchCity") %>, <%= rsTemp("vchState") %>&nbsp;<%= rsTemp("vchZip") 
						
						%>
						<%
						response.write "<br><br>"
						%>

                </tr>
                <%  
                    rsTemp.MoveNext
                    wend
                %>
        </table>
		<br clear="all"/><br clear="all"/><br clear="all"/><br clear="all"/>
    <% end sub %>