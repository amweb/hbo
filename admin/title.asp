<%@ LANGUAGE="VBScript" %>
<!--#include file="config/incInit.asp"-->
<%
' =====================================================================================
' = File: title.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Admin Frameset - Title Frame
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
	<TITLE><%= STR_SITE_NAME %></TITLE>
	<STYLE>
	<!--
		A	{text-decoration : none;}
		A:Hover	{background : <%= cBlue %>;}
	// -->
	</STYLE>
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
<!--
function GetPrintTag()
{
	// reload the content frame with a 'print' argument
	var s;
	var x;
	s = top.swgc5_main.location + '';
	if (s.indexOf('?') == -1)
	{
		s = s + '?print=1';
	}
	else
	{
		s = s + '&print=1';
	}
	return s;
}
// -->
</SCRIPT>
</HEAD>
<BODY BGCOLOR="<%= cBlue %>" TEXT="<%= cWhite %>" LINK="<%= cOrange %>" VLINK="<%= cOrange %>" ALINK="<%= cOrange %>">
<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%">
<TR VALIGN=TOP>
	<TD><A HREF="http://www.americanwebservices.com/" TARGET="_new"><img src="<%= imagebase %>arrowclick_small.gif" width="161" height="39" alt="ArrowClick Software" border="0" /></A></TD>
	<TD WIDTH="100%"><TABLE BORDER=0 CELLPADDING=3 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cDkBlue %>">
	<TR>
		<TD COLSPAN=2><%= spacer(1,1) %></TD>
	</TR>
	<TR>
		<TD><%= font(2) %><%= spacer(35,1) %><B><%= STR_COMPANY_NAME %> Document/Merchant System</B>
		&nbsp;
<%
	if CheckAdminLogin_Sideframe() then
		response.write font(1) & "<B>" & Session(SESSION_MERCHANT_UNAME) & "</B></FONT>" ' logged-in since " & Session(SESSION_MERCHANT_UTIME) & "</FONT>"
	else
		response.write font(1) & "Not logged in.</FONT>"
	end if
%>
		</TD>
		<TD ALIGN=RIGHT><%= font(1) %>
<% if CheckAdminLogin_Sideframe() then %>
		<A HREF="login.asp" TARGET="swgc5_main">Logoff</A>
<% end if %>
		<A TARGET="_blank" NAME="link_print" HREF="#" onClick="link_print.href=GetPrintTag(); return true;" xonClick="window.parent.swgc5_main.focus(); print(); return false;"><IMG SRC="<%= imagebase %>icon_print.gif" WIDTH="16" HEIGHT="16" ALT="Print" BORDER="0" ALIGN=ABSMIDDLE HSPACE=25></A>
		</TD>
	</TR>
	</TABLE></TD>
</TR>
</TABLE>
</BODY>
</HTML>
