<%@ LANGUAGE="VBScript" %>
<%

dim strSQL, rsTemp, conn

set conn = Server.CreateObject("ADODB.Connection")
conn.open "DSN=pwdx; UID=pwdx; PWD=pwdx"

strSQL = "SELECT I.*, P.vchItemName AS P_ItemName FROM TCS_Inv AS I, CTS_Inv AS P WHERE I.intParentID *= P.intID ORDER BY I.chrType, I.intParentID, I.intID"
set rsTemp = conn.execute(strSQL)

intFolderID = Request("folder")
if IsNumeric(intFolderID) then
	intFolderID = CLng(intFolderID)
else
	intFolderID = 0
end if

while not rsTemp.eof
	if rsTemp("chrType") = "A" then
		response.write "<A HREF=""upgrade.asp?folder=" & rsTemp("intID") & """>Folder " & rsTemp("intParentID") & "-" & rsTemp("intID") & "</A>: " & rsTemp("vchItemName") & " (inside " & rsTemp("P_ItemName") & ")<BR>"
		if rsTemp("intID") = intFolderID then
		%>
		<FORM ACTION="upgrade.asp" METHOD="POST">
		<INPUT TYPE="hidden" NAME="folder" VALUE="<%= intFolderID %>">
		Parent ID: <INPUT TYPE="text" NAME="parent" VALUE="<%= rsTemp("intParentID") %>"> <INPUT TYPE="submit" VALUE="Change">
		</FORM>
		<%
		end if
	else
		response.write "Item " & rsTemp("intParentID") & "-" & rsTemp("intID") & ": " & rsTemp("vchItemName") & " (inside " & rsTemp("P_ItemName") & ")<BR>"
	end if
	rsTemp.MoveNext
wend

rsTemp.close
set rsTemp = nothing

conn.close
set conn = nothing

%>