<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: list.asp
' = Copyright (c)200 American Web Services, Inc. All rights reserved.
' = Description:
' =   Referal System Admin
' = Revision History:
' =   27sep2000 ssutterfield: created
' = Description of Customizations:
' =   
' =====================================================================================

if not CheckUserAccess(ACC_REFERAL_ADMIN) then
	response.redirect "../main.asp"
end if

CheckAdminLogin

dim strPageTitle
strPageTitle = "Referal"
const STR_PAGE_TYPE = "adminreflist"

const STR_SEARCH_SCRIPT = "list.asp?"
const STR_DETAIL_SCRIPT = "mod.asp?"

OpenConn

dim strAction

strAction = lcase(Request("action"))

dim strSQL, rsData

strSQL = "SELECT * FROM " & STR_TABLE_REFERER & " WHERE chrStatus<>'D' ORDER BY vchCompany"
set rsData = gobjConn.execute(strSQL)

call DrawHeaderUpdate()
DrawPage rsData
DrawFooter STR_PAGE_TYPE

rsData.close
set rsData = nothing
response.end

sub DrawPage(rsInput)
	DrawListHeader
	if rsData.eof then
%>
<TR>
	<TD COLSPAN="5" ALIGN="center">No referals found.</TD>
</TR>
<%
	else
		while not rsData.eof
			DrawListItem rsData("intID"), rsData("vchCompany"), rsData("fltDiscount"), rsData("chrStatus")
			rsData.MoveNext
			if not rsData.eof then
				DrawListItemSep
			end if
		wend
	end if
	DrawListFooter
end sub

sub DrawListHeader
%>
<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVLtGrey) %>">
<TR>
	<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cWhite %>">
	<TR>
		<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%">
		<TR BGCOLOR="<%= cVLtYellow %>">
			<TD NOWRAP><%= font(1) %><B>Referal #</B></TD>
			<TD NOWRAP><%= font(1) %><B>Company&nbsp;</B></TD>
			<TD NOWRAP><%= font(1) %><B>&nbsp;Discount&nbsp;</B></TD>
			<TD NOWRAP><%= font(1) %><B>Status</B></TD>
			<TD NOWRAP>[<A HREF="mod.asp">Add...</A>]</TD>
		</TR>
<%
end sub

sub DrawListItem(intID, strName, fltDiscount, strStatus)
	dim strColor
	strColor = ""
	if strStatus = "E" then
		strColor = "BGCOLOR=""#FFCCCC"""
	end if
%>
		<TR VALIGN=BOTTOM <%= strColor %>>
			<TD NOWRAP><%= intID %></TD>
			<TD><A HREF="<%= STR_DETAIL_SCRIPT %>id=<%= intID %>"><%= strName %></A></TD>
			<TD NOWRAP>&nbsp;<%= FormatNumber(fltDiscount, 1) & "%" %>&nbsp;</TD>
			<TD NOWRAP><%= GetStatusStr(strStatus, true) %></TD>
			<TD NOWRAP>[<A HREF="mod.asp?id=<%= intID %>">Edit...</A>]</TD>
		</TR>
<%
end sub

sub DrawListItemSep
%>
		<TR>
			<TD COLSPAN=5><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVVLtGrey) %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
		</TR>
<%
end sub

sub DrawListFooter
%>
		</TABLE></TD>
	</TR>
	</TABLE></TD>
</TR>
</TABLE>
<%
end sub
%>