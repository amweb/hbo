<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: mod.asp
' = Copyright (c)2000 American Web Services, Inc. All rights reserved.
' = Description:
' =   Referal Admin
' = Revision History:
' =   27sep2000 ssutterfield: created
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_REFERAL_ADMIN) then
	response.redirect "../main.asp"
end if

const STR_PAGE_TITLE = "Referal Details"
const STR_PAGE_TYPE = "adminreflist"

OpenConn

dim strSQL

dim intID, intParentID, strAction
strAction = Request("action")
intID = Request("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if

if strAction = "delete" then
	gobjConn.execute("UPDATE " & STR_TABLE_REFERER & " SET chrStatus='D' WHERE intID=" & intID)
	response.redirect "list.asp"
elseif strAction = "setstatus" then
	gobjConn.execute("UPDATE " & STR_TABLE_REFERER & " SET chrStatus='" & Request("status") & "' WHERE intID=" & intID)
	response.redirect "list.asp"
elseif strAction = "submit" then
	AutoCheck Request, ""
	if Request("chrSoftFlag") = "Y" then
		CheckField "vchSoftURL", "IsEmpty", Request, "Missing Download URL"
	end if
	if Request("chrICFlag") = "Y" then
		if not FormErrors.Exists("intStock") then
			CheckField "intStock", "IsInt", Request, "Missing Qty"
		end if
	end if
	if FormErrors.count = 0 then
		dim rsTemp, intNewOrder
		strSQL = "SELECT ISNULL(MAX(intSortOrder),0) AS intMaxSortOrder FROM " & STR_TABLE_INVENTORY
		set rsTemp = gobjConn.execute(strSQL)
		intNewOrder = rsTemp("intMaxSortOrder") + 1
		rsTemp.close
		set rsTemp = nothing
		dim dctSaveList
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "*@dtmCreated", "GETDATE()"
			dctSaveList.Add "@dtmUpdated", "GETDATE()"
			dctSaveList.Add "*vchCreatedByUser", Session(SESSION_MERCHANT_ULOGIN)
			dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
			dctSaveList.Add "*vchCreatedByIP", gstrUserIP
			dctSaveList.Add "vchUpdatedByIP", gstrUserIP
			dctSaveList.Add "chrType", "R"
			dctSaveList.Add "*chrStatus", "A"
			dctSaveList.Add "!vchCompany", ""
			dctSaveList.Add "!vchSiteName", ""
			dctSaveList.Add "!vchContactName", ""
			dctSaveList.Add "!vchContactEmail", ""
			dctSaveList.Add "!vchContactPhone1", ""
			dctSaveList.Add "!vchContactPhone2", ""
			dctSaveList.Add "!#fltDiscount", ""
		SaveDataRecord STR_TABLE_REFERER, Request, intID, dctSaveList
		response.redirect "list.asp"
	else
		DrawHeader STR_PAGE_TITLE, STR_PAGE_TYPE
		DrawPage Request
		DrawFooter STR_PAGE_TYPE
	end if
else
	dim rsData, blnDataIsRS
	if intID > 0 then
		strSQL = "SELECT * FROM " & STR_TABLE_REFERER & " WHERE intID=" & intID & " AND chrStatus<>'D'"
		set rsData = gobjConn.execute(strSQL)
		blnDataIsRS = true
		if rsData.eof then
			response.redirect "list.asp"
		end if
	else
		set rsData = Request
		blnDataIsRS = false
	end if
	
	DrawHeader STR_PAGE_TITLE, STR_PAGE_TYPE
	DrawPage rsData
	DrawFooter STR_PAGE_TYPE
	
	if blnDataIsRS then
		rsData.close
		set rsData = nothing
	end if
end if
response.end

sub DrawPage(rsInput)
	dim strTitle, strInfoText
	if intID > 0 then
		strTitle = "Edit Referal"
	else
		strTitle = "Add Referal"
	end if
%>
<FORM ACTION="mod.asp" METHOD="POST" NAME="frm">
<INPUT TYPE="hidden" NAME="action" VALUE="submit">
<INPUT TYPE="hidden" NAME="id" VALUE="<%= intID %>">
<%
	AddPageAction "Cancel", "list.asp", ""
	if intID > 0 then
		if rsInput("chrStatus") <> "A" then
			AddPageAction "Activate", "mod.asp?action=setstatus&status=A&id=" & intID, "Are you sure you want to activate this referal?"
		end if
		if rsInput("chrStatus") <> "I" then
			AddPageAction "Deactivate", "mod.asp?action=setstatus&status=I&id=" & intID, "Are you sure you want to deactivate this referal?"
		end if
		AddPageAction "Delete", "mod.asp?action=delete&id=" & intID, "Are you sure you want to delete this referal?"
	end if
	
	if intID > 0 then
		strInfoText = "<B>Record ID:</B> " & rsInput("intID")
		strInfoText = strInfoText & "&nbsp; <B>Created:</B> " & rsInput("dtmCreated")
		strInfoText = strInfoText & "&nbsp; <B>Updated:</B> " & rsInput("dtmUpdated")
	else
		strInfoText = ""
	end if
	
	DrawFormHeader "100%", strTitle, strInfoText
	
	stlBeginStdTable "100%"
	response.write "<TR><TD>"
	
	stlBeginFormSection "100%", 2
	stlTextInput "vchCompany", 32, 32, rsInput, "Company", "IsEmpty", "Missing Company Name"
	stlTextInput "vchSiteName", 32, 50, rsInput, "Site Name", "", ""
	stlTextInput "vchContactName", 32, 32, rsInput, "Contact Name", "", ""
	stlTextInput "vchContactEmail", 32, 50, rsInput, "Contact Email", "", ""
	stlTextInput "vchContactPhone1", 32, 32, rsInput, "Contact Phone 1", "", ""
	stlTextInput "vchContactPhone2", 32, 32, rsInput, "Contact Phone 2", "", ""
	stlTextInput "fltDiscount", 5, 5, rsInput, "Discount", "IsNumeric", "Invalid Discount"
	stlEndFormSection
	
	response.write "</TD></TR>"
	stlSubmit "Submit"
	
	stlEndStdTable
	response.write "</FORM>"
	
	SetFieldFocus "frm", "vchCompany"
end sub
%>