<%@ LANGUAGE="VBScript" %>
<!--#include file="config/incInit.asp"-->
<!--#include file="../config/incVeracore.asp"-->
<%

' =====================================================================================
' = File: main.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Admin Frameset - main frame, display system summary
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

dim strPageType, strPageTitle

strPageType = "adminmain"
strPageTitle = "System Summary for " & STR_SITE_NAME

'response.write Session(SESSION_MERCHANT_UID) & ", " & Session(SESSION_MERCHANT_BID) & "<br />"

OpenConn

dim strSQL, strWhere, rsData, dtmThisWeek, dtmThisMonth, dtmThisYear, aryStats(9,6)
dtmThisWeek = DateAdd("d", 1-Weekday(Date()), Date())
dtmThisMonth = CDate(Month(Date()) & "/1/" & Year(Date()))
dtmThisYear = CDate("1/1/" & Year(Date()))

if Request("toggle_prebuy")&""<>"" then
    
    strSQL = "UPDATE RNDC_Setting SET vchValue= case when vchValue='true' then 'false' else 'true' end WHERE vchName='Pre-Buy Open'" 
    gobjConn.execute strSQL 
    

end if

if Request("toggle_POD")&""<>"" then
    
    strSQL = "UPDATE RNDC_Setting SET vchValue= case when vchValue='true' then 'false' else 'true' end WHERE vchName='POD On'" 
    gobjConn.execute strSQL 
    

end if

if Request("action")="sync" then
    Server.ScriptTimeout  = 6000
    strSQL = "SELECT vchPartNumber, IsNull(intStock,0) AS intStock FROM " & STR_TABLE_INVENTORY_LONG_TERM & " WHERE chrType='I' AND chrStatus='A'" 
    set rsData = gobjConn.execute(strSQL) 

    while not rsData.Eof
        call AddProduct(rsData("vchPartNumber")&"",rsData("intStock")&"")
        rsData.MoveNext
    wend

end if


if Request("send_message")&""<>"" then

 Dim strText,strPreBuy,intCampaign, strEmail

    strSQL = "SELECT TOP 1 intID, vchItemName FROM RNDC_INV WHERE chrType='C' AND chrStatus='A'" 
    set rsData = gobjConn.execute(strSQL) 

    intCampaign = rsData("intID")
    strPreBuy = rsData("vchItemName")&""

    strSQL = "SELECT DISTINCT vchEmail FROM RNDC_SHOPPER S INNER JOIN RNDC_ORDER O ON O.intShopperID=S.intID INNER JOIN RNDC_LINEITEM L ON O.intID=L.intOrderID INNER JOIN RNDC_INV I ON L.intInvId=I.intID WHERE I.intCampaign=" & intCampaign
    set rsData = gobjConn.execute(strSQL) 
    
    While not rsData.eof
    strEmail = rsData(0)&""


    strText = strText & "" & vbNewLine
strText = strText & "<html xmlns=""http://www.w3.org/1999/xhtml""><head>" & vbNewLine
strText = strText & "      <meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"">" & vbNewLine
strText = strText & "      <meta name=""viewport"" content=""width=device-width, initial-scale=1.0"">" & vbNewLine
strText = strText & "      <title>RNDC POS Resource Center</title>" & vbNewLine
strText = strText & "      <style type=""text/css"">" & vbNewLine
strText = strText & "         /* Client-specific Styles */" & vbNewLine
strText = strText & "         #outlook a {padding:0;} /* Force Outlook to provide a ""view in browser"" menu link. */" & vbNewLine
strText = strText & "         body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}" & vbNewLine
strText = strText & "         /* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */" & vbNewLine
strText = strText & "         .ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */" & vbNewLine
strText = strText & "         .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing.  More on that: http://www.emailonacid.com/forum/viewthread/43/ */" & vbNewLine
strText = strText & "         #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}" & vbNewLine
strText = strText & "         img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}" & vbNewLine
strText = strText & "         a img {border:none;}" & vbNewLine
strText = strText & "         .image_fix {display:block;}" & vbNewLine
strText = strText & "         p {margin: 0px 0px !important;}" & vbNewLine
strText = strText & "         " & vbNewLine
strText = strText & "         table td {border-collapse: collapse;}" & vbNewLine
strText = strText & "         table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }" & vbNewLine
strText = strText & "         /*a {color: #e95353;text-decoration: none;text-decoration:none!important;}*/" & vbNewLine
strText = strText & "         /*STYLES*/" & vbNewLine
strText = strText & "         table[class=full] { width: 100%; clear: both; }" & vbNewLine
strText = strText & "         " & vbNewLine
strText = strText & "         /*################################################*/" & vbNewLine
strText = strText & "         /*IPAD STYLES*/" & vbNewLine
strText = strText & "         /*################################################*/" & vbNewLine
strText = strText & "         @media only screen and (max-width: 640px) {" & vbNewLine
strText = strText & "         a[href^=""tel""], a[href^=""sms""] {" & vbNewLine
strText = strText & "         text-decoration: none;" & vbNewLine
strText = strText & "         color: #ffffff; /* or whatever your want */" & vbNewLine
strText = strText & "         pointer-events: none;" & vbNewLine
strText = strText & "         cursor: default;" & vbNewLine
strText = strText & "         }" & vbNewLine
strText = strText & "         .mobile_link a[href^=""tel""], .mobile_link a[href^=""sms""] {" & vbNewLine
strText = strText & "         text-decoration: default;" & vbNewLine
strText = strText & "         color: #ffffff !important;" & vbNewLine
strText = strText & "         pointer-events: auto;" & vbNewLine
strText = strText & "         cursor: default;" & vbNewLine
strText = strText & "         }" & vbNewLine
strText = strText & "         table[class=devicewidth] {width: 440px!important;text-align:center!important;}" & vbNewLine
strText = strText & "         table[class=devicewidthinner] {width: 420px!important;text-align:center!important;}" & vbNewLine
strText = strText & "         table[class=""sthide""]{display: none!important;}" & vbNewLine
strText = strText & "         img[class=""bigimage""]{width: 420px!important;height:219px!important;}" & vbNewLine
strText = strText & "         img[class=""col2img""]{width: 420px!important;height:258px!important;}" & vbNewLine
strText = strText & "         img[class=""image-banner""]{width: 440px!important;height:106px!important;}" & vbNewLine
strText = strText & "         td[class=""menu""]{text-align:center !important; padding: 0 0 10px 0 !important;}" & vbNewLine
strText = strText & "         td[class=""logo""]{padding:10px 0 5px 0!important;margin: 0 auto !important;}" & vbNewLine
strText = strText & "         img[class=""logo""]{width: 420px!important;padding:0!important;margin: 0 auto !important;}" & vbNewLine
strText = strText & "" & vbNewLine
strText = strText & "         }" & vbNewLine
strText = strText & "         /*##############################################*/" & vbNewLine
strText = strText & "         /*IPHONE STYLES*/" & vbNewLine
strText = strText & "         /*##############################################*/" & vbNewLine
strText = strText & "         @media only screen and (max-width: 480px) {" & vbNewLine
strText = strText & "         a[href^=""tel""], a[href^=""sms""] {" & vbNewLine
strText = strText & "         text-decoration: none;" & vbNewLine
strText = strText & "         color: #ffffff; /* or whatever your want */" & vbNewLine
strText = strText & "         pointer-events: none;" & vbNewLine
strText = strText & "         cursor: default;" & vbNewLine
strText = strText & "         }" & vbNewLine
strText = strText & "         .mobile_link a[href^=""tel""], .mobile_link a[href^=""sms""] {" & vbNewLine
strText = strText & "         text-decoration: default;" & vbNewLine
strText = strText & "         color: #ffffff !important; " & vbNewLine
strText = strText & "         pointer-events: auto;" & vbNewLine
strText = strText & "         cursor: default;" & vbNewLine
strText = strText & "         }" & vbNewLine
strText = strText & "         table[class=devicewidth] {width: 280px!important;text-align:center!important;}" & vbNewLine
strText = strText & "         table[class=devicewidthinner] {width: 260px!important;text-align:center!important;}" & vbNewLine
strText = strText & "         table[class=""sthide""]{display: none!important;}" & vbNewLine
strText = strText & "         img[class=""bigimage""]{width: 260px!important;height:136px!important;}" & vbNewLine
strText = strText & "         img[class=""col2img""]{width: 260px!important;height:160px!important;}" & vbNewLine
strText = strText & "         img[class=""image-banner""]{width: 280px!important;height:68px!important;}" & vbNewLine
strText = strText & "	     img[class=""logo""]{width: 260px!important;padding:0!important;margin: 0 auto !important;}" & vbNewLine
strText = strText & "" & vbNewLine
strText = strText & "         " & vbNewLine
strText = strText & "         }" & vbNewLine
strText = strText & "      </style>" & vbNewLine
strText = strText & "" & vbNewLine
strText = strText & "      " & vbNewLine
strText = strText & "   </head>" & vbNewLine
strText = strText & "<body bgcolor=""#f6f4f5"">" & vbNewLine
strText = strText & "<div class=""block"">" & vbNewLine
strText = strText & "   <!-- Start of preheader -->" & vbNewLine
strText = strText & "   <table width=""100%"" bgcolor=""#f6f4f5"" cellpadding=""0"" cellspacing=""0"" border=""0"" id=""backgroundTable"" st-sortable=""preheader"">" & vbNewLine
strText = strText & "      <tbody>" & vbNewLine
strText = strText & "         <tr>" & vbNewLine
strText = strText & "            <td width=""100%"">" & vbNewLine
strText = strText & "               <table width=""580"" cellpadding=""0"" cellspacing=""0"" border=""0"" align=""center"" class=""devicewidth"">" & vbNewLine
strText = strText & "                  <tbody>" & vbNewLine
strText = strText & "                     <!-- Spacing -->" & vbNewLine
strText = strText & "                     <tr>" & vbNewLine
strText = strText & "                        <td width=""100%"" height=""20""></td>" & vbNewLine
strText = strText & "                     </tr>" & vbNewLine
strText = strText & "                     <!-- Spacing -->" & vbNewLine
strText = strText & "                  </tbody>" & vbNewLine
strText = strText & "               </table>" & vbNewLine
strText = strText & "            </td>" & vbNewLine
strText = strText & "         </tr>" & vbNewLine
strText = strText & "      </tbody>" & vbNewLine
strText = strText & "   </table>" & vbNewLine
strText = strText & "   <!-- End of preheader -->" & vbNewLine
strText = strText & "</div>" & vbNewLine
strText = strText & "<div class=""block"">" & vbNewLine
strText = strText & "   <!-- start of header -->" & vbNewLine
strText = strText & "   <table width=""100%"" bgcolor=""#f6f4f5"" cellpadding=""0"" cellspacing=""0"" border=""0"" id=""backgroundTable"" st-sortable=""header"">" & vbNewLine
strText = strText & "      <tbody>" & vbNewLine
strText = strText & "         <tr>" & vbNewLine
strText = strText & "            <td>" & vbNewLine
strText = strText & "               <table width=""580"" bgcolor=""#ffffff"" cellpadding=""0"" cellspacing=""0"" border=""0"" align=""center"" class=""devicewidth"" hlitebg=""edit"" shadow=""edit"">" & vbNewLine
strText = strText & "                  <tbody>" & vbNewLine
strText = strText & "                     <tr>" & vbNewLine
strText = strText & "                        <td>" & vbNewLine
strText = strText & "                           <!-- logo -->" & vbNewLine
strText = strText & "                           <table width=""280"" cellpadding=""0"" cellspacing=""0"" border=""0"" align=""left"" class=""devicewidth"">" & vbNewLine
strText = strText & "                              <tbody>" & vbNewLine
strText = strText & "                                 <tr>" & vbNewLine
strText = strText & "                                    <td valign=""middle"" width=""270"" style=""padding: 20px 0px 10px 20px;"" class=""logo"">" & vbNewLine
strText = strText & "                                       <div class=""imgpop"">" & vbNewLine
strText = strText & "                                          <a href=""http://RNDC.drcportal.com/""><img src=""http://RNDC.drcportal.com/images/logo.gif"" alt=""logo"" border=""0"" style=""display:block; border:none; outline:none; text-decoration:none;"" st-image=""edit"" class=""logo""></a>" & vbNewLine
strText = strText & "                                       </div>" & vbNewLine
strText = strText & "                                    </td>" & vbNewLine
strText = strText & "                                 </tr>" & vbNewLine
strText = strText & "                              </tbody>" & vbNewLine
strText = strText & "                           </table>" & vbNewLine
strText = strText & "                           <!-- End of logo -->" & vbNewLine
strText = strText & "                        </td>" & vbNewLine
strText = strText & "                     </tr>" & vbNewLine
strText = strText & "                  </tbody>" & vbNewLine
strText = strText & "               </table>" & vbNewLine
strText = strText & "            </td>" & vbNewLine
strText = strText & "         </tr>" & vbNewLine
strText = strText & "      </tbody>" & vbNewLine
strText = strText & "   </table>" & vbNewLine
strText = strText & "   <!-- end of header -->" & vbNewLine
strText = strText & "</div><div class=""block"">" & vbNewLine
strText = strText & "   <!-- image + text -->" & vbNewLine
strText = strText & "   <table width=""100%"" bgcolor=""#f6f4f5"" cellpadding=""0"" cellspacing=""0"" border=""0"" id=""backgroundTable"" st-sortable=""bigimage"">" & vbNewLine
strText = strText & "      <tbody>" & vbNewLine
strText = strText & "         <tr>" & vbNewLine
strText = strText & "            <td>" & vbNewLine
strText = strText & "               <table bgcolor=""#ffffff"" width=""580"" align=""center"" cellspacing=""0"" cellpadding=""0"" border=""0"" class=""devicewidth"" modulebg=""edit"">" & vbNewLine
strText = strText & "                  <tbody>" & vbNewLine
strText = strText & "                     <tr>" & vbNewLine
strText = strText & "                        <td width=""100%"" height=""20""></td>" & vbNewLine
strText = strText & "                     </tr>" & vbNewLine
strText = strText & "                     <tr>" & vbNewLine
strText = strText & "                        <td>" & vbNewLine
strText = strText & "                           <table width=""540"" align=""center"" cellspacing=""0"" cellpadding=""0"" border=""0"" class=""devicewidthinner"">" & vbNewLine
strText = strText & "                              <tbody>" & vbNewLine
strText = strText & "                                 <!-- title -->" & vbNewLine
strText = strText & "                                 <tr>" & vbNewLine
strText = strText & "                                    <td style=""font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #333333; text-align:left;line-height: 20px;"" st-title=""rightimage-title"">The " & strPreBuy &  " Is Now Closed</td>" & vbNewLine
strText = strText & "                                 </tr>" & vbNewLine
strText = strText & "                                 <!-- end of title -->" & vbNewLine
strText = strText & "                                 <!-- Spacing -->" & vbNewLine
strText = strText & "                                 <tr>" & vbNewLine
strText = strText & "                                    <td width=""100%"" height=""20""></td>" & vbNewLine
strText = strText & "                                 </tr>" & vbNewLine
strText = strText & "                                 <!-- Spacing -->" & vbNewLine
strText = strText & "                                 <!-- content -->" & vbNewLine
strText = strText & "                                 <tr>" & vbNewLine
strText = strText & "                                    <td style=""font-family: Helvetica, arial, sans-serif; font-size: 13px; color: #666666; text-align:left;line-height: 24px;"" st-content=""rightimage-paragraph"">" & vbNewLine
strText = strText & "                                       <p>The items that were in your cart at the close of the pre-buy have been submitted to the RNDC admin. Items that do not meet the minimum order quantity will not be produced. Once items are finalized, your Order History will be updated. To view your order History, log into the <a href=""http://RNDC.drcportal.com/"" style=""color: #c1262f"">POS Resource Center</a>.</p></td>" & vbNewLine
strText = strText & "                                 </tr>" & vbNewLine
strText = strText & "                                 <!-- end of content -->" & vbNewLine
strText = strText & "                                 <!-- Spacing -->" & vbNewLine
strText = strText & "                                 <tr>" & vbNewLine
strText = strText & "                                    <td width=""100%"" height=""50""></td>" & vbNewLine
strText = strText & "                                 </tr>" & vbNewLine
strText = strText & "                                 <!-- Spacing -->" & vbNewLine
strText = strText & "                              </tbody>" & vbNewLine
strText = strText & "                           </table>" & vbNewLine
strText = strText & "                        </td>" & vbNewLine
strText = strText & "                     </tr>" & vbNewLine
strText = strText & "                  </tbody>" & vbNewLine
strText = strText & "               </table>" & vbNewLine
strText = strText & "            </td>" & vbNewLine
strText = strText & "         </tr>" & vbNewLine
strText = strText & "      </tbody>" & vbNewLine
strText = strText & "   </table>" & vbNewLine
strText = strText & "</div>" & vbNewLine
strText = strText & "" & vbNewLine
strText = strText & "</body></html>" & vbNewLine

Dim myMail
Set myMail = CreateObject("CDONTS.NewMail")
myMail.From = "RNDCsupport@domeprinting.com"
myMail.To = strEmail
myMail.Subject = "The " & strPreBuy &  " Is Now Closed"
myMail.Body = strText
myMail.BodyFormat=0
myMail.MailFormat=0
myMail.Send
Set myMail = Nothing

        rsData.MoveNext
    Wend
end if


dim strBrand,  strBrandClause, intBrand
if Session(SESSION_MERCHANT_BID) <> "" then
	strBrand = "brand_"
	intBrand = Session(SESSION_MERCHANT_BID)
	strBrandClause = strBrandClause & " LEFT JOIN " & STR_TABLE_LINEITEM & " AS I ON O.intID = I.intOrderID AND I.chrStatus<>'D'"
	strBrandClause = strBrandClause & " RIGHT JOIN " & STR_TABLE_INVENTORY & " AS B ON I.intInvID = B.intID AND B.intBrand=" & intBrand & " AND (B.chrType='I' OR B.chrType IS NULL)"
end if

dim intItemCount, intFolderCount, intLowItemCount, intHighItemCount, intUserLoginCount, intUserSessionCount, intBadLoginCount
dim strStatusClause
' gather statistics
strStatusClause = UserAccessStatus_BuildSQLWhereX("SAZE")
if strStatusClause <> "" then
	aryStats(0,0) = "Open Orders"
	strSQL = "SELECT COUNT(DISTINCT O.intID) AS intCount, ISNULL(SUM(O.mnyGrandTotal),0) AS mnyTotal FROM " & STR_TABLE_ORDER & " AS O" & strBrandClause & " WHERE O.chrStatus IN (" & strStatusClause & ")"
'	response.write strSQL & "<br />"
	set rsData = gobjConn.execute(strSQL)
	'--------JRMCodeChange---------
	aryStats(0,1) = "<A HREF=""merchant/"&strBrand&"order_list.asp?preset=open"">" & rsData("intCount") & "</A>"
	if strBrand = "" then
	aryStats(0,2) = "<A HREF=""merchant/"&strBrand&"order_list.asp?preset=open"">" & SafeFormatCurrency("n/a", rsData("mnyTotal"), 2) & "</A>"
	end if
	rsData.close
	set rsData = nothing
end if

strStatusClause = UserAccessStatus_BuildSQLWhereX("SAZXCHKER")
if strStatusClause <> "" then
	aryStats(1,0) = "Orders Placed"
	strWhere = "WHERE O.chrStatus IN (" & strStatusClause & ") AND O.dtmCreated >= '"
	strSQL = "SELECT COUNT(*) AS intCount, ISNULL(SUM(O.mnyGrandTotal),0) AS mnyTotal FROM " & STR_TABLE_ORDER & " AS O" & strBrandClause & " " & strWhere & dtmThisWeek & "'"
	set rsData = gobjConn.execute(strSQL)
	'response.Write "<br />Orders Placed: " & strSQL
	'--------JRMCodeChange---------
	aryStats(1,1) = "<A HREF=""merchant/"&strBrand&"order_list.asp?preset=placedw&action=search&n_groups=1&mode=a&c_group=1&value_%25O.dtmCreated=" & dtmThisWeek & "&value_%2BO.chrStatus=S%2BA%2BZ%2BX%2BC%2BH%2BK%2BE%2BR""</A>" & rsData("intCount") & "</A>"
'http://resdev.sacweb.com/demo/www/admin/order_list.asp?
'action=search
'&n_groups=1
'&mode=a
'&reset=&
'c_group=1
'&exclude_%23O.intID=
'&value_%23O.intID=
'&value2_%23O.intID=
'&exclude_%25O.dtmCreated=
'&value_%25O.dtmCreated=dtmThisWeek
'&value2_%25O.dtmCreated=
'&exclude_%25O.dtmUpdated=
'&value_%25O.dtmUpdated=
'&value2_%25O.dtmUpdated=
'&exclude_O.vchCreatedByIP=
'&value_O.vchCreatedByIP=
'&exclude_O.vchUpdatedByIP=
'&value_O.vchUpdatedByIP=
'&exclude_O.vchCreatedByUser=
'&value_O.vchCreatedByUser=
'&exclude_O.vchUpdatedByUser=
'&value_O.vchUpdatedByUser=
'&exclude_%2BO.chrType=
'&exclude_%2BO.chrStatus=
'&value_%2BO.chrStatus=S
'&value_%2BO.chrStatus=A
'&value_%2BO.chrStatus=H
'&value_%2BO.chrStatus=K
'&value_%2BO.chrStatus=Z
'&value_%2BO.chrStatus=X
'&value_%2BO.chrStatus=C
'&value_%2BO.chrStatus=E
'&exclude_O.vchShippingNumber=
'&value_O.vchShippingNumber=
'&exclude_O.vchShopperID=
'&value_O.vchShopperID=
'&exclude_O.vchShopperBrowser=
'&value_O.vchShopperBrowser=
'&exclude_%2BO.chrPaymentMethod=
'&exclude_B.vchLastName=
'&value_B.vchLastName=
'&exclude_B.vchFirstName=
'&value_B.vchFirstName=
'&exclude_O.vchPaymentCardType=
'&value_O.vchPaymentCardType=
'&exclude_O.vchPaymentCardName=
'&value_O.vchPaymentCardName=
'&exclude_O.vchPaymentCardNumber=
'&value_O.vchPaymentCardNumber=
'&exclude_%25O.vchPaymentCardExp=
'&value_%25O.vchPaymentCardExp=
'&value2_%25O.vchPaymentCardExp=
'&exclude_%2B%23O.intTaxZone=
'&exclude_%23O.mnyNonTaxableSubtotal=
'&value_%23O.mnyNonTaxableSubtotal=
'&exclude_%23O.mnyTaxableSubtotal=
'&value_%23O.mnyTaxableSubtotal=
'&exclude_%23O.fltTaxRate=
'&value_%23O.fltTaxRate=
'&exclude_%23O.mnyTaxAmount=
'&value_%23O.mnyTaxAmount=
'&exclude_%23O.mnyShipAmount=
'&value_%23O.mnyShipAmount=
'&exclude_%23O.mnyGrandTotal=
'&value_%23O.mnyGrandTotal=
'&submit=Search
	if strBrand = "" then
		aryStats(1,2) = SafeFormatCurrency("n/a", rsData("mnyTotal"), 2)
	end if
	rsData.close
	set rsData = nothing
	strSQL = "SELECT COUNT(DISTINCT O.intID) AS intCount, ISNULL(SUM(O.mnyGrandTotal),0) AS mnyTotal FROM " & STR_TABLE_ORDER & " AS O" & strBrandClause & " " & strWhere & dtmThisMonth & "'"
	set rsData = gobjConn.execute(strSQL)
	'response.Write "<br />strSQL: " & strSQL
	'--------JRMCodeChange---------
	aryStats(1,3) = "<A HREF=""merchant/"&strBrand&"order_list.asp?preset=placedm&action=search&n_groups=1&mode=a&c_group=1&value_%25O.dtmCreated=" & dtmThisMonth & "&value_%2BO.chrStatus=S%2BA%2BZ%2BX%2BC%2BH%2BK%2BE%2BR""</A>" & rsData("intCount") & "</A>"
	if strBrand = "" then
		aryStats(1,4) = SafeFormatCurrency("n/a", rsData("mnyTotal"), 2)
	end if
	strSQL = "SELECT COUNT(DISTINCT O.intID) AS intCount, ISNULL(SUM(O.mnyGrandTotal),0) AS mnyTotal FROM " & STR_TABLE_ORDER & " AS O" & strBrandClause & " " & strWhere & dtmThisYear & "'"
	rsData.close
	set rsData = nothing
	set rsData = gobjConn.execute(strSQL)
	'--------JRMCodeChange---------
	aryStats(1,5) = "<A HREF=""merchant/"&strBrand&"order_list.asp?preset=placedy&action=search&n_groups=1&mode=a&c_group=1&value_%25O.dtmCreated=" & dtmThisYear & "&value_%2BO.chrStatus=S%2BA%2BZ%2BX%2BC%2BH%2BK%2BE%2BR""</A>" & rsData("intCount") & "</A>"
	if strBrand = "" then
		aryStats(1,6) = SafeFormatCurrency("n/a", rsData("mnyTotal"), 2)
	end if
	rsData.close
	set rsData = nothing
end if

if CheckUserAccess(ACC_SHOPPER_VIEW) then
	aryStats(2,0) = "New Salespeople"
	strWhere = "FROM " & STR_TABLE_ORDER & " AS O, " & STR_TABLE_SHOPPER & " AS S WHERE O.intBillShopperID=S.intID AND O.chrStatus<>'D' AND S.chrStatus<>'D' AND O.dtmCreated <= S.dtmCreated AND O.dtmCreated >= '"
	strSQL = "SELECT COUNT(*) AS intCount " & strWhere & dtmThisWeek & "'"
	set rsData = gobjConn.execute(strSQL)
	aryStats(2,1) = rsData("intCount")
	aryStats(2,2) = ""
	rsData.close
	set rsData = nothing
	strSQL = "SELECT COUNT(*) AS intCount " & strWhere & dtmThisMonth & "'"
	set rsData = gobjConn.execute(strSQL)
	aryStats(2,3) = rsData("intCount")
	aryStats(2,4) = ""
	rsData.close
	set rsData = nothing
	strSQL = "SELECT COUNT(*) AS intCount " & strWhere & dtmThisYear & "'"
	set rsData = gobjConn.execute(strSQL)
	aryStats(2,5) = rsData("intCount")
	aryStats(2,6) = ""
	rsData.close
	set rsData = nothing
	
end if

const intAuditHours = 72

strSQL = "SELECT "
strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_INVENTORY & " WHERE chrStatus<>'D' AND chrType='I') AS intItemCount,"
strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_INVENTORY & " WHERE chrStatus<>'D' AND chrType='A') AS intFolderCount,"
strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_INVENTORY & " WHERE chrStatus<>'D' AND chrType='I' AND chrICFlag='Y' AND intStock < intLowStock) AS intLowItemCount,"
strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_INVENTORY & " WHERE chrStatus<>'D' AND chrType='I' AND chrICFlag='Y' AND intStock > intHighStock) AS intHighItemCount,"
strSQL = strSQL & "(SELECT COUNT(DISTINCT vchObject) FROM " & STR_TABLE_AUDIT & " WHERE dtmCreated >= '" & DateAdd("h", -intAuditHours, Now()) & "' AND chrStatus='A' AND chrType='S' AND vchAction='login') AS intUserLoginCount,"
strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_AUDIT & " WHERE dtmCreated >= '" & DateAdd("h", -intAuditHours, Now()) & "' AND chrStatus='A' AND chrType='S' AND vchAction='login') AS intUserSessionCount,"
strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_AUDIT & " WHERE dtmCreated >= '" & DateAdd("h", -intAuditHours, Now()) & "' AND chrStatus='A' AND chrType='S' AND vchAction='loginfail') AS intBadLoginCount"
set rsData = gobjConn.execute(strSQL)
intItemCount = rsData("intItemCount")
intFolderCount = rsData("intFolderCount")
intLowItemCount = rsData("intLowItemCount")
intHighItemCount = rsData("intHighItemCount")
intUserLoginCount = rsData("intUserLoginCount")
intUserSessionCount = rsData("intUserSessionCount")
intBadLoginCount = rsData("intBadLoginCount")
rsData.close
set rsData = nothing

DrawPage
response.end

sub DrawPage
	call DrawHeaderUpdate()

    dim rsSettings, strSQL, isPreBuyOpen, isPODOn, intCampaign, emailCount,rsData

    strSQL = "SELECT vchValue  FROM RNDC_Setting WHERE vchName='Pre-Buy Open' "
	set rsSettings = gobjConn.execute(strSQL)

    isPreBuyOpen = false

    if not rsSettings.Eof Then
        isPreBuyOpen = (rsSettings("vchValue")&"" = "true")
    End if 
	
	strSQL = "SELECT vchValue  FROM RNDC_Setting WHERE vchName='POD On' "
	set rsSettings = gobjConn.execute(strSQL)

    isPODOn = false

    if not rsSettings.Eof Then
        isPODOn = (rsSettings("vchValue")&"" = "true")
    End if 

    Set rsSettings = Nothing

    strSQL = "SELECT TOP 1 intID FROM RNDC_INV WHERE chrType='C' AND chrStatus='A'" 
    set rsData = gobjConn.execute(strSQL) 
	
	if not rsData.EOF then
		intCampaign = rsData("intID")

		strSQL = "SELECT COUNT(DISTINCT vchEmail) FROM RNDC_SHOPPER S INNER JOIN RNDC_ORDER O ON O.intShopperID=S.intID INNER JOIN RNDC_LINEITEM L ON O.intID=L.intOrderID INNER JOIN RNDC_INV I ON L.intInvId=I.intID WHERE I.intCampaign=" & intCampaign
		set rsData = gobjConn.execute(strSQL) 
		emailCount = rsData(0)
	else
		emailCount = 0
	end if
%>

<%= font(5) %>System Summary for <%= STR_SITE_NAME %></FONT><BR>
<%= spacer(1,5) %><BR>
<% stlDateBar %>
<TABLE BORDER=0 CELLPADDING=5 CELLSPACING=0 WIDTH="100%">
<TR>
	<TD>
	<%
	 if CheckUserAccess(ACC_INVENTORY_VIEW) then 
	 	response.write "Current inventory contains <B>" & intItemCount & " products</B> in <B>" & intFolderCount & " folders</B>. "
		if intLowItemCount > 0 then
			response.write "<B>" & intLowItemCount & " products</B> are below stock thresholds"
			if intHighItemCount > 0 then
				response.write " and "
			else
				response.write "."
			end if
		end if
		if intHighItemCount > 0 then
			response.write "<B>" & intHighItemCount & " products</B> are overstocked."
		end if
		response.write "<BR>"
	end if
	if CheckUserAccess(ACC_ADMIN) then
		response.write "<B>" & intuserLoginCount & " users</B> have logged-in during the past " & intAuditHours & " hours for a total of <B>" & intUserSessionCount & " sessions</B>."
	end if
	%>
	</TD>
</TR>
</TABLE>
<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVLtGrey) %>">
<TR>
	<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cWhite %>">
	<TR>
		<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%">
		<TR BGCOLOR="<%= cVLtYellow %>">
			<TD>&nbsp;</TD>
			<TD COLSPAN=2 ALIGN=CENTER><B>This Week</B></TD>
			<TD>&nbsp;</TD>
			<TD COLSPAN=2 ALIGN=CENTER><B>This Month</B></TD>
			<TD>&nbsp;</TD>
			<TD COLSPAN=2 ALIGN=CENTER><B>This Year</B></TD>
			<TD>&nbsp;</TD>
		</TR>
		
<%
	dim c, blnHasDrawnItem
	blnHasDrawnItem = false
	for c = 0 to 2
		if aryStats(c,0) <> "" then
			if blnHasDrawnItem then
			%>
		<TR>
			<TD COLSPAN=10><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVVLtGrey) %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
		</TR>
			<%
			end if
			%>
		<TR>
			<TD><%= font(2) %><%= aryStats(c,0) %></B></TD>
			<TD ALIGN="right"><%= aryStats(c,1) %></TD>
			<TD ALIGN="right"><%= aryStats(c,2) %></TD>
			<TD>&nbsp;</TD>
			<TD ALIGN="right" BGCOLOR="<%= cVVLtGrey %>"><%= aryStats(c,3) %></TD>
			<TD ALIGN="right" BGCOLOR="<%= cVVLtGrey %>"><%= aryStats(c,4) %></TD>
			<TD BGCOLOR="<%= cVVLtGrey %>">&nbsp;</TD>
			<TD ALIGN="right"><%= aryStats(c,5) %></TD>
			<TD ALIGN="right"><%= aryStats(c,6) %></TD>
		</TR>
<%
			blnHasDrawnItem = true
		end if
	next
%>
		</TABLE></TD>
	</TR>
	</TABLE></TD>
</TR>
</TABLE>
<%
	if CheckUserAccess(ACC_ADMIN) then
		if intBadLoginCount > 0 then
%>
<BR>
<TABLE BORDER="0" CELLPADDING="4" CELLSPACING="1" BGCOLOR="<%= cRed %>" WIDTH="100%">
<TR BGCOLOR="<%= cWhite %>" ALIGN=CENTER>
	<TD>There have been <B><A HREF="<%= absbase %>admin/merchant/audit_list.asp"><%= fontx(1,1,cRed) %><%= intBadLoginCount %> failed login attempts</FONT></A></B> during this period.</TD>
</TR>
</TABLE>

    



<%
		end if
	end if
%>
<center>
<table>
    <tr>
        <td valign="top">
        
<form action="main.asp" style="margin-top:20px;">
<input type="hidden" name="send_message" value="true" />
    <center>
      <input type="submit" value="Send Closed Email" style="height:50px;width:250px;border:1px solid #000;background-color:black;color:White;font-weight:bold;font-size:larger;" onclick="return confirm('This will send emails to all sales people.\r\n A total of <%= emailCount %> emails will be sent.');" />
    </center>
    </form>
<form action="main.asp" style="margin-top:20px;">
<input type="hidden" name="toggle_prebuy" value="true" />
    <center>
    <%
        if isPreBuyOpen then
    %>
        <input type="submit" value="Close Pre-Buy Ordering" style="height:50px;width:250px;border:1px solid #000;background-color:red;color:White;font-weight:bold;font-size:larger;" />

        <%
        else
         %>
          <input type="submit" value="Open Pre-Buy Ordering" style="height:50px;width:250px;border:1px solid #000;background-color:green;color:White;font-weight:bold;font-size:larger;" />
    <%
    end if
    %>
    
    </center>
    </form>
	
<form action="main.asp" style="margin-top:20px;">
<input type="hidden" name="toggle_POD" value="true" />
    <center>
    <%
        if isPODOn then
    %>
		<input type="submit" value="Turn POD Off" style="height:50px;width:250px;border:1px solid #000;background-color:green;color:White;font-weight:bold;font-size:larger;" />
        

        <%
        else
         %>
        <input type="submit" value="Turn POD On" style="height:50px;width:250px;border:1px solid #000;background-color:red;color:White;font-weight:bold;font-size:larger;" />
    <%
    end if
    %>
    
    </center>
    </form>

    <form action="../store/default.asp" style="margin-top:20px;">
<input type="hidden" name="action" value="edit_prebuy" />
    <center>
        <input type="submit" value="Edit Pre-buy Page Text" style="height:50px;width:250px;border:1px solid #000;background-color:black;color:White;font-weight:bold;font-size:larger;" />

    </center>
    </form>

    <form action="../store/default.asp" style="margin-top:20px;">
<input type="hidden" name="action" value="edit_cover" />
    <center>
        <input type="submit" value="Edit Cover Page Text" style="height:50px;width:250px;border:1px solid #000;background-color:black;color:White;font-weight:bold;font-size:larger;" />

    </center>
    </form>

        </td>
        <td>&nbsp;</td>
        <td  valign="top">
        
    <form action="../store/default.asp" style="margin-top:20px;">
<input type="hidden" name="action" value="edit_faq" />
    <center>
        <input type="submit" value="Edit FAQs" style="height:50px;width:250px;border:1px solid #000;background-color:black;color:White;font-weight:bold;font-size:larger;" />

    </center>
    </form>

<form action="../store/default.asp" style="margin-top:20px;">
<input type="hidden" name="action" value="edit_scroll" />
    <center>
        <input type="submit" value="Edit Welcome Message" style="height:50px;width:250px;border:1px solid #000;background-color:black;color:White;font-weight:bold;font-size:larger;" />

    </center>
    </form>
	

<form action="../store/default.asp" style="margin-top:20px;">
<input type="hidden" name="action" value="edit_top_dasboard_text" />
    <center>
        <input type="submit" value="Edit Top Dashboard Text" style="height:50px;width:250px;border:1px solid #000;background-color:black;color:White;font-weight:bold;font-size:larger;" />

    </center>
    </form>
	
<form action="../store/default.asp" style="margin-top:20px;">
<input type="hidden" name="action" value="edit_lower_dashboard_text" />
    <center>
        <input type="submit" value="Edit Lower Dashboard Text" style="height:50px;width:250px;border:1px solid #000;background-color:black;color:White;font-weight:bold;font-size:larger;" />

    </center>
    </form>
	
		<form action="../store/default.asp" style="margin-top:20px;">
<input type="hidden" name="action" value="edit_image_text" />
    <center>
        <input type="submit" value="Edit Dashboard Image Text" style="height:50px;width:250px;border:1px solid #000;background-color:black;color:White;font-weight:bold;font-size:larger;" />

    </center>
    </form>

        </td>
    </tr>
</table>
</CENTER>
<%
end sub
%>