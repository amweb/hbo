$(document).ready(function() {
    set_button_events();
    
    $(".fancybox").fancybox();
    $(".page-content").css("height", "890px");
});

// changes the source of the iframe to show the desired profile
// after clicking the create a profile button or after clicking
// a certain profile button
function change_iframe_src(name, id, old) {
    $("iframe").attr("src", "");
    
    if (old) {
        $("iframe").attr("src", "profile.asp?viewprofile=yes&" + 
        "layout=" + $("#layout_id").text() + 
        "&name=" + name +
        "&id=" + id);
    } else {
        $("iframe").attr("src", "profile.asp?viewprofile=no&" + 
        "layout=" + $("#layout_id").text() + 
        "&name=" + name +
        "&id=" + id);
    }
}

// takes in a name when creating a profile and stores it
// data is the intid of the newly created profile
function profile_name_prompt() {
    var profile_name = "";

    while (profile_name == ""){
        profile_name = prompt("Name the profile: ");
    };
    
    if (profile_name == null) {
        return;
    }

    $.post("functions.asp",
    {
        run: "create_new_profile",
        name: profile_name,
        layout: $("#layout_id").text()
    },
    function(data) {
        change_iframe_src(profile_name, data, false);
        update_profile_list();
    });
}

function update_profile_list() {
    $.post("create_profile.asp",
    {
        run: "get_profiles",
        layout: $("#layout_id").text()
    },
    function(data) {
        $("#select_profile").html(data);
        set_button_events();
        highlight_after_update();
    });
}

function set_button_events() {
    // change the color of the button to show what profile is currently
    // being viewed
    $(".btn").click(function() {
        $(".btn").css("background-color", "#428bca");
        $(this).css("background-color", "#ed1d24");
    
    });
    
    $("#create_profile_button").click(function() {
        profile_name_prompt();
    });
    
     // show a pre-existing profile
    $(".profile").click(function() {
        change_iframe_src(this.value, this.id, true);
    });
}

// highlight button of newly added profile
// go up to the parent, then the next parent
// which has a child that is the newly created
// profile button
function highlight_after_update() {
    var parent = $("#create_profile_button").parent();
    var next_parent = parent.next();   
    var new_profile_button = next_parent.children();
    
    new_profile_button.css("background-color", "#ed1d24");
}
