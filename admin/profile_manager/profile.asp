<!--#include file="../../config/incInit.asp"-->

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>HBO Resource Center | Dome Printing</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/global/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/global/plugins/bootstrap/css/sticky-footer.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>/assets/global/plugins/bootstrap-multiselect/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css"/>

<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="<%= virtualbase %>assets/global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="<%= virtualbase %>assets/global/css/components.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/admin/layout/css/themes/light2.css" rel="stylesheet" type="text/css" />
<link href="<%= virtualbase %>assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<script src="<%= virtualbase %>assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script src="<%= virtualbase %>assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js" type="text/javascript"></script>
<script src="profile.js"></script>
<script>
        jQuery(document).ready(function() {
			jQuery("#storenumber").multiselect({
                selectedText: "# of # selected",
                nonSelectedText: "Store"   
            });
			jQuery("#state").multiselect({
                selectedText: "# of # selected",
                nonSelectedText: "State"
            });
        });
    </script>
<style>
    .CSSTableGenerator {
        margin:0px;padding:0px;
        width:100%;
	
    }.CSSTableGenerator table{
        border-collapse: collapse;
        border-spacing: 0;
        width:100%;
        height:100%;
        margin:0px;padding:0px;
    }.CSSTableGenerator tr:last-child td:last-child {
        -moz-border-radius-bottomright:0px;
        -webkit-border-bottom-right-radius:0px;
        border-bottom-right-radius:0px;
    }
    .CSSTableGenerator table tr:first-child td:first-child {
        -moz-border-radius-topleft:0px;
        -webkit-border-top-left-radius:0px;
        border-top-left-radius:0px;
    }
    .CSSTableGenerator table tr:first-child td:last-child {
        -moz-border-radius-topright:0px;
        -webkit-border-top-right-radius:0px;
        border-top-right-radius:0px;
    }.CSSTableGenerator tr:last-child td:first-child{
        -moz-border-radius-bottomleft:0px;
        -webkit-border-bottom-left-radius:0px;
        border-bottom-left-radius:0px;
    }.CSSTableGenerator tr:hover td{
	
    }
    .CSSTableGenerator tr:nth-child(odd){ background-color:#f0f0f0; }
    .CSSTableGenerator tr:nth-child(even)    { background-color:#ffffff; }
    .CSSTableGenerator tr:nth-child(2), .CSSTableGenerator tr:nth-child(2) td{ background-color:#393333;color:#fff !important; }
    .CSSTableGenerator td{vertical-align:middle;text-align:left;
        padding:7px;
        font-size:10pt;
        font-family: 'Open Sans', sans-serif;
        font-weight:normal;
        color:#000000;
        }
</style>
</head>
<body>
<form method="post">
<%

dim intStoreID, vchState, strStoreSearch, rstemp, strsql, save_sql, rs, query

intStoreID = Request("storeid")
strStoreSearch = Request("store_search")
vchState = Request("state")

OpenConn
DrawProfilePage

function DrawProfilePage
%>
<table class="CSSTableGenerator">
    <% if request("viewprofile") = "yes" then %>
        
        <tr>           
            <td colspan="4">
                <input id="add_stores" type="button" value="Edit Profile" class="btn-group-vertical" style="border:1px solid #ccc;height:34px;margin-left:2px;width:115px;font-weight:bold;background-color:#f0f0f0;">
                <input id="delete_profile" type="button" value="Delete Profile" class="btn-group-vertical" style="border:1px solid #ccc;height:34px;margin-left:2px;width:115px;font-weight:bold;background-color:#f0f0f0;">
            </td>
        </tr>
        <% DrawTableHeaders %>
        
        <% if ProfileEmpty then %>
            <tr>
                <td colspan="3">There are no stores in this profile.</td>
            </tr>
        <% else %>
            <% DrawStoresInProfile %>
        <% end if %>
    
    
    <% else %>
    
        <% 
            DrawSearchOptions 
            DrawTableHeaders 
        
            strsql = ""
            strSQL = strSQL & "SELECT intID, vchLabel, vchAddress1, vchAddress2, vchcity, vchState, vchZip "
            strSQL = strSQL & "FROM " & STR_TABLE_SHOPPER & " "
            strSQL = strSQL & "WHERE chrtype = 'S' "
            strSQL = strSQL & "AND chrStatus = 'A' "
            strSQL = strSQL & "AND (vchfirstname LIKE '%" & strStoreSearch & "%' OR "
            strSQL = strSQL & "vchlastname LIKE '%" & strStoreSearch & "%' OR "
            strSQL = strSQL & "vchAddress1 LIKE '%" & strStoreSearch & "%' OR "
            strSQL = strSQL & "vchAddress2 LIKE '%" & strStoreSearch & "%' OR "
            strSQL = strSQL & "vchState LIKE '%" & strStoreSearch & "%' OR "
            strSQL = strSQL & "vchCity LIKE '%" & strStoreSearch & "%' OR "
            strSQL = strSQL & "vchZip LIKE '%" & strStoreSearch & "%' ) "
            if request("storeid") <> "" then
                strSQL = strSQL & "AND intID IN (" & request("storeid") & ") "
            end if
            if request("state") <> "" then
                strSQL = strSQL & " AND vchState = '" & request("state") & "' "
            end if  
            strSQL = strSQL & "AND intLayout = " & request("layout") & " "                
            strSQL = strSQL & "ORDER BY intid "
           
            ' response.write strsql : response.end
            set rsTemp = gobjConn.execute(strSQL)
        
            while not rsTemp.Eof 
        %>
            
            <tr>
                <td><%= rsTemp("vchLabel") %></td>
                <td>
                    <%= rsTemp("vchAddress2") %><br>
                    <%= rsTemp("vchCity") %>, <%= rsTemp("vchState") %>&nbsp;<%= rsTemp("vchZip") %>
                </td>
                
                <td colspan="2">
                    <% if InProfile(rstemp("intid")) then %>
                        <input class="check_one" type="checkbox" id="<%= rsTemp("intID") %>" checked>
                    <% else %>
                        <input class="check_one" type="checkbox" id="<%= rsTemp("intID") %>">
                    <% end if %>
                </td>
            </tr>
        <%  
            rsTemp.MoveNext
            wend
        %>
        
    <% end if %>
</table>
</form>
</body>
</html>
<%
end function

function ProfileEmpty
    query = query & "SELECT TOP 1 intprofileid FROM " & STR_TABLE_PROFILE_MAPPER & " "
    query = query & "WHERE intprofileid = " & request("id") & " "
    set rs = gobjConn.execute(query)
    
    ProfileEmpty = true
    
    if not rs.eof then 
        ProfileEmpty = false
    end if
end function

' is the store, in the current profile
function InProfile(store)
    query = ""
    query = query & "SELECT TOP 1 intstoreshopperid "
    query = query & "FROM HBO_profilemapper "
    query = query & "WHERE intprofileid = " & request("id") & " "
    query = query & "AND intstoreshopperid = " & store & " "
    set rs = gobjConn.execute(query)
    
    InProfile = false
    if not rs.eof then
        InProfile = true
    end if 
end function

function DrawStoresInProfile 
    query = ""
    query = query & "SELECT vchlabel as name, "
    query = query & "s.vchaddress2 as address, "
    query = query & "s.vchcity as city, "
    query = query & "s.vchstate as state, "
    query = query & "s.vchzip as zip, "
    query = query & "s.intid as store "
    query = query & "FROM HBO_shopper AS s "
    query = query & "INNER JOIN HBO_profilemapper AS pm "
    query = query & "ON s.intid = pm.intstoreshopperid "
    query = query & "INNER JOIN HBO_profiles AS p "
    query = query & "ON p.intid = pm.intprofileid "
    query = query & "WHERE p.intid = " & request("id") & " "
    query = query & "ORDER BY s.intid "
    set rs = gobjConn.execute(query)
%>    
    <% while not rs.eof %>
        <tr>
            <td><%= rs("name") %></td>
            <td>
                <%= rs("address") %><br>
                <%= rs("city") %>, <%= rs("state") %>&nbsp;<%= rs("zip") %>
            </td>
            <td>&nbsp;</td>
        </tr>
        <% rs.movenext %>
    <% wend %>
<%    
end function

function DrawSearchOptions
%>
    <tr>
        <td colspan="4">
            <select class="multiselect" name="storeid" id="storenumber" multiple="multiple" onchange="this.form.submit()">
                <%
                    strSQL = strSQL & "SELECT intID, vchLabel "
                    strSQL = strSQL & "FROM " & STR_TABLE_SHOPPER & " "
                    strSQL = strSQL & "WHERE chrtype = 'S' "
                    strSQL = strSQL & "AND chrStatus = 'A' "
                    strSQL = strSQL & "AND (vchfirstname LIKE '%" & strStoreSearch & "%' OR "
                    strSQL = strSQL & "vchlastname LIKE '%" & strStoreSearch & "%' OR "
                    strSQL = strSQL & "vchAddress1 LIKE '%" & strStoreSearch & "%' OR "
                    strSQL = strSQL & "vchAddress2 LIKE '%" & strStoreSearch & "%' OR "
                    strSQL = strSQL & "vchState LIKE '%" & strStoreSearch & "%' OR "
                    strSQL = strSQL & "vchCity LIKE '%" & strStoreSearch & "%' OR "
                    strSQL = strSQL & "vchZip LIKE '%" & strStoreSearch & "%' ) "
                    if request("state") <> "" then
                        strSQL = strSQL & " AND vchState = '" & request("state") & "' "
                    end if                                        
                    strSQL = strSQL & "AND intLayout = " & request("layout") & " "
                    strSQL = strSQL & "ORDER BY vchlabel "
                    
                    set rsTemp = gobjConn.execute(strSQL)
                  
                    while not rsTemp.Eof
                        %>
                        <option value="<%= rsTemp("intID") %>"<%= iif(InStr(intStoreID,rsTemp("intID"))>0," SELECTED","") %>><%= rsTemp("vchLabel") %></option>
                        <%
                        rsTemp.movenext
                    wend
                %>
            </select>

            <select class="multiselect" name="state" id="state" onchange="this.form.submit()">
                <option value="">State</option>
                <%
                strSQL = "SELECT distinct(vchState) FROM " & STR_TABLE_SHOPPER & " WHERE chrStatus='A' "
                strSQL = strSQL & " AND intLayout = " & request("layout") & " "
                set rsTemp = gobjConn.execute(strSQL)
                while not rsTemp.Eof
                    %>
                    <option value="<%= rsTemp("vchState") %>"<%= iif(InStr(vchState,rsTemp("vchState"))>0," SELECTED","") %>><%=rsTemp("vchState") %></option>
                    <%
                    rsTemp.movenext
                wend
                %>
            </select>
            
            <input type="text" placeholder="Enter Search Details" name="store_search" value="<%= strStoreSearch %>" class="btn-group-vertical" style="height:34px;width:200px;border:1px solid #ccc;padding:5px;">
            <input type="submit" value="Search" class="btn-group-vertical" style="border:1px solid #ccc;height:34px;margin-left:2px;width:80px;font-weight:bold;background-color:#f0f0f0;">
            <input id="view_profile" type="submit" value="View Profile" class="btn-group-vertical" style="border:1px solid #ccc;height:34px;margin-left:2px;width:125px;font-weight:bold;background-color:#f0f0f0;">
        </td>
    </tr>
<%
end function

function DrawTableHeaders
%>
    <% if request("viewprofile") = "yes" then %>
        <tr>
            <td>Viewing profile:<span id="name" style="font-weight: bold !important;"> <%= request("name") %></span></td>
            <td><span style="display: none;" id="id"> <%= request("id") %></span>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><u>Store</u></td>
            <td><u>Address</u></td>
            <td>&nbsp;</td>
        </tr>
    <% else %>
        <tr>
            <td>Editing profile: <span id="name" style="font-weight: bold;"><%= request("name") %></span></td>
            <td><span style="display: none;" id="id"> <%= request("id") %></span>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td><u>Store</u></td>
            <td><u>Address</u></td>
            <td><input id="check_all" type="checkbox">Check/Uncheck All</td>
        </tr>
    <% end if %>
    
<%
end function
%>