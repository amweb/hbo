$(document).ready(function(){
    // addition/removal of one store
    $(".check_one").change(function(){
        if (this.checked){
            AddStore(this.id);
        } else{
            RemoveStore(this.id);
        }
    });
    
    // mass addition/remove of stores from profile
    $("#check_all").click(function(){
        if (this.checked){
            $(".check_one").each(function(){
                if (!this.checked){
                    this.checked = true;
                    AddStore(this.id);
                }
            });
        } else {
            $(".check_one").each(function(){
                if (this.checked){
                    this.checked = false;
                    RemoveStore(this.id);
                }
            });
        }
    });
    
    // switch between searching for stores to add, and viewing stores that
    // you have in you profile
    $("#add_stores").click(function() {
        var current_url = window.location.href;
        window.location.replace(current_url.replace("yes", "no"));
        return false;
    });
    
    $("#view_profile").click(function() {
        var current_url = window.location.href;
        window.location.replace(current_url.replace("no", "yes"));
        return false;
    });
    
    $("#delete_profile").click(function() {
        RemoveProfile();
    });
});

function AddStore(store){
    $.post("functions.asp",
    {
        run: "add_store",
        profile: $("#id").text(),
        store: store
    });
}

function RemoveStore(store){
    $.post("functions.asp",
    {
        run: "remove_store",
        profile: $("#id").text(),
        store: store
    });
}

function RemoveProfile() {
    var delete_check = "";
    
    while (delete_check != null && delete_check != "delete") {
        delete_check = prompt('Type "delete" and press "OK" to delete the profile.');;
    };
    
    if (delete_check == null) {
        return false;
    }
    
    $.post("functions.asp",
    {
        run: "remove_profile",
        profile: $("#id").text(),
    });
    
    // $('#select_profile', window.parent.location).html("hahaha");
    window.parent.location.reload();
}

