<!--#include file="../../config/incInit.asp"-->

<%
OpenConn
DrawHeader "", ""
DrawPage
DrawFooter ""

function DrawLayouts
    dim query, rsinput, imagelocation
    
    query = query & "SELECT intid, vchlayoutname, vchimagelocation FROM " & STR_TABLE_LAYOUTS & " "
    set rsinput = gobjConn.execute(query)
%>    
    <% while not rsinput.eof %>
    
        <div style="float:left;width:262px;margin-right:px;margin-left:30px;margin-bottom:30px;padding:20px;height:320px;">
            <table width="100%">
                <tr>
                    <td align="center" valign="middle" style="width:200px">
                    
                    <% 
                        if isnull(rsInput("vchimagelocation")) then 
                            imagelocation = virtualbase & "images/nophoto_big.gif" 
                        else 
                            imagelocation = virtualbase & "images/layouts/" & rsInput("vchimagelocation")
                        end if
                    %>
                    
                    <div style="background-image:url(<%= imagelocation %>);width:170px;height:130px;margin-bottom:5px;background-repeat:no-repeat;background-size:cover;"></div>
                    <a class="fancybox" rel="group" href="<%= imagelocation %>" style="background-color:#fff;">
                        <img src="<%= virtualbase & "images/magnifier_medium_left.png" %>" style="margin-bottom:20px;height:25px;width:25px;">
                    </a>
                    </td>
                </tr>
                <tr>
                    <td><strong style="word-wrap:break-word;width:100px;"><%= rsInput("vchlayoutname") %></strong></td>
                </tr>
                <tr>
                    <td><input type="button" value="Create/Edit Profile Using This Layout" onclick="window.location.href='create_profile.asp?drawpage=yes&layout=<%= rsinput("intid") %>';"></td>
                </tr>
            </table>
        </div>
        
        <% rsinput.movenext %>
    <% wend %>
<%
end function

function DrawPage
%>
<style>
        .page-content {
            margin-bottom: 59px;
        }
        .page-breadcrumb {
            width: 1180px;
            border-top: 5px solid #13436D !important;
        }
    </style>
    <div class="page-content-wrapper">
        <!-- BEGIN PAGE HEADER-->
		<div class="page-content">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">Select a Layout</h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<%= virtualbase %>store/dashboard.asp">Home</a>
                            <i class="fa fa-angle-right"></i>
                            <a>Profile Manager</a>
                            <i class="fa fa-angle-right"></i>
                            <a>Select a Layout</a>
                        </li>
                    </ul>
                </div>
            </div>
            <% DrawLayouts %>
            <script>
            $(".fancybox").fancybox({
                arrows : false
            });
            </script>
        </div>
    </div>
        <!-- END PAGE HEADER-->
<%                    
end function
%>