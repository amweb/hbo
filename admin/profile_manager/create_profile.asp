<!--#include file="../../config/incInit.asp"-->

<%
dim query
dim rs

OpenConn

if request("drawpage") = "yes" then
    DrawHeader "", "" 
    DrawPage 
    DrawFooter "" 
end if

select case request.form("run")
    case "create_profile"
        DrawCreateProfile
    case "get_profiles"
        DrawSelectProfile
end select

function DrawPage
%>
    <style>
        .page-content {
            margin-bottom: 59px;
        }
        .page-breadcrumb {
            width: 1180px;
            border-top: 5px solid #13436D !important;
        }
    </style>

    <script src="create_profile.js"></script>
    
    <div class="page-content-wrapper">
        <!-- BEGIN PAGE HEADER-->
		<div class="page-content">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">Create/Edit Profile</h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<%= virtualbase %>store/dashboard.asp">Home</a>
                            <i class="fa fa-angle-right"></i>
                            <a>Profile Manager</a>
                            <i class="fa fa-angle-right"></i>
                            <a href="<%= virtualbase %>admin/profile_manager/select_layout.asp">Select a Layout</a>
                            <i class="fa fa-angle-right"></i>
                            <a>Create/Edit Profile</a>
                            <i class="fa fa-angle-right"></i>
                            <a><%= " Layout " & request("layout") %></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div id="current_layout" style="text-align: center;width: 250px;height: 250px;border: 1px solid black;position: relative;top: -25px;">
                <table width="100%">
                    <tr>
                        <td>
                            <img src="<%= virtualbase %>images/<%= DrawCurrentLayout %>">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <a class="fancybox" rel="group" href="<%= virtualbase %>images/<%= DrawCurrentLayout %>" style="background-color:#fff;">
                                <img src="<%= virtualbase & "images/magnifier_medium_left.png" %>" style="height:25px;width:25px;">
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Layout<%= " " & request("layout") %></b>
                            <span id="layout_id" style="display: none;"><%= request("layout") %></span>
                        </td>
                    </tr>    
                </table>
            </div>
            <div id="select_profile" style="width: 250px;height: 500px;border: 1px solid black;position: relative;top: -25px;overflow-y: scroll;">
                <% DrawSelectProfile %>
            </div>
            <div id="current_profile" style="width: 930px;height: 750px;border: 1px solid black;position: relative;left: 250px; top: -775px;">
                <iframe style="width: 100%;height: 100%;" src=""></iframe>
            </div>
        </div>
    </div>
        <!-- END PAGE HEADER-->
<%                    
end function

function DrawCurrentLayout
    query = query & "SELECT vchlayoutname, vchimagelocation FROM " & STR_TABLE_LAYOUTS & " WHERE intid =" & request("layout")
    set rs = gobjConn.execute(query)
    
    if not rs.eof then
        if isnull(rs("vchimagelocation")) then
            DrawCurrentLayout = "nophoto_big.gif" 
        else
            DrawCurrentLayout = "layouts/" & rs("vchimagelocation")
        end if
    end if
end function

function GetProfileName
    query = query & "SELECT TOP 1 FROM " & STR_TABLE_PROFILES & " "
    set rs = gobjConn.execute(query)
    
    if not rs.eof then
        if isnull(rs("vchimagelocation")) then
            DrawCurrentLayout = "nophoto_big.gif" 
        else
            DrawCurrentLayout = "layouts/" & rs("vchimagelocation")
        end if
    end if

end function

function DrawSelectProfile
    ' first, give the option to create a new profile
    ' afterwards, give the option to select from existing profile for editing/viewing
%>
    <div style="margin: 5px;">
        <input id="create_profile_button" type="button" class="btn btn-primary btn-block" value="Create a Profile">
    </div>
    <% DrawPreExistingProfiles %>
<%
end function

function DrawPreExistingProfiles
    query = ""
    query = query & "SELECT * "
    query = query & "FROM " & STR_TABLE_PROFILES & " "
    
    if isempty(request.form("layout")) then
        query = query & "WHERE intlayoutid = " & request("layout") & " "
    else
        query = query & "WHERE intlayoutid = " & request.form("layout") & " "
    end if
    
    query = query & "AND chrstatus = 'A' " 
    query = query & "ORDER BY vchprofilename " 
    set rs = gobjConn.execute(query)
%>
    <% while not rs.eof %>
        <div style="margin: 5px;">
            <input id="<%= rs("intid") %>" type="button" class="btn btn-primary btn-block profile" value="<%= rs("vchprofilename") %>">
        </div>
        <% rs.movenext %>
    <% wend %>
<%
end function 
%>