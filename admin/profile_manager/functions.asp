<!--#include file="../../config/incInit.asp"-->

<%
dim rs
dim strsql

openconn

select case request.form("run")
    case "create_new_profile"
        response.write CreateNewProfile
    case "add_store"
        AddStore
    case "remove_store"
        RemoveStore
    case "remove_profile"
        RemoveProfile
end select

function CreateNewProfile
    ' create the profile
    strsql = strsql & "SET NOCOUNT ON;INSERT INTO " & STR_TABLE_PROFILES & " (intlayoutid, vchprofilename) "
    strsql = strsql & "VALUES (" & request.form("layout") & ", '" & request.form("name") & "');SET NOCOUNT OFF;SELECT SCOPE_IDENTITY() ID"
    set rs = gobjConn.execute(strsql)

    ' get the newly create profile's id
    if not rs.eof then
        CreateNewProfile = rs("id")
    end if
end function

function AddStore
    strsql = strsql & "INSERT INTO " & STR_TABLE_PROFILE_MAPPER & " "
    strsql = strsql & "VALUES (" & request.form("profile") & ", '" & request.form("store") & "')"
    set rs = gobjConn.execute(strsql)
end function

function RemoveStore
    strsql = strsql & "DELETE FROM " & STR_TABLE_PROFILE_MAPPER & " "
    strsql = strsql & "WHERE intprofileid = " & request.form("profile") & " "
    strsql = strsql & "AND intstoreshopperid = " & request.form("store") & " "
    set rs = gobjConn.execute(strsql)
end function

' don't actually delete the profile informoration just incase you need it
' just set it to inactive
function RemoveProfile
    strsql = strsql & "UPDATE " & STR_TABLE_PROFILES & " "
    strsql = strsql & "SET chrstatus = 'I' "
    strsql = strsql & "WHERE intid = " & request.form("profile") & " "
    set rs = gobjConn.execute(strsql)
end function




















%>