<!--#include file="./config/incInit.asp"-->

<%
'profile_assignment.asp

OpenConn
DrawHeader "", ""

if Session(SESSION_MERCHANT_UID)&"" = "" then
	response.redirect virtualbase & "store/dashboard.asp"
end if

dim intProfileID
if isNumeric(Request("intProfileID")) then
	intProfileID = ProtectSQL(Request("intProfileID"))
end if

if request.form("assignvalues")&"" = "" then
	DrawLandingPage
else
	AssignValuesToProfile intProfileID
end if


DrawFooter ""


function DrawLandingPage
	dim strSQL, strUpdateSQL, strSearchFilterSQL, strProfileName, intProfileID, rsStoreList, rsStoreInfo, intSelectionCount
	dim intStoreID, intDistrict, vchDivision
	
	if isNumeric(Request("intProfileID")) then
		intProfileID = ProtectSQL(Request("intProfileID"))
	end if
	strSQL = "SELECT [vchProfileName] FROM " & STR_TABLE_PROFILES & " WHERE [intID]=" & intProfileID
	set rsStoreInfo = gobjConn.execute(strSQL)
%>
	<div class="page-content-wrapper">
        <!-- BEGIN PAGE HEADER-->
		<div class="page-content">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">Assign an address</h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<%= virtualbase %>store/dashboard.asp">Home</a>
                            <i class="fa fa-angle-right"></i>
                            <a href="<%= virtualbase %>admin/profile_manager.asp">Profile Manager</a>
                            <i class="fa fa-angle-right"></i>
                            <a href="#">Assign an address</a>
                        </li>
                    </ul>
                </div>
            </div>
			<div id="profilepopout" data-dialog-minwidth="900">
				<h3>Assign addresses to profile <%= rsStoreInfo("vchProfileName") %></h3>
					<span class="counter pull-right"></span>
					<form id="jqSearch">
						<fieldset>
							<select name="intStoreID">
								<option value="">Store</option>
								<%
									strSQL = "SELECT DISTINCT [intStoreID] FROM " & STR_TABLE_SHOPPER & " WHERE [intStoreID] IS NOT NULL"
									set rsStoreInfo = gobjConn.execute(strSQL)
									intSelectionCount = 0
									do until rsStoreInfo.eof
										intSelectionCount = intSelectionCount + 1
										Response.Write "<option value='" & rsStoreInfo("intStoreID") & "' " & iif(cint(rsStoreInfo("intStoreID"))=intStoreID, "selected", "") & ">" & rsStoreInfo("intStoreID") & "</option>"
										rsStoreInfo.movenext
									loop
									if intSelectionCount > 0 then
										rsStoreInfo.MoveFirst()
									end if 
								%>
							</select>
							<select name="intDistrict">
								<option value="">District</option>
								<%
									strSQL = "SELECT DISTINCT [intDistrict] FROM " & STR_TABLE_SHOPPER & " WHERE [intDistrict] IS NOT NULL"
									set rsStoreInfo = gobjConn.execute(strSQL)
									do until rsStoreInfo.eof
										if isNumeric(Request.Form("intAssignSortDistrict")) then
											intDistrict = cint(Request.Form("intAssignSortDistrict"))
										else
											intDistrict = 0
										end if
										Response.Write "<option value='" & rsStoreInfo("intDistrict") & "' " & iif(cint(rsStoreInfo("intDistrict"))=intDistrict, "selected", "") & ">" & rsStoreInfo("intDistrict") & "</option>"
										rsStoreInfo.movenext
									loop
									if intSelectionCount > 0 then
										rsStoreInfo.MoveFirst()
									end if 
								%>
							</select>
							<select name="vchDivision">
								<option value="">Region</option>
								<%
									strSQL = "SELECT DISTINCT [vchDivision] FROM " & STR_TABLE_SHOPPER & " WHERE [vchDivision] IS NOT NULL"
									set rsStoreInfo = gobjConn.execute(strSQL)
									do until rsStoreInfo.eof
										Response.Write "<option value='" & rsStoreInfo("vchDivision") & "' " & iif(rsStoreInfo("vchDivision")&""=Request.Form("vchDivision")&"", "selected", "") & ">" & rsStoreInfo("vchDivision") & "</option>"
										rsStoreInfo.movenext
									loop
									if intSelectionCount > 0 then
										rsStoreInfo.MoveFirst()
									end if 
									%>
							</select>
							<input type="text" class="search form-control">
							<button type="button" id="addresssearchsubmit">Search</button>
							<button type="button" id="resetsearch">Reset</button>
						</fieldset>
					</form>
					<%
					strSearchFilterSQL = ""
					strSQL = "SELECT S.[intDistrict], S.[vchLabel], S.[vchLastName], S.[vchAddress1], S.[vchAddress2], S.[vchCity], S.[vchState], S.[vchZip], S.[intID], "
					strSQL = strSQL & "S.[intLayout], S.[intStoreID], S.[vchDivision], S.[chrStoreType], M.[intProfileID], L.[vchImageLocation] FROM "
					strSQL = strSQL & STR_TABLE_SHOPPER & " S LEFT JOIN " & STR_TABLE_LAYOUTS & " L ON S.intLayout = L.intID "
					strSQL = strSQL & " LEFT JOIN " & STR_TABLE_PROFILEMAPPER & " M ON S.[intID] = M.[intStoreShopperID] AND M.[intProfileID]=" & intProfileID
					strSQL = strSQL & " WHERE S.[chrType]='S' AND S.[chrStatus]='A'"
					if true=false then
						if Request.Form("intAssignmentSortStoreID")&"" <> "" then
							intStoreID = cint(ProtectSQL(Request.Form("intAssignmentSortStoreID")))
							strSearchFilterSQL = strSearchFilterSQL & " AND S.[intStoreID]=" & ProtectSQL(Request.Form("intAssignmentSortStoreID"))
						end if
						if Request.Form("intAssignSortDistrict")&"" <> "" then
							intDistrict = cint(Request.Form("intAssignSortDistrict"))
							strSearchFilterSQL = strSearchFilterSQL & " AND S.[intDistrict]=" & intDistrict & " "
						end if
						if Request.Form("vchDivision")&"" <> "" then
							strSearchFilterSQL = strSearchFilterSQL & " AND S.[vchDivision]='" & ProtectSQL(Request.Form("vchDivision")) & "' "
						end if
						if request.form("searchstore")&"" <> "" then
							strSearchFilterSQL = strSearchFilterSQL & " AND ( "
							strSearchFilterSQL = strSearchFilterSQL & "S.[vchLabel] LIKE '%" & request.form("searchstore") & "%' OR "
							strSearchFilterSQL = strSearchFilterSQL & "S.[vchLastName] LIKE '%" & request.form("searchstore") & "%' OR "
							strSearchFilterSQL = strSearchFilterSQL & "S.[vchAddress1] LIKE '%" & request.form("searchstore") & "%' OR "
							strSearchFilterSQL = strSearchFilterSQL & "S.[vchCity] LIKE '%" & request.form("searchstore") & "%' OR "
							strSearchFilterSQL = strSearchFilterSQL & "S.[vchState] LIKE '%" & request.form("searchstore") & "%' OR "
							strSearchFilterSQL = strSearchFilterSQL & "S.[vchZip] LIKE '%" & request.form("searchstore") & "%' OR "
							strSearchFilterSQL = strSearchFilterSQL & "S.[intStoreID] LIKE '%" & request.form("searchstore") & "%' "
							strSearchFilterSQL = strSearchFilterSQL & ") "
						end if
					end if
					strSQL = strSQL & strSearchFilterSQL
					set rsStoreList = gobjConn.execute(strSQL)
					'response.write "<p>" & strSQL & "</p>"
					%>
					<p id="assignmenterror">Please select at least one (1) profile.</p>
					<form action="<%= virtualbase %>admin/profile_assignment.asp" method="post" id="assignaddressestoprofile">
					<input type="hidden" name="intProfileID" value="<%= intProfileID %>">
					<input type="hidden" name="assignvalues" value="true">
					<div id="table-wrapper">
						<div id="table-scroll">
							<table id="assigntoprofile" class="results">
								<thead>
									<tr>
										<th>
											<input type="checkbox" name="selectAll" value="selectAll">
											<br>
											<label for="selectAll">Select/Deselect All</label>
										</th>
										<th>Store Name</th>
										<th>Store</th>
										<th>District</th>
										<th>Address</th>
										<th>Type</th>
										<th>Region</th>
										<th>Schematic</th>
									</tr>
								</thead>
								<tbody>
									<%
									while not rsStoreList.eof
										'iif(rsMatchingStores("intStoreID")=rsStoreList("intStoreID"), "checked", "")
									%>
									<tr visible="true">
										<td>
											<input
												type="checkbox"
												class="addstoretoprofile"
												data-storeid="<%= rsStoreList("intID") %>"
												data-profileid="<%= intProfileID %>"
												name="addstoretoprofile-<%= rsStoreList("intID") %>"
												<%
												if rsStoreList("intProfileID")&"" = intProfileID&"" then
													response.write "checked='true' "
												end if
												response.write " value='" & rsStoreList("intID") & "'"
												%>
											>
										</td>
										<td><%= rsStoreList("vchLastName") %></td>
										<td data-search-id="intStoreID"><%= rsStoreList("intStoreID") %></td>
										<td data-search-id="intDistrict"><%= rsStoreList("intDistrict") %></td>
										<td>
										<p>
											<%
											response.write rsStoreList("vchAddress1")
											if rsStoreList("vchAddress2")&""<>"" then
												response.write "&nbsp;" & rsStoreList("vchAddress2")
											end if
											%>
											<br>
											<%= rsStoreList("vchCity") %>, <%= rsStoreList("vchState") %>&nbsp;<%= rsStoreList("vchZip") %>
										</p>
										</td>
										<td>
										<%
										select case rsStoreList("chrStoreType")&""
											Case "T"
												response.write "Traditional"
											Case "N"
												response.write "Neighborhood"
											Case else
												response.write ""
										end select
										%>
										</td>
										<td data-search-id="vchDivision"><%= rsStoreList("vchDivision") %></td>
										<td>
											<a href="<%= virtualbase %>images/layouts/<%= rsStoreList("vchImageLocation") %>" class="layoutzoom" data-width="640"><img src="<%= virtualbase %>images/Profile-Icon.svg" alt="<%= rsStoreList("vchLastName") %>"></a>
										</td>
									</tr>
									<%
										rsStoreList.movenext
									wend
									%>
								</tbody>
							</table>
						</div>
					</div>
					<button type="Submit" value="Submit">Submit</button>
					<button type="reset" value="Cancel">Cancel</button>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	$(document).keypress(function(event){
		if (event.which == '13') {
			event.preventDefault();
		}
	});
	</script>
<%
end function
function AssignValuesToProfile(argIntProfileID)
	dim strSQL,	strStoreShopperIDValues, intIndividualCheckboxValue, intProfileID, strRequestFormKey
	intProfileID = argIntProfileID
	
	strSQL  = "DELETE FROM " & STR_TABLE_PROFILEMAPPER & " WHERE [intProfileID]=" & intProfileID & "; "
	
	strSQL = strSQL & "INSERT INTO " & STR_TABLE_PROFILEMAPPER & " ([intProfileID], [intStoreShopperID]) VALUES "
	for each strRequestFormKey in request.form
		if instr(strRequestFormKey, "addstoretoprofile-") > 0 then
			strSQL = strSQL & "(" & intProfileID & "," & ProtectSQL(request.form(strRequestFormKey)) & "), "
		end if
	Next
	strSQL = LEFT(strSQL, (LEN(strSQL)-2))
	gobjConn.execute(strSQL)
	strSQL = "UPDATE " & STR_TABLE_PROFILES & " SET [chrStatus]='A' WHERE [intID]=" & intProfileID
	gobjConn.execute(strSQL)
	%>
	<form id="profileassignmenttoprofilelist" action="<%= virtualbase %>admin/profile_list.asp" method="post" class="formautosubmit">
		<input type="hidden" name="profileid" value="<%= intProfileID %>">
	</form>
	<%
end function
%>