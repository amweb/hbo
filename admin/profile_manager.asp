<!--#include file="./config/incInit.asp"-->

<%
OpenConn

if Session(SESSION_MERCHANT_UID)&"" = "" then
	response.redirect virtualbase & "store/dashboard.asp"
end if

DrawHeader "", ""
DrawPage
DrawFooter ""

%>
<%

function DrawPage
	dim query, rsinput, imagelocation, intReturnToProfileNumber
	
	if request.form("returntoprofilelist")&""<>"" then
		intReturnToProfileNumber = request.form("returntoprofilelist")
	end if

	query = "SELECT intid, vchProfileName FROM " & STR_TABLE_PROFILES & " WHERE chrStatus='A' ORDER BY [intid] ASC"
	set rsinput = gobjConn.execute(query)
%>
    <div class="page-content-wrapper">
        <!-- BEGIN PAGE HEADER-->
		<div class="page-content">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">Profile Manager</h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<%= virtualbase %>store/dashboard.asp">Home</a>
                            <i class="fa fa-angle-right"></i>
                            <a href="<%= virtualbase %>admin/profile_manager.asp">Profile Manager</a>
                            <i class="fa fa-angle-right"></i>
                            <a>Select a Layout</a>
                        </li>
                    </ul>
                </div>
            </div>
			<div id="profilepopout" data-dialog-minwidth="500">
				<!-- Returned from profile: <%= intReturnToProfileNumber %>-->
				<form action="<%= virtualbase %>admin/profile_list.asp" method="post" class="profilemanager">
					<label for="profileid">Select Profile</label>
					<br>
					<select name="profileid">
						<% while not rsinput.eof %>
							<option value="<%= rsinput("intid") %>" <%= iif(intReturnToProfileNumber = rsinput("intid")&"", " selected", "") %>>Profile <%= rsinput("intid") %> - <%= rsinput("vchProfileName") %></option>
							<% rsinput.movenext %>
						<% wend %>
					</select>
					<br>
					<button type="submit" value="Submit">Submit</button>
					<button class="redirecttodashboard" type="reset">Cancel</button>
				</form>
				<hr>
				<form action="<%= virtualbase %>admin/profile_create.asp" method="post" class="profilemanager">
					<input type="hidden" name="createprofile" value="true">
					<label for="profilename">Create New Profile</label>
					<br>
					<input type="text" name="profilename" placeholder="Name of profile">
					<br>
					<button type="submit" value="Submit">Submit</button>
					<button class="redirecttodashboard" type="reset">Cancel</button>
				</form>
			</div>
        </div>
    </div>
<%                    
end function
%>