<!--#include file="config/incInit.asp"-->

<%
OpenConn

if Session(SESSION_MERCHANT_UID)&"" = "" then
	response.redirect virtualbase & "store/dashboard.asp"
end if

if cint(Request.Form("profileid")) = 0 then
	Response.Redirect virtualbase & "admin/profile_manager.asp"
end if
DrawHeader "", ""
DrawListStoresFromProfile(cint(Request.Form("profileid")))
DrawFooter ""
%>
<%
function DrawListStoresFromProfile(intProfileID)
	dim strSQL, rsStores, rsStoreInfo, rsIndivStoreInfo, rsDctDistricts, blnSelectedItem, intStoreID, intDistrict, vchDivision, keyDctIndex, intSelectionCount
	strSQL = "SELECT [intStoreID], [intDistrict], [vchDivision] FROM " & STR_TABLE_SHOPPER
	strSQL = strSQL & " WHERE [intID] IN (SELECT [intStoreShopperID] FROM "
	strSQL = strSQL & STR_TABLE_PROFILEMAPPER & " WHERE [intProfileID]=" & intProfileID & ") AND [chrStatus] = 'A' "
	'response.write strSQL
	set rsStores = gobjConn.execute(strSQL)
	%>
	<div class="page-content-wrapper">
        <!-- BEGIN PAGE HEADER-->
		<div class="page-content">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">Profile Manager</h3>
                    <ul class="page-breadcrumb breadcrumb listprofilebreadcrumbs">
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<%= virtualbase %>store/dashboard.asp">Home</a>
                            <i class="fa fa-angle-right"></i>
                            <a href="<%= virtualbase %>admin/profile_manager.asp">Profile Manager</a>
                            <i class="fa fa-angle-right"></i>
                            <a>Profile List</a>
                        </li>
                    </ul>
                </div>
            </div>
			<%
			dim rsStoreName
			strSQL = "SELECT [vchProfileName] FROM " & STR_TABLE_PROFILES & " WHERE [intID]=" & intProfileID
			'response.write strSQL
			set rsIndivStoreInfo = gobjConn.execute(strSQL)
			%>
			<h3 id="profilelistheader">Profile <%= intProfileID %> - <%= rsIndivStoreInfo(0) %></h3>
			<div id="storelistcontainer">
				<%
				'response.write strSQL
				'response.end
				%>
				<form action="<%= virtualbase %>admin/profile_list.asp" method="post" class="filter">
					<input name="profileid" value="<%= intProfileID %>" type="hidden">
					<select name="intStoreID">
						<option value="">Store</option>
						<%
						intSelectionCount = 0
						strSQL = "SELECT DISTINCT [intStoreID] FROM " & STR_TABLE_SHOPPER & " WHERE [intID] IN (SELECT [intStoreShopperID] FROM " & STR_TABLE_PROFILEMAPPER & " WHERE [intProfileID]=" & intProfileID & ") AND [chrStatus] = 'A' AND [intStoreID] IS NOT NULL"
						set rsStoreInfo = gobjConn.execute(strSQL)
						do until rsStoreInfo.eof
							intSelectionCount = intSelectionCount + 1
							if isNumeric(Request.Form("intStoreID")) then
								intStoreID = cint(Request.Form("intStoreID"))
							'else
								'intStoreID = 0
							end if
							Response.Write "<option value='" & rsStoreInfo("intStoreID") & "'" & iif(cint(rsStoreInfo("intStoreID"))=intStoreID, "selected", "") & ">" & rsStoreInfo("intStoreID") & "</option>"
							rsStoreInfo.movenext
						loop
						if intSelectionCount > 0 then
							rsStoreInfo.MoveFirst()
						end if 
						%>
					</select>
					<select name="intDistrict">
						<option value="">District</option>
						<%
						strSQL = "SELECT DISTINCT [intDistrict] FROM " & STR_TABLE_SHOPPER & " WHERE [intID] IN (SELECT [intStoreShopperID] FROM HBO_ProfileMapper WHERE [intProfileID]=" & intProfileID & ") AND [chrStatus] = 'A' AND [intDistrict] IS NOT NULL"
						set rsStoreInfo = gobjConn.execute(strSQL)
						do until rsStoreInfo.eof
							if isNumeric(Request.Form("intDistrict")) then
								intDistrict = cint(Request.Form("intDistrict"))
							'else
								'intDistrict = 0
							end if
							Response.Write "<option value='" & rsStoreInfo("intDistrict") & "' " & iif(cint(rsStoreInfo("intDistrict"))=intDistrict, "selected", "") & ">" & rsStoreInfo("intDistrict") & "</option>"
							rsStoreInfo.movenext
						loop
						if intSelectionCount > 0 then
							rsStoreInfo.MoveFirst()
						end if 
						%>
					</select>
					<select name="vchDivision">
						<option value="">Region</option>
						<%
						strSQL = "SELECT DISTINCT [vchDivision] FROM " & STR_TABLE_SHOPPER & " WHERE [intID] IN (SELECT [intStoreShopperID] FROM HBO_ProfileMapper WHERE [intProfileID]=" & intProfileID & ") AND [chrStatus] = 'A' AND [vchDivision] IS NOT NULL"
						set rsStoreInfo = gobjConn.execute(strSQL)
						do until rsStoreInfo.eof
							Response.Write "<option value='" & rsStoreInfo("vchDivision") & "' " & iif(rsStoreInfo("vchDivision")&""=Request.Form("vchDivision")&"", "selected", "") & ">" & rsStoreInfo("vchDivision") & "</option>"
							rsStoreInfo.movenext
						loop
						if intSelectionCount > 0 then
							rsStoreInfo.MoveFirst()
						end if 
						%>
					</select>
					<input type="text" name="searchstore" placeholder="Search store" <%= iif(Request.Form("searchstore")<>"", "value='" & Request.Form("searchstore") & "'", "") %>>
					<button type="submit" value="Search">Search</button>
					<button type="button" class="resetnoredirect">Reset</button>
				</form>
				<%
				strSQL = "SELECT S.intDistrict, S.vchLabel, S.vchFirstName, S.[vchLastName], S.vchAddress1, S.[vchAddress2], S.vchCity, S.vchState, S.vchZip, S.intLayout, S.intStoreID, S.vchDivision, S.chrStoreType, L.vchImageLocation "
				strSQL = strSQL & "FROM " & STR_TABLE_SHOPPER & " S "
				strSQL = strSQL & "LEFT JOIN " & STR_TABLE_LAYOUTS & " L ON S.intLayout = L.intID "
				strSQL = strSQL & "WHERE S.intID IN (SELECT [intStoreShopperID] FROM "
				strSQL = strSQL & STR_TABLE_PROFILEMAPPER & " WHERE [intProfileID]=" & intProfileID & ") AND [chrStatus] = 'A' "
				if Request.Form("searchstore")&"" <> "" then
					strSQL = strSQL & "AND ("
					strSQL = strSQL & "[vchAddress1] LIKE '%" & ProtectSQL(Request.Form("searchstore")) & "%'"
					strSQL = strSQL & "OR [vchAddress2] LIKE '%" & ProtectSQL(Request.Form("searchstore")) & "%'"
					strSQL = strSQL & " OR [vchCity] LIKE '%" & ProtectSQL(Request.Form("searchstore")) & "%'"
					strSQL = strSQL & " OR [vchState] LIKE '%" & ProtectSQL(Request.Form("searchstore")) & "%'"
					strSQL = strSQL & " OR [vchZip] LIKE '%" & ProtectSQL(Request.Form("searchstore")) & "%'"
					strSQL = strSQL & ")" 
				end if
				if Request.Form("intStoreID")&""<>"" then
					intStoreID = cint(Request.Form("intStoreID"))
					strSQL = strSQL & "AND S.[intStoreID]=" & intStoreID & " "
				end if
				if Request.Form("intDistrict")&""<>"" then
					intDistrict = cint(Request.Form("intDistrict"))
					strSQL = strSQL & "AND S.[intDistrict]=" & intDistrict & " "
				'else
					'intDistrict = ""
				end if
				if Request.Form("vchDivision")&"" <> "" then
					strSQL = strSQL & "AND [vchDivision] LIKE '" & ProtectSQL(Request.Form("vchDivision")) & "' "
				end if
				'Response.Write strSQL
				'response.end
				set rsStores = gobjConn.execute(strSQL)
				%>
				<table id="storelistfromprofile">
					<thead>
						<tr>
							<th>Store Name</th>
							<th>Store</th>
							<th>District</th>
							<th>Address</th>
							<th>Type</th>
							<th>Region</th>
							<th>Vew Schematic</th>
						</tr>
					</thead>
					<tbody>
						<%
						if rsStores.Fields.Count = 0 then
						%>
						<tr>
							<p>No floor plans have been associated with this profile</p>
						</tr>
						<%
						end if
						while not rsStores.eof
						%>
						<tr>
							<td><%= rsStores("vchLastName") %></td>
							<td><%= rsStores("intStoreID") %></td>
							<td><%= rsStores("intDistrict") %></td>
							<td>
							<p>
								<%
								response.write rsStores("vchAddress1")
								if rsStores("vchAddress2")&""<>"" then
									response.write "&nbsp;" & rsStores("vchAddress2")
								end if
								%>
								<br>
								<%= rsStores("vchCity") %>, <%= rsStores("vchState") %>&nbsp;<%= rsStores("vchZip") %>
							</p>
							</td>
							<td>
							<%
							select case rsStores("chrStoreType")&""
								Case "T"
									response.write "Traditional"
								Case "N"
									response.write "Neighborhood"
								Case else
									response.write ""
							end select
							%>
							</td>
							<td><%= rsStores("vchDivision") %></td>
							<td>
								<a href="<%= virtualbase %>images/layouts/<%= rsStores("vchImageLocation") %>" class="layoutzoom" data-width="640"><img src="<%= virtualbase %>images/Profile-Icon.svg" alt="<%= rsStores("vchLastName") %>"></a>
							</td>
						</tr>
						<%
							rsStores.movenext
						wend
						%>
				</tbody>
				</table>
			</div>
			<%
			strSQL = "SELECT * FROM " & STR_TABLE_PROFILES & " WHERE [intID]=" & intProfileID
			'response.write strSQL
			set rsIndivStoreInfo = gobjConn.execute(strSQL)
			strSQL = "SELECT DISTINCT intDistrict AS dctUniqueDistrics FROM " & STR_TABLE_SHOPPER & " "
			strSQL = strSQL & "WHERE intID IN (SELECT [intStoreShopperID] FROM " & STR_TABLE_PROFILEMAPPER & " "
			strSQL = strSQL & "WHERE [intProfileID]=" & intProfileID & ") AND [chrStatus] = 'A'"
			set rsDctDistricts = gobjConn.execute(strSQL)
			%>
			<div id="indivstoreinfo">
				<h4>District(s): 
				<%
				do
					if not rsDctDistricts.eof then
						response.write rsDctDistricts("dctUniqueDistrics")
						rsDctDistricts.movenext()
						if not rsDctDistricts.eof then
							response.write ","
						end if
					end if
				loop until rsDctDistricts.eof
				%>
				</h4>
				<h4>Max fixture amounts:</h4>
				<%
				'dctFixtures
				for keyDctIndex=0 to dctFixtures.Count - 1
					Response.Write "<p>" & dctFixtures.Items()(keyDctIndex) & ": " & rsIndivStoreInfo(dctFixtures.Keys()(keyDctIndex)) & "</p>"
				Next
				keyDctIndex = 0
				%>
				<form action="<%= virtualbase %>admin/profile_create.asp" method="post">
					<input type="hidden" name="intProfileID" value="<%= intProfileID %>">
					<input type="hidden" name="editexistingprofile" value="true">
					<button type="submit" value="editstoreprofile" name="profilelistbutton">Edit Store Profile</button>
				</form>
				<form action="<%= virtualbase %>admin/profile_manager.asp" method="post">
					<input type="hidden" name="returntoprofilelist" value="<%= intProfileID %>">
					<button type="submit" value="selectprofile" name="profilelistbutton">Select Another Profile</button>
					<button type="submit" value="createprofile" name="profilelistbutton">Create Another Profile</button>
				</form>
				<form action="<%= virtualbase %>admin/profile_create.asp" method="post">
					<input type="hidden" name="intProfileID" value="<%= intProfileID %>">
					<input type="hidden" name="duplicateprofile" value="true">
					<button type="submit" value="duplicateprofile" name="profilelistbutton" style="background-color: #2cb0cf; color: white;">Duplicate and Edit</button>
				</form>
			</div>
	</div>
	</div>
	<%
end function
%>