<% option explicit %>
<!--#include file="incForm.asp"-->
<!--#include file="incCheck.asp"-->
<!--#include file="incStyle.asp"-->
<!--#include file="../../config/incGoCartAPI.asp"-->
<!--#include file="incUserAcc.asp"-->
<%

' =====================================================================================
' = File: incInit.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Admin Page incInit.asp
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

dim blnPrintMode

dim gstrHostBase		' {"resdev", "internal", "staging", "live"}
dim virtualbase, aspbase, imagebase, homebase, absbase, userfilebase, storefront	' special paths
dim securebase, nonsecurebase, liveimagebase, photoimagebase, thumbimagebase
dim strServerHost, strScriptName, strScript, gblnSecure		' URL info
dim gstrConnectString, gstrDSNSource		' database info
dim gstrClientCopyright
'dim gobjConn, gblnConnOpen
dim gstrUser, gstrUserIP
dim grsInvFolders, gintInvParentUpID
gintInvParentUpID = -1

dim g_imagebase

' TEMPORARY COMPATIBILITY
dim conn, connectstring

dim dctLinks, optSomeType, optSomeStatus, optBannerSize, dctSearch, dctAdminLinks, dctAccess, dctStatus, dctTipType, dctLinkType

const STR_COMPANY_NAME = "HBO"
const STR_SITE_NAME = ""
const STR_DOMAIN_NAME = "thecampaignstore"
const STR_STAGING_DSN = "webdatasql"
const STR_EMAIL_CONTACT = "info@.com"

const STR_TABLE_GOEDIT = "HBO_GoEdit"
const STR_TABLE_PROMO = "HBO_PromoCodes"

call InitDomain()

sub InitDomain()
	blnPrintMode = (Request.QueryString("print").count > 0)
	
	dim rootfolder
	gstrClientCopyright = "Copyright &copy;2000 " & STR_COMPANY_NAME & ". All rights reserved."
	
	gstrUser = Request.ServerVariables("REMOTE_USER")
	gstrUserIP = Request.ServerVariables("REMOTE_ADDR")
	
	strServerHost = Request.ServerVariables("HTTP_HOST")
	strScriptName = Request.ServerVariables("SCRIPT_NAME")
	strScript = "http://" & strServerHost & strScriptName & "?"
	
	if lcase(strServerHost) = "awsdev" then
		' running from internal
		rootfolder = "/hbo/www/"
		absbase = absbase & strServerHost & rootfolder
		gstrHostBase = "internal"
		securebase = "http://" & strServerHost & rootfolder
		nonsecurebase = "http://" & strServerHost & rootfolder
		homebase = nonsecurebase
		gstrConnectString = "DSN=webdatasql; UID=webuser; PWD=webdata"
	elseif InStr(lcase(strServerHost), "localhost") > 0 then
		' running from staging
		rootfolder = "/hbo/"
		absbase = absbase & strServerHost & rootfolder
		gstrHostBase = "localhost"
		securebase = "http://" & strServerHost & rootfolder
		nonsecurebase = "http://" & strServerHost & rootfolder
		homebase = nonsecurebase
		gstrConnectString = "DSN=webdatasql; UID=webuser; PWD=welcome"
	elseif lcase(strServerHost) = "staging.drcportal.com" then
		' running from awsdev staging
		rootfolder = "/hbo/"
		absbase = "http://" & strServerHost & "/" & rootfolder
		nonsecurebase = "http://" & strServerHost & "/" & rootfolder
		securebase = nonsecurebase ' secure mode runs under nonsecure
		'absbase = absbase & strServerHost & rootfolder
		gstrHostBase = "staging"
		securebase = "" & rootfolder
		nonsecurebase = "http://" & strServerHost & rootfolder
		homebase = nonsecurebase
		'virtualbase = rootfolder & absbase
		gstrConnectString = "DSN=stagingsql; UID=webuser; PWD=welcome"
	else
		' running from live site
		if Request("dbg") = "" then
			on error resume next
		end if
		rootfolder = "/"
		absbase = absbase & strServerHost & rootfolder
		gstrHostBase = "live"
		securebase = "" & rootfolder
		nonsecurebase = "http://" & strServerHost & rootfolder
		homebase = nonsecurebase
		gstrConnectString = "DSN=webdatasql; UID=webuser; PWD=welcome"
	end if
	connectstring = gstrConnectString
	
	dim tmpPos
	tmpPos = instr(lcase(strScriptName),rootfolder) + len(rootfolder)
	while instr(tmpPos, lcase(strScriptName), "/")
		virtualbase = virtualbase & "../"
		tmpPos = instr(tmpPos, lcase(strScriptName), "/") + 1
	wend
	
	aspbase = virtualbase
	imagebase = virtualbase & "images/"
	userfilebase = virtualbase & "GoDocUserFiles/"
	liveimagebase = virtualbase & "images/products/"
	photoimagebase = virtualbase & "images/products/big/"
	thumbimagebase = virtualbase & "images/products/small/"
	storefront = virtualbase & "storefront/"
	g_imagebase = virtualbase & "_images/"

end sub

function GetSacWebUser(strUser, strPassword, intMinLevel)
	' minimum levels:
	'   15 = programmer admin
	'   25 = system admin (normal)  <-- ***
	'   35 = guest admin (read-only)
	'   45 = guest user (read-only, but not to admin pages)
	dim objAuth, intResult
	set objAuth = Server.CreateObject("IIS4Utils.SacWebSecurity")
	intResult = objAuth.Authenticate(strUser, strPassword)
	'if (intResult > 0) and (intResult <= intMinLevel) then
	'	GetSacWebUser = intResult
	'else
	'	GetSacWebUser = intResult
	'end if
	set objAuth = nothing
end function

sub CheckAdminLogin()
	' call this function ONLY in the main content frame
	if Session(SESSION_MERCHANT_UID) = "" then
		response.redirect aspbase & "admin/login.asp"
	end if
end sub

function CheckAdminLogin_Sideframe()
	' call this function ONLY from a frame OTHER THAN the main content frame
	' this function will return true or false
	CheckAdminLogin_Sideframe = (Session(SESSION_MERCHANT_UID) <> "")
end function

' -----------------------------------
' =========== DrawHeader ============
' -----------------------------------
Sub DrawHeader(byVal strTitle, strPageType)
	'response.expires = -1	' prevent any client-side or proxy caching of ASP pages
	if strTitle <> "" then
		'strTitle = STR_SITE_NAME & " - " & strTitle
	else
		strTitle = STR_SITE_NAME
	end if
' ==== common HTML header ====
%>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>HBO | Dome Printing</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="<%= virtualbase %>assets/global/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/global/plugins/bootstrap/css/sticky-footer.css" rel="stylesheet" type="text/css"/>
<script src="<%= virtualbase %>assets/global/plugins/jquery-1.11.0.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link href="<%= virtualbase %>drcpod/css/jquery-ui.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="<%= virtualbase %>assets/global/plugins/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<link rel="stylesheet" href="<%= virtualbase %>assets/global/plugins/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN PAGE STYLES -->
<link href="<%= virtualbase %>assets/global/plugins/select2/select2.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/admin/pages/css/login.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/admin/pages/css/profile_manager.css?v=0.1" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/admin/pages/css/store_view.css?v=0.2" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/admin/pages/css/store_resources.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/admin/pages/css/file_tracker.css?v=0.4" rel="stylesheet" type="text/css"/>
<!-- END PAGE STYLES -->
<!-- BEGIN THEME STYLES -->
<link href="<%= virtualbase %>assets/global/css/components.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/admin/layout/css/themes/light2.css" rel="stylesheet" type="text/css" />
<link href="<%= virtualbase %>assets/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<link href="<%= virtualbase %>assets/admin/layout/css/print.css" rel="stylesheet" media="print" type="text/css"/>

<!-- END THEME STYLES -->
<link href="<%= virtualbase %>drcpod/css/ui.jqgrid.css" rel="stylesheet" type="text/css"/>		
<script src="<%= virtualbase %>drcpod/js/scripts.js" type="text/javascript"></script>		
<link rel="stylesheet" href="<%= virtualbase %>assets/global/plugins/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />		
<link rel="stylesheet" href="<%= virtualbase %>assets/global/plugins/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />		
<!-- END THEME STYLES -->		
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="<%= virtualbase %>assets/global/plugins/fancybox/source/jquery.fancybox.js"></script>
<script type="text/javascript" src="<%= virtualbase %>assets/admin/pages/scripts/profile_manager.js"></script>


<!-- END THEME STYLES -->
<link rel="shortcut icon" href=""/>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed login">
<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
            <a href="<%= virtualbase %>store/dashboard.asp"><img src="<%= virtualbase %>assets/admin/layout/img/logo.png" alt="logo" class="logo-default" style="float:left"></a>
		</div>
        <% if Session(SESSION_MERCHANT_ULOGIN)&""<>"" then %>
		<!-- END LOGO -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				<li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
					<a href="#" style="color:Blue;">Welcome <strong><%= Session(SESSION_MERCHANT_ULOGIN) %></strong></a>
				</li>
				<li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">
					<a href="<%= virtualbase %>admin"><i class="fa fa-caret-down"></i> My Account</a>
				</li>
				<li class="dropdown dropdown-extended dropdown-tasks" id="header_task_bar">
					<a href="<%= virtualbase %>default.asp?action=logout"><i class="fa fa-power-off"></i> Logout</a>
				</li>
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
        <% end if %>
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- BEGIN CONTAINER -->
<div class="page-container">
<%
end sub

'--------------- DrawFooter ---------------
sub DrawFooter(footertype)
%>
</div>
<!-- END CONTAINER -->	
<!-- BEGIN COPYRIGHT -->
<div class="footer copyright">
	<div class="page-footer">
		&copy; <%= Now %> DOME Solutions. All rights reserved. Under license to HBO<br />
        Need help? Contact HBO In-Store Marketing at instoremarketing@HBO.com for assistance.
	</div>
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->

<script src="<%= virtualbase %>assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<%= virtualbase %>assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<%= virtualbase %>assets/global/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<%= virtualbase %>assets/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="<%= virtualbase %>assets/admin/pages/scripts/login.js" type="text/javascript"></script>
<script type="text/javascript" src="<%= virtualbase %>assets/global/plugins/fancybox/source/jquery.fancybox.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<script>
    jQuery(document).ready(function () {
        Metronic.init();
        QuickSidebar.init()
        Login.init();
    });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

<%
End Sub


sub DrawHeaderUpdate()
	' this function should be called by any page that may be redirected to
	' by the login page - it will update the title and sidebar frames
	if Request.QueryString("updateheader").count > 0 then
%>
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
<!--
    // the reload() call is causing a JavaScript error if the frame has not yet loaded already
    //top.swgc5_title.location.reload(true);
    //top.swgc5_side.location.reload(true);
    top.swgc5_title.location.replace('title.asp');
    top.swgc5_side.location.replace('sidebar.asp');
// -->
</SCRIPT>
<%
	end if
end sub

%>