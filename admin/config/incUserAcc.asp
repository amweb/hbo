<%

' =====================================================================================
' = File: incUserAcc.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   GoCart User Access Definitions
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

const ACC_ADMIN					= 1		' Admin - can create/modify administrative users, view audit log
const ACC_ORDER_EMAILRECEIPT	= 2 ' can resend a customer a copy of their email receipt
const ACC_RAW					= 3 ' Modify Raw Data - can modify raw information (PROGRAMMER-LEVEL)
const ACC_INVENTORY_VIEW		= 4 ' View Inventory - can view inventory details
const ACC_INVENTORY_EDIT		= 5 ' Edit Inventory - can edit inventory details
const ACC_REPORT_VIEW			= 6 ' View Reports - can view reports
const ACC_SHOPPER_VIEW			= 7 ' View Shoppers - can view shopper list admin
const ACC_SHOPPER_EDIT			= 8 ' Edit Shoppers - can edit shopper list
const ACC_ORDER_VIEW			= 9 ' View Order Details - can view order details
const ACC_ORDER_PAYMENT_VIEW	= 10 ' View Order Payment Info - can view payment info in order details
const ACC_ORDER_BILLING_VIEW	= 11 ' View Order Billing Info - can view billing address info in order details
const ACC_ORDER_SHIPPING_VIEW	= 12 ' View Order Shipping Info - can view shipping address info in order details
const ACC_ORDER_TRANS_VIEW		= 13 ' View Order Transactions - can view transactions list in order details
const ACC_CARDS_VIEW			= 14 ' View Cards - can view full credit card numbers
const ACC_ORDER_CREATE			= 15 ' Create Orders - can create a new order from scratch
const ACC_ORDER_EDIT			= 16 ' Edit Order Before Submitted - can modify an "in progress" order
const ACC_ORDER_PAYMENT_EDIT	= 17 ' Edit Order Payment Info - can modify payment info for a submitted order
const ACC_ORDER_BILLING_EDIT	= 18 ' Edit Order Billing Info - can modify billing info for a submitted order
const ACC_ORDER_SHIPPING_EDIT	= 19 ' Edit Order Shipping Info - can modify shipping info for a submitted order
const ACC_ORDER_ITEMS_EDIT		= 20 ' Edit Order Items - can add/edit/remove line items and notes for a submitted order
const ACC_ORDER_NOTES_ADD		= 21 ' Add Order Notes - can add new line item notes to a submitted order
const ACC_ORDER_RETURNS_ADD		= 22 ' Add Order Returns - can add new line item returns to a submitted order
const ACC_ORDER_DECLINE			= 23 ' Decline Order - can decline a submitted order
const ACC_ORDER_APPROVE			= 24 ' Approve Order - can approve a submitted order
const ACC_ORDER_SHIP			= 25 ' Ship Order - can ship a submitted order
const ACC_ORDER_TRANS_AUTH		= 26 ' DoAuth Order - can perform an auth trans on a submitted order
const ACC_ORDER_TRANS_DEPOSIT	= 27 ' DoDeposit Order - can perform a deposit trans on a submitted order
const ACC_ORDER_TRANS_CREDIT	= 28 ' DoCredit Order - can perform a credit trans on a submitted order
const ACC_ORDER_TRANS_MANUAL	= 29 ' DoManualTrans Order - can enter a manual transaction on a submitted order
const ACC_ORDER_TRANS_AUTHDEP	= 30 ' DoAuthDep Order - can perform a deposit without an authorization on a submitted order
const ACC_TRANS_VIEW			= 31 ' View System Transactions
' SWSCodeChange 2000-07-25 - added GoEdit admin
const ACC_REFERAL_ADMIN			= 32 ' Can edit GoEdit documents

function CheckUserAccess(intAccessFlag)
	CheckUserAccess = (mid(Session(SESSION_MERCHANT_ACCESS), intAccessFlag, 1) = "Y")
end function

function UserAccessStatus_BuildSQLWhere()
	' returns part of a WHERE clause: chrStatus IN ("'..','..',...'..'")
	UserAccessStatus_BuildSQLWhere = UserAccessStatus_BuildSQLWhereX("09SABZXCHKEPRUV")
end function

function UserAccessStatus_BuildSQLWhereX(strStatus)
	' returns part of a WHERE clause: "chrStatus IN ('..','..',...'..')"
	' but only from the subset passed in as strStatus
	dim c, x, s, ch, strAccess
	strAccess = Session(SESSION_MERCHANT_ACCESS_STATUS)
	x = len(strStatus)
	s = ""
	for c = 1 to x
		ch = mid(strStatus, c, 1)
		if (InStr(strAccess, ch) > 0) or (strAccess = "*") then
			if s <> "" then
				s = s & ","
			end if
			if ch = "0" then
				s = s & "'0','1','2','3','4','5','6','7','8'"
			else
				s = s & "'" & ch & "'"
			end if
		end if
	next
	UserAccessStatus_BuildSQLWhereX = s
end function

function CheckUserAccessStatus(chrStatus)
	dim strAccess
	strAccess = Session(SESSION_MERCHANT_ACCESS_STATUS)
	CheckUserAccessStatus = (InStr(strAccess, chrStatus) > 0) or (strAccess = "*")
end function
%>