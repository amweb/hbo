<% option explicit %>
<!--#include file="incForm.asp"-->
<!--#include file="incCheck.asp"-->
<!--#include file="incStyle.asp"-->
<!--#include file="../../config/incGoCartAPI.asp"-->
<!--#include file="incUserAcc.asp"-->
<%

' =====================================================================================
' = File: incInit.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Admin Page incInit.asp
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

dim blnPrintMode

dim gstrHostBase		' {"resdev", "internal", "staging", "live"}
dim virtualbase, aspbase, imagebase, homebase, absbase, userfilebase	' special paths
dim securebase, nonsecurebase, liveimagebase, photoimagebase, thumbimagebase
dim strServerHost, strScriptName, strScript, gblnSecure		' URL info
dim gstrConnectString, gstrDSNSource		' database info
dim gstrClientCopyright
'dim gobjConn, gblnConnOpen
dim gstrUser, gstrUserIP
dim grsInvFolders, gintInvParentUpID
gintInvParentUpID = -1

dim g_imagebase

' TEMPORARY COMPATIBILITY
dim conn, connectstring

dim dctLinks, optSomeType, optSomeStatus, optBannerSize, dctSearch, dctAdminLinks, dctAccess, dctStatus, dctTipType, dctLinkType

const STR_COMPANY_NAME = "The Campaign Store, LLC"
const STR_SITE_NAME = "TheCampaignStore.com"
const STR_DOMAIN_NAME = "thecampaignstore"
const STR_STAGING_DSN = "Staging SQL"
const STR_EMAIL_CONTACT = "info@thecampaignstore.com"

const STR_TABLE_GOEDIT = "TCS_GoEdit"

call InitDomain()

sub InitDomain()
	blnPrintMode = (Request.QueryString("print").count > 0)
	
	dim rootfolder
	gstrClientCopyright = "Copyright &copy;2000 " & STR_COMPANY_NAME & ". All rights reserved."
	
	gstrUser = Request.ServerVariables("REMOTE_USER")
	gstrUserIP = Request.ServerVariables("REMOTE_ADDR")
	
	strServerHost = Request.ServerVariables("HTTP_HOST")
	strScriptName = Request.ServerVariables("SCRIPT_NAME")
	strScript = "http://" & strServerHost & strScriptName & "?"
	
	if lcase(strServerHost) = "awsdev" then
		' running from internal
		rootfolder = "/thecampaignstore/www/"
		absbase = absbase & strServerHost & rootfolder
		gstrHostBase = "internal"
		securebase = "http://" & strServerHost & rootfolder
		nonsecurebase = "http://" & strServerHost & rootfolder
		homebase = nonsecurebase
		gstrConnectString = "DSN=webdatasql; UID=webuser; PWD=webdata"
	elseif InStr(lcase(strServerHost), "clients") > 0 then
		' running from staging
		rootfolder = "/thecampaignstore/www/"
		absbase = absbase & strServerHost & rootfolder
		gstrHostBase = "staging"
		gstrDSNSource = STR_STAGING_DSN
		securebase = "http://" & strServerHost & rootfolder
		nonsecurebase = "http://" & strServerHost & rootfolder
		homebase = nonsecurebase
		gstrConnectString = "DSN=stagingsql; UID=webuser; PWD="
	else
		' running from live site
		if Request("dbg") = "" then
			on error resume next
		end if
		rootfolder = "/"
		absbase = absbase & strServerHost & rootfolder
		gstrHostBase = "live"
		securebase = "https://" & strServerHost & rootfolder
		nonsecurebase = "http://" & strServerHost & rootfolder
		homebase = nonsecurebase
		gstrConnectString = "DSN=tcssql; UID=tcswebuser; PWD=winner$"
	end if
	connectstring = gstrConnectString
	
	dim tmpPos
	tmpPos = instr(lcase(strScriptName),rootfolder) + len(rootfolder)
	while instr(tmpPos, lcase(strScriptName), "/")
		virtualbase = virtualbase & "../"
		tmpPos = instr(tmpPos, lcase(strScriptName), "/") + 1
	wend
	
	aspbase = virtualbase
	imagebase = virtualbase & "images/"
	userfilebase = virtualbase & "GoDocUserFiles/"
	liveimagebase = virtualbase & "images/products/"
	photoimagebase = virtualbase & "images/products/big/"
	thumbimagebase = virtualbase & "images/products/small/"
	
	g_imagebase = virtualbase & "_images/"

end sub

function GetSacWebUser(strUser, strPassword, intMinLevel)
	' minimum levels:
	'   15 = programmer admin
	'   25 = system admin (normal)  <-- ***
	'   35 = guest admin (read-only)
	'   45 = guest user (read-only, but not to admin pages)
	dim objAuth, intResult
	set objAuth = Server.CreateObject("IIS4Utils.SacWebSecurity")
	intResult = objAuth.Authenticate(strUser, strPassword)
	'if (intResult > 0) and (intResult <= intMinLevel) then
	'	GetSacWebUser = intResult
	'else
	'	GetSacWebUser = intResult
	'end if
	set objAuth = nothing
end function

sub CheckAdminLogin()
	' call this function ONLY in the main content frame
	if Session(SESSION_MERCHANT_UID) = "" then
		response.redirect aspbase & "admin/login.asp"
	end if
end sub

function CheckAdminLogin_Sideframe()
	' call this function ONLY from a frame OTHER THAN the main content frame
	' this function will return true or false
	CheckAdminLogin_Sideframe = (Session(SESSION_MERCHANT_UID) <> "")
end function

sub DrawHeaderUpdate()
	' this function should be called by any page that may be redirected to
	' by the login page - it will update the title and sidebar frames
	if Request.QueryString("updateheader").count > 0 then
%>
<SCRIPT LANGUAGE="JavaScript" TYPE="text/javascript">
<!--
	// the reload() call is causing a JavaScript error if the frame has not yet loaded already
	//top.swgc5_title.location.reload(true);
	//top.swgc5_side.location.reload(true);
	top.swgc5_title.location.replace('title.asp');
	top.swgc5_side.location.replace('sidebar.asp');
// -->
</SCRIPT>
<%
	end if
end sub

Sub DrawHeader(strPageTitle, strPageType)
	response.expires = -1
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
	<TITLE><%= strPageTitle %></TITLE>
	<STYLE TYPE="text/css">
	<!--
	body {
		font-size: 13px;
		font-family: Verdana,Geneva,Arial;
	}
	td {
		font-size: 10px;
		font-family: Verdana,Geneva,Arial;
	}
	A	{text-decoration : none;}
	A:Hover	{background : <%= cLtYellow %>;}
	
	
	INPUT {
		font-size: 11px;
		font-family: Verdana;
		padding: 2;
		background-color: <%= cVLtYellow %>;
		xborder-width: 1;
	}
	SELECT {
		font-size: 11px;
		font-family: Verdana;
		background-color: <%= cVLtYellow %>;
		padding: 2;
	}
	.textarea {
		font-size: 11px;
		font-family: Verdana;
		background-color: <%= cWhite %>;
		width: 100%;
	}
	.button {
		font-size: 10px;
		font-family: Verdana;
		font-weight: bold;
		color: <%= cWhite %>;
		background-color: <%= cDkBlue %>;
		height: 23px;
	}
	.checkbox {
		color: <%= cWhite %>;
		background-color: <%= cWhite %>;
		height: 23px;
	}
	-->
	</STYLE>
<%
	select case strPageType
	' ==== Sidebar header ====
	case "sidebar"
%>
</HEAD>
<BODY
	BGCOLOR="<%= cBodyBG %>" BACKGROUND="<%= imagebase %>sidebar_bg.gif"
	TEXT="<%= cText %>" LINK="<%= cLink %>" VLINK="<%= cVLink %>" ALINK="<%= cALink %>"
	LEFTMARGIN="<%= PAGE_LEFT_MARGIN %>" TOPMARGIN="<%= PAGE_TOP_MARGIN %>" MARGINWIDTH="<%= PAGE_LEFT_MARGIN %>" MARGINHEIGHT="<%= PAGE_TOP_MARGIN %>">
<%
	' ==== else header ====
	case else
%>
</HEAD>
<BODY
	BGCOLOR="<%= cBodyBG %>"
	TEXT="<%= cText %>" LINK="<%= cLink %>" VLINK="<%= cVLink %>" ALINK="<%= cALink %>"
	LEFTMARGIN="<%= PAGE_LEFT_MARGIN %>" TOPMARGIN="<%= PAGE_TOP_MARGIN %>" MARGINWIDTH="<%= PAGE_LEFT_MARGIN %>" MARGINHEIGHT="<%= PAGE_TOP_MARGIN %>">
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="<%= INT_PAGE_WIDTH %>">
<TR>
	<TD>
<!-- end of Header -->

<%
	end select
end sub

sub DrawFooter(strPageType)
%>
<!-- start of Footer -->
	</TD>
</TR>
</TABLE>
</BODY>
</HTML>
<%
End Sub
%>