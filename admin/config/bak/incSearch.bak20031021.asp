<%

' =====================================================================================
' = File: incSearch.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Search page code
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

const INT_MAX_SEARCH_GROUPS = 3

sub Search_AddLabel(s) : dctSearchList.Add "label~~" & s, "" : end sub
sub Search_AddNumberRange(strField, strLabel) : dctSearchList.Add "nr~" & strField & "~" & strLabel, "" : end sub
sub Search_AddDateRange(strField, strLabel) : dctSearchList.Add "dr~" & strField & "~" & strLabel, "" : end sub
sub Search_AddText(strField, strWidth, strMax, strLabel) : dctSearchList.Add "t~" & strWidth & "~" & strMax & "~" & strField & "~" & strLabel, "" : end sub
sub Search_AddCheckboxGroup(strField, intColCount, dctList, strLabel) : dctSearchList.Add "dchk~" & intColCount & "~" & strField & "~" & strLabel, dctList : end sub
sub Search_AddNumber(strField, strLabel) : dctSearchList.Add "n~" & strField & "~" & strLabel, "" : end sub
sub Search_AddRadioGroup(strField, intColCount, dctList, strLabel) : dctSearchList.Add "dradio~" & intColCount & "~" & strField & "~" & strLabel, dctList : end sub

dim strSearchAuxText, strSearchAuxAction
dim dctSearchList, intNumGroups, intCurrentGroup, strSearchSQLPrefix, strSearchSQLSuffix
redim arySearchGroups(INT_MAX_SEARCH_GROUPS,1)
set dctSearchList = Server.CreateObject("Scripting.Dictionary")

sub InitSearchEngine
	dim c
	
	for c = 1 to INT_MAX_SEARCH_GROUPS
		set arySearchGroups(c,0) = Server.CreateObject("Scripting.Dictionary")
		arySearchGroups(c,1) = ""
	next
	
	' validate any current criteria
	if Request("preset") <> "" then
		LoadPresetSearch Request("preset")
		strAction = "search"
		intNumGroups = 1
		intCurrentGroup = 0
	else
		intNumGroups = Request("n_groups")
		if IsNumeric(intNumGroups) then
			intNumGroups = clng(intNumGroups)
		else
			intNumGroups = 0
		end if
		if intNumGroups < 0 then
			intNumGroups = 0
		end if
		if intNumGroups > INT_MAX_SEARCH_GROUPS then
			intNumGroups = INT_MAX_SEARCH_GROUPS
		end if
		
		'intCurrentGroup = intNumGroups
		
		intCurrentGroup = Request("c_group")
		if IsNumeric(intCurrentGroup) then
			intCurrentGroup = clng(intCurrentGroup)
		else
			intCurrentGroup = 0
		end if
		if intCurrentGroup < 0 then
			intCurrentGroup = 0
		end if
		if intCurrentGroup > intNumGroups + 1 then
			intCurrentGroup = intNumGroups + 1
		end if
		if intCurrentGroup > INT_MAX_SEARCH_GROUPS then
			intCurrentGroup = 0
		end if
		
		if intCurrentGroup = intNumGroups + 1 then
			intNumGroups = intNumGroups + 1
		end if
		
		if Request("reset") = "y" then
			' start over
			intNumGroups = 1
			intCurrentGroup = 1
			LoadSearchCriteria "", dctSearchlist, arySearchGroups(1,0)
		else
			for c = 1 to INT_MAX_SEARCH_GROUPS
				set arySearchGroups(c,0) = Server.CreateObject("Scripting.Dictionary")
				if c <> intCurrentGroup then
					if Request("reset") <> "y" then
						LoadSearchCriteria c & "_", dctSearchlist, arySearchGroups(c,0)
						arySearchGroups(c,1) = Request(c & "_mode")
					else
						arySearchGroups(c,1) = ""
					end if
				else
					arySearchGroups(c,1) = Request("reset")
					LoadSearchCriteria "", dctSearchList, arySearchGroups(c,0)
					if arySearchGroups(c,0).count = 0 then
						' not a valid search submission, don't count it
						intNumGroups = intNumGroups - 1
						if intNumGroups < 0 then
							intNumGroups = 0
						end if
						intCurrentGroup = intCurrentGroup - 1
						if intCurrentGroup < 0 then
							intCurrentGroup = 0
						end if
					end if
				end if
			next
		end if
	end if
end sub

function LoadSearchCriteria(strPrefix, dctSearchList, dctSearchData)
	' scan each field listed in dctSearchList, and add to dctSearchData
	' return true if criteria is valid, false if there are any data errors
	dim c, x, k, i, strType, strName, strLabel, strWidth, strMax, strExclude, strValue, strValue2
	k = dctSearchList.keys
	i = dctSearchList.items
	x = dctSearchList.count - 1
	for c = 0 to x
		ParseSearchDef k(c), strType, strName, strLabel, strWidth, strMax
		strExclude = Request(strPrefix & "exclude_" & strName)
		if lcase(strExclude) = "y" then
			strExclude = "y~"
		else
			strExclude = "n~"
		end if
		strValue = Request(strPrefix & "value_" & strName)
		strValue2 = Request(strPrefix & "value2_" & strName)
		if strValue <> "" or strValue2 <> "" then
			select case strType
				case "t":
					dctSearchData.Add strName, strExclude & replace(strValue, "~", "")
				case "d":
					if IsDate(strValue) then
						strValue = Month(strValue) & "/" & Day(strValue) & "/" & Year(strValue) & " " & Hour(strValue) & ":" & Minute(strValue)
					else
						strValue = ""
					end if
					if strValue <> "" then
						dctSearchData.Add strName, strExclude & strValue
					end if
				case "n":
					if IsNumeric(strValue) then
						strValue = CDbl(strValue)
					else
						strValue = ""
					end if
					if strValue <> "" then
						dctSearchData.Add strName, strExclude & strValue
					end if
				case "dr":
					if IsDate(strValue) then
						strValue = Month(strValue) & "/" & Day(strValue) & "/" & Year(strValue) & " " & Hour(strValue) & ":" & Minute(strValue)
					else
						strValue = ""
					end if
					if IsDate(strValue2) then
						strValue2 = Month(strValue2) & "/" & Day(strValue2) & "/" & Year(strValue2) & " " & Hour(strValue2) & ":" & Minute(strValue2)
					else
						strValue2 = ""
					end if
					if strValue <> "" or strValue2 <> "" then
						dctSearchData.Add strName, strExclude & strValue & "~" & strValue2
					end if
				case "nr":
					if IsNumeric(strValue) then
						strValue = CDbl(strValue)
					else
						strValue = ""
					end if
					if IsNumeric(strValue2) then
						strValue2 = CDbl(strValue2)
					else
						strValue2 = ""
					end if
					if strValue <> "" or strValue2 <> "" then
						dctSearchData.Add strName, strExclude & strValue & "~" & strValue2
					end if
				case "dchk", "dradio":
					' retrieve all the values, separate by "+" in strValue
					dim intOptionCount, intOptionIndex
					intOptionCount = Request(strPrefix & "value_" & strName).count
					strValue = ""
					if intOptionCount > 0 then
						for intOptionIndex = 1 to intOptionCount
							strValue = strValue & "+" & Request(strPrefix & "value_" & strName)(intOptionIndex)
						next
						dctSearchData.Add strName, strExclude & mid(strValue,2)
					end if
			end select
		end if
	next
	LoadSearchCriteria = true
end function

sub ParseSearchDef(byVal strDef, strType, strName, strLabel, strWidth, strMax)
	dim x, c
	x = InStr(strDef, "~")
	if x > 0 then
		strType = left(strDef, x - 1)
		c = x + 1
		select case strType
			case "t":	' args: width, max
				x = InStr(c, strDef, "~")
				if x > 0 then
					strWidth = mid(strDef, c, x - c)
					c = x + 1
					x = InStr(c, strDef, "~")
					if x > 0 then
						strMax = mid(strDef, c, x - c)
						c = x + 1
					else
						err.Raise 3003, "Invalid Search Field"
					end if
				else
					err.Raise 3004, "Invalid Search Field"
				end if
			case "d", "dr":
				strWidth = 20
				strMax = 20
			case "n", "nr":
				strWidth = 10
				strMax = 10
			case "dchk", "dradio":	' args: num_cols
				x = InStr(c, strDef, "~")
				if x > 0 then
					strWidth = mid(strDef, c, x - c)
					c = x + 1
				else
					err.Raise 3005, "Invalid Search Field"
				end if
		end select
		x = InStr(c, strDef, "~")
		if x > 0 then
			strName = mid(strDef, c, x - c)
			strLabel = mid(strDef, x + 1)
		else
			err.Raise 3001, "Invalid Search Field"
		end if
	else
		err.Raise 3002, "Invalid Search Field"
	end if
end sub

function BuildBasicSearchSQL()
	dim strSQL, strValue, tfHasContent
	tfHasContent = true
	strValue = Request("keywords")
	if strValue = "" then
		strSQL = ""
	else
		dim strValueDate, tfIsNumeric, tfIsDate, s, pos
		tfIsNumeric = IsNumeric(strValue)
		tfIsDate = IsDate(strValue)
		if tfIsDate then
			strValueDate = Day(strValue) & "/" & Month(strValue) & "/" & Year(strValue) & " " & Hour(strValue) & ":" & Minute(strValue)
		end if
	'	strSQL = strSearchSQLPrefix & " AND ("
		dim c, x, i, k, pos2
		x = dctSearchlist.count - 1
		i = dctSearchList.items
		k = dctSearchList.keys
		for c = 0 to x
			s = k(c)
			if left(s,5) <> "label" then
				pos = InStrRev(s, "~")
		'		pos = InStrRev(s, "~", pos - 1)
				pos2 = InStrRev(s, "~", pos - 1)
				s = mid(s, pos2 + 1, pos - InStrRev(s, "~", pos-1) - 1)
				if left(s, 1) = "#" then
					if tfIsNumeric then
						if strSQL <> "" then
							strSQL = strSQL & " OR "
						end if
						strSQL = strSQL & mid(s,2) & "=" & strValue
					end if
				elseif left(s,1) = "%" then
					if tfIsDate then
						if strSQL <> "" then
							strSQL = strSQL & " OR "
						end if
						strSQL = strSQL & mid(s,2) & "='" & strValueDate & "'"
					end if
				elseif isValid(strValue) then
					if strSQL <> "" then
						strSQL = strSQL & " OR "
					end if
					strSQL = strSQL & s & " LIKE '%" & SQLEncode(strValue) & "%'"
				end if
			end if
		next
	end if
	if strSQL <> "" then
		strSQL = strSearchSQLPrefix & " AND (" & strSQL & ") " & strSearchSQLSuffix
	else
		strSQL = strSearchSQLPrefix & " " & strSearchSQLSuffix
	end if
	if tfHasContent then
		BuildBasicSearchSQL = strSQL
	else
		BuildBasicSearchSQL = ""
	end if
end function

function BuildSearchSQL()
	dim strSQL,tfHasContent
	tfHasContent = false
'	strSQL = strSearchSQLPrefix & " AND ("
	dim intGroupIndex
	for intGroupIndex = 1 to intNumGroups
		dim c, x, i, k, strType, strName, strLabel, strValue, strValue2, tfExclude, tfNumeric, tfDate, tfGrouping
		x = arySearchGroups(intGroupIndex,0).count - 1
		i = arySearchGroups(intGroupIndex,0).items
		k = arySearchGroups(intGroupIndex,0).keys
		if x >= 0 then
			if tfHasContent then
				if arySearchGroups(intGroupIndex,1) = "r" then
					strSQL = strSQL & " AND "
				else
					strSQL = strSQL & " OR "
				end if
			end if
			tfHasContent = true
			strSQL = strSQL & "("
			for c = 0 to x
				'ParseSearchDef strDef, strType, strName, strLabel, strWidth, strMax
				strName = k(c)
				if left(strName,1) = "+" then
					tfGrouping = true
					strName = mid(strName,2)
				else
					tfGrouping = false
				end if
				if left(strName,1) = "#" then
					tfNumeric = true
					tfDate = false
					strName = mid(strName,2)
				elseif left(strName,1) = "%" then
					tfNumeric = false
					tfDate = true
					strName = mid(strName,2)
				else
					tfNumeric = false
					tfDate = false
				end if
				strValue = i(c)
				tfExclude = (left(strValue,1) = "y")
				strValue = mid(strValue,3)
				if c > 0 then
					strSQL = strSQL & " AND "
				end if
				if tfExclude then
					strSQL = strSQL & " NOT "
				end if
				dim pos
				pos = InStr(strValue,"~")
				if pos > 0 then
					' range
					strValue2 = mid(strValue, pos + 1)
					strValue = left(strValue, pos - 1)
					if not tfNumeric then
						strValue2 = "'" & SQLEncode(strValue2) & "'"
						strValue = "'" & SQLEncode(strValue) & "'"
					end if
					strSQL = strSQL & "("
					if strValue <> "''" and strValue <> "" then
						strSQL = strSQL & strName & ">=" & strValue & " "
						if strValue2 <> "''" and strValue2 <> "" then
							strSQL = strSQL & " AND "
						end if
					end if
					if strValue2 <> "''" and strValue2 <> "" then
						strSQL = strSQL & strName & "<=" & strValue2
					end if
					strSQL = strSQL & ")"
				else
					' single
					if tfNumeric then
						strSQL = strSQL & strName & "=" & strValue
					elseif tfDate then
						strSQL = strSQL & strName & "='" & strValue & "'"
					elseif tfGrouping then
						strSQL = strSQL & strName & " IN ("
						if tfNumeric then
							strSQL = strSQL & replace(strValue, "+", ",")
						else
							strSQL = strSQL & "'" & replace(SQLEncode(strValue), "+", "','") & "'"
						end if
						strSQL = strSQL & ")"
					elseif isValid(strValue) then
						strSQL = strSQL & strName & " LIKE '%" & SQLEncode(strValue) & "%'"
					end if
				end if
			next
			strSQL = strSQL & ")"
		end if
	next
	if strSQL <> "()" then
		strSQL = strSearchSQLPrefix & " AND (" & strSQL & ") " & strSearchSQLSuffix
	else
		strSQL = strSearchSQLPrefix & " " & strSearchSQLSuffix
	end if
	if tfHasContent then
		BuildSearchSQL = strSQL
	else
		BuildSearchSQL = ""
	end if
end function

sub SaveOldGroups
	dim intGroupIndex
	for intGroupIndex = 1 to intNumGroups
		if intGroupIndex <> intCurrentGroup then
			dim c, x, k, i, strType, strName, strLabel, strWidth, strMax, tfExclude, strValue, strValue2
			k = arySearchGroups(intGroupIndex,0).keys
			i = arySearchGroups(intGroupIndex,0).items
			x = arySearchGroups(intGroupIndex,0).count - 1
			response.write "<INPUT TYPE=""hidden"" NAME=""" & intGroupIndex & "_mode"" VALUE=""" & arySearchGroups(intGroupIndex,1) & """>"
			for c = 0 to x
				strName = k(c)
				strValue = i(c)
				tfExclude = (left(strValue,1) = "y")
				strValue = mid(strValue,3)
				if tfExclude then
					response.write "<INPUT TYPE=""hidden"" NAME=""" & intGroupIndex & "_exclude_" & strName & """ VALUE=""y"">"
				end if
				dim pos
				pos = InStr(strValue,"~")
				if pos > 0 then
					' range
					strValue2 = mid(strValue, pos + 1)
					strValue = left(strValue, pos - 1)
					response.write "<INPUT TYPE=""hidden"" NAME=""" & intGroupIndex & "_value_" & strName & """ VALUE=""" & Server.HTMLEncode(strValue) & """>"
					response.write "<INPUT TYPE=""hidden"" NAME=""" & intGroupIndex & "_value2_" & strName & """ VALUE=""" & Server.HTMLEncode(strValue2) & """>"
				else
					' single
					response.write "<INPUT TYPE=""hidden"" NAME=""" & intGroupIndex & "_value_" & strName & """ VALUE=""" & Server.HTMLEncode(strValue) & """>"
				end if
			next
		end if
	next
end sub

sub DrawSearchField(byVal strDef, varData)
	dim strType, strName, strLabel, strWidth, strMax, strValue, tfExclude, strValue2, x, blnCanExclude
	ParseSearchDef strDef, strType, strName, strLabel, strWidth, strMax
	if arySearchGroups(intCurrentGroup,0).Exists(strName) then
		strValue = arySearchGroups(intCurrentGroup,0)(strName)
		tfExclude = (left(strValue,1) = "y")
		strValue = mid(strValue,3)
	else
		strValue = ""
		tfExclude = false
	end if
	if strType = "label" then
		response.write "<TR BGCOLOR=""" & cBlue & """><TD COLSPAN=""3"">" & fontx(1,1,cWhite) & "<B>" & strLabel & "</B></TD></TR>"
	else
		response.write "<TR><TD ALIGN=""right"">" & strLabel & ":</TD>"
		blnCanExclude = true
		if strType = "dradio" then
			blnCanExclude = false '(varData.count > 2)
		end if
		if blnCanExclude then
			response.write "<TD>"
			response.write "<SELECT NAME=""exclude_" & strName & """ VALUE="""
			if tfExclude then
				response.write "y"
			end if
			response.write """><OPTION VALUE="""">including</OPTION><OPTION VALUE=""y"">excluding</OPTION></SELECT>"
			response.write "</TD>"
		else
			'response.write "including"
		end if
		response.write "<TD>"
		select case strType
			case "t", "d", "n":
				TextInput "value_" & strName, strWidth, strMax, strValue
			case "dr", "nr":
				x = InStr(strValue, "~")
				if x > 0 then
					strValue2 = mid(strValue, x+1)
					strValue = left(strValue, x-1)
				else
					strValue2 = ""
				end if
				TextInput "value_" & strName, strWidth, strMax, strValue
				response.write " to "
				TextInput "value2_" & strName, strWidth, strMax, strValue2
			case "dchk", "dradio":
				' draw series of radio buttons, strWidth = # of columns per row
				DrawCheckboxSeries "value_" & strName, strWidth, strType="dchk", varData
		end select
		response.write "</TD></TR>"
	end if
end sub

sub DrawCheckboxSeries(strName, intColCount, blnCheckbox, dctList)
	dim c, x, i, k, s
	x = dctList.count - 1
	i = dctList.items
	k = dctList.keys
	response.write "<TABLE BORDER=""0"" CELLPADDING=""0"" CELLSPACING=""0""><TR>"
	s = "<INPUT TYPE=""" & iif(blnCheckbox, "checkbox", "radio") & """ CLASS=""checkbox"" NAME=""" & strName & """ VALUE=""" 
	for c = 0 to x
		if (c > 0) and (c mod intColCount = 0) then
			response.write "</TR><TR>"
		end if
		response.write "<TD>" & s & k(c) & """></TD><TD>" & i(c) & "&nbsp;&nbsp;</TD>"
	next
	response.write "</TR></TABLE>"
end sub

sub DrawSearchPage(strTitle)
	strPageTitle = "Search Form - " & strTitle
	DrawSearchListTitle strTitle
	stlBeginTableFrame "100%"
	if strMode = "b" then
		' basic search form
%>
<TABLE BORDER="0" CELLPADDING="3" CELLSPACING="0" WIDTH="100%">
<FORM ACTION="<%= STR_SEARCH_SCRIPT %>" METHOD="GET">
<INPUT TYPE="hidden" NAME="action" VALUE="search">
<INPUT TYPE="hidden" NAME="mode" VALUE="b">
<TR>
	<TD>Search for:</TD>
	<TD><% TextInput "keywords", 30, 64, Request %></TD>
	<TD ALIGN=RIGHT><% stlRawSubmit "submit", "Search" %></TD>
</TR>
</FORM>
</TABLE>
<%
	else
'		if intCurrentGroup <> 0 then
%>
<TABLE BORDER="0" CELLPADDING="4" CELLSPACING="0" WIDTH="100%">
<FORM ACTION="<%= STR_SEARCH_SCRIPT %>" METHOD="GET">
<INPUT TYPE="hidden" NAME="action" VALUE="search">
<INPUT TYPE="hidden" NAME="n_groups" VALUE="<%= intNumGroups + 1 %>">
<INPUT TYPE="hidden" NAME="mode" VALUE="<%= strMode %>">
<INPUT TYPE="hidden" NAME="reset" VALUE="<%= iif(Request("reset") = "r", "r", "") %>">
<INPUT TYPE="hidden" NAME="c_group" VALUE="<%= intCurrentGroup %>">
<%
			SaveOldGroups
			dim c, x, i, k
			i = dctSearchList.Items
			k = dctSearchList.Keys
			x = dctSearchList.count - 1
			for c = 0 to x
				DrawSearchField k(c), i(c)
			next
%>
<TR BGCOLOR="<%= cBlue %>">
	<TD COLSPAN="3" ALIGN="center"><% stlRawSubmit "submit", "Search" %></TD>
</TR>
</FORM>
</TABLE>
<%
'		end if
	end if
	stlEndTableFrame
end sub

sub DrawSearchListTitle(strPageTitle)
%>
<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%">
<TR>
	<TD><%= font(5) %><%= strPageTitle %></TD>
</TR>
</TABLE><%= spacer(1,5) %><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cBlue %>">
<% ' info bar %>
<TR>
	<FORM ACTION="<%= STR_SEARCH_SCRIPT %>" METHOD="GET" NAME="frm">
	<TD><%= fontx(1,1,cWhite) %>
	<INPUT TYPE="hidden" NAME="action" VALUE="modify">
	<INPUT TYPE="hidden" NAME="n_groups" VALUE="<%= intNumGroups %>">
	<INPUT TYPE="hidden" NAME="mode" VALUE="<%= strMode %>">
	<INPUT TYPE="hidden" NAME="reset" VALUE="">
<%'	<INPUT TYPE="hidden" NAME="xexport" VALUE=""> %>
	<INPUT TYPE="hidden" NAME="auxaction" VALUE="">
<%'	&nbsp;<B><A HREF="#" onClick="frm.xexport.value='y'; frm.submit(); return false;">Export Search</A></B> %>
	<% if intNumGroups < INT_MAX_SEARCH_GROUPS and strMode <> "b" and intNumGroups > 0 and strAction = "search" then %>
<%'	&nbsp;|&nbsp; %>
	&nbsp;
	<B><A HREF="#" onClick="frm.reset.value='r'; frm.submit(); return false;">Restrict Search Further</A></B>
	&nbsp;|&nbsp;
	<B><A HREF="#" onClick="frm.submit(); return false;">Add To Search</A></B>
	<% if strSearchAuxText <> "" and strAction = "search" then %>
	&nbsp;|
	<% end if %>
	<% end if %>
	<% if strSearchAuxText <> "" and strAction = "search" then %>
	&nbsp;
	<B><A HREF="#" onClick="frm.auxaction.value='<%= strSearchAuxAction %>'; frm.submit(); return false;"><%= strSearchAuxText %></A></B>
	<% end if %>
	<INPUT TYPE="hidden" NAME="c_group" VALUE="0">
	<% SaveOldGroups %>
	</FONT></TD>
	</FORM>
	<TD ALIGN=RIGHT><%= fontx(1,1,cWhite) & FormatDateTime(date,1) %>&nbsp;</TD>
</TR>
</TABLE>
<% if not blnPrintMode then %>
<TABLE BORDER=0 CELLPADDING=5 CELLSPACING=0 WIDTH="100%">
<TR>
	<TD><%= font(1) %>
	<%= iif(strMode="b", "<B>", "") %><A HREF="<%= STR_SEARCH_SCRIPT %>">Basic Search</A></B> |
	<%= iif(strMode="i", "<B>", "") %><A HREF="<%= STR_SEARCH_SCRIPT %>mode=i">Intermediate Search</A></B> |
	<%= iif(strMode="a", "<B>", "") %><A HREF="<%= STR_SEARCH_SCRIPT %>mode=a">Advanced Search</A></B>
	</TD>
</TR>
</TABLE>
<% end if %>
<%
end sub
%>