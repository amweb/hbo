<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<!--#include file="../config/incSearch.asp"-->
<%

' =====================================================================================
' = File: order_export.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Order List Export (INCOMPLETE)
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_SHOPPER_VIEW) then
	response.redirect "menu.asp"
end if

const STR_PAGE_TITLE = "Order List Export"
const STR_PAGE_TYPE = "adminsorderexport"

const STR_SEARCH_SCRIPT = "order_list.asp"
const STR_DETAIL_SCRIPT = "order_detail.asp?"

OpenConn

dim strStatusClause
strStatusClause = UserAccessStatus_BuildSQLWhere()
if strStatusClause = "" then
	response.redirect "menu.asp"
end if

dim strPageTitle
strPageTitle = STR_PAGE_TITLE

dim intID, strAction, strMode
strMode = "i"
strAction = lcase(Request("auxaction"))
if strAction = "" then
	strAction = lcase(Request("action"))
end if

intID = Request("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if

dim strIncludeBilling, strIncludeShipping, strIncludeNotes
dim blnIncludeBilling, blnIncludeShipping, blnIncludeNotes, strFormat
strFormat = Request("strFormat")
strIncludeBilling = Request("strIncludeBilling")
strIncludeShipping = Request("strIncludeShipping")
strIncludeNotes = Request("strIncludeNotes")

blnIncludeBilling = (ucase(strIncludeBilling) = "Y") 
blnIncludeShipping = (ucase(strIncludeShipping) = "Y")
blnIncludeNotes = (ucase(strIncludeNotes) = "Y")

if strAction = "submit" or (Instr(strAction,"export") > 0) then
	SetExportFlags blnIncludeBilling, blnIncludeShipping, blnIncludeNotes, strFormat
'	response.redirect "order_list.asp?auxaction=export" & iif(blnIncludeNotes,"notes","")
	response.redirect "order_list.asp?" & replace(replace(Request.Form,"action=submit&amp;",""),"auxaction=export",iif(blnIncludeNotes,"auxaction=exportnotes&top=100000","auxaction=export&top=100000"))
end if

'-- new stuff 12/19/05 per Roger

Search_AddLabel "Order Information"
Search_AddNumberRange "#O.intID", "Order ID"
Search_AddDateRange "%O.dtmCreated", "Date Created"
Search_AddText "O.vchCustomReferralName", 30, 30, "Promo Code"
Search_AddText "O.chrMonthlyOrder", 1, 1, "Recurring Order"
'Search_AddDateRange "%O.dtmUpdated", "Date Updated"
'Search_AddDateRange "%O.dtmShipped", "Date Shipped"
	
InitSearchEngine

intCurrentGroup = intNumGroups + 1
if intCurrentGroup > INT_MAX_SEARCH_GROUPS then
	intCurrentGroup = 0
end if

'-- end new


DrawPage
'DrawSearchPage "Order List Export"


response.end

sub DrawPage()
	if FormErrors.count = 0 then
		GetExportFlags blnIncludeBilling, blnIncludeShipping, blnIncludeNotes, strFormat
		strIncludeBilling = iif(blnIncludeBilling, "Y", "")
		strIncludeShipping = iif(blnIncludeShipping, "Y", "")
		strIncludeNotes = iif(blnIncludeNotes, "Y", "")
	end if
	if strFormat = "" then
		strFormat = "CSV"
	end if
%>
<FORM ACTION="<%= strScript %>" METHOD="POST" NAME="frm">
	<INPUT TYPE="hidden" NAME="action" VALUE="submit">
	<INPUT TYPE="hidden" NAME="auxaction" VALUE="export">
	<INPUT TYPE="hidden" NAME="mode" VALUE="<%=strMode%>">
	<INPUT TYPE="hidden" NAME="reset" VALUE="y">
<%
			SaveOldGroups

	DrawFormHeader "100%", STR_PAGE_TITLE, ""
	stlBeginStdTable "100%"
'	response.write "<TR><TD>"

'if false then
'-- new stuff 12/19/05 per Roger
	stlBeginFormSection "100%", 2
			SaveOldGroups
			dim c, x, i, k
			i = dctSearchList.Items
			k = dctSearchList.Keys
			x = dctSearchList.count - 1
			for c = 1 to x
				DrawSearchField k(c), i(c)
			next
	stlEndFormSection
	stlRule
'-- end new stuff
'end if	

	stlBeginFormSection "", 2
	stlRawCellX "<p>To export your order list, select the appropriate options below<br /></p>", "", "", "", 2
'	response.write "and then click on the ""Export Order List"" option when viewing your search results.<BR><BR>"
	
	stlRawCellX "<B>Select Export Options:</B>", "", "", "", 2

	stlCheckbox "strIncludeBilling", "Y", strIncludeBilling, "", "Include Billing Address"
	stlCheckbox "strIncludeShipping", "Y", strIncludeShipping, "", "Include Shipping Address"
	stlCheckbox "strIncludeNotes", "Y", strIncludeNotes, "", "Only Include Orders with Notes that have NOT Shipped"
	stlEndFormSection
	
	stlBeginFormSection "", 2
	stlRawCellX "<B>Select Export Format:</B>", "", "", "", 2

	stlRadio "strFormat", "CSV", strFormat, "", "CSV-text document"
'	stlRadio "strFormat", "EXCEL", strFormat, "", "Microsoft Excel document"
	stlEndFormSection
	
'	response.write "</TD></TR>"
	stlSubmit "Submit"
	
	stlEndStdTable
	response.write "</FORM>"
	
	SetFieldFocus "frm", "strIncludeBilling"
end sub
%>