<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<!--#include file="../config/incSearch.asp"-->
<%
server.scripttimeout = 900
' =====================================================================================
' = File: order_list.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Merchant Order Admin
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin
if not CheckUserAccess(ACC_ORDER_VIEW) and not CheckUserAccess(ACC_REFERAL_ADMIN) then
	response.redirect "menu.asp"
end if


dim strBillAddress1, strBillAddress2, strShipAddress1, strShipAddress2

const STR_PAGE_TITLE = "Order Search"
const STR_PAGE_TYPE = "adminorderlist"

const STR_SEARCH_SCRIPT = "order_list.asp?"
const STR_LIST_SCRIPT 	= "order_list.asp?"
const STR_DETAIL_SCRIPT = "order_detail.asp?"

OpenConn

dim strPageTitle
strPageTitle = STR_PAGE_TITLE

dim dtmThisWeek, dtmThisMonth, dtmThisYear
dtmThisWeek = DateAdd("d", 1-Weekday(Date()), Date())
dtmThisMonth = CDate(Month(Date()) & "/1/" & Year(Date()))
dtmThisYear = CDate("1/1/" & Year(Date()))

strSearchAuxText = "Export Order List"
strSearchAuxAction = "export"

dim strMode, strAction
strMode = lcase(Request("mode"))
if strMode <> "a" and strMode <> "i" then
	strMode = "b"
end if
strAction = lcase(Request("auxaction"))
if strAction = "" then
	strAction = lcase(Request("action"))
end if

dim strComapny, intSalesPersonID,intDistributorId,intCampaignID

intSalesPersonID = "0" & Request("intSalesPersonID")&""

intSalesPersonID = CINT(intSalesPersonID)

intDistributorId = "0" & Request("intDistributorId")&""

intDistributorId = CINT(intDistributorId)

intCampaignID = "0" & Request("intCampaignID")&""

intCampaignID = CINT(intCampaignID)

if Instr(strAction,"export") > 0 then
	'response.Write "strAction: " & strAction
	'response.End
	
	dim blnIncludeBilling, blnIncludeShipping, blnIncludeNotes, strFormat
	GetExportFlags blnIncludeBilling, blnIncludeShipping, blnIncludeNotes, strFormat
	'request("top") = "4000"
'	response.Write "<small>" & blnIncludeBilling & ", " & blnIncludeShipping & ", " & blnIncludeNotes & ", " & strFormat & "</small><br />" & vbcrlf
'	if strAction = "export" and blnIncludeNotes then strAction = "exportnotes"
end if

dim strStatusClause
strStatusClause = UserAccessStatus_BuildSQLWhereX("0")
if ( request("preset") = "pastcampaign" ) then
	strStatusClause = "'S'"
end if
'response.Write "<br />strStatusClause: " & strStatusClause 
if strStatusClause = "" then
	response.redirect "menu.asp"
end if

strSearchSQLPrefix = "SELECT O.*,"

strSearchSQLPrefix = strSearchSQLPrefix & "B.vchLastName AS B_vchLastName,B.vchFirstName AS B_vchFirstName,B.vchCompany AS B_vchCompany,B.vchBusinessUnit AS [Business Unit],B.vchDivision AS B_vchDivision,B.vchEmail AS B_vchEmail"
if Instr(strAction,"export") > 0 then
	
    strSearchSQLPrefix = Replace(strSearchSQLPrefix,"O.*","O.intID,O.chrStatus,O.mnyGrandTotal,O.dtmSubmitted")
    
    ' remove 0-9 In Progress, B Abandoned, P Pending, E Error
	' remove K Declined, S Submitted, C Credit, R ?, U ?, V ?
	' allow X Deposited, H Shipped
	' strStatusClause = replace(strStatusClause,",'X','C','H','R','U','V'","")
'		strStatusClause = replace(strStatusClause,"'0','1','2','3','4','5','6','7','8','9',","")
'		strStatusClause = replace(strStatusClause,",'R','U','V'","")
'		strStatusClause = replace(strStatusClause,"'B',","")
'		strStatusClause = replace(strStatusClause,"'C',","")
'		strStatusClause = replace(strStatusClause,"'P',","")
'		strStatusClause = replace(strStatusClause,"'E',","")
'		strStatusClause = replace(strStatusClause,"'K',","")
'		strStatusClause = replace(strStatusClause,"'Z',","")
 '       strStatusClause = replace(strStatusClause,"'X',","")
    strSearchSQLPrefix = strSearchSQLPrefix & ",C.vchItemName as CampaignName, Br.vchItemName as [Brand], Pr.vchItemName as [Program], Cat.vchItemName as [Category]"
    strSearchSQLPrefix = strSearchSQLPrefix & ",L.vchPartNumber as L_vchPartNumber,L.vchItemName as L_vchItemName,L.mnyUnitPrice as L_mnyUnitPrice, L.intQuantity as L_intQuantity, (IsNull(L.intQuantity,0)* IsNull(L.mnyUnitPrice,0.0)) as L_mnySubTotal, L.chrStatus as L_ChrStatus"


	
end if

strSearchSQLPrefix = strSearchSQLPrefix & ",H.intID AS H_intID"
strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchLastName AS H_vchLastName"
strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchFirstName AS H_vchFirstName"
strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchLabel AS H_vchCompany"
strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchEmail AS H_vchEmail"
strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchAddress1 AS H_vchAddress1"
strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchAddress2 AS H_vchAddress2"
strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchCity AS H_vchCity"
strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchState AS H_vchState"
strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchZip AS H_vchZip"
strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchDayPhone AS H_vchDayPhone"
strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchBusinessUnit AS H_vchBusinessUnit"
strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchDivision AS H_vchDivision"

if Instr(strAction,"export") > 0 then
    strSearchSQLPrefix = strSearchSQLPrefix & ",I.vchSpecifications AS [Item Specifications]"
end if

strSearchSQLPrefix = strSearchSQLPrefix & " FROM " & STR_TABLE_ORDER & " AS O"

if Instr(strAction,"export") > 0 then
    strSearchSQLPrefix = strSearchSQLPrefix & " LEFT JOIN " & STR_TABLE_LINEITEM & " AS L ON O.intID=L.intOrderID AND (L.chrStatus='A' or L.chrStatus='C')"
    strSearchSQLPrefix = strSearchSQLPrefix & " LEFT JOIN " & STR_TABLE_INVENTORY & " AS I ON I.intID=L.intInvID AND I.chrStatus='A'"
    strSearchSQLPrefix = strSearchSQLPrefix & " LEFT JOIN " & STR_TABLE_INVENTORY & " AS C ON C.intID=I.intCampaign AND C.chrStatus='A'"
    strSearchSQLPrefix = strSearchSQLPrefix & " LEFT JOIN " & STR_TABLE_INVENTORY & " AS Br ON Br.intID=I.intBrand AND Br.chrStatus='A'"    
    strSearchSQLPrefix = strSearchSQLPrefix & " LEFT JOIN " & STR_TABLE_INVENTORY & " AS Pr ON Pr.intID=I.intProgram AND Pr.chrStatus='A'"
    strSearchSQLPrefix = strSearchSQLPrefix & " LEFT JOIN " & STR_TABLE_INVENTORY & " AS Cat ON Cat.intID=I.intCategory AND Cat.chrStatus='A'"
end if
	strSearchSQLPrefix = strSearchSQLPrefix & " LEFT OUTER JOIN " & STR_TABLE_SHOPPER & " AS B ON O.intShopperID=B.intID"

	strSearchSQLPrefix = strSearchSQLPrefix & " LEFT OUTER JOIN " & STR_TABLE_SHOPPER & " AS P ON O.intBillShopperID=P.intID"
    strSearchSQLPrefix = strSearchSQLPrefix & " LEFT OUTER JOIN " & STR_TABLE_SHOPPER & " AS H ON O.intShipShopperID=H.intID"
  
	strSearchSQLPrefix = strSearchSQLPrefix & " WHERE (O.intShopperID<>0) AND O.intShipShopperID IS NOT NULL AND B.vchLastName IS NOT NULL"

if intSalesPersonID>0 Then
    strSearchSQLPrefix = strSearchSQLPrefix & " AND O.intShopperID=" & intSalesPersonID
End If

if intDistributorId>0 Then
    strSearchSQLPrefix = strSearchSQLPrefix & " AND O.intShipShopperID=" & intDistributorId
End If

if CheckUserAccess(ACC_REFERAL_ADMIN) AND NOT CheckUserAccess(ACC_ADMIN) then
	strSearchSQLPrefix = strSearchSQLPrefix & " AND O.intShopperID in (SELECT intID FROM " & STR_TABLE_SHOPPER & " WHERE intManagerID=" & Session(SESSION_MERCHANT_UID) & ") "
end if

if intCampaignID>0 Then
    strSearchSQLPrefix = strSearchSQLPrefix & " AND (SELECT COUNT(*) FROM " & STR_TABLE_INVENTORY & " AS V INNER JOIN " & STR_TABLE_LINEITEM & " AS L ON V.intID= L.intInvID WHERE L.intOrderID=O.intID AND V.intCampaign=" & intCampaignID & ")>0 "
End If

if strMode<>"a" or Instr(strAction,"export") > 0 then
	strSearchSQLPrefix = strSearchSQLPrefix & " AND O.chrStatus IN (" & strStatusClause & ")"
end if
'strSearchSQLPrefix = strSearchSQLPrefix & " AND O.dtmSubmitted IS NOT NULL"
if strAction = "exportnotes" then
	strSearchSQLPrefix = strSearchSQLPrefix & " AND O.dtmShipped IS NULL "
	strSearchSQLPrefix = strSearchSQLPrefix & " AND NOT O.txtGiftMessage IS NULL "
end if
	if (request("preset") = "pastcampaign") then
		strSearchSQLSuffix = " AND O.intID IN (select distinct Z.intID from " & STR_TABLE_ORDER & " AS Z inner JOIN " & STR_TABLE_LINEITEM & " L ON Z.intID=L.intOrderID inner JOIN " & STR_TABLE_INVENTORY & " I ON L.intInvId=I.intID inner JOIN " & STR_TABLE_INVENTORY & " C ON I.intCampaign=C.intID where C.chrStatus='I' AND Z.chrStatus IN ('S')) ORDER BY O.intID DESC"
	elseif (request("preset") = "campaign") then
		strSearchSQLSuffix = " AND O.intID IN (select distinct Z.intID from " & STR_TABLE_ORDER & " AS Z inner JOIN " & STR_TABLE_LINEITEM & " L ON Z.intID=L.intOrderID inner JOIN " & STR_TABLE_INVENTORY & " I ON L.intInvId=I.intID inner JOIN " & STR_TABLE_INVENTORY & " C ON I.intCampaign=C.intID where C.chrStatus='A') ORDER BY O.intID DESC"	
	else
		strSearchSQLSuffix = "ORDER BY O.intID DESC"
	end if
	'response.write "<br />" & strSearchSQLPrefix & "" & strSearchSQLSuffix & "<br />" & vbcrlf
	'response.end

Search_AddLabel "Order Information"
Search_AddNumberRange "#O.intID", "Order ID"
Search_AddDateRange "%O.dtmCreated", "Date Created"
Search_AddDateRange "%O.dtmUpdated", "Date Updated"
Search_AddDateRange "%O.dtmSubmitted", "Date Submitted"
Search_AddDateRange "%O.dtmShipped", "Date Shipped"
Search_AddText "O.vchCustomReferralName", 30, 30, "Promo Code"

if strMode = "a" then
	Search_AddText "O.vchCreatedByIP", 15, 15, "Created by IP"
	Search_AddText "O.vchUpdatedByIP", 15, 15, "Updated by IP"
	Search_AddText "O.vchCreatedByUser", 15, 32, "Created by User"
	Search_AddText "O.vchUpdatedByUser", 15, 32, "Updated by User"
	Search_AddCheckboxGroup "+O.chrType", 4, dctOrderTypeValues, "Type"
	Search_AddCheckboxGroup "+O.chrStatus", 4, dctOrderStatusSearchValues, "Status"
end if
Search_AddText "O.vchShippingNumber", 32, 32, "Shipping/Conf Number"
if strMode = "a" then
	Search_AddText "O.vchShopperID", 15, 15, "Shopper IP"
	Search_AddText "O.vchShopperBrowser", 32, 64, "Shopper Browser"
	Search_AddCheckboxGroup "+O.chrPaymentMethod", 2, dctPaymentMethod, "Payment Method"
end if
Search_AddText "O.chrMonthlyOrder", 1, 1, "Recurring Order"

Search_AddLabel "Shopper Information"
Search_AddText "B.vchLastName", 24, 32, "Last Name"
Search_AddText "B.vchFirstName", 24, 32, "First Name"
Search_AddText "B.vchCompany", 24, 32, "Company"
Search_AddLabel "Credit Card Information"
Search_AddText "O.vchPaymentCardType", 24, 32, "Card Type"
Search_AddText "O.vchPaymentCardName", 24, 32, "Name on Card"
Search_AddText "O.vchPaymentCardNumber", 24, 32, "Card Number"
if strMode = "a" or strMode = "i" then
	Search_AddDateRange "%O.vchPaymentCardExp", "Expiration Date"
end if
Search_AddLabel "Summary Information"
if strMode = "a" then
	Search_AddCheckboxGroup "+#O.intTaxZone", 2, GetTaxZoneList(), "Tax Zone"
	Search_AddNumber "#O.mnyNonTaxableSubtotal", "Non-Taxable Subtotal"
	Search_AddNumber "#O.mnyTaxableSubtotal", "Taxable Subtotal"
	Search_AddNumber "#O.fltTaxRate", "Tax Rate"
	Search_AddNumber "#O.mnyTaxAmount", "Tax Amount"
	Search_AddNumber "#O.mnyShipAmount", "Shipping Amount"
	Search_AddRadioGroup "+O.chrShipTaxFlag", 2, dctYesNoValues, "Shipping Tax Flag"
end if
Search_AddNumber "#O.mnyGrandTotal", "Grand Total"
	
Search_AddLabel "Referral Discount"
'Search_AddText "O.vchRefCode", 12, 12, "Referral Code"
Search_AddText "O.vchReferalName", 32, 32, "Referral"
'	response.write "Action: " & strAction & "<br />" & vbcrlf

InitSearchEngine

dim strSQL, rsData
if strAction = "search" or Instr(strAction,"export") > 0 then
	
	if strMode = "b" then
		strSQL = BuildBasicSearchSQL()
	else
		strSQL = BuildSearchSQL()
	end if
	if strSQL <> "" then
		'response.write strSQL
		'response.end

		set rsData = gobjConn.execute(strSQL)
	else
		strAction = ""
	end if
end if

intCurrentGroup = intNumGroups + 1
if intCurrentGroup > INT_MAX_SEARCH_GROUPS then
	intCurrentGroup = 0
end if
if Instr(lcase(Request("auxaction")),"export") > 0 then
  if rsData.eof then
	DrawSearchResults rsData
	
  else
	if strAction = "exportnotes" then
		DrawExportNotes blnIncludeBilling, blnIncludeShipping, strFormat
	else
		DrawExport blnIncludeBilling, blnIncludeShipping, strFormat
	end if
  end if
else
	if strAction = "search" then
		DrawSearchResults rsData
	else
		DrawSearchPage "Orders"
	end if
	
end if

if IsObject(rsData) then
	rsData.close
	set rsData = nothing
end if


sub LoadPresetSearch(strPreset)
	
	strMode = "i"
	select case strPreset
		case "open":
			arySearchGroups(1,0).Add "+O.chrStatus", "n~S+A+Z+E+V"
		case "48hours":
			arySearchGroups(1,0).Add "O.dtmUpdated", "n~" & DateAdd("h", -48, Now()) & "~"
		case "72hours":
			arySearchGroups(1,0).Add "O.dtmUpdated", "n~" & DateAdd("h", -72, Now()) & "~"
		case "thisweek":
			arySearchGroups(1,0).Add "O.dtmUpdated", "n~" & DateAdd("d", 1-Weekday(Date()), Date()) & "~"
		case "thismonth":
			arySearchGroups(1,0).Add "O.dtmUpdated", "n~" & Month(Date()) & "/1/" & Year(Date()) & "~"
		case "thisyear":
			arySearchGroups(1,0).Add "O.dtmUpdated", "n~1/1/" & Year(Date()) & "~"
		case "all":
			arySearchGroups(1,0).Add "O.chrStatus", "~D"
		case "placedw":
			arySearchGroups(1,0).Add "O.dtmUpdated", "n~" & DateAdd("d", 1-Weekday(Date()), Date()) & "~"
			arySearchGroups(1,0).Add "+O.chrStatus", "n~S+A+Z+X+C+H+K+E+R"
		case "placedm":
			arySearchGroups(1,0).Add "O.dtmUpdated", "n~" & Month(Date()) & "/1/" & Year(Date()) & "~"
			arySearchGroups(1,0).Add "+O.chrStatus", "n~S+A+Z+X+C+H+K+E+R"
		case "placedy":
			arySearchGroups(1,0).Add "O.dtmUpdated", "n~1/1/" & Year(Date()) & "~"
			arySearchGroups(1,0).Add "+O.chrStatus", "n~S+A+Z+X+C+H+K+E+R"
		case "recur":
			arySearchGroups(1,0).Add "O.chkRecurringOrder", "n~Y"
		case "recurinfo":
			arySearchGroups(1,0).Add "O.chkMonthlyOrderInfo", "n~Y"	
		case else:	' invalid preset
			
			
	end select
	



	Session("presetType") = strPreset
end sub

sub DrawSearchResults(rsInput)
call DrawHeaderUpdate()
	'response.Write "Here!"
	'response.end
	strPageTitle = "Search Results - Orders"
	DrawSearchListTitle strPageTitle
	DrawListHeader
	if rsInput.eof then
%>
<TR>
	<TD COLSPAN="5" ALIGN="center">No orders were found matching your search.</TD>
</TR>
<%
	else
		while not rsInput.eof AND response.IsClientConnected()
			DrawListItem rsInput
			rsInput.MoveNext
			if not rsInput.eof then
				DrawListItemSep
			end if
		wend
	end if
	DrawListFooter
end sub

sub DrawListHeader
%>
		<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%">
		<TR BGCOLOR="<%= cVLtYellow %>">
			<TD>&nbsp;</TD>
			<TD NOWRAP><B>Order #</B></TD>
			<TD WIDTH="25%"><B>Store</B></TD>
            <TD WIDTH="25%"><B>Distributor</B></TD>
			<TD NOWRAP>&nbsp;<B>Created</B></TD>
			<TD NOWRAP>&nbsp;<B>Submitted</B></TD>
			<TD NOWRAP>&nbsp;<B>Shipped</B></TD>
			<TD NOWRAP ALIGN="right"><B>Amount</B>&nbsp;</TD>
			<TD NOWRAP ALIGN="right"><B>Status</B>&nbsp;</TD>
		</TR>
<%
end sub

sub DrawListItem(rsInput)
	dim intID, strName, strCompany, mnyPrice, strStatus, dtmCreated, dtmUpdated, dtmSubmitted, dtmShipped, chrViewed,strDistributor
	dim dtmSentToPrinter
	intID = rsInput("intID")
	strName = rsInput("B_vchLastName") & ", " & rsInput("B_vchFirstName")
	strComapny = rsInput("B_vchCompany")
	mnyPrice = rsInput("mnyGrandTotal")
	strStatus = rsInput("chrStatus")
	dtmCreated = rsInput("dtmCreated")
	dtmSubmitted = rsInput("dtmSubmitted")
	dtmCreated = rsInput("dtmUpdated")
	dtmShipped = rsInput("dtmShipped")
    strDistributor = rsInput("H_vchLastName") & ",&nbsp;" & rsInput("H_vchFirstName")
'	dtmSentToPrinter = rsInput("dtmSentToPrinter")
'	chrViewed = rsInput("chrViewed")
	dim strColor
	if strStatus = "E" then
		strColor = " BGCOLOR=""#FFCCCC"""
	else
		strColor = ""
	end if
	'response.write "name: |" & rsInput("B_vchLastName")
	'response.write "name: |" & rsInput("vchPaymentCardName") & "|"

%>
		<TR VALIGN=BOTTOM<%= strColor %>>
			<TD ALIGN=CENTER><A HREF="<%= STR_DETAIL_SCRIPT %>id=<%= intID %>"><IMG SRC="<%= imagebase %>icon_order.gif" WIDTH="16" HEIGHT="16" ALT="View Order Details" BORDER="0"></A></TD>
			<TD NOWRAP><%= intID %></TD>
			<TD WIDTH="25%"><A HREF="<%= STR_DETAIL_SCRIPT %>id=<%= intID %>"><%= strName %></A></TD>
			<TD WIDTH="25%"><%= strDistributor %></A></TD>
            <TD NOWRAP>&nbsp;<%= FormatDateTimeNoSeconds(dtmCreated) %></TD>
			<TD NOWRAP>&nbsp;<%= FormatDateTimeNoSeconds(dtmSubmitted) %></TD>
			<TD NOWRAP>&nbsp;<%= FormatDateTimeNoSeconds(dtmShipped) %></TD>
			<TD NOWRAP ALIGN="right">&nbsp;<%= SafeFormatCurrency(fontx(1,1,cLight) & "n/a</FONT>", mnyPrice, 2) %>&nbsp;</TD>
			<TD NOWRAP ALIGN="right"><%= GetArrayValue(strStatus, dctOrderStatus) %>&nbsp;</TD>
		</TR>
<%
end sub

sub DrawListItemSep
%>
		<TR>
			<TD COLSPAN=9><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVVLtGrey) %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
		</TR>
<%
end sub

sub DrawListFooter
%>
		</TABLE>
<%
end sub

'----- draw export ------

sub DrawExport(blnIncludeBilling, blnIncludeShipping, strFormat)
	'response.Write "in DrawExport"
	dim strFieldSep, strFieldQuote, strFieldAltQuote, strFileName, strData, strEOL, strGiftMessage, sqlCampaignName, rsCampaignName, strSuffix
	dim strLine, strItem, strNote
	if strFormat = "CSV" then
		strFileName = "orderexport.csv"
		strFieldSep = ","
		strFieldQuote = """"
		strFieldAltQuote = "'"
		strEOL = vbCrLf
	else
		strFileName = "orderexport.xls"
		strFieldSep = Chr(9)
		strFieldQuote = ""
		strFieldAltQuote = ""
		strEOL = vbCrLf
	end If

	if request("intCampaignID") <> "" then
		sqlCampaignName = "select vchItemName from " & STR_TABLE_INVENTORY & " WHERE chrType='C' and intID=" & request("intCampaignID")
       if instr(strFileName, ".csv") > 0 then
			strSuffix = ".csv"
		else
			strSuffix = ".xls"
		end if
        set rsCampaignName = gobjConn.execute(sqlCampaignName)
        While not rsCampaignName.Eof
			strFileName = rsCampaignName("vchItemName") & " Report" & strSuffix
			rsCampaignName.MoveNext
        Wend           
    end if

	response.ContentType = "text/csv"
	response.AddHeader "Content-transfer-encoding", "binary"
	response.AddHeader "Content-Disposition", "attachment; filename=" & strFileName

	if strFieldQuote <> "" then
        
       Response.Write """Status"",""POS #"",""Description"",""Program"",""Brand"",""Type"",""User"",""Qty"",""Cost"",""Total Cost"",""Sales Person"",""Distributor"",""Contact"",""City"",""State"",""Zip"",""Dist#"",""Zone"",""BU""" & vbCrLf

		while not rsData.eof
			'Response.Write """" & rsData("intID") & ""","
			Response.Write """" & iif(rsData("L_chrStatus") = "A", "Approved", "Cancelled") & ""","
			'Response.Write """" & rsData("CampaignName") & ""","  
			Response.Write """" & rsData("L_vchPartNumber") & ""","    
            Response.Write """" & rsData("L_vchItemName") & ""","
            Response.Write """" & rsData("Program") & ""","
            Response.Write """" & rsData("Brand") & ""","
            Response.Write """" & rsData("Category") & ""","
            Response.Write """" & rsData("B_vchEmail") & ""","
            Response.Write """" & rsData("L_intQuantity") & ""","
            Response.Write """" & rsData("L_mnyUnitPrice") & ""","
            Response.Write """" & rsData("L_mnySubTotal") & ""","
            Response.Write """" & rsData("B_vchFirstName") & " " & rsData("B_vchLastName") & ""","
            Response.Write """" & rsData("H_vchCompany") & ""","
            Response.Write """" & rsData("H_vchFirstName") & " " & rsData("H_vchLastName") & ""","
            Response.Write """" & rsData("H_vchCity") & ""","
            Response.Write """" & rsData("H_vchState") & ""","
            Response.Write """" & rsData("H_vchZip") & ""","
            Response.Write """" & rsData("H_intID") & ""","
            Response.Write """" & rsData("B_vchDivision") & ""","
            Response.Write """" & rsData("Business Unit") & ""","
            Response.Write vbCrLf
			rsData.MoveNext
		wend
	end if
end sub

'----- draw export notes ------

sub DrawExportNotes(blnIncludeBilling, blnIncludeShipping, strFormat)
	dim strFieldSep, strFieldQuote, strFieldAltQuote, strFileName, strData, strEOL, strGiftMessage, sqlCampaignName, rsCampaignName, strSuffix
	response.ContentType = "text/csv"
	response.AddHeader "Content-transfer-encoding", "binary"
	if strFormat = "CSV" then
		strFileName = "ordernotesexport.csv"
		strFieldSep = ","
		strFieldQuote = """"
		strFieldAltQuote = "'"
		strEOL = vbCrLf
	else
		strFileName = "ordernotesexport.xls"
		strFieldSep = Chr(9)
		strFieldQuote = ""
		strFieldAltQuote = ""
		strEOL = vbCrLf
	end if

	if request("intCampaignID") <> "" then
		sqlCampaignName = "select vchItemName from " & STR_TABLE_INVENTORY & " WHERE chrType='C' and intID=" & request("intCampaignID")
		if instr(strFileName, ".csv") > 0 then
			strSuffix = ".csv"
		else
			strSuffix = ".xls"
		end if
        set rsCampaignName = gobjConn.execute(sqlCampaignName)
        While not rsCampaignName.Eof
			strFileName = rsCampaignName("vchItemName") & " Report" & strSuffix
			rsCampaignName.MoveNext
        Wend           
    end if
	
	response.AddHeader "Content-Disposition", "filename=" & strFileName

	while not rsData.eof
		strGiftMessage = replace(replace(rsData("txtGiftMessage")&"", vbCrLf, " "), vbcr, " ")

		response.write rsData("intID") & ","
		response.write strFieldQuote & replace(replace(rsData("dtmCreated") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","

'			response.write rsData("intShopperID") & ","
'			response.write strFieldQuote & replace(replace(rsData("B_vchLastName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
'			response.write strFieldQuote & replace(replace(rsData("B_vchFirstName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
'			response.write strFieldQuote & replace(replace(rsData("B_vchEmail") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","

'		response.write strFieldQuote & replace(replace(rsData("vchShippingNumber") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
'		response.write strFieldQuote & replace(replace(GetArrayValue(rsData("chrStatus"), dctOrderStatus) & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","

		if blnIncludeBilling then
'			response.write rsData("intBillShopperID") & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchLastName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchFirstName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchCompany") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			
			strBillAddress1 = TrimCarriageReturnChars(rsData("P_vchAddress1"))
			response.write strFieldQuote & replace(replace(strBillAddress1 & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			
			strBillAddress2 = TrimCarriageReturnChars(rsData("P_vchAddress2"))
			response.write strFieldQuote & replace(replace(strBillAddress2 & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			
			response.write strFieldQuote & replace(replace(rsData("P_vchCity") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchState") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchZip") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchDayPhone") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchEmail") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
		end if
		
		if blnIncludeShipping then
'			response.write rsData("intShipShopperID") & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchLastName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchFirstName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchCompany") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			
			strShipAddress1 = TrimCarriageReturnChars(rsData("H_vchAddress1"))
			response.write strFieldQuote & replace(replace(strShipAddress1 & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			
			strShipAddress2 = TrimCarriageReturnChars(rsData("H_vchAddress2"))
			response.write strFieldQuote & replace(replace(strShipAddress2 & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			
			response.write strFieldQuote & replace(replace(rsData("H_vchCity") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchState") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchZip") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchDayPhone") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchEmail") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
		end if

		'/// get line items
		dim mySQLString,myItemDataSet,myItemCounter,myPaddingCounter
			
		myItemCounter = 0
		mySQLString = "SELECT TOP 3 intQuantity, vchPartNumber FROM " & STR_TABLE_LINEITEM & " WHERE chrStatus='A' AND intOrderID=" & rsData("intID") & ""
		set myItemDataSet = gobjConn.Execute(mySQLString)
			
		while not myItemDataSet.eof
			response.write myItemDataSet("intQuantity") & ","
			response.write strFieldQuote & replace(replace(myItemDataSet("vchPartNumber") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			myItemCounter = myItemCounter + 1
			myItemDataSet.MoveNext()
		wend
			
		for myPaddingCounter = myItemCounter to 2
			response.Write ","&","
		next

		response.write strFieldQuote & replace(replace(strGiftMessage & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote
		response.write strEOL
		rsData.MoveNext
	wend
end sub

function TrimCarriageReturnChars(strAddress)
	if isvalid(strAddress) then
		strAddress = replace(strAddress, vbcr, "")
		strAddress = replace(strAddress, vbcrlf, "")
		strAddress = replace(strAddress, vblf, "")
		TrimCarriageReturnChars = strAddress
	end if
end function

%>