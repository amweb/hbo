<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: report_state.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Generate report by state
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_REPORT_VIEW) then
	response.redirect "menu.asp"
end if

dim strPageType, strPageTitle

strPageType = "adminrepsummary"
strPageTitle = "Order Report By State"

OpenConn

dim strSQL, rsData, dtmStart, dtmEnd, intRptType, strSQLColumn
GetReportDateRangeX dtmStart, dtmEnd, intRptType

strSQLColumn = GetSQLColumnName
'response.End

strSQL = "SELECT"
strSQL = stRSQL & " COUNT(DISTINCT O.intID) AS intOrderCount,"
strSQL = strSQL & " SUM(O.mnyGrandTotal) AS mnyGrandTotal,"
strSQL = strSQL & " SUM(O.mnyTaxAmount) AS mnyTaxAmount,"
strSQL = strSQL & " SUM(O.mnyShipAmount) AS mnyShipAmount,"
strSQL = strSQL & " B.vchState AS vchBillState"
strSQL = strSQL & " FROM " & STR_TABLE_ORDER & " AS O, " & STR_TABLE_SHOPPER & " AS B"
strSQL = strSQL & " WHERE O.chrStatus NOT IN ('D')"	' only show shipped or deposited orders
strSQL = strSQL & " AND O.intBillShopperID = B.intID"
'strSQL = strSQL & strSQLColumn
strSQL = strSQL & " GROUP BY B.vchState"
strSQL = strSQL & " ORDER BY B.vchState"

'response.write strSQL
set rsData = gobjConn.execute(strSQL)


DrawPage


rsData.close
set rsData = nothing
response.end

sub DrawPage
	call DrawHeaderUpdate()
%>
<CENTER><%= font(1) %>
<%= font(2) %><B><%= STR_SITE_NAME %><BR>
<%= strPageTitle %><BR>
<%= GetDateRangeType() %> </B></FONT><BR>
<BR>
<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="2">
<TR>
	<TD>&nbsp;</TD>
	<TD ALIGN="center"><B># Orders</B></TD>
	<TD ALIGN="center" COLSPAN="2"><B>Amount</B></TD>
	<TD ALIGN="center" COLSPAN="2"><B>Tax</B></TD>
	<TD ALIGN="center" COLSPAN="2"><B>Shipping</B></TD>
</TR>
<TR>
	<TD>&nbsp;</TD>
	<TD ALIGN="center">&nbsp;</TD>
	<TD ALIGN="center"><B>Total</B></TD>
	<TD ALIGN="center"><B>Avg</B></TD>
	<TD ALIGN="center"><B>Total</B></TD>
	<TD ALIGN="center"><B>Avg</B></TD>
	<TD ALIGN="center"><B>Total</B></TD>
	<TD ALIGN="center"><B>Avg</B></TD>
</TR>
<%
	while not rsData.eof
%>
<TR>
	<TD><B>State: <%= rsData("vchBillState") %></B></TD>
	<TD ALIGN="center"><%= rsData("intOrderCount") %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData("mnyGrandTotal"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData("mnyGrandTotal") / rsData("intOrderCount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData("mnyTaxAmount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData("mnyTaxAmount") / rsData("intOrderCount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData("mnyShipAmount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData("mnyShipAmount") / rsData("intOrderCount"), 2) %></TD>
</TR>
<%
		rsData.MoveNext
	wend
%>
</TABLE>
</FONT></CENTER>
<%
end sub
%>