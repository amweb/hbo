<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: inv_list.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Inventory Admin
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_INVENTORY_VIEW) then
	response.redirect "main.asp"
end if

dim blnCanEdit
blnCanEdit = CheckUserAccess(ACC_INVENTORY_EDIT)

dim strPageType, strPageTitle

strPageType = "admininvlist"
strPageTitle = "Inventory Listing by Format"

const STR_LIST_SCRIPT = "inv_listbrands.asp?"
const STR_MOVE_SCRIPT = "inv_move.asp?"
const STR_DETAIL_FOLDER_SCRIPT = "inv_fdetail.asp?"
const STR_DETAIL_ITEM_SCRIPT = "inv_idetail.asp?"
const STR_STOCK_LOW_SCRIPT = "inv_stock.asp?"
const STR_STOCK_HIGH_SCRIPT = "inv_stock.asp?"

OpenConn

if Request("action") = "updateprice" then
	UpdatePrices
	CloseConn
	response.redirect STR_LIST_SCRIPT & "brand=" & Request.form("brand")
elseif Request("action") = "resetcount" then
	if blnCanEdit then Inventory_ResetHitCount Request("ID")
end if

dim strSQL, rsData

' gather inventory statistics
dim intLowCount, intHighCount, intInvCount
strSQL = "SELECT "
strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_INVENTORY & " WHERE chrStatus<>'D' AND chrType='I') AS intCount,"
strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_INVENTORY & " WHERE chrStatus<>'D' AND chrType='I' AND intStock < intLowStock) AS intLowCount,"
strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_INVENTORY & " WHERE chrStatus<>'D' AND chrType='I' AND intStock > intHighStock) AS intHighCount"
set rsData = gobjConn.execute(strSQL)
intInvCount = rsData("intCount")
intLowCount = rsData("intLowCount")
intHighCount = rsData("intHighCount")
rsData.close
set rsData = nothing

' get folder list
dim dctNavList, intParentID, intBrandID

intParentID = Request("parentid")
if IsNumeric(intParentID) then
	intParentID = CLng(intParentID)
else
	intParentID = 0
end if

intBrandID = Request("brand")
if IsNumeric(intBrandID) then
	intBrandID = CLng(intBrandID)
else
	intBrandID = 0
end if

BuildNavList intParentID

' SWSCodeChange - 04aug2000 added chrParentStatus
' not implemented for thecampaignstore - , I.chrParentStatus, I.dtmHitReset
strSQL = "SELECT P.intID AS P_intID, P.vchItemName AS P_vchItemName, I.intID, I.chrType, I.chrStatus, I.intSortOrder, I.vchPartNumber, I.vchItemName, I.mnyItemPrice, I.mnyShipPrice, I.intStock, I.intHitCount, I.intSortOrder"
strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY & " AS I"
strSQL = strSQL & " left JOIN " & STR_TABLE_INVENTORY & " AS P"
strSQL = strSQL & " ON I.intParentID = P.intID"
strSQL = strSQL & " WHERE I.chrStatus<>'D'"
if intBrandID <> 0 then
	strSQL = strSQL & " AND I.chrType = 'I' AND I.intBrand = " & intBrandID
else
	strSQL = strSQL & " AND I.chrType = 'B'"
end if
strSQL = strSQL & " ORDER BY (CASE I.chrType WHEN 'B' THEN I.vchItemName ELSE P.vchItemName END), P.intID, I.vchItemName"
set rsData = gobjConn.execute(strSQL)


DrawPage


rsData.close
set rsData = nothing
response.end

sub DrawPage
call DrawHeaderUpdate()

	dim c, strPrevType, blnCanMoveUp, blnCanMoveDown
	dim intID, strType, strStatus, strPartNumber, B_intID, B_strName, strName, mnyPrice, mnyShipPrice, intStock
	dim intHitCount, strHitReset, dtmHitReset
	dim intSortOrder, intThisParent
	dim strParentStatus
	' SWSCodeChange - 04aug2000 added strParentStatus
	' not implemented in thecampaignstore
	
	call DrawHeaderUpdate()
	
	DrawListTitle
	DrawListNavigation
	DrawListHeader
	strPrevType = ""
	if rsData.eof then
		%>
		<tr>
		<td></td>
		<td class="font1" colspan=6>&nbsp;<BR />
		<span class="font2"><I>This folder contains no items or sub-folders.</I></span><BR />&nbsp;
		</td>
		</tr>
		<%
	else
		intThisParent = 0
		while not rsData.eof
			if strPrevType <> rsData("chrType") then
				strPrevType = rsData("chrType")
				c = 0
			end if
			c = c + 1
			if intThisParent <> rsData("P_intID") then
%>
		<tr>
		<td</td>
		<td class="font1" colspan=6>&nbsp;<BR />
		<span class="font2"><B><%= rsData("P_vchItemName") %></B></span>&nbsp;
		</td>
		</tr>
<%
				intThisParent = rsData("P_intID")
			end if
			intID = rsData("intID")
			strType = rsData("chrType")
			strStatus = rsData("chrStatus")
			'strParentStatus = rsData("chrParentStatus") ' SWSCodeChange - 04aug2000 added strParentStatus
			strPartNumber = rsData("vchPartNumber")
			strName = rsData("vchItemName")
			mnyPrice = rsData("mnyItemPrice")
			mnyShipPrice = rsData("mnyShipPrice")
			intStock = rsData("intStock")
			intHitCount = rsData("intHitCount")
			'dtmHitReset = rsData("dtmHitReset")
			intSortOrder = rsData("intSortOrder")
			blnCanMoveUp = (c > 1) and blnCanEdit
			blnCanMoveDown = false
			rsData.MoveNext
			if not rsData.eof then
				if rsData("chrType") = strType then
					blnCanMoveDown = true and blnCanEdit
				end if
			end if
			' SWSCodeChange - 04aug2000 added parent status
			'strHitReset = FormatDateTime(dtmHitReset, 1)
			'strHitReset = mid(strHitReset, instr(strHitReset, " ") + 1)
			DrawListItem intID, strType, strStatus, strParentStatus, strName, strPartNumber, mnyPrice, mnyShipPrice, intStock, blnCanMoveUp, blnCanMoveDown, intHitCount, strHitReset, intSortOrder
			if not rsData.eof then
				DrawListItemSep
			end if
		wend
	end if
	DrawListFooter
end sub

sub DrawListTitle
%>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
	<td class="font5"><%= strPageTitle %></td>
<% if not blnPrintMode then %>
	<td class="font1" align=right>&nbsp;</td>
<% end if %>
</tr>
</table>
<%= spacer(1,5) %><BR />

	<% stlBeginInfoBar INT_PAGE_WIDTH %>
	<B>Total Inventory:</B> <%= intInvCount %>&nbsp;
	<B><A HREF="<%= STR_STOCK_LOW_SCRIPT %>">Low Stock:</A></B> <%= intLowCount %>&nbsp;
	<B><A HREF="<%= STR_STOCK_HIGH_SCRIPT %>">High Stock:</A></B> <%= intHighCount %>&nbsp;
	</FONT></td>
	<td align=right><%= fontx(1,1,cWhite) & FormatDateTime(date,1) %>
	<% stlEndInfoBar %>
<%
end sub

sub DrawListNavigation
%>
<table border="0" cellpadding="6" cellspacing="0" width="100%">
<tr class="font1"><td>
<%
	' dctNavList - reversed order, key=ID, item=name
	dim c, x, i, k
	x = dctNavList.count - 1
	i = dctNavList.items
	k = dctNavList.keys
	for c = x to 0 step -1
		if c > 0 then
			response.write "<B><A HREF=""" & STR_LIST_SCRIPT & "brand=" & intBrandID & "&parentid=" & k(c) & """>" & i(c) & "</A></B> : "
		else
			response.write i(c)
		end if
	next
	response.write "</td>"
	'if x > 0 then
	'	response.write "<td align=right><A HREF=""" & STR_LIST_SCRIPT & "brand=" & intBrandID & "&parentid=" & k(1) & """>To Previous Folder<IMG SRC=""" & imagebase & "icon_folderup.gif"" width=""16"" height=""16"" ALT=""To Previous Folder"" border=""0"" align=""absmiddle"" HSPACE=""5"" /></A></td>"
	'end if
%>
</tr></table>
<%
end sub

sub BuildNavList(byVal intParentID)
	dim strSQL, rsData
	set dctNavList = Server.CreateObject("Scripting.Dictionary")
	while intParentID <> 0
		strSQL = "SELECT intParentID, vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intParentID & " AND chrStatus<>'D'"
		set rsData = gobjConn.execute(strSQL)
		dctNavList.Add intParentID, rsData("vchItemName") & ""
		intParentID = rsData("intParentID")
		rsData.close
		set rsData = nothing
	wend
	if intBrandID <> 0 then
		strSQL = "SELECT intParentID, vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intBrandID & " AND chrStatus<>'D'"
		set rsData = gobjConn.execute(strSQL)
		dctNavList.Add intBrandID, rsData("vchItemName") & ""
		rsData.close
		set rsData = nothing
	end if
	dctNavList.Add 0, "Top"
end sub

sub DrawListHeader
%>
<table border="0" cellpadding="1" cellspacing="0" width="100%" bgcolor="<%= iif(blnPrintMode, cBlack, cVLtGrey) %>">
<FORM ACTION="<%= STR_LIST_SCRIPT %>" NAME="frmPrice" METHOD="POST">
<INPUT TYPE="hidden" NAME="action" VALUE="updateprice" />
<INPUT TYPE="hidden" NAME="brand" VALUE="<%= intBrandID %>" />
<tr>
	<td><table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="<%= cWhite %>">
	<tr>
		<td><table border="0" cellpadding="2" cellspacing="0" width="100%">
		<tr class="font1" bgcolor="<%= cVLtYellow %>">
			<td>&nbsp;</td>
			<td NOWRAP width="40%"><B>Name</B></td>
			<td NOWRAP width="15%"><B>&nbsp;Part #&nbsp;</B></td>
			<td NOWRAP align="right" width="20%"><B>&nbsp;Price&nbsp;&nbsp;&nbsp;</B></td>
			<td NOWRAP align="right"><B>&nbsp;Shipping&nbsp;</B></td>
			<td NOWRAP align="center"><B>&nbsp;Qty&nbsp;</B></td>
			<td NOWRAP align="right"><B>Status</B></td>
		</tr>
<%
end sub

' SWSCodeChange - 04aug2000 added strParentStatus
sub DrawListItem(intID, strType, strStatus, strParentStatus, strName, strPartNumber, mnyPrice, mnyShipPrice, intQty, blnCanMoveUp, blnCanMoveDown, intHitCount, strHitReset, intSortOrder)
	dim strActionLabel
	strActionLabel = iif(blnCanEdit, "Edit", "Move")
	select case strType
		case "I": ' item
%>
		<tr valign=BOTTOM>
			<td align=center><A HREF="<%= STR_DETAIL_ITEM_SCRIPT %>id=<%= intID %>"><IMG SRC="<%= imagebase %>icon_product.gif" width="16" height="16" ALT="<%= strActionlabel %> This Item" border="0" /></A></td>
			<td class="font2"><A HREF="<%= STR_DETAIL_ITEM_SCRIPT %>id=<%= intID %>"><%= strName %></A></td>
			<td class="font1" NOWRAP>&nbsp;<%= iif(IsNull(strPartNumber), "n/a", strPartNumber) %>&nbsp;</td>
			<td NOWRAP align="right">&nbsp;
<% if not blnPrintMode then %>
			<INPUT TYPE="TEXT" NAME="mnyItemPrice<%= intID %>" SIZE="6" MAXLENGTH="10" <%
			if FormErrors.exists("frmPrice" & intID) then
				%>VALUE="<%= Request("mnyItemPrice") %>" STYLE="text-align: right; background-color: <%= cVLtRed %>;"<%
			else
				%>VALUE="<%= SafeFormatCurrency("n/a", mnyPrice, 2) %>" STYLE="text-align: right;"<%
			end if
			%> />
<% else %>
			<%= SafeFormatCurrency(fontx(1,1,cLight) & "n/a</FONT>", mnyPrice, 2) %>&nbsp;
<% end if %>
			&nbsp;</td>
			<td NOWRAP align="right">&nbsp;<%= SafeFormatCurrency(fontx(1,1,cLight) & "n/a</FONT>", mnyShipPrice, 2) %>&nbsp;</td>
			<td NOWRAP align="center">&nbsp;<%= intQty %>&nbsp;</td>
			<% ' SWSCodeChange - 04aug2000 added parent status %>
			<td NOWRAP align="right"><%= font(1) %><%= GetStatusStr(strStatus, true) %> (<%= strParentStatus %>)</td>
		</tr>
<% if not blnPrintMode then %>
		<tr>
			<td></td>
			<td><%= fontx(1,1,cLight) %><% if blnCanEdit then %><A HREF="<%= STR_DETAIL_ITEM_SCRIPT %>id=<%= intID %>"><%= strActionLabel %></A><% end if %></td>
			<td></td>
			<td NOWRAP align=right>
			<%
			if FormErrors.exists("frmPrice" & intID) then
				response.write "(Not saved) "
			end if
			%>
			</td>
		</tr>
<%
			end if
		case "A": ' folder
%>
		<tr valign=center>
			<td align=center><A HREF="<%= STR_LIST_SCRIPT %>brand=<%= intBrandID %>&parentid=<%= intID %>"><IMG SRC="<%= imagebase %>icon_folder.gif" width="16" height="16" ALT="Open This Folder" border="0" /></A></td>
			<td class="font2"><A HREF="<%= STR_LIST_SCRIPT %>brand=<%= intBrandID %>&parentid=<%= intID %>"><%= strName %></A></td>
			<td NOWRAP><%= fontx(1,1,cLight) & "n/a</FONT>" %></td>
			<td NOWRAP align="right"><%= fontx(1,1,cLight) & "&nbsp;n/a&nbsp;</FONT>" %></td>
			<td NOWRAP align="right"><%= fontx(1,1,cLight) & "&nbsp;n/a&nbsp;</FONT>" %></td>
			<td NOWRAP align="center"><%= fontx(1,1,cLight) & "&nbsp;n/a&nbsp;</FONT>" %></td>
			<% ' SWSCodeChange - 04aug2000 added parent status %>
			<td class="font1" NOWRAP align="right"><%= GetStatusStr(strStatus, true) %> (<%= strParentStatus %>)</td>
		</tr>
<% if not blnPrintMode then %>
		<tr>
			<td></td>
			<td colspan=2><%= fontx(1,1,cLight) %><A HREF="<%= STR_LIST_SCRIPT %>brand=<%= intBrandID %>&parentid=<%= intID %>">Open</A>
			<% if blnCanEdit then %> | <A HREF="<%= STR_DETAIL_FOLDER_SCRIPT %>id=<%= intID %>"><%= strActionLabel %></A><% end if %>
			&nbsp;&nbsp;Hit count: <B><%= intHitCount %></B> as of <%= strHitReset %>
			<% if blnCanEdit then %>&nbsp;<A HREF="<%= STR_LIST_SCRIPT %>brand=<%= intBrandID %>&parentid=<%= intParentID %>&id=<%= intID %>&action=resetcount">Reset</A><% end if %>
			</td>
			<td NOWRAP align=right colspan=4></td>
		</tr>
<%
		end if
		case "B": ' brand
%>
		<tr valign=BOTTOM>
			<td align=center><A HREF="<%= STR_LIST_SCRIPT %>brand=<%= intID %>"><IMG SRC="<%= imagebase %>icon_shopper.gif" width="16" height="16" ALT="Open This Folder" border="0"></A></td>
			<td class="font2"><A HREF="<%= STR_LIST_SCRIPT %>brand=<%= intID %>"><%= strName %></A></td>
			<td NOWRAP><%= fontx(1,1,cLight) & "n/a</FONT>" %></td>
			<td NOWRAP align="right"><%= fontx(1,1,cLight) & "&nbsp;n/a&nbsp;</FONT>" %></td>
			<td NOWRAP align="right"><%= fontx(1,1,cLight) & "&nbsp;n/a&nbsp;</FONT>" %></td>
			<td NOWRAP align="center"><%= fontx(1,1,cLight) & "&nbsp;n/a&nbsp;</FONT>" %></td>
			<% ' SWSCodeChange - 04aug2000 added parent status %>
			<td class="font1" NOWRAP align="right"><%= GetStatusStr(strStatus, true) %> (<%= strStatus %>)</td>
		</tr>
<% if not blnPrintMode then %>
		<tr>
			<td></td>
			<td colspan=2><%= fontx(1,1,cLight) %><A HREF="<%= STR_LIST_SCRIPT %>brand=<%= intID %>">Open</A>
			<% if blnCanEdit then %> | <A HREF="inv_bdetail.asp?id=<%= intID %>&return=<%= STR_LIST_SCRIPT %>"><%= strActionLabel %></A><% end if %>
			</td>
			<td NOWRAP align=right colspan=4></td>
		</tr>
<%
		end if
	end select
end sub

sub DrawListItemSep
%>
		<tr>
			<td colspan=7><table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="<%= iif(blnPrintMode, cBlack, cVVLtGrey) %>"><tr><td><%= spacer(1,1) %></td></tr></table></td>
		</tr>
<%
end sub

sub DrawListFooter
%>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td align="right"><INPUT TYPE="submit" VALUE="Update Prices" /></td>
		</tr>
		</table></td>
	</tr>
	</table></td>
</tr>
</FORM>
</table>
<%
end sub

sub UpdatePrices
	dim k, intID, mnyPrice, dctSaveList
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
	dctSaveList.Add "@dtmUpdated", "GETDATE()"
	dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
	dctSaveList.Add "vchUpdatedByIP", gstrUserIP
	for each k in Request.form
		if left(k,12) = "mnyItemPrice" then
			intID = mid(k,13)
			if IsNumeric(intID) then
				intID = CLng(intID)
				mnyPrice = Request.form(k)
				if IsNumeric(mnyPrice) then
					mnyPrice = CDbl(mnyPrice)
					dctSaveList("#mnyItemPrice") = mnyPrice
					SaveDataRecord STR_TABLE_INVENTORY, Request, intID, dctSaveList
				end if
			end if
		end if
	next
end sub
%>