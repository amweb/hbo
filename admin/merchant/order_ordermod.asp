<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: order_ordermod.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Allow merchant to modify items in an order
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

OpenConn

if not CheckUserAccess(ACC_ORDER_VIEW) then
	response.redirect aspbase & "admin/main.asp"
end if

const STR_LIST_SCRIPT = "order_list.asp?"
const STR_PAGE_TYPE = "adminordermod"

dim strPageTitle
strPageTitle = "Modify Order"

dim STR_EDIT_SCRIPT, STR_DETAIL_SCRIPT, STR_EDIT_LINES_SCRIPT

dim intOrderID
intOrderID = Request("order")
if IsNumeric(intOrderID) then
	intOrderID = CLng(intOrderID)
else
	intOrderID = 0
end if

STR_EDIT_SCRIPT = "order_ordermod.asp?order=" & intOrderID & "&"
STR_DETAIL_SCRIPT = "order_detail.asp?id=" & intOrderID & "&"
STR_EDIT_LINES_SCRIPT = "order_itemmod.asp?order=" & intOrderID & "&"

if intOrderID = 0 then
	response.redirect STR_LIST_SCRIPT
end if
gintOrderID = intOrderID

dim intUserID
intUserID = Merchant_GetOrderShopper(intOrderID)

dim strAction, intStep, intMaxStep
strAction = lcase(Request("action"))

'AL - Additional functionality added for modifying referral field via admin
if not instr(strAction, "refmodify") > 0 then
	if left(strAction, 4) <> "step" then
		strAction = ""
	end if
	intStep = mid(strAction, 5, 1)
	if IsNumeric(intStep) then
		intStep = clng(intStep)
	else
		intStep = 6
	end if

	if strAction = "" then
		strAction = "step6"
		intStep = 6
	end if
else
	intStep = 6
end if

dim dctErrors
set dctErrors = Server.CreateObject("Scripting.Dictionary")
'dctErrors.Add "paymentok", true
ValidateOrder_Other intOrderID, dctErrors
if dctErrors.Exists("order") then
	response.redirect STR_LIST_SCRIPT
end if
if dctErrors.Exists("billing") then
	intMaxStep = 1
elseif dctErrors.Exists("shipping") then
	intMaxStep = 2
elseif dctErrors.Exists("shipoption") then
	intMaxStep = 3
'elseif dctErrors.Exists("payment") then
'	intMaxStep = 4
'elseif dctErrors.Exists("giftmsg") then
'	intMaxStep = 5
else
	intMaxStep = 7
end if
set dctErrors = nothing

if not instr(strAction, "refmodify") > 0 then
	if intStep > intMaxStep then
		strAction = "step" & intMaxStep
		intStep = intMaxStep
	end if
end if

dim dctList, intID
'response.write straction
'response.End
select case strAction
	case "step0": ' STEP 0: select account
		if Request("search") <> "" then
			set dctList = Merchant_GetUserAccountList(Request("keywords"))
		end if
		DrawHeaderUpdate
		DrawPage_Step0 dctList
		
	case "step0submit":
		AutoCheck Request, ""
		if FormErrors.count = 0 then
			Merchant_SetOrderShopper intOrderID, Request("shopper")
			RecalcOrder_Other intOrderID
			response.redirect STR_DETAIL_SCRIPT
		else
			if Request("search") <> "" then
				set dctList = Merchant_GetUserAccountList(Request("keywords"))
			end if
			DrawHeaderUpdate
			DrawPage_Step0 dctList
			
		end if
	case "step1", "step2": ' STEP 1: Billing Information		
		if intStep = 1 then
			strPageTitle = "Step 1: Billing Information"
		else
			strPageTitle = "Step 2: Shipping Information"
		end if
		' if the user has billing records, allow them to select one or choose to create a new one
		' if there are no current billing records, go directly to the 'create a new one' screen
		set dctList = GetUserBillingList_Other(intUserID, intStep = 2)		
		if dctList.count > 0 then
			DrawHeaderUpdate
'			DrawCheckoutStatusBar intStep
			DrawPage_Step1or2_Choose intStep, dctList
			
		else
			'quick checkout shoudl always come here			
			DrawHeaderUpdate
'			DrawCheckoutStatusBar intStep
			DrawPage_Step1or2_NewEdit intStep, 0
			
		end if
	case "step1new", "step2new": ' display new billing form
		intID = Request("id")
		if IsNumeric(intID) then
			intID = CLng(intID)
		else
			intID = 0
		end if
		if intStep = 1 then
			strPageTitle = "Step 1: Billing Information"
		else
			strPageTitle = "Step 2: Shipping Information"
		end if
		DrawHeaderUpdate
'		DrawCheckoutStatusBar intStep
		DrawPage_Step1or2_NewEdit intStep, intID
		
	case "step1newsubmit", "step2newsubmit": ' submit new billing form
		if intStep = 1 then
			strPageTitle = "Step 1: Billing Information"
		else
			strPageTitle = "Step 2: Shipping Information"
		end if
		AutoCheck Request, ""
		if FormErrors.count = 0 then
			' submit new billing form and proceed to step 2
			dim intBillID, intTaxZone
			intBillID = Request("id")
			if IsNumeric(intBillID) then
				intBillID = CLng(intBillID)
			else
				intBillID = 0
			end if
			if intStep = 2 then
				' since the only tax zone is California, USA, and since both state and country are controlled entries
				' (i.e. pulldowns, not free-type text fields), we can base the tax zone on the state and country rather
				' than using a separate pulldown in the form.
				'intTaxZone = Request("intTaxZone")
				intTaxZone = iif((ucase(Request("strCountry")) = "US") and (ucase(Request("strState")) = "CA"), 1, 0)
			else
				intTaxZone = 0
			end if
			'response.write "billid: " & intbillid
			if intBillID = 0 then
				intBillID = CreateNewBillRecord_Other(intUserID, intStep=2, Request("strLabel"), Request("strEmail"), Request("strFirstName"), Request("strLastName"), Request("strCompany"), Request("strAddress1"), Request("strAddress2"), Request("strCity"), Request("strState"), Request("strZip"), Request("strCountry"), Request("strDayPhone"), Request("strNightPhone"), Request("strFax"), intTaxZone)
			else
				intBillID = UpdateBillRecord_Other(intUserID, intStep=2, intBillID, Request("strLabel"), Request("strEmail"), Request("strFirstName"), Request("strLastName"), Request("strCompany"), Request("strAddress1"), Request("strAddress2"), Request("strCity"), Request("strState"), Request("strZip"), Request("strCountry"), Request("strDayPhone"), Request("strNightPhone"), Request("strFax"), intTaxZone)
			end if
			''------------------JRMCodeChange--12/12--------changed these to intorderID from intUserID
			SetOrderBillID_Other intOrderID, intStep=2, intBillID
			if intStep = 1 then
				CopyPaymentFromBillRecord_Other intOrderID, intBillID
			end if
			RecalcOrder_Other intOrderID
			response.redirect STR_DETAIL_SCRIPT
		else
			DrawHeaderUpdate
'			DrawCheckoutStatusBar intStep
			DrawPage_Step1or2_NewEdit intStep, 0
			
		end if
	case "step1submit", "step2submit": ' submit billing record selection form	
		if intStep = 1 then
			strPageTitle = "Step 1: Billing Information"
		else
			strPageTitle = "Step 2: Shipping Information"
		end if
		if Request("btnEdit") <> "" then
			' user chose to edit an existing record
			if Request("billid" & intStep) <> "" then
				response.redirect STR_EDIT_SCRIPT & "action=step" & intStep & "new&id=" & Request("billid" & intStep)
			end if
		elseif Request("billid" & intStep) = "new" then
			' user chose to create a new billing record
			response.redirect STR_EDIT_SCRIPT & "action=step" & intStep & "new&rr=" & Request("rr")
		elseif CheckIsBillID_Other(intUserID, intStep=2, Request("billid" & intStep)) then
			SetOrderBillID_Other intOrderID, intStep=2, Request("billid" & intStep)
			if intStep = 1 then
				CopyPaymentFromBillRecord_Other intOrderID, Request("billid" & intStep)
			end if
			RecalcOrder_Other intOrderID
			response.redirect STR_DETAIL_SCRIPT
		end if
		FormErrors.Add "billid" & intStep, "Please make a selection."
		set dctList = GetUserBillingList_Other(intUserID, intStep = 2)
		DrawHeaderUpdate
'		DrawCheckoutStatusBar intStep
		DrawPage_Step1or2_Choose intStep, dctList
		
	case "step3": ' STEP 3 - Shipping Options
		dim mnySubtotal, strShipCountry, intShipOption, strState, mnyShipPrice
		GetOrderShippingZone_Other intOrderID, mnySubtotal, strShipCountry, strState, intShipOption, mnyShipPrice
		DrawHeaderUpdate
		DrawPage_Step3 mnySubtotal, strShipCountry, strState, intShipOption, mnyShipPrice
		
	case "step3submit":
		intShipOption = Request("intShipOption")
		if IsNumeric(intShipOption) then
			intShipOption = CLng(intShipOption)
		else
			intShipOption = 0
		end if
		if intShipOption = 0 then
			FormErrors.Add "intShipOption", "Please select a shipping option:"
		end if
		if FormErrors.count = 0 then
			SetOrderShipOption_Other intOrderID, intShipOption, ""
			response.redirect STR_DETAIL_SCRIPT
		else
			' use a temp. variable for the call to GetOrderShippingZone -- we want to use the current Request("intShipOption")
			' vlaue for the call to DrawPage
			dim t
'			GetOrderShippingZone_Other intOrderID, mnySubtotal, strShipCountry, t
			GetOrderShippingZone_Other intOrderID, mnySubtotal, strShipCountry, strState, t, mnyShipPrice
			DrawHeaderUpdate
'			DrawCheckoutStatusBar 3
'			DrawPage_Step3 mnySubtotal, strShipCountry, intShipOption
			DrawPage_Step3 mnySubtotal, strShipCountry, strState, intShipOption, mnyShipPrice
			
		end if
	case "step4":
		dim strPaymentMethod, strCardName, strCardType, strCardNumber, strCardExtended, strCardExpMonth, strCardExpYear
		strPaymentMethod = GetOrderPaymentMethod_Other(intOrderID) & ""
		if strPaymentMethod = "OCC" then
			GetOrderPaymentInfo_OCC_Other intOrderID, strCardName, strCardType, strCardNumber, strCardExtended, strCardExpMonth, strCardExpYear
			if strCardNumber & "" = "" then
				' user never saved their info with their profile
				strPaymentMethod = ""
			elseif strCardName = "" or strCardType = "" or strCardNumber = "" or IsCardExpired(strCardExpMonth, strCardExpYear) then
				strPaymentMethod = ""
				FormErrors.Add "chrPaymentMethod", "The previously saved credit card is no longer valid."
			end if
		end if
		if strPaymentMethod = "OEC" then
			' not implemented
			strPaymentMethod = ""
		end if
		if strPaymentMethod = "" then
			DrawHeaderUpdate
'			DrawCheckoutStatusBar 4
			DrawPage_Step4Select
			
		else
			DrawHeaderUpdate
'			DrawCheckoutStatusBar 4
			DrawPage_Step4 strPaymentMethod, strCardName, strCardType, strCardNumber, strCardExtended, strCardExpMonth, strCardExpYear
			
		end if
	case "step4select":
		if Request("btnEdit") <> "" then
			DrawHeaderUpdate
'			DrawCheckoutStatusBar 4
			DrawPage_Step4Select
			
		else
			RecalcOrder_Other intOrderID
			response.redirect STR_DETAIL_SCRIPT
		end if
	case "step4submit":
		CheckField "chrPaymentMethod", "IsEmpty", Request, "Please select a payment method."
		if FormErrors.count = 0 then
			SetOrderPaymentMethod_Other intOrderID, Request("chrPaymentMethod")
			if right(Request("chrPaymentMethod"),2) = "CC" then
				response.redirect STR_EDIT_SCRIPT & "action=step4occ&chrpaymentmethod=" & Request("chrPaymentMethod")
			elseif Request("chrPaymentMethod") = "OEC" then
				response.redirect STR_EDIT_SCRIPT & "action=step4oec"
			else
				RecalcOrder_Other intOrderID
				response.redirect STR_DETAIL_SCRIPT
			end if
		else
			DrawHeaderUpdate
			DrawPage_Step4Select
			
		end if
	case "step4occ"
		DrawHeaderUpdate
'		DrawCheckoutStatusBar 4
		DrawPage_Step4_CreditCard
		
	case "step4occsubmit"
		AutoCheck Request, ""
		CheckField "chrCardExpMonth~chrCardExpYear", "CCExp", Request, "Invalid Expiration"
		if FormErrors.count = 0 then
			SetOrderPaymentInfo_OCC_Other intOrderID, GetOrderBillName_Other(intOrderID), Request("vchCardType"), Request("vchCardNumber"), Request("vchCardExtended"), Request("chrCardExpMonth"), Request("chrCardExpYear")
			if lcase(Request("strSaveProfile")) = "y" then
				UpdateBillRecordPayment_Other intUserID, "OCC", Request("vchCardType"), Request("vchCardNumber"), Request("vchCardExtended"), Request("chrCardExpMonth"), Request("chrCardExpYear")
			end if
			RecalcOrder_Other intOrderID
			response.redirect STR_DETAIL_SCRIPT
		else
			DrawHeaderUpdate
'			DrawCheckoutStatusBar 4
			DrawPage_Step4_CreditCard
			
		end if
	case "step5"
		DrawHeaderUpdate
	'	DrawCheckoutStatusBar 5
		DrawPage_Step5 GetOrderGiftMessage_Other(intOrderID)
		
	case "step5submit"
		SetOrderGiftMessage_Other intOrderID, Request("txtGiftMessage")
		RecalcOrder_Other intOrderID
		response.redirect STR_DETAIL_SCRIPT
	case "refmodify"
		
		DrawHeaderUpdate
		DrawPage_refModify GetOrderReferral_Other(intOrderID)
		
	case "refmodifysubmit"
		SetOrderReferral_Other intOrderID, Request("vchCustomReferralName")
		response.redirect STR_DETAIL_SCRIPT
	case else

		response.redirect STR_DETAIL_SCRIPT
end select
response.end

sub DrawPage_Step0(dctList)
	DrawFormHeader "100%", "Select Store", ""
%>
<FORM ACTION="<%= STR_EDIT_SCRIPT %>" METHOD="POST">
<%
	HiddenInput "action", "step0"
	HiddenInput "search", "yes"
	
	stlBeginStdTable "100%"
	response.write "<TR><TD>"
	
	stlBeginFormSection "100%", 2
	stlCaption "Enter the store number", 2
	stlTextInput "keywords", 32, 32, Request, "Keywords", "", ""
	stlEndFormSection
	stlSubmit "Search"
	
	response.write "</TD></TR>"
	stlEndStdTable
	response.write "</FORM>"
	
	if IsObject(dctList) then
%>
<FORM ACTION="<%= STR_EDIT_SCRIPT %>" METHOD="POST">
<%
		HiddenInput "action", "step0submit"
		HiddenInput "search", "yes"
		HiddenInput "keywords", Request("keywords") & ""
		
		stlBeginStdTable "100%"
		response.write "<TR><TD>"
		
		stlBeginFormSection "100%", 2
		stlArrayPulldownX "shopper", 8, "Select...", "", dctList, "Shopper Account", "IsEmpty", "Please make a selection.", "", 1
		stlEndFormSection
		
		stlSubmit "Continue -->"
		response.write "</TD></TR>"
		stlEndStdTable
		response.write "</FORM>"
	end if
end sub

sub DrawPage_Step1or2_NewEdit(intStep, intID)

%>
<FORM ACTION="<%= STR_EDIT_SCRIPT %>" METHOD="POST">
<%
	HiddenInput "action", "step" & intStep & "newsubmit"
	HiddenInput "iform", Request("iform") & ""
	HiddenInput "id", intID
	
	dim strFormMode, strLabel, strEmail, strFirstName, strLastName, strCompany, strAddress1, strAddress2, strCity, strState, strZip, strCountry, strDayPhone, strNightPhone, strFax, intTaxZone
	if intID = 0  then		
		' drawing a new form -- use values from acount or billing record
		dim strDummy
		if intStep = 1 then
			GetUserInfo strDummy, strFirstName, strLastName, strCompany, strEmail, strAddress1, strCity, strState, strZip, strCountry
			strAddress2 = ""
			strCountry = "US"
			strDayPhone = ""
			strNightPhone = ""
			strFax = ""
			strFormMode = ""
		else
			'response.write "iD: " & GetOrderBillID(false)
			GetOrderAddress true, GetOrderBillID(false), strLabel, strEmail, strFirstName, strLastName, strCompany, strAddress1, strAddress2, strCity, strState, strZip, strCountry, strDayPhone, strNightPhone, strFax, intTaxZone
			strFormMode = iif(ucase(strCountry)="US", "n", "y")
			'------------------JRMCodeChange---------12/11
			if strCountry = "" then
				strFormMode = "n"
			end if 
		end if
	elseif FormErrors.count > 0 then
		' either redrawing a form with errors -- use values from form data
		strLabel = Request("strLabel")
		strEmail = Request("strEmail")
		strFirstName = Request("strFirstName")
		strLastName = Request("strLastName")
		strCompany = Request("strCompany")
		strAddress1 = Request("strAddress1")
		strAddress2 = Request("strAddress2")
		strCity = Request("strCity")
		strState = Request("strState")
		strZip = Request("strZip")
		strCountry = Request("strCountry")
		strDayPhone = Request("strDayPhone")
		strNightPhone = Request("strNightPhone")
		strFax = Request("strFax")
		strFormMode = lcase(Request("iform"))
		intTaxZone = Request("intTaxZone")
	else
		' load values from record
		GetOrderAddress (intStep=2), intID, strLabel, strEmail, strFirstName, strLastName, strCompany, strAddress1, strAddress2, strCity, strState, strZip, strCountry, strDayPhone, strNightPhone, strFax, intTaxZone
		' to determine the appropriate form to use (domestic vs. international), check the country field ONLY IF
		' Request("iform") doesn't specify otherwise (if the user is editing an existing record, but clicks on the link
		' to switch forms, the country field in the data will still have the old value while the "iform" specifies the correct
		' form to use)
		strFormMode = Request("iform")
		if strFormMode = "" then
			' select form based on country field
			strFormMode = iif(ucase(strCountry)="US", "n", "y")			
		end if
	end if
	
	dim strPageLabel
	strPageLabel = iif(intStep = 1, "Billing", "Shipping")
	'DrawFormHeader "100%", iif(intID=0, "New ", "Edit ") & strPageLabel & " Information", ""
	if Request("iform") <> "" then
		strFormMode = Request("iform")
	end if
	
	if strFormMode <> "y" then
		%>
		<B><A HREF="<%= STR_EDIT_SCRIPT %>action=step<%= intStep %>new&iform=y&id=<%= intID %>"><IMG SRC="<%= imagebase %>btn_arrow.gif" WIDTH="14" HEIGHT="13" BORDER="0" ALIGN="ABSMIDDLE" HSPACE="4" VSPACE="4" ALT="">International Form</A></B><BR>
		<%
	else
		%>
		<B><A HREF="<%= STR_EDIT_SCRIPT %>action=step<%= intStep %>new&iform=n&id=<%= intID %>"><IMG SRC="<%= imagebase %>btn_arrow.gif" WIDTH="14" HEIGHT="13" BORDER="0" ALIGN="ABSMIDDLE" HSPACE="4" VSPACE="4" ALT="">US Domestic Form</A></B><BR>
		<%
	end if
	
	response.write "<BR>"

	
	stlBeginStdTable "100%"
	response.write "<TR><TD>"

	if strFormMode <> "y" then

		' domestic address form
		HiddenInput "strCountry", "US"
		
		stlBeginFormSection "100%", 2
		'stlCaption "For international addresses, please use our <A HREF=""checkout.asp?action=step" & intStep & "new&iform=y&id=" & intID & """>International Form</A>.", 2
		'stlCaption "You may save additional billing and shipping profiles with your customer account (e.g. multiple credit cards, company cards, home address, work address, etc.). Please provide a label that will help you remember which address this is. This label is for your online reference only, and will not appear on any invoices or receipts.", 2

		stlCaption "You may save multiple billing and shipping addresses.  For your own reference, please choose a label for this address (ex: ""Home"", ""Office"").",2
		stlTextInput "strLabel", 32, 32, strLabel, "Label", "IsEmpty", "Please provide a label for this address."

		'stlTableRule 2

		stlCaption "A copy of the invoice and receipt will be sent to the email address you provide here.", 2
		if intStep = 2 then
			stlCaption "If you do not wish a copy to be sent to this address, leave this blank.", 2		' (if this package is a gift, for example)
		end if
		stlTextInput "strEmail", 32, 50, strEmail, "Email Address", iif(intStep=1, "IsEmpty", ""), "Please provide the account holder's email address."

		if intStep = 1 then
			stlCaption "Please provide the name and address as it appears on the credit card or bank account statement.", 2
		else
			stlCaption "Please provide the name and address of the person who will be receiving this order.", 2
		end if
		stlTextInput "strFirstName", 32, 32, strFirstName, "First Name", "IsEmpty", iif(intStep=1, "Please provide the account holder's first name.", "Please provide the recipient's first name.")
		stlTextInput "strLastName", 32, 32, strLastName, "Last Name", "IsEmpty", iif(intStep=1, "Please provide the account holder's last name.", "Please provide the recipient's last name.")
		stlTextInput "strCompany", 32, 50, strCompany, "Company", "", ""
		stlTextInput "strAddress1", 32, 50, strAddress1, "Address Line 1", "IsEmpty", iif(intStep=1, "Please provide the account holder's mailing address.", "Please provide the recipient's mailing address.")
		stlTextInput "strAddress2", 32, 50, strAddress2, "Address Line 2", "", ""
		stlTextInput "strCity", 32, 32, strCity, "City", "IsEmpty", iif(intStep=1, "Please provide the account holder's city.", "Please provide the recipient's city.")
		stlArrayPulldown "strState", "Select...", strState, dctUSStateCodes, "State", "IsEmpty", iif(intStep=1, "Please provide the account holder's state.", "Please provide the recipient's state.")
		stlTextInput "strZip", 12, 12, strZip, "Zip", "IsEmpty", iif(intStep=1, "Please provide the account holder's zip-code.", "Please provide the recipient's zip-code.")
		stlTextInput "strDayPhone", 20, 20, strDayPhone, "Daytime Phone", "IsEmpty", iif(intStep=1, "Please provide a daytime telephone number.", "Please provide a daytime telephone number.")
		stlTextInput "strNightPhone", 20, 20, strNightPhone, "Evening Phone", "", ""
		stlTextInput "strFax", 20, 20, strFax, "Fax Number", "", ""
		'if intStep = 2 then
		'	stlArrayPulldown "intTaxZone", "Select...", intTaxZone, GetTaxZoneList(), "Order Being Shipped To", "IsEmpty", "Please identify where this order is being shipped to."
		'end if
		
		stlEndFormSection
	else
		' International Address Form
		HiddenInput "intTaxZone", 0
		stlBeginFormSection "100%", 2
		stlCaption "You may save additional billing and shipping profiles with your customer account (e.g. multiple credit cards, company cards, home address, work address, etc.). Please provide a label that will help you remember which address this is. This label is for your online reference only, and will not appear on any invoices or receipts.", 2
		stlTextInput "strLabel", 32, 32, strLabel, "Label", "IsEmpty", "Please provide a label for this address."
		stlCaption "A copy of the invoice and receipt will be sent to the email address you provide here.", 2
		if intStep = 2 then
			stlCaption "If you do not wish a copy to be sent to this address, leave this blank.", 2		' (if this package is a surprise gift, for example)
		end if
		stlTextInput "strEmail", 32, 50, strEmail, "Email Address", iif(intStep=1, "IsEmpty", ""), "Please provide the account holder's email address."
		if intStep = 1 then
			stlCaption "Please provide the name and address as it appears on the credit card or bank account statement.", 2
		else
			stlCaption "Please provide the name and address of the person who will be receiving this order.", 2
		end if
		stlTextInput "strFirstName", 32, 32, strFirstName, "First Name", "IsEmpty", "Please provide the account holder's first name."
		stlTextInput "strLastName", 32, 32, strLastName, "Last Name", "IsEmpty", "Please provide the account holder's last name."
		stlTextInput "strCompany", 32, 50, strCompany, "Company", "", ""
		stlTextInput "strAddress1", 32, 50, strAddress1, "Address Line 1", "IsEmpty", "Please provide the account holder's mailing address."
		stlTextInput "strAddress2", 32, 50, strAddress2, "Address Line 2", "", ""
		stlTextInput "strCity", 32, 32, strCity, "City", "IsEmpty", "Please provide the account holder's city."
		stlTextInput "strState", 32, 32, strState, "State", "IsEmpty", "Please provide the account holder's state."
		stlTextInput "strZip", 12, 12, strZip, "Zip", "IsEmpty", "Please provide the account holder's zip-code."
		stlArrayPulldown "strCountry", "Select...", strCountry, dctCountryCodes, "Country", "IsEmpty", "Please provide the account holder's country."
		stlTextInput "strDayPhone", 20, 20, strDayPhone, "Daytime Phone", "IsEmpty", "Please provide a daytime telephone number."
		stlTextInput "strNightPhone", 20, 20, strNightPhone, "Evening Phone", "", ""
		stlTextInput "strFax", 20, 20, strFax, "Fax Number", "", ""
		
		stlEndFormSection
	end if
	response.write "</TD></TR>"
	stlSubmit "Continue to Step " & (intStep + 1) & " -->"
	stlEndStdTable
	response.write "</FORM>"
end sub

sub DrawPage_Step1or2_Choose(intStep, dctList)
	dctList.Add "new", "Create A New " & iif(intStep=1, "Billing", "Shipping") & " Record..."
%>
<FORM ACTION="<%= STR_EDIT_SCRIPT %>" METHOD="POST">
<%
	HiddenInput "action", "step" & intStep & "submit"
	
	DrawFormHeader "100%", "Select " & iif(intStep=1, "Billing", "Shipping") & " Info", ""
	
	stlBeginStdTable "100%"
	response.write "<TR><TD>"
	
	dim strThisValue
	if FormErrors.count > 0 then
		' redisplay user's previous selection
		strThisValue = Request("billid" & intStep)
	else
		strThisValue = GetOrderBillID(intStep = 2)
	end if
	
	stlBeginFormSection "100%", 3
	stlCaption "You may select one of the following " & iif(intStep=1, "Billing", "Shipping") & " records from your profile or choose to create a new one.", 3
	stlArrayPulldown "billid" & intStep, "Select...", strThisValue, dctList, iif(intStep=1, "Billing", "Shipping") & " Record", "IsEmpty", "Please make a selection."
	stlRawCell "<INPUT TYPE=""submit"" CLASS=""button"" NAME=""btnEdit"" VALUE=""Modify..."">"
	stlEndFormSection
	
	response.write "</TD></TR>"
	stlSubmit "Continue to Step " & (intStep + 1) & " -->"
	stlEndStdTable
	response.write "</FORM>"
end sub

sub DrawPage_Step3(mnySubtotal, strShipCountry, strShipState, intShipOption, mnyShipPrice)
	dim aryShipBaseMatrix, aryShipUPS_Matrix, aryShipOption, mnyBaseShipping
	GetShipPriceMatrix intShipOption, mnySubTotal, aryShipBaseMatrix, aryShipUPS_Matrix, aryShipOption, mnyBaseShipping
	
	if IsNumeric(intShipOption) then
		intShipOption = CLng(intShipOption)
	else
		intShipOption = 1
	end if
'	' verify which shipping options are available based on delivery address
'	if strShipCountry <> "US" and strShipCountry <> "CA" then
'		' international must choose option 5
'		aryShipOption(0,0) = -1
'		aryShipOption(1,0) = -1
'		aryShipOption(2,0) = -1
'		aryShipOption(3,0) = -1
'	elseif (strShipCountry = "US" and (strShipState = "HI" OR strShipState = "AK" OR strShipState = "VI")) then
'		' Hawaii, Alaska, Virgin Islands must choose option 4
'		aryShipOption(0,0) = -1
'		aryShipOption(2,0) = -1
'		aryShipOption(3,0) = -1
'		aryShipOption(4,0) = -1
'	else
'		' Non-international can not choose option 3
'		aryShipOption(4,0) = -1
'	end if
	dim strFormMode
	strFormMode = ""
	if Request("iform") <> "" then
		strFormMode = Request("iform")
	else
		if strShipCountry <> "" then
			strFormMode = iif((ucase(strShipCountry)="US"), "n", "y")
		else
			strFormMode = "n"
		end if
	end if

'	 new method - test for international shipping
	dim maxCnt
	maxCnt = ubound(aryShipOption) -3
	if strFormMode = "y" then
		for c = 1 to maxCnt
			aryShipOption(x,0) = -1
		next
	else
		aryShipOption(maxCnt+1,0) = -1
	end if

'	 old method for international shipping
'	aryShipOption(4,0) = -1
'	if strFormMode = "y" then
'	'	aryShipOption(0,0) = -1
'		aryShipOption(1,0) = -1
'		aryShipOption(2,0) = -1
'		aryShipOption(3,0) = -1
'	else
'		aryShipOption(4,0) = -1
'	end if
	
	' verify which shipping options are available based on special product requirements
	dim c, x, strSQL, rsLineItem, blnSpecialItems, blnOptionValid, intFirstOption
	blnSpecialItems = false
	x = ubound(aryShipOption)
	strSQL = "SELECT * FROM " & STR_TABLE_LINEITEM & " WHERE intOrderID=" & intOrderID & " AND chrStatus<>'D'"
	set rsLineItem = gobjConn.execute(strSQL)
	while not rsLineItem.eof
		if not IsNull(rsLineItem("intForceShipMethod")) then
			blnSpecialItems = true
			for c = 0 to x
				if aryShipOption(c,0) <> rsLineItem("intForceShipMethod") then
					aryShipOption(c,0) = -1
				end if
			next
		end if
		rsLineItem.MoveNext
	wend
	rsLineItem.close
	set rsLineItem = nothing
	
	' verify that there is an available option, and that the currently selected option is available
	blnOptionValid = false
	intFirstOption = -1
	for c = 0 to x
		if aryShipOption(c,0) <> -1 then
			if intFirstOption = -1 then
				intFirstOption = aryShipOption(c,0)
			end if
			if aryShipOption(c,0) = intShipOption then
				blnOptionValid = true
			end if
		end if
	next
	
	if blnOptionValid = false and intFirstOption <> -1 then
		intShipOption = intFirstOption
	end if
	
	if intFirstOption = -1 then
		' there are no available shipping options
		response.write "<FORM ACTION=""" & STR_DETAIL_SCRIPT & """ METHOD=""POST"">"
		
		stlBeginStdTable "100%"
		response.write "<TR><TD>"
		
		response.write font(1) & "<B>Problem with The Order</B><BR><BR>"
		response.write "Unfortunately, the items that have been ordered require special shipping methods that are not available based on the shipping address.<BR><BR>"
		response.write "&nbsp;"
		response.write "</TD></TR>"
		stlSubmit "Return To Order Details -->"
		stlEndStdTable
		response.write "</FORM>"
	else
		response.write "<FORM ACTION=""" & STR_EDIT_SCRIPT & """ METHOD=""POST"">"
		HiddenInput "action", "step3submit"
		HiddenInput "rr", Request("rr") & ""
		
		stlBeginStdTable "100%"
		response.write "<TR><TD>"
		
		stlBeginFormSection "350", 3
		stlCaption "Please select one of the following shipping options:", 3
		x = ubound(aryShipOption)
		
		for c = 0 to x
			if aryShipOption(c,0) <> -1 and aryShipOption(c,1) <> "" then
				stlRadioButton "intShipOption", aryShipOption(c,0), intShipOption, "-", ""
				stlRawCellX aryShipOption(c,1), "", "", "top", 1
				stlRawCellX FormatCurrency(aryShipOption(c,2),2), "", "right", "top", 1
			else
				' uncomments the next three lines to display unavailable shipping options as greyed out options
				'stlRawCell ""
				'stlRawCellX "<FONT COLOR=""#999999"">" & aryShipOption(c,1) & "</FONT>", "", "", "top", 1
				'stlRawCellX "<FONT COLOR=""#999999"">" & FormatCurrency(aryShipOption(c,2),2) & "</FONT>", "", "right", "top", 1
			end if
		next
		
		stlEndFormSection
		
		if blnSpecialItems then
			response.write font(1) & "<B>Due to the nature of the items in your order, your order must be shipped by one of the methods shown above.</B></FONT><BR><BR>"
		end if
		
		response.write font(1) & "No Orders will be shipped on Fridays unless they require Saturday Delivery, at an additional cost. We can only ship to homes and business addresses. We cannot ship to APO, FPO, or P.O. Boxes. We will ship most orders overseas. Clients are asked to please email us their request, including destination address so that we can give them the total price including shipping. Inquiries should be sent to <A HREF=""mailto:info@thecampaignstore.com"">info@thecampaignstore.com</A> or call us at " & STR_MERCHANT_PHONE & " or fax at " & STR_MERCHANT_FAX & ".<BR><BR></FONT>"
		
		response.write "</TD></TR>"
		stlSubmit "Continue to Step 4 -->"
		stlEndStdTable
		response.write "</FORM>"
	end if
end sub

sub DrawPage_Step4(strPaymentMethod, strCardName, strCardType, strCardNumber, strCardExtended, strCardExpMonth, strCardExpYear)
%>
<FORM ACTION="<%= STR_EDIT_SCRIPT %>" METHOD="POST">
<%
	HiddenInput "action", "step4select"
	
	DrawFormHeader "100%", "Credit Card Information", ""
	
	stlBeginStdTable "100%"
	response.write "<TR><TD>"

	stlBeginFormSection "100%", 2
	stlStaticText "Payment Method:", GetArrayValue(strPaymentMethod, dctPaymentMethod)
	if strPaymentMethod = "OCC" then
		stlStaticText "Card Type:", strCardType
		stlStaticText "Card Number:", GetProtectedCardNumberX(strCardNumber)
		stlStaticText "Expiration:", strCardExpMonth & "/" & strCardExpYear
	elseif strPaymentMethod = "OEC" then
		stlStaticText "n/a", "n/a"
	end if
	stlRawCell ""
	stlRawCell "<INPUT TYPE=""submit"" CLASS=""button"" NAME=""btnEdit"" VALUE=""Change Payment..."">"
	stlEndFormSection
	
	response.write "</TD></TR>"
	stlSubmit "Continue to Step 5 -->"
	stlEndStdTable
	response.write "</FORM>"
end sub

sub DrawPage_Step4Select()
	dim strPaymentMethod
	if FormErrors.count > 0 then
		strPaymentMethod = Request("chrPaymentMethod")
	else
		strPaymentMethod = GetOrderPaymentMethod()
	end if
%>
<FORM ACTION="<%= STR_EDIT_SCRIPT %>" METHOD="POST">
<%
	HiddenInput "action", "step4submit"
	HiddenInput "rr", Request("rr") & ""
	
	DrawFormHeader "100%", "Select Payment Method", ""
	
	stlBeginStdTable "100%"
	response.write "<TR><TD>" & font(1)
	
	stlBeginFormSection "100%", 2
	stlArrayPulldown "chrPaymentMethod", "Select...", strPaymentMethod, dctPaymentMethod, "Payment Method", "IsEmpty", "Please make a selection."
	stlEndFormSection
%>
You have several payment options available:
<UL>
<LI><B>EXPRESS METHOD</B> - pay online by credit card or by direct debit - nothing to send by mail! (Please have your credit card or checkbook available.)<BR>
<LI><B>By Phone or Fax</B> - submit your order online, and then call a customer service representative and provide your credit card or direct debit information over the phone.
<LI><B>By Mail</B> - submit your order online, and then mail your payment to the address provided below:
</UL>
<CENTER><B><%= STR_MERCHANT_NAME %></B><BR>
<%= STR_MERCHANT_ADDRESS1 & iif(STR_MERCHANT_ADDRESS2 <> "", ", " & STR_MERCHANT_ADDRESS2, "") %><BR>
<%= STR_MERCHANT_CITY & ", " & STR_MERCHANT_STATE & " " & STR_MERCHANT_ZIP %><BR>
<%= "Phone " & STR_MERCHANT_PHONE & "&nbsp;&nbsp;Fax " & STR_MERCHANT_FAX %></CENTER>
<BR>

<CENTER><B>Your order can not be processed until payment is received.</B></CENTER>
<%	
	response.write "</FONT></TD></TR>"
	stlSubmit "Continue with Step 4 -->"
	stlEndStdTable
	response.write "</FORM>"
end sub

sub DrawPage_Step4_CreditCard()
	dim dctCardExpMonthValues, dctCardExpYearValues, c
	set dctCardExpMonthValues = Server.CreateObject("Scripting.Dictionary")
	for c = 1 to 9
		dctCardExpMonthValues.Add "0" & c, "0" & c
	next
	dctCardExpMonthValues.Add "10", "10"
	dctCardExpMonthValues.Add "11", "11"
	dctCardExpMonthValues.Add "12", "12"
	set dctCardExpYearValues = Server.CreateObject("Scripting.Dictionary")
	for c = Year(Date()) to Year(Date()) + 10
		dctCardExpYearValues.Add c & "", c & ""
	next
	
	dim strCardType, strCardNumber, strCardExtended, strCardExpMonth, strCardExpYear, strSaveProfile
	if FormErrors.count > 0 then
		strCardType = Request("vchCardType")
		strCardNumber = Request("vchCardNumber")
		strCardExtended = Request("vchCardExtended")
		strCardExpMonth = Request("vchCardExpMonth")
		strCardExpYear = Request("vchCardExpYear")
		strSaveProfile = lcase(Request("strSaveProfile"))
	else
		strCardType = ""
		strCardNumber = ""
		strCardExtended = ""
		strCardExpMonth = ""
		strCardExpYear = ""
		strSaveProfile = "y"
	end if
%>
<FORM ACTION="<%= STR_EDIT_SCRIPT %>" METHOD="POST">
<%
	HiddenInput "action", "step4occsubmit"
	
	DrawFormHeader "100%", "Credit Card Information", ""
	
	stlBeginStdTable "100%"
	response.write "<TR><TD>"

	stlBeginFormSection "100%", 3
	stlArrayPulldownX "vchCardType", 1, "Select...", strCardType, dctCreditCardTypeValues, "Type of Card", "IsEmpty", "Please make a selection.", "", 2
	stlTextInputX "vchCardNumber", 32, 32, strCardNumber, "Card Number", "IsEmpty", "Please enter the credit card number.", "", 2
	stlTextInputX "vchCardExtended", 8, 8, strCardExtended, "Card PIN", "OptIsNumeric", "Please enter the credit card PIN.", "", 2
	stlArrayPulldownX "chrCardExpMonth", 1, "", strCardExpMonth, dctCardExpMonthValues, "Expiration", "", "", "", 1
	stlArrayPulldownX "chrCardExpYear", 1, "", strCardExpYear, dctCardExpYearValues, "-", "", "", "", 1
	if Request("strPaymentMethod") = "OCC" then
		stlCheckbox "strSaveProfile", "y", strSaveProfile, "", "Save This With Billing Profile"
	end if
	stlEndFormSection
	
	response.write "</TD></TR>"
	stlSubmit "Continue to Step 5 -->"
	stlEndStdTable
	response.write "</FORM>"
end sub

sub DrawPage_Step5(strGiftMessage)
	response.write "<FORM ACTION=""" & STR_EDIT_SCRIPT & """ METHOD=""POST"">"
	HiddenInput "action", "step5submit"
	HiddenInput "rr", Request("rr") & ""
	
	stlBeginStdTable "90%"
'	response.write "<TR><TD>"
	
'	stlBeginFormSection "100%", 2
	stlCaption "You may enter a message to be included with your order - FOR FREE!", 2
'	
	stlTextArea "txtGiftMessage", 10, 50, strGiftMessage & "", "Comments:"
'	
'	stlEndFormSection
	
'	response.write "</TD></TR>"
	stlSubmit "Continue to Step 6 -->"
	stlEndStdTable
	response.write "</FORM>"
end sub

sub DrawPage_refModify(strReferral)
	response.write "<FORM ACTION=""" & STR_EDIT_SCRIPT & """ METHOD=""POST"">"
	HiddenInput "action", "refmodifysubmit"
	HiddenInput "rr", Request("rr") & ""
	
	stlBeginStdTable "90%"
'	response.write "<TR><TD>"
	
'	stlBeginFormSection "100%", 2
	'stlCaption "Enter Referral:", 2
'	
	stlTextArea "vchCustomReferralName", 2, 50, strReferral & "", "Enter Referral:"
'	
'	stlEndFormSection
	
'	response.write "</TD></TR>"
	stlSubmit "Back to Order Details -->"
	stlEndStdTable
	response.write "</FORM>"
end sub

%>