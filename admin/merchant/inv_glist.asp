<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: inv_list.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Inventory Admin
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_INVENTORY_VIEW) then
	response.redirect "main.asp"
end if

dim blnCanEdit
blnCanEdit = CheckUserAccess(ACC_INVENTORY_EDIT)

dim strPageType, strPageTitle
strPageType = "admininvlist"
strPageTitle = "Inventory Categories"

const STR_LIST_SCRIPT = "inv_glist.asp?"
const STR_DETAIL_BRAND_SCRIPT = "inv_gdetail.asp?"
const STR_STOCK_LOW_SCRIPT = "inv_stock.asp?"
const STR_STOCK_HIGH_SCRIPT = "inv_stock.asp?"

OpenConn
dim strSQL, rsData

' gather inventory statistics
dim intLowCount, intHighCount, intInvCount
strSQL = "SELECT "
strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_INVENTORY & " WHERE chrStatus<>'D' AND chrType='I') AS intCount,"
strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_INVENTORY & " WHERE chrStatus<>'D' AND chrType='I' AND intStock < intLowStock) AS intLowCount,"
strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_INVENTORY & " WHERE chrStatus<>'D' AND chrType='I' AND intStock > intHighStock) AS intHighCount"
set rsData = gobjConn.execute(strSQL)
intInvCount = rsData("intCount")
intLowCount = rsData("intLowCount")
intHighCount = rsData("intHighCount")
rsData.close
set rsData = nothing

' get folder list
dim dctNavList, intParentID

intParentID = 0

' SWSCodeChange - 04aug2000 added chrParentStatus
' , chrParentStatus - currently is not supported in thecampaignstore
strSQL = "SELECT intID, chrType, chrStatus, intSortOrder, vchPartNumber, vchItemName, mnyItemPrice, mnyShipPrice, intStock"
strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY
strSQL = strSQL & " WHERE chrStatus<>'D' AND chrType='G'"' AND intParentID=" & intParentID
strSQL = strSQL & " ORDER BY vchItemName"
set rsData = gobjConn.execute(strSQL)


DrawPage


rsData.close
set rsData = nothing
response.end

sub DrawPage
	dim c, strPrevType, blnCanMoveUp, blnCanMoveDown
	dim intID, strType, strStatus, strPartNumber, strName, mnyPrice, mnyShipPrice, intStock
	' SWSCodeChange - 04aug2000 added strParentStatus
	dim strParentStatus
	
	call DrawHeaderUpdate()
	
	DrawListTitle
	DrawListHeader
	strPrevType = ""
	if rsData.eof then
		%>
		<tr>
		<td></td>
		<td class="font1" colspan=6>&nbsp;<BR />
		<span class="font2" ><I>There are no categories currently in inventory system.</I></span><BR />&nbsp;
		</td>
		</tr>
		<%
	else
		while not rsData.eof
			intID = rsData("intID")
			strType = rsData("chrType")
			strStatus = rsData("chrStatus")
			strName = rsData("vchItemName")
			rsData.MoveNext
			' SWSCodeChange - 04aug2000 added parent status
			DrawListItem intID, strType, strStatus, strParentStatus, strName, strPartNumber, mnyPrice, mnyShipPrice, intStock, blnCanMoveUp, blnCanMoveDown
			if not rsData.eof then
				DrawListItemSep
			end if
		wend
	end if
	DrawListFooter
end sub

sub DrawListTitle
%>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
	<td class="font5"><%= strPageTitle %></td>
<% if not blnPrintMode then %>
	<td class="font1" align=right>
	<A HREF="<%= STR_DETAIL_BRAND_SCRIPT %>">New Category<img src="<%= imagebase %>icon_newfolder.gif" width="21" height="16" alt="" border="0" align="absmiddle" /></A>
	</td>
<% end if %>
</tr>
</table>
<%= spacer(1,5) %><BR>

	<% stlBeginInfoBar INT_PAGE_WIDTH %>
	<B>Total Inventory:</B> <%= intInvCount %>&nbsp;
	<B><A HREF="<%= STR_STOCK_LOW_SCRIPT %>">Low Stock:</A></B> <%= intLowCount %>&nbsp;
	<B><A HREF="<%= STR_STOCK_HIGH_SCRIPT %>">High Stock:</A></B> <%= intHighCount %>&nbsp;
	</FONT></td>
	<td align=right><%= fontx(1,1,cWhite) & FormatDateTime(date,1) %>
	<% stlEndInfoBar %>
<%
end sub

sub DrawListHeader
%>
<table border="0" cellpadding="1" cellspacing="0" width="100%" bgcolor="<%= iif(blnPrintMode, cBlack, cVLtGrey) %>">
<tr>
	<td><table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="<%= cWhite %>">
	<tr>
		<td><table border="0" cellpadding="2" cellspacing="0" width="100%">
		<tr class="font1" bgcolor="<%= cVLtYellow %>">
			<td NOWRAP><B>Name</B></td>
			<td NOWRAP align="right"><B>Status</B></td>
		</tr>
<%
end sub

' SWSCodeChange - 04aug2000 added strParentStatus
sub DrawListItem(intID, strType, strStatus, strParentStatus, strName, strPartNumber, mnyPrice, mnyShipPrice, intQty, blnCanMoveUp, blnCanMoveDown)
	dim strActionLabel
	strActionLabel = iif(blnCanEdit, "Edit", "View")
%>
		<tr valign=BOTTOM>
			<td class="font2"><A HREF="<%= STR_DETAIL_BRAND_SCRIPT %>id=<%= intID %>"><%= strName %></A></td>
			<% ' SWSCodeChange - 04aug2000 added parent status %>
			<td class="font1" NOWRAP align="right"><%= GetStatusStr(strStatus, true) %> (<%= strParentStatus %>)</td>
		</tr>
<%	if not blnPrintMode then %>
		<tr>
			<td ><%= fontx(1,1,cLight) %><% if blnCanEdit then %><A HREF="<%= STR_DETAIL_BRAND_SCRIPT %>id=<%= intID %>"><%= strActionLabel %></A><% end if %></td>
		</tr>
<%
	end if
end sub

sub DrawListItemSep
%>
		<tr>
			<td colspan=7><table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="<%= iif(blnPrintMode, cBlack, cVVLtGrey) %>"><tr><td><%= spacer(1,1) %></td></tr></table></td>
		</tr>
<%
end sub

sub DrawListFooter
%>
		</table></td>
	</tr>
	</table></td>
</tr>
</table>
<%
end sub
%>