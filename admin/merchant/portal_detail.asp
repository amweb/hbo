<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: portal_detail.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Inventory admin - details on folders
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_INVENTORY_VIEW) then
	response.redirect "main.asp"
end if

dim blnCanEdit
blnCanEdit = CheckUserAccess(ACC_INVENTORY_EDIT)

const STR_PAGE_TITLE = "Portal Details"
const STR_PAGE_TYPE = "admininvlist"

OpenConn

dim dctInvList
dim strSQL, objUpload, rsRequestData
Set objUpload = Server.CreateObject("SoftArtisans.FileUp")
Set rsRequestData = objUpload.Form

dim intID, intParentID, strAction
strAction = RequestPoll("action")
intID = RequestPoll("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if

intParentID = RequestPoll("parentid")
if IsNumeric(intParentID) then
	intParentID = CLng(intParentID)
else
	intParentID = 0
end if

if strAction = "delete" then
	if blnCanEdit then
		gobjConn.execute("UPDATE " & STR_TABLE_STOREFRONT & " SET chrStatus='D' WHERE intID=" & intID)
	end if
	response.redirect "portal_list.asp?parentid=" & intParentID
elseif strAction = "setstatus" then
	if blnCanEdit then
		gobjConn.execute("UPDATE " & STR_TABLE_STOREFRONT & " SET chrStatus='" & RequestPoll("status") & "' WHERE intID=" & intID)
	end if
	response.redirect "portal_list.asp?parentid=" & intParentID
elseif strAction = "submit" then
	if blnCanEdit then
		AutoCheck rsRequestData, ""
		if RequestPoll("chrSoftFlag") = "Y" then
			CheckField "vchSoftURL", "IsEmpty", Request, "Missing Download URL"
		end if
		if FormErrors.count = 0 then
			
			dim dctSaveList
			set dctSaveList = Server.CreateObject("Scripting.Dictionary")
				dctSaveList.Add "*@dtmCreated", "GETDATE()"
				dctSaveList.Add "@dtmUpdated", "GETDATE()"
				dctSaveList.Add "*vchCreatedByUser", Session(SESSION_MERCHANT_ULOGIN)
				dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
				dctSaveList.Add "*vchCreatedByIP", gstrUserIP
				dctSaveList.Add "vchUpdatedByIP", gstrUserIP
				dctSaveList.Add "*chrStatus", "A"
				dctSaveList.Add "!vchItemName", ""
				dctSaveList.Add "!vchURL", ""
				dctSaveList.Add "!intInvID", ""
				
			SaveDataRecord STR_TABLE_STOREFRONT, rsRequestData, intID, dctSaveList
			
			dim fImage, uploadPath, IsImageGood, fImageOrient
			IsImageGood = false
			fImageOrient = 0
			If IsObject(objUpload.Form("vchHeaderImg")) then
				
				if (objUpload.Form("vchHeaderImg").TotalBytes <> 0) and (lcase(right(objUpload.Form("vchHeaderImg").UserFilename, 4))=".jpg" or lcase(right(objUpload.Form("vchHeaderImg").UserFilename, 4))=".gif") then
					fImage = "" & intID & "." & Mid(objUpload.form("vchHeaderImg").UserFilename, InstrRev(objUpload.form("vchHeaderImg").UserFilename, "\") + 1)
					uploadPath = Server.MapPath(storefront) & "\"
					objUpload.Form("vchHeaderImg").SaveAs uploadPath & fImage
					if lcase(right(objUpload.Form("vchHeaderImg").UserFilename, 4))=".gif" then 
						IsImageGood = true	' - GIF Files don't work with JDO
					else
						IsImageGood = true'SetImageReSize( uploadPath, fImage, fImageOrient)	' true
					end if
				end if
			end if
			
			if IsImageGood then
				dctSaveList.RemoveAll
				dctSaveList.Add "@dtmUpdated", "GETDATE()"
				dctSaveList.Add "vchHeaderImg", fImage
				SaveDataRecord STR_TABLE_STOREFRONT, rsRequestData, intID, dctSaveList
			end if
			IsImageGood = false
			fImageOrient = 0
			fImage = ""
			If IsObject(objUpload.Form("vchFooterImg")) then
				
				
				if (objUpload.Form("vchFooterImg").TotalBytes <> 0) and (lcase(right(objUpload.Form("vchFooterImg").UserFilename, 4))=".jpg" or lcase(right(objUpload.Form("vchFooterImg").UserFilename, 4))=".gif") then
					fImage = "" & intID & "." & Mid(objUpload.form("vchFooterImg").UserFilename, InstrRev(objUpload.form("vchFooterImg").UserFilename, "\") + 1)
					uploadPath = Server.MapPath(storefront) & "\"
					objUpload.Form("vchFooterImg").SaveAs uploadPath & fImage
					if lcase(right(objUpload.Form("vchHeaderImg").UserFilename, 4))=".gif" then 
						IsImageGood = true	' - GIF Files don't work with JDO
					else
						IsImageGood = true 'SetImageReSize( uploadPath, fImage, fImageOrient)	' true
					end if
				end if
			end if
			if IsImageGood then
				dctSaveList.RemoveAll
				dctSaveList.Add "@dtmUpdated", "GETDATE()"
				dctSaveList.Add "vchFooterImg", fImage
				SaveDataRecord STR_TABLE_STOREFRONT, rsRequestData, intID, dctSaveList
			end if
			
			response.redirect "portal_list.asp?parentid=" & intParentID
		else
			
			DrawPage rsRequestData
			
		end if
	else
		response.redirect "portal_list.asp?parentid=" & intParentID
	end if
else
	dim rsData, blnDataIsRS
	if intID > 0 then
		strSQL = "SELECT * FROM " & STR_TABLE_STOREFRONT & " WHERE intID=" & intID & " AND chrStatus<>'D'"
		set rsData = gobjConn.execute(strSQL)
		blnDataIsRS = true
		if rsData.eof then
			response.redirect "portal_list.asp"
		end if
	else
		set rsData = rsRequestData
		blnDataIsRS = false
	end if
	
	
	DrawPage rsData
	
	
	if blnDataIsRS then
		rsData.close
		set rsData = nothing
	end if
end if
response.end

sub DrawPage(rsInput)
	dim strTitle, strInfoText
	if intID > 0 then
		strTitle = "Edit Folder - " & rsInput("vchItemName")
	else
		strTitle = "Add New Folder"
	end if
%>
<FORM ACTION="portal_detail.asp" METHOD="POST" NAME="frm" ENCTYPE="multipart/form-data">
<INPUT TYPE="hidden" NAME="action" VALUE="submit">
<INPUT TYPE="hidden" NAME="id" VALUE="<%= intID %>">
<INPUT TYPE="hidden" NAME="parentid" VALUE="<%= intParentID %>">
<%
	if blnCanEdit then
		AddPageAction "Cancel", "portal_list.asp?parentid=" & intParentID, ""
		if intID > 0 then
			if rsInput("chrStatus") <> "A" then
				AddPageAction "Activate", "portal_detail.asp?action=setstatus&status=A&id=" & intID & "&parentid=" & intParentID, "Are you sure you want to activate this item?"
			end if
			if rsInput("chrStatus") <> "I" then
				AddPageAction "Deactivate", "portal_detail.asp?action=setstatus&status=I&id=" & intID & "&parentid=" & intParentID, "Are you sure you want to deactivate this item?"
			end if
			AddPageAction "Delete", "portal_detail.asp?action=delete&id=" & intID & "&intParentID=" & intParentID, "Are you sure you want to delete this item?"
		end if
	else
		AddPageAction "Return", "portal_list.asp?parentid=" & intParentID, ""
	end if
	
	if intID > 0 then
		strInfoText = "<B>Record ID:</B> " & rsInput("intID")
		strInfoText = strInfoText & "&nbsp; <B>Created:</B> " & rsInput("dtmCreated")
		strInfoText = strInfoText & "&nbsp; <B>Updated:</B> " & rsInput("dtmUpdated")
	else
		strInfoText = ""
	end if
	
	DrawFormHeader "100%", strTitle, strInfoText
	
	stlBeginStdTable "100%"
	response.write "<TR><TD>"
	
		stlBeginFormSection "100%", 2
		stlTextInput "vchItemName", 50, 50, rsInput, "Store Front Name", "IsEmpty", "Missing Store Front Name"
		stlTextInput "vchURL", 50, 50, rsInput, "URL Link", "IsEmpty", "Missing URL Link"
		
		response.Write "<TR><TD>&nbsp;</TD></TR>"
		stlStaticText "Current Header Image", rsData("vchHeaderImg")
		stlFileInput "vchHeaderImg", 50, 50, rsInput, "New Header Image", "", ""
		
		response.Write "<TR><TD>&nbsp;</TD></TR>"
		stlStaticText "Current Header Image", rsData("vchFooterImg")
		stlFileInput "vchFooterImg", 50, 50, rsInput, "New Footer Name", "", ""
		
		response.Write "<TR><TD>&nbsp;</TD></TR>"
		MakeDctInvList
		stlArrayPulldown "intInvID", "", rsInput, dctInvList, "\Inventory ID", "", ""
		
		stlEndFormSection
		
	response.write "</TD></TR>"
	response.Write "<TR><TD>"
	stlSubmit iif(blnCanEdit, "Submit", "Return")
	response.Write "</TD></TR>"
	
	stlEndStdTable
	response.write "</FORM>"
	
	if blnCanEdit then
		SetFieldFocus "frm", "vchItemName"
	end if
end sub

sub MakeDctInvList
	dim SQL, rsTemp
	
	SQL = "SELECT intID, vchItemName FROM " & STR_TABLE_INVENTORY
	SQL = SQL & " WHERE chrStatus='A' AND chrType = 'A'"
	SQL = SQL & " ORDER BY vchItemName"
	
	set rsTemp = ConnOpenRS(SQL)

	if IsObject(dctInvList) then
		dctInvList.RemoveAll
	else
		set dctInvList = Server.CreateObject("Scripting.Dictionary")
	end if
	
	while not rsTemp.EOF
		dctInvList.Add cint(rsTemp("intID")), rsTemp("vchItemName")&""
		rsTemp.MoveNext
	wend
	
	rsTemp.close
	set rsTemp = nothing
	
end sub

function RequestPoll (strField)
	Dim strValue
	strValue = rsRequestData(strField)
	If strValue = "" Then
		strValue = Request.QueryString(strField)
	end if
	RequestPoll = strValue
end function

%>