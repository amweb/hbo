<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: group_list_detail.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Inventory admin - details on folders
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   (1) SWSCodeChange - fixed error on NULL chrParentStatus on creating new record
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_INVENTORY_VIEW) then
	response.redirect "main.asp"
end if

dim blnCanEdit
blnCanEdit = CheckUserAccess(ACC_INVENTORY_EDIT)

const STR_PAGE_TITLE = "Group Details"
const STR_PAGE_TYPE = "admininvlist"

OpenConn

dim dctShoppers
dim strSQL, rsRequestData, objUpload
Set objUpload = Server.CreateObject("SoftArtisans.FileUp")
Set rsRequestData = objUpload.Form

dim intID, strAction, returnURL
strAction = RequestPoll("action")
intID = RequestPoll("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if
returnURL = RequestPoll("return")
if returnURL = "" then
	returnURL = "group_list.asp" 
end if

if strAction = "delete" then
	if blnCanEdit then
		gobjConn.execute("UPDATE " & STR_TABLE_GROUPS & " SET chrStatus='D' WHERE intID=" & intID)
		' SWSCodeChange - 04aug2000 added call to ResyncInvParentStatus
		'ResyncInvParentStatus intID
	end if
	response.redirect returnURL	'"group_list.asp"
elseif strAction = "setstatus" then
	if blnCanEdit then
		gobjConn.execute("UPDATE " & STR_TABLE_GROUPS & " SET chrStatus='" & RequestPoll("status") & "' WHERE intID=" & intID)
		' SWSCodeChange - 04aug2000 added call to ResyncInvParentStatus
		'ResyncInvParentStatus intID
	end if
	response.redirect returnURL	'"group_list.asp"
elseif strAction = "submit" then
	if blnCanEdit then
		'AutoCheck rsRequestData, ""
		if FormErrors.count = 0 then
			dim rsTemp, intNewOrder
			dim dctSaveList
			set dctSaveList = Server.CreateObject("Scripting.Dictionary")
				dctSaveList.Add "*chrStatus", "A"
				dctSaveList.Add "!vchGroupName", ""
			SaveDataRecord STR_TABLE_GROUPS, rsRequestData, intID, dctSaveList
' End SWSCodeChange (1)
			response.redirect returnURL	'"group_list.asp"
		else
			
			DrawPage rsRequestData
			
		end if
	else
		response.redirect returnURL
	end if
else
	dim rsData, blnDataIsRS
	if intID > 0 then
		strSQL = "SELECT * FROM " & STR_TABLE_GROUPS & " WHERE intID=" & intID & " AND chrStatus<>'D'"
		set rsData = gobjConn.execute(strSQL)
		blnDataIsRS = true
		if rsData.eof then
			response.redirect returnURL	'"invlt_blist.asp"
		end if
	else
		set rsData = rsRequestData
		blnDataIsRS = false
	end if
	
	
	DrawPage rsData
	
	
	if blnDataIsRS then
		rsData.close
		set rsData = nothing
	end if
end if
response.end

sub DrawPage(rsInput)
call DrawHeaderUpdate()
	dim strTitle, strInfoText
	if intID > 0 then
		strTitle = "Edit Group - " & rsInput("vchGroupName")
	else
		strTitle = "Add New Group"
	end if
%>
<FORM ACTION="group_list_detail.asp" METHOD="POST" NAME="frm" ENCTYPE="multipart/form-data">
<INPUT TYPE="hidden" NAME="action" VALUE="submit" />
<INPUT TYPE="hidden" NAME="id" VALUE="<%= intID %>" />
<%
	if blnCanEdit then		'"invlt_list.asp?parentid=" & intParentID
		AddPageAction "Cancel", returnURL, ""
		if intID > 0 then
			if rsInput("chrStatus") <> "A" then
				AddPageAction "Activate", "group_list_detail.asp?action=setstatus&status=A&id=" & intID, "Are you sure you want to activate this item?"
			end if
			if rsInput("chrStatus") <> "I" then
				AddPageAction "Deactivate", "group_list_detail.asp?action=setstatus&status=I&id=" & intID, "Are you sure you want to deactivate this item?"
			end if
			AddPageAction "Delete", "group_list_detail.asp?action=delete&id=" & intID, "Are you sure you want to delete this item?"
		end if
	else
		AddPageAction "Return", "group_list.asp"
	end if
	
	if intID > 0 then
		strInfoText = "<B>Record ID:</B> " & rsInput("intID")
	else
		strInfoText = ""
	end if
	
	DrawFormHeader "100%", strTitle, strInfoText
	
	stlBeginStdTable "100%"
	response.write "<tr><td>"
	
	if blnCanEdit then
		stlBeginFormSection "100%", 4
		stlTextInput "vchGroupName", 20, 32, rsInput, "Group \Name", "IsEmpty", "Missing Group Name"
'		stlCheckbox "chrSpecial", "Y", rsInput, "\Special", "yes"
		stlEndFormSection
	else
		stlBeginFormSection "100%", 2
		stlStaticText "Group Name", rsInput("vchGroupName")
'		stlStaticText "Special:", iif(rsInput("chrSpecial") = "Y", "Yes", "No")
		stlEndFormSection

		stlRule

		stlRule
		
	end if
	response.write "</td></tr>"
	stlSubmit iif(blnCanEdit, "Submit", "Return")
	
	stlEndStdTable
	response.write "</FORM>"
	
	if blnCanEdit then
		SetFieldFocus "frm", "vchGroupName"
	end if
end sub

function RequestPoll (strField)
	Dim strValue
	strValue = rsRequestData(strField)
	If strValue = "" Then
		strValue = Request.QueryString(strField)
	end if
	RequestPoll = strValue
end function

' Added RFKCodeChange (1) - 8oct2002

Function IsValid(TheString)
	If IsNull(TheString) Or IsEmpty(TheString) Or TheString = "" Or TheString = " " Then
		IsValid = False
	Else
		If IsNumeric(TheString) Then
			IsValid = iif(TheString > 0, True, False)
		Else
			IsValid = True
		End if
	End If
End Function

' ========== RenoveSpaces ==========
function RemoveSpaces(strInput)
	dim strOut,thischar,i
	strOut = ""
	for i = 1 to len(strInput)
		thischar = mid(strInput,i,1)
		select case thischar
			case " ", ",", "'", """", "(", ")", "{", "}", "[", "]", "|"
			' remove Whitespace characters
			case "!", "@", "#", "$", "%", "^", "*", "+", ":", ";", "<", ">", "?"
			' remove DOS reserved characters
			case else
				StrOut = StrOut & thischar
		end select
	next
	RemoveSpaces = StrOut
end function


%>