<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<!--#include file="../../config/incVeracore.asp"-->
<%

Server.ScriptTimeout = 1000

' =====================================================================================
' = File: inv_stock.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Inventory stock-range report and flat-inventory listing.
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

'CheckAdminLogin

'if not CheckUserAccess(ACC_INVENTORY_VIEW) then
'	response.redirect "main.asp"
'end if

dim blnCanEdit
blnCanEdit = CheckUserAccess(ACC_INVENTORY_EDIT)

dim strPageTitle

const STR_PAGE_TYPE = "admininvlist"

const STR_LIST_SCRIPT = "inv_stock.asp?all=1&"
const STR_STOCK_SCRIPT = "inv_stock.asp?"
const STR_DETAIL_FOLDER_SCRIPT = "inv_fdetail.asp?"
const STR_DETAIL_ITEM_SCRIPT = "inv_idetail.asp?"
const STR_STOCK_LOW_SCRIPT = "inv_stock.asp?"
const STR_STOCK_HIGH_SCRIPT = "inv_stock.asp?"

OpenConn

dim tfShowAll
'tfShowAll = (Request.QueryString("all").count > 0)
tfShowAll = True

if tfShowAll then
	strPageTitle = "Flat Inventory Listing"
else
	strPageTitle = "Low/High Stock Quantities"
end if

dim strSQL, rsData, myFile, FSO, strSave, strTimestamp
dim dctSaveList, dctUpdatedInv, intID, updateInvCount, a, x

if Request.QueryString("resync") = "fullInfoResync" Then
	strSQL = "SELECT * FROM " & STR_TABLE_INVENTORY_LONG_TERM
	strSQL = strSQL & " WHERE vchPartNumber<>'' "
	set rsData = gobjConn.execute(strSQL)

	While Not rsData.EOF
		NewAddProduct rsData("vchItemName"),rsData("vchPartNumber"),rsData("vchBundleQuantity"),rsData("intLowStock")
		rsData.MoveNext
	wend 
	response.end
end if

If Request.QueryString("resync") = "sqlsync" or Request.QueryString("resync") = "1" Then
	response.write "Veracore SQL Resync Requested<br />"
	strSave = "Veracore SQL Resync Requested" & vbNewLine
	strSQL = "SELECT * FROM " & STR_TABLE_INVENTORY_LONG_TERM
	strSQL = strSQL & " WHERE vchPartNumber<>'' and chrstatus <>'D'  "
	set rsData = gobjConn.execute(strSQL)

While Not rsData.EOF
	response.write "Updating: " & rsData("vchPartNumber") & "<br />"
	strSave = strSave & "Updating: " & rsData("vchPartNumber") & vbNewLine
	response.write "Current Inv: " & rsData("intStock") & "<br />"
	strSave = strSave & "Current Inv: " & rsData("intStock") & vbNewLine
		
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "#intStock", vcGetStock(rsData("vchPartNumber"))
		If dctSaveList("#intStock")&"" <> rsData("intStock")&"" Then		
			intID = SaveDataRecord( STR_TABLE_INVENTORY_LONG_TERM, rsData, rsData("intID"), dctSaveList)
			response.write "Updated Item: " & intID & "<br />"
			strSave = strSave & "Updated Item: " & intID & vbNewLine
		End If
		response.write "Updated Inv: " & dctSaveList("#intStock") & "<br />"
		strSave = strSave & "Updated Inv: " & dctSaveList("#intStock") & vbNewLine
	rsData.MoveNext
	response.flush
Wend

	set FSO = Server.CreateObject("scripting.FileSystemObject")
	strTimestamp = year(now) & right("0" & month(now),2) & right("0" & day(now),2) & "_" & right("0" & hour(now),2) & right("0" & minute(now),2) & right("0" & second(now),2)
	set myFile = fso.CreateTextFile("C:\\WebLogs\\HBO\\SOAP-History\\full-resync-" & strTimestamp & ".txt", true)

	myFile.WriteLine(strSave)
	myFile.Close

strSQL = "Truncate Table " & STR_TABLE_INVENTORY_LONG_TERM_UPDATE
gobjConn.execute(strSQL)

End If

If Request.QueryString("resync") = "indiv" Then
	response.write "Veracore Resync Requested<br />"
	strSave = "Veracore Resync Requested" & vbNewLine
	strSQL = "SELECT * FROM " & STR_TABLE_INVENTORY_LONG_TERM_UPDATE
	strSQL = strSQL & " WHERE datediff(mi, dteCheckoutTime, GetDate()) > 5 "
	set rsData = gobjConn.execute(strSQL)
	
	updateInvCount = 0
	
	set dctUpdatedInv = Server.CreateObject("Scripting.Dictionary")
While Not rsData.EOF
	response.write "Updating: " & rsData("vchPartNumber") & "<br />"
	strSave = strSave & "Updating: " & rsData("vchPartNumber") & vbNewLine
	response.write "Current Inv: " & rsData("intStock") & "<br />"
	strSave = strSave & "Current Inv: " & rsData("intStock") & vbNewLine
		'dim dctSaveList, intID
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "#intStock", GetProductAvailabilites(rsData("vchPartNumber"))
		If dctSaveList("#intStock")&"" <> rsData("intStock")&"" Then	
			intID = SaveDataRecord( STR_TABLE_INVENTORY_LONG_TERM, rsData, rsData("intInvID"), dctSaveList)
			dctUpdatedInv.Add updateInvCount&"", rsData("intInvID") &""
			strSave = strSave & "Updated Item: " & intID & vbNewLine
		End If
		response.write "Updated Inv: " & dctSaveList("#intStock") & "<br />"
		strSave = strSave & "Updated Inv: " & dctSaveList("#intStock") & vbNewLine
		updateInvCount = updateInvCount + 1
	rsData.MoveNext
Wend
	'response.write dctUpdatedInv.Count
	if dctUpdatedInv.Count > 0 then
		a = dctUpdatedInv.Items
		strSQL = "Delete FROM " & STR_TABLE_INVENTORY_LONG_TERM_UPDATE
		strSQL = strSQL & " WHERE intInvID in ( "
		for x = 0 To dctUpdatedInv.Count -1
			strSQL = strSQL & a(x)&""
			if x <> dctUpdatedInv.count -1 then
				strSQL = strSQL & ","
			end if
		next
		strSQL = strSQL & " ) "
		gobjConn.execute(strSQL)
		set FSO = Server.CreateObject("scripting.FileSystemObject")
		strTimestamp = year(now) & right("0" & month(now),2) & right("0" & day(now),2) & "_" & right("0" & hour(now),2) & right("0" & minute(now),2) & right("0" & second(now),2)
		set myFile = fso.CreateTextFile("C:\\WebLogs\\HBO\\SOAP-History\\indiv-resync-" & strTimestamp & ".txt", true)

		myFile.WriteLine(strSave)
		myFile.Close
	end if

End If

' gather inventory statistics
dim intLowCount, intHighCount, intInvCount
strSQL = "SELECT "
strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_INVENTORY & " WHERE chrStatus<>'D') AS intCount,"
strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_INVENTORY & " WHERE chrStatus<>'D' AND intStock < intLowStock) AS intLowCount,"
strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_INVENTORY & " WHERE chrStatus<>'D' AND intStock > intHighStock) AS intHighCount"
set rsData = gobjConn.execute(strSQL)
intInvCount = rsData("intCount")
intLowCount = rsData("intLowCount")
intHighCount = rsData("intHighCount")
rsData.close
set rsData = nothing

' get folder list

strSQL = "SELECT intID, chrType, chrStatus, vchPartNumber, vchItemName, intLowStock, intHighStock, intStock"
strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY
strSQL = strSQL & " WHERE chrStatus<>'D' AND chrType='I'"
if tfShowAll = false then
	strSQL = strSQL & " AND (intStock < intLowStock OR intStock > intHighStock)"
end if
strSQL = strSQL & " ORDER BY vchPartNumber,vchItemName"
set rsData = gobjConn.execute(strSQL)

DrawPage

rsData.close
set rsData = nothing
response.end

sub DrawPage
	dim c, strPrevType, blnCanMoveUp, blnCanMoveDown
	dim intID, strStatus, strPartNumber, strName, intMinStock, intMaxStock, intStock
	
	call DrawHeaderUpdate()
	
	DrawListTitle
	'DrawListNavigation
	DrawListHeader
	while not rsData.eof
		intID = rsData("intID")
		strStatus = rsData("chrStatus")
		strPartNumber = rsData("vchPartNumber")
		strName = rsData("vchItemName")
		intMinStock = rsData("intLowStock")
		intMaxStock = rsData("intHighStock")
		intStock = rsData("intStock")
		rsData.MoveNext
		DrawListItem intID, strStatus, strName, strPartNumber, intMinStock, intMaxStock, intStock
		if not rsData.eof then
			DrawListItemSep
		end if
	wend
	DrawListFooter
end sub

sub DrawListTitle
%>
<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="<%= INT_PAGE_WIDTH %>">
<TR>
	<TD><%= font(5) %><%= strPageTitle %></TD>
<% if not blnPrintMode then %>
	<TD ALIGN=RIGHT><%= font(1) %>
	<% ' page actions 
	%>
	[<A HREF="inv_stock.asp?resync=1">Resync Inventory</A>]
	</TD>
<% end if %>
</TR>
</TABLE>
<%= spacer(1,5) %><BR>
<% stlBeginInfoBar INT_PAGE_WIDTH %>
	<B>Total Inventory:</B> <%= intInvCount %>&nbsp;
	<B><A HREF="<%= STR_STOCK_LOW_SCRIPT %>">Low Stock:</A></B> <%= intLowCount %>&nbsp;
	<B><A HREF="<%= STR_STOCK_HIGH_SCRIPT %>">High Stock:</A></B> <%= intHighCount %>&nbsp;
	</FONT></TD>
	<TD ALIGN=RIGHT><%= fontx(1,1,cWhite) & FormatDateTime(date,1) %>
<% stlEndInfoBar %>
<%= spacer(1,10) %>
<%
end sub

sub DrawListNavigation
	' dctNavList - reversed order, key=ID, item=name
	dim c, x, i, k
	x = dctNavList.count - 1
	i = dctNavList.items
	k = dctNavList.keys
	response.write "<TABLE BORDER=0 CELLPADDING=6 CELLSPACING=0 WIDTH=""" & INT_PAGE_WIDTH & """>"
	response.write "<TR><TD>" & font(1)
	for c = x to 0 step -1
		if c > 0 then
			response.write "<B><A HREF=""" & STR_LIST_SCRIPT & "parentid=" & k(c) & """>" & i(c) & "</A></B> : "
		else
			response.write i(c)
		end if
	next
	response.write "</TD>"
	if x > 0 then
		response.write "<TD ALIGN=RIGHT>" & font(1) & "<A HREF=""" & STR_LIST_SCRIPT & "parentid=" & k(1) & """>To Previous Folder<IMG SRC=""" & imagebase & "icon_folderup.gif"" WIDTH=""16"" HEIGHT=""16"" ALT=""To Previous Folder"" BORDER=""0"" ALIGN=""absmiddle"" HSPACE=""5""></A></TD>"
	end if
	response.write "</TR></TABLE>"
end sub

sub BuildNavList(byVal intParentID)
	dim strSQL, rsData
	set dctNavList = Server.CreateObject("Scripting.Dictionary")
	while intParentID <> 0
		strSQL = "SELECT intParentID, vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intParentID & " AND chrStatus<>'D'"
		set rsData = gobjConn.execute(strSQL)
		dctNavList.Add intParentID, rsData("vchItemName") & ""
		intParentID = rsData("intParentID")
		rsData.close
		set rsData = nothing
	wend
	dctNavList.Add 0, "Top"
end sub

sub DrawListHeader
%>
<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 WIDTH="<%= INT_PAGE_WIDTH %>" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVLtGrey) %>">
<TR>
	<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cWhite %>">
	<TR>
		<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%">
		<TR BGCOLOR="<%= cVLtYellow %>">
			<TD><%= font(1) %>&nbsp;</TD>
			<TD NOWRAP><B>Name</B></TD>
			<TD NOWRAP><B>&nbsp;Part #&nbsp;</B></TD>
			<TD NOWRAP ALIGN="center"><B>&nbsp;In&nbsp;Stock</B></TD>
			<TD NOWRAP ALIGN="right"><B>Status&nbsp;</B></TD>
		</TR>
<%
end sub

sub DrawListItem(intID, strStatus, strName, strPartNumber, intMinStock, intMaxStock, intQty)
%>
		<TR VALIGN=BOTTOM>
			<TD ALIGN=CENTER><A HREF="<%= STR_DETAIL_ITEM_SCRIPT %>id=<%= intID %>"><IMG SRC="<%= imagebase %>icon_product.gif" WIDTH="16" HEIGHT="16" ALT="<%= iif(blnCanEdit, "Edit", "View") %> This Item" BORDER="0"></A></TD>
			<TD><%= font(2) %><A HREF="<%= STR_DETAIL_ITEM_SCRIPT %>id=<%= intID %>"><%= strName %></A></TD>
			<TD NOWRAP><%= font(1) %>&nbsp;<%= iif(IsNull(strPartNumber), "n/a", strPartNumber) %>&nbsp;</TD>
			<TD NOWRAP ALIGN="center">&nbsp;<%= intQty %>&nbsp;</TD>
			<TD NOWRAP ALIGN="right"><%= font(1) %><%= GetStatusStr(strStatus, true) %>&nbsp;</TD>
		</TR>
<%
end sub

sub DrawListItemSep
%>
		<TR>
			<TD COLSPAN=7><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVVLtGrey) %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
		</TR>
<%
end sub

sub DrawListFooter
%>
		</TABLE></TD>
	</TR>
	</TABLE></TD>
</TR>
</TABLE>
<%
end sub

sub xDrawPage
%>
<CENTER>

<B><%= strPageTitle %></B><BR>
<BR>

<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="0">
<TR>
	<TD ALIGN="right" COLSPAN="2"><B>Total Inventory:</B></TD>
	<TD>&nbsp;<%= intInvCount %></TD>
</TR>
<TR>
	<TD ALIGN="right"><A HREF="inv_stock.asp"><B>Low Stock:</B></A></TD>
	<TD>&nbsp;<%= intLowCount %></TD>
	<TD>&nbsp;</TD>
	<TD ALIGN="right"><A HREF="inv_stock.asp"><B>High Stock:</B></A></TD>
	<TD>&nbsp;<%= intHighCount %></TD>
</TR>
</TABLE>

<FONT SIZE=1>
<BR>
<% if tfShowAll then %>
	[<A HREF="inv_stock.asp">Show Out of Stock Range</A>]
<% else %>
	[<A HREF="inv_stock.asp?all=1">Show All Items</A>]
<% end if %>
<BR>
(add a keyword search here)
<BR><BR>
</FONT>
<TABLE BORDER="1" CELLPADDING="2" CELLSPACING="0" WIDTH="540">
<TR BGCOLOR="#AAAAAA">
	<TD COLSPAN="6" ALIGN="center"><FONT SIZE="2"><B><% if tfShowAll then %>All Inventory Items<% else %>Inventory Items Out of Range<% end if %></B></FONT></TD>
</TR>
<TR BGCOLOR="#AAAAAA">
	<TD NOWRAP VALIGN="bottom"><B>Part #</B></TD>
	<TD NOWRAP WIDTH="100%" VALIGN="bottom"><B>Item Name</B></TD>
	<TD NOWRAP ALIGN="center"><B>Preferred<BR>Stock</B></TD>
	<TD NOWRAP ALIGN="center"><B>Current<BR>Stock</B></TD>
	<TD ALIGN="center" VALIGN="bottom"><B>Status</B></TD>
	<TD>&nbsp;</TD>
</TR>
<%
	if rsData.eof then
%>
<TR BGCOLOR="#DDDDDD">
	<TD COLSPAN="6" ALIGN="middle">This folder is empty.</TD>
</TR>
<%
	else
		dim tfAltColor
		while not rsData.eof
			tfAltColor = not tfAltColor
%>
<TR<%= iif(tfAltColor=true, " BGCOLOR=""#DDDDDD""", "") %>>
	<TD NOWRAP VALIGN="top"><%= rsData("vchPartNumber") %>&nbsp;</TD>
	<TD VALIGN="top"><%= rsData("vchItemName") %></TD>
<% if rsData("chrType") = "A" then %>
	<TD COLSPAN="3" VALIGN="top">&nbsp;</TD>
	<TD NOWRAP VALIGN="top">[<A HREF="inv_list.asp?parentid=<%= rsData("intID") %>">Open Folder</A>] [<A HREF="inv_fdetail.asp?id=<%= rsData("intID") %>"><%= iif(blnCanEdit, "Edit", "View") %></A>]</TD>
<% else %>
	<TD ALIGN="center" VALIGN="top"><%= rsData("intLowStock") %> - <%= rsData("intHighStock") %></TD>
	<TD ALIGN="center" VALIGN="top"><%= rsData("intStock") %></TD>
	<TD ALIGN="center" VALIGN="top"><%= GetStatusStr(rsData("chrStatus"), true) %></TD>
	<TD NOWRAP ALIGN="center" VALIGN="top">[<A HREF="inv_idetail.asp?id=<%= rsData("intID") %>"><%= iif(blnCanEdit, "Edit", "View") %></A>]</TD>
<% end if %>
</TR>
<%
			rsData.MoveNext
		wend
	end if
%>
</TABLE>
</CENTER>
<%
end sub

Function vcGetStock(partNum)
	Dim veraConn,vcrs
	set veraConn = Server.CreateObject("ADODB.Connection")
	veraConn.Open "Provider=SQLOLEDB.1;Data Source='vc-sql';Initial Catalog='HBO001';user id = 'sa';password='L99ght99ng'"
	strSQL = "select isnull(ONHAND,0)-isnull(MARKED,0)-isnull(UNAVAILABLE,0)-isnull(RESERVED,0) as avail from dbo.PrdSum WHERE "
	strSQL = strSQL &" PRDUCT_PARTNUMBER like '"&partNum&"'"
	set vcrs = veraConn.execute(strSQL)
	if not vcrs.eof then
		vcGetStock=clng(vcrs("avail"))
	else
		vcGetStock=0
	end if

End Function
%>