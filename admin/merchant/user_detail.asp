<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

CheckAdminLogin

const STR_PAGE_TITLE = "User Details"
const STR_PAGE_TYPE = "adminuserlist"
dim dctBrands
MakeDctBrands()

dim dctAccessList
set dctAccessList = Server.CreateObject("Scripting.Dictionary")
	dctAccessList.Add ACC_ADMIN, "Admin - can create/modify administrative users, view audit log"
if CheckUserAccess(ACC_RAW) then
	dctAccessList.Add ACC_RAW, "Modify Raw Data (PROGRAMMER LEVEL)"
end if
    
	dctAccessList.Add ACC_REFERAL_ADMIN, "Sales Manager - Can only view orders for their sales people"
	dctAccessList.Add ACC_INVENTORY_VIEW, "View Inventory - can view inventory details"
	dctAccessList.Add ACC_INVENTORY_EDIT, "Edit Inventory - can edit inventory details"
	dctAccessList.Add ACC_REPORT_VIEW, "View Reports - can view reports"
	dctAccessList.Add ACC_SHOPPER_VIEW, "View Shoppers - can view shopper list admin"
	dctAccessList.Add ACC_SHOPPER_EDIT, "Edit Shoppers - can edit shopper list"
	dctAccessList.Add ACC_ORDER_VIEW, "View Order Details - can view order details"
	dctAccessList.Add ACC_ORDER_PAYMENT_VIEW, "View Order Payment Info - can view payment info in order details"
	dctAccessList.Add ACC_ORDER_BILLING_VIEW, "View Order Billing Info - can view billing address info in order details"
	dctAccessList.Add ACC_ORDER_SHIPPING_VIEW, "View Order Shipping Info - can view shipping address info in order details"
	dctAccessList.Add ACC_ORDER_TRANS_VIEW, "View Order Transactions - can view transactions list in order details"
	dctAccessList.Add ACC_CARDS_VIEW, "View Cards - can view full credit card numbers"
	dctAccessList.Add ACC_ORDER_CREATE, "Create Orders - can create a new order from scratch"
	dctAccessList.Add ACC_ORDER_EDIT, "Edit Order Before Submitted - can modify an 'in progress' order"
	dctAccessList.Add ACC_ORDER_EMAILRECEIPT, "Resend Customer Invoice - allows viewing billing and shipping!"
	dctAccessList.Add ACC_ORDER_PAYMENT_EDIT, "Edit Order Payment Info - can modify payment info for a submitted order"
	dctAccessList.Add ACC_ORDER_BILLING_EDIT, "Edit Order Billing Info - can modify billing info for a submitted order"
	dctAccessList.Add ACC_ORDER_SHIPPING_EDIT, "Edit Order Shipping Info - can modify shipping info for a submitted order"
	dctAccessList.Add ACC_ORDER_ITEMS_EDIT, "Edit Order Items - can add/edit/remove line items and notes for a submitted order"
	dctAccessList.Add ACC_ORDER_NOTES_ADD, "Add Order Notes - can add new line item notes to a submitted order"
	dctAccessList.Add ACC_ORDER_RETURNS_ADD, "Add Order Returns - can add new line item returns to a submitted order"
	dctAccessList.Add ACC_ORDER_DECLINE, "Decline Order - can decline a submitted order"
	dctAccessList.Add ACC_ORDER_APPROVE, "Approve Order - can approve a submitted order"
	dctAccessList.Add ACC_ORDER_SHIP, "Ship Order - can ship a submitted order"
	dctAccessList.Add ACC_ORDER_TRANS_AUTH, "DoAuth Order - can perform an auth trans on a submitted order"
	dctAccessList.Add ACC_ORDER_TRANS_DEPOSIT, "DoDeposit Order - can perform a deposit trans on a submitted order"
	dctAccessList.Add ACC_ORDER_TRANS_CREDIT, "DoCredit Order - can perform a credit trans on a submitted order"
	dctAccessList.Add ACC_ORDER_TRANS_MANUAL, "DoManualTrans Order - can enter a manual transaction on a submitted order"
	dctAccessList.Add ACC_ORDER_TRANS_AUTHDEP, "DoAuthDep Order - can perform a deposit without an authorization on a submitted order"
	dctAccessList.Add ACC_TRANS_VIEW, "View Transactions - can view system-wide transactions"

dim dctAccessStatusList
set dctAccessStatusList = Server.CreateObject("Scripting.Dictionary")
	dctAccessStatusList.Add "0", "In Progress By Shopper"
	dctAccessStatusList.Add "9", "In Progress By Merchant"
	dctAccessStatusList.Add "S", "Submitted"
	dctAccessStatusList.Add "A", "Approved By Merchant"
	dctAccessStatusList.Add "B", "Abandoned"
	dctAccessStatusList.Add "Z", "Authorized (credit card)"
	dctAccessStatusList.Add "X", "Deposited (credit card/debit)"
	dctAccessStatusList.Add "C", "Credited (credit card/debit)"
	dctAccessStatusList.Add "H", "Shipped"
	dctAccessStatusList.Add "K", "Declined"
	dctAccessStatusList.Add "E", "System Error"
	dctAccessStatusList.Add "R", "Rejected (credit card/debit)"
	dctAccessStatusList.Add "V", "Voice Auth Required (credit card)"

OpenConn

dim strPass, strAccess, strAccessStatus

dim intID, strAction
strAction = Request("action")
intID = Request("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if

if strAction = "delete" then
	gobjConn.execute("UPDATE " & STR_TABLE_USER & " SET chrStatus='D' WHERE intID=" & intID)
	response.redirect "user_list.asp"
elseif strAction = "setstatus" then
	gobjConn.execute("UPDATE " & STR_TABLE_USER & " SET chrStatus='" & Request("status") & "' WHERE intID=" & intID)
	response.redirect "user_list.asp"
elseif strAction = "submit" then
	AutoCheck Request, ""
	if Request("vchPassword") <> "" then
		if Request("vchPassword") <> Request("cnf_vchPassword") then
			FormErrors.Add "vchPassword", "Password did not match."
		end if
	elseif intID = 0 then
		FormErrors.Add "vchPassword", "Missing Password"
	end if
	if FormErrors.count = 0 then
		strAccess = GetAccessString()
		strAccessStatus = GetAccessStatusString()
		dim dctSaveList
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "*@dtmCreated", "GETDATE()"
			dctSaveList.Add "@dtmUpdated", "GETDATE()"
			dctSaveList.Add "*vchCreatedByUser", Session(SESSION_MERCHANT_ULOGIN)
			dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
			dctSaveList.Add "*vchCreatedByIP", gstrUserIP
			dctSaveList.Add "vchUpdatedByIP", gstrUserIP
			dctSaveList.Add "chrType", "U"
			dctSaveList.Add "*chrStatus", "A"
			dctSaveList.Add "!vchFullName", ""
			dctSaveList.Add "!vchUsername", ""
			if Request("vchPassword") <> "" then
				dctSaveList.Add "!vchPassword", ""
			end if
			'if Request("intBrand") <> "" then
				dctSaveList.Add "!intBrand", ""
			'end if
			' SWSCodeChange - 7/20 - added vchReceiveEmail field
			dctSaveList.Add "!vchReceiveEmail", ""
			dctSaveList.Add "vchCanViewStatus", strAccessStatus
			dctSaveList.Add "vchAccess", strAccess
		SaveDataRecord STR_TABLE_USER, Request, intID, dctSaveList
		response.redirect "user_list.asp"
	else
		strPass = Request("vchPassword")
		strAccess = GetAccessString()
		strAccessStatus = GetAccessStatusString()
		
		DrawPage Request
		
	end if
else
	dim strSQL, rsData, blnDataIsRS
	if intID > 0 then
		strSQL = "SELECT * FROM " & STR_TABLE_USER & " WHERE intID=" & intID & " AND chrStatus<>'D'"
		set rsData = gobjConn.execute(strSQL)
		blnDataIsRS = true
		if rsData.eof then
			rsData.close
			set rsData = nothing
			response.redirect "user_list.asp"
		end if
		strPass = ""
		strAccessStatus = rsData("vchCanViewStatus")
		strAccess = rsData("vchAccess")
	else
		blnDataIsRS = false
		set rsData = Request
		strPass = ""
		strAccess = GetAccessString()
		strAccessStatus = GetAccessStatusString()
	end if
	
	
	DrawPage rsData
	
	
	if blnDataIsRS then
		rsData.close
		set rsData = nothing
	end if
end if
response.end

sub DrawPage(rsInput)
call DrawHeaderUpdate()
	dim strTitle, strInfoText
	if intID > 0 then
		strTitle = "Edit User"
	else
		strTitle = "Add User"
	end if
%>
<FORM ACTION="user_detail.asp" METHOD="POST" NAME="frm">
<INPUT TYPE="hidden" NAME="action" VALUE="submit">
<INPUT TYPE="hidden" NAME="id" VALUE="<%= intID %>">
<%
	AddPageAction "Cancel", "user_list.asp", ""
	if intID > 0 then
		if rsInput("chrStatus") <> "A" then
			AddPageAction "Activate", "user_detail.asp?action=setstatus&status=A&id=" & intID, "Are you sure you want to activate this user?"
		end if
		if rsInput("chrStatus") <> "I" then
			AddPageAction "Deactivate", "user_detail.asp?action=setstatus&status=I&id=" & intID, "Are you sure you want to deactivate this user?"
		end if
		AddPageAction "Delete", "user_detail.asp?action=delete&id=" & intID, "Are you sure you want to delete this user?"
	end if
	
	if intID > 0 then
		strInfoText = "<B>Record ID:</B> " & rsInput("intID")
		strInfoText = strInfoText & "&nbsp; <B>Created:</B> " & rsInput("dtmCreated")
		strInfoText = strInfoText & "&nbsp; <B>Updated:</B> " & rsInput("dtmUpdated")
	else
		strInfoText = ""
	end if
	
	DrawFormHeader "100%", strTitle, strInfoText
	
	stlBeginStdTable "100%"
	response.write "<TR><TD>"
	
	stlBeginFormSection "100%", 5
	stlTextInput "vchFullName", 32, 42, rsInput, "\Full Name", "IsEmpty", "Missing Full Name"
	stlResetRow
	
	stlTextInput "vchUsername", 20, 30, rsInput, "\Username", "IsEmpty", "Missing Username"
	stlResetRow
	
	stlPasswordInput "vchPassword", 20, 20, strPass, "\Password", "", ""
	stlRawCell "&nbsp;&nbsp;"
	stlPasswordInput "cnf_vchPassword", 20, 20, "", "\Confirm", "", ""
	stlEndFormSection
	
	' SWSCodeChange - 7/20 - added vchReceiveEmail field
	stlBeginFormSection "100%", 5
	stlRawCellX "Specify an email address if this user should receive email notifications on submitted orders:", "", "", "", 5
	stlEndFormSection
	stlBeginFormSection "100%", 5

	stlTextInput "vchReceiveEmail", 32, 50, rsInput, "Receive \Email", "OptIsEmail", "Invalid email address"

	stlArrayPulldown "intBrand", "Select one...", rsInput, dctBrands, "Brand", "", ""
	stlEndFormSection
	
	stlRule
	
	stlBeginFormSection "100%", 2
	
	stlRawCell "Can&nbsp;View&nbsp;Orders:"
	stlRawCell "(hold down the CTRL key to select more than one)"
	
	stlRawCell ""
	stlRawCell GetOptionBoxStatus(strAccessStatus)
	
	stlRawCell "Access:"
	stlRawCell "(hold down the CTRL key to select more than one)"
	
	stlRawCell ""
	stlRawCell GetOptionBox(strAccess)
	
	stlEndFormSection
	
	response.write "</TD></TR>"
	stlSubmit "Submit"
	
	stlEndStdTable
	response.write "</FORM>"
	
	SetFieldFocus "frm", "vchFullName"
end sub

function GetOptionBox(strValue)
	dim s, c, x, i, k
	s = "<SELECT NAME=""strAccess"" SIZE=""30"" MULTIPLE>"
	x = dctAccessList.count - 1
	i = dctAccessList.items
	k = dctAccessList.keys
	for c = 0 to x
		s = s & "<OPTION VALUE=""" & k(c) & """"
		if mid(strValue, k(c), 1) = "Y" then
			s = s & " SELECTED"
		end if
		s = s & ">" & Server.HTMLEncode(i(c)) & "</OPTION>"
	next
	s = s & "</SELECT>"
	GetOptionBox = s
end function

function GetAccessString()
	dim i, s, x
	s = string(100, "N")
	for each i in Request("strAccess")
		x = clng(i)
		s = left(s, x - 1) & "Y" & mid(s, x + 1)
	next
	GetAccessString = s
end function

function GetOptionBoxStatus(strValue)
	dim s, c, x, i, k
	s = "<SELECT NAME=""strAccessStatus"" SIZE=""14"" MULTIPLE>"
	x = dctAccessStatusList.count - 1
	i = dctAccessStatusList.items
	k = dctAccessStatusList.keys
	for c = 0 to x
		s = s & "<OPTION VALUE=""" & k(c) & """"
		if InStr(strValue, k(c)) > 0 then
			s = s & " SELECTED"
		end if
		s = s & ">" & Server.HTMLEncode(i(c)) & "</OPTION>"
	next
	s = s & "</SELECT>"
	GetOptionBoxStatus = s
end function

function GetAccessStatusString()
	dim i, s, x
	s = ""
	for each i in Request("strAccessStatus")
		s = s & i
	next
	GetAccessStatusString = s
end function

sub MakeDctBrands
	dim SQL, rsTemp
	
	SQL = "SELECT intID, vchItemName FROM " & STR_TABLE_INVENTORY
	SQL = SQL & " WHERE chrStatus='A' AND chrType='B'"
	SQL = SQL & " ORDER BY vchItemName"
	
	set rsTemp = ConnOpenRS(SQL)

	if IsObject(dctBrands) then
		dctBrands.RemoveAll
	else
		set dctBrands = Server.CreateObject("Scripting.Dictionary")
	end if
	
	while not rsTemp.EOF
		dctBrands.Add cint(rsTemp("intID")), rsTemp("vchItemName")&""
		rsTemp.MoveNext
	wend
	
	rsTemp.close
	set rsTemp = nothing
	
end sub
%>