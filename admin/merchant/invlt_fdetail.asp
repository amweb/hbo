<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: invlt_fdetail.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Inventory admin - details on folders
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_INVENTORY_VIEW) then
	response.redirect "main.asp"
end if
dim dctAccessStatusList, strAccessGroups
set dctAccessStatusList = Server.CreateObject("Scripting.Dictionary")
dim blnCanEdit
blnCanEdit = CheckUserAccess(ACC_INVENTORY_EDIT)

const STR_PAGE_TITLE = "Folder Details"
const STR_PAGE_TYPE = "admininvlist"

OpenConn

dim dctBrands
dim strSQL, rsRequestData, objUpload, rsGroupList
Set objUpload = Server.CreateObject("SoftArtisans.FileUp")

set dctAccessStatusList = Server.CreateObject("Scripting.Dictionary")
strSQL = "SELECT intID,vchGroupName FROM " & STR_TABLE_GROUPS & " WHERE chrStatus='A'"
set rsGroupList = gobjConn.execute(strSQL)
while not rsGroupList.eof 
	dctAccessStatusList.Add rsGroupList("intID")&"", rsGroupList("vchGroupName")&""
	rsGroupList.MoveNext
wend

Set rsRequestData = objUpload.Form

dim intID, intParentID, strAction
strAction = RequestPoll("action")
intID = RequestPoll("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if

intParentID = RequestPoll("parentid")
if IsNumeric(intParentID) then
	intParentID = CLng(intParentID)
else
	intParentID = 0
end if

if strAction = "delete" then
	if blnCanEdit then
		gobjConn.execute("UPDATE " & STR_TABLE_INVENTORY_LONG_TERM & " SET chrStatus='D' WHERE intID=" & intID)
	end if
	response.redirect "invlt_list.asp?parentid=" & intParentID
elseif strAction = "setstatus" then
	if blnCanEdit then
		gobjConn.execute("UPDATE " & STR_TABLE_INVENTORY_LONG_TERM & " SET chrStatus='" & RequestPoll("status") & "' WHERE intID=" & intID)
	end if
	response.redirect "invlt_list.asp?parentid=" & intParentID
elseif strAction = "submit" then
	if blnCanEdit then
		AutoCheck rsRequestData, ""
		if RequestPoll("chrSoftFlag") = "Y" then
			CheckField "vchSoftURL", "IsEmpty", rsRequestData, "Missing Download URL"
		end if
		if FormErrors.count = 0 then
			dim rsTemp, intNewOrder
			strAccessGroups = GetGroupAccessString()
			strSQL = "SELECT ISNULL(MAX(intSortOrder),0) AS intMaxSortOrder FROM " & STR_TABLE_INVENTORY_LONG_TERM
			set rsTemp = gobjConn.execute(strSQL)
			intNewOrder = rsTemp("intMaxSortOrder") + 1
			rsTemp.close
			set rsTemp = nothing
			dim dctSaveList
			set dctSaveList = Server.CreateObject("Scripting.Dictionary")
				dctSaveList.Add "*@dtmCreated", "GETDATE()"
				dctSaveList.Add "@dtmUpdated", "GETDATE()"
				dctSaveList.Add "*vchCreatedByUser", Session(SESSION_MERCHANT_ULOGIN)
				dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
				dctSaveList.Add "*vchCreatedByIP", gstrUserIP
				dctSaveList.Add "vchUpdatedByIP", gstrUserIP
				dctSaveList.Add "chrType", "A"
				dctSaveList.Add "*chrStatus", "A"
				dctSaveList.Add "*#intParentID", intParentID
				dctSaveList.Add "*#intSortOrder", intNewOrder
				dctSaveList.Add "*#intHitCount", 0
				dctSaveList.Add "!vchItemName", ""
				dctSaveList.Add "!vchImageURL", ""
				dctSaveList.Add "#mnyItemPrice", "0"
				dctSaveList.Add "chrICFlag", "N"
				dctSaveList.Add "vchGroupingIDs", strAccessGroups
				dctSaveList.Add "chrTaxFlag", "N"
				dctSaveList.Add "chrSoftFlag", "N"
				dctSaveList.Add "!#intBrand", ""
				dctSaveList.Add "!txtDescription", ""
			intID = SaveDataRecord(STR_TABLE_INVENTORY_LONG_TERM, rsRequestData, intID, dctSaveList)
			
            dim fImage, uploadPath, IsImageGood, fImageOrient
			IsImageGood = false
			fImageOrient = 0
			If IsObject(objUpload.Form("ImageFile")) then
				if (objUpload.Form("ImageFile").TotalBytes <> 0) and (lcase(right(objUpload.Form("ImageFile").UserFilename, 4))=".jpg" or lcase(right(objUpload.Form("ImageFile").UserFilename, 4))=".gif") then
					fImage = "Photo" & intID & "." & Mid(objUpload.form("ImageFile").UserFilename, InstrRev(objUpload.form("ImageFile").UserFilename, "\") + 1)
					uploadPath = Server.MapPath(liveimagebase) & "\"
					objUpload.Form("ImageFile").SaveAs uploadPath & fImage
					if lcase(right(objUpload.Form("ImageFile").UserFilename, 4))=".gif" then 
						IsImageGood = true	' - GIF Files don't work with JDO
					else
					    'AL - not resizing these folder images - they have forced size of 943w x 201h per site design
                        IsImageGood = true
                    	'IsImageGood = SetImageReSize( uploadPath, fImage, fImageOrient)	' true
					end if
				end if
			end if
			if IsImageGood then
				dctSaveList.RemoveAll
				dctSaveList.Add "*@dtmCreated", "GETDATE()"
				dctSaveList.Add "vchImageURL", fImage
				SaveDataRecord STR_TABLE_INVENTORY_LONG_TERM, rsRequestData, intID, dctSaveList
			end if
			
			'strSQL =  "update " & STR_TABLE_INVENTORY_LONG_TERM & " set vchGroupingIDs = '"& strAccessGroups & "' where intparentid = " & intID
			'gobjConn.execute(strSQL)
			
			strSQL =  "update " & STR_TABLE_INVENTORY_LONG_TERM & " set vchGroupingIDs = '"& strAccessGroups & "' where intid = " & intID
			gobjConn.execute(strSQL)

            response.redirect "invlt_list.asp?parentid=" & intParentID
		else
			strAccessGroups = GetGroupAccessString()
			DrawPage rsRequestData
			
		end if
	else
		response.redirect "invlt_list.asp?parentid=" & intParentID
	end if
else
	dim rsData, blnDataIsRS
	if intID > 0 then
		strSQL = "SELECT * FROM " & STR_TABLE_INVENTORY_LONG_TERM & " WHERE intID=" & intID & " AND chrStatus<>'D'"
		set rsData = gobjConn.execute(strSQL)
		blnDataIsRS = true
		if rsData.eof then
			response.redirect "invlt_list.asp"
		end if
		intParentID = rsData("intParentID")
		
		strAccessGroups = rsData("vchGroupingIDs")
		
	else
		strAccessGroups = GetGroupAccessString()
		set rsData = rsRequestData
		blnDataIsRS = false
	end if
	
	
	DrawPage rsData
	
	
	if blnDataIsRS then
		rsData.close
		set rsData = nothing
	end if
end if
response.end

sub DrawPage(rsInput)
	dim strTitle, strInfoText
	if intID > 0 then
		strTitle = "Edit Folder - " & rsInput("vchItemName")
	else
		strTitle = "Add New Folder"
	end if
%>
<FORM ACTION="invlt_fdetail.asp" METHOD="POST" NAME="frm" ENCTYPE="multipart/form-data">
<INPUT TYPE="hidden" NAME="action" VALUE="submit">
<INPUT TYPE="hidden" NAME="id" VALUE="<%= intID %>">
<INPUT TYPE="hidden" NAME="parentid" VALUE="<%= intParentID %>">
<%
	if blnCanEdit then
		AddPageAction "Cancel", "invlt_list.asp?parentid=" & intParentID, ""
		if intID > 0 then
			if rsInput("chrStatus") <> "A" then
				AddPageAction "Activate", "invlt_fdetail.asp?action=setstatus&status=A&id=" & intID & "&parentid=" & intParentID, "Are you sure you want to activate this item?"
			end if
			if rsInput("chrStatus") <> "I" then
				AddPageAction "Deactivate", "invlt_fdetail.asp?action=setstatus&status=I&id=" & intID & "&parentid=" & intParentID, "Are you sure you want to deactivate this item?"
			end if
			AddPageAction "Delete", "invlt_fdetail.asp?action=delete&id=" & intID & "&intParentID=" & intParentID, "Are you sure you want to delete this item?"
		end if
	else
		AddPageAction "Return", "invlt_list.asp?parentid=" & intParentID, ""
	end if
	
	if intID > 0 then
		strInfoText = "<B>Record ID:</B> " & rsInput("intParentID") & "-" & rsInput("intID")
		strInfoText = strInfoText & "&nbsp; <B>Created:</B> " & rsInput("dtmCreated")
		strInfoText = strInfoText & "&nbsp; <B>Updated:</B> " & rsInput("dtmUpdated")
	else
		strInfoText = ""
	end if
	
	DrawFormHeader "100%", strTitle, strInfoText
	
	stlBeginStdTable "100%"
	response.write "<TR><TD>"
	
	if blnCanEdit then
		stlBeginFormSection "100%", 4
		stlTextInput "vchItemName", 20, 32, rsInput, "Folder \Name", "IsEmpty", "Missing Folder Name"

		MakeDctBrands
		stlArrayPulldown "intBrand", "None", rsInput, dctBrands, "\Brand", "", ""

		stlEndFormSection
		stlRule

		stlBeginFormSection "100%", 4
		'stlTextInput "vchImageURL", 20, 32, rsInput, "\Image Name", "", ""
        stlTextInput "vchImageURL", 30, 255, rsInput, "\Image ", "", ""
		stlFileInput "ImageFile", 30, 255, "", "or \Upload Image (943w x 201h)", "", ""
		
		'stlRule
		'stlRawCell "Restrict Access:"
		'stlRawCell GetOptionBoxGroups(strAccessGroups)
		'stlEndFormSection
		
		
		
		stlRule
		
		response.write "<U>D</U>escription:<BR><BR>"
		response.write "<TEXTAREA NAME=""txtDescription"" ROWS=6 COLS=50 ACCESSKEY=""D"" CLASS=""textarea"">" & Server.HTMLEncode(rsInput("txtDescription") & "") & "</TEXTAREA>"
	else
		stlBeginFormSection "100%", 2
		stlStaticText "Folder Name", rsInput("vchItemName")
		stlEndFormSection
		
		stlRule
		
		stlStaticText "Description", rsInput("txtDescription")
	end if
	response.write "</TD></TR>"
	stlSubmit iif(blnCanEdit, "Submit", "Return")
	
	stlEndStdTable
	response.write "</FORM>"
	
	if blnCanEdit then
		SetFieldFocus "frm", "vchItemName"
	end if
end sub

sub MakeDctBrands
	dim SQL, rsTemp
	
	SQL = "SELECT intID, vchItemName FROM " & STR_TABLE_INVENTORY_LONG_TERM
	SQL = SQL & " WHERE chrStatus='A' AND chrType='B'"
	SQL = SQL & " ORDER BY vchItemName"
	
	set rsTemp = ConnOpenRS(SQL)

	if IsObject(dctBrands) then
		dctBrands.RemoveAll
	else
		set dctBrands = Server.CreateObject("Scripting.Dictionary")
	end if
	
	while not rsTemp.EOF
		dctBrands.Add cint(rsTemp("intID")), rsTemp("vchItemName")&""
		rsTemp.MoveNext
	wend
	
	rsTemp.close
	set rsTemp = nothing
	
end sub

function RequestPoll (strField)
	Dim strValue
	strValue = rsRequestData(strField)
	If strValue = "" Then
		strValue = Request.QueryString(strField)
	end if
	RequestPoll = strValue
end function


function GetOptionBoxGroups(strValue)
	dim s, c, x, i, k
	s = "(hold down the CTRL key to select more than one)<br /><SELECT style=""width:100%"" NAME=""vchGroupIDs"" MULTIPLE>"
	x = dctAccessStatusList.count - 1
	i = dctAccessStatusList.items
	k = dctAccessStatusList.keys
	for c = 0 to x
		s = s & "<OPTION VALUE=""" & k(c) & """"
		if InStr(strValue, k(c)) > 0 then
			s = s & " SELECTED"
		end if
		s = s & ">" & Server.HTMLEncode(i(c)) & "</OPTION>"
	next
	s = s & "</SELECT>"
	GetOptionBoxGroups = s
end function

function GetGroupAccessString()
	dim i, s, x
	s = ""
	s = RequestPoll("vchGroupIDs")
	GetGroupAccessString = s
end function


%>