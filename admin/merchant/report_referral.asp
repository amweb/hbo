<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: report_state.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Generate report by state
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_REPORT_VIEW) then
	response.redirect "menu.asp"
end if

dim strPageType, strPageTitle

strPageType = "adminrepsummary"
strPageTitle = "Order Report By Referral"

OpenConn

dim strSQL, rsData, dtmStart, dtmEnd,rsCustomData, rsData_Incomplete, rsCustomData_Incomplete
dim intRptType, strSQLColumn
GetReportDateRangeX dtmStart, dtmEnd, intRptType

strSQLColumn = GetSQLColumnName
'response.End

strSQL = "SELECT"
strSQL = stRSQL & " COUNT(DISTINCT intID) AS intOrderCount,"
strSQL = strSQL & " SUM(mnyGrandTotal) AS mnyGrandTotal,"
strSQL = strSQL & " SUM(mnyTaxAmount) AS mnyTaxAmount,"
strSQL = strSQL & " SUM(mnyShipAmount) AS mnyShipAmount,"
strSQL = strSQL & " vchReferalName AS vchReferalName"
strSQL = strSQL & " FROM (SELECT intID,mnyGrandTotal,mnyTaxAmount,mnyShipAmount,IsNull(vchReferalName,'') AS vchReferalName FROM " & STR_TABLE_ORDER & " AS O"
strSQL = strSQL & " WHERE O.chrStatus IN ('H','X')"	' only show shipped or deposited orders

'strSQL = strSQL & " AND O.dtmShipped >= '" & dtmStart & "' AND O.dtmShipped <= '" & dtmEnd & "') dt_Orders"

strSQL = strSQL &  strSQLColumn & ") dt_Orders"
strSQL = strSQL & " GROUP BY vchReferalName"
strSQL = strSQL & " ORDER BY vchReferalName"
'response.Write strSQL
'response.End

set rsData = gobjConn.execute(strSQL)

strSQL = "SELECT"
strSQL = stRSQL & " COUNT(DISTINCT intID) AS intOrderCount,"
strSQL = strSQL & " SUM(mnyGrandTotal) AS mnyGrandTotal,"
strSQL = strSQL & " SUM(mnyTaxAmount) AS mnyTaxAmount,"
strSQL = strSQL & " SUM(mnyShipAmount) AS mnyShipAmount,"
strSQL = strSQL & " vchCustomReferralName AS vchReferalName"
strSQL = strSQL & " FROM (SELECT intID,mnyGrandTotal,mnyTaxAmount,mnyShipAmount,IsNull(vchCustomReferralName,'') AS vchCustomReferralName FROM " & STR_TABLE_ORDER & " AS O"
strSQL = strSQL & " WHERE O.chrStatus IN ('H','X')"	' only show shipped or deposited orders
strSQL = strSQL &  strSQLColumn & ") dt_Orders"
strSQL = strSQL & " GROUP BY vchCustomReferralName"
strSQL = strSQL & " ORDER BY vchCustomReferralName"
'response.Write "<br /><br />" & strSQL
set rsCustomData = gobjConn.execute(strSQL)

strSQL = "SELECT"
strSQL = stRSQL & " COUNT(DISTINCT intID) AS intOrderCount,"
strSQL = strSQL & " SUM(mnyGrandTotal) AS mnyGrandTotal,"
strSQL = strSQL & " SUM(mnyTaxAmount) AS mnyTaxAmount,"
strSQL = strSQL & " SUM(mnyShipAmount) AS mnyShipAmount,"
strSQL = strSQL & " vchReferalName AS vchReferalName"
strSQL = strSQL & " FROM (SELECT intID,mnyGrandTotal,mnyTaxAmount,mnyShipAmount,IsNull(vchReferalName,'') AS vchReferalName FROM " & STR_TABLE_ORDER & " AS O"
strSQL = strSQL & " WHERE O.chrStatus IN ('9', 'P', 'S', 'A', 'Z')"	' show only incomplete orders
strSQL = strSQL &  strSQLColumn & ") dt_Orders"
strSQL = strSQL & " GROUP BY vchReferalName"
strSQL = strSQL & " ORDER BY vchReferalName"
set rsData_Incomplete = gobjConn.execute(strSQL)

strSQL = "SELECT"
strSQL = stRSQL & " COUNT(DISTINCT intID) AS intOrderCount,"
strSQL = strSQL & " SUM(mnyGrandTotal) AS mnyGrandTotal,"
strSQL = strSQL & " SUM(mnyTaxAmount) AS mnyTaxAmount,"
strSQL = strSQL & " SUM(mnyShipAmount) AS mnyShipAmount,"
strSQL = strSQL & " vchCustomReferralName AS vchReferalName"
strSQL = strSQL & " FROM (SELECT intID,mnyGrandTotal,mnyTaxAmount,mnyShipAmount,IsNull(vchCustomReferralName,'') AS vchCustomReferralName FROM " & STR_TABLE_ORDER & " AS O"
strSQL = strSQL & " WHERE O.chrStatus IN ('9', 'P', 'S', 'A', 'Z')"	' only show shipped or deposited orders
strSQL = strSQL &  strSQLColumn & ") dt_Orders"
strSQL = strSQL & " GROUP BY vchCustomReferralName"
strSQL = strSQL & " ORDER BY vchCustomReferralName"
set rsCustomData_Incomplete = gobjConn.execute(strSQL)


DrawPage


rsData.close
set rsData = nothing
response.end

sub DrawPage
	call DrawHeaderUpdate()
%>
<CENTER><%= font(1) %>
<%= font(2) %><B><%= STR_SITE_NAME %><BR>
<%= strPageTitle %><BR>
<%= GetDateRangeType() %></B><BR>
Orders that have been shipped or charged</FONT><BR>
<BR>
<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="2" ID="Table2">
<TR>
	<TD>&nbsp;</TD>
	<TD ALIGN="center">&nbsp;</TD>
	<TD ALIGN="center" COLSPAN="2"><B>Amount</B></TD>
	<TD ALIGN="center" COLSPAN="2"><B>Tax</B></TD>
	<TD ALIGN="center" COLSPAN="2"><B>Shipping</B></TD>
</TR>
<TR>
	<TD><B>referrerid</B></TD>
	<TD ALIGN="center"><B># Orders</B></TD>
	<TD ALIGN="center"><B>Total</B></TD>
	<TD ALIGN="center"><B>Avg</B></TD>
	<TD ALIGN="center"><B>Total</B></TD>
	<TD ALIGN="center"><B>Avg</B></TD>
	<TD ALIGN="center"><B>Total</B></TD>
	<TD ALIGN="center"><B>Avg</B></TD>
</TR>

<%
	while not rsData.eof
%>
<TR>
	<TD><B><%= iif(IsValid(rsData("vchReferalName")),rsData("vchReferalName"),"[No Referral]") %></B></TD>
	<TD ALIGN="center"><%= rsData("intOrderCount") %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData("mnyGrandTotal"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData("mnyGrandTotal") / rsData("intOrderCount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData("mnyTaxAmount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData("mnyTaxAmount") / rsData("intOrderCount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData("mnyShipAmount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData("mnyShipAmount") / rsData("intOrderCount"), 2) %></TD>
</TR>
<%
		rsData.MoveNext
	wend
%>
<tr>
	<td colspan="8" style="border-bottom:1px solid #ccc;">&nbsp;</td>
</tr>
<TR>
	<TD>&nbsp;</TD>
	<TD ALIGN="center">&nbsp;</TD>
	<TD ALIGN="center" COLSPAN="2"><B>Amount</B></TD>
	<TD ALIGN="center" COLSPAN="2"><B>Tax</B></TD>
	<TD ALIGN="center" COLSPAN="2"><B>Shipping</B></TD>
</TR>
<TR>
	<TD><B>Rname</B></TD>
	<TD ALIGN="center"><B># Orders</B></TD>
	<TD ALIGN="center"><B>Total</B></TD>
	<TD ALIGN="center"><B>Avg</B></TD>
	<TD ALIGN="center"><B>Total</B></TD>
	<TD ALIGN="center"><B>Avg</B></TD>
	<TD ALIGN="center"><B>Total</B></TD>
	<TD ALIGN="center"><B>Avg</B></TD>
</TR>
<%
	while not rsCustomData.eof
%>
<TR>
	<TD><B><%= iif(IsValid(rsCustomData("vchReferalName")),rsCustomData("vchReferalName"),"[No Referral]") %></B></TD>
	<TD ALIGN="center"><%= rsCustomData("intOrderCount") %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsCustomData("mnyGrandTotal"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsCustomData("mnyGrandTotal") / rsCustomData("intOrderCount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsCustomData("mnyTaxAmount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsCustomData("mnyTaxAmount") / rsCustomData("intOrderCount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsCustomData("mnyShipAmount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsCustomData("mnyShipAmount") / rsCustomData("intOrderCount"), 2) %></TD>
</TR>
<%
		rsCustomData.MoveNext
	wend 
	%>
</table>

<p></p>

<%= font(2) %><B><%= STR_SITE_NAME %><BR>
Incomplete orders by Referral<BR>
<%= GetDateRangeType() %></B><br>
Orders requested by customers that have not yet been processed<br>
(Status codes: Merchant-In-Progress, Pending, Submitted, Approved, Authorized)</FONT><BR>


<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="2" ID="Table1">
<TR>
	<TD>&nbsp;</TD>
	<TD ALIGN="center">&nbsp;</TD>
	<TD ALIGN="center" COLSPAN="2"><B>Amount</B></TD>
	<TD ALIGN="center" COLSPAN="2"><B>Tax</B></TD>
	<TD ALIGN="center" COLSPAN="2"><B>Shipping</B></TD>
</TR>
<TR>
	<TD><B>referrerid</B></TD>
	<TD ALIGN="center"><B># Orders</B></TD>
	<TD ALIGN="center"><B>Total</B></TD>
	<TD ALIGN="center"><B>Avg</B></TD>
	<TD ALIGN="center"><B>Total</B></TD>
	<TD ALIGN="center"><B>Avg</B></TD>
	<TD ALIGN="center"><B>Total</B></TD>
	<TD ALIGN="center"><B>Avg</B></TD>
</TR>
<%
	while not rsData_Incomplete.eof
%>
<TR>
	<TD><B><%= iif(IsValid(rsData_Incomplete("vchReferalName")),rsData_Incomplete("vchReferalName"),"[No Referral]") %></B></TD>
	<TD ALIGN="center"><%= rsData_Incomplete("intOrderCount") %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData_Incomplete("mnyGrandTotal"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData_Incomplete("mnyGrandTotal") / rsData_Incomplete("intOrderCount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData_Incomplete("mnyTaxAmount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData_Incomplete("mnyTaxAmount") / rsData_Incomplete("intOrderCount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData_Incomplete("mnyShipAmount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData_Incomplete("mnyShipAmount") / rsData_Incomplete("intOrderCount"), 2) %></TD>
</TR>
<%
		rsData_Incomplete.MoveNext
	wend
	%>
<tr>
	<td colspan="8" style="border-bottom:1px solid #ccc;">&nbsp;</td>
</tr>
<TR>
	<TD>&nbsp;</TD>
	<TD ALIGN="center">&nbsp;</TD>
	<TD ALIGN="center" COLSPAN="2"><B>Amount</B></TD>
	<TD ALIGN="center" COLSPAN="2"><B>Tax</B></TD>
	<TD ALIGN="center" COLSPAN="2"><B>Shipping</B></TD>
</TR>
<TR>
	<TD><B>Rname</B></TD>
	<TD ALIGN="center"><B># Orders</B></TD>
	<TD ALIGN="center"><B>Total</B></TD>
	<TD ALIGN="center"><B>Avg</B></TD>
	<TD ALIGN="center"><B>Total</B></TD>
	<TD ALIGN="center"><B>Avg</B></TD>
	<TD ALIGN="center"><B>Total</B></TD>
	<TD ALIGN="center"><B>Avg</B></TD>
</TR>
<%
	
	while not rsCustomData_Incomplete.eof
%>
<TR>
	<TD><B><%= iif(IsValid(rsCustomData_Incomplete("vchReferalName")),rsCustomData_Incomplete("vchReferalName"),"[No Referral]") %></B></TD>
	<TD ALIGN="center"><%= rsCustomData_Incomplete("intOrderCount") %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsCustomData_Incomplete("mnyGrandTotal"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsCustomData_Incomplete("mnyGrandTotal") / rsCustomData_Incomplete("intOrderCount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsCustomData_Incomplete("mnyTaxAmount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsCustomData_Incomplete("mnyTaxAmount") / rsCustomData_Incomplete("intOrderCount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsCustomData_Incomplete("mnyShipAmount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsCustomData_Incomplete("mnyShipAmount") / rsCustomData_Incomplete("intOrderCount"), 2) %></TD>
</TR>
<%
		rsCustomData_Incomplete.MoveNext
	wend
%>
</TABLE>

</FONT></CENTER>
<%
end sub
%>