<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: report_selectdate.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Allow user to select date range for reports.
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_REPORT_VIEW) then
	response.redirect "menu.asp"
end if

dim strPageType, strPageTitle

strPageType = "adminrepselectdate"
strPageTitle = "Report Date Range"

OpenConn

dim dtmStart, dtmEnd, intRptType, strCriteria

' AL 1 is the default for Date Shipped
intRptType = 1

if Request("action") = "submit" then
	
	
	
		if Request("dtmStartSubmit") <> "" then
			dtmStart = Request("dtmStartSubmit")
			dtmEnd = Request("dtmEndSubmit") 
			intRptType = 2
		end if
		
		if Request("dtmStartShip") <> "" then
			dtmStart = Request("dtmStartShip")
			dtmEnd = Request("dtmEndShip") 
			intRptType = 1
		end if
				
		if Request("dtmStartDeposit") <> "" then
			dtmStart = Request("dtmStartDeposit")
			dtmEnd = Request("dtmEndDeposit") 
			intRptType = 3
		end if
		

	
	if IsDate(dtmStart) and IsDate(dtmEnd) then
		dtmStart = DateValue(CDate(dtmStart))
		dtmEnd = DateValue(CDate(dtmEnd))
		if dtmStart <= dtmEnd then
			SetReportDateRangeX dtmStart, dtmEnd & " 11:59:59 PM", intRptType
		end if
	end if
	response.Redirect "report_selectdate.asp"
end if


GetReportDateRangeX dtmStart, dtmEnd, intRptType
dtmEnd = DateValue(dtmEnd)


DrawPage

response.end

sub DrawPage
call DrawHeaderUpdate()
%>

<%= font(5) %><%= strPageTitle %></FONT><BR>
<%= spacer(1,5) %><BR>

<% stlBeginInfoBar INT_PAGE_WIDTH %>
Current <%= strCriteria %>: <B><%= DateValue(dtmStart) %></B> to <B><%= DateValue(dtmEnd) %></B>
<% stlEndInfoBar %>
<%= spacer(1,10) %><BR>
<% stlBeginTableFrame(INT_PAGE_WIDTH) %>
<TABLE BORDER=0 CELLPADDING=4 CELLSPACING=0 WIDTH="100%">

<FORM ACTION="report_selectdate.asp" METHOD="POST" >
<INPUT TYPE="hidden" NAME="action" VALUE="submit" >
<TR>
	<TD>Date Deposited Range:</TD>
	<TD><INPUT TYPE="text" NAME="dtmStartShip" VALUE="<%= iif(intRptType=1,dtmStart,"") %>" SIZE=10> &nbsp;to&nbsp; <INPUT TYPE="text" NAME="dtmEndShip" VALUE="<%= iif(intRptType=1,dtmEnd,"") %>" SIZE=10></TD>
	<TD ALIGN=RIGHT><INPUT TYPE="submit" VALUE="Set" CLASS="button"></TD>
</TR>
</FORM>

<FORM ACTION="report_selectdate.asp" METHOD="POST" >
<INPUT TYPE="hidden" NAME="action" VALUE="submit" >
<TR>
	<TD>Date Submitted Range:</TD>
	<TD><INPUT TYPE="text" NAME="dtmStartSubmit" VALUE="<%= iif(intRptType=2,dtmStart,"") %>" SIZE=10 ID="Text1"> &nbsp;to&nbsp; <INPUT TYPE="text" NAME="dtmEndSubmit" VALUE="<%= iif(intRptType=2,dtmEnd,"") %>" SIZE=10 ID="Text2"></TD>
	<TD ALIGN=RIGHT><INPUT TYPE="submit" VALUE="Set" CLASS="button" ID="Submit1" NAME="Submit1"></TD>
</TR>
</FORM>

</TABLE>
<% stlEndTableFrame %>

<%
end sub
%>