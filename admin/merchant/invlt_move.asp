<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: inv_move.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Move an inventory item or resort items.
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

' script arguments:
'   action = "resort"
'     dir = "up" or "down"
'     parent = parent folder ID
'     source = item ID to resort
'   action = "move"
'     parent = parent folder ID to display
'     source = item ID to move
'   action = "movefinal"
'     source = item ID to move
'     destin = new parent folder ID

CheckAdminLogin

if not CheckUserAccess(ACC_INVENTORY_EDIT) then
	response.redirect "main.asp"
end if

dim strPageType, strPageTitle

strPageType = "admininvlist"
strPageTitle = "Move Inventory Item"

const STR_LIST_SCRIPT = "invlt_list.asp?"
dim STR_MOVE_SCRIPT, STR_MOVE_FINAL_SCRIPT
STR_MOVE_SCRIPT = "invlt_move.asp?action=move&source=" & Request("source") & "&"
STR_MOVE_FINAL_SCRIPT = "invlt_move.asp?action=movefinal&source=" & Request("source") & "&destin=" & Request("parent") & "&"
const STR_DETAIL_FOLDER_SCRIPT = "invlv_fdetail.asp?"
const STR_DETAIL_ITEM_SCRIPT = "invlv_idetail.asp?"
const STR_STOCK_LOW_SCRIPT = "invlv_stock.asp?"
const STR_STOCK_HIGH_SCRIPT = "invlv_stock.asp?"

OpenConn

dim strAction, intSource, intDestin, intParent, strDir
strAction = lcase(Request("action"))
intSource = Request("source")
if IsNumeric(intSource) then
	intSource = CLng(intSource)
else
	intSource = 0
end if
intDestin = Request("destin")
if IsNumeric(intDestin) then
	intDestin = CLng(intDestin)
else
	intDestin = 0
end if
intParent = Request("parent")
strDir = lcase(Request("dir"))

select case strAction
	case "resort":
		ChangeItemSort intParent, intSource, strDir
		response.redirect STR_LIST_SCRIPT & "parentid=" & intParent
	case "movefinal":
		MoveItem intSource, intDestin
		response.redirect STR_LIST_SCRIPT & "parentid=" & intDestin
end select

' get folder list
dim dctNavList, intParentID

' display move page
dim strSQL, rsData

BuildNavList intParent

strSQL = "SELECT intID, chrType, chrStatus, intSortOrder, vchItemName, mnyItemPrice, mnyShipPrice, intStock"
strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY_LONG_TERM
strSQL = strSQL & " WHERE chrStatus<>'D' AND intParentID=" & intParent
strSQL = strSQL & " ORDER BY chrType, intSortOrder"
set rsData = gobjConn.execute(strSQL)


DrawPage


rsData.close
set rsData = nothing
response.end

sub MoveItem(intSource, intDestin)
	dim strSQL, rsData, intNewSort
	strSQL = "SELECT ISNULL(MAX(intSortOrder),0) AS intMaxSortOrder FROM " & STR_TABLE_INVENTORY_LONG_TERM & " WHERE intParentID=" & intDestin
	set rsData = gobjConn.execute(strSQL)
	intNewSort = rsData("intMaxSortOrder") + 1
	rsData.close
	set rsData = nothing
	strSQL = "UPDATE " & STR_TABLE_INVENTORY_LONG_TERM & " SET intParentID=" & intDestin & ",intSortOrder=" & intNewSort & " WHERE intID=" & intSource
	gobjConn.execute(strSQL)
end sub

sub ChangeItemSort(intParent, intSource, strDir)
	dim strSQL, rsData, intTempOrder, intTempID, intOldOrder
	intTempOrder = 0
	strSQL = "SELECT intID,intSortOrder FROM " & STR_TABLE_INVENTORY_LONG_TERM & " WHERE intParentID=" & intParent & " AND chrStatus<>'D' ORDER BY intSortOrder"
	set rsData = gobjConn.execute(strSQL)
	while not rsData.eof
		if rsData("intID") = intSource then
			intOldOrder = rsData("intSortOrder")
			if strDir = "up" then
				rsData.close
				set rsData = nothing
				SetSortOrder intTempID, intOldOrder, intSource, intTempOrder
				exit sub
			else
				rsData.MoveNext
				if not rsData.eof then
					intTempID = rsData("intID")
					intTempOrder = rsData("intSortOrder")
					rsData.close
					set rsData = nothing
					SetSortOrder intTempID, intOldOrder, intSource, intTempOrder
				else
					rsData.close
					set rsData = nothing
				end if
				exit sub
			end if
		end if
		intTempID = rsData("intID")
		intTempOrder = rsData("intSortOrder")
		rsData.MoveNext
	wend
	rsData.close
	set rsData = nothing
end sub

sub SetSortOrder(intID1, intOrder1, intID2, intOrder2)
	dim strSQL
	strSQL = "UPDATE " & STR_TABLE_INVENTORY_LONG_TERM & " SET intSortOrder=" & intOrder1 & " WHERE intID=" & intID1
	gobjConn.execute(strSQL)
	strSQL = "UPDATE " & STR_TABLE_INVENTORY_LONG_TERM & " SET intSortOrder=" & intOrder2 & " WHERE intID=" & intID2
	gobjConn.execute(strSQL)
end sub

sub DrawPage

	call DrawHeaderUpdate()
	
	DrawListTitle
	DrawListNavigation
	DrawListHeader
	while not rsData.eof
		DrawListItem rsData
		rsData.MoveNext
		if not rsData.eof then
			DrawListItemSep
		end if
	wend
	DrawListFooter
end sub

sub DrawListTitle
%>
<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%">
<TR>
	<TD><%= font(5) %><%= strPageTitle %></TD>
	<TD ALIGN=RIGHT><%= font(1) %>
	<% ' page actions %>
	<B><A HREF="<%= STR_MOVE_FINAL_SCRIPT %>">Move Here</A></B>
	&nbsp;
	<A HREF="<%= STR_LIST_SCRIPT %>">Cancel Move</A>
	</TD>
</TR>
</TABLE>
<%= spacer(1,5) %><BR>
<% stlDateBar %>
<%
end sub

sub DrawListNavigation
	' dctNavList - reversed order, key=ID, item=name
	dim c, x, i, k
	x = dctNavList.count - 1
	i = dctNavList.items
	k = dctNavList.keys
	response.write "<TABLE BORDER=0 CELLPADDING=6 CELLSPACING=0 WIDTH=""100%"">"
	response.write "<TR><TD>" & font(1)
	for c = x to 0 step -1
		if c > 0 then
			response.write "<B><A HREF=""" & STR_MOVE_SCRIPT & "parent=" & k(c) & """>" & i(c) & "</A></B> : "
		else
			response.write i(c)
		end if
	next
	response.write "</TD>"
	if x > 0 then
		response.write "<TD ALIGN=RIGHT>" & font(1) & "<A HREF=""" & STR_MOVE_SCRIPT & "parent=" & k(1) & """>To Previous Folder<IMG SRC=""" & imagebase & "icon_folderup.gif"" WIDTH=""16"" HEIGHT=""16"" ALT=""To Previous Folder"" BORDER=""0"" ALIGN=""absmiddle"" HSPACE=""5""></A></TD>"
	end if
	response.write "</TR></TABLE>"
end sub

sub BuildNavList(byVal intParent)
	dim strSQL, rsData
	set dctNavList = Server.CreateObject("Scripting.Dictionary")
	while intParent <> 0
		strSQL = "SELECT intParentID, vchItemName FROM " & STR_TABLE_INVENTORY_LONG_TERM & " WHERE intID=" & intParent & " AND chrStatus<>'D'"
		set rsData = gobjConn.execute(strSQL)
		dctNavList.Add intParent, rsData("vchItemName") & ""
		intParent = rsData("intParentID")
		rsData.close
		set rsData = nothing
	wend
	dctNavList.Add 0, "Top"
end sub

sub DrawListHeader
%>
<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVLtGrey) %>">
<TR>
	<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cWhite %>">
	<TR>
		<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%">
		<TR BGCOLOR="<%= cVLtYellow %>">
			<TD><%= font(1) %>&nbsp;</TD>
			<TD><%= font(1) %><B>Name</B></TD>
			<TD ALIGN="right"><%= font(1) %><B>&nbsp;Price&nbsp;</B></TD>
			<TD ALIGN="right"><%= font(1) %><B>&nbsp;Shipping&nbsp;</B></TD>
			<TD ALIGN="center"><%= font(1) %><B>&nbsp;Qty&nbsp;</B></TD>
			<TD ALIGN="right"><%= font(1) %><B>&nbsp;Status&nbsp;</B></TD>
		</TR>
<%
end sub

sub DrawListItem(rsInput)
	select case rsInput("chrType")
		case "I": ' item
%>
		<TR VALIGN=BOTTOM>
			<TD ALIGN=CENTER><IMG SRC="<%= imagebase %>icon_product.gif" WIDTH="16" HEIGHT="16" BORDER="0"></TD>
			<TD><%= fontx(2,2,cDisabled) %><%= rsInput("vchItemName") %></TD>
			<TD ALIGN="right">&nbsp;<%= fontx(1,1,cLight) & SafeFormatCurrency("n/a", rsInput("mnyItemPrice"), 2) %></FONT>&nbsp;</TD>
			<TD ALIGN="right">&nbsp;<%= fontx(1,1,cLight) & SafeFormatCurrency("n/a", rsInput("mnyShipPrice"), 2) %></FONT>&nbsp;</TD>
			<TD ALIGN="center">&nbsp;<%= fontx(1,1,cLight) & rsInput("intStock") %></FONT>&nbsp;</TD>
			<TD ALIGN="right">&nbsp;<%= font(1) %><%= GetStatusStr(rsInput("chrStatus"), true) %>&nbsp;</TD>
		</TR>
<%
		case "A": ' folder
%>
		<TR VALIGN=BOTTOM>
			<TD ALIGN=CENTER><A HREF="<%= STR_MOVE_SCRIPT %>parent=<%= rsInput("intID") %>"><IMG SRC="<%= imagebase %>icon_folder.gif" WIDTH="16" HEIGHT="16" ALT="Open This Folder" BORDER="0"></A></TD>
			<TD><%= font(2) %><A HREF="<%= STR_MOVE_SCRIPT %>parent=<%= rsInput("intID") %>"><%= rsInput("vchItemName") %></A></TD>
			<TD ALIGN="right">&nbsp;<%= fontx(1,1,cLight) & "n/a</FONT>" %>&nbsp;</TD>
			<TD ALIGN="right">&nbsp;<%= fontx(1,1,cLight) & "n/a</FONT>" %>&nbsp;</TD>
			<TD ALIGN="center">&nbsp;<%= fontx(1,1,cLight) & "n/a</FONT>" %>&nbsp;</TD>
			<TD ALIGN="right">&nbsp;<%= font(1) %><%= GetStatusStr(rsInput("chrStatus"), true) %>&nbsp;</TD>
		</TR>
<%
	end select
end sub

sub DrawListItemSep
%>
		<TR>
			<TD COLSPAN=6><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVVLtGrey) %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
		</TR>
<%
end sub

sub DrawListFooter
%>
		</TABLE></TD>
	</TR>
	</TABLE></TD>
</TR>
</TABLE>
<%
end sub
%>