<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<!--#include file="../config/incSearch.asp"-->
<%

' =====================================================================================
' = File: trans_search.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Merchant Transaction Admin
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_TRANS_VIEW) then
	response.redirect "menu.asp"
end if

dim blnCanViewOrder, blnCanRaw
blnCanViewOrder = CheckUserAccess(ACC_ORDER_VIEW)
blnCanRaw = CheckUserAccess(ACC_RAW)

dim strPageTitle
strPageTitle = "Transaction Search"
const STR_PAGE_TYPE = "admintranslist"

const STR_SEARCH_SCRIPT = "trans_search.asp?"
const STR_DETAIL_ORDER_SCRIPT = "order_detail.asp?"
const STR_DETAIL_TRANS_SCRIPT = "order_transdetail.asp?"

OpenConn

dim strMode, strAction

strMode = lcase(Request("mode"))
if strMode <> "a" and strMode <> "i" then
	strMode = "b"
end if
strAction = lcase(Request("action"))

strSearchSQLPrefix = "SELECT T.*, O.intID AS O_intID"
strSearchSQLPrefix = strSearchSQLPrefix & " FROM " & STR_TABLE_ORDER & " AS O, " & STR_TABLE_TRANS & " AS T"
strSearchSQLPrefix = strSearchSQLPrefix & " WHERE "
strSearchSQLPrefix = strSearchSQLPrefix & " O.chrStatus <> 'D' AND T.chrStatus <> 'D'"
strSearchSQLPrefix = strSearchSQLPrefix & " AND O.intID = T.intOrderID"
strSearchSQLSuffix = " ORDER BY T.dtmTransDate"

Search_AddLabel "Transaction Information"
Search_AddNumberRange "#T.intID", "Record ID"
Search_AddDateRange "%T.dtmCreated", "Date Created"
Search_AddDateRange "%T.dtmUpdated", "Date Updated"
if strMode = "a" then
	Search_AddText "T.vchCreatedByIP", 15, 15, "Created by IP"
	Search_AddText "T.vchUpdatedByIP", 15, 15, "Updated by IP"
	Search_AddText "T.vchCreatedByUser", 15, 32, "Created by User"
	Search_AddText "T.vchUpdatedByUser", 15, 32, "Updated by User"
end if
if strMode = "a" or strMode = "i" then
	Search_AddCheckboxGroup "+T.chrType", 2, dctTransModeValues, "Mode"
	Search_AddCheckboxGroup "+T.chrAccountType", 4, dctTransAcctTypeValues, "Account Type"
	Search_AddCheckboxGroup "+T.chrStatus", 4, dctTransStatusValues, "Status"
	Search_AddCheckboxGroup "+T.chrTransType", 4, dctTransType, "Type"
	Search_AddCheckboxGroup "+T.vchAVCode", 2, dctAVSValues, "Address-Verify Code"
	Search_AddText "T.vchAuthCode", 12, 12, "Authorization Code"
	Search_AddText "T.vchRefCode", 12, 12, "ECHO Reference Code"
	Search_AddText "T.vchOrderCode", 20, 20, "ECHO Order Code"
	Search_AddText "T.vchDeclineCode", 12, 12, "Decline Code"
end if
Search_AddNumberRange "#T.mnyTransAmount", "Transaction Amount"
	
InitSearchEngine

dim strSQL, rsData
if strAction = "search" then
	if strMode = "b" then
		strSQL = BuildBasicSearchSQL()
	else
		strSQL = BuildSearchSQL()
	end if
	if strSQL <> "" then
		set rsData = gobjConn.execute(strSQL)
	else
		strAction = ""
	end if
end if

intCurrentGroup = intNumGroups + 1
if intCurrentGroup > INT_MAX_SEARCH_GROUPS then
	intCurrentGroup = 0
end if

DrawHeader strPageTitle, STR_PAGE_TYPE
if strAction = "search" then
	DrawSearchResults rsData
else
	DrawSearchPage "Transactions"
end if


if IsObject(rsData) then
	rsData.close
	set rsData = nothing
end if
response.end

sub LoadPresetSearch(strPreset)
	strMode = "i"
	select case strPreset
		case "review":
			arySearchGroups(1,0).Add "+T.chrStatus", "n~P+E+V"
		case "48hours":
			arySearchGroups(1,0).Add "T.dtmUpdated", "n~" & DateAdd("h", -48, Now()) & "~"
		case "thisweek":
			arySearchGroups(1,0).Add "T.dtmUpdated", "n~" & DateAdd("d", 1-Weekday(Date()), Date()) & "~"
		case "thismonth":
			arySearchGroups(1,0).Add "T.dtmUpdated", "n~" & Month(Date()) & "/1/" & Year(Date()) & "~"
		case "thisyear":
			arySearchGroups(1,0).Add "T.dtmUpdated", "n~1/1/" & Year(Date()) & "~"
		case "all":
			arySearchGroups(1,0).Add "T.chrStatus", "~D"
		case else:	' invalid preset
			response.redirect "default.asp"
	end select
end sub

sub DrawSearchResults(rsData)
	strPageTitle = "Search Results - Transaction"
	DrawListTitle
	DrawListHeader
	if rsData.eof then
%>
<TR>
	<TD COLSPAN="6" ALIGN="center">No orders were found matching your search.</TD>
</TR>
<%
	else
		while not rsData.eof
			DrawListItem rsData("intID"), rsData("chrTransType"), rsData("chrStatus"), rsData("O_intID"), rsData("mnyTransAmount"), rsData("dtmTransDate"), rsData
			rsData.MoveNext
			if not rsData.eof then
				DrawListItemSep
			end if
		wend
	end if
	DrawListFooter
end sub

sub DrawListTitle
%>
<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%">
<TR>
	<TD><%= font(5) %><%= strPageTitle %></TD>
<% if not blnPrintMode then %>
	<TD ALIGN=RIGHT><%= font(1) %>
	<% ' page actions %>
	<A HREF="<%= STR_SEARCH_SCRIPT %>">Basic Search</A> -
	<A HREF="<%= STR_SEARCH_SCRIPT %>mode=i">Intermediate Search</A><BR>
	<A HREF="<%= STR_SEARCH_SCRIPT %>mode=a">Advanced Search</A>
	</TD>
<% end if %>
</TR>
</TABLE><%= spacer(1,5) %><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cBlue %>">
<% ' info bar %>
<TR>
	<FORM ACTION="<%= STR_SEARCH_SCRIPT %>" METHOD="POST" NAME="frm">
	<TD><%= fontx(1,1,cWhite) %>
	<INPUT TYPE="hidden" NAME="action" VALUE="modify">
	<INPUT TYPE="hidden" NAME="n_groups" VALUE="<%= intNumGroups %>">
	<INPUT TYPE="hidden" NAME="mode" VALUE="<%= strMode %>">
	<INPUT TYPE="hidden" NAME="reset" VALUE="">
	<% if intNumGroups < INT_MAX_SEARCH_GROUPS and strMode <> "b" then %>
	&nbsp;<B><A HREF="#" onClick="frm.reset.value='r'; frm.submit(); return false;">Restrict This Search Further</A></B>
	&nbsp;|&nbsp;
	<B><A HREF="#" onClick="frm.submit(); return false;">Add To This Search</A></B>
	<% end if %>
	<INPUT TYPE="hidden" NAME="c_group" VALUE="0">
	<% SaveOldGroups %>
	</FONT></TD>
	</FORM>
	<TD ALIGN=RIGHT><%= fontx(1,1,cWhite) & FormatDateTime(date,1) %>&nbsp;</TD>
</TR>
</TABLE>
<%
end sub

sub DrawListHeader
%>
<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVLtGrey) %>">
<TR>
	<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cWhite %>">
	<TR>
		<TD><TABLE BORDER=0 CELLPADDING=3 CELLSPACING=0 WIDTH="100%">
		<TR BGCOLOR="<%= cVLtYellow %>">
			<TD>&nbsp;</TD>
			<TD NOWRAP><B>Date</B></TD>
			<TD NOWRAP ALIGN=CENTER><B>Trans ID</B></TD>
			<TD NOWRAP ALIGN=CENTER><B>&nbsp;Type&nbsp;</B></TD>
			<TD NOWRAP ALIGN="right"><B>&nbsp;Amount&nbsp;</B></TD>
			<TD ALIGN=RIGHT><B>Notes</B></TD>
		</TR>
<%
end sub

sub DrawListItem(intID, strType, strStatus, intOrderID, mnyAmount, dtmTransDate, rsTrans)
%>
		<TR VALIGN=BOTTOM>
			<TD><% if blnCanViewOrder then %><A HREF="<%= STR_DETAIL_ORDER_SCRIPT %>id=<%= intOrderID %>"><% end if %><IMG SRC="<%= imagebase %>icon_order.gif" WIDTH="16" HEIGHT="16" ALT="View Order Details" BORDER="0"></A></TD>
			<TD><%= dtmTransDate %></TD>
			<TD ALIGN="center" NOWRAP><% if blnCanRaw then %><A HREF="<%= STR_DETAIL_TRANS_SCRIPT %>id=<%= intID %>">*</A> <% end if %><%= intOrderID %>-<%= intID %></TD>
			<TD ALIGN=CENTER><A HREF="<%= STR_DETAIL_ORDER_SCRIPT %>id=<%= intOrderID %>"><%= GetArrayValue(strType, dctTransType) %></A></TD>
			<TD NOWRAP ALIGN="right">&nbsp;<%= SafeFormatCurrency(fontx(1,1,cLight) & "n/a</FONT>", mnyAmount, 2) %>&nbsp;</TD>
			<TD ALIGN=RIGHT>
			<%
				if strStatus = "A" then
					if not IsNull(rsTrans("vchAuthCode")) then
						response.write "Auth&nbsp;" & rsTrans("vchAuthCode") & " "
					end if
					if not IsNull(rsTrans("vchRefCode")) then
						response.write "Ref&nbsp;" & rsTrans("vchRefCode") & " "
					end if
					if not IsNull(rsTrans("vchOrderCode")) then
						response.write "Order&nbsp;" & rsTrans("vchOrderCode") & " "
					end if
					if not IsNull(rsTrans("vchDeclineCode")) then
						response.write "Decline&nbsp;" & rsTrans("vchDeclineCode") & " "
					end if
				else
					response.write GetArrayValue(strStatus, dctTransStatusValues)
				end if
			%>
			</TD>
		</TR>
<%
end sub

sub DrawListItemSep
%>
		<TR>
			<TD COLSPAN=6><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVVLtGrey) %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
		</TR>
<%
end sub

sub DrawListFooter
%>
		</TABLE></TD>
	</TR>
	</TABLE></TD>
</TR>
</TABLE>
<%
end sub
%>