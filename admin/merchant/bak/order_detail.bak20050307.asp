<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: order_detail.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Display order details.
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_ORDER_VIEW) then
	response.redirect "menu.asp"
end if

const STR_PAGE_TITLE = "Order Details"
const STR_PAGE_TYPE = "adminorderlist"

OpenConn

dim blnCanRaw, blnCanEdit, blnCanApprove, blnCanDecline, blnCanShip, blnCanTrans, blnCanSendReceipt, blnCanShipOption
blnCanRaw = CheckUserAccess(ACC_RAW)
blnCanEdit = CheckUserAccess(ACC_ORDER_EDIT)
blnCanApprove = CheckUserAccess(ACC_ORDER_APPROVE)
blnCanDecline = CheckUserAccess(ACC_ORDER_DECLINE)
blnCanShip = CheckUserAccess(ACC_ORDER_SHIP)
blnCanShipOption = blnCanShip
blnCanSendReceipt = CheckUserAccess(ACC_ORDER_EMAILRECEIPT)
blnCanTrans = CheckUserAccess(ACC_ORDER_TRANS_AUTH) OR CheckUserAccess(ACC_ORDER_TRANS_DEPOSIT) OR CheckUserAccess(ACC_ORDER_TRANS_CREDIT) OR CheckUserAccess(ACC_ORDER_TRANS_MANUAL) OR CheckUserAccess(ACC_ORDER_TRANS_AUTHDEP)

dim intID, strAction
strAction = Request("action")
intID = Request("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if

dim STR_SEARCH_SCRIPT, STR_DETAIL_SCRIPT, STR_EDIT_SCRIPT, STR_EDIT_LINES_SCRIPT
STR_SEARCH_SCRIPT = "order_list.asp?"
STR_DETAIL_SCRIPT = "order_detail.asp?id=" & intID
STR_EDIT_SCRIPT = "order_ordermod.asp?order=" & intID
STR_EDIT_LINES_SCRIPT = "order_itemmod.asp?order=" & intID

select case strAction
	case "createnew":
		' create a new order
		if CheckUserAccess(ACC_ORDER_CREATE) then
			response.redirect "order_detail.asp?id=" & CreateNewOrder_Other("", "9")
		else
			response.redirect "menu.asp"
		end if
	case "recalc":
		' recalc order
		ReCalcOrder_Other intID
		response.redirect "order_detail.asp?id=" & intID
	case "btnAbandon":
		' change status to 'B' (abandoned)
		if blnCanEdit then
			UpdateOrderStatus_Other intID, "B"
		end if
		response.redirect "main.asp"
	case "btnLock":
		if blnCanEdit then
			UpdateOrderStatus_Other intID, "9"
		end if
		response.redirect "order_detail.asp?id=" & intID
	case "btnUnlock":
		if blnCanEdit then
			UpdateOrderStatus_Other intID, "P"
		end if
		response.redirect "order_detail.asp?id=" & intID
	case "btnSubmit":
		' change status to 'S' (submitted)
		if blnCanEdit then
			UpdateOrderStatus_Other intID, "S"
		end if
		response.redirect "order_detail.asp?id=" & intID
	case "btnApprove":
		' change status to 'A' (approved)
		if blnCanApprove then
			UpdateOrderStatus_Other intID, "A"
			ProcessOrderApprove intID
		end if
		response.redirect "order_detail.asp?id=" & intID
	case "btnDecline":
		' change status to 'K' (declined)
		if blnCanDecline then
			UpdateOrderStatus_Other intID, "K"
		end if
		response.redirect "order_detail.asp?id=" & intID
	case "btnShip":
		' redirect to shipping form
		if blnCanShip then
			response.redirect "order_ship.asp?id=" & intID
		else
			response.redirect "menu.asp"
		end if
	case "btnTrans":
		' redirect to transaction form
		if blnCanTrans then
			response.redirect "order_trans.asp?id=" & intID
		else
			response.redirect "menu.asp"
		end if
'	case "btnModifyOrder":
		' redirect to order mod form
'		response.redirect "order_ordermod.asp?order=" & intID
	case "btnReceipt":
		if blnCanSendReceipt then
			response.redirect "order_emailinvoice.asp?id=" & intiD
		else
			response.redirect "menu.asp"
		end if
	case "btnSystemEdit"
		' redirect to system edit form
		if blnCanRaw then
			response.redirect "order_sysedit.asp?id=" & intID
		else
			response.redirect "menu.asp"
		end if
end select

dim strSQL, rsData
if intID <= 0 then
	response.redirect STR_SEARCH_SCRIPT
end if

dim strStatusClause
strStatusClause = UserAccessStatus_BuildSQLWhere()
if strStatusClause = "" then
	response.redirect STR_SEARCH_SCRIPT
end if
strSQL = "SELECT S.vchFirstName + ' ' + S.vchLastName AS S_vchShopperAccount, O.* FROM " & STR_TABLE_ORDER & " AS O LEFT JOIN " & STR_TABLE_SHOPPER & " AS S ON O.intShopperID = S.intID WHERE O.intID=" & intID & " AND O.chrStatus<>'D' AND (S.chrType='A' OR S.chrType IS NULL)"
strSQL = strSQL & " AND O.chrStatus IN (" & strStatusClause & ")"
set rsData = gobjConn.execute(strSQL)
if rsData.eof then
	rsData.close
	set rsData = nothing
	response.redirect STR_SEARCH_SCRIPT
end if


DrawPage rsData
if (Request.QueryString("print").count > 0) then
%>
	<SCRIPT LANGUAGE="JavaScript"><!--
	// print the window
	window.print();
// --></SCRIPT>
<%
end if


rsData.close
set rsData = nothing

response.end

sub DrawPage(rsInput)
	dim rsLineItem, rsShopper, strColor
	dim blnErrors, dctErrorList
	set dctErrorList = Server.CreateObject("Scripting.Dictionary")
	blnErrors = ValidateOrder_Other(intID, dctErrorList)
	' menu options based on current order status
	dim dctMenuList
	set dctMenuList = Server.CreateObject("Scripting.Dictionary")
	select case rsInput("chrStatus")
		case "0","1","2","3","4","5","6","7","8": ' in progress
			if blnCanEdit then
				dctMenuList.Add "btnAbandon", "Abandon Order"
				dctMenuList.Add "btnLock", "Lock Order"
			end if
		case "9": ' in progress by merchant
			if blnCanEdit then
				dctMenuList.Add "btnAbandon", "Abandon Order"
				if blnErrors then
					dctMenuList.Add "btnSubmit", "Submit Order"
				end if
				if not IsNull(rsInput("intShopperID")) then
					dctMenuList.Add "btnUnlock", "Unlock Order"
				end if
			end if
		case "P": ' pending
			if blnCanEdit then
				dctMenuList.Add "btnAbandon", "Abandon Order"
				dctMenuList.Add "btnLock", "Lock Order"
			end if
		case "S", "V": ' submitted or voice-auth
			if blnCanEdit then
				dctMenuList.Add "btnAbandon", "Abandon Order"
			end if
			if blnCanApprove and blnErrors then
				dctMenuList.Add "btnApprove", "Approve Order"
			end if
			if blnCanDecline then
				dctMenuList.Add "btnDecline", "Decline Order"
			end if
			if blnCanShip and blnErrors then
				dctMenuList.Add "btnShip", "Ship Order"
			end if
			if blnCanTrans and blnErrors then
				dctMenuList.Add "btnTrans", "Perform Transaction"
			end if
			if blnCanSendReceipt then
				dctMenuList.Add "btnReceipt", "Resend Receipt"
			end if
		case "A": ' approved
			if blnCanEdit then
				dctMenuList.Add "btnAbandon", "Abandon Order"
			end if
			if blnCanDecline then
				dctMenuList.Add "btnDecline", "Decline Order"
			end if
			if blnCanShip then
				dctMenuList.Add "btnShip", "Ship Order"
			end if
			if blnCanTrans and blnErrors then
				dctMenuList.Add "btnTrans", "Perform Transaction"
			end if
			if blnCanSendReceipt then
				dctMenuList.Add "btnReceipt", "Resend Receipt"
			end if
		case "Z": ' authorized
			if blnCanEdit then
				dctMenuList.Add "btnAbandon", "Abandon Order"
			end if
			if blnCanDecline then
				dctMenuList.Add "btnDecline", "Decline Order"
			end if
			if blnCanShip then
				dctMenuList.Add "btnShip", "Ship Order"
			end if
			if blnCanTrans and blnErrors then
				dctMenuList.Add "btnTrans", "Perform Transaction"
			end if
			if blnCanSendReceipt then
				dctMenuList.Add "btnReceipt", "Resend Receipt"
			end if
		case "X": ' deposited
			if blnCanShip and blnErrors then
				if not IsValid(rsInput("vchShippingNumber")) then
					dctMenuList.Add "btnShip", "Ship Order"
				end if
			end if
			if blnCanTrans and blnErrors then
				dctMenuList.Add "btnTrans", "Perform Transaction"
			end if
			if blnCanSendReceipt then
				dctMenuList.Add "btnReceipt", "Resend Receipt"
			end if
		case "C": ' credited - order locked
			if blnCanSendReceipt then
				dctMenuList.Add "btnReceipt", "Resend Receipt"
			end if
		case "H": ' shipped - allow returns and additional line item notes
			if blnCanTrans and blnErrors then
				dctMenuList.Add "btnTrans", "Perform Transaction"
			end if
			if blnCanSendReceipt then
				dctMenuList.Add "btnReceipt", "Resend Receipt"
			end if
		case "B": ' abandoned - order locked
		case "K": ' declined
			if blnCanDecline then
				dctMenuList.Add "btnSubmit", "Submit Order" ' can "un-decline"
			end if
			if blnCanApprove and blnErrors then
				dctMenuList.Add "btnApprove", "Approve Order"
			end if
			if blnCanTrans and blnErrors then
				dctMenuList.Add "btnTrans", "Perform Transaction"
			end if
			if blnCanSendReceipt then
				dctMenuList.Add "btnReceipt", "Resend Receipt"
			end if
			if blnCanEdit then
				dctMenuList.Add "btnLock", "Allow Changes"
			end if
		case "E": ' error - order locked until sysadmin intervention
		case else: ' unknown status
	end select
	if blnCanRaw then
		dctMenuList.Add "btnSystemEdit", "System View"
		dctMenuList.Add "recalc", "Recalc Order"
	end if
	
	dim strStatus, strStatusText, strStatusCode
	strStatusCode = rsInput("chrStatus")
	strStatus = GetArrayValue(strStatusCode, dctOrderStatus)
	select case rsInput("chrStatus")
		case "0","1","2","3","4","5","6","7","8", "P": ' in progress
			strStatusText = "This order has not yet been submitted by the shopper. Changes can not be made unless the order is ""locked""."
		case "9": ' in progress by merchant
			strStatusText = "This order is being created or modified by the merchant, and has been locked out from the shopper's account."
		case "S":	' submitted
			strStatusText = "This order has been submitted for approval."
		case "A":	' approved
			strStatusText = "This order has been approved for processing."
		case "B":	' abandoned
			strStatusText = "This order has been abandoned."
		case "H":	' deposited
			strStatusText = "This order has been shipped."
		case "Z":	' authorized
			strStatusText = "Funds have not yet been deposited."
		case "X":	' deposited
			strStatusText = "Funds have been deposited and this order has been shipped."
		case "C":	' credited
			strStatusText = "Funds have been returned to purchaser."
		case "K":	' declined
		case "V":	' voice-auth required
			strStatusText = "The credit card issuer has requested verbal authorization."
		case "E":	' error
			strStatusText = "A system error occurred while processing this order. Please contact technical support."
	end select
	
	dim blnCanEditAccount, blnCanEditPayment, blnCanEditBilling, blnCanEditShipping, blnCanEditItems, blnCanEditShipOption
	if strStatusCode = "9" then
		blnCanEditAccount = blnCanEdit
		blnCanEditPayment = blnCanEdit
		blnCanEditBilling = blnCanEdit
		blnCanEditShipping = blnCanEdit
		blnCanEditItems = blnCanEdit
	else
		blnCanEditAccount = false
		blnCanEditPayment = (InStr("SVAZ", strStatusCode) > 0) and not IsNull(rsInput("intShopperID")) and CheckUserAccess(ACC_ORDER_PAYMENT_EDIT)
		blnCanEditBilling = (InStr("SVAZ", strStatusCode) > 0) and not IsNull(rsInput("intShopperID")) and CheckUserAccess(ACC_ORDER_BILLING_EDIT)
		blnCanEditShipping = (InStr("SVAZ", strStatusCode) > 0) and not IsNull(rsInput("intShopperID")) and CheckUserAccess(ACC_ORDER_SHIPPING_EDIT)
		blnCanEditItems = (InStr("SVAZXH", strStatusCode) > 0) and (CheckUserAccess(ACC_ORDER_ITEMS_EDIT) or CheckUserAccess(ACC_ORDER_NOTES_ADD) or CheckUserAccess(ACC_ORDER_RETURNS_ADD))
	end if
	
	blnCanEditBilling = blnCanEditBilling AND NOT dctErrorList.Exists("account")
	blnCanEditShipping = blnCanEditShipping AND NOT dctErrorList.Exists("account")
	blnCanEditPayment = blnCanEditPayment AND NOT dctErrorList.Exists("billing")
	blnCanEditShipOption = blnCanEditShipping AND NOT dctErrorList.Exists("shipping")
	if (Request.QueryString("print").count > 0) then
%>
	<div style="width:500;align:right;float:right;">
		<a href="<%= nonsecurebase %>store/"><img src="<%= g_imagebase %>banners/banner-sm2.jpg" border="0" /></a>
	</div>
<%
	end if
	DrawTitle "Order Details - " & STR_MERCHANT_TRACKING_PREFIX & rsInput("intID"), dctMenuList
%>
<CENTER>
<FONT SIZE="2"><B><%= STR_MERCHANT_NAME %></B></FONT><BR>
<%= STR_MERCHANT_ADDRESS1 & iif(STR_MERCHANT_ADDRESS2 <> "", ", " & STR_MERCHANT_ADDRESS2, "") %><BR>
<%= STR_MERCHANT_CITY & ", " & STR_MERCHANT_STATE & " " & STR_MERCHANT_ZIP %><BR>
<%= "Phone " & STR_MERCHANT_PHONE & "&nbsp;&nbsp;Fax " & STR_MERCHANT_FAX %>
</CENTER>
<BR>
<% stlBeginStdTable "100%" %>

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
<TR BGCOLOR="<%= cVLtYellow %>">
	<TD><B>Created:</B></TD>
	<TD><B>Submitted:</B></TD>
	<TD><B>Updated:</B></TD>
</TR>
<TR>
	<TD>&nbsp;&nbsp;<%= FormatDateTimeNoSeconds(rsInput("dtmCreated")) %>&nbsp;&nbsp;&nbsp;</TD>
	<TD>&nbsp;&nbsp;<%= FormatDateTimeNoSeconds(rsInput("dtmSubmitted")) %>&nbsp;&nbsp;&nbsp;</TD>
	<TD>&nbsp;&nbsp;<%= FormatDateTimeNoSeconds(rsInput("dtmUpdated")) %>&nbsp;&nbsp;&nbsp;</TD>
</TR>
<TR>
	<TD COLSPAN="3">&nbsp;</TD>
</TR>
<TR BGCOLOR="<%= cVLtYellow %>">
	<TD><B>Tracking Number:</B></TD>
	<TD <% if dctErrorList.Exists("account") then %>BGCOLOR="<%= cErrorBG %>"<% end if %>><B>Account:</B><% if blnCanEditAccount then %>&nbsp;&nbsp;&nbsp;(<B><A HREF="<%= STR_EDIT_SCRIPT %>&action=step0">Change</A></B>)<% end if %></TD>
	<TD><B>Shipping/Conf #:</B></TD>
</TR>
<TR>
	<TD>&nbsp;&nbsp;<%= STR_MERCHANT_TRACKING_PREFIX & rsInput("intID") %>&nbsp;&nbsp;&nbsp;</TD>
	<TD>&nbsp;&nbsp;<%= rsInput("S_vchShopperAccount") %>&nbsp;&nbsp;&nbsp;</TD>
	<TD>&nbsp;&nbsp;<%= rsInput("vchShippingNumber") %>&nbsp;&nbsp;&nbsp;</TD>
</TR>
</TABLE>
<BR>
<TABLE CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
<TR BGCOLOR="<%= cVLtYellow %>">
	<TD><B>Status:</B></TD>
</TR>
<TR>
	<TD>&nbsp;&nbsp;<%= strStatus & " - " & strStatusText %></TD>
</TR>
</TABLE>
<BR>
<% if CheckUserAccess(ACC_ORDER_PAYMENT_VIEW) then %>
<TABLE CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
<TR BGCOLOR="<%= cVLtYellow %>">
	<TD <% if dctErrorList.Exists("payment") then %>BGCOLOR="<%= cErrorBG %>"<% end if %>><B>Payment Information:</B><% if blnCanEditPayment then %>&nbsp;&nbsp;&nbsp;(<B><A HREF="<%= STR_EDIT_SCRIPT %>&action=step4">Change</A></B>)<% end if %></TD>
</TR>
<TR>
	<TD>
<%
		response.write "&nbsp;&nbsp;" & GetArrayValue(rsInput("chrPaymentMethod"), dctPaymentMethod)
		if right(rsInput("chrPaymentMethod"), 2) = "CC" then
			if not IsNull(rsInput("vchPaymentCardNumber")) then
				response.write " " & rsInput("vchPaymentCardType") & " " & GetProtectedCardNumberX(rsInput("vchPaymentCardNumber")) & " (exp " & rsInput("chrPaymentCardExpMonth") & "/" & rsInput("chrPaymentCardExpyear") & ")"
			end if
		elseif right(rsInput("chrPaymentMethod"), 2) = "EC" then
			response.write " " & rsInput("vchPaymentBankName") & " " & rsInput("vchPaymentRtnNumber") & "-" & GetProtectedCardNumberX(rsInput("vchPaymentAcctNumber")) & " " & rsInput("vchPaymentCheckNumber")
		end if
		response.write "<BR>"
		if CheckUserAccess(ACC_ORDER_TRANS_VIEW) then
			strSQL = "SELECT * FROM " & STR_TABLE_TRANS & " WHERE chrStatus<>'D' AND intOrderID=" & rsInput("intID") & " ORDER BY intID"
			set rsLineItem = gobjConn.execute(strSQL)
			while not rsLineItem.eof
				response.write "&nbsp;&nbsp;" 
				if blnCanRaw then
					response.write "<B><A HREF=""order_transdetail.asp?order=" & intID & "&id=" & rsLineItem("intID") & """>*</A></B> "
				end if
				response.write GetArrayValue(rsLineItem("chrTransType"), dctTransType) & " "
				response.write SafeFormatCurrency("", rsLineItem("mnyTransAmount"), 2) & " "
				response.write rsLineItem("dtmTransDate") & " "
				if rsLineItem("chrStatus") = "A" then
					if not IsNull(rsLineItem("vchAuthCode")) then
						response.write "A" & rsLineItem("vchAuthCode") & " "
					end if
					if not IsNull(rsLineItem("vchAVCode")) then
						response.write "AVS&nbsp;" & rsLineItem("vchAVCode") & " "
					end if
					if not IsNull(rsLineItem("vchRefCode")) then
						response.write "Ref&nbsp;" & rsLineItem("vchRefCode") & " "
					end if
					if not IsNull(rsLineItem("vchDeclineCode")) then
						response.write "Decline&nbsp;" & rsLineItem("vchDeclineCode") & " "
					end if
					if not IsNull(rsLineItem("vchOrderCode")) then
						response.write "Order&nbsp;" & rsLineItem("vchOrderCode") & " "
					end if
				else
					response.write "<B>" & GetArrayValue(rsLineItem("chrStatus"), dctTransStatusValues) & "</B>"
				end if
				response.write "<BR>"
				if not IsNull(rsLineItem("vchAVCode")) then
					response.write "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Address Verification Result: " & GetArrayValue(rsLineItem("vchAVCode"), dctAVSValues) & "<BR>"
				end if
				rsLineItem.MoveNext
			wend
			rsLineItem.close
			set rsLineItem = nothing
		end if
%>
	</TD>
</TR>
</TABLE>
<BR>
<%
	end if
	dim blnCanViewBilling, blnCanViewShipping
	blnCanViewBilling = CheckUserAccess(ACC_ORDER_BILLING_VIEW)
	blnCanViewShipping = CheckUserAccess(ACC_ORDER_SHIPPING_VIEW)
	if blnCanViewBilling or blnCanViewShipping then
%>
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
<TR>
	<TD VALIGN="top" WIDTH="50%">
<% if blnCanViewBilling then %>
	<TABLE CELLPADDING="0" CELLSPACING="0" WIDTH="100%">		<% 'iif(blnCanViewShipping, "90%", "100%") %>
	<TR BGCOLOR="<%= cVLtYellow %>">
		<TD <% if dctErrorList.Exists("billing") then %>BGCOLOR="<%= cErrorBG %>"<% end if %>><B>Bill To:</B><% if blnCanEditBilling then %>&nbsp;&nbsp;&nbsp;(<B><A HREF="<%= STR_EDIT_SCRIPT %>&action=step1">Change</A></B>)<% end if %></TD>
	</TR>
	<TR>
		<TD>
<%
		if isNull(rsInput("intBillShopperID")) or IsNull(rsInput("intShopperID")) then
			response.write "&nbsp;&nbsp;no information"
		else
			strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & rsInput("intBillShopperID") & " AND chrStatus<>'D' AND chrType='B' AND intShopperID=" & rsInput("intShopperID")
			
			set rsShopper = gobjConn.execute(strSQL)
			if rsShopper.eof then
				response.write "&nbsp;&nbsp;<B>System error</B>--unable to retrieve shopper information."
			else
				response.write "&nbsp;&nbsp;" & rsShopper("vchLastName") & ", " & rsShopper("vchFirstName") & "<BR>"
				if not IsNull(rsShopper("vchCompany")) then
					response.write "&nbsp;&nbsp;" & rsShopper("vchCompany") & "<BR>"
				end if
				if not IsNull(rsShopper("vchAddress1")) then
					response.write "&nbsp;&nbsp;" & rsShopper("vchAddress1") & "<BR>"
				end if
				if not IsNull(rsShopper("vchAddress2")) then
					response.write "&nbsp;&nbsp;" & rsShopper("vchAddress2") & "<BR>"
				end if
				response.write "&nbsp;&nbsp;" & rsShopper("vchCity") & ", " & rsShopper("vchState") & " " & rsShopper("vchZip") & "<BR>"
				if not IsNull(rsShopper("vchCountry")) then
					response.write "&nbsp;&nbsp;" & rsShopper("vchCountry") & "<BR>"
				end if
				if not IsNull(rsShopper("vchDayPhone")) then
					response.write "&nbsp;&nbsp;Day: " & rsShopper("vchDayPhone") & "<BR>"
				end if
				if not IsNull(rsShopper("vchNightPhone")) then
					response.write "&nbsp;&nbsp;Night: " & rsShopper("vchNightPhone") & "<BR>"
				end if
				if not IsNull(rsShopper("vchFax")) then
					response.write "&nbsp;&nbsp;Fax: " & rsShopper("vchFax") & "<BR>"
				end if
				if not IsNull(rsShopper("vchEmail")) then
					response.write "&nbsp;&nbsp;Email: " & rsShopper("vchEmail") & "<BR>"
				end if
			end if
			rsShopper.close
			set rsShopper = nothing
		end if
%>
		</TD>
	</TR>
	</TABLE>
<% else %>
	&nbsp;&nbsp;
<% end if %>
	</TD>
	<TD VALIGN="top" WIDTH="50%" ALIGN="right">
<% if blnCanViewShipping then %>
	<TABLE CELLPADDING="0" CELLSPACING="0" WIDTH="100%">		<% 'iif(blnCanViewBilling, "90%", "100%") %>
	<TR BGCOLOR="<%= cVLtYellow %>">
		<TD <% if dctErrorList.Exists("shipping") then %>BGCOLOR="<%= cErrorBG %>"<% end if %>><B>Ship To:</B><% if blnCanEditShipping then %>&nbsp;&nbsp;&nbsp;(<B><A HREF="<%= STR_EDIT_SCRIPT %>&action=step2">Change</A></B>)<% end if %></TD>
	</TR>
	<TR>
		<TD>
<%
		if IsNull(rsInput("intShipShopperID")) or isNull(rsInput("intShopperID")) then
			response.write "&nbsp;&nbsp;no information"
		else
			strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & rsInput("intShipShopperID") & " AND chrStatus<>'D' and (chrType='S' OR chrType='B') and intShopperID=" & rsInput("intShopperID")
			set rsShopper = gobjConn.execute(strSQL)
			if rsShopper.eof then
				response.write "&nbsp;&nbsp;<B>System error</B>--unable to retrieve shopper information."
			else
				response.write "&nbsp;&nbsp;" & rsShopper("vchLastName") & ", " & rsShopper("vchFirstName") & "<BR>"
				if not IsNull(rsShopper("vchCompany")) then
					response.write "&nbsp;&nbsp;" & rsShopper("vchCompany") & "<BR>"
				end if
				if not IsNull(rsShopper("vchAddress1")) then
					response.write "&nbsp;&nbsp;" & rsShopper("vchAddress1") & "<BR>"
				end if
				if not IsNull(rsShopper("vchAddress2")) then
					response.write "&nbsp;&nbsp;" & rsShopper("vchAddress2") & "<BR>"
				end if
				response.write "&nbsp;&nbsp;" & rsShopper("vchCity") & ", " & rsShopper("vchState") & " " & rsShopper("vchZip") & "<BR>"
				if not IsNull(rsShopper("vchCountry")) then
					response.write "&nbsp;&nbsp;" & rsShopper("vchCountry") & "<BR>"
				end if
				if not IsNull(rsShopper("vchDayPhone")) then
					response.write "&nbsp;&nbsp;Day: " & rsShopper("vchDayPhone") & "<BR>"
				end if
				if not IsNull(rsShopper("vchNightPhone")) then
					response.write "&nbsp;&nbsp;Night: " & rsShopper("vchNightPhone") & "<BR>"
				end if
				if not IsNull(rsShopper("vchFax")) then
					response.write "&nbsp;&nbsp;Fax: " & rsShopper("vchFax") & "<BR>"
				end if
				if not IsNull(rsShopper("vchEmail")) then
					response.write "&nbsp;&nbsp;Email: " & rsShopper("vchEmail") & "<BR>"
				end if
			end if
			rsShopper.close
			set rsShopper = nothing
		end if
%>
		</TD>
	</TR>
	</TABLE>
<% else %>
	&nbsp;&nbsp;
<% end if %>
	</TD>
</TR>
</TABLE>
<BR>
<%
	end if
	if true then	' - kill gift message
%>
<TABLE CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
<%
if not IsNull(rsInput("vchReferalName")) and false then
	response.write "<TR><TD>Referral: " & rsInput("vchReferalName") & "</TD></TR>"
end if
%>
<%
	if not IsNull(rsInput("vchCustomReferralName")) then
%>
<TR BGCOLOR="<%= cVLtYellow %>">
	<TD><B>Referred By:</B></TD>
</TR>
<tr>
	<td><%=rsInput("vchCustomReferralName") & ""%></td>
</tr>
<%
	end if
%>
<TR BGCOLOR="<%= cVLtYellow %>">
	<TD <% if dctErrorList.Exists("giftmsg") then %>BGCOLOR="<%= cErrorBG %>"<% end if %>><B>Comments:</B><% if blnCanEditShipOption then %>&nbsp;&nbsp;&nbsp;(<B><A HREF="<%= STR_EDIT_SCRIPT %>&action=step5">Change</A></B>)<% end if %></TD>
</TR>

<TR>
	<TD><%= replace(Server.HTMLEncode(rsInput("txtGiftMessage") & ""), vbCR, "<BR>") %></TD>
</TR>
</TABLE>
<BR>
<%
	end if 
%>

<% if blnCanEditShipOption then %>(<B><A HREF="<%= STR_EDIT_SCRIPT %>&action=step3">Change Shipping Option</A></B>)<BR><% end if %>
<% if blnCanEditItems then %>(<B><A HREF="<%= STR_EDIT_LINES_SCRIPT %>">Modify Order</A></B>)<BR><% end if %>
<%
'	strSQL = "SELECT vchPartNumber, vchItemName, mnyUnitPrice, intQuantity, (mnyUnitPrice * intQuantity) AS mnyPrice FROM " & STR_TABLE_LINEITEM & " WHERE chrStatus<>'D' AND intOrderID=" & rsInput("intID") & " ORDER BY intSortOrder,intID"
'	set rsLineItem = gobjConn.execute(strSQL)
	set rsLineItem = GetOrderLineItems_Other(intID)
	Merchant_DrawOrderCart rsInput, rsLineItem, false, "", "", "", iif(dctErrorList.Exists("items"), cErrorBG, ""), iif(dctErrorList.Exists("shipoption"), cErrorBG, "")
	rsLineItem.close
	set rsLineItem = nothing
	
	stlEndStdTable
end sub

sub DrawTitle(strPageTitle, dctMenuList)
	dim s, c, x, i, k
	x = dctMenuList.count - 1
	i = dctMenuList.items
	k = dctMenuList.keys
'	s = ""
	s = "&nbsp;<a href=""" & STR_SEARCH_SCRIPT & """><b>Back</b></a>"
	for c = 0 to x
		if c >= 0 then
			s = s & "&nbsp;|"
		end if
		s = s & "&nbsp;<A HREF=""" & STR_DETAIL_SCRIPT & "&action=" & k(c) & """><B>" & i(c) & "</B></A>"
	next
	DrawFormHeader "100%", strPageTitle, s
end sub


' -----------------------------------
' ====== PROCESS ORDER APPROVE ======
' -----------------------------------

sub ProcessOrderApprove(intID)
	gintOrderID = intID
	' process flow:
	'  (1) set order status to 'approved'
	'  (2) display/email invoice (unless declined or rejected)

	' SWSCodeChange - 7/20 - added merchant email
	dim strMerchantMessage, strSubject
	strMerchantMessage = "An order has been approved. Please review the order by visiting your merchant website." & vbcrlf
	strMerchantMessage = strMerchantMessage & vbcrlf
	strMerchantMessage = strMerchantMessage & "Order # "& STR_MERCHANT_TRACKING_PREFIX & gintOrderID & vbcrlf
	strMerchantMessage = strMerchantMessage & "Merchant Website: " & nonsecurebase & "admin/" & vbcrlf
	strMerchantMessage = strMerchantMessage & vbcrlf
	if lcase(strServerHost) = "awsdev" then
		AddStdEmailSecurity strMerchantMessage
	end if
	strSubject = "Order Notification # " & STR_MERCHANT_TRACKING_PREFIX & gintOrderID & ""
	SendMerchantEmail strSubject, strMerchantMessage		' SWSCodeChange - 7/20 - added merchant email

end sub
%>