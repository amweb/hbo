<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: report_selectdate.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Allow user to select date range for reports.
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_REPORT_VIEW) then
	response.redirect "menu.asp"
end if

dim strPageType, strPageTitle

strPageType = "adminrepselectdate"
strPageTitle = "Report Date Range"

OpenConn

dim dtmStart, dtmEnd

if Request("action") = "submit" then
	dtmStart = Request("dtmStart")
	dtmEnd = Request("dtmEnd") 
	if IsDate(dtmStart) and IsDate(dtmEnd) then
		dtmStart = DateValue(CDate(dtmStart))
		dtmEnd = DateValue(CDate(dtmEnd))
		if dtmStart <= dtmEnd then
			SetReportDateRange dtmStart, dtmEnd & " 11:59:59 PM"
		end if
	end if
end if

GetReportDateRange dtmStart, dtmEnd
dtmEnd = DateValue(dtmEnd)


DrawPage

response.end

sub DrawPage
%>
<FORM ACTION="report_selectdate.asp" METHOD="POST">
<INPUT TYPE="hidden" NAME="action" VALUE="submit">
<%= font(5) %><%= strPageTitle %></FONT><BR>
<%= spacer(1,5) %><BR>

<% stlBeginInfoBar INT_PAGE_WIDTH %>
Current Date Range: <B><%= DateValue(dtmStart) %></B> to <B><%= DateValue(dtmEnd) %></B>
<% stlEndInfoBar %>
<%= spacer(1,10) %><BR>
<% stlBeginTableFrame(INT_PAGE_WIDTH) %>
<TABLE BORDER=0 CELLPADDING=4 CELLSPACING=0 WIDTH="100%">
<TR>
	<TD>New Range:</TD>
	<TD><INPUT TYPE="text" NAME="dtmStart" VALUE="<%= dtmStart %>" SIZE=10> &nbsp;to&nbsp; <INPUT TYPE="text" NAME="dtmEnd" VALUE="<%= dtmEnd %>" SIZE=10></TD>
	<TD ALIGN=RIGHT><INPUT TYPE="submit" VALUE="Submit" CLASS="button"></TD>
</TR>
</TABLE>
<% stlEndTableFrame %>
</FORM>
<%
end sub
%>