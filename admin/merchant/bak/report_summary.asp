<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: report_summary.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Generate system summary report
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_REPORT_VIEW) then
	response.redirect "menu.asp"
end if

dim strPageType, strPageTitle

strPageType = "adminrepsummary"
strPageTitle = "Order Summary"

OpenConn

dim strSQL, rsData, dtmStart, dtmEnd
GetReportDateRange dtmStart, dtmEnd

dim intTotalOrders, mnyTotalOrders
dim intShippedOrders, mnyShippedOrders
dim intTotalAuth, mnyTotalAuth
dim intTotalDeposit, mnyTotalDeposit
dim intTotalCredit, mnyTotalCredit
dim intTotalDecline, mnyTotalDecline
dim intShippedProducts, intNewShoppers, intOldShoppers

' gather statistics	- only show deposited or shipped order
strSQL = "SELECT COUNT(*) AS intCount, ISNULL(SUM(mnyGrandTotal),0) AS mnyTotal FROM " & STR_TABLE_ORDER & " WHERE chrStatus IN ('S','A','Z','X','C','H','K','E','R','V') AND dtmCreated >= '" & dtmStart & "' AND dtmCreated <= '" & dtmEnd & "'"
set rsData = gobjConn.execute(strSQL)
intTotalOrders = rsData("intCount")
mnyTotalOrders = rsData("mnyTotal")
rsData.close
set rsData = nothing
strSQL = "SELECT COUNT(*) AS intCount, ISNULL(SUM(mnyGrandTotal),0) AS mnyTotal FROM " & STR_TABLE_ORDER & " WHERE chrStatus IN ('H','X') AND dtmShipped >= '" & dtmStart & "' AND dtmShipped <= '" & dtmEnd & "'"
set rsData = gobjConn.execute(strSQL)
intShippedOrders = rsData("intCount")
mnyShippedOrders = rsData("mnyTotal")
rsData.close
set rsData = nothing
strSQL = "SELECT COUNT(*) AS intCount, ISNULL(SUM(mnyTransAmount),0) AS mnyTotal FROM " & STR_TABLE_TRANS & " WHERE chrStatus='A' AND chrTransType IN ('AS','AV') AND dtmCreated >= '" & dtmStart & "' AND dtmCreated <= '" & dtmEnd & "'"
set rsData = gobjConn.execute(strSQL)
intTotalAuth = rsData("intCount")
mnyTotalAuth = rsData("mnyTotal")
rsData.close
set rsData = nothing
strSQL = "SELECT COUNT(*) AS intCount, ISNULL(SUM(mnyTransAmount),0) AS mnyTotal FROM " & STR_TABLE_TRANS & " WHERE chrStatus='A' AND chrTransType IN ('ES','EV','DS') AND dtmCreated >= '" & dtmStart & "' AND dtmCreated <= '" & dtmEnd & "'"
set rsData = gobjConn.execute(strSQL)
intTotalDeposit = rsData("intCount")
mnyTotalDeposit = rsData("mnyTotal")
rsData.close
set rsData = nothing
strSQL = "SELECT COUNT(*) AS intCount, ISNULL(SUM(mnyTransAmount),0) AS mnyTotal FROM " & STR_TABLE_TRANS & " WHERE chrStatus='A' AND chrTransType='CR' AND dtmCreated >= '" & dtmStart & "' AND dtmCreated <= '" & dtmEnd & "'"
set rsData = gobjConn.execute(strSQL)
intTotalCredit = rsData("intCount")
mnyTotalCredit = rsData("mnyTotal")
rsData.close
set rsData = nothing
strSQL = "SELECT COUNT(*) AS intCount, ISNULL(SUM(mnyTransAmount),0) AS mnyTotal FROM " & STR_TABLE_TRANS & " WHERE chrStatus in ('K','V') AND dtmCreated >= '" & dtmStart & "' AND dtmCreated <= '" & dtmEnd & "'"
set rsData = gobjConn.execute(strSQL)
intTotalDecline = rsData("intCount")
mnyTotalDecline = rsData("mnyTotal")
rsData.close
set rsData = nothing
strSQL = "SELECT SUM(L.intQuantity) AS intCount FROM " & STR_TABLE_ORDER & " AS O, " & STR_TABLE_LINEITEM & " AS L WHERE O.chrStatus IN ('X','H') AND O.intID=L.intOrderID AND O.dtmShipped >= '" & dtmStart & "' AND O.dtmShipped <= '" & dtmEnd & "'"
set rsData = gobjConn.execute(strSQL)
intShippedProducts = rsData("intCount")
rsData.close
set rsData = nothing
strSQL = "SELECT COUNT(*) AS intCount FROM " & STR_TABLE_ORDER & " AS O, " & STR_TABLE_SHOPPER & " AS S WHERE O.chrStatus IN ('S','A','Z','X','C','H','K','E','R') AND O.intBillShopperID=S.intID AND O.dtmCreated <= S.dtmCreated AND O.dtmCreated >= '" & dtmStart & "' AND O.dtmCreated <= '" & dtmEnd & "'"
set rsData = gobjConn.execute(strSQL)
intNewShoppers = rsData("intCount")
strSQL = "SELECT COUNT(*) AS intCount FROM " & STR_TABLE_ORDER & " AS O, " & STR_TABLE_SHOPPER & " AS S WHERE O.chrStatus IN ('S','A','Z','X','C','H','K','E','R') AND O.intBillShopperID=S.intID AND O.dtmCreated > S.dtmCreated AND O.dtmCreated >= '" & dtmStart & "' AND O.dtmCreated <= '" & dtmEnd & "'"
rsData.close
set rsData = nothing
set rsData = gobjConn.execute(strSQL)
intOldShoppers = rsData("intCount")
rsData.close
set rsData = nothing



DrawPage

response.end

sub DrawPage
	call DrawHeaderUpdate()
%>
<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%">
<TR>
	<TD><%= font(5) %>Summary Report</TD>
	<TD ALIGN=RIGHT><%= font(1) %><a href="report_selectdate.asp">New Date Range</a></TD>
</TR>
</TABLE>
<%= spacer(1,5) %><BR>
<% stlBeginInfoBar INT_PAGE_WIDTH %>
Summary Report for <B><%= DateValue(dtmStart) %></B> to <B><%= DateValue(dtmEnd) %></B>
<% stlEndInfoBar %>
<%= spacer(1,10) %><BR>
<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0" WIDTH="200">
<TR>
	<TD>Total Orders Placed:</TD>
	<TD ALIGN="right"><B><%= intTotalOrders %></B>&nbsp;&nbsp;</TD>
	<TD ALIGN="right"><B><%= FormatCurrency(mnyTotalOrders, 2) %></B></TD>
</TR>
<TR>
	<TD>Total Orders Shipped:</TD>
	<TD ALIGN="right"><B><%= intShippedOrders %></B>&nbsp;&nbsp;</TD>
	<TD ALIGN="right"><B><%= FormatCurrency(mnyShippedOrders, 2) %></B></TD>
</TR>
<TR>
	<TD COLSPAN="3">&nbsp;</TD>
</TR>
<TR>
	<TD>Total Authorizations</TD>
	<TD ALIGN="right"><B><%= intTotalAuth %></B>&nbsp;&nbsp;</TD>
	<TD ALIGN="right"><B><%= FormatCurrency(mnyTotalAuth, 2) %></B></TD>
</TR>
<TR>
	<TD>Total Deposits:</TD>
	<TD ALIGN="right"><B><%= intTotalDeposit %></B>&nbsp;&nbsp;</TD>
	<TD ALIGN="right"><B><%= FormatCurrency(mnyTotalDeposit, 2) %></B></TD>
</TR>
<TR>
	<TD>Total Credits:</TD>
	<TD ALIGN="right"><B><%= intTotalCredit %></B>&nbsp;&nbsp;</TD>
	<TD ALIGN="right"><B><%= FormatCurrency(mnyTotalCredit, 2) %></B></TD>
</TR>
<TR>
	<TD>Total Declines:</TD>
	<TD ALIGN="right"><B><%= intTotalDecline %></B>&nbsp;&nbsp;</TD>
	<TD ALIGN="right"><B><%= FormatCurrency(mnyTotalDecline, 2) %></B></TD>
</TR>
<TR>
	<TD COLSPAN="3">&nbsp;</TD>
</TR>
<TR>
	<TD>Products Shipped:</TD>
	<TD ALIGN="right"><B><%= intShippedProducts %></B>&nbsp;&nbsp;</TD>
</TR>
<TR>
	<TD>New Shoppers:</TD>
	<TD ALIGN="right"><B><%= intNewShoppers %></B>&nbsp;&nbsp;</TD>
</TR>
<TR>
	<TD>Repeat Shoppers:</TD>
	<TD ALIGN="right"><B><%= intOldShoppers %></B>&nbsp;&nbsp;</TD>
</TR>
</TABLE>
</FONT></CENTER>
<%
end sub
%>