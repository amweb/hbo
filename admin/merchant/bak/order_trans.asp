<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<!--#include file="../../config/incEcho.asp"-->
<%

' =====================================================================================
' = File: order_trans.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Allow user to view and perform transactions for an order
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_ORDER_VIEW) then
	response.redirect "menu.asp"
end if

const STR_PAGE_TITLE = "Perform Transaction"
const STR_PAGE_TYPE = "adminordertrans"

OpenConn

dim blnCanAuth, blnCanDeposit, blnCanCredit, blnCanManual, blnCanAuthDep,blnIsCredit
blnCanAuth = CheckUserAccess(ACC_ORDER_TRANS_AUTH)
blnCanDeposit = CheckUserAccess(ACC_ORDER_TRANS_DEPOSIT)
blnCanCredit = CheckUserAccess(ACC_ORDER_TRANS_CREDIT)
blnCanManual = CheckUserAccess(ACC_ORDER_TRANS_MANUAL)
blnCanAuthDep = CheckUserAccess(ACC_ORDER_TRANS_AUTHDEP)
blnIsCredit = false



dim intID, strAction
strAction = Request("action")
intID = Request("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if

Dim rsCheckType

set rsCheckType = gobjConn.Execute("SELECT chrPaymentMethod FROM TCS_Order WHERE intID = " & intID & "")


if not rsCheckType.eof then
	blnIsCredit = rsCheckType(0) = "OCC"
end if


dim STR_DETAIL_SCRIPT
STR_DETAIL_SCRIPT = "order_detail.asp?id=" & intID & "&"

dim mnyGrandTotal, vchPaymentCardName, mnyAuthTotal, mnyDepositTotal, mnyCreditTotal, blnManualTrans
dim strSQL, rsData
if intID > 0 then
	strSQL = "SELECT intID, mnyGrandTotal, vchPaymentCardName, "
	strSQL = strSQL & "(SELECT ISNULL(SUM(mnyTransAmount),0) FROM " & STR_TABLE_TRANS & " WHERE chrStatus='A' AND chrTransType IN ('AV','AS','ES','EV','DD') AND intOrderID=" & intID & ") AS mnyAuthTotal,"
	strSQL = strSQL & "(SELECT ISNULL(SUM(mnyTransAmount),0) FROM " & STR_TABLE_TRANS & " WHERE chrStatus='A' AND chrTransType IN ('ES','EV','DS','DD') AND intOrderID=" & intID & ") AS mnyDepositTotal,"
	strSQL = strSQL & "(SELECT ISNULL(SUM(mnyTransAmount),0) FROM " & STR_TABLE_TRANS & " WHERE chrStatus='A' AND chrTransType IN ('CR','DC') AND intOrderID=" & intID & ") AS mnyCreditTotal,"
	strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_TRANS & " WHERE chrStatus='A' AND chrType='M' AND intOrderID=" & intID & ") AS intManualTrans"
	strSQL = strSQL & " FROM " & STR_TABLE_ORDER & " WHERE intID=" & intID & " AND chrStatus<>'D'"
	'response.write strSQL & "<BR>"
	set rsData = gobjConn.execute(strSQL)
	if rsData.eof then
		rsData.close
		set rsData = nothing
		response.redirect "order_list.asp"
	end if
	mnyGrandTotal = rsData("mnyGrandTotal")
	vchPaymentCardName = rsData("vchPaymentCardName")
	mnyAuthTotal = rsData("mnyAuthTotal")
	mnyDepositTotal = rsData("mnyDepositTotal")
	mnyCreditTotal = rsData("mnyCreditTotal")
	blnManualTrans = (rsData("intManualTrans") > 0)
	rsData.close
	set rsData = nothing
else
	response.redirect "order_list.asp"
end if

select case strAction
	case "authorize":
		if mnyAuthTotal = 0 and not blnManualTrans and blnCanAuth then
			DoAuthorize
		else
			' action not valid
			response.redirect "order_trans.asp?id=" & intID
		end if
	case "deposit":
		if mnyDepositTotal = 0 and not blnManualTrans and (blnCanAuthDep or (mnyGrandTotal <= mnyAuthTotal and blnCanDeposit)) and blnIsCredit then
			DoDeposit
		else
			' action not valid
			response.redirect "order_trans.asp?id=" & intID
		end if
	case "credit":
		if mnyDepositTotal - mnyCreditTotal > 0 and not blnManualTrans and blnCanCredit then
			if not DoCredit(Request("mnyAmount")) then
				
				DrawPage "Amount to credit is not valid."
				
			end if
		else
			' action not valid
			response.redirect "order_trans.asp?id=" & intID
		end if
	case else:
		
		DrawPage ""
		
end select
response.end

sub DrawPage(strError)
	DrawFormHeader "100%", "Perform Transaction", "<A HREF=""" & STR_DETAIL_SCRIPT & """>Cancel</A>"
%>
<TABLE BORDER="0" CELLPADDING="4" CELLSPACING="0" WIDTH="530">
<% if strError <> "" then %>
<TR BGCOLOR="#FF0000">
	<TD VALIGN="top" ALIGN="center">
	<TABLE BORDER="1" CELLPADDING="4" CELLSPACING="0">
	<TR BGCOLOR="#DDDDDD">
		<TD ALIGN="center"><B><%= strError %></B></TD>
	</TR>
	</TABLE>
	</TD>
</TR>
<% end if %>
<TR>
	<TD ALIGN="center">
		<FONT SIZE="2"><B>Transaction Menu</B></FONT><BR>
		<BR>
		<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0">
		<TR>
			<TD NOWRAP VALIGN="top"><B>Tracking Number:</B></TD>
			<TD>&nbsp;&nbsp;</TD>
			<TD ALIGN="right" NOWRAP><%= STR_MERCHANT_TRACKING_PREFIX & intID %></TD>
		</TR>
		<TR>
			<TD NOWRAP VALIGN="top"><B>Order Total:</B></TD>
			<TD>&nbsp;&nbsp;</TD>
			<TD ALIGN="right" NOWRAP><%= SafeFormatCurrency("n/a", mnyGrandTotal, 2) %></TD>
		</TR>
		<TR>
			<TD NOWRAP VALIGN="top"><B>Amount Authorized:</B></TD>
			<TD>&nbsp;&nbsp;</TD>
			<TD ALIGN="right" NOWRAP><%= SafeFormatCurrency("n/a", mnyAuthTotal - mnyCreditTotal, 2) %></TD>
		</TR>
		<TR>
			<TD NOWRAP VALIGN="top"><B>Amount Deposited:</B></TD>
			<TD>&nbsp;&nbsp;</TD>
			<TD ALIGN="right" NOWRAP><%= SafeFormatCurrency("n/a", mnyDepositTotal - mnyCreditTotal, 2) %></TD>
		</TR>
		<TR>
			<TD NOWRAP VALIGN="top"><B>Amount Credited:</B></TD>
			<TD>&nbsp;&nbsp;</TD>
			<TD ALIGN="right" NOWRAP><%= SafeFormatCurrency("n/a", mnyCreditTotal, 2) %></TD>
		</TR>
		<% if mnyDepositTotal - mnyCreditTotal > mnyGrandTotal then %>
		<TR>
			<TD NOWRAP VALIGN="top"><B>Overcharge:</B></TD>
			<TD>&nbsp;&nbsp;</TD>
			<TD ALIGN="right" NOWRAP><%= SafeFormatCurrency("n/a", mnyDepositTotal - mnyGrandTotal - mnyCreditTotal, 2) %></TD>
		</TR>
		<% end if %>
		</TABLE>
	</TD>
</TR>
<% if blnManualTrans then %>
<TR>
	<TD VALIGN="top" ALIGN="center">
	<TABLE BORDER="1" CELLPADDING="4" CELLSPACING="0">
	<TR BGCOLOR="#DDDDDD">
		<TD ALIGN="center">
			Because one or more transactions have been performed manually (e.g. over the phone with the bank),
			no further online transactions may be performed for this order.
		</TD>
	</TR>
	</TABLE>
	</TD>
</TR>
<% end if %>
<% if blnCanManual then %>
<TR>
	<TD VALIGN="top" ALIGN="center">
	<TABLE BORDER="1" CELLPADDING="4" CELLSPACING="0">
	<TR BGCOLOR="#DDDDDD">
		<TD ALIGN="center">
			To enter a manual transaction (e.g. performed over the phone with the bank), <A HREF="order_transmanual.asp?id=<%= intID %>">click here</A>.
		</TD>
	</TR>
	</TABLE>
	</TD>
</TR>
<% end if %>
<% if mnyDepositTotal - mnyCreditTotal > 0 and blnCanCredit and not blnManualTrans then %>
<FORM ACTION="order_trans.asp" METHOD="POST">
<INPUT TYPE="hidden" NAME="action" VALUE="credit">
<INPUT TYPE="hidden" NAME="id" VALUE="<%= intID %>">
<TR>
	<TD VALIGN="top" ALIGN="center">
	<TABLE BORDER="1" CELLPADDING="4" CELLSPACING="0">
	<TR BGCOLOR="#DDDDDD">
		<TD ALIGN="center">
			To credit the cardholder's account, enter the amount to credit.<BR>
			<B>Amount to credit: (max <%= FormatCurrency(mnyDepositTotal - mnyCreditTotal, 2) %>)</B> <INPUT TYPE="text" NAME="mnyAmount" SIZE="10" MAXLENGTH="10" VALUE=""><BR>
			<INPUT TYPE="submit" VALUE="Credit Order">
		</TD>
	</TR>
	</TABLE>
	</TD>
</TR>
</FORM>
<% end if %>
<% if mnyAuthTotal = 0 and blnCanAuth and not blnManualTrans then %>
<FORM ACTION="order_trans.asp" METHOD="POST">
<INPUT TYPE="hidden" NAME="action" VALUE="authorize">
<INPUT TYPE="hidden" NAME="id" VALUE="<%= intID %>">
<TR>
	<TD VALIGN="top" ALIGN="center">
	<TABLE BORDER="1" CELLPADDING="4" CELLSPACING="0">
	<TR BGCOLOR="#DDDDDD">
		<TD ALIGN="center">
			To perform an authorization for funds: (an authorization places a hold on the cardholder's account for
			the specified amount but does not actually transfer money until a deposit is performed--the deposit
			should not be performed until the products ordered are ready to be shipped)<BR>
			<B>Amount to authorize: <%= SafeFormatCurrency("$0.00", mnyGrandTotal, 2) %></B><BR>
			<INPUT TYPE="submit" VALUE="Authorize">
		</TD>
	</TR>
	</TABLE>
	</TD>
</TR>
</FORM>
<%
	end if
	if mnyDepositTotal = 0 and (blnCanDeposit or blnCanAuthDep) and not blnManualTrans then
		if mnyGrandTotal <= mnyAuthTotal or blnCanAuthDep then
%>
<FORM ACTION="order_trans.asp" METHOD="POST">
<INPUT TYPE="hidden" NAME="action" VALUE="deposit">
<INPUT TYPE="hidden" NAME="id" VALUE="<%= intID %>">
<TR>
	<TD VALIGN="top" ALIGN="center">
	<TABLE BORDER="1" CELLPADDING="4" CELLSPACING="0">
	<TR BGCOLOR="#DDDDDD">
		<TD ALIGN="center">
			To deposit the funds to your merchant account, enter the shipping/conf number for the order:<BR>
			<B>Shipping/Conf Number:</B> <INPUT TYPE="text" NAME="strShippingNumber" SIZE="30" MAXLENGTH="30" VALUE=""><BR>
			<B>Amount to deposit: <%= FormatCurrency(mnyGrandTotal) %></B><BR>
			<INPUT TYPE="submit" VALUE="Deposit">
		</TD>
	</TR>
	</TABLE>
	</TD>
</TR>
</FORM>
<%
		elseif mnyAuthTotal > 0 then
%>
<TR BGCOLOR="#FF0000">
	<TD VALIGN="top" ALIGN="center">
	<TABLE BORDER="1" CELLPADDING="4" CELLSPACING="0">
	<TR BGCOLOR="#DDDDDD">
		<TD ALIGN="center">
			<B>** NOTICE **</B><BR>
			The order has been modified since the shopper originally entered the order. The amount to deposit (<%= FormatCurrency(mnyGrandTotal, 2) %>)
			is greater then the amount originally authorized by the credit-card company. You do not have sufficient rights
			to perform a deposit transaction in this situation. Please contact your manager for assistance.
		</TD>
	</TR>
	</TABLE>
	</TD>
</TR>
<%
		end if
		if mnyGrandTotal > mnyAuthTotal and mnyAuthTotal > 0 then
%>
<TR BGCOLOR="#FF0000">
	<TD VALIGN="top" ALIGN="center">
	<TABLE BORDER="1" CELLPADDING="4" CELLSPACING="0">
	<TR BGCOLOR="#DDDDDD">
		<TD ALIGN="center">
			<B>** NOTICE **</B><BR>
			The order has been modified since the shopper originally entered the order. The amount to deposit (<%= FormatCurrency(mnyGrandTotal, 2) %>)
			is greater then the amount originally authorized by the credit-card company. This transaction may be declined
			despite available funds in the cardholder's account if the previous authorization is still standing.
		</TD>
	</TR>
	</TABLE>
	</TD>
</TR>
<%
		end if
	end if
	
	dim strSQL, rsTrans, blnGoodAVWarning, blnBadAVWarning
	strSQL = "SELECT * FROM " & STR_TABLE_TRANS & " WHERE chrStatus<>'D' AND intOrderID=" & intID & " ORDER BY intID"
	set rsTrans = gobjConn.execute(strSQL)
	blnGoodAVWarning = false
	blnBadAVWarning = false
	if rsTrans.eof then
		blnGoodAVWarning = true
	else
%>
<TR>
	<TD ALIGN="center">
		<BR>
		<B>Transaction Log</B><BR>
		<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="0">
		<TR>
			<TD NOWRAP><B>Date<B></TD>
			<TD NOWRAP><B>Action</B></TD>
			<TD NOWRAP><B>Amount</B></TD>
			<TD NOWRAP><B>Result</B></TD>
			<TD NOWRAP><B>Notes</B></TD>
		</TR>
<%
		while not rsTrans.eof
%>
		<TR>
			<TD NOWRAP><B><A HREF="order_transdetail.asp?order=<%= intID %>&id=<%= rsTrans("intID") %>">*</A></B> <%= rsTrans("dtmTransDate") %></TD>
			<TD NOWRAP><% if rsTrans("chrType") = "M" then %>MANUAL <% end if %><%= GetArrayValue(rsTrans("chrTransType"), dctTransType) %>&nbsp;&nbsp;</TD>
			<TD ALIGN="right" NOWRAP><%= SafeFormatCurrency("n/a", rsTrans("mnyTransAmount"), 2) %>&nbsp;&nbsp;</TD>
			<TD NOWRAP><%= GetArrayValue(rsTrans("chrStatus"), dctTransStatusValues) %>&nbsp;&nbsp;</TD>
			<TD NOWRAP>
			<%
				if not IsNull(rsTrans("vchAuthCode")) then
					response.write "Auth " & rsTrans("vchAuthCode") & " "
				end if
				if not IsNull(rsTrans("vchRefCode")) then
					response.write "Ref " & rsTrans("vchRefCode") & " "
				end if
				if not IsNull(rsTrans("vchOrderCode")) then
					response.write "Order " & rsTrans("vchOrderCode") & " "
				end if
				if not IsNull(rsTrans("vchDeclineCode")) then
					response.write "Decline " & rsTrans("vchDeclineCode") & " "
				end if
				if not IsNull(rsTrans("vchAVCode")) then
					response.write "AVS " & rsTrans("vchAVCode") & " "
				end if
			%>
			</TD>
		</TR>
<%
			if not IsNull(rsTrans("vchAVCode")) then
				if rsTrans("vchAVCode") = "X" or rsTrans("vchAVCode") = "Y" then
					blnGoodAVWarning = true
				else
					blnBadAVWarning = true
				end if
%>
		<TR>
			<TD></TD>
			<TD COLSPAN="4">
				Address Verification Result: <%= GetArrayValue(rsTrans("vchAVCode"), dctAVSValues) %>
			</TD>
		</TR>
			<%
			end if
			rsTrans.MoveNext
		wend
%>
		</TABLE>
	</TD>
</TR>
<%
	end if
	rsTrans.close
	set rsTrans = nothing
	if blnBadAVWarning then
%>
<TR BGCOLOR="#FF0000">
	<TD VALIGN="top" ALIGN="center">
	<TABLE BORDER="1" CELLPADDING="4" CELLSPACING="0">
	<TR BGCOLOR="#DDDDDD">
		<TD ALIGN="center">
			<B>** WARNING **</B><BR>
			At least one transaction on this order has failed address verification. Address verification compares the
			billing address provided on this order with the billing address on file with the cardholder's bank. For
			the protection of both the cardholder and the merchant, you are recommended to verify that the contact
			information provided on this order is accurate by calling the customer before performing any further
			transactions.
		</TD>
	</TR>
	</TABLE>
	</TD>
</TR>
<% elseif not blnGoodAVWarning and not blnManualTrans then %>
<TR BGCOLOR="#FF0000">
	<TD VALIGN="top" ALIGN="center">
	<TABLE BORDER="1" CELLPADDING="4" CELLSPACING="0">
	<TR BGCOLOR="#DDDDDD">
		<TD ALIGN="center">
			<B>** ADDRESS VERIFICATION NOTICE **</B><BR>
			No address verification has been performed yet. When an authorize transaction is performed, the billing address
			provided on this order will be compared with the billing address on file with the cardholder's bank.
		</TD>
	</TR>
	</TABLE>
	</TD>
</TR>
<% end if %>
</TABLE>
</FONT>
<%
end sub

sub DoAuthorize()
	dim mnyAmount, intTransID, intResult
	mnyAmount = mnyGrandTotal
	
	' configure ASP and display header
	Trans_DrawPageHeader
	' prepare transaction
	intTransID = Trans_Prepare(intID, "AV", mnyAmount)
	' display intro msg
	call Trans_DrawIntro
	' perform transaction
	intResult = Trans_Run(intTransID)
	' display result msg
	select case intResult
		case 0:	' transaction successful
			gobjConn.execute("UPDATE " & STR_TABLE_ORDER & " SET chrStatus='Z' WHERE intID=" & intID)
			Trans_DrawSuccess intTransID
			Trans_DrawJavaScriptRedirect "order_detail.asp?id=" & intID
		case 1:	' transaction failed
			Trans_DrawFailed intTransID
		case 2:	' transaction requires 
			Trans_DrawVerbal intTransID
		case 3:	' system error
			Trans_DrawSysError intTransID
	end select
	' display footer
	Trans_DrawPageFooter
end sub

sub DoDeposit()
	dim mnyAmount, intTransID, intResult
	mnyAmount = mnyGrandTotal
	
	UpdateOrderShippingNumber_Other intID, Request("strShippingNumber")
	
	' configure ASP and display header
	Trans_DrawPageHeader
	' prepare transaction
	if mnyAuthTotal > 0 then
		intTransID = Trans_Prepare(intID, "DS", mnyAmount)
	else
		intTransID = Trans_Prepare(intID, "EV", mnyAmount)
	end if
	' display intro msg
	call Trans_DrawIntro
	' perform transaction
	intResult = Trans_Run(intTransID)
	' display result msg
	select case intResult
		case 0:	' transaction successful
			gobjConn.execute("UPDATE " & STR_TABLE_ORDER & " SET chrStatus='X' WHERE intID=" & intID)
			Trans_DrawSuccess intTransID
			Trans_DrawJavaScriptRedirect "order_detail.asp?id=" & intID
		case 1:	' transaction failed
			Trans_DrawFailed intTransID
		case 2:	' transaction requires 
			Trans_DrawVerbal intTransID
		case 3:	' system error
			Trans_DrawSysError intTransID
	end select
	' display footer
	Trans_DrawPageFooter
	
	SendOrderShipEmail_Other intID
end sub


function DoCredit(mnyAmount)
	dim intTransID, intResult, tfFullCredit
	' return false if amount to credit is not valid
	if not IsNumeric(mnyAmount) then
		DoCredit = false
		exit function
	end if
		
	mnyAmount = cCur(mnyAmount)
	if mnyAmount <= 0 or mnyAmount > mnyDepositTotal - mnyCreditTotal then
	'if mnyAmount <= 0 or mnyAmount > CCur(FormatCurrency(mnyDepositTotal, 2)) - mnyCreditTotal then
		DoCredit = false
		exit function
	end if
	
	DoCredit = true
	
	tfFullCredit = (mnyAmount = mnyDepositTotal - mnyCreditTotal)
	
	' configure ASP and display header
	Trans_DrawPageHeader
	' prepare transaction
	intTransID = Trans_Prepare(intID, "CR", mnyAmount)
	' display intro msg
	call Trans_DrawIntro
	' perform transaction
	intResult = Trans_Run(intTransID)
	' display result msg
	select case intResult
		case 0:	' transaction successful
			if tfFullCredit then
				gobjConn.execute("UPDATE " & STR_TABLE_ORDER & " SET chrStatus='C' WHERE intID=" & intID)
			end if
			Trans_DrawSuccess intTransID
			Trans_DrawJavaScriptRedirect "order_detail.asp?id=" & intID
		case 1:	' transaction failed
			Trans_DrawFailed intTransID
		case 2:	' transaction requires 
			Trans_DrawVerbal intTransID
		case 3:	' system error
			Trans_DrawSysError intTransID
	end select
	' display footer
	Trans_DrawPageFooter
end function

' Trans_DrawPageHeader():
'	prepare ASP environment (script timeout, buffering, etc.)
'	draw HTML header
sub Trans_DrawPageHeader()
	response.Buffer = true
	Server.ScriptTimeout = 90
	response.write "<HTML>"
	response.write "<HEAD>"
	response.write "  <TITLE>Processing Payment</TITLE>"
	response.write "</HEAD>"
	response.write "<BODY>"
	
	response.flush
end sub

' Trans_DrawPageFooter():
'	draw HTML footer
sub Trans_DrawPageFooter()
	response.write "</BODY>"
	response.write "</HTML>"
	response.flush
end sub

' Trans_DrawIntro()
'	draw introductory text ("Now processing your transaction...", etc.)
sub Trans_DrawIntro()
%>
<CENTER>
<TABLE BORDER=1 CELLPADDING=6 CELLSPACING=0>
<TR>
	<TD ALIGN="center"><%= font(2) %>
	<B>* Processing Transaction... *</B><BR>
	This may take up to two minutes to complete...
	</TD>
</TR>
</TABLE>
</CENTER>
<BR>
<%
	response.flush
end sub

' Trans_DrawSuccess()
'	draw success text ("Your credit card was approved...", etc.)
sub Trans_DrawSuccess(intTransID)
	dim strSQL, rsTemp
	strSQL = "SELECT * FROM " & STR_TABLE_TRANS & " WHERE intID=" & intTransID
	set rsTemp = gobjConn.execute(strSQL)
%>
<CENTER>
<TABLE BORDER=1 CELLPADDING=6 CELLSPACING=0>
<TR>
	<TD ALIGN="center"><%= font(2) %>
	<B>* Transaction Approved *</B><BR>
	<FONT SIZE="3"><TT>Auth <%= rsTemp("vchAuthCode") %> Ref <%= rsTemp("vchRefCode") %> Order <%= rsTemp("vchOrderCode") & " " & SafeFormatCurrency("$0.00", rsTemp("mnyTransAmount"), 2) %></TT></FONT><BR>
	The credit card transaction was approved.<BR>
	Please <A HREF="order_detail.asp?id=<%= rsTemp("intOrderID") %>">click here</A> to continue.
	</TD>
</TR>
</TABLE>
</CENTER>
<BR>
<%
	response.flush
	rsTemp.close
	set rsTemp = nothing
end sub

' Trans_DrawJavaScriptRedirect():
'	draw a client-side JavaScript that will make the browser redirect to another URL
sub Trans_DrawJavaScriptRedirect(strURL)
%>
<SCRIPT LANGUAGE="JavaScript">
<!--
	document.location.replace('<%= strURL %>');
// -->
</SCRIPT>
<%
	response.flush
end sub

' Trans_DrawFailed():
'	draw failed text ("Your credit card was declined...", etc.)
sub Trans_DrawFailed(intTransID)
	dim strSQL, rsTemp
	strSQL = "SELECT * FROM " & STR_TABLE_TRANS & " WHERE intID=" & intTransID
	set rsTemp = gobjConn.execute(strSQL)
%>
<CENTER>
<TABLE BORDER=1 CELLPADDING=6 CELLSPACING=0>
<TR>
	<TD ALIGN="center"><%= font(2) %>
	<B>* Transaction Declined *</B><BR>
	Decline Code <%= rsTemp("vchDeclineCode") %>: <%= rsTemp("vchShopperMsg") %><BR>
	The credit card transaction was not approved.<BR>
	Please <A HREF="order_detail.asp?id=<%= rsTemp("intOrderID") %>">click here</A> to continue.
	</TD>
</TR>
</TABLE>
</CENTER>
<BR>
<%
	response.flush
	rsTemp.close
	set rsTemp = nothing
end sub

' Trans_DrawVerbal():
'	draw verbal assistance required text ("Please call VISA at....", etc.)
sub Trans_DrawVerbal(intTransID)
	dim strSQL, rsTemp
	strSQL = "SELECT * FROM " & STR_TABLE_TRANS & " WHERE intID=" & intTransID
	set rsTemp = gobjConn.execute(strSQL)
%>
<CENTER>
<TABLE BORDER=1 CELLPADDING=6 CELLSPACING=0>
<TR>
	<TD ALIGN="center"><%= font(2) %>
	<B>* Transaction Requires Verbal Authorization *</B><BR>
	Decline Code <%= rsTemp("vchDeclineCode") %>: <%= rsTemp("vchShopperMsg") %><BR>
	The credit card company has requested that<BR>
	you call them for verbal authorization.<BR>
	Please <A HREF="order_detail.asp?id=<%= rsTemp("intOrderID") %>">click here</A> to continue.
	</TD>
</TR>
</TABLE>
</CENTER>
<BR>
<%
	response.flush
	rsTemp.close
	set rsTemp = nothing
end sub

' Trans_DrawSysError():
'	draw system error text ("A system error occurred...", etc.)
sub Trans_DrawSysError(intTransID)
	dim strSQL, rsTemp
	strSQL = "SELECT * FROM " & STR_TABLE_TRANS & " WHERE intID=" & intTransID
	set rsTemp = gobjConn.execute(strSQL)
%>
<CENTER>
<TABLE BORDER=1 CELLPADDING=6 CELLSPACING=0>
<TR>
	<TD ALIGN="center"><%= font(2) %>
	<B>* System Error *</B><BR>
	Error Code <%= rsTemp("vchDeclineCode") %><BR>
	A system error has occurred while processing<BR>
	this request. Please contact technical support<BR>
	for assistance and refer to order #<%= rsTemp("intOrderID") %>.<BR>
	Please <A HREF="order_detail.asp?id=<%= rsTemp("intOrderID") %>">click here</A> to continue.
	</TD>
</TR>
</TABLE>
</CENTER>
<BR>
<%
	response.flush
	rsTemp.close
	set rsTemp = nothing
end sub

%>