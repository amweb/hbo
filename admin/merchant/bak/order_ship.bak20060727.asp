<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: order_ship.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Order shipment screen -- for orders without online card processing
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_ORDER_SHIP) then
	response.redirect "menu.asp"
end if

const STR_PAGE_TITLE = "Ship Order"
const STR_PAGE_TYPE = "adminordership"

OpenConn

dim intID, intBrand, strAction
strAction = Request("action")
intID = Request("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if
if intID <= 0 then
	response.redirect "order_list.asp"
end if
intBrand = Session(SESSION_MERCHANT_BID)
if IsNumeric(intBrand) then
	intBrand = CLng(intBrand)
else
	intBrand = 0
end if

dim STR_SHIP_SCRIPT, STR_DETAIL_SCRIPT
STR_SHIP_SCRIPT = "order_ship.asp?id=" & intID & "&"
STR_DETAIL_SCRIPT = "order_detail.asp?id=" & intID & "&"

select case strAction
	case "submit":
		AutoCheck Request, ""
		if FormErrors.count > 0 then
			
			DrawPage ""
			
		else
			UpdateOrderShippingNumber_Other intID, Request("vchShippingNumber")
			UpdateItemShippingNumber_Other intID, Request("vchShippingNumber")
			UpdateOrderStatus_Other intID, "H"
			SendOrderShipEmail_Other intID
			response.redirect STR_DETAIL_SCRIPT
		end if
	case else:
		
		DrawPage ""
		
end select
response.end

sub DrawPage(strError)
	DrawFormHeader "100%", STR_PAGE_TITLE, ""
	stlBeginStdTable "100%"
%>
<FORM ACTION="<%= STR_SHIP_SCRIPT %>" METHOD="POST">
<%
	HiddenInput "action", "submit"
	stlBeginFormSection "100%", 2
	stlCaption "Please enter the shipping/conf number for this order:", 2
	stlTextInput "vchShippingNumber", 32, 32, Request, "Shipping/Conf Number", "IsEmpty", "Missing shipping/conf number"
	stlEndFormSection
	stlSubmit "Submit"
%>
</FORM>
<%
	stlEndStdTable
end sub

sub UpdateItemShippingNumber_Other(intOrderID, strShippingNumber)
	if intOrderID > 0 then
		dim strSQL, strSQL2, strStatusClause, strUpdtByUser
		strStatusClause = UserAccessStatus_BuildSQLWhereX("SAZE")
		strUpdtByUser = iif(intBrand>0,Session(SESSION_MERCHANT_ULOGIN),"pub")

		strSQL2 = "SELECT I.intID FROM " & STR_TABLE_LINEITEM & " AS I"
		if intBrand > 0 then
			strSQL2 = strSQL2 & ", " & STR_TABLE_INVENTORY & " AS B "
		end if
		strSQL2 = strSQL2 & " WHERE (I.intOrderID=" & intOrderID & ") AND (I.chrStatus IN (" & strStatusClause & "))"
		if intBrand > 0 then
			strSQL2 = strSQL2 & " AND (B.intID=I.intInvID) AND (B.intBrand=" & intBrand & ") AND (B.chrType='I' OR B.chrType IS NULL)"
		end if
		strSQL2 = strSQL2 & " AND (IsNull(vchShippingNumber,'') = '')"

		strSQL = "UPDATE " & STR_TABLE_LINEITEM & " SET dtmUpdated = GETDATE(), vchUpdatedByUser = '" & strUpdtByUser & "', vchUpdatedByIP = '" & gstrUserIP & "', vchShippingNumber = '" & strShippingNumber & "'"
		strSQL =  strSQL & " WHERE intID IN (" & strSQL2 & ")"
'		response.write "<br />" & strSQL & "<br />"
		gobjConn.execute(strSQL)
	end if
end sub
%>