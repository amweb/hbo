<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<!--#include file="../config/incSearch.asp"-->
<%
server.scripttimeout = 900
' =====================================================================================
' = File: brand_order_list.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Merchant Order Admin
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_ORDER_VIEW) then
	response.redirect "../main.asp"
end if

const STR_PAGE_TITLE = "Order Search"
const STR_PAGE_TYPE = "adminorderlist"

const STR_SEARCH_SCRIPT = "brand_order_list.asp?"
const STR_DETAIL_SCRIPT = "brand_order_detail.asp?"

OpenConn

dim strPageTitle
strPageTitle = STR_PAGE_TITLE

strSearchAuxText = "Export Order List"
strSearchAuxAction = "export"

dim strBrandClause, strBrandFields, intBrand
strBrandClause = ""
if Session(SESSION_MERCHANT_BID) <> "" then
	intBrand = Session(SESSION_MERCHANT_BID)
	strBrandFields = ", (I.mnyUnitPrice * I.intQuantity) AS mnyPrice, ISNULL(I.chrViewed,'') AS chrViewed"
	strBrandClause = strBrandClause & " LEFT JOIN " & STR_TABLE_LINEITEM & " AS I ON (I.intOrderID = O.intID) AND (I.chrStatus<>'D')"		
	' AND IsNull(I.vchShippingNumber,'')='' ' bkonvalin 1/13/04 - deactivated per email James Dutra 1/12/04 RE: Gagan# 8751
	strBrandClause = strBrandClause & " RIGHT JOIN " & STR_TABLE_INVENTORY & " AS B ON (B.intID=I.intInvID) AND (B.intBrand=" & intBrand & ") AND (B.chrType='I' OR B.chrType IS NULL)"
else
	strBrandFields = ",'' AS chrViewed"
end if

dim strMode, strAction
strMode = lcase(Request("mode"))
if strMode <> "a" and strMode <> "i" then
	strMode = "b"
end if
strAction = lcase(Request("auxaction"))
if strAction = "" then
	strAction = lcase(Request("action"))
end if

if strAction = "export" then
	dim strFormat, blnIncludeShipping, blnIncludeBilling
	GetExportFlags blnIncludeBilling, blnIncludeShipping, strFormat
end if

dim strStatusClause
strStatusClause = UserAccessStatus_BuildSQLWhere()
if strStatusClause = "" then
	response.redirect "menu.asp"
end if

strSearchSQLPrefix = "SELECT O.*,S.vchLastName AS S_vchLastName,S.vchFirstName AS S_vchFirstName"
strSearchSQLPrefix = strSearchSQLPrefix & ",S.vchEmail AS S_vchEmail" & strBrandFields
if strAction = "export" then
	if blnIncludeBilling then
		strSearchSQLPrefix = strSearchSQLPrefix & ",P.vchLastName AS P_vchLastName"
		strSearchSQLPrefix = strSearchSQLPrefix & ",P.vchFirstName AS P_vchFirstName"
		strSearchSQLPrefix = strSearchSQLPrefix & ",P.vchAddress1 AS P_vchAddress1"
		strSearchSQLPrefix = strSearchSQLPrefix & ",P.vchAddress2 AS P_vchAddress2"
		strSearchSQLPrefix = strSearchSQLPrefix & ",P.vchCity AS P_vchCity"
		strSearchSQLPrefix = strSearchSQLPrefix & ",P.vchState AS P_vchState"
		strSearchSQLPrefix = strSearchSQLPrefix & ",P.vchZip AS P_vchZip"
		strSearchSQLPrefix = strSearchSQLPrefix & ",P.vchDayPhone AS P_vchDayPhone"
		strSearchSQLPrefix = strSearchSQLPrefix & ",P.vchEmail AS P_vchEmail"
	end if
	if blnIncludeShipping then
		strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchLastName AS H_vchLastName"
		strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchFirstName AS H_vchFirstName"
		strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchAddress1 AS H_vchAddress1"
		strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchAddress2 AS H_vchAddress2"
		strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchCity AS H_vchCity"
		strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchState AS H_vchState"
		strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchZip AS H_vchZip"
		strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchDayPhone AS H_vchDayPhone"
		strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchEmail AS H_vchEmail"
	end if
end if
strSearchSQLPrefix = strSearchSQLPrefix & " FROM " & STR_TABLE_ORDER & " AS O"
strSearchSQLPrefix = strSearchSQLPrefix & " LEFT JOIN " & STR_TABLE_SHOPPER & " AS S ON S.intID = O.intShopperID"
if strAction = "export" then
	if blnIncludeBilling then
		strSearchSQLPrefix = strSearchSQLPrefix & " LEFT JOIN " & STR_TABLE_SHOPPER & " AS P ON P.intID = O.intBillShopperID"
	end if
	if blnIncludeShipping then
		strSearchSQLPrefix = strSearchSQLPrefix & " LEFT JOIN " & STR_TABLE_SHOPPER & " AS H ON H.intID = O.intShipShopperID"
	end if
end if
strSearchSQLPrefix = strSearchSQLPrefix & strBrandClause
strSearchSQLPrefix = strSearchSQLPrefix & " WHERE "
strSearchSQLPrefix = strSearchSQLPrefix & " O.chrStatus IN (" & strStatusClause & ")"
strSearchSQLSuffix = " ORDER BY O.intID"
'response.write Session(SESSION_MERCHANT_UID) & "<br />" & strsearchsqlprefix & "<br />"
Search_AddLabel "Order Information"
Search_AddNumberRange "#O.intID", "Order ID"
Search_AddDateRange "%O.dtmCreated", "Date Created"
Search_AddDateRange "%O.dtmUpdated", "Date Updated"
if strMode = "a" then
'	Search_AddText "O.vchCreatedByIP", 15, 15, "Created by IP"
'	Search_AddText "O.vchUpdatedByIP", 15, 15, "Updated by IP"
'	Search_AddText "O.vchCreatedByUser", 15, 32, "Created by User"
'	Search_AddText "O.vchUpdatedByUser", 15, 32, "Updated by User"
	Search_AddCheckboxGroup "+O.chrType", 4, dctOrderTypeValues, "Type"
	Search_AddCheckboxGroup "+O.chrStatus", 4, dctOrderStatusSearchValues, "Status"
end if
Search_AddText "O.vchShippingNumber", 32, 32, "Shipping/Conf Number"
Search_AddText "O.vchPaymentCardName", 24, 32, "Name on Card"
'if strMode = "a" then
'	Search_AddText "O.vchShopperID", 15, 15, "Shopper IP"
'	Search_AddText "O.vchShopperBrowser", 32, 64, "Shopper Browser"
'	Search_AddCheckboxGroup "+O.chrPaymentMethod", 2, dctPaymentMethod, "Payment Method"
'end if
Search_AddLabel "Shopper Information"
Search_AddText "S.vchLastName", 24, 32, "Last Name"
Search_AddText "S.vchFirstName", 24, 32, "First Name"

'Search_AddLabel "Credit Card Information"
'Search_AddText "O.vchPaymentCardType", 24, 32, "Card Type"
'Search_AddText "O.vchPaymentCardName", 24, 32, "Name on Card"
'Search_AddText "O.vchPaymentCardNumber", 24, 32, "Card Number"
'if strMode = "a" or strMode = "i" then
'	Search_AddDateRange "%O.vchPaymentCardExp", "Expiration Date"
'end if
'Search_AddLabel "Summary Information"
'if strMode = "a" then
'	Search_AddCheckboxGroup "+#O.intTaxZone", 2, GetTaxZoneList(), "Tax Zone"
'	Search_AddNumber "#O.mnyNonTaxableSubtotal", "Non-Taxable Subtotal"
'	Search_AddNumber "#O.mnyTaxableSubtotal", "Taxable Subtotal"
'	Search_AddNumber "#O.fltTaxRate", "Tax Rate"
'	Search_AddNumber "#O.mnyTaxAmount", "Tax Amount"
'	Search_AddNumber "#O.mnyShipAmount", "Shipping Amount"
'	Search_AddRadioGroup "+O.chrShipTaxFlag", 2, dctYesNoValues, "Shipping Tax Flag"
'end if
'Search_AddNumber "#O.mnyGrandTotal", "Grand Total"
	
InitSearchEngine

dim strSQL, rsData
if strAction = "search" or strAction = "export" then
	if strMode = "b" then
		strSQL = BuildBasicSearchSQL()
	else
		strSQL = BuildSearchSQL()
	end if
	'response.write strSQL & "<br />"
	if strSQL <> "" then
		set rsData = gobjConn.execute(strSQL)
	else
		strAction = ""
	end if
end if

intCurrentGroup = intNumGroups + 1
if intCurrentGroup > INT_MAX_SEARCH_GROUPS then
	intCurrentGroup = 0
end if

if strAction = "export" then
	DrawExport strFormat
else
	
	if strAction = "search" then
		DrawSearchResults rsData
	else
		DrawSearchPage "Orders"
	end if
	
end if

if IsObject(rsData) then
	rsData.close
	set rsData = nothing
end if

response.end

sub LoadPresetSearch(strPreset)
	strMode = "i"
	select case strPreset
		case "open":
			arySearchGroups(1,0).Add "+O.chrStatus", "n~S+A+Z+E+V"
		case "48hours":
			arySearchGroups(1,0).Add "O.dtmUpdated", "n~" & DateAdd("h", -48, Now()) & "~"
		case "72hours":
			arySearchGroups(1,0).Add "O.dtmUpdated", "n~" & DateAdd("h", -72, Now()) & "~"
		case "thisweek":
			arySearchGroups(1,0).Add "O.dtmUpdated", "n~" & DateAdd("d", 1-Weekday(Date()), Date()) & "~"
		case "thismonth":
			arySearchGroups(1,0).Add "O.dtmUpdated", "n~" & Month(Date()) & "/1/" & Year(Date()) & "~"
		case "thisyear":
			arySearchGroups(1,0).Add "O.dtmUpdated", "n~1/1/" & Year(Date()) & "~"
		case "all":
			arySearchGroups(1,0).Add "O.chrStatus", "~D"
		case else:	' invalid preset
			response.redirect "default.asp"
	end select
end sub

sub DrawSearchResults(rsInput)
	strPageTitle = "Search Results - Orders"
	DrawSearchListTitle strPageTitle
	DrawListHeader
	if rsInput.eof then
%>
<TR>
	<TD COLSPAN="5" ALIGN="center">No orders were found matching your search.</TD>
</TR>
<%
	else
		while not rsInput.eof
			DrawListItem rsInput
			rsInput.MoveNext
			if not rsInput.eof then
				DrawListItemSep
			end if
		wend
	end if
	DrawListFooter
end sub

sub DrawListHeader
%>
<!-- <TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVLtGrey) %>">
<TR>
	<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cWhite %>">
	<TR>
		<TD> --><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%">
		<TR BGCOLOR="<%= cVLtYellow %>">
			<TD><%= font(1) %>&nbsp;</TD>
			<TD NOWRAP><%= font(1) %><B>Order #</B></TD>
			<TD WIDTH="33%"><%= font(1) %><B>Shopper&nbsp;</B></TD>
			<TD NOWRAP><%= font(1) %><B>&nbsp;Submitted&nbsp;</B></TD>
			<TD NOWRAP ALIGN="right"><%= font(1) %><B>&nbsp;Amount&nbsp;</B></TD>
			<TD NOWRAP ALIGN="right"><%= font(1) %><B>Status</B></TD>
		</TR>
<%
end sub

sub DrawListItem(rsInput)
	dim intID, strName, mnyPrice, strStatus, dtmSubmitted, chrViewed, strColor
	intID = rsInput("intID")
	strName = rsInput("S_vchLastName") & ", " & rsInput("S_vchFirstName")
	mnyPrice = rsInput("mnyGrandTotal")
	strStatus = rsInput("chrStatus")
	dtmSubmitted = rsInput("dtmSubmitted")
	chrViewed = rsInput("chrViewed")
	if strStatus = "E" then
		strColor = "BGCOLOR=""#FFCCCC"""
	else
		strColor = ""
	end if
	'response.write "name: " & rsInput("S_vchLastName")
	'response.write "name: |" & rsInput("vchPaymentCardName") & "|"

	'THIS CODE IS HACKED. Since people can now check out without
	'creating an account I had to grab peoples name for the list from other places
	if len(strName) > 2  then
		'response.write "|" & len(strName) & "|"
	else
		'newer code
		if rsInput("intBillShopperID") <> "" Then
			strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE chrStatus<>'D' AND chrType='B' AND intID=" & rsInput("intBillShopperID") & " AND intShopperID=" & rsInput("intShopperID")
			'strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE  chrStatus<>'D' AND chrType='B' AND intShopperID=" & rsInput("intShopperID")
'			response.write strSQL & "<br />"

 			dim loopcrazyRS
			set loopcrazyRS = gobjConn.execute(strSQL)
			if not LoopCrazyRS.EOF then
				strName = loopcrazyRS("vchLastName") & ", " & loopcrazyRS("vchFirstName")
			end if 
			loopcrazyRS.close
			set loopcrazyRS = nothing
		end if
	end if
%>
		<TR VALIGN=BOTTOM <%= strColor %>>
			<TD ALIGN=CENTER><A HREF="<%= STR_DETAIL_SCRIPT %>id=<%= intID %>"><IMG SRC="<%= imagebase %>icon_order.gif" WIDTH="16" HEIGHT="16" ALT="View Order Details" BORDER="0"></A></TD>
			<TD NOWRAP><%= intID %></TD>
			<TD WIDTH="33%"><A HREF="<%= STR_DETAIL_SCRIPT %>id=<%= intID %>"><%= strName %></A></TD>
			<TD NOWRAP>&nbsp;<%= FormatDateTimeNoSeconds(dtmSubmitted) %>&nbsp;</TD>
			<TD NOWRAP ALIGN="right">&nbsp;<%= SafeFormatCurrency(fontx(1,1,cLight) & "n/a</FONT>", mnyPrice, 2) %>&nbsp;</TD>
			<TD NOWRAP ALIGN="right"><%= GetArrayValue(strStatus, dctOrderStatus) %></TD>
		</TR>
<%
end sub

sub DrawListItemSep
%>
		<TR>
			<TD COLSPAN=6><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVVLtGrey) %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
		</TR>
<%
end sub

sub DrawListFooter
%>
		</TABLE><!-- </TD>
	</TR>
	</TABLE></TD>
</TR>
</TABLE> -->
<%
end sub

sub DrawExport(strFormat)
	dim strFieldSep, strFieldQuote, strFieldAltQuote, strFileName, strData, strEOL, strGiftMessage
	response.ContentType = "text/csv"
	response.AddHeader "Content-transfer-encoding", "binary"
	if strFormat = "CSV" then
		strFileName = "orderexport.csv"
		strFieldSep = ","
		strFieldQuote = """"
		strFieldAltQuote = "'"
		strEOL = vbCrLf
	else
		strFileName = "orderexport.xls"
		strFieldSep = Chr(9)
		strFieldQuote = ""
		strFieldAltQuote = ""
		strEOL = vbCrLf
	End If
	response.AddHeader "Content-Disposition", "filename=" & strFileName

	if strFieldQuote <> "" then
		while not rsData.eof
			strGiftMessage = replace(rsData("txtGiftMessage") & "", vbCrLf, " ")

'			response.write strFieldQuote & replace(rsData("intID") & "", strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
'			response.write strFieldQuote & replace(replace(rsData("vchEmail") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("dtmCreated") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("dtmSubmitted") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("dtmUpdated") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write rsData("intID") & ","
			response.write rsData("intShopperID") & ","
			response.write strFieldQuote & replace(replace(rsData("S_vchLastName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("S_vchFirstName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("S_vchEmail") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("vchShippingNumber") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(GetArrayValue(rsData("chrStatus"), dctOrderStatus) & "", ",", " "), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","

		if blnIncludeBilling then
'			response.write rsData("intBillShopperID") & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchLastName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchFirstName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchAddress1") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchAddress2") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchCity") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchState") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchZip") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchDayPhone") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchEmail") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
		end if
		if blnIncludeShipping then
'			response.write rsData("intShipShopperID") & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchLastName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchFirstName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchAddress1") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchAddress2") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchCity") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchState") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchZip") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchDayPhone") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchEmail") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
		end if

'			response.write (rsData("mnyNonTaxSubTotal")+rsData("mnyTaxSubTotal")) & ","
'			response.write rsData("mnyTaxAmount") & ","
'			response.write rsData("mnyShipAmount") & ","
'			response.write rsData("mnyGrandTotal") & ","

			response.write strFieldQuote & replace(replace( strGiftMessage & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote
			response.write strEOL
			rsData.MoveNext
		wend
	else
		while not rsData.eof
			strGiftMessage = rsData("txtGiftMessage")

'			response.write rsData("intID") & ","
'			response.write replace(rsData("vchEmail") & "", ",", "") & ","
			response.write replace(rsData("S_vchLastName") & "", ",", "") & ","
			response.write replace(rsData("S_vchFirstName") & "", ",", "")
			response.write replace(rsData("S_vchEmail") & "", ",", "")
			response.write strEOL
			rsData.MoveNext
		wend
	end if
end sub

%>