<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: audit_list.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Display system audit events
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

dim dctAuditTypeValues
set dctAuditTypeValues = Server.CreateObject("Scripting.Dictionary")
	dctAuditTypeValues.Add "S", "Security"
	dctAuditTypeValues.Add "D", "Database"
	dctAuditTypeValues.Add "M", "Maintenance"
	dctAuditTypeValues.Add "U", "Action"
	dctAuditTypeValues.Add "E", "Error"

const STR_PAGE_TITLE = "System Audit Log"
const STR_PAGE_TYPE = "adminauditlist"

OpenConn

dim strSQL, rsData

' gather database statistics
dim intAuditCount
strSQL = "SELECT COUNT(*) AS intCount FROM " & STR_TABLE_AUDIT & " WHERE chrStatus='A' AND dtmCreated >= '" & DateAdd("h", -72, Now()) & "'"
set rsData = gobjConn.execute(strSQL)
intAuditCount = rsData("intCount")
rsData.Close
set rsData = nothing

strSQL = "SELECT intID, dtmCreated, vchCreatedByUser, vchCreatedByIP, chrType, vchAction, vchObject, vchDescription"
strSQL = strSQL & " FROM " & STR_TABLE_AUDIT
strSQL = strSQL & " WHERE chrStatus='A' AND dtmCreated >= '" & DateAdd("h", -72, Now()) & "'"
strSQL = strSQL & " ORDER BY intID DESC"
set rsData = gobjConn.execute(strSQL)


DrawPage


rsData.close
set rsData = nothing
response.end

sub DrawPage
	DrawFormHeader "100%", "System Audit Log", "For Past 72 Hours"
	DrawListHeader
	while not rsData.eof
		DrawListItem rsData("intID"), rsData("dtmCreated"), rsData("vchCreatedByUser"), rsData("vchCreatedByIP"), rsData("chrType"), rsData("vchAction"), rsData("vchDescription") & ""
		rsData.MoveNext
		if not rsData.eof then
			DrawListItemSep
		end if
	wend
	DrawListFooter
end sub

sub DrawListHeader
%>
<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVLtGrey) %>">
<TR>
	<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cWhite %>">
	<TR>
		<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%">
		<TR BGCOLOR="<%= cVLtYellow %>">
			<TD><%= font(1) %>&nbsp;</TD>
			<TD NOWRAP><%= font(1) %><B>Date/Time</B></TD>
			<TD NOWRAP><%= font(1) %><B>&nbsp;User&nbsp;</B></TD>
			<TD NOWRAP><%= font(1) %><B>&nbsp;User IP&nbsp;</B></TD>
			<TD NOWRAP><%= font(1) %><B>&nbsp;Type&nbsp;</B></TD>
			<TD NOWRAP><%= font(1) %><B>&nbsp;Action&nbsp;</B></TD>
			<TD NOWRAP><%= font(1) %><B>&nbsp;Description&nbsp;</B></TD>
		</TR>
<%
end sub

sub DrawListItem(intID, dtmEventTime, strUser, strUserIP, strType, strAction, strDescription)
	dim strColor
	if strAction = "loginfail" then
		strColor = "BGCOLOR=""" & cErrorBG & """"
	else
		strColor = ""
	end if
%>
		<TR VALIGN=BOTTOM <%= strColor %>>
			<TD ALIGN=CENTER><IMG SRC="<%= imagebase %>icon_auditevent.gif" WIDTH="16" HEIGHT="16" ALT="Event" BORDER="0"></TD>
			<TD><%= dtmEventTime %></TD>
			<TD NOWRAP>&nbsp;<%= strUser %>&nbsp;</TD>
			<TD NOWRAP>&nbsp;<%= strUserIP %>&nbsp;</TD>
			<TD NOWRAP>&nbsp;<%= GetArrayValue(strType, dctAuditTypeValues) %>&nbsp;</TD>
			<TD NOWRAP>&nbsp;<%= strAction %>&nbsp;</TD>
			<TD NOWRAP>&nbsp;<%= strDescription %></TD>
		</TR>
<%
end sub

sub DrawListItemSep
%>
		<TR>
			<TD COLSPAN=7><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVVLtGrey) %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
		</TR>
<%
end sub

sub DrawListFooter
%>
		</TABLE></TD>
	</TR>
	</TABLE></TD>
</TR>
</TABLE>
<%
end sub
%>