<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: order_transmanual.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Allow user to enter manual transactions (e.g. voice authorizations)
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_ORDER_TRANS_MANUAL) then
	response.redirect "menu.asp"
end if

const STR_PAGE_TITLE = "Manual Transaction"
const STR_PAGE_TYPE = "admintransmanual"

OpenConn

dim blnCanAuth, blnCanDeposit, blnCanCredit, blnCanManual, blnCanAuthDep
blnCanAuth = CheckUserAccess(ACC_ORDER_TRANS_AUTH)
blnCanDeposit = CheckUserAccess(ACC_ORDER_TRANS_DEPOSIT)
blnCanCredit = CheckUserAccess(ACC_ORDER_TRANS_CREDIT)
blnCanManual = CheckUserAccess(ACC_ORDER_TRANS_MANUAL)
blnCanAuthDep = CheckUserAccess(ACC_ORDER_TRANS_AUTHDEP)

dim intID, strAction
strAction = Request("action")
intID = Request("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if

dim STR_DETAIL_SCRIPT
STR_DETAIL_SCRIPT = "order_trans.asp?id=" & intID & "&"

dim mnyGrandTotal, vchPaymentCardName, mnyAuthTotal, mnyDepositTotal, mnyCreditTotal, blnManualTrans
dim strSQL, rsData
if intID > 0 then
	strSQL = "SELECT intID, mnyGrandTotal, vchPaymentCardName, "
	strSQL = strSQL & "(SELECT ISNULL(SUM(mnyTransAmount),0) FROM " & STR_TABLE_TRANS & " WHERE chrStatus='A' AND chrTransType IN ('AV','AS','ES','EV','DD') AND intOrderID=" & intID & ") AS mnyAuthTotal,"
	strSQL = strSQL & "(SELECT ISNULL(SUM(mnyTransAmount),0) FROM " & STR_TABLE_TRANS & " WHERE chrStatus='A' AND chrTransType IN ('ES','EV','DS','DD') AND intOrderID=" & intID & ") AS mnyDepositTotal,"
	strSQL = strSQL & "(SELECT ISNULL(SUM(mnyTransAmount),0) FROM " & STR_TABLE_TRANS & " WHERE chrStatus='A' AND chrTransType IN ('CR','DC') AND intOrderID=" & intID & ") AS mnyCreditTotal,"
	strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_TRANS & " WHERE chrStatus='A' AND chrType='M' AND intOrderID=" & intID & ") AS intManualTrans"
	strSQL = strSQL & " FROM " & STR_TABLE_ORDER & " WHERE intID=" & intID & " AND chrStatus<>'D'"
	'response.write strSQL & "<BR>"
	set rsData = gobjConn.execute(strSQL)
	if rsData.eof then
		rsData.close
		set rsData = nothing
		response.redirect "order_list.asp"
	end if
	mnyGrandTotal = rsData("mnyGrandTotal")
	vchPaymentCardName = rsData("vchPaymentCardName")
	mnyAuthTotal = rsData("mnyAuthTotal")
	mnyDepositTotal = rsData("mnyDepositTotal")
	mnyCreditTotal = rsData("mnyCreditTotal")
	blnManualTrans = (rsData("intManualTrans") > 0)
	rsData.close
	set rsData = nothing
else
	response.redirect "order_list.asp"
end if

if strAction = "submit" then
	dim strTransType, blnTransOK, mnyTransAmount, strNewStatus
	strTransType = ucase(Request("strTransType"))
	' validate transaction type
	select case strTransType
		case "AS":
			blnTransOK = blnCanAuth and (mnyAuthTotal = 0)
			mnyTransAmount = mnyGrandTotal
		case "DS":
			blnTransOK = blnCanDeposit and (mnyAuthTotal > 0) and (mnyDepositTotal = 0)
			mnyTransAmount = mnyGrandTotal
		case "ES", "EV":
			blnTransOK = blnCanAuthDep and (mnyAuthTotal = 0)
			mnyTransAmount = mnyGrandTotal
		case "CR":
			blnTransOK = blnCanCredit and (mnyDepositTotal - mnyCreditTotal > 0)
			mnyTransAmount = Request("mnyTransAmount")
		case else:
			blnTransOK = false
	end select
	if not blnTransOK then
		FormErrors.Add "strTransType", "This transaction is not permitted."
	else
		AutoCheck Request, ""
		if FormErrors.count = 0 then
			dim dctSaveList
			set dctSaveList = Server.CreateObject("Scripting.Dictionary")
				dctSaveList.Add "*@dtmCreated", "GETDATE()"
				dctSaveList.Add "@dtmUpdated", "GETDATE()"
				dctSaveList.Add "*vchCreatedByUser", Session(SESSION_MERCHANT_ULOGIN)
				dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
				dctSaveList.Add "*vchCreatedByIP", gstrUserIP
				dctSaveList.Add "vchUpdatedByIP", gstrUserIP
				dctSaveList.Add "chrType", "M"
				dctSaveList.Add "chrStatus", "A"
				dctSaveList.Add "#intOrderID", intID
				dctSaveList.Add "chrAccountType", "?"
				dctSaveList.Add "chrTransType", strTransType
				dctSaveList.Add "!vchAuthCode", ""
				dctSaveList.Add "!vchRefCode", ""
				dctSaveList.Add "!vchOrderCode", ""
				dctSaveList.Add "!%dtmTransDate", ""
				dctSaveList.Add "#mnyTransAmount", mnyTransAmount
			SaveDataRecord STR_TABLE_TRANS, Request, 0, dctSaveList
			strNewStatus = ""
			select case strTransType
				case "AS":
					strNewStatus = "Z"
				case "DS", "ES", "EV":
					strNewStatus = "X"
				case "CS":
					if mnyDepositTotal - mnyCreditTotal - mnyTransAmount <= 0 then
						strNewStatus = "C"
					end if
			end select
			if strNewStatus <> "" then
				UpdateOrderStatus_Other intID, strNewStatus
			end if
			response.redirect "order_trans.asp?id=" & intID
		end if
	end if
end if


DrawPage

response.end

sub DrawPage()
	dim dctTransList
	set dctTransList = Server.CreateObject("Scripting.Dictionary")
	if blnCanAuth and (mnyAuthTotal = 0) then
		dctTransList.Add "AS", "Authorize (" & FormatCurrency(mnyGrandTotal, 2) & ")"
	end if
	if blnCanDeposit and (mnyAuthTotal > 0) and (mnyDepositTotal = 0) then
		dctTransList.Add "DS", "Deposit (" & FormatCurrency(mnyGrandTotal, 2) & ")"
	end if
	if blnCanAuthDep and (mnyAuthTotal = 0) then
		dctTransList.Add "ES", "Authorize & Deposit (" & FormatCurrency(mnyGrandTotal, 2) & ")"
	end if
	if blnCanCredit and (mnyDepositTotal > 0) then
		dctTransList.Add "CR", "Credit (max " & FormatCurrency(mnyDepositTotal - mnyCreditTotal, 2) & ")"
	end if
%>
<FORM ACTION="order_transmanual.asp" METHOD="POST" NAME="frm">
<INPUT TYPE="hidden" NAME="action" VALUE="submit">
<INPUT TYPE="hidden" NAME="id" VALUE="<%= intID %>">
<%
	AddPageAction "Cancel", "order_trans.asp?id=" & intID, ""
	
	DrawFormHeader "100%", STR_PAGE_TITLE, ""
	
	stlBeginStdTable "100%"
	response.write "<TR><TD>"
	
	stlBeginFormSection "100%", 2
	stlArrayPulldown "strTransType", "", Request, dctTransList, "Transaction Type", "IsEmpty", "Missing Transaction Type"
	stlTextInput "dtmTransDate", 20, 20, Request, "Transaction Date/Time", "IsDate", "Invalid Transaction Date"
	if blnCanCredit and (mnyDepositTotal > 0) then
		stlTextInput "mnyTransAmount", 10, 10, Request, "Transaction Amount", "IsNumeric", "Invalid Transaction Amount"
	end if
	stlTextInput "vchAuthCode", 12, 12, Request, "Authorization Code", "", ""
	stlTextInput "vchRefCode", 12, 12, Request, "Reference Code", "", ""
	stlTextInput "vchOrderCode", 20, 20, Request, "ECHO Order Code", "", ""
	stlEndFormSection
	
	response.write "</TD></TR>"
	stlSubmit "Submit"
	
	stlEndStdTable
	response.write "</FORM>"
	
	SetFieldFocus "frm", "strTransType"
end sub
%>