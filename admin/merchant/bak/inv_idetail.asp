<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: inv_idetail.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Inventory Admin - details for items
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_INVENTORY_VIEW) then
	response.redirect "main.asp"
end if

dim blnCanEdit
blnCanEdit = CheckUserAccess(ACC_INVENTORY_EDIT)

const STR_PAGE_TITLE = "Item Details"
const STR_PAGE_TYPE = "admininvlist"

OpenConn

dim dctBrands, dctVideos
dim strSQL, rsRequestData, objUpload
Set objUpload = Server.CreateObject("SoftArtisans.FileUp")

'if lcase(strServerHost) = "awsdev" then _
'	objUpload.Path = "C:\


Set rsRequestData = objUpload.Form

dim intID, intParentID, strAction
strAction = RequestPoll("action")
intID = RequestPoll("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if

intParentID = RequestPoll("parentid")
if IsNumeric(intParentID) then
	intParentID = CLng(intParentID)
else
	intParentID = 0
end if

Dim strPromoCodes

strPromoCodes = RequestPoll("strPromoCodes")

if strAction = "delete" then
	if blnCanEdit then
		gobjConn.execute("UPDATE " & STR_TABLE_INVENTORY & " SET chrStatus='D' WHERE intID=" & intID)
	end if
	response.redirect "inv_list.asp?parentid=" & intParentID
elseif strAction = "setstatus" then
	if blnCanEdit then
		gobjConn.execute("UPDATE " & STR_TABLE_INVENTORY & " SET chrStatus='" & Request("status") & "' WHERE intID=" & intID)
	end if
	response.redirect "inv_list.asp?parentid=" & intParentID
elseif strAction = "submit" then
	if blnCanEdit then
		AutoCheck rsRequestData, ""
'		if RequestPoll("chrSoftFlag") = "Y" then
'			CheckField "vchSoftURL", "IsEmpty", rsRequestData, "Missing Download URL"
'		end if
		if RequestPoll("chrICFlag") = "Y" then
			if not FormErrors.Exists("intStock") then
				CheckField "intStock", "IsInt", rsRequestData, "Missing Qty"
			end if
		end if
		if FormErrors.count = 0 then
			dim rsTemp, intNewOrder
			strSQL = "SELECT ISNULL(MAX(intSortOrder),0) AS intMaxSortOrder FROM " & STR_TABLE_INVENTORY
			set rsTemp = gobjConn.execute(strSQL)
			intNewOrder = rsTemp("intMaxSortOrder") + 1
			rsTemp.close
			set rsTemp = nothing
			dim dctSaveList
			set dctSaveList = Server.CreateObject("Scripting.Dictionary")
				dctSaveList.Add "*@dtmCreated", "GETDATE()"
				dctSaveList.Add "@dtmUpdated", "GETDATE()"
				dctSaveList.Add "*vchCreatedByUser", Session(SESSION_MERCHANT_ULOGIN)
				dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
				dctSaveList.Add "*vchCreatedByIP", gstrUserIP
				dctSaveList.Add "vchUpdatedByIP", gstrUserIP
				dctSaveList.Add "chrType", "I"
				dctSaveList.Add "*chrStatus", "A"
				dctSaveList.Add "*#intParentID", intParentID
				dctSaveList.Add "*#intSortOrder", intNewOrder
				dctSaveList.Add "*#intHitCount", 0
				dctSaveList.Add "!vchItemName", ""
				dctSaveList.Add "!vchPartNumber", ""
				dctSaveList.Add "#mnyItemPrice", 0
				dctSaveList.Add "#mnyWholesale", 0
				dctSaveList.Add "#mnyShipPrice", 0
				dctSaveList.Add "!#fltShipWeight", ""
				dctSaveList.Add "$!chrTaxFlag", ""
				dctSaveList.Add "$!chrSpecial", ""
				dctSaveList.Add "$!chrICFlag", ""
				dctSaveList.Add "!#intStock", ""
				dctSaveList.Add "!#intLowStock", ""
				dctSaveList.Add "!#intHighStock", ""
				dctSaveList.Add "!#intMinQty", ""
				dctSaveList.Add "!#mnyMinPrice", ""
'				dctSaveList.Add "intLgImageWidth", ""  This will be used to store length of subscription
				dctSaveList.Add "!vchSoftURL", ""
				dctSaveList.Add "$!chrSoftFlag", ""
				dctSaveList.Add "!vchImageURL", ""
				dctSaveList.Add "!vchImageURL2", ""
				'dctSaveList.Add "$!chrICampaignsFlag", ""
				'dctSaveList.Add "!intICampaignsListID", ""
				'dctSaveList.Add "!vchICampaignsHash", ""
				dctSaveList.Add "!#intBrand", ""
				dctSaveList.Add "!vchOptionList1", ""
				dctSaveList.Add "!vchOptionList2", ""
				dctSaveList.Add "!vchOptionList3", ""
				' SWSCodeChange - added intForceShipMethod and chrForceSoloItem
				dctSaveList.Add "!#intForceShipMethod", ""
				dctSaveList.Add "!chrForceSoloItem", ""
				dctSaveList.Add "!txtDescription", ""
			intID = SaveDataRecord( STR_TABLE_INVENTORY, rsRequestData, intID, dctSaveList)
			dim fImage, uploadPath, IsImageGood, fImageOrient
			IsImageGood = false
			fImageOrient = 0
			If IsObject(objUpload.Form("ImageFile")) then
				if (objUpload.Form("ImageFile").TotalBytes <> 0) and (lcase(right(objUpload.Form("ImageFile").UserFilename, 4))=".jpg" or lcase(right(objUpload.Form("ImageFile").UserFilename, 4))=".gif") then
					fImage = "Photo" & intID & "." & Mid(objUpload.form("ImageFile").UserFilename, InstrRev(objUpload.form("ImageFile").UserFilename, "\") + 1)
					uploadPath = Server.MapPath(liveimagebase) & "\"
					objUpload.Form("ImageFile").SaveAs uploadPath & fImage
					if lcase(right(objUpload.Form("ImageFile").UserFilename, 4))=".gif" then 
						IsImageGood = true	' - GIF Files don't work with JDO
					else
						IsImageGood = SetImageReSize( uploadPath, fImage, fImageOrient)	' true
					end if
				end if
			end if
			if IsImageGood then
				dctSaveList.RemoveAll
				dctSaveList.Add "*@dtmCreated", "GETDATE()"
				dctSaveList.Add "vchImageURL", fImage
				SaveDataRecord STR_TABLE_INVENTORY, rsRequestData, intID, dctSaveList
			end if
			IsImageGood = false
			fImageOrient = 0
			If IsObject(objUpload.Form("ImageFile2")) then
				if (objUpload.Form("ImageFile2").TotalBytes <> 0) and (lcase(right(objUpload.Form("ImageFile2").UserFilename, 4))=".jpg" or lcase(right(objUpload.Form("ImageFile2").UserFilename, 4))=".gif") then
					fImage = "Photo" & intID & "." & Mid(objUpload.form("ImageFile2").UserFilename, InstrRev(objUpload.form("ImageFile2").UserFilename, "\") + 1)
					uploadPath = Server.MapPath(liveimagebase) & "\"
					objUpload.Form("ImageFile2").SaveAs uploadPath & fImage
					if lcase(right(objUpload.Form("ImageFile").UserFilename, 4))=".gif" then 
						IsImageGood = true	' - GIF Files don't work with JDO
					else
						IsImageGood = SetImageReSize( uploadPath, fImage, fImageOrient)	' true
					end if
				end if
			end if
			if IsImageGood then
				dctSaveList.RemoveAll
				dctSaveList.Add "*@dtmCreated", "GETDATE()"
				dctSaveList.Add "vchImageURL2", fImage
				SaveDataRecord STR_TABLE_INVENTORY, rsRequestData, intID, dctSaveList
			end if
			
			'///
			'/// Delete all the promocodes for this product
			'///
			gobjConn.Execute("DELETE FROM " & STR_TABLE_PROMO & " WHERE intProductID=" & intID & "")
			
			'///
			'/// Save the PromoCodes
			'///
			on error resume next
			Dim arrPromoCodes,PromoCode
			
			if inStr(strPromoCodes,",")>0 then
			
				arrPromoCodes = Split(strPromoCodes,",")
				
				
				for each PromoCode in arrPromoCodes
					gobjConn.Execute("INSERT INTO " & STR_TABLE_PROMO & " (intProductID,chrPromoCode,mnyAmount) VALUES (" & intID & ",'" & GetPromoCode(PromoCode) & "'," & GetAmount(PromoCode) & ")")
				next
			elseif inStr(strPromoCodes,"=")>0 then
				gobjConn.Execute("INSERT INTO " & STR_TABLE_PROMO & " (intProductID,chrPromoCode,mnyAmount) VALUES (" & intID & ",'" & GetPromoCode(strPromoCodes) & "'," & GetAmount(strPromoCodes) & ")")
			end if
			
			on error goto 0	
			
			response.redirect "inv_list.asp?parentid=" & intParentID
		else
			
			DrawPage rsRequestData
			
		end if
	else
		response.redirect "inv_list.asp?parentid=" & intParentID
	end if
else
	dim rsData, blnDataIsRS,rsPromoCodes
	if intID > 0 then
		strSQL = "SELECT * FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intID & " AND chrStatus<>'D'"
		set rsData = gobjConn.execute(strSQL)
		blnDataIsRS = true
		if rsData.eof then
			response.redirect "inv_list.asp"
		end if
		
		'///
		'/// J.Helms
		'/// Support for Promo Codes
		'///
		strSQL = "SELECT chrPromoCode,mnyAmount FROM " & STR_TABLE_PROMO & " WHERE intProductID=" & intID & ""
		set rsPromoCodes = gobjConn.execute(strSQL)
		
		strPromoCodes = "" 'init
		
		
		while not rsPromoCodes.eof 
			strPromoCodes=strPromoCodes & rsPromoCodes(0) & "=" & SafeFormatPercentage(rsPromoCodes(1)) & ","
			rsPromoCodes.MoveNext
		wend
		 
	else
		set rsData = rsRequestData
		blnDataIsRS = false
	end if
	
	
	DrawPage rsData
	
	
	if blnDataIsRS then
		rsData.close
		set rsData = nothing
	end if
end if
response.end


function GetPromoCode(PromoCode)
	GetPromoCode = Split(PromoCode,"=")(0)
End Function

Function GetAmount(PromoCode)
	Dim m_fltAmount,m_sPromoCode
					
	'///
	'/// We are assuming that they are using percents now
	'/// 
	'm_sPromoCode = Split(PromoCode,"=")(0)
	m_fltAmount = Split(PromoCode,"=")(1)
	
	m_fltAmount = Replace(m_fltAmount,"%","")
	m_fltAmount = CDBL(m_fltAmount)/100

	GetAmount = m_fltAmount

end function

sub DrawPage(rsInput)
	dim strTitle, strInfoText
	dim txtDescription
	txtDescription = rsInput("txtDescription") & ""
	intParentID = rsInput("intParentID")
	if intID > 0 then
		strTitle = "Edit Item"
	else
		strTitle = "Add Item"
	end if
%>
<FORM ACTION="inv_idetail.asp" METHOD="POST" NAME="frm" ENCTYPE="multipart/form-data">
<INPUT TYPE="hidden" NAME="action" VALUE="submit">
<INPUT TYPE="hidden" NAME="id" VALUE="<%= intID %>">
<INPUT TYPE="hidden" NAME="parentid" VALUE="<%= intParentID %>">
<%
	AddPageAction iif(blnCanEdit, "Cancel", "Return"), "inv_list.asp?parentid=" & intParentID, ""
	if blnCanEdit then
		if intID > 0 then
			if rsInput("chrStatus") <> "A" then
				AddPageAction "Activate", "inv_idetail.asp?action=setstatus&status=A&id=" & intID & "&parentid=" & intParentID, "Are you sure you want to activate this item?"
			end if
			if rsInput("chrStatus") <> "I" then
				AddPageAction "Deactivate", "inv_idetail.asp?action=setstatus&status=I&id=" & intID & "&parentid=" & intParentID, "Are you sure you want to deactivate this item?"
			end if
			AddPageAction "Delete", "inv_idetail.asp?action=delete&id=" & intID & "&intParentID=" & intParentID, "Are you sure you want to delete this item?"
		end if
	end if
	
	if intID > 0 then
		strInfoText = "<B>Record ID:</B> " & rsInput("intParentID") & "-" & rsInput("intID")
		strInfoText = strInfoText & "&nbsp; <B>Created:</B> " & rsInput("dtmCreated")
		strInfoText = strInfoText & "&nbsp; <B>Updated:</B> " & rsInput("dtmUpdated")
	else
		strInfoText = ""
	end if
	
	DrawFormHeader "100%", strTitle, strInfoText
	
	stlBeginStdTable "100%"
	response.write "<TR><TD>"
	
	if blnCanEdit then
		stlBeginFormSection "100%", 5
		stlTextInput "vchItemName", 20, 255, rsInput, "Item \Name", "IsEmpty", "Missing Item Name"
		stlRawCell "&nbsp;&nbsp;"
		stlTextInput "vchPartNumber", 14, 32, rsInput, "P\art Number", "", ""
		
		stlTextInput "mnyItemPrice", 8, 32, rsInput, "Item \Price", "IsNumeric", "Invalid Price"
		stlRawCell "&nbsp;&nbsp;"
		MakeDctBrands
		stlArrayPulldown "intBrand", "Select one...", rsInput, dctBrands, "Brand", "OptIsEmpty", "Select a Brand"

		'stlRawCellX "&nbsp;&nbsp;", "", "", "", 3
		'stlCheckbox "chrICFlag", "Y", rsInput, "Qty \Control", "yes"

		stlTextInput "mnyWholesale", 8, 8, rsInput, "\Wholesale Price", "OptIsNumeric", "Invalid Price"
		stlRawCell "&nbsp;&nbsp;"
		stlTextInput "intMinQty", 5, 5, rsInput, "\Min Order Qty", "OptIsInt", "Invalid Min Qty"
		
		stlRawCell "&nbsp;&nbsp;"
		stlRawCell "&nbsp;&nbsp;"
		stlRawCell "&nbsp;&nbsp;"
		stlTextInput "mnyMinPrice", 5, 5, rsInput, "\Min Order Price", "OptIsNumeric", "Invalid Min Price"
		
		stlTextInput "mnyShipPrice", 8, 8, rsInput, "\Shipping Price", "OptIsNumeric", "Invalid Price"
		stlRawCell "&nbsp;&nbsp;"
		stlCheckbox "chrICFlag", "Y", rsInput, "Qty \Control", "yes"
		
		stlTextInput "fltShipWeight", 8, 8, rsInput, "Shipping \Weight", "OptIsNumeric", "Invalid Weight"
		stlRawCell "&nbsp;&nbsp;"
		stlTextInput "intStock", 5, 5, rsInput, "\Qty in Stock", "OptIsInt", "Invalid Qty"
		
		stlTextInput "intLowStock", 5, 5, rsInput, "\Low Qty", "OptIsInt", "Invalid Qty"
		stlRawCell "&nbsp;&nbsp;"
		stlTextInput "intHighStock", 5, 5, rsInput, "\High Qty", "OptIsInt", "Invalid Qty"
		
		stlCheckbox "chrTaxFlag", "Y", rsInput, "\Taxable", "yes"
		stlRawCell "&nbsp;&nbsp;"
		stlCheckbox "chrSpecial", "Y", rsInput, "\Special", "yes"
		
		' SWSCodeChange - added intForceShipMethod and chrForceSoloItem
		stlArrayPulldown "intForceShipMethod", "None", rsInput, dctShipOption, "Require Shipping \Method", "", ""
		stlRawCell "&nbsp;&nbsp;"
		stlCheckbox "chrForceSoloItem", "Y", rsInput, "\Require Separate Order", "yes"
		
		stlEndFormSection
		
		stlRule
		
		stlBeginFormSection "100%", 2
		stlRawCell "For subscription items only:"
		stlRawCell "&nbsp;"
		'The following DB fields have been reclaimed(in the name of France!) for the following
		stlTextInput "vchSoftURL", 40, 255, rsInput, "Subscription URL", "", ""
'		To be added once subscription lengths are enabled
'		stlTextInput "intLgImageWidth", 5, 5, rsInput, "Number of days subscription is valid", "", ""
		stlEndFormSection
		
		stlRule
		
		'iCampaigns information added by Michael Hudson, 2006-07-28
		stlBeginFormSection "60%", 2
		stlRawCell "For iCampaigns items only:"
		stlRawCell "&nbsp;"

		stlCheckbox "chrICampaignsFlag", "Y", rsInput, "Enable iCampaigns Post", "yes"


		stlTextInput "intICampaignsListID", 40, 255, rsInput, "iCampaigns List ID", "", ""
		stlTextInput "vchICampaignsHash", 40, 255, rsInput, "iCampaigns List Hash", "", ""

		stlEndFormSection
		
		stlRule
		
		stlBeginFormSection "", 3
		if not IsNull(rsInput("vchImageURL")) then
			stlRawCell "Current Image:<BR>(click to view full size)"
'			stlRawCell "<A HREF=""" & imagebase & "products/small/" & rsInput("vchImageURL") & """ TARGET=""_blank""><IMG SRC=""" & imagebase & "products/small/" & rsInput("vchImageURL") & """ WIDTH=""50"" HEIGHT=""50"" ALT="""" BORDER=""0""></A>"
			stlRawCell "<A HREF=""" & imagebase & "products/big/" & rsInput("vchImageURL") & """ TARGET=""_blank""><IMG SRC=""" & imagebase & "products/big/" & rsInput("vchImageURL") & """ WIDTH=""50"" HEIGHT=""50"" ALT="""" BORDER=""0""></A>"
			if not IsNull(rsInput("vchImageURL2")) then
			stlRawCell "<A HREF=""" & imagebase & "products/big/" & rsInput("vchImageURL2") & """ TARGET=""_blank""><IMG SRC=""" & imagebase & "products/big/" & rsInput("vchImageURL2") & """ WIDTH=""50"" HEIGHT=""50"" ALT="""" BORDER=""0""></A>"
			else
			stlRawCell "&nbsp;&nbsp;"
			end if
'			stlRawCell " left shows thumbnail<br /> right shows big image"
		end if
		stlEndFormSection

		stlBeginFormSection "", 4
		stlTextInput "vchImageURL", 30, 255, rsInput, "\Image", "", ""
		stlFileInput "ImageFile", 30, 255, "", "or \Upload Image", "", ""
		stlTextInput "vchImageURL2", 30, 255, rsInput, "\Image2", "", ""
		stlFileInput "ImageFile2", 30, 255, "", "or \Upload Image2", "", ""
		stlEndFormSection

		stlRule
		
		stlBeginFormSection "100%", 2
		stlTextInput "vchOptionList1", 40, 255, rsInput, "Option List 1 (comma-sep)", "", ""
		stlTextInput "vchOptionList2", 40, 255, rsInput, "Option List 2 (comma-sep)", "", ""
		stlTextInput "vchOptionList3", 40, 255, rsInput, "Option List 3 (comma-sep)", "", ""
		stlEndFormSection
		
		stlRule
		
		stlBeginFormSection "100%", 2
		stlTextInput "strPromoCodes", 40, 255, strPromoCodes, "Promo Codes & Percent Discount (comma-sep)", "", ""
		stlEndFormSection
		
		stlRule
		
		response.write "<U>D</U>escription:<BR>"
		response.write "<CENTER><TEXTAREA NAME=""txtDescription"" ROWS=6 COLS=50 ACCESSKEY=""D"">" & Server.HTMLEncode(txtDescription) & "</TEXTAREA></CENTER>"

	else
		stlBeginFormSection "100%", 5
		stlStaticText "Item Name:", rsInput("vchItemName")
		stlRawCell "&nbsp;&nbsp;"
		stlStaticText "Part Number:", rsInput("vchPartNumber")
		
		stlStaticText "Item Price:", rsInput("mnyItemPrice")
		stlRawCell "&nbsp;&nbsp;"
		stlStaticText "Min Order Qty:", rsInput("intMinQty")

		stlStaticText "Wholesale Price:", SafeFormatCurrency("n/a", rsInput("mnyWholesale"), 2)
		stlRawCell "&nbsp;&nbsp;"
		stlStaticText "Qty Control:", iif(rsInput("chrICFlag") = "Y", "Yes", "No")
		
		stlStaticText "Shipping Price:", SafeFormatCurrency("n/a", rsInput("mnyShipPrice"), 2)
		stlRawCell "&nbsp;&nbsp;"
		stlStaticText "Qty in Stock:", rsInput("intStock")
		
		stlStaticText "Shipping Weight:", rsInput("fltShipWeight")
		stlRawCell "&nbsp;&nbsp;"
		
		stlStaticText "Low Qty:", rsInput("intLowStock")
		stlRawCell "&nbsp;&nbsp;"
		stlStaticText "High Qty:", rsInput("intHighStock")
		
		stlStaticText "Taxable:", iif(rsInput("chrTaxFlag") = "Y", "Yes", "No")
		stlRawCell "&nbsp;&nbsp;"
		stlStaticText "Special:", iif(rsInput("chrSpecial") = "Y", "Yes", "No")

		'stlRawCellX "&nbsp;&nbsp;", "", "", "", 3
		stlEndFormSection
		
		stlRule
'		
		stlBeginFormSection "100%", 1
		stlStaticText "Subscription URL:", rsInput("vchSoftURL")
'		To be added once subscription lengths are enabled
'		stlStaticText "Number of days subscription is valid:", rsInput("intLgImageWidth")
		stlEndFormSection
		
		stlRule
		
		stlBeginFormSection "100%", 2
		stlStaticText "Option List 1:", rsInput("vchOptionList1")
		stlStaticText "Option List 2:", rsInput("vchOptionList2")
		stlStaticText "Option List 3:", rsInput("vchOptionList3")
		stlEndFormSection
		
		stlRule
		
		response.write "Description:<BR>" & Server.HTMLEncode(txtDescription)
		
		stlRule
		
		stlBeginFormSection "", 3
		if not IsNull(rsInput("vchImageURL")) then
			stlRawCell "Current Image:<BR>(click to view full size)"
'			stlRawCell "<A HREF=""" & imagebase & "products/small/" & rsInput("vchImageURL") & """ TARGET=""_blank""><IMG SRC=""" & imagebase & "products/small/" & rsInput("vchImageURL") & """ WIDTH=""50"" HEIGHT=""50"" ALT="""" BORDER=""0""></A>"
			stlRawCell "<A HREF=""" & imagebase & "products/big/" & rsInput("vchImageURL") & """ TARGET=""_blank""><IMG SRC=""" & imagebase & "products/big/" & rsInput("vchImageURL") & """ WIDTH=""50"" HEIGHT=""50"" ALT="""" BORDER=""0""></A>"
			if not IsNull(rsInput("vchImageURL2")) then
			stlRawCell "<A HREF=""" & imagebase & "products/big/" & rsInput("vchImageURL2") & """ TARGET=""_blank""><IMG SRC=""" & imagebase & "products/big/" & rsInput("vchImageURL2") & """ WIDTH=""50"" HEIGHT=""50"" ALT="""" BORDER=""0""></A>"
			else
			stlRawCell "&nbsp;&nbsp;"
			end if
		else
			stlStaticText "Current Image:", "None"
		end if
		stlEndFormSection

	end if
'	response.write "</TD></TR>"
	stlSubmit iif(blnCanEdit, "Submit", "Return")
	
	stlEndStdTable
	response.write "</FORM>"
	
	if blnCanEdit then
		SetFieldFocus "frm", "vchItemName"
	end if
end sub

function RequestPoll (strField)
	Dim strValue
	strValue = rsRequestData(strField)
	If strValue = "" Then
		strValue = Request.QueryString(strField)
	end if
	RequestPoll = strValue
end function

sub MakeDctBrands
	dim SQL, rsTemp
	
	SQL = "SELECT intID, vchItemName FROM " & STR_TABLE_INVENTORY
	SQL = SQL & " WHERE chrStatus='A' AND chrType='B'"
	SQL = SQL & " ORDER BY vchItemName"
	
	set rsTemp = ConnOpenRS(SQL)

	if IsObject(dctBrands) then
		dctBrands.RemoveAll
	else
		set dctBrands = Server.CreateObject("Scripting.Dictionary")
	end if
	
	while not rsTemp.EOF
		dctBrands.Add cint(rsTemp("intID")&""), rsTemp("vchItemName")&""
		rsTemp.MoveNext
	wend
	
	rsTemp.close
	set rsTemp = nothing
	
end sub

function SetImageReSize( uploadPath, fImage, fImageOrient)

'dim fImage, uploadPath, IsImageGood
'IsImageGood = false
'fImage = "Photo" & intID & "." & Mid(objUpload.form("ImageFile").UserFilename, InstrRev(objUpload.form("ImageFile").UserFilename, "\") + 1)
'uploadPath = Server.MapPath(liveimagebase) & "\"
'if right(uploadPath,1) <> "\" then
'	uploadPath = uploadPath & "\"
'end if
if IsNull(fImageOrient) or fImageOrient = "" then
	fImageOrient = ""
end if

if IsNull(fImage) or fImage = "" then
	SetImageReSize = false
else
	' resize image here
	dim objExec, dnloadPath1, dnloadPath2, strExecResult1, strExecResult2, strSizeDim1, strSizeDim2
	Set objExec = Server.CreateObject("ASPExec.Execute")
	if fImageOrient = 3 then
		strSizeDim1 = "-rh 100 -rw 100"		' -rh 100 -rw 250
		strSizeDim2 = "-rh 300 -rw 750"
	else
		strSizeDim1 = "-rh 100 -rw 100"
		strSizeDim2 = "-rh 300 -rw 300"
	end if

	' 1st pass create thumbnail image
	dnloadPath1 = Server.MapPath(thumbimagebase) & "\"
	objExec.Application = "c:\com\jdo.exe -ss -q 75 " & strSizeDim1 & " -rb -op " & dnloadPath1 & " " & uploadpath & fImage
	objExec.Parameters = ""
	strExecResult1 = objExec.ExecuteDosApp

	' 2nd pass create photo image
	dnloadPath2 = Server.MapPath(photoimagebase) & "\"
	objExec.Application = "c:\com\jdo.exe -ss -q 85 " & strSizeDim2 & " -rb -op " & dnloadPath2 & " " & uploadpath & fImage
	objExec.Parameters = ""
	strExecResult2 = objExec.ExecuteDosApp

	' IsImageGood = true
	set objExec = Nothing
	SetImageReSize = true
end if

end function

Function SafeFormatPercentage(fltAmount)
	
	if IsNumeric(fltAmount)then
		fltAmount = CDBL(fltAmount)
		fltAmount = fltAmount*100
		SafeFormatPercentage = fltAmount & "%"
	else
		SafeFormatPercentage = fltAmount
	end if
	
End Function

%>