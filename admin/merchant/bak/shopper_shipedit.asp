<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: shopper_shipedit.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Merchant Shopper Admin - display/edit shopper billing and shipping addresses
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_SHOPPER_VIEW) then
	response.redirect "menu.asp"
end if

dim blnCanEdit
blnCanEdit = CheckUserAccess(ACC_SHOPPER_EDIT)

const STR_PAGE_TITLE = "Shopper Details"
const STR_PAGE_TYPE = "adminshopperlist"

OpenConn

dim intID, intShopperID, strAction
strAction = Request("postaction")
intID = Request("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if

intShopperID = Request("shopper")
if IsNumeric(intShopperID) then
	intShopperID = CLng(intShopperID)
else
	intShopperID = 0
end if

if intShopperID = 0 then
	response.redirect "shopper_list.asp"
end if

dim strType, strLabel
strType = ucase(Request("addtype"))
select case strType
	case "B":
		strLabel = "Billing"
	case "S":
		strLabel = "Shipping"
	case else:
		response.redirect "shopper_detail.asp?id=" & intShopperID
end select

if strAction = "delete" then
	if blnCanEdit then
		gobjConn.execute("UPDATE " & STR_TABLE_SHOPPER & " SET chrStatus='D' WHERE intID=" & intID)
	end if
	response.redirect "shopper_detail.asp?id=" & intShopperID
elseif strAction = "setstatus" then
	if blnCanEdit then
		gobjConn.execute("UPDATE " & STR_TABLE_SHOPPER & " SET chrStatus='" & Request("status") & "' WHERE intID=" & intID)
	end if
	response.redirect "shopper_detail.asp?id=" & intShopperID
elseif left(strAction,6) = "submit" then
	if blnCanEdit then
		AutoCheck Request, ""
		if Request("vchDayPhone") = "" and Request("vchNightPhone") = "" then
			FormErrors.Add "vchDayPhone", "Either day or night phone is required."
		end if
		
		if strType = "B" then
			if Request("chrPaymentMethod") = "OCC" then
				CheckField "vchCardType", "IsEmpty", Request, "Missing Card Type"
				CheckField "vchCardNumber", "IsEmpty", Request, "Invalid Card Number"
				CheckField "vchCardExtended", "OptIsNumeric", Request, "Invalid Card PIN"
				CheckField "chrCardExpMonth~chrCardExpYear", "CCExp", Request, "Invalid Expiration"
			elseif Request("chrPaymentMethod") = "OEC" then
				CheckField "vchCheckBankName", "IsEmpty", Request, "Missing Bank Name"
				CheckField "vchCheckRtnNumber", "IsNumeric", Request, "Invalid Routing #"
				CheckField "vchCheckDLNumber", "IsEmpty", Request, "Missing License"
				CheckField "vchCheckDLState", "IsEmpty", Request, "Missing License State"
				CheckField "vchCheckAcctNumber", "IsEmpty", Request, "Missing Account Number"
				CheckField "chrCheckAcctType", "IsEmpty", Request, "Missing Account Type"
			end if
		end if
		
		if FormErrors.count = 0 then
			dim dctSaveList
			set dctSaveList = Server.CreateObject("Scripting.Dictionary")
				dctSaveList.Add "*@dtmCreated", "GETDATE()"
				dctSaveList.Add "@dtmUpdated", "GETDATE()"
				dctSaveList.Add "*vchCreatedByUser", Session(SESSION_MERCHANT_ULOGIN)
				dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
				dctSaveList.Add "*vchCreatedByIP", gstrUserIP
				dctSaveList.Add "vchUpdatedByIP", gstrUserIP
				dctSaveList.Add "*chrType", strType
				dctSaveList.Add "*chrStatus", "A"
				dctSaveList.Add "*#intShopperID", intShopperID
				dctSaveList.Add "!vchLabel", ""
				dctSaveList.Add "!vchFirstName", ""
				dctSaveList.Add "!vchLastName", ""
				dctSaveList.Add "!vchCompany", ""
				dctSaveList.Add "!vchAddress1", ""
				dctSaveList.Add "!vchAddress2", ""
				dctSaveList.Add "!vchCity", ""
				dctSaveList.Add "!vchState", ""
				dctSaveList.Add "!vchZip", ""
				dctSaveList.Add "!vchDayPhone", ""
				dctSaveList.Add "!vchNightPhone", ""
				dctSaveList.Add "!vchFax", ""
				dctSaveList.Add "!vchEmail", ""
			if strType = "B" then
				dctSaveList.Add "!chrPaymentMethod", ""
				if Request("chrPaymentMethod") = "OCC" then
					dim s
					dctSaveList.Add "!vchCardType", ""
					dctSaveList.Add "!vchCardNumber", ""
					s = Request("chrCardExpMonth")
					if len(s) < 2 then
						s = string("0", 2 - len(s)) & s
					end if
					dctSaveList.Add "!vchCardExtended", ""
					dctSaveList.Add "chrCardExpMonth", s
					s = Request("chrCardExpYear")
					if len(s) = 2 then
						s = "20" & s
					end if
					dctSaveList.Add "chrCardExpYear", s
				else
					dctSaveList.Add "vchCardType", ""
					dctSaveList.Add "vchCardNumber", ""
					dctSaveList.Add "vchCardExtended", ""
					dctSaveList.Add "chrCardExpMonth", ""
					dctSaveList.Add "chrCardExpYear", ""
				end if
				if Request("chrPaymentMethod") = "OEC" then
					dctSaveList.Add "!vchCheckBankName", ""
					dctSaveList.Add "!vchCheckRtnNumber", ""
					dctSaveList.Add "!vchCheckDLNumber", ""
					dctSaveList.Add "!vchCheckDLState", ""
					dctSaveList.Add "!vchCheckAcctNumber", ""
					dctSaveList.Add "!chrCheckAcctType", ""
				else
					dctSaveList.Add "vchCheckBankName", ""
					dctSaveList.Add "vchCheckRtnNumber", ""
					dctSaveList.Add "vchCheckDLNumber", ""
					dctSaveList.Add "vchCheckDLState", ""
					dctSaveList.Add "vchCheckAcctNumber", ""
					dctSaveList.Add "chrCheckAcctType", ""
				end if
			end if
			intID = SaveDataRecord("" & STR_TABLE_SHOPPER, Request, intID, dctSaveList)
			response.redirect "shopper_detail.asp?id=" & intShopperID
		else
			
			DrawPage Request
			
		end if
	else
		response.redirect "shopper_detail.asp?id=" & intShopperID
	end if
else
	dim strSQL, rsData, blnDataIsRS
	if intID > 0 then
		strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & intID & " AND chrType='" & strType & "' AND intShopperID=" & intShopperID & " AND chrStatus<>'D'"
		set rsData = gobjConn.execute(strSQL)
		blnDataIsRS = true
		if rsData.eof then
			rsData.close
			set rsData = nothing
			response.redirect "shopper_detail.asp?" & intShopperID
		end if
	else
		set rsData = Request
		blnDataIsRS = false
	end if
	
	
	DrawPage rsData
	
	
	if blnDataIsRS then
		rsData.close
		set rsData = nothing
	end if
end if
response.end

sub DrawPage(rsInput)
	dim strTitle, strSQL, rsShopper, strInfoText
	if intID > 0 then
		strTitle = "Edit " & strLabel & " Address"
	else
		strTitle = "Add " & strLabel & " Address"
	end if
%>
<FORM ACTION="shopper_shipedit.asp" METHOD="POST" NAME="frm">
<%
	HiddenInput "postaction", "submit"
	HiddenInput "addtype", strType
	HiddenInput "id", intID
	HiddenInput "shopper", intShopperID
	
	strSQL = "SELECT vchLastName, vchFirstName FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & intShopperID
	set rsShopper = gobjConn.execute(strSQL)
	strTitle = strTitle & " for " & rsShopper("vchLastName") & ", " & rsShopper("vchFirstName")
	rsShopper.close
	set rsShopper = nothing
	
	AddPageAction iif(blnCanEdit, "Cancel", "Return"), "shopper_detail.asp?id=" & intShopperID, ""
	if blnCanEdit then
		if intID > 0 then
			if rsInput("chrStatus") <> "A" then
				AddPageAction "Activate", "shopper_shipedit.asp?postaction=setstatus&status=A&shopper=" & intShopperID & "&id=" & intID & "&addtype=" & strType, "Are you sure you want to activate this address?"
			end if
			if rsInput("chrStatus") <> "I" then
				AddPageAction "Deactivate", "shopper_shipedit.asp?postaction=setstatus&status=I&shopper=" & intShopperID & "&id=" & intID & "&addtype=" & strType, "Are you sure you want to deactivate this address?"
			end if
'			AddPageAction "Delete", "shopper_shipedit.asp?postaction=delete&shopper=" & intShopperID & "&id=" & intID & "&addtype=" & strType, "Are you sure you want to delete this address?"
		end if
	end if
	
	if intID > 0 then
		strInfoText = "<B>Record ID:</B> " & rsInput("intID")
		strInfoText = strInfoText & "&nbsp; <B>Created:</B> " & rsInput("dtmCreated")
		strInfoText = strInfoText & "&nbsp; <B>Updated:</B> " & rsInput("dtmUpdated")
	else
		strInfoText = ""
	end if
	
	DrawFormHeader "100%", strTitle, strInfoText
	
	stlBeginStdTable "100%"
	response.write "<TR><TD>"
	
	if blnCanEdit then
		response.write "<B>Description for this Address</B>"
		stlBeginFormSection "", 2
		stlTextInput "vchLabel", 32, 32, rsInput, "\Label", "IsEmpty", "Missing Label"
		stlEndFormSection
		
		stlRule
		
		response.write "<B>Address Information</B>"
		stlBeginFormSection "", 5
		
		stlTextInputX "vchLastName", 20, 32, rsInput, "\Name", "IsEmpty", "Missing Last Name", "(Last)", 1
		stlTextInputX "vchFirstName", 20, 32, rsInput, "-", "IsEmpty", "Missing First Name", "(First)", 3
		stlResetRow
		
		stlTextInputX "vchCompany", 32, 50, rsInput, "\Company", "", "", "", 4
		stlResetRow
		
		stlTextInputX "vchAddress1", 32, 50, rsInput, "\Address", "IsEmpty", "Missing Street Address", "", 4
		stlResetRow
		
		stlTextInputX "vchAddress2", 32, 50, rsInput, "", "", "", "", 4
		stlResetRow
		
		stlTextInputX "vchCity", 32, 32, rsInput, "", "IsEmpty", "Missing City", "(city)", 2
		stlTextInputX "vchState", 5, 32, rsInput, "-", "IsEmpty", "Missing State", "(st)", 1
		stlTextInputX "vchZip", 12, 10, rsInput, "-", "IsEmpty", "Missing Zip", "(zip)", 1
		stlResetRow
		
		stlTextInputX "vchDayPhone", 20, 20, rsInput, "\Day&nbsp;Phone", "", "", "", 2
		stlTextInputX "vchNightPHone", 20, 20, rsInput, "Night \Phone", "", "", "", 2
		stlResetRow
		
		stlTextInputX "vchFax", 20, 20, rsInput, "\Fax", "", "", "", 2
		stlTextInputX "vchEmail", 20, 50, rsInput, "\Email", "", "", "", 2
		stlEndFormSection
		
		if strType = "B" then
			response.write "<B>Payment Information</B>"
			stlBeginFormSection "", 5
			
			stlArrayPulldownX "chrPaymentMethod", 1, "None", rsInput, dctPaymentMethod, "Payment Method", "", "", "", 4
			
			stlRawCellX "<B>Credit Card</B>", "", "", "", 5
			stlTextInputX "vchCardType", 12, 12, rsInput, "Card Type", "", "", "", 4
			stlTextInputX "vchCardNumber", 32, 32, rsInput, "Card Number", "", "", "", 4
			stlTextInputX "vchCardExtended", 8, 8, rsInput, "Card PIN", "", "", "", 4
			stlTextInputX "chrCardExpMonth", 2, 2, rsInput, "Card Expiration", "", "", "(mm)", 1
			stlTextInputX "chrCardExpYear", 4, 4, rsInput, "-", "", "", "(yyyy)", 3
			
			stlRawCellX "<B>Electronic Check</B>", "", "", "", 5
			stlTextInputX "vchCheckBankName", 32, 32, rsInput, "Bank Name", "", "", "", 2
			stlTextInputX "vchCheckRtnNumber", 9, 9, rsInput, "Routing #", "", "", "", 1
			stlTextInputX "vchCheckDLNumber", 32, 32, rsInput, "Driver's License #", "", "", "", 2
			stlTextInputX "vchCheckDLState", 2, 2, rsInput, "-", "", "", "(state)", 2
			stlTextInputX "vchCheckAcctNumber", 20, 20, rsInput, "Checking Account #", "", "", "", 1
			stlArrayPulldownX "chrCheckAcctType", 1, "Select...", rsInput, dctCheckAcctTypeValues, "-", "", "", "", 1
			
			stlEndFormSection
		end if
	else
		response.write "<B>Description for this Address</B>"
		stlBeginFormSection "", 2
		stlStaticText "Label:", rsInput("vchLabel")
		stlEndFormSection
		
		stlRule
		
		response.write "<B>Address Information</B>"
		stlBeginFormSection "", 2
		
		stlStaticText "Name:", rsInput("vchLastName") & ", " & rsInput("vchFirstName")
		stlStaticText "Company:", rsInput("vchCompany")
		stlStaticText "Address:", rsInput("vchAddress1")
		stlStaticText "", rsInput("vchAddress2")
		stlStaticText "", rsInput("vchCity") & ", " & rsInput("vchState") & " " & rsInput("vchZip")
		stlStaticText "Day Phone:", rsInput("vchDayPhone")
		stlStaticText "Night Phone:", rsInput("vchNightPHone")
		stlStaticText "Fax:", rsInput("vchFax")
		stlStaticText "Email:", rsInput("vchEmail")
		stlEndFormSection
		
		if strType = "B" then
			response.write "<B>Payment Information</B>"
			stlBeginFormSection "", 2
			
			stlStaticText "Payment Method:", GetArrayValue(rsInput("chrPaymentMethod"), dctPaymentMethod)
			stlRawCellX "<B>Credit Card</B>", "", "", "", 2
			stlStaticText "Card Type:", rsInput("vchCardType")
			stlStaticText "Card Number:", rsInput("vchCardNumber")
			stlStaticText "Card Expiration:", rsInput("chrCardExpMonth") & "/" & rsInput("chrCardExpYear")
			
			stlRawCellX "<B>Electronic Check</B>", "", "", "", 2
			stlStaticText "Bank Name:", rsInput("vchCheckBankName")
			stlStaticText "Routing #:", rsInput("vchCheckRtnNumber")
			stlStaticText "Drivers License #:", rsInput("vchCheckDLNumber") & " (" & rsInput("vchCheckDLState") & ")"
			stlStaticText "Account #:", rsInput("vchCheckAcctNumber") & " (" & GetArrayValue(rsInput("chrCheckAcctType"), dctCheckAcctTypeValues) & ")"
			stlEndFormSection
		end if
	end if
	
	response.write "</TD></TR>"
	stlSubmit iif(blnCanEdit, "Submit", "Return")
	
	stlEndStdTable
	response.write "</FORM>"
	
	if blnCanEdit then
		SetFieldFocus "frm", "vchLabel"
	end if
end sub
%>