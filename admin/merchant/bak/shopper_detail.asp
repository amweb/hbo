<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: shopper_detail.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Merchant Shopper Admin
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_SHOPPER_VIEW) then
	response.redirect "menu.asp"
end if

dim blnCanEdit
blnCanEdit = CheckUserAccess(ACC_SHOPPER_EDIT)

const STR_PAGE_TITLE = "Shopper Details"
const STR_PAGE_TYPE = "adminshopperlist"

OpenConn

dim strPass

dim blnShowBilling, blnShowShipping
blnShowBilling = (Request("showbilling") = "1")
blnShowShipping = (Request("showshipping") = "1")

dim strSQL, rsData

dim intID, strAction
strAction = Request("postaction")
intID = Request("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if

if Request("btnSubmitAddress") <> "" then
	strAction = "submitadd"
end if

if strAction = "delete" then
	if blnCanEdit then
		gobjConn.execute("UPDATE " & STR_TABLE_SHOPPER & " SET chrStatus='D' WHERE intID=" & intID)
	end if
	response.redirect "shopper_list.asp"
elseif strAction = "setstatus" then
	if blnCanEdit then
		gobjConn.execute("UPDATE " & STR_TABLE_SHOPPER & " SET chrStatus='" & Request("status") & "' WHERE intID=" & intID)
	end if
	response.redirect "shopper_list.asp"
elseif left(strAction,6) = "submit" then
	if blnCanEdit then
		AutoCheck Request, ""
		if not FormErrors.Exists("vchPassword") then
			if Request("vchPassword") <> "" then
				if Request("vchPassword") <> Request("cnf_vchPassword") then
					FormErrors.Add "vchPassword", "Password did not match."
				end if
			elseif intID = 0 then
				FormErrors.Add "vchPassword", "Missing Password"
			end if
		end if
		if not FormErrors.Exists("vchEmail") and Request("vchEmail") <> "" then
			strSQL = "SELECT intID FROM " & STR_TABLE_SHOPPER & " WHERE chrType='A' AND chrStatus<>'D' AND vchEmail='" & SQLEncode(Request("vchEmail")) & "'"
			if intID <> 0 then
				strSQL = strSQL & " AND intID<>" & intID
			end if
			set rsData = gobjConn.execute(strSQL)
			if not rsData.eof then
				FormErrors.Add "vchEmail", "Another account already exists with this email address."
			end if
			rsData.close
			set rsData = nothing
		end if
		
		if FormErrors.count = 0 then
			dim dctSaveList
			set dctSaveList = Server.CreateObject("Scripting.Dictionary")
				dctSaveList.Add "*@dtmCreated", "GETDATE()"
				dctSaveList.Add "@dtmUpdated", "GETDATE()"
				dctSaveList.Add "*vchCreatedByUser", Session(SESSION_MERCHANT_ULOGIN)
				dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
				dctSaveList.Add "*vchCreatedByIP", gstrUserIP
				dctSaveList.Add "vchUpdatedByIP", gstrUserIP
				dctSaveList.Add "*chrType", "A"
				dctSaveList.Add "*chrStatus", "A"
'				dctSaveList.Add "!vchUsername", ""
				if Request("vchPassword") <> "" then
					dctSaveList.Add "!vchPassword", ""
				end if
				dctSaveList.Add "!vchHint", ""
				dctSaveList.Add "!vchFirstName", ""
				dctSaveList.Add "!vchLastName", ""
				dctSaveList.Add "!vchCompany", ""
				dctSaveList.Add "!vchEmail", ""
			intID = SaveDataRecord("" & STR_TABLE_SHOPPER, Request, intID, dctSaveList)
			if strAction = "submitadd" then
				' user wants to add a shipping address
				response.redirect "shopper_shipedit.asp?shopper=" & intID & "&addtype=" & Request("addtype")
			elseif strAction = "submiteditadd" then
				' user want sto edit a shipping address
				response.redirect "shopper_shipedit.asp?shopper=" & intID & "&id=" & Request("shipid") & "&addtype=" & Request("addtype")
			end if
			response.redirect "shopper_list.asp"
		else
			strPass = Request("vchPassword")
			
			DrawPage Request
			
		end if
	else
		if strAction = "submiteditadd" then
			' user want sto edit a shipping address
			response.redirect "shopper_shipedit.asp?shopper=" & intID & "&id=" & Request("shipid") & "&addtype=" & Request("addtype")
		else
			response.redirect "shopper_list.asp"
		end if
	end if
elseif strAction = "redraw" then
	strPass = Request("vchPassword")
	
	DrawPage Request
	
else
	dim blnDataIsRS
	if intID > 0 then
		strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & intID & " AND chrStatus<>'D'"
		set rsData = gobjConn.execute(strSQL)
		blnDataIsRS = true
		if rsData.eof then
			response.redirect "shopper_list.asp"
		end if
		strPass = ""
	else
		set rsData = Request
		strPass = ""
		blnDataIsRS = false
	end if
	
	
	DrawPage rsData
	
	
	if blnDataIsRS then
		rsData.close
		set rsData = nothing
	end if
end if
response.end

sub DrawPage(rsInput)
	dim strTitle, strInfoText
	if intID > 0 then
		strTitle = "Edit Shopper"
	else
		strTitle = "Add Shopper"
	end if
%>
<FORM ACTION="shopper_detail.asp" METHOD="POST" NAME="frm">
<%
	HiddenInput "postaction", "submit"
	HiddenInput "addtype", ""
	HiddenInput "id", intID
	HiddenInput "shipid", ""
	HiddenInput "dtmCreated", rsInput("dtmCreated") & ""
	HiddenInput "dtmUpdated", rsInput("dtmUpdated") & ""
	HiddenInput "showbilling", iif(blnShowBilling, "1", "0")
	HiddenInput "showshipping", iif(blnShowShipping, "1", "0")
	AddPageAction iif(blnCanEdit, "Cancel", "Return"), "shopper_list.asp", ""
	if blnCanEdit then
		if intID > 0 then
			if rsInput("chrStatus") <> "A" then
				AddPageAction "Activate", "shopper_detail.asp?postaction=setstatus&status=A&id=" & intID, "Are you sure you want to activate this shopper?"
			end if
			if rsInput("chrStatus") <> "I" then
				AddPageAction "Deactivate", "shopper_detail.asp?postaction=setstatus&status=I&id=" & intID, "Are you sure you want to deactivate this shopper?"
			end if
'			AddPageAction "Delete", "shopper_detail.asp?postaction=delete&id=" & intID, "Are you sure you want to delete this shopper?"
		end if
	end if
	
	if intID > 0 then
		strInfoText = "<B>Record ID:</B> " & intID
		strInfoText = strInfoText & "&nbsp; <B>Created:</B> " & rsInput("dtmCreated")
		strInfoText = strInfoText & "&nbsp; <B>Updated:</B> " & rsInput("dtmUpdated")
	else
		strInfoText = ""
	end if
	
	DrawFormHeader "100%", strTitle, strInfoText
	
	stlBeginStdTable "100%"
	response.write "<TR><TD>"
	
	if blnCanEdit then
		response.write "<B>Account Information</B>"
		stlBeginFormSection "", 5
		stlTextInputX "vchEmail", 32, 50, rsInput, "\Email", "OptIsEmail", "Invalid Email Address", "", 2
		if not IsNull(rsInput("vchAOL_ID")) then
			stlStaticText "AOL ID:", rsInput("vchAOL_ID")
		end if
		'stlTextInput "vchUsername", 20, 20, rsInput, "\Username", "IsEmpty", "Missing Username"
		stlResetRow
		
		stlPasswordInput "vchPassword", 20, 20, strPass, "\Password", iif(intID = 0, "IsEmpty", ""), "Missing Password"
		stlRawCell "&nbsp;&nbsp;"
		stlPasswordInput "cnf_vchPassword", 20, 20, "", "Confirm", "", ""
		
		stlTextInput "vchHint", 32, 32, rsInput, "\Hint", "OptIsEmpty", "Missing Hint"
		stlEndFormSection
		
		stlRule
		
		stlBeginFormSection "", 4
		
		stlTextInputX "vchLastName", 20, 32, rsInput, "\Name", "IsEmpty", "Missing Last Name", "(Last)", 1
		stlTextInputX "vchFirstName", 20, 32, rsInput, "-", "IsEmpty", "Missing First Name", "(First)", 2
		stlResetRow
		
		stlTextInputX "vchCompany", 25, 32, rsInput, "\Company", "", "", "", 3
		stlResetRow
		
		stlEndFormSection
		
		stlRule
		
		response.write "<B>Billing Profiles</B> - "
		response.write "<A HREF=""#"" onClick=""document.frm.postaction.value='redraw'; document.frm.showbilling.value='" & iif(blnShowBilling, "0", "1") & "'; document.frm.submit(); return false;"">" & iif(blnShowBilling, "Hide", "Show") & " Inactive</A><BR>"
		if intID > 0 then
			dim strSQL, rsShipAddress
			strSQL = "SELECT intID, chrStatus, vchLabel, vchFirstName, vchLastName, vchCity, vchState"
			strSQL = strSQL & " FROM " & STR_TABLE_SHOPPER
			strSQL = strSQL & " WHERE chrType='B'"
			if blnShowBilling then
				strSQL = strSQL & " AND chrStatus<>'D'"
			else
				strSQL = strSQL & " AND chrStatus='A'"
			end if
			strSQL = strSQL & " AND intShopperID=" & intID
			set rsShipAddress = gobjConn.execute(strSQL)
			if rsShipAddress.eof then
				response.write "No alternate addresses on file.<BR>"
			else
				stlBeginFormSection "", 4
				while not rsShipAddress.eof
					stlRawCell "<B>" & rsShipAddress("vchLabel") & "</B>"
					stlRawCell rsShipAddress("vchFirstName") & " " & rsShipAddress("vchLastName") & ", " & rsShipAddress("vchCity") & ", " & rsShipAddress("vchState")
					stlRawCell "<B>" & GetStatusStr(rsShipAddress("chrStatus"), true) & "</B>"
					stlRawCell "[<A HREF=""#"" onClick=""document.frm.postaction.value='submiteditadd'; document.frm.addtype.value='B'; document.frm.shipid.value='" & rsShipAddress("intID") & "'; document.frm.submit(); return false;"">Edit</A>]"
					rsShipAddress.MoveNext
				wend
				stlEndFormSection
			end if
			
			rsShipAddress.close
			set rsShipAddress = nothing
		else
			response.write "No alternate addresses on file.<BR>"
		end if
		response.write "<A HREF=""#"" onClick=""document.frm.postaction.value='submitadd'; document.frm.addtype.value='B'; document.frm.submit(); return false;"">Add Address</A><BR>"
		
		stlRule
		
		response.write "<B>Shipping Profiles</B> - "
		response.write "<A HREF=""#"" onClick=""document.frm.postaction.value='redraw'; document.frm.showshipping.value='" & iif(blnShowShipping, "0", "1") & "'; document.frm.submit(); return false;"">" & iif(blnShowShipping, "Hide", "Show") & " Inactive</A><BR>"
		if intID > 0 then
			strSQL = "SELECT intID, chrStatus, vchLabel, vchFirstName, vchLastName, vchCity, vchState"
			strSQL = strSQL & " FROM " & STR_TABLE_SHOPPER
			strSQL = strSQL & " WHERE chrType='S'"
			if blnShowShipping then
				strSQL = strSQL & " AND chrStatus<>'D'"
			else
				strSQL = strSQL & " AND chrStatus='A'"
			end if
			strSQL = strSQL & " AND intShopperID=" & intID
			set rsShipAddress = gobjConn.execute(strSQL)
			if rsShipAddress.eof then
				response.write "No alternate addresses on file.<BR>"
			else
				stlBeginFormSection "", 4
				while not rsShipAddress.eof
					stlRawCell "<B>" & rsShipAddress("vchLabel") & "</B>"
					stlRawCell rsShipAddress("vchFirstName") & " " & rsShipAddress("vchLastName") & ", " & rsShipAddress("vchCity") & ", " & rsShipAddress("vchState")
					stlRawCell "<B>" & GetStatusStr(rsShipAddress("chrStatus"), true) & "</B>"
					stlRawCell "[<A HREF=""#"" onClick=""document.frm.postaction.value='submiteditadd'; document.frm.addtype.value='S'; document.frm.shipid.value='" & rsShipAddress("intID") & "'; document.frm.submit(); return false;"">Edit</A>]"
					rsShipAddress.MoveNext
				wend
				stlEndFormSection
			end if
			
			rsShipAddress.close
			set rsShipAddress = nothing
		else
			response.write "No alternate addresses on file.<BR>"
		end if
		response.write "<A HREF=""#"" onClick=""document.frm.postaction.value='submitadd'; document.frm.addtype.value='S'; document.frm.submit(); return false;"">Add Address</A><BR>"
		response.write "<CENTER>"
		response.write "</CENTER>"
	else
		response.write "<B>Account Information</B>"
		stlBeginFormSection "", 5
		stlStaticText "Email:", rsInput("vchEmail")
		if not IsNull(rsInput("vchAOL_ID")) then
			stlStaticText "AOL ID:", rsInput("vchAOL_ID")
		end if
		'stlStaticText "Username:", rsInput("vchUsername")
		stlResetRow
		
		stlStaticText "Hint:", rsInput("vchHint")
		stlEndFormSection
		
		stlRule
		
		stlBeginFormSection "", 4
		
		stlStaticText "Name:", rsInput("vchLastName") & ", " & rsInput("vchFirstName")
		stlResetRow
		
		stlStaticText "Company:", rsInput("vchCompany")
		stlResetRow
		
		stlEndFormSection
		
		stlRule
		
		response.write "<B>Billing Profiles</B><BR>"
		if intID > 0 then
			strSQL = "SELECT intID, chrStatus, vchLabel, vchFirstName, vchLastName, vchCity, vchState"
			strSQL = strSQL & " FROM " & STR_TABLE_SHOPPER
			strSQL = strSQL & " WHERE chrType='B' AND chrStatus<>'D' AND intShopperID=" & intID
			set rsShipAddress = gobjConn.execute(strSQL)
			if rsShipAddress.eof then
				response.write "No alternate addresses on file.<BR>"
			else
				stlBeginFormSection "", 4
				while not rsShipAddress.eof
					stlRawCell "<B>" & rsShipAddress("vchLabel") & "</B>"
					stlRawCell rsShipAddress("vchFirstName") & " " & rsShipAddress("vchLastName") & ", " & rsShipAddress("vchCity") & ", " & rsShipAddress("vchState")
					stlRawCell "<B>" & GetStatusStr(rsShipAddress("chrStatus"), true) & "</B>"
					stlRawCell "[<A HREF=""#"" onClick=""document.frm.postaction.value='submiteditadd'; document.frm.addtype.value='B'; document.frm.shipid.value='" & rsShipAddress("intID") & "'; document.frm.submit(); return false;"">View</A>]"
					rsShipAddress.MoveNext
				wend
				stlEndFormSection
			end if
			
			rsShipAddress.close
			set rsShipAddress = nothing
		else
			response.write "No alternate addresses on file.<BR>"
		end if
		
		stlRule
		
		response.write "<B>Alternate Shipping Profiles</B><BR>"
		if intID > 0 then
			strSQL = "SELECT intID, chrStatus, vchLabel, vchFirstName, vchLastName, vchCity, vchState"
			strSQL = strSQL & " FROM " & STR_TABLE_SHOPPER
			strSQL = strSQL & " WHERE chrType='S' AND chrStatus<>'D' AND intShopperID=" & intID
			set rsShipAddress = gobjConn.execute(strSQL)
			if rsShipAddress.eof then
				response.write "No alternate addresses on file.<BR>"
			else
				stlBeginFormSection "", 4
				while not rsShipAddress.eof
					stlRawCell "<B>" & rsShipAddress("vchLabel") & "</B>"
					stlRawCell rsShipAddress("vchFirstName") & " " & rsShipAddress("vchLastName") & ", " & rsShipAddress("vchCity") & ", " & rsShipAddress("vchState")
					stlRawCell "<B>" & GetStatusStr(rsShipAddress("chrStatus"), true) & "</B>"
					stlRawCell "[<A HREF=""#"" onClick=""document.frm.postaction.value='submiteditadd'; document.frm.addtype.value='S'; document.frm.shipid.value='" & rsShipAddress("intID") & "'; document.frm.submit(); return false;"">View</A>]"
					rsShipAddress.MoveNext
				wend
				stlEndFormSection
			end if
			
			rsShipAddress.close
			set rsShipAddress = nothing
		else
			response.write "No alternate addresses on file.<BR>"
		end if
	end if
	
	response.write "</TD></TR>"
	stlSubmitX "btnSubmit", iif(blnCanEdit, "Submit", "Return")
	
	stlEndStdTable
	response.write "</FORM>"
	
	if blnCanEdit then
		SetFieldFocus "frm", "vchEmail"
	end if
end sub
%>