<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<!--#include file="../config/incSearch.asp"-->
<%

' =====================================================================================
' = File: inv_search.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Inventory Admin
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_INVENTORY_VIEW) then
	response.redirect "main.asp"
end if

dim strPageTitle
strPageTitle = "Inventory Search"
const STR_PAGE_TYPE = "admininvlist"

const STR_SEARCH_SCRIPT = "inv_search.asp?"
const STR_DETAIL_FOLDER_SCRIPT = "inv_fdetail.asp?"
const STR_DETAIL_ITEM_SCRIPT = "inv_idetail.asp?"

OpenConn

dim strMode, strAction

strMode = lcase(Request("mode"))
if strMode <> "a" and strMode <> "i" then
	strMode = "b"
end if
strAction = lcase(Request("action"))

strSearchSQLPrefix = "SELECT I.*"
strSearchSQLPrefix = strSearchSQLPrefix & " FROM " & STR_TABLE_INVENTORY & " AS I"
strSearchSQLPrefix = strSearchSQLPrefix & " WHERE "
strSearchSQLPrefix = strSearchSQLPrefix & " I.chrStatus <> 'D'"
strSearchSQLSuffix = " ORDER BY I.intID"

Search_AddLabel "Inventory Information"
Search_AddNumberRange "#I.intID", "Item ID"
Search_AddDateRange "%I.dtmCreated", "Date Created"
Search_AddDateRange "%I.dtmUpdated", "Date Updated"
if strMode = "a" then
	Search_AddText "I.vchCreatedByIP", 15, 15, "Created by IP"
	Search_AddText "I.vchUpdatedByIP", 15, 15, "Updated by IP"
	Search_AddText "I.vchCreatedByUser", 15, 32, "Created by User"
	Search_AddText "I.vchUpdatedByUser", 15, 32, "Updated by User"
end if
Search_AddCheckboxGroup "+I.chrType", 2, dctInvTypeValues, "Type"
Search_AddCheckboxGroup "+I.chrStatus", 4, dctInvStatusValues, "Status"
Search_AddText "I.vchItemName", 15, 255, "Item Name"
Search_AddText "I.vchPartNumber", 15, 32, "Part Number"
Search_AddText "I.txtDescription", 15, 255, "Description"
if strMode = "a" then
	Search_AddNumberRange "#I.intStock", "Qty in Stock"
	Search_AddNumberRange "#I.intLowStock", "Minimum Stock"
	Search_AddNumberRange "#I.intHighStock", "Maximum Stock"
	Search_AddNumberRange "#I.intHitCount", "Hit Count"
	Search_AddText "I.vchImageURL", 32, 255, "Image URL"
end if
Search_AddNumberRange "#I.mnyItemPrice", "Item Price"
Search_AddNumberRange "#I.mnyShipPrice", "Shipping Price"
if strMode = "a" then
	Search_AddNumberRange "#I.fltShipWeight", "Shipping Weight"
	Search_AddRadioGroup "+I.chrTaxFlag", 2, dctYesNoValues, "Taxable Flag"
	Search_AddRadioGroup "+I.chrICFlag", 2, dctYesNoValues, "Inventory Control Flag"
	Search_AddRadioGroup "+I.chrSoftFlag", 2, dctYesNoValues, "Software Item"
	Search_AddText "I.vchSoftURL", 32, 255, "Software URL"
end if
	
InitSearchEngine

dim strSQL, rsData
if strAction = "search" then
	if strMode = "b" then
		strSQL = BuildBasicSearchSQL()
	else
		strSQL = BuildSearchSQL()
	end if
	if strSQL <> "" then
		set rsData = gobjConn.execute(strSQL)
	else
		strAction = ""
	end if
end if

intCurrentGroup = intNumGroups + 1
if intCurrentGroup > INT_MAX_SEARCH_GROUPS then
	intCurrentGroup = 0
end if

DrawHeader strPageTitle, STR_PAGE_TYPE
if strAction = "search" then
	DrawSearchResults
else
	DrawSearchPage "Search Inventory"
end if


if IsObject(rsData) then
	rsData.close
	set rsData = nothing
end if

response.end

sub LoadPresetSearch(strPreset)
	strMode = "i"
	select case strPreset
		case "open":
			arySearchGroups(1,0).Add "+O.chrStatus", "n~S+A+Z+E"
		case "48hours":
			arySearchGroups(1,0).Add "O.dtmUpdated", "n~" & DateAdd("h", -48, Now()) & "~"
		case "thisweek":
			arySearchGroups(1,0).Add "O.dtmUpdated", "n~" & DateAdd("d", 1-Weekday(Date()), Date()) & "~"
		case "thismonth":
			arySearchGroups(1,0).Add "O.dtmUpdated", "n~" & Month(Date()) & "/1/" & Year(Date()) & "~"
		case "thisyear":
			arySearchGroups(1,0).Add "O.dtmUpdated", "n~1/1/" & Year(Date()) & "~"
		case "all":
			arySearchGroups(1,0).Add "O.chrStatus", "~D"
		case else:	' invalid preset
			response.redirect "default.asp"
	end select
end sub

sub DrawSearchResults
	strPageTitle = "Search Results - Inventory"
	DrawListTitle
	DrawListHeader
	if rsData.eof then
%>
<TR>
	<TD COLSPAN="5" ALIGN="center">No orders were found matching your search.</TD>
</TR>
<%
	else
		while not rsData.eof
			DrawListItem rsData("intID"), rsData("chrType"), rsData("vchItemName"), rsData("vchPartNumber"), rsData("mnyItemPrice"), rsData("chrStatus")
			rsData.MoveNext
			if not rsData.eof then
				DrawListItemSep
			end if
		wend
	end if
	DrawListFooter
end sub

sub DrawListTitle
%>
<FORM ACTION="<%= STR_SEARCH_SCRIPT %>" METHOD="POST" NAME="frm">
<INPUT TYPE="hidden" NAME="action" VALUE="modify">
<INPUT TYPE="hidden" NAME="n_groups" VALUE="<%= intNumGroups %>">
<INPUT TYPE="hidden" NAME="mode" VALUE="<%= strMode %>">
<INPUT TYPE="hidden" NAME="reset" VALUE="">
<INPUT TYPE="hidden" NAME="c_group" VALUE="0">
<%= font(5) %><%= strPageTitle %><BR>
<%= spacer(1,5) %><BR>
<%
	stlBeginInfoBar INT_PAGE_WIDTH
	if intNumGroups < INT_MAX_SEARCH_GROUPS and strMode <> "b" and intNumGroups > 0 then
		%>
		&nbsp;<B><A HREF="#" onClick="frm.reset.value='r'; frm.submit(); return false;">Restrict This Search Further</A></B>
		&nbsp;|&nbsp;
		<B><A HREF="#" onClick="frm.submit(); return false;">Add To This Search</A></B>
		<%
	end if
	SaveOldGroups
	%>
	</TD>
	<TD ALIGN=RIGHT><%= fontx(1,1,cWhite) & FormatDateTime(date,1) %>
<% stlEndInfoBar %>
<% if not blnPrintMode then %>
<TABLE BORDER=0 CELLPADDING=5 CELLSPACING=0 WIDTH="100%">
<TR>
	<TD><%= font(1) %>
	<%= iif(strMode="b", "<B>", "") %><A HREF="<%= STR_SEARCH_SCRIPT %>">Basic Search</A></B> |
	<%= iif(strMode="i", "<B>", "") %><A HREF="<%= STR_SEARCH_SCRIPT %>mode=i">Intermediate Search</A></B> |
	<%= iif(strMode="a", "<B>", "") %><A HREF="<%= STR_SEARCH_SCRIPT %>mode=a">Advanced Search</A></B>
	</TD>
</TR>
</TABLE>
<% end if %>
<%
end sub

sub DrawListHeader
%>
<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVLtGrey) %>">
<TR>
	<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cWhite %>">
	<TR>
		<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%">
		<TR BGCOLOR="<%= cVLtYellow %>">
			<TD><%= font(1) %>&nbsp;</TD>
			<TD NOWRAP><%= font(1) %><B>Part Number</B></TD>
			<TD NOWRAP><%= font(1) %><B>&nbsp;Name&nbsp;</B></TD>
			<TD NOWRAP ALIGN="right"><%= font(1) %><B>&nbsp;Amount&nbsp;</B></TD>
			<TD NOWRAP ALIGN="right"><%= font(1) %><B>Status</B></TD>
		</TR>
<%
end sub

sub DrawListItem(intID, strType, strName, strPartNumber, mnyPrice, strStatus)
%>
		<TR VALIGN=BOTTOM>
			<%
			select case strType
				case "A"
					%>
					<TD ALIGN=CENTER><A HREF="inv_fdetail.asp?id=<%= intID %>"><IMG SRC="<%= imagebase %>icon_folder.gif" WIDTH="16" HEIGHT="16" ALT="Edit This Folder" BORDER="0"></A></TD>
					<%
				case "I"
					%>
					<TD ALIGN=CENTER><A HREF="inv_idetail.asp?id=<%= intID %>"><IMG SRC="<%= imagebase %>icon_product.gif" WIDTH="16" HEIGHT="16" ALT="Edit This Item" BORDER="0"></A></TD>
					<%
				case else
					%>
					<TD ALIGN=CENTER><%= fontx(1,1,cRed) %>?</TD>
					<%
			end select
			%>

			<TD NOWRAP><%= iif(strType="I", strPartNumber, fontx(1,1,cGrey) & "n/a") %></TD>
			<TD><A HREF="<%= STR_DETAIL_ITEM_SCRIPT %>id=<%= intID %>"><%= strName %></A></TD>
			<TD NOWRAP ALIGN="right"><%= iif(strType="I", SafeFormatCurrency(fontx(1,1,cLight) & "n/a</FONT>", mnyPrice, 2), fontx(1,1,cGrey) & "n/a") %>
			<TD NOWRAP ALIGN="right"><%= GetArrayValue(strStatus, dctInvStatusValues) %></TD>
		</TR>
<%
end sub

sub DrawListItemSep
%>
		<TR>
			<TD COLSPAN=5><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVVLtGrey) %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
		</TR>
<%
end sub

sub DrawListFooter
%>
		</TABLE></TD>
	</TR>
	</TABLE></TD>
</TR>
</TABLE>
</FORM>

<%
end sub
%>