<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: order_itemmod.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Allow merchant to edit items in an order.
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

OpenConn

if not CheckUserAccess(ACC_ORDER_VIEW) then
	response.redirect "menu.asp"
end if

dim blnCanEdit, blnCanEditItem, blnCanAddNote, blnCanReturnItem
blnCanEdit = CheckUserAccess(ACC_ORDER_EDIT)
blnCanEditItem = CheckUserAccess(ACC_ORDER_ITEMS_EDIT)
blnCanAddNote = CheckUserAccess(ACC_ORDER_NOTES_ADD)
blnCanReturnItem = CheckUserAccess(ACC_ORDER_RETURNS_ADD)

const STR_LIST_SCRIPT = "order_list.asp?"
const STR_PAGE_TYPE = "adminordermod"

const STR_PAGE_TITLE = "Modify Order"

dim STR_EDIT_SCRIPT, STR_DETAIL_SCRIPT

dim intOrderID
intOrderID = Request("order")
if IsNumeric(intOrderID) then
	intOrderID = CLng(intOrderID)
else
	intOrderID = 0
end if

STR_DETAIL_SCRIPT = "order_detail.asp?id=" & intOrderID & "&"
STR_EDIT_SCRIPT = "order_itemmod.asp?order=" & intOrderID & "&"

if intOrderID = 0 then
	response.redirect STR_LIST_SCRIPT
end if
gintOrderID = intOrderID

dim intUserID, intParentID, intViewItemID, intItemID
intUserID = Merchant_GetOrderShopper(intOrderID)

intParentID = Request("parentid")
if IsNumeric(intParentID) then
	intParentID = CLng(intParentID)
else
	intParentID = 0
end if

intItemID = Request("parentid")
if IsNumeric(intItemID) then
	intItemID = CLng(intItemID)
else
	intItemID = 0
end if

intViewItemID = Request("viewid")
if IsNumeric(intViewItemID) then
	intViewItemID = CLng(intViewItemID)
else
	intViewItemID = 0
end if

dim strParentName, intParentUpID
if intParentID = 0 then
	strParentName = ""
	intParentUpID = 0
else
	dim strSQL, rsTemp
	strSQL = "SELECT intParentID, vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intParentID & " AND chrType='A' AND chrStatus <> 'D'"
	set rsTemp = gobjConn.execute(strSQL)
	if not rsTemp.eof then
		intParentUpID = rsTemp("intParentID")
		strParentName = rsTemp("vchItemName")
	else
		intParentID = 0
		strParentName = ""
		intParentUpID = 0
	end if
	rsTemp.close
	set rsTemp = nothing
end if

dim strStatus
strStatus = GetOrderStatus_Other(intOrderID)
if not CheckUserAccessStatus(strStatus) then
	response.redirect "menu.asp"
end if
if strStatus = "9" then
	blnCanEditItem = blnCanEdit
	blnCanReturnItem = false
	blnCanAddNote = blnCanEdit
else
	blnCanEditItem = blnCanEditItem and (InStr("SVAZ", strStatus) > 0)
	blnCanReturnItem = blnCanReturnItem and (InStr("XH", strStatus) > 0)
	blnCanAddNote = blnCanAddNote and (InStr("SVAZXH", strStatus) > 0)
end if

dim strAction
strAction = lcase(Request("action"))

dim strItemType
select case strAction
	case "editsubmit":
		AutoCheck Request, ""
		if FormErrors.count > 0 then
			strAction = "edititem"
			
			DrawPage_List
			
			response.end
		else
			if Request("chrType") = "I" and (blnCanEditItem OR blnCanReturnItem) then
				if Request("id") = "0" then
					if (not CanAddItemSpecial_Other(intOrderID, Request("intForceShipMethod"), Request("chrForceSoloItem"))) and (Request("special") <> "y") then
						strAction = "edititem"
						FormErrors.Add "special", " "
						
						DrawPage_List
						
						response.end
					end if
					PrepareSpecialAdd_Other intOrderID, Request("intForceShipMethod"), (Request("chrForceSoloItem") = "Y")
				end if
				EditOrderItem_Other intOrderID, Request("id"), Request("intInvID"), Request("vchPartNumber"), Request("vchItemName"), Request("mnyUnitPrice"), Request("mnyShipPrice"), Request("chrTaxFlag") = "Y", Request("intQuantity"), Request("intForceShipMethod"), Request("chrForceSoloItem")
			elseif Request("chrType") = "R" and (blnCanEditItem or (Request("id") = "0" and blnCanReturnItem)) then
				EditOrderReturn_Other intOrderID, Request("id"), Request("intInvID"), Request("vchPartNumber"), Request("vchItemName"), Request("mnyUnitPrice"), Request("mnyShipPrice"), Request("chrTaxFlag") = "Y", Request("intQuantity")
			elseif blnCanEditItem or (Request("id") = "" and blnCanAddNote) then
				EditOrderNote_Other intOrderID, Request("id"), Request("vchItemName")
			end if
			response.redirect STR_EDIT_SCRIPT & "parentid=" & intParentID
		end if
	case "remove":
		intViewItemID = Request("id")
		if IsNumeric(intViewItemID) and blnCanEditItem then
			RemoveLineFromOrder_Other intOrderID, CLng(intViewItemID)
		end if
		response.redirect STR_EDIT_SCRIPT & "parentid=" & intParentID
end select


DrawPage_List

response.end

sub DrawPage_List
	dim rsTemp, strSQL
	dim rsOrder, rsLineItem
	set rsOrder = GetOrderRecord_Other(intOrderID)
	
	DrawFormHeader "100%", STR_PAGE_TITLE, ""
	
	stlBeginStdTable "100%"
%>
<TR>
	<TD>
<%
	dim strAuxMsgItem, strAuxMsgNote, strAuxMsgReturn
	set rsLineItem = GetOrderLineItems_Other(intOrderID)
	if blnCanEditItem then
		strAuxMsgItem = "<A HREF=""" & STR_EDIT_SCRIPT & "action=edititem&parentid=" & intParentID & "&id=$ID$"">Details</A>&nbsp;<A HREF=""" & STR_EDIT_SCRIPT & "action=remove&parentid=" & intParentID & "&id=$ID$"" onClick=""return confirm('Are you sure you want to delete this item?');"">Remove</A>"
		strAuxMsgNote = "<A HREF=""" & STR_EDIT_SCRIPT & "action=edititem&parentid=" & intParentID & "&id=$ID$"">Details</A>&nbsp;<A HREF=""" & STR_EDIT_SCRIPT & "action=remove&parentid=" & intParentID & "&id=$ID$"" onClick=""return confirm('Are you sure you want to delete this item?');"">Remove</A>"
		strAuxMsgReturn = "<A HREF=""" & STR_EDIT_SCRIPT & "action=edititem&parentid=" & intParentID & "&id=$ID$"">Details</A>&nbsp;<A HREF=""" & STR_EDIT_SCRIPT & "action=remove&parentid=" & intParentID & "&id=$ID$"" onClick=""return confirm('Are you sure you want to delete this item?');"">Remove</A>"
	elseif blnCanReturnItem then
		strAuxMsgItem = "<A HREF=""" & STR_EDIT_SCRIPT & "action=returnitem&parentid=" & intParentID & "&viewid=$ID$"">Return Item</A>"
		strAuxMsgNote = ""
		strAuxMsgReturn = ""
	else
		strAuxMsgItem = ""
		strAuxMsgNote = ""
		strAuxMsgReturn = ""
	end if
	Merchant_DrawOrderCart rsOrder, rsLineItem, true, strAuxMsgItem, strAuxMsgNote, strAuxMsgReturn, "", ""
	rsLineItem.close
	set rsLineItem = nothing
	rsOrder.close
	set rsOrder = nothing
%>
	</TD>
</TR>
<%
	if strAction = "edititem" or strAction = "returnitem" then
		DrawEditForm
	else
%>
<TR>
	<TD>
<%
	if blnCanEditItem or blnCanReturnItem then
		response.write "<A HREF=""" & STR_EDIT_SCRIPT & "parentid=" & intParentID & "&action=edititem&chrtype=I"">Add Custom Item...</A><BR>"
	end if
	if blnCanAddNote then
		response.write "<A HREF=""" & STR_EDIT_SCRIPT & "parentid=" & intParentID & "&action=edititem&chrtype=N"">Add Note...</A><BR>"
	end if
	if blnCanEditItem then
		DrawInventory
	end if
%>
	</TD>
</TR>
<FORM ACTION="<%= STR_DETAIL_SCRIPT %>" METHOD="POST">
<%
		stlSubmit "Return to Review"
	end if
	stlEndStdTable
	response.write "</FORM>"
end sub

sub DrawInventory
	dim rsLineItem, strSQL
	if intParentID > 0 then
%>
		Folder: <%= strParentName %> <A HREF="<%= STR_EDIT_SCRIPT %>parentid=<%= intParentUpID %>">(Close)</A><BR>
		<% end if %>
		<TABLE BORDER=1 CELLPADDING=1 CELLSPACING=0>
		<TR>
			<TD><B>Part #</B></TD>
			<TD><B>Name</B></TD>
            <TD><B>Size</B></TD>
            <TD><B>Color</B></TD>
			<TD><B>Substrate</B></TD>
            <TD><B>Numnber of Sides</B></TD>
			<TD><B>Special Finishing</B></TD>
			<TD ALIGN="right"><B>Unit $</B></TD>
			<TD ALIGN="right"><B>Ship $</B></TD>
			<TD ALIGN="center"><B><A TITLE="Item requires a specific shipping method">Special Ship</A></B></TD>
			<TD ALIGN="center"><B><A TITLE="Indicates if an item must be ordered by itself">Sep</A></B></TD>
			<TD><B>&nbsp;</B></TD>
		</TR>
<%
	strSQL = "SELECT * FROM " & STR_TABLE_INVENTORY & " WHERE chrStatus<>'D' AND intParentID=" & intParentID & " ORDER BY chrType, intSortOrder"
	set rsLineItem = gobjConn.execute(strSQL)
	if not rsLineItem.eof then
		while not rsLineItem.eof
			if rsLineItem("chrType") = "A" then
%>
		<TR>
			<TD>&nbsp;</TD>
			<TD>&nbsp;<%= rsLineItem("vchItemName") %>&nbsp;</TD>
			<TD COLSPAN=4>&nbsp;</TD>
			<TD>&nbsp;<A HREF="<%= STR_EDIT_SCRIPT %>parentid=<%= rsLineItem("intID") %>">Open...</A>&nbsp;</TD>
		</TR>
<%
			else
				if rsLineItem("intID") = intViewItemID then
%>
		<TR>
			<TD>&nbsp;<%= rsLineItem("vchPartNumber") %>&nbsp;</TD>
			<TD>&nbsp;<%= rsLineItem("vchItemName") %>&nbsp;</TD>
            <TD>&nbsp;<%= rsLineItem("vchSize") %>&nbsp;</TD>
			<TD>&nbsp;<%= rsLineItem("vchColor") %>&nbsp;</TD>
            <TD>&nbsp;<%= rsLineItem("vchSubstrate") %>&nbsp;</TD>
			<TD>&nbsp;<%= rsLineItem("vchNumSides") %>&nbsp;</TD>
            <TD>&nbsp;<%= rsLineItem("vchSpecialFinishing") %>&nbsp;</TD>
			<TD ALIGN="right">&nbsp;<%= SafeFormatCurrency("$0.00", rsLineItem("mnyItemPrice"), 2) %>&nbsp;</TD>
			<TD ALIGN="right">&nbsp;<%= SafeFormatCurrency("n/a", rsLineItem("mnyShipPrice"), 2) %>&nbsp;</TD>
			<TD ALIGN="center">&nbsp;<%= iif(IsNull(rsLineItem("intForceShipMethod")), "", GetArrayValue(rsLineItem("intForceShipMethod"), dctShipOption)) %>&nbsp;</TD>
			<TD ALIGN="center">&nbsp;<%= iif(rsLineItem("chrForceSoloItem") & "" = "Y", "Y", "") %>&nbsp;</TD>
			<TD>&nbsp;<A HREF="<%= STR_EDIT_SCRIPT %>parentid=<%= intParentID %>">Hide</A>&nbsp;<A HREF="<%= STR_EDIT_SCRIPT %>parentid=<%= intParentID %>&action=edititem&viewid=<%= rsLineItem("intID") %>">Add...</A>&nbsp;</TD>
		</TR>
		<TR>
			<TD COLSPAN=12>
				<TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0>
				<TR>
					<% if not IsNull(rsLineItem("vchImageURL")) then %>
					<TD VALIGN="top"><IMG SRC="<%= imagebase & "products/small/" & rsLineItem("vchImageURL") %>"></TD>
					<% end if %>
					<TD VALIGN="top">
						<B><%= rsLineItem("vchPartNumber") %>&nbsp;&nbsp;<%= rsLineItem("vchItemName") %></B><BR>
						<%= rsLineItem("txtDescription") %>
					</TD>
					<TD VALIGN="top" NOWRAP>
						Stock: <%= rsLineItem("intStock") %><BR>
						Item Price: <%= SafeFormatCurrency("$0.00", rsLineItem("mnyItemPrice"), 2) %><BR>
						Shipping Charge: <%= SafeFormatCurrency("n/a", rsLineItem("mnyShipPrice"), 2) %><BR>
						Shipping Weight: <%= rsLineItem("fltShipWeight") %><BR>
						Taxable: <%= rsLineItem("chrTaxFlag") %><BR>
						<% if not IsNull(rsLineItem("intForceShipMethod")) then %>
						Must ship by: <B><%= GetArrayValue(rsLineItem("intForceShipMethod"), dctShipOption) %></B><BR>
						<% end if %>
						<% if rsLineItem("chrForceSoloItem") & "" = "Y" then %>
						Must be ordered <B>separately</B>.
						<% end if %>
						<% if rsLineItem("chrSoftFlag") = "Y" then %><B>*Software Item*</B><% end if %>
					</TD>
				</TR>
				</TABLE>
			</TD>
		</TR>
<%
				else
%>
		<TR>
			<TD>&nbsp;<%= rsLineItem("vchPartNumber") %>&nbsp;</TD>
			<TD>&nbsp;<%= rsLineItem("vchItemName") %>&nbsp;</TD>
            <TD>&nbsp;<%= rsLineItem("vchSize") %>&nbsp;</TD>
			<TD>&nbsp;<%= rsLineItem("vchColor") %>&nbsp;</TD>
            <TD>&nbsp;<%= rsLineItem("vchSubstrate") %>&nbsp;</TD>
			<TD>&nbsp;<%= rsLineItem("vchNumSides") %>&nbsp;</TD>
            <TD>&nbsp;<%= rsLineItem("vchSpecialFinishing") %>&nbsp;</TD>
			<TD ALIGN="right">&nbsp;<%= SafeFormatCurrency("$0.00", rsLineItem("mnyItemPrice"), 2) %>&nbsp;</TD>
			<TD ALIGN="right">&nbsp;<%= SafeFormatCurrency("n/a", rsLineItem("mnyShipPrice"), 2) %>&nbsp;</TD>
			<TD ALIGN="center">&nbsp;<%= iif(IsNull(rsLineItem("intForceShipMethod")), "", GetArrayValue(rsLineItem("intForceShipMethod"), dctShipOption)) %>&nbsp;</TD>
			<TD ALIGN="center">&nbsp;<%= iif(rsLineItem("chrForceSoloItem") & "" = "Y", "Y", "") %>&nbsp;</TD>
			<TD>&nbsp;<A HREF="<%= STR_EDIT_SCRIPT %>parentid=<%= intParentID %>&viewid=<%= rsLineItem("intID") %>">View...</A>&nbsp;<A HREF="<%= STR_EDIT_SCRIPT %>parentid=<%= intParentID %>&action=edititem&viewid=<%= rsLineItem("intID") %>">Add...</A>&nbsp;</TD>
		</TR>
<%
				end if
			end if
			rsLineItem.MoveNext
		wend
	else
%>
		<TR>
			<TD COLSPAN=4>This folder is empty.</TD>
		</TR>
<%
	end if
	rsLineItem.close
	set rsLineItem = nothing
%>
		</TABLE>
<%
end sub

sub DrawEditForm
	dim strPartNumber, strItemName, mnyUnitPrice, mnyShipPrice, intQuantity, intInvID, strType, intID, intForceShipMethod, chrForceSoloItem, chrTaxFlag
	intID = Request("id")
	if IsNumeric(intID) then
		intID = CLng(intID)
	else
		intID = 0
	end if
	if strAction = "returnitem" then
		' RETURN ITEM
		if FormErrors.count > 0 then
			' form error
			strPartNumber = Request("vchPartNumber")
			strItemName = Request("vchItemName")
			mnyUnitPrice = Request("mnyUnitPrice")
			mnyShipPrice = Request("mnyShipPrice")
			intQuantity = Request("intQuantity")
			intInvID = Request("intInvID")
			intForceShipMethod = Request("intForceShipMethod")
			chrForceSoloItem = Request("chrForceSoloItem")
			chrTaxFlag = Request("chrTaxFlag")
			strType = "R"
		elseif intID > 0 then
			' edit an existing return
			strSQL = "SELECT chrType, intInvID, vchPartNumber, vchItemName, chrTaxFlag, mnyUnitPrice, mnyShipPrice, intQuantity FROM " & STR_TABLE_LINEITEM & " WHERE intID=" & intID & " AND chrStatus<>'D'"
			set rsTemp = gobjConn.execute(strSQL)
			strPartNumber = rsTemp("vchPartNumber")
			strItemName = rsTemp("vchItemName")
			mnyUnitPrice = rsTemp("mnyUnitPrice")
			mnyShipPrice = rsTemp("mnyShipPrice")
			intQuantity = rsTemp("intQuantity")
			intInvID = rsTemp("intInvID")
			intForceShipMethod = rsTemp("intForceShipMethod")
			chrForceSoloItem = rsTemp("chrForceSoloItem")
			chrTaxFlag = rsTemp("chrTaxFlag")
			strType = "R"
			rsTemp.close
			set rsTemp = nothing
		else
			' add a new return
			strSQL = "SELECT chrType, intInvID, vchPartNumber, vchItemName, chrTaxFlag, mnyUnitPrice, mnyShipPrice, intQuantity FROM " & STR_TABLE_LINEITEM & " WHERE intID=" & intViewItemID & " AND chrStatus<>'D'"
			set rsTemp = gobjConn.execute(strSQL)
			if not rsTemp.eof then
				strPartNumber = rsTemp("vchPartNumber")
				strItemName = rsTemp("vchItemName")
				mnyUnitPrice = rsTemp("mnyUnitPrice")
				if not IsNull(mnyUnitPrice) then
					mnyUnitPrice = -mnyUnitPrice
				end if
				mnyShipPrice = rsTemp("mnyShipPrice")
				if not IsNull(mnyShipPrice) then
					mnyShipPrice = -mnyShipPrice
				end if
				intInvID = rsTemp("intInvID")
				chrTaxFlag = rsTemp("chrTaxFlag")
				chrForceSoloItem = ""
				intForceShipMethod = ""
			else
				strPartNumber = ""
				strItemName = ""
				mnyUnitPrice = 0
				mnyShipPrice = 0
				intInvID = 0
				intForceShipMethod = ""
				chrForceSoloItem = ""
				chrTaxFlag = ""
			end if
			strType = "R"
			rsTemp.close
			set rsTemp = nothing
			intQuantity = 1
		end if
	else
		' ITEM OR NOTE
		if FormErrors.count > 0 or (intID = 0 and intViewItemID = 0) then
			' either a form error or creating a new item
			strPartNumber = Request("vchPartNumber")
			strItemName = Request("vchItemName")
			mnyUnitPrice = Request("mnyUnitPrice")
			mnyShipPrice = Request("mnyShipPrice")
			intQuantity = Request("intQuantity")
			intInvID = Request("intInvID")
			strType = Request("chrType")
			intForceShipMethod = Request("intForceShipMethod")
			chrForceSoloItem = Request("chrForceSoloItem")
			chrTaxFlag = Request("chrTaxFlag")
		elseif intID > 0 then
			' edit an existing line item
			strSQL = "SELECT chrType, intInvID, vchPartNumber, vchItemName, chrTaxFlag, mnyUnitPrice, mnyShipPrice, intQuantity, intForceShipMethod, chrForceSoloItem FROM " & STR_TABLE_LINEITEM & " WHERE intID=" & intID & " AND chrStatus<>'D'"
			set rsTemp = gobjConn.execute(strSQL)
			strPartNumber = rsTemp("vchPartNumber")
			strItemName = rsTemp("vchItemName")
			mnyUnitPrice = rsTemp("mnyUnitPrice")
			mnyShipPrice = rsTemp("mnyShipPrice")
			intQuantity = rsTemp("intQuantity")
			intInvID = rsTemp("intInvID")
			strType = rsTemp("chrType")
			chrTaxFlag = rsTemp("chrTaxFlag")
			intForceShipMethod = rsTemp("intForceShipMethod")
			chrForceSoloItem = rsTemp("chrForceSoloItem")
			rsTemp.close
			set rsTemp = nothing
		else
			' add a new item from the inventory
			strSQL = "SELECT chrType, vchPartNumber, vchItemName, chrTaxFlag, mnyItemPrice, mnyShipPrice, intForceShipMethod, chrForceSoloItem FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intViewItemID & " AND chrStatus<>'D'"
			set rsTemp = gobjConn.execute(strSQL)
			strPartNumber = rsTemp("vchPartNumber")
			strItemName = rsTemp("vchItemName")
			mnyUnitPrice = rsTemp("mnyItemPrice")
			mnyShipPrice = rsTemp("mnyShipPrice")
			intInvID = intViewItemID
			strType = rsTemp("chrType")
			chrTaxFlag = rsTemp("chrTaxFlag")
			intForceShipMethod = rsTemp("intForceShipMethod")
			chrForceSoloItem = rsTemp("chrForceSoloItem")
			rsTemp.close
			set rsTemp = nothing
			intQuantity = 1
		end if
	end if
	if strType <> "N" and strType <> "R" then
		strType = "I"
	end if
%>
<FORM ACTION="<%= STR_EDIT_SCRIPT %>action=editsubmit&parentid=<%= intParentID %>" METHOD="POST">
<TR>
	<TD><A HREF="<%= STR_EDIT_SCRIPT %>parentid=<%= intParentID %>">Cancel</A><BR>
<%
	stlBeginFormSection "100%", 2
	HiddenInput "intInvID", intInvID
	HiddenInput "id", intID
	HiddenInput "chrtype", strType
	if FormErrors.Exists("special") then
		stlCaption "Adding this item will cause certain other items to be removed due to shipping conflicts.", 2
		stlCheckbox "special", "y", Request, "Add this item, remove others", "Yes, remove conflicting items"
	end if
	if strType = "I" then
		stlTextInput "vchPartNumber", 32, 32, strPartNumber, "Part Number", "", ""
		stlTextInput "vchItemName", 42, 255, strItemName, "Item Name", "IsEmpty", "Missing Item Name"
		stlTextInput "mnyUnitPrice", 10, 10, mnyUnitPrice, "Unit Price", "IsNumeric", "Missing Unit Price"
		stlTextInput "mnyShipPrice", 10, 10, mnyShipPrice, "Shipping Price", "OptIsNumeric", "Invalid Shipping Price"
		stlTextInput "intQuantity", 10, 10, intQuantity, "Quantity", "IsNumeric", "Invalid Quantity"
		stlCheckbox "chrTaxFlag", "Y", chrTaxFlag, "Taxable", "Yes"
		stlArrayPulldown "intForceShipMethod", "None", intForceShipMethod, dctShipOption, "Require Shipping", "", ""
		stlCheckbox "chrForceSoloItem", "Y", chrForceSoloItem, "Must Order Seperately", "Yes"
	elseif strType = "R" then
		HiddenInput "vchPartNumber", strPartNumber
		stlStaticText "Part Number:", strPartNumber & " (RETURNED)"
		stlTextInput "vchItemName", 42, 255, strItemName, "Item Name", "IsEmpty", "Missing Item Name"
		stlTextInput "mnyUnitPrice", 10, 10, mnyUnitPrice, "Unit Price", "IsNumeric", "Missing Unit Price"
		stlTextInput "mnyShipPrice", 10, 10, mnyShipPrice, "Shipping Price", "OptIsNumeric", "Invalid Shipping Price"
		stlTextInput "intQuantity", 10, 10, intQuantity, "Quantity", "IsNumeric", "Invalid Quantity"
		stlCheckbox "chrTaxFlag", "Y", chrTaxFlag, "Taxable", "Yes"
	else
		stlTextInput "vchItemName", 42, 255, strItemName, "Note", "IsEmpty", "Missing Note"
	end if
	stlEndFormSection
	if intID > 0 then
		stlSubmit "Save Changes"
	else
		stlSubmit "Add to Cart"
	end if
%>
	</TD>
</TR>
</FORM>
<%
end sub
%>