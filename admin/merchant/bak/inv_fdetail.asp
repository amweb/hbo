<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: inv_fdetail.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Inventory admin - details on folders
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_INVENTORY_VIEW) then
	response.redirect "main.asp"
end if

dim blnCanEdit
blnCanEdit = CheckUserAccess(ACC_INVENTORY_EDIT)

const STR_PAGE_TITLE = "Folder Details"
const STR_PAGE_TYPE = "admininvlist"

OpenConn

dim dctBrands
dim strSQL

dim intID, intParentID, strAction
strAction = Request("action")
intID = Request("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if

intParentID = Request("parentid")
if IsNumeric(intParentID) then
	intParentID = CLng(intParentID)
else
	intParentID = 0
end if

if strAction = "delete" then
	if blnCanEdit then
		gobjConn.execute("UPDATE " & STR_TABLE_INVENTORY & " SET chrStatus='D' WHERE intID=" & intID)
	end if
	response.redirect "inv_list.asp?parentid=" & intParentID
elseif strAction = "setstatus" then
	if blnCanEdit then
		gobjConn.execute("UPDATE " & STR_TABLE_INVENTORY & " SET chrStatus='" & Request("status") & "' WHERE intID=" & intID)
	end if
	response.redirect "inv_list.asp?parentid=" & intParentID
elseif strAction = "submit" then
	if blnCanEdit then
		AutoCheck Request, ""
		if Request("chrSoftFlag") = "Y" then
			CheckField "vchSoftURL", "IsEmpty", Request, "Missing Download URL"
		end if
		if FormErrors.count = 0 then
			dim rsTemp, intNewOrder
			strSQL = "SELECT ISNULL(MAX(intSortOrder),0) AS intMaxSortOrder FROM " & STR_TABLE_INVENTORY
			set rsTemp = gobjConn.execute(strSQL)
			intNewOrder = rsTemp("intMaxSortOrder") + 1
			rsTemp.close
			set rsTemp = nothing
			dim dctSaveList
			set dctSaveList = Server.CreateObject("Scripting.Dictionary")
				dctSaveList.Add "*@dtmCreated", "GETDATE()"
				dctSaveList.Add "@dtmUpdated", "GETDATE()"
				dctSaveList.Add "*vchCreatedByUser", Session(SESSION_MERCHANT_ULOGIN)
				dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
				dctSaveList.Add "*vchCreatedByIP", gstrUserIP
				dctSaveList.Add "vchUpdatedByIP", gstrUserIP
				dctSaveList.Add "chrType", "A"
				dctSaveList.Add "*chrStatus", "A"
				dctSaveList.Add "*#intParentID", intParentID
				dctSaveList.Add "*#intSortOrder", intNewOrder
				dctSaveList.Add "*#intHitCount", 0
				dctSaveList.Add "!vchItemName", ""
				dctSaveList.Add "!vchImageURL", ""
				dctSaveList.Add "#mnyItemPrice", "0"
				dctSaveList.Add "chrICFlag", "N"
				dctSaveList.Add "chrTaxFlag", "N"
				dctSaveList.Add "chrSoftFlag", "N"
				dctSaveList.Add "!#intBrand", ""
				dctSaveList.Add "!txtDescription", ""
			SaveDataRecord STR_TABLE_INVENTORY, Request, intID, dctSaveList
			response.redirect "inv_list.asp?parentid=" & intParentID
		else
			
			DrawPage Request
			
		end if
	else
		response.redirect "inv_list.asp?parentid=" & intParentID
	end if
else
	dim rsData, blnDataIsRS
	if intID > 0 then
		strSQL = "SELECT * FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intID & " AND chrStatus<>'D'"
		set rsData = gobjConn.execute(strSQL)
		blnDataIsRS = true
		if rsData.eof then
			response.redirect "inv_list.asp"
		end if
		intParentID = rsData("intParentID")
	else
		set rsData = Request
		blnDataIsRS = false
	end if
	
	
	DrawPage rsData
	
	
	if blnDataIsRS then
		rsData.close
		set rsData = nothing
	end if
end if
response.end

sub DrawPage(rsInput)
	dim strTitle, strInfoText
	if intID > 0 then
		strTitle = "Edit Folder - " & rsInput("vchItemName")
	else
		strTitle = "Add New Folder"
	end if
%>
<FORM ACTION="inv_fdetail.asp" METHOD="POST" NAME="frm">
<INPUT TYPE="hidden" NAME="action" VALUE="submit">
<INPUT TYPE="hidden" NAME="id" VALUE="<%= intID %>">
<INPUT TYPE="hidden" NAME="parentid" VALUE="<%= intParentID %>">
<%
	if blnCanEdit then
		AddPageAction "Cancel", "inv_list.asp?parentid=" & intParentID, ""
		if intID > 0 then
			if rsInput("chrStatus") <> "A" then
				AddPageAction "Activate", "inv_fdetail.asp?action=setstatus&status=A&id=" & intID & "&parentid=" & intParentID, "Are you sure you want to activate this item?"
			end if
			if rsInput("chrStatus") <> "I" then
				AddPageAction "Deactivate", "inv_fdetail.asp?action=setstatus&status=I&id=" & intID & "&parentid=" & intParentID, "Are you sure you want to deactivate this item?"
			end if
			AddPageAction "Delete", "inv_fdetail.asp?action=delete&id=" & intID & "&intParentID=" & intParentID, "Are you sure you want to delete this item?"
		end if
	else
		AddPageAction "Return", "inv_list.asp?parentid=" & intParentID, ""
	end if
	
	if intID > 0 then
		strInfoText = "<B>Record ID:</B> " & rsInput("intParentID") & "-" & rsInput("intID")
		strInfoText = strInfoText & "&nbsp; <B>Created:</B> " & rsInput("dtmCreated")
		strInfoText = strInfoText & "&nbsp; <B>Updated:</B> " & rsInput("dtmUpdated")
	else
		strInfoText = ""
	end if
	
	DrawFormHeader "100%", strTitle, strInfoText
	
	stlBeginStdTable "100%"
	response.write "<TR><TD>"
	
	if blnCanEdit then
		stlBeginFormSection "100%", 4
		stlTextInput "vchItemName", 20, 32, rsInput, "Folder \Name", "IsEmpty", "Missing Folder Name"

		MakeDctBrands
		stlArrayPulldown "intBrand", "None", rsInput, dctBrands, "\Brand", "", ""

		stlEndFormSection
		stlRule

		stlBeginFormSection "100%", 2
		stlTextInput "vchImageURL", 20, 32, rsInput, "\Image Name", "", ""
		stlEndFormSection
		
		stlRule
		
		response.write "<U>D</U>escription:<BR><BR>"
		response.write "<TEXTAREA NAME=""txtDescription"" ROWS=6 COLS=50 ACCESSKEY=""D"" CLASS=""textarea"">" & Server.HTMLEncode(rsInput("txtDescription") & "") & "</TEXTAREA>"
	else
		stlBeginFormSection "100%", 2
		stlStaticText "Folder Name", rsInput("vchItemName")
		stlEndFormSection
		
		stlRule
		
		stlStaticText "Description", rsInput("txtDescription")
	end if
	response.write "</TD></TR>"
	stlSubmit iif(blnCanEdit, "Submit", "Return")
	
	stlEndStdTable
	response.write "</FORM>"
	
	if blnCanEdit then
		SetFieldFocus "frm", "vchItemName"
	end if
end sub

sub MakeDctBrands
	dim SQL, rsTemp
	
	SQL = "SELECT intID, vchItemName FROM " & STR_TABLE_INVENTORY
	SQL = SQL & " WHERE chrStatus='A' AND chrType='B'"
	SQL = SQL & " ORDER BY vchItemName"
	
	set rsTemp = ConnOpenRS(SQL)

	if IsObject(dctBrands) then
		dctBrands.RemoveAll
	else
		set dctBrands = Server.CreateObject("Scripting.Dictionary")
	end if
	
	while not rsTemp.EOF
		dctBrands.Add cint(rsTemp("intID")), rsTemp("vchItemName")&""
		rsTemp.MoveNext
	wend
	
	rsTemp.close
	set rsTemp = nothing
	
end sub

%>