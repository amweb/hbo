<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: brand_order_detail.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Display order details.
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_ORDER_VIEW) then
	response.redirect "menu.asp"
end if

const STR_PAGE_TITLE = "Order Details"
const STR_PAGE_TYPE = "adminorderlist"

OpenConn

dim blnCanRaw, blnCanEdit, blnCanApprove, blnCanDecline, blnCanShip, blnCanTrans, blnCanSendReceipt, blnCanShipOption
blnCanRaw = CheckUserAccess(ACC_RAW)
blnCanEdit = CheckUserAccess(ACC_ORDER_EDIT)
blnCanApprove = CheckUserAccess(ACC_ORDER_APPROVE)
blnCanDecline = CheckUserAccess(ACC_ORDER_DECLINE)
blnCanShip = CheckUserAccess(ACC_ORDER_SHIP)
blnCanShipOption = blnCanShip
blnCanSendReceipt = CheckUserAccess(ACC_ORDER_EMAILRECEIPT)
blnCanTrans = CheckUserAccess(ACC_ORDER_TRANS_AUTH) OR CheckUserAccess(ACC_ORDER_TRANS_DEPOSIT) OR CheckUserAccess(ACC_ORDER_TRANS_CREDIT) OR CheckUserAccess(ACC_ORDER_TRANS_MANUAL) OR CheckUserAccess(ACC_ORDER_TRANS_AUTHDEP)

dim intID, strAction
strAction = Request("action")
intID = Request("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if

dim STR_SEARCH_SCRIPT, STR_DETAIL_SCRIPT, STR_EDIT_SCRIPT, STR_EDIT_LINES_SCRIPT
STR_SEARCH_SCRIPT = "brand_order_list.asp?"
STR_DETAIL_SCRIPT = "brand_order_detail.asp?id=" & intID
STR_EDIT_SCRIPT = "brand_order_ordermod.asp?order=" & intID
STR_EDIT_LINES_SCRIPT = "brand_order_itemmod.asp?order=" & intID

select case strAction
	case "btnShip":
		' redirect to shipping form
		if blnCanShip then
			response.redirect "brand_order_ship.asp?id=" & intID
		else
			response.redirect "menu.asp"
		end if
end select

dim strSQL, rsData
if intID <= 0 then
	response.redirect STR_SEARCH_SCRIPT
end if

dim strStatusClause
strStatusClause = UserAccessStatus_BuildSQLWhere()
if strStatusClause = "" then
	response.redirect STR_SEARCH_SCRIPT
end if

dim strBrandClause, strBrandFields, intBrand
if Session(SESSION_MERCHANT_BID) <> "" then
	intBrand = Session(SESSION_MERCHANT_BID)
	strBrandFields = ", (I.mnyUnitPrice * I.intQuantity) AS mnyPrice"
	strBrandClause = strBrandClause & " LEFT JOIN " & STR_TABLE_LINEITEM & " AS I ON (I.intOrderID = O.intID) AND (I.chrStatus<>'D')"
	' AND IsNull(I.vchShippingNumber,'')='' ' bkonvalin 1/13/04 - deactivated per email James Dutra 1/12/04 RE: Gagan# 8751
	strBrandClause = strBrandClause & " RIGHT JOIN " & STR_TABLE_INVENTORY & " AS B ON (B.intID = I.intInvID) AND (B.intBrand=" & intBrand & ") AND (B.chrType='I' OR B.chrType IS NULL)"
else
	strBrandClause = ""
	strBrandFields = ""
end if

strSQL = "SELECT O.*, S.vchFirstName + ' ' + S.vchLastName AS S_vchShopperAccount" & strBrandFields 	', O.txtGiftMessage
strSQL = strSQL & " FROM " & STR_TABLE_ORDER & " AS O"
strSQL = strSQL & " LEFT JOIN " & STR_TABLE_SHOPPER & " AS S ON O.intShopperID = S.intID" & strBrandClause
strSQL = strSQL & " WHERE O.intID=" & intID & " AND O.chrStatus<>'D' AND (S.chrType='A' OR S.chrType IS NULL)"
strSQL = strSQL & " AND O.chrStatus IN (" & strStatusClause & ")"
'response.write strSQL & "<br />"
set rsData = gobjConn.execute(strSQL)
if rsData.eof then
	rsData.close
	set rsData = nothing
	response.redirect STR_SEARCH_SCRIPT
end if


DrawPage rsData


rsData.close
set rsData = nothing

response.end

sub DrawPage(rsInput)
	dim rsLineItem, rsShopper, strColor
	dim blnErrors, dctErrorList
	set dctErrorList = Server.CreateObject("Scripting.Dictionary")
	blnErrors = ValidateOrder_Other(intID, dctErrorList)
	' menu options based on current order status
	dim dctMenuList
	set dctMenuList = Server.CreateObject("Scripting.Dictionary")
	select case rsInput("chrStatus")
		case "S", "V": ' submitted or voice-auth
			if blnCanShip and blnErrors then
				dctMenuList.Add "btnShip", "Ship Order"
			end if
		case "A": ' approved
			if blnCanShip then
				dctMenuList.Add "btnShip", "Ship Order"
			end if
		case "Z": ' authorized
			if blnCanShip then
				dctMenuList.Add "btnShip", "Ship Order"
			end if
		case else: ' unknown status
	end select
	
	dim strStatus, strStatusText, strStatusCode
	strStatusCode = rsInput("chrStatus")
	strStatus = GetArrayValue(strStatusCode, dctOrderStatus)
	select case rsInput("chrStatus")
		case "0","1","2","3","4","5","6","7","8", "P": ' in progress
			strStatusText = "This order has not yet been submitted by the shopper. Changes can not be made unless the order is ""locked""."
		case "9": ' in progress by merchant
			strStatusText = "This order is being created or modified by the merchant, and has been locked out from the shopper's account."
		case "S":	' submitted
			strStatusText = "This order has been submitted for approval."
		case "A":	' approved
			strStatusText = "This order has been approved for processing."
		case "Z":	' authorized
			strStatusText = "Funds have not yet been deposited."
		case "X":	' deposited
			strStatusText = "Funds have been deposited and this order has been shipped."
		case "C":	' credited
			strStatusText = "Funds have been returned to purchaser."
		case "B":	' abandoned
		case "K":	' declined
		case "V":	' voice-auth required
			strStatusText = "The credit card issuer has requested verbal authorization."
		case "E":	' error
			strStatusText = "A system error occurred while processing this order. Please contact technical support."
	end select
	
	dim blnCanEditAccount, blnCanEditPayment, blnCanEditBilling, blnCanEditShipping, blnCanEditItems, blnCanEditShipOption
	if strStatusCode = "9" then
		blnCanEditAccount = blnCanEdit
		blnCanEditPayment = blnCanEdit
		blnCanEditBilling = blnCanEdit
		blnCanEditShipping = blnCanEdit
		blnCanEditItems = blnCanEdit
	else
		blnCanEditAccount = false
		blnCanEditPayment = (InStr("SVAZ", strStatusCode) > 0) and not IsNull(rsInput("intShopperID")) and CheckUserAccess(ACC_ORDER_PAYMENT_EDIT)
		blnCanEditBilling = (InStr("SVAZ", strStatusCode) > 0) and not IsNull(rsInput("intShopperID")) and CheckUserAccess(ACC_ORDER_BILLING_EDIT)
		blnCanEditShipping = (InStr("SVAZ", strStatusCode) > 0) and not IsNull(rsInput("intShopperID")) and CheckUserAccess(ACC_ORDER_SHIPPING_EDIT)
		blnCanEditItems = (InStr("SVAZXH", strStatusCode) > 0) and (CheckUserAccess(ACC_ORDER_ITEMS_EDIT) or CheckUserAccess(ACC_ORDER_NOTES_ADD) or CheckUserAccess(ACC_ORDER_RETURNS_ADD))
	end if
	
	blnCanEditBilling = blnCanEditBilling AND NOT dctErrorList.Exists("account")
	blnCanEditShipping = blnCanEditShipping AND NOT dctErrorList.Exists("account")
	blnCanEditPayment = blnCanEditPayment AND NOT dctErrorList.Exists("billing")
	blnCanEditShipOption = blnCanEditShipping AND NOT dctErrorList.Exists("shipping")
	if (Request.QueryString("print").count > 0) then
%>
	<div style="width:500;align:right;float:right;">
		<a href="<%= nonsecurebase %>store/"><img src="<%= g_imagebase %>banners/banner-sm2.jpg" border="0" /></a>
	</div>
<%
	end if
	DrawTitle "Order Details - " & STR_MERCHANT_TRACKING_PREFIX & rsInput("intID"), dctMenuList
%>

<CENTER>
<FONT SIZE="2"><B><%= STR_MERCHANT_NAME %></B></FONT><BR>
<%= STR_MERCHANT_ADDRESS1 & iif(STR_MERCHANT_ADDRESS2 <> "", ", " & STR_MERCHANT_ADDRESS2, "") %><BR>
<%= STR_MERCHANT_CITY & ", " & STR_MERCHANT_STATE & " " & STR_MERCHANT_ZIP %><BR>
<%= "Phone " & STR_MERCHANT_PHONE & "&nbsp;&nbsp;Fax " & STR_MERCHANT_FAX %>
</CENTER>
<BR>
<%
	stlBeginStdTable "100%"
	response.write "<tr><td>"
%>

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
<TR BGCOLOR="<%= cVLtYellow %>">
	<TD><B>Created:</B></TD>
	<TD><B>Submitted:</B></TD>
	<TD><B>Updated:</B></TD>
</TR>
<TR>
	<TD>&nbsp;&nbsp;<%= FormatDateTimeNoSeconds(rsInput("dtmCreated")) %>&nbsp;&nbsp;&nbsp;</TD>
	<TD>&nbsp;&nbsp;<%= FormatDateTimeNoSeconds(rsInput("dtmSubmitted")) %>&nbsp;&nbsp;&nbsp;</TD>
	<TD>&nbsp;&nbsp;<%= FormatDateTimeNoSeconds(rsInput("dtmUpdated")) %>&nbsp;&nbsp;&nbsp;</TD>
</TR>
<TR>
	<TD COLSPAN="3">&nbsp;</TD>
</TR>
<TR BGCOLOR="<%= cVLtYellow %>">
	<TD><B>Tracking Number:</B>&nbsp;&nbsp;</TD>
	<TD COLSPAN="2"><B>Status:</B></TD>
</TR>
<TR>
	<TD>&nbsp;&nbsp;<%= STR_MERCHANT_TRACKING_PREFIX & rsInput("intID") %>&nbsp;&nbsp;&nbsp;</TD>
	<TD COLSPAN="2">&nbsp;&nbsp;<%= strStatus & " - " & strStatusText %></TD>
</TR>
</TABLE>
<BR>
<%
	dim blnCanViewShipping
	blnCanViewShipping = CheckUserAccess(ACC_ORDER_SHIPPING_VIEW)
	if blnCanViewShipping then
%>
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
	<TR BGCOLOR="<%= cVLtYellow %>">
		<TD COLSPAN="3" <% if dctErrorList.Exists("shipping") then %>BGCOLOR="<%= cErrorBG %>"<% end if %>><B>Ship To:</B></TD>
	</TR>
	<TR>
<%
	if IsNull(rsInput("intShipShopperID")) or isNull(rsInput("intShopperID")) then
		response.write "<TD COLSPAN=""3"">&nbsp;&nbsp;no information"
	else
		strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & rsInput("intShipShopperID") & " AND chrStatus<>'D' and (chrType='S' OR chrType='B') and intShopperID=" & rsInput("intShopperID")
		set rsShopper = gobjConn.execute(strSQL)
		if rsShopper.eof then
			response.write "<TD COLSPAN=""3"" valign=""top"">&nbsp;&nbsp;<B>System error</B>--unable to retrieve shopper information."
		else
			response.write "<TD COLSPAN=""2"" valign=""top"">&nbsp;&nbsp;" & rsShopper("vchLastName") & ", " & rsShopper("vchFirstName") & "<br />"
			if not IsNull(rsShopper("vchCompany")) then
				response.write "&nbsp;&nbsp;" & rsShopper("vchCompany") & "<br />"
			end if
			if not IsNull(rsShopper("vchAddress1")) then
				response.write "&nbsp;&nbsp;" & rsShopper("vchAddress1") & "<br />"
			end if
			if not IsNull(rsShopper("vchAddress2")) then
				response.write "&nbsp;&nbsp;" & rsShopper("vchAddress2") & "<br />"
			end if
			response.write "&nbsp;&nbsp;" & rsShopper("vchCity") & ", " & rsShopper("vchState") & " " & rsShopper("vchZip") & "<br />"
			if not IsNull(rsShopper("vchCountry")) then
				response.write "&nbsp;&nbsp;" & rsShopper("vchCountry") & "<br />"
			end if
			response.write "</TD><TD valign=""top"">"
			if not IsNull(rsShopper("vchDayPhone")) then
				response.write "&nbsp;&nbsp;Day: " & rsShopper("vchDayPhone") & "<br />"
			end if
			if not IsNull(rsShopper("vchNightPhone")) then
				response.write "&nbsp;&nbsp;Night: " & rsShopper("vchNightPhone") & "<br />"
			end if
			if not IsNull(rsShopper("vchFax")) then
				response.write "&nbsp;&nbsp;Fax: " & rsShopper("vchFax") & "<br />"
			end if
			if not IsNull(rsShopper("vchEmail")) then
				response.write "&nbsp;&nbsp;Email: " & rsShopper("vchEmail") & "<br />"
			end if
		end if
		rsShopper.close
		set rsShopper = nothing
	end if
%>
	</TD>
</TR>
</TABLE>
<BR>
<%
	end if 
	if true then	' = false - kill gift message
%>
<TABLE CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
<%
if not IsNull(rsInput("vchReferalName")) then
	response.write "<TR><TD>Referral: " & rsInput("vchReferalName") & "</TD></TR>"
end if
%>
<TR BGCOLOR="<%= cVLtYellow %>">
	<TD<% if dctErrorList.Exists("giftmsg") then %>BGCOLOR="<%= cErrorBG %>"<% end if %>><B>Comments:</B><% if blnCanEditShipOption then %>&nbsp;&nbsp;&nbsp;<% end if %></TD>
</TR>
<TR>
	<TD>&nbsp;&nbsp;<%= replace(Server.HTMLEncode(rsInput("txtGiftMessage") & ""), vbCR, "<BR>") %></TD>
</TR>
</TABLE>
<BR>
<%
	end if 

if intBrand <> "" then
	UpdateItemViewed intID, intBrand
'	strBrandClause = "RIGHT JOIN " & STR_TABLE_INVENTORY & " AS B ON I.intInvID = B.intID WHERE B.intBrand=" & intBrand & " AND (B.chrType='I' OR B.chrType IS NULL)"

'	strSQL = "SELECT I.*,(I.mnyUnitPrice * I.intQuantity) AS mnyPrice "
'	strSQL = strSQL & " FROM " & STR_TABLE_LINEITEM & " AS I"
'	strSQL = strSQL & " RIGHT JOIN " & STR_TABLE_INVENTORY & " AS B ON I.intInvID = B.intID AND B.intBrand=" & intBrand & " AND (B.chrType='I' OR B.chrType IS NULL)"
'	strSQL = strSQL & " WHERE I.intOrderID=" & intID & " AND I.chrStatus<>'D' ORDER BY I.intOrderID, I.intID"

	strSQL = "SELECT I.*,(I.mnyUnitPrice * I.intQuantity) AS mnyPrice"
	strSQL = strSQL & " FROM TCS_LineItem AS I, TCS_Inv AS B"
	strSQL = strSQL & " WHERE I.intOrderID = " & intID & " AND I.chrStatus<>'D'"
	strSQL = strSQL & " AND B.intID=I.intInvID AND (B.chrType='I' OR B.chrType IS NULL) AND B.intBrand=" & intBrand & ""
	strSQL = strSQL & " ORDER BY I.intOrderID, I.intID"
'	response.write strSQL & "<br />"
	set rsLineItem = gobjConn.execute(strSQL)
else
'	strSQL = "SELECT *,(mnyUnitPrice * intQuantity) AS mnyPrice FROM " & STR_TABLE_LINEITEM & " WHERE intOrderID=" & intOrderID & " AND chrStatus<>'D' ORDER BY intID"
'	strSQL = "SELECT vchPartNumber, vchItemName, mnyUnitPrice, intQuantity, (mnyUnitPrice * intQuantity) AS mnyPrice FROM " & STR_TABLE_LINEITEM & " WHERE chrStatus<>'D' AND intOrderID=" & rsInput("intID") & " ORDER BY intSortOrder,intID"
'	set rsLineItem = gobjConn.execute(strSQL)
	set rsLineItem = GetOrderLineItems_Other(intID)
end if

	Brand_DrawOrderCart rsInput, rsLineItem, false, "", "", "", iif(dctErrorList.Exists("items"), cErrorBG, ""), iif(dctErrorList.Exists("shipoption"), cErrorBG, "")

	rsLineItem.close
	set rsLineItem = nothing
	
	response.write "</td></tr>"
	stlEndStdTable
end sub

sub UpdateItemViewed(intOrderID, intBrand)
	if intOrderID > 0 then
		dim strBrandClause
		strBrandClause = "RIGHT JOIN " & STR_TABLE_INVENTORY & " AS B ON I.intInvID = B.intID WHERE B.intBrand=" & intBrand & " AND (B.chrType='I' OR B.chrType IS NULL)"
		
	 	dim rsItem, strSQL, strUpdtList
		strUpdtList = ""
		strSQL = "SELECT I.intID FROM " & STR_TABLE_LINEITEM & " AS I " & strBrandClause & " AND I.intOrderID=" & intOrderID & " AND I.chrStatus IN (" & strStatusClause & ")"
'		response.write strSQL & "<br />"
		set rsItem = gobjConn.execute(strSQL)
		while not rsItem.eof
'			if rsItem("intBrand") = gintBrand then
				strUpdtList = strUpdtList & rsItem("intID") & ","
'			end if
			rsItem.MoveNext
		wend
		rsItem.close
		set rsItem = nothing
		if strUpdtList <> "" then
			strSQL = "UPDATE " & STR_TABLE_LINEITEM & " SET dtmUpdated = GETDATE(), vchUpdatedByUser = '"&Session(SESSION_MERCHANT_ULOGIN)&"', vchUpdatedByIP = '" & gstrUserIP & "', chrViewed = 'Y'"
			strSQL =  strSQL & " WHERE intID IN (" & left(strUpdtList, len(strUpdtList) - 1) & ")"
'			response.write "<br />" & strSQL & "<br />"
			gobjConn.execute(strSQL)
		end if
	end if
end sub

sub DrawTitle(strPageTitle, dctMenuList)
	dim s, c, x, i, k
	x = dctMenuList.count -1
	i = dctMenuList.items
	k = dctMenuList.keys
	s = "&nbsp;<a href=""" & STR_SEARCH_SCRIPT & """><b>Back</b></a>"
	for c = 0 to x
		if c >= 0 then
			s = s & "&nbsp;|"
		end if
		s = s & "&nbsp;<a href=""" & STR_DETAIL_SCRIPT & "&action=" & k(c) & """><b>" & i(c) & "</b></a>"
	next
	DrawFormHeader "100%", strPageTitle, s

	'response.write x & " " & s & "&nbsp;&nbsp; can ship: " & blnCanShip & "</br>"
end sub
%>