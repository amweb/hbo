<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: order_sysedit.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Allow 'progadmin' to edit raw fields in an order (for recovery purposes)
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_RAW) then
	response.redirect "menu.asp"
end if

const STR_PAGE_TITLE = "Order Details"
const STR_PAGE_TYPE = "adminorderlist"

OpenConn

dim intID, strAction
strAction = Request("action")
intID = Request("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if

if strAction = "save" then
	if intID > 0 then
		dim dctSaveList
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "@dtmUpdated", "GETDATE()"
			dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
			dctSaveList.Add "vchUpdatedByIP", gstrUserIP
			dctSaveList.Add "!chrStatus", ""
		SaveDataRecord STR_TABLE_ORDER, Request, intID, dctSaveList
		' change status to 'A' (approved)
		if Request("chrStatus") = "A" then
'			UpdateOrderStatus_Other intID, "A"
			gintOrderID = intID
			ProcessOrderApprove
			gintOrderID = ""
		end if
	end if
	response.redirect "order_detail.asp?id=" & intID
end if

dim strSQL, rsData
if intID <= 0 then
	response.redirect "order_list.asp"
end if

strSQL = "SELECT O.*, S.vchFirstName + ' ' + S.vchLastName AS S_vchShopperAccount FROM " & STR_TABLE_ORDER & " AS O, " & STR_TABLE_SHOPPER & " AS S WHERE O.intID=" & intID & " AND O.chrStatus<>'D' AND O.intShopperID *= S.intID AND S.chrType='A'"
set rsData = gobjConn.execute(strSQL)
if rsData.eof then
	response.redirect "order_list.asp"
end if


DrawPage rsData


rsData.close
set rsData = nothing

response.end

sub DrawPage(rsInput)
	dim strSQL, rsShopper, rsLineItem
%>
<FONT SIZE=1>
<FORM ACTION="order_detail.asp" METHOD="POST">
<INPUT TYPE="hidden" NAME="id" VALUE="<%= intID %>">
<TABLE BORDER="1" CELLPADDING="4" CELLSPACING="0" WIDTH="530">
<TR BGCOLOR="#DDDDDD">
	<TD ALIGN="center">
		<INPUT TYPE="submit" VALUE="Return" CLASS="button">
	</TD>
</TR>
</TABLE>
</FORM>

<FORM ACTION="order_sysedit.asp" METHOD="POST">
<%
	HiddenInput "action", "save"
	HiddenInput "id", intID
%>
<FONT SIZE=2><B>Order Details - Technical Information</B></FONT><BR>
Change Status To: <%= PulldownFromArray("chrStatus", 0, "", rsInput("chrStatus"), dctOrderStatusRawValues, "") %>
<% stlRawSubmit "", "Save" %>
</FONT>
</FORM>

<FONT SIZE=2><B>Order Details - Technical Information</B></FONT><BR>
<%
	stlBeginFormSection "530", 2
	
	DrawTechHeader "Order Information"
	DrawTechRow "Order ID:", rsInput("intID") & "-" & rsInput("chrType") & "-" & rsInput("chrStatus"), "", ""
	DrawTechRow "Type:", GetArrayValue(rsInput("chrType"), dctOrderTypeValues), "", ""
	DrawTechRow "Status:", GetArrayValue(rsInput("chrStatus"), dctOrderStatus), "", ""
	DrawTechRow "Date Created:", rsInput("dtmCreated") & " by " & rsInput("vchCreatedByUser") & " (" & rsInput("vchCreatedByIP") & ")", "", ""
	DrawTechRow "Date Updated:", rsInput("dtmUpdated") & " by " & rsInput("vchUpdatedByUser") & " (" & rsInput("vchUpdatedByIP") & ")", "", ""
	DrawTechRow "Order Modified:", GetArrayValueX(rsInput("chrModifyFlag"), dctModifyFlagValues, "no"), "", ""
	DrawTechRow "Shopper IP Address:", rsInput("vchShopperIP"), "O.vchShopperIP", 15
	DrawTechRow "Shopper Browser:", rsInput("vchShopperBrowser"), "O.vchShopperBrowser", 64
	DrawTechRow "Shipping/Conf Number:", rsInput("vchShippingNumber"), "O.vchShippingNumber", 32
	
	DrawTechHeader "Payment Information"
	dim s
	s = rsInput("chrPaymentMethod") & ""
	DrawTechRow "Payment Method:", GetArrayValue(s, dctPaymentMethod), "", ""
	if right(s,2) = "CC" then
		' credit card information
		DrawTechRow "Name on Card:", rsInput("vchPaymentCardName"), "O.vchPaymentCardName", 32
		DrawTechRow "Payment Card Type:", rsInput("vchPaymentCardType"), "O.vchPaymentCardType", 12
		DrawTechRow "Card Number:", rsInput("vchPaymentCardNumber"), "O.vchPaymentCardNumber", 32
		DrawTechRow "Card PIN:", rsInput("vchPaymentCardExtended"), "O.vchPaymentCardExtended", 8
		DrawTechRow "Card Exp Month:", rsInput("chrPaymentCardExpMonth"), "O.chrPaymentCardExpMonth", 2
		DrawTechRow "Card Exp Year:", rsInput("chrPaymentCardExpYear"), "O.chrPaymentCardExpYear", 4
	elseif right(s,2) = "EC" then
		DrawTechRow "Payment Bank Name:", rsInput("vchPaymentBankName"), "", ""
		DrawTechRow "Payment Rtn #:", rsInput("vchPaymentRtnNumber"), "", ""
		DrawTechRow "Payment Check #:", rsInput("vchPaymentCheckNumber"), "", ""
		DrawTechRow "Payment DL #:", rsInput("vchPaymentDLNumber") & " (state: " & rsInput("vchPaymentDLState") & ")", "", ""
		DrawTechRow "Payment Account Type:", GetArrayValue(rsInput("chrPaymentAcctType"), dctElecCheckAcctValues), "", ""
		DrawTechRow "Payment Account #:", rsInput("vchPaymentAcctNumber"), "", ""
	end if
	
	DrawTechHeader "Shopper Information"
	DrawTechRow "Account ID:", rsInput("intShopperID"), "O.intShopperID", 5
	if not IsNull(rsInput("intBillShopperID")) then
		strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & rsInput("intShopperID")& " AND chrStatus<>'D' AND chrType='A'"
		set rsShopper = gobjConn.execute(strSQL)
		if rsShopper.eof then
			DrawTechRow "Account ID:", rsInput("intShopperID") & "-EOF", "", ""
		else
			DrawTechRow "Account ID:", rsShopper("intID") & "-" & rsShopper("chrType") & "-" & rsShopper("chrStatus") & " (" & rsShopper("vchEmail") & ")", "", ""
			DrawTechRow "Name:", rsShopper("vchFirstName") & " " & rsShopper("vchLastName"), "", ""
			DrawTechRow "Company:", rsShopper("vchCompany"), "", ""
			DrawTechRow "Email:", rsShopper("vchEmail"), "", ""
		end if
		rsShopper.close
		set rsShopper = nothing
	end if
	DrawTechHeader "Shopper Information - Billing"
	DrawTechRow "Billing ID:", rsInput("intBillShopperID"), "O.intBillShopperID", 5
	if not IsNull(rsInput("intBillShopperID")) then
		strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & rsInput("intBillShopperID") & " AND chrStatus<>'D' AND chrType='B' AND intShopperID=" & rsInput("intShopperID")
		response.write strsql
		set rsShopper = gobjConn.execute(strSQL)
		if rsShopper.eof then
			DrawTechRow "Billing ID:", rsInput("intBillShopperID") & "-EOF", "", ""
		else
			DrawTechRow "Billing ID:", rsShopper("intID") & "-" & rsShopper("chrType") & "-" & rsShopper("chrStatus"), "", ""
			DrawTechRow "Name:", rsShopper("vchFirstName") & " " & rsShopper("vchLastName"), "", ""
			DrawTechRow "Company:", rsShopper("vchCompany"), "", ""
			DrawTechRow "Address 1:", rsShopper("vchAddress1"), "", ""
			DrawTechRow "Address 2:", rsShopper("vchAddress2"), "", ""
			DrawTechRow "City, State ZIP:", rsShopper("vchCity") & ", " & rsShopper("vchState") & " " & rsShopper("vchZip"), "", ""
			DrawTechRow "Country:", rsShopper("vchCountry"), "", ""
			DrawTechRow "Phone:", "(d)" & rsShopper("vchDayPhone") & " (n)" & rsShopper("vchNightPhone") & " (f)" & rsShopper("vchFax"), "", ""
			DrawTechRow "Email:", rsShopper("vchEmail"), "", ""
		end if
		rsShopper.close
		set rsShopper = nothing
	end if
	
	DrawTechHeader "Shopper Information - Shipping"
	DrawTechRow "Shipping ID:", rsInput("intShipShopperID"), "O.intShipShopperID", 5
	if not IsNull(rsInput("intShipShopperID")) then
		strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & rsInput("intShipShopperID") & " AND chrStatus<>'D' AND (chrType='S' OR chrType='B') AND intShopperID=" & rsInput("intShopperID")
		set rsShopper = gobjConn.execute(strSQL)
		if rsShopper.eof then
			DrawTechRow "Shipping ID:", rsInput("intShipShopperID") & "-EOF", "", ""
		else
			DrawTechRow "Shipping ID:", rsShopper("intID") & "-" & rsShopper("chrType") & "-" & rsShopper("chrStatus"), "", ""
			DrawTechRow "Name:", rsShopper("vchFirstName") & " " & rsShopper("vchLastName"), "", ""
			DrawTechRow "Company:", rsShopper("vchCompany"), "", ""
			DrawTechRow "Address 1:", rsShopper("vchAddress1"), "", ""
			DrawTechRow "Address 2:", rsShopper("vchAddress2"), "", ""
			DrawTechRow "City, State ZIP:", rsShopper("vchCity") & ", " & rsShopper("vchState") & " " & rsShopper("vchZip"), "", ""
			DrawTechRow "Country:", rsShopper("vchCountry"), "", ""
			DrawTechRow "Phone:", "(d)" & rsShopper("vchDayPhone") & " (n)" & rsShopper("vchNightPhone") & " (f)" & rsShopper("vchFax"), "", ""
			DrawTechRow "Email:", rsShopper("vchEmail"), "", ""
		end if
		rsShopper.close
		set rsShopper = nothing
	end if
	
	dim intIndex
	intIndex = 0
	DrawTechHeader "Line Items"
	strSQL = "SELECT * FROM " & STR_TABLE_LINEITEM & " WHERE chrStatus<>'D' AND intOrderID=" & rsInput("intID") & " ORDER BY intSortOrder,intID"
	set rsLineItem = gobjConn.execute(strSQL)
	while not rsLineItem.eof
		intIndex = intIndex + 1
		if rsLineItem("chrType") = "I" then
			DrawTechRow "Line " & intIndex & " (" & rsLineItem("intID") & "-" & rsLineItem("chrType") & "-" & rsLineItem("chrStatus") & "): ", rsLineItem("vchItemName") & " (Part #" & rsLineItem("vchPartNumber") & ")", "", ""
			DrawTechRow "", "Qty " & rsLineItem("intQuantity") & " @ " & SafeFormatCurrency("free", rsLineItem("mnyUnitPrice"), 2) & " + S/H: " & SafeFormatCurrency("n/a", rsLineItem("mnyShipPrice"), 2) & iif(rsLineItem("chrTaxFlag")="Y", " TAXABLE", " NONTAXABLE"), "", ""
			DrawTechRow "", "Inv ID: " & iif(IsNull(rsLineItem("intInvID")), "NULL", rsLineItem("intInvID")), "", ""
		else
			DrawTechRow "Line " & intIndex & " (" & rsLineItem("intID") & "-" & rsLineItem("chrType") & "-" & rsLineItem("chrStatus") & "): ", rsLineItem("vchItemName") & " (NOTE)", "", ""
		end if
		rsLineItem.MoveNext
	wend
	rsLineItem.close
	set rsLineItem = nothing
	DrawTechRow "Total Items:", intIndex, "", ""
	
	DrawTechHeader "Pricing Information"
	DrawTechRow "Shipping:", SafeFormatCurrency("n/a", rsInput("mnyShipAmount"),2), "", 10
	DrawTechRow "Shipping Taxable:", rsInput("chrShipTaxFlag"), "", 1
	DrawTechRow "Non-Taxable Subtotal:", SafeFormatCurrency("n/a", rsInput("mnyNonTaxSubtotal"),2), "", 10
	DrawTechRow "Taxable Subtotal:", SafeFormatCurrency("n/a", rsInput("mnyTaxSubtotal"),2), "", 10
	DrawTechRow "Tax:", SafeFormatCurrency("n/a", rsInput("mnyTaxAmount"),2) & " (" & rsInput("fltTaxRate") * 100 & "%, zone " & rsInput("intTaxZone") & ")", "", ""
	DrawTechRow "Grand Total:", SafeFormatCurrency("n/a", rsInput("mnyGrandTotal"),2), "", 10
	
	intIndex = 0
	DrawTechHeader "Transactions"
	strSQL = "SELECT * FROM " & STR_TABLE_TRANS & " WHERE chrStatus<>'D' AND intOrderID=" & rsInput("intID") & " ORDER BY intID"
	set rsLineItem = gobjConn.execute(strSQL)
	while not rsLineItem.eof
		intIndex = intIndex + 1
		DrawTechRow rsLineItem("intID") & "-" & rsLineItem("chrType") & "-" & rsLineItem("chrStatus") & ":", SafeFormatCurrency("n/a", rsLineItem("mnyTransAmount"), 2) & " " & rsLineItem("chrAccountType") & "-" & rsLineItem("chrTransType") & iif(IsNull(rsLineItem("vchAuthCode")), "", " Auth&nbsp;" & rsLineItem("vchAuthCode")) & iif(IsNull(rsLineItem("vchAVCode")), "", " AV&nbsp;" & rsLineItem("vchAVCode")) & iif(IsNull(rsLineItem("vchRefCode")), "", " Ref&nbsp;" & rsLineItem("vchRefCode")) & iif(IsNull(rsLineItem("vchOrderCode")), "", " Order&nbsp;" & rsLineItem("vchOrderCode")) & iif(IsNull(rsLineItem("vchDeclineCode")), "", " Decline&nbsp;" & rsLineItem("vchDeclineCode")) & " " & rsLineItem("dtmTransDate"), "", ""
		DrawTechRow "", "<A HREF=""#"" TITLE=""" & Server.HTMLEncode(rsLineItem("vchShopperMsg") & "") & """>Shopper Message</A>  <A HREF=""#"" TITLE=""" & Server.HTMLEncode(rsLineItem("txtComments") & "") & """>Log</A>", "", ""
		rsLineItem.MoveNext
	wend
	rsLineItem.close
	set rsLineItem = nothing
	DrawTechRow "Total Trans:", intIndex, "", ""
	
	stlEndFormSection
%>
</TABLE>

</FONT>
<%
end sub

sub DrawTechRow(strHeader, strValue, strFieldName, strLength)
	stlRawCell strHeader
	'if strFieldName = "" then
		stlRawCell strValue
	'else
	'	if FormErrors.count > 0 then
			strValue = Request(strFieldName)
	'	end if
	'	stlTextInput strFieldName, 40, strLength, strValue, "-", "", ""
	'end if
end sub

sub DrawTechHeader(strA)
	stlRawCellX "<BR><B>" & strA & "</B>", "", "", "", 2
end sub

sub ChangeOrderStatus(strNewStatus)
	dim dctSaveList
	set dctSaveList = Server.CreateObject("Scripting.Dictionary")
		dctSaveList.Add "@dtmUpdated", "GETDATE()"
		dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
		dctSaveList.Add "vchUpdatedByIP", gstrUserIP
		dctSaveList.Add "chrStatus", strNewStatus
	SaveDataRecord STR_TABLE_ORDER, Request, intID, dctSaveList
end sub

' -----------------------------------
' ====== PROCESS ORDER APPROVE ======
' -----------------------------------

sub ProcessOrderApprove
	' process flow:
	'  (1) set order status to 'approved'
	'  (2) display/email invoice (unless declined or rejected)

	' SWSCodeChange - 7/20 - added merchant email
	dim strMerchantMessage, strSubject
	strMerchantMessage = "An order has been approved. Please review the order by visiting your merchant website." & vbcrlf
	strMerchantMessage = strMerchantMessage & vbcrlf
	strMerchantMessage = strMerchantMessage & "Order # " & STR_MERCHANT_TRACKING_PREFIX & gintOrderID & vbcrlf
	strMerchantMessage = strMerchantMessage & "Merchant Website: " & nonsecurebase & "admin/" & vbcrlf
	strMerchantMessage = strMerchantMessage & vbcrlf
	if lcase(strServerHost) = "awsdev" then
		AddStdEmailSecurity strMerchantMessage
	end if
	strSubject = "Order Notification # " & STR_MERCHANT_TRACKING_PREFIX & gintOrderID & ""
	SendMerchantEmail strSubject, strMerchantMessage		' SWSCodeChange - 7/20 - added merchant email

end sub
%>