<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

response.buffer = true

dim fso
set fso = Server.CreateObject("Scripting.FileSystemObject")

CheckAdminLogin

if not CheckUserAccess(ACC_RAW) then
	response.redirect "main.asp"
end if

dim strPageType, strPageTitle
strPageType = "admininvlist"
strPageTitle = "Image Scan"

if Request("action") <> "scan" then
	
	DrawPage
	
else
	OpenConn
	
	dim strSQL, rsData
	
	response.write "Clearing old data..."
	response.flush
	' clear the height and width data for all existing items
	strSQL = "UPDATE " & STR_TABLE_INVENTORY & " SET intSmImageHeight=NULL, intSmImageWidth=NULL, intLgImageHeight=NULL, intLgImageWidth=NULL WHERE chrStatus<>'D'"
	gobjConn.execute(strSQL)
	response.write "<B>ok</B><BR>"
	
	dim objScan, dctList
	set dctList = Server.Createobject("Scripting.Dictionary")
	set objScan = Server.CreateObject("IIS4Utils.ImageUtils")
	
	response.write "Scanning products...<BR>"
	strSQL = "SELECT intID, vchImageURL FROM " & STR_TABLE_INVENTORY & " WHERE chrStatus<>'D' AND vchImageURL IS NOT NULL"
	set rsData = gobjConn.execute(strSQL)
	dim intSmHeight, intSmWidth, intLgHeight, intLgWidth, intBitDepth, intColorComponents
	while not rsData.eof
		ScanImageSize Server.MapPath(imagebase & "products/small/" & rsData("vchImageURL")), intSmHeight, intSmWidth
		response.write "Product " & rsData("intID") & ": small '" & rsData("vchImageURL") & "'"
		response.write " " & intSmHeight & "x" & intSmWidth
		response.write "<BR>"
		
		ScanImageSize Server.MapPath(imagebase & "products/big/" & rsData("vchImageURL")), intLgHeight, intLgWidth
		response.write "Product " & rsData("intID") & ": big '" & rsData("vchImageURL") & "'"
		response.write " " & intLgHeight & "x" & intLgWidth
		response.write "<BR>"
		
		dctList.Add rsData("intID") & "", intSmHeight & "," & intSmWidth & "," & intLgHeight & "," & intLgWidth
		
		response.flush
		rsData.MoveNext
	wend
	response.write "<B>ok</B><BR>"
	response.flush
	rsData.close
	set rsData = nothing
	
	dim i, k, c, x, s, a, b
	x = dctList.count - 1
	i = dctList.items
	k = dctList.keys
	for c = 0 to x
		s = i(c)
		a = InStr(s, ",")
		intSmHeight = left(s, a - 1)
		a = a + 1
		b = InStr(a, s, ",")
		intSmWidth = mid(s, a, b - a)
		a = b + 1
		b = InStr(a, s, ",")
		intLgHeight = mid(s, a, b - a)
		intLgWidth = mid(s, b + 1)
		if intSmHeight <> "0" or intLgHeight <> "0" then
			strSQL = "UPDATE " & STR_TABLE_INVENTORY & " SET "
			if intSmheight <> "0" then
				strSQL = strSQL & "intSmImageHeight=" & intSmHeight & ", intSmImageWidth="& intSmWidth
				if intLgHeight <> "0" then
					strSQL = strSQL & ","
				end if
			end if
			if intLgHeight <> "0" then
				strSQL = strSQL & "intLgImageHeight=" & intLgHeight & ", intLgImageWidth=" & intLgWidth
			end if
			strSQL = strSQL & " WHERE intID=" & k(c)
'			response.write strSQL & "<BR>"
			ConnExecute strSQL
		end if
	next
	
	set objScan = nothing
	
	response.write "End of scan."
	CloseConn
end if
response.end

sub DrawPage
%>
This process will scan all inventory data for their associated images and determine the height and width of the image. This information will be used to optimize the display of products to web users.<BR>
<BR>
This process may take a few minutes. If you are sure you want to do this, please <A HREF="imagescan.asp?action=scan">click here</A>.
<%
end sub

sub ScanImageSize(strPath, intHeight, intWidth)
	const DEBUG = false
	intHeight = 0
	intWidth = 0
	
	strPath = lcase(strPath)
	Dim stream, s
	on error resume next
	Set stream = fso.OpenTextFile(strPath, 1)
	if err.number <> 0 then
		err.clear
'		response.write "<BR><B>fnf</B><BR>"
		exit sub
	end if
	on error goto 0
	if right(strPath, 4) = ".gif" then
		dim lowByte, highByte
		s = stream.read(10)
		lowByte = asc(mid(s,7,1))
		highByte = asc(mid(s,8,1))
		intWidth = highByte * 256 + lowByte
		lowByte = asc(mid(s,9,1))
		highByte = asc(mid(s,10,1))
		intHeight = highByte * 256 + lowByte
	elseif right(strPath,4) = ".jpg" or right(strPath,5) = ".jpeg" then
		dim a, intMarkerType, intMarkerLength, n
		
		' look for marker (0xFF)
if DEBUG then 		response.write "<BR>"
		while Response.IsClientConnected()
			a = 0
			n = 0
			while a <> &hFF
				a = asc(stream.read(1))
				n = n + 1
			wend
			if n > 1 then
if DEBUG then 				response.write "skipped " & n & " bytes looking for 0xFF<BR>"
			end if
			' look for first non-0xFF char
			n = 0
			while a = &hFF
				a = asc(stream.read(1))
				n = n + 1
			wend
			if n > 1 then
if DEBUG then 				response.write "skipped " & n & " padding bytes (0xFF)<BR>"
			end if
			intMarkerType = a
			if intMarkerType >= &hC0 and intMarkerType <= &hCF and intMarkerType <> &hC4 and intMarkerType <> &hC8 and intMarkerType <> &hCC then	' 0xC0-0xCF = SOFn marker
				' SOFn marker
				' data format:
				'   1 byte = data precision
				'   2 bytes = image height
				'   2 bytes = image width
				'   1 byte = # of color components
				intMarkerLength = asc(stream.read(1)) * 256 + asc(stream.read(1))
if DEBUG then 				response.write "[" & intMarkerType & "," & intMarkerLength & "] "
if DEBUG then 				response.write "SOFn marker<BR>"
				if a >= 8 then
					a = asc(stream.read(1))
if DEBUG then 					response.write "Data precision: " & a & "<BR>"
					a = asc(stream.read(1)) * 256 + asc(stream.read(1))
					intHeight = a
if DEBUG then 					response.write "image height: " & a & "<BR>"
					a = asc(stream.read(1)) * 256 + asc(stream.read(1))
					intWidth = a
if DEBUG then 					response.write "image width: " & a & "<BR>"
					a = asc(stream.read(1))
if DEBUG then 					response.write "color components: " & a & "<BR>"
					if a <> 3 then
						response.write "<B>" & a & " components!</B>"
					end if
					if intMarkerLength > 8 then
						stream.read(intMarkerLength - 8)	' skip remaining data
					end if
				else
if DEBUG then 					response.write "segment too short<BR>"
					stream.read(intMarkerLength - 2)	' skip data
				end if
			elseif intMarkerType = &hFE then	' 0xFE = comment marker
				intMarkerLength = asc(stream.read(1)) * 256 + asc(stream.read(1))
if DEBUG then 				response.write "[" & intMarkerType & "," & intMarkerLength & "] "
if DEBUG then 				response.write "comment marker<BR>"
if DEBUG then 				response.write "comment: " & Server.HTMLEncode(stream.read(intMarkerLength - 2) & "") & "<BR>"
			elseif intMarkerType = &hD8 then	'0xD8 = SOI - start of image (beginning of data stream)
if DEBUG then 				response.write "[" & intMarkerType & ",0] "
if DEBUG then 				response.write "SOI marker<BR>"
			elseif intMarkerType = &hD9 then	'0xD9 = EOI - end of image (end of data stream)
if DEBUG then 				response.write "[" & intMarkerType & ",0] "
if DEBUG then 				response.write "EOI marker<BR>"
			elseif intMarkerType = &hDA then	'0xDA = SOS - start of scan (compressed image data)
if DEBUG then 				response.write "[" & intMarkerType & ",eof] "
if DEBUG then 				response.write "SOS marker<BR>"
				stream.close
				exit sub
			elseif intMarkerType >= &hE0 and intMarkerType <= &hEC then	' 0xE0 - 0xEC - APPn
				intMarkerLength = asc(stream.read(1)) * 256 + asc(stream.read(1))
if DEBUG then 				response.write "[" & intMarkerType & "," & intMarkerLength & "] "
if DEBUG then 				response.write "APPn marker<BR>"
				stream.read(intMarkerLength - 2)
			else
				intMarkerLength = asc(stream.read(1)) * 256 + asc(stream.read(1))
if DEBUG then 				response.write "[" & intMarkerType & "," & intMarkerLength & "] "
if DEBUG then 				response.write "unknown marker<BR>"
				stream.read(intMarkerLength - 2)	' skip segment data
			end if
		wend
	end if
	stream.close
end sub
%>