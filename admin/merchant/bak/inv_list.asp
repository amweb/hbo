<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: inv_list.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Inventory Admin
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_INVENTORY_VIEW) then
	response.redirect "main.asp"
end if

dim blnCanEdit
blnCanEdit = CheckUserAccess(ACC_INVENTORY_EDIT)

dim strPageType, strPageTitle

strPageType = "admininvlist"
strPageTitle = "Inventory Listing"

const STR_LIST_SCRIPT = "inv_list.asp?"
const STR_MOVE_SCRIPT = "inv_move.asp?"
const STR_CLONE_SCRIPT = "inv_clone.asp?"
const STR_DETAIL_FOLDER_SCRIPT = "inv_fdetail.asp?"
const STR_DETAIL_BRAND_SCRIPT = "inv_bdetail.asp?"
const STR_DETAIL_ITEM_SCRIPT = "inv_idetail.asp?"
const STR_STOCK_LOW_SCRIPT = "inv_stock.asp?"
const STR_STOCK_HIGH_SCRIPT = "inv_stock.asp?"

OpenConn

dim strSQL, rsData

' gather inventory statistics
dim intLowCount, intHighCount, intInvCount
strSQL = "SELECT "
strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_INVENTORY & " WHERE chrStatus<>'D' AND chrType='I') AS intCount,"
strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_INVENTORY & " WHERE chrStatus<>'D' AND chrType='I' AND intStock < intLowStock) AS intLowCount,"
strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_INVENTORY & " WHERE chrStatus<>'D' AND chrType='I' AND intStock > intHighStock) AS intHighCount"
set rsData = gobjConn.execute(strSQL)
intInvCount = rsData("intCount")
intLowCount = rsData("intLowCount")
intHighCount = rsData("intHighCount")
rsData.close
set rsData = nothing

' get folder list
dim dctNavList, intParentID

intParentID = Request("parentid")
if IsNumeric(intParentID) then
	intParentID = CLng(intParentID)
else
	intParentID = 0
end if

BuildNavList intParentID

strSQL = "SELECT intID, chrType, chrStatus, intSortOrder, vchPartNumber, vchItemName, mnyItemPrice, mnyShipPrice, intStock"
strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY
strSQL = strSQL & " WHERE chrStatus<>'D' AND chrType in ('A', 'I') AND intParentID=" & intParentID
strSQL = strSQL & " ORDER BY chrType, intSortOrder"
set rsData = gobjConn.execute(strSQL)


DrawPage


rsData.close
set rsData = nothing
response.end

sub DrawPage
	dim c, strPrevType, blnCanMoveUp, blnCanMoveDown
	dim intID, strType, strStatus, strPartNumber, strName, mnyPrice, mnyShipPrice, intStock
	
	call DrawHeaderUpdate()
	
	DrawListTitle
	DrawListNavigation
	DrawListHeader
	strPrevType = ""
	if rsData.eof then
		%>
		<TR>
		<TD></TD>
		<TD COLSPAN=6><%= font(1) %>&nbsp;<BR>
		<%= font(2) %><I>This folder contains no items or sub-folders.</I></FONT><BR>
		&nbsp;</TD>
		</TR>
		<%
	else
		while not rsData.eof
			if strPrevType <> rsData("chrType") then
				strPrevType = rsData("chrType")
				c = 0
			end if
			c = c + 1
			intID = rsData("intID")
			strType = rsData("chrType")
			strStatus = rsData("chrStatus")
			strPartNumber = rsData("vchPartNumber")
			strName = rsData("vchItemName")
			mnyPrice = rsData("mnyItemPrice")
			mnyShipPrice = rsData("mnyShipPrice")
			intStock = rsData("intStock")
			blnCanMoveUp = (c > 1) and blnCanEdit
			blnCanMoveDown = false
			rsData.MoveNext
			if not rsData.eof then
				if rsData("chrType") = strType then
					blnCanMoveDown = true and blnCanEdit
				end if
			end if
			DrawListItem intID, strType, strStatus, strName, strPartNumber, mnyPrice, mnyShipPrice, intStock, blnCanMoveUp, blnCanMoveDown
			if not rsData.eof then
				DrawListItemSep
			end if
		wend
	end if
	DrawListFooter
end sub

sub DrawListTitle
%>
<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%">
<TR>
	<TD><%= font(5) %><%= strPageTitle %></TD>
<% if not blnPrintMode then %>
	<TD ALIGN=RIGHT><%= font(1) %>
	<% ' page actions %>
	<A HREF="<%= STR_DETAIL_FOLDER_SCRIPT %>parentid=<%= intParentID %>">New Folder<img src="<%= imagebase %>icon_newfolder.gif" width="21" height="16" alt="" border="0" align="absmiddle" /></A>
	&nbsp;
	<A HREF="<%= STR_DETAIL_ITEM_SCRIPT %>parentid=<%= intParentID %>">New Inventory Item<IMG SRC="<%= imagebase %>icon_newproduct.gif" WIDTH="21" HEIGHT="16" ALT="" BORDER="0" ALIGN=ABSMIDDLE></A>
	</TD>
<% end if %>
</TR>
</TABLE>
<%= spacer(1,5) %><BR>

	<% stlBeginInfoBar INT_PAGE_WIDTH %>
	<B>Total Inventory:</B> <%= intInvCount %>&nbsp;
	<B><A HREF="<%= STR_STOCK_LOW_SCRIPT %>">Low Stock:</A></B> <%= intLowCount %>&nbsp;
	<B><A HREF="<%= STR_STOCK_HIGH_SCRIPT %>">High Stock:</A></B> <%= intHighCount %>&nbsp;
	</FONT></TD>
	<TD ALIGN=RIGHT><%= fontx(1,1,cWhite) & FormatDateTime(date,1) %>
	<% stlEndInfoBar %>
<%
end sub

sub DrawListNavigation
	' dctNavList - reversed order, key=ID, item=name
	dim c, x, i, k
	x = dctNavList.count - 1
	i = dctNavList.items
	k = dctNavList.keys
	response.write "<TABLE BORDER=0 CELLPADDING=6 CELLSPACING=0 WIDTH=""100%"">"
	response.write "<TR><TD>" & font(1)
	for c = x to 0 step -1
		if c > 0 then
			response.write "<B><A HREF=""" & STR_LIST_SCRIPT & "parentid=" & k(c) & """>" & i(c) & "</A></B> : "
		else
			response.write i(c)
		end if
	next
	response.write "</TD>"
	if x > 0 then
		response.write "<TD ALIGN=RIGHT>" & font(1) & "<A HREF=""" & STR_LIST_SCRIPT & "parentid=" & k(1) & """>To Previous Folder<IMG SRC=""" & imagebase & "icon_folderup.gif"" WIDTH=""16"" HEIGHT=""16"" ALT=""To Previous Folder"" BORDER=""0"" ALIGN=""absmiddle"" HSPACE=""5""></A></TD>"
	end if
	response.write "</TR></TABLE>"
end sub

sub BuildNavList(byVal intParentID)
	dim strSQL, rsData
	set dctNavList = Server.CreateObject("Scripting.Dictionary")
	while intParentID <> 0
		strSQL = "SELECT intParentID, vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intParentID & " AND chrStatus<>'D'"
		set rsData = gobjConn.execute(strSQL)
		dctNavList.Add intParentID, rsData("vchItemName") & ""
		intParentID = rsData("intParentID")
		rsData.close
		set rsData = nothing
	wend
	dctNavList.Add 0, "Top"
end sub

sub DrawListHeader
%>
<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVLtGrey) %>">
<TR>
	<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cWhite %>">
	<TR>
		<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%">
		<TR BGCOLOR="<%= cVLtYellow %>">
			<TD><%= font(1) %>&nbsp;</TD>
			<TD NOWRAP><%= font(1) %><B>Name</B></TD>
			<TD NOWRAP><%= font(1) %><B>&nbsp;Part #&nbsp;</B></TD>
			<TD NOWRAP ALIGN="right"><%= font(1) %><B>&nbsp;Price&nbsp;</B></TD>
			<TD NOWRAP ALIGN="right"><%= font(1) %><B>&nbsp;Shipping&nbsp;</B></TD>
			<TD NOWRAP ALIGN="center"><%= font(1) %><B>&nbsp;Qty&nbsp;</B></TD>
			<TD NOWRAP ALIGN="right"><%= font(1) %><B>Status</B></TD>
		</TR>
<%
end sub

sub DrawListItem(intID, strType, strStatus, strName, strPartNumber, mnyPrice, mnyShipPrice, intQty, blnCanMoveUp, blnCanMoveDown)
	dim strActionLabel
	strActionLabel = iif(blnCanEdit, "Edit", "Move")
	select case strType
		case "I": ' item
%>
		<TR VALIGN=BOTTOM>
			<TD ALIGN=CENTER><A HREF="<%= STR_DETAIL_ITEM_SCRIPT %>id=<%= intID %>"><IMG SRC="<%= imagebase %>icon_product.gif" WIDTH="16" HEIGHT="16" ALT="<%= strActionlabel %> This Item" BORDER="0"></A></TD>
			<TD><%= font(2) %><A HREF="<%= STR_DETAIL_ITEM_SCRIPT %>id=<%= intID %>"><%= strName %></A></TD>
			<TD NOWRAP><%= font(1) %>&nbsp;<%= iif(IsNull(strPartNumber), "n/a", strPartNumber) %>&nbsp;</TD>
			<TD NOWRAP ALIGN="right">&nbsp;<%= SafeFormatCurrency(fontx(1,1,cLight) & "n/a</FONT>", mnyPrice, 2) %>&nbsp;</TD>
			<TD NOWRAP ALIGN="right">&nbsp;<%= SafeFormatCurrency(fontx(1,1,cLight) & "n/a</FONT>", mnyShipPrice, 2) %>&nbsp;</TD>
			<TD NOWRAP ALIGN="center">&nbsp;<%= intQty %>&nbsp;</TD>
			<TD NOWRAP ALIGN="right"><%= font(1) %><%= GetStatusStr(strStatus, true) %></TD>
		</TR>
<% if not blnPrintMode then %>
		<TR>
			<TD></TD>
			<TD><%= fontx(1,1,cLight) %><% if blnCanEdit then %><A HREF="<%= STR_DETAIL_ITEM_SCRIPT %>id=<%= intID %>"><%= strActionLabel %></A>&nbsp;<% end if %></TD>
			<TD NOWRAP ALIGN=RIGHT COLSPAN=5><% if blnCanEdit then %><A HREF="<%= STR_CLONE_SCRIPT %>action=clone&source=<%= intID %>&parent=<%= intParentID %>">Clone Item</A> | <% if blnCanMoveUp then %><%= fontx(1,1,cLight) %><A HREF="<%= STR_MOVE_SCRIPT %>action=resort&dir=up&parent=<%= intParentID %>&source=<%= intID %>">Move Up</A><% else %><%= fontx(1,1,cDisabled) %>Move Up<% end if %></FONT> | 
			<% if blnCanMoveDown then %><A HREF="<%= STR_MOVE_SCRIPT %>action=resort&dir=down&parent=<%= intParentID %>&source=<%= intID %>">Move Down</A><% else %><%= fontx(1,1,cDisabled) %>Move Down<% end if %></FONT> | <A HREF="<%= STR_MOVE_SCRIPT %>action=move&source=<%= intID %>&parent=<%= intParentID %>">Move to Folder...</A></FONT><% end if %>
			</TD>
		</TR>
<%
			end if
		case "A": ' folder
%>
		<TR VALIGN=BOTTOM>
			<TD ALIGN=CENTER><A HREF="<%= STR_LIST_SCRIPT %>parentid=<%= intID %>"><IMG SRC="<%= imagebase %>icon_folder.gif" WIDTH="16" HEIGHT="16" ALT="Open This Folder" BORDER="0"></A></TD>
			<TD><%= font(2) %><A HREF="<%= STR_LIST_SCRIPT %>parentid=<%= intID %>"><%= strName %></A></TD>
			<TD NOWRAP><%= fontx(1,1,cLight) & "n/a</FONT>" %></TD>
			<TD NOWRAP ALIGN="right"><%= fontx(1,1,cLight) & "&nbsp;n/a&nbsp;</FONT>" %></TD>
			<TD NOWRAP ALIGN="right"><%= fontx(1,1,cLight) & "&nbsp;n/a&nbsp;</FONT>" %></TD>
			<TD NOWRAP ALIGN="center"><%= fontx(1,1,cLight) & "&nbsp;n/a&nbsp;</FONT>" %></TD>
			<TD NOWRAP ALIGN="right"><%= font(1) %><%= GetStatusStr(strStatus, true) %></TD>
		</TR>
<% if not blnPrintMode then %>
		<TR>
			<TD></TD>
			<TD><%= fontx(1,1,cLight) %><A HREF="<%= STR_LIST_SCRIPT %>parentid=<%= intID %>">Open</A><% if blnCanEdit then %> | <A HREF="<%= STR_DETAIL_FOLDER_SCRIPT %>id=<%= intID %>"><%= strActionLabel %></A><% end if %></TD>
			<TD NOWRAP ALIGN=RIGHT COLSPAN=5><% if blnCanEdit then %><A HREF="<%= STR_CLONE_SCRIPT %>action=clone&source=<%= intID %>&parent=<%= intParentID %>">Clone Item</A> | <% if blnCanMoveUp then %><%= fontx(1,1,cLight) %><A HREF="<%= STR_MOVE_SCRIPT %>action=resort&dir=up&parent=<%= intParentID %>&source=<%= intID %>">Move Up</A><% else %><%= fontx(1,1,cDisabled) %>Move Up<% end if %></FONT> | 
			<% if blnCanMoveDown then %><A HREF="<%= STR_MOVE_SCRIPT %>action=resort&dir=down&parent=<%= intParentID %>&source=<%= intID %>">Move Down</A><% else %><%= fontx(1,1,cDisabled) %>Move Down<% end if %></FONT> | <A HREF="<%= STR_MOVE_SCRIPT %>action=move&source=<%= intID %>&parent=<%= intParentID %>">Move to Folder...</A></FONT><% end if %>
			</TD>
		</TR>
<%
		end if
	end select
end sub

sub DrawListItemSep
%>
		<TR>
			<TD COLSPAN=7><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVVLtGrey) %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
		</TR>
<%
end sub

sub DrawListFooter
%>
		</TABLE></TD>
	</TR>
	</TABLE></TD>
</TR>
</TABLE>
<%
end sub
%>