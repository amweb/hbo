<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: report_shopper.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Generate report by shopper
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_REPORT_VIEW) then
	response.redirect "menu.asp"
end if

dim strPageType, strPageTitle

strPageType = "adminrepsummary"
strPageTitle = "Order Report By Shopper"

OpenConn

dim strSQL, rsData, dtmStart, dtmEnd
GetReportDateRange dtmStart, dtmEnd

strSQL = "SELECT"
strSQL = stRSQL & " COUNT(DISTINCT O.intID) AS intOrderCount,"
strSQL = strSQL & " SUM(O.mnyGrandTotal) AS mnyGrandTotal,"
strSQL = strSQL & " SUM(O.mnyTaxAmount) AS mnyTaxAmount,"
strSQL = strSQL & " SUM(O.mnyShipAmount) AS mnyShipAmount,"
strSQL = strSQL & " B.intID AS intShopperID,"
strSQL = strSQL & " B.vchLastName,"
strSQL = strSQL & " B.vchFirstName,"
strSQL = strSQL & " O.chrStatus"
strSQL = strSQL & " FROM " & STR_TABLE_ORDER & " AS O, " & STR_TABLE_SHOPPER & " AS B"
strSQL = strSQL & " WHERE O.chrStatus IN ('H','X')"	' only show shipped or deposited orders
strSQL = strSQL & " AND (O.intShopperID = B.intID) OR (O.intShopperID = -1 AND B.intID = O.intBillShopperID)"
strSQL = strSQL & " AND O.dtmShipped >= '" & dtmStart & "' AND O.dtmShipped <= '" & dtmEnd & "'"
strSQL = strSQL & " GROUP BY B.intID, B.vchLastName, B.vchFirstName, O.chrStatus"
strSQL = strSQL & " ORDER BY B.vchLastName, B.vchFirstName"

'strSQL = strSQL & " UNION SELECT"
'strSQL = stRSQL & " 1 AS intOrderCount,"
'strSQL = strSQL & " O.mnyGrandTotal AS mnyGrandTotal,"
'strSQL = strSQL & " O.mnyTaxAmount AS mnyTaxAmount,"
'strSQL = strSQL & " O.mnyShipAmount AS mnyShipAmount,"
'strSQL = strSQL & " B.intID AS intShopperID,"
'strSQL = strSQL & " B.vchLastName,"
'strSQL = strSQL & " B.vchFirstName,"
'strSQL = strSQL & " O.chrStatus"
'strSQL = strSQL & " FROM " & STR_TABLE_ORDER & " AS O, " & STR_TABLE_SHOPPER & " AS B"
'strSQL = strSQL & " WHERE O.intShopperID = -1 AND O.intBillShopperID = B.intID"
''strSQL = strSQL & " GROUP BY B.intID, B.vchLastName, B.vchFirstName, O.chrStatus"
'strSQL = strSQL & " ORDER BY B.vchLastName, B.vchFirstName"
'response.write strsql
set rsData = gobjConn.execute(strSQL)


DrawPage


rsData.close
set rsData = nothing
response.end

sub DrawPage
	call DrawHeaderUpdate()
%>
<CENTER><%= font(1) %>
<%= font(2) %><B><%= STR_SITE_NAME %><BR>
<%= strPageTitle %><BR>
<%= DateValue(dtmStart) %> to <%= DateValue(dtmEnd) %></B></FONT><BR>
<BR>
<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="2">
<TR>
	<TD>&nbsp;</TD>
	<TD ALIGN="center"><B># Orders</B></TD>
	<TD ALIGN="center" COLSPAN="2"><B>Open Orders</B></TD>
	<TD ALIGN="center" COLSPAN="2"><B>Shipped Orders</B></TD>
	<TD ALIGN="center" COLSPAN="2"><B>Declined Orders</B></TD>
	<TD ALIGN="center" COLSPAN="2"><B>Credited Orders</B></TD>
</TR>
<%
	dim intShopperID, strShopper, intOrderCount, mnyOpenTotal, mnyShipTotal, mnyDeclineTotal, mnyCreditTotal
	dim intOpenCount, intShipCount, intDeclineCount, intCreditCount
	intShopperID = 0
	while not rsData.eof
		if intShopperID <> rsData("intShopperID") then
			if intShopperID <> 0 then
				intOrderCount = intOpenCount + intShipCount + intDeclineCount + intCreditCount
%>
<TR>
	<TD><B><%= strShopper %></B></TD>
	<TD ALIGN="center"><%= intOrderCount %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", mnyOpenTotal, 2) %></TD>
	<TD ALIGN="center"><%= intOpenCount %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", mnyShipTotal, 2) %></TD>
	<TD ALIGN="center"><%= intShipCount %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", mnyDeclineTotal, 2) %></TD>
	<TD ALIGN="center"><%= intDeclineCount %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", mnyCreditTotal, 2) %></TD>
	<TD ALIGN="center"><%= intCreditCount %></TD>
</TR>
<%
			end if
			intShopperID = rsData("intShopperID")
			strShopper = rsData("vchLastName") & ", " & rsData("vchFirstName")
			intOrderCount = 0
			mnyOpenTotal = 0
			mnyShipTotal = 0
			mnyDeclineTotal = 0
			mnyCreditTotal = 0
			intOpenCount = 0
			intShipCount = 0
			intDeclineCount = 0
			intCreditCount = 0
		end if
		select case rsData("chrStatus")
			case "S", "A", "Z": ' open orders
				mnyOpenTotal = mnyOpenTotal + rsData("mnyGrandTotal")
				intOpenCount = intOpenCount + rsData("intOrderCount")
			case "H", "X": ' shipped orders
				mnyShipTotal = mnyShipTotal + rsData("mnyGrandTotal")
				intShipCount = intShipCount + rsData("intOrderCount")
			case "K": ' declined orders
				mnyDeclineTotal = mnyDeclineTotal + rsData("mnyGrandTotal")
				intDeclineCount = intDeclineCount + rsData("intOrderCount")
			case "C": ' credited orders
				mnyCreditTotal = mnyCreditTotal + rsData("mnyGrandTotal")
				intCreditCount = intCreditCount + rsData("intOrderCount")
		end select
		rsData.MoveNext
	wend
	if intShopperID <> 0 then
		intOrderCount = intOpenCount + intShipCount + intDeclineCount + intCreditCount
%>
<TR>
	<TD><B><%= strShopper %></B></TD>
	<TD ALIGN="center"><%= intOrderCount %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", mnyOpenTotal, 2) %></TD>
	<TD ALIGN="center"><%= intOpenCount %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", mnyShipTotal, 2) %></TD>
	<TD ALIGN="center"><%= intShipCount %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", mnyDeclineTotal, 2) %></TD>
	<TD ALIGN="center"><%= intDeclineCount %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", mnyCreditTotal, 2) %></TD>
	<TD ALIGN="center"><%= intCreditCount %></TD>
</TR>
<%
	end if
%>
</TABLE>
</FONT></CENTER>
<%
end sub
%>