<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: brand_order_export.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Order List Export (INCOMPLETE)
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_SHOPPER_VIEW) then
	response.redirect "menu.asp"
end if

const STR_PAGE_TITLE = "Order List Export"
const STR_PAGE_TYPE = "adminsorderexport"

OpenConn

dim intID, strAction
strAction = Request("action")
intID = Request("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if

if strAction = "submit" then
	SetExportFlags Request("strIncludeBilling") = "Y", Request("strIncludeShipping") = "Y", Request("strFormat")
	response.redirect "order_list.asp?auxaction=export"
end if


DrawPage


response.end

sub DrawPage()
	dim strIncludeBilling, strIncludeShipping, strFormat, blnIncludeBilling, blnIncludeShipping
	if FormErrors.count = 0 then
		GetExportFlags blnIncludeBilling, blnIncludeShipping, strFormat
		strIncludeBilling = iif(blnIncludeBilling, "Y", "")
		strIncludeShipping = iif(blnIncludeShipping, "Y", "")
	else
		strIncludeBilling = Rquest("strIncludeBilling")
		strIncludeShipping = Request("strIncludeShipping")
		strFormat = Request("strFormat")
	end if
	if strFormat = "" then
		strFormat = "CSV"
	end if
	DrawFormHeader "100%", STR_PAGE_TITLE, ""
	stlBeginStdTable "100%"
%>
<FORM ACTION="<%= strScript %>" METHOD="POST" NAME="frm">
<INPUT TYPE="hidden" NAME="action" VALUE="submit">
<%
	response.write "<TR><TD>"
	
	response.write "To export your order list, select the appropriate options below <BR><BR>"
'	"and then click on the ""Export Order List"" option when viewing your search results.<BR><BR>"
	
	response.write "<B>Select Export Options:</B>"
	stlBeginFormSection "", 2
	stlCheckbox "strIncludeBilling", "Y", strIncludeBilling, "", "Include Billing Address"
	stlCheckbox "strIncludeShipping", "Y", strIncludeShipping, "", "Include Shipping Address"
	stlEndFormSection
	
	response.write "<B>Select Export Format:</B>"
	stlBeginFormSection "", 2
	stlRadio "strFormat", "CSV", strFormat, "", "CSV-text document"
'	stlRadio "strFormat", "EXCEL", strFormat, "", "Microsoft Excel document"
	stlEndFormSection
	
	response.write "</TD></TR>"
	stlSubmit "Submit"
	
	response.write "</FORM>"
	stlEndStdTable
	
	SetFieldFocus "frm", "strIncludeBilling"
end sub
%>