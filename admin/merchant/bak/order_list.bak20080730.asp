<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<!--#include file="../config/incSearch.asp"-->
<%
server.scripttimeout = 900
' =====================================================================================
' = File: order_list.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Merchant Order Admin
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin
if not CheckUserAccess(ACC_ORDER_VIEW) then
	response.redirect "menu.asp"
end if

const STR_PAGE_TITLE = "Order Search"
const STR_PAGE_TYPE = "adminorderlist"

const STR_SEARCH_SCRIPT = "order_list.asp?"
const STR_DETAIL_SCRIPT = "order_detail.asp?"

OpenConn

dim strPageTitle
strPageTitle = STR_PAGE_TITLE

strSearchAuxText = "Export Order List"
strSearchAuxAction = "export"

dim strMode, strAction
strMode = lcase(Request("mode"))
if strMode <> "a" and strMode <> "i" then
	strMode = "b"
end if
strAction = lcase(Request("auxaction"))
if strAction = "" then
	strAction = lcase(Request("action"))
end if

if Instr(strAction,"export") > 0 then
	dim blnIncludeShipping, blnIncludeBilling, blnIncludeNotes, strFormat
	GetExportFlags blnIncludeBilling, blnIncludeShipping, blnIncludeNotes, strFormat
'	response.Write "<small>" & blnIncludeBilling & ", " & blnIncludeShipping & ", " & blnIncludeNotes & ", " & strFormat & "</small><br />" & vbcrlf
'	if strAction = "export" and blnIncludeNotes then strAction = "exportnotes"
end if

dim strStatusClause
strStatusClause = UserAccessStatus_BuildSQLWhere()
if strStatusClause = "" then
	response.redirect "menu.asp"
end if

strSearchSQLPrefix = "SELECT O.*,S.vchLastName AS S_vchLastName,S.vchFirstName AS S_vchFirstName,S.vchEmail AS S_vchEmail"
if Instr(strAction,"export") > 0 then
	' remove 0-9 In Progress, B Abandoned, P Pending, E Error
	' remove K Declined, S Submitted, C Credit, R ?, U ?, V ?
	' allow X Deposited, H Shipped
	' strStatusClause = replace(strStatusClause,",'X','C','H','R','U','V'","")
	strStatusClause = replace(strStatusClause,"'0','1','2','3','4','5','6','7','8','9',","")
	strStatusClause = replace(strStatusClause,",'R','U','V'","")
	strStatusClause = replace(strStatusClause,"'B',","")
	strStatusClause = replace(strStatusClause,"'C',","")
	strStatusClause = replace(strStatusClause,"'P',","")
	strStatusClause = replace(strStatusClause,"'E',","")
	strStatusClause = replace(strStatusClause,"'K',","")
	strStatusClause = replace(strStatusClause,"'S',","")

		strSearchSQLPrefix = strSearchSQLPrefix & ",P.vchLastName AS P_vchLastName"
		strSearchSQLPrefix = strSearchSQLPrefix & ",P.vchFirstName AS P_vchFirstName"
		strSearchSQLPrefix = strSearchSQLPrefix & ",P.vchEmail AS P_vchEmail"

	if blnIncludeBilling then
		strSearchSQLPrefix = strSearchSQLPrefix & ",P.vchAddress1 AS P_vchAddress1"
		strSearchSQLPrefix = strSearchSQLPrefix & ",P.vchAddress2 AS P_vchAddress2"
		strSearchSQLPrefix = strSearchSQLPrefix & ",P.vchCity AS P_vchCity"
		strSearchSQLPrefix = strSearchSQLPrefix & ",P.vchState AS P_vchState"
		strSearchSQLPrefix = strSearchSQLPrefix & ",P.vchZip AS P_vchZip"
		strSearchSQLPrefix = strSearchSQLPrefix & ",P.vchDayPhone AS P_vchDayPhone"
	end if

	if blnIncludeShipping then
		strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchLastName AS H_vchLastName"
		strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchFirstName AS H_vchFirstName"
		strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchEmail AS H_vchEmail"
		strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchAddress1 AS H_vchAddress1"
		strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchAddress2 AS H_vchAddress2"
		strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchCity AS H_vchCity"
		strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchState AS H_vchState"
		strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchZip AS H_vchZip"
		strSearchSQLPrefix = strSearchSQLPrefix & ",H.vchDayPhone AS H_vchDayPhone"
	end if
end if
	strSearchSQLPrefix = strSearchSQLPrefix & " FROM " & STR_TABLE_ORDER & " AS O"
	strSearchSQLPrefix = strSearchSQLPrefix & " LEFT OUTER JOIN " & STR_TABLE_SHOPPER & " AS S ON O.intShopperID=S.intID"

if Instr(strAction,"export") > 0 then
	strSearchSQLPrefix = strSearchSQLPrefix & " LEFT OUTER JOIN " & STR_TABLE_SHOPPER & " AS P ON O.intBillShopperID=P.intID"
  if blnIncludeShipping then
	strSearchSQLPrefix = strSearchSQLPrefix & " LEFT OUTER JOIN " & STR_TABLE_SHOPPER & " AS H ON O.intShipShopperID=H.intID"
  end if
end if
	strSearchSQLPrefix = strSearchSQLPrefix & " WHERE (O.intShopperID<>0)"
if strMode<>"a" or Instr(strAction,"export") > 0 then
	strSearchSQLPrefix = strSearchSQLPrefix & " AND O.chrStatus IN (" & strStatusClause & ") "
end if
if strAction = "exportnotes" then
	strSearchSQLPrefix = strSearchSQLPrefix & " AND O.dtmShipped IS NULL "
	strSearchSQLPrefix = strSearchSQLPrefix & " AND NOT O.txtGiftMessage IS NULL "
end if
	strSearchSQLSuffix = " ORDER BY O.intID"
'response.write strSearchSQLPrefix

Search_AddLabel "Order Information"
Search_AddNumberRange "#O.intID", "Order ID"
Search_AddDateRange "%O.dtmCreated", "Date Created"
Search_AddDateRange "%O.dtmUpdated", "Date Updated"
Search_AddDateRange "%O.dtmShipped", "Date Shipped"
if strMode = "a" then
	Search_AddText "O.vchCreatedByIP", 15, 15, "Created by IP"
	Search_AddText "O.vchUpdatedByIP", 15, 15, "Updated by IP"
	Search_AddText "O.vchCreatedByUser", 15, 32, "Created by User"
	Search_AddText "O.vchUpdatedByUser", 15, 32, "Updated by User"
	Search_AddCheckboxGroup "+O.chrType", 4, dctOrderTypeValues, "Type"
	if Instr(strAction,"export") = 0 then
	Search_AddCheckboxGroup "+O.chrStatus", 4, dctOrderStatusSearchValues, "Status"
	end if
end if
Search_AddText "O.vchShippingNumber", 32, 32, "Shipping/Conf Number"
Search_AddText "O.vchReferalName", 32, 32, "Referral"
if strMode = "a" then
	Search_AddText "O.vchShopperID", 15, 15, "Shopper IP"
	Search_AddText "O.vchShopperBrowser", 32, 64, "Shopper Browser"
	Search_AddCheckboxGroup "+O.chrPaymentMethod", 2, dctPaymentMethod, "Payment Method"
end if
Search_AddLabel "Shopper Information"
Search_AddText "S.vchLastName", 24, 32, "Last Name"
Search_AddText "S.vchFirstName", 24, 32, "First Name"
Search_AddLabel "Credit Card Information"
Search_AddText "O.vchPaymentCardType", 24, 32, "Card Type"
Search_AddText "O.vchPaymentCardName", 24, 32, "Name on Card"
Search_AddText "O.vchPaymentCardNumber", 24, 32, "Card Number"
if strMode = "a" or strMode = "i" then
	Search_AddDateRange "%O.vchPaymentCardExp", "Expiration Date"
end if
Search_AddLabel "Summary Information"
if strMode = "a" then
	Search_AddCheckboxGroup "+#O.intTaxZone", 2, GetTaxZoneList(), "Tax Zone"
	Search_AddNumber "#O.mnyNonTaxableSubtotal", "Non-Taxable Subtotal"
	Search_AddNumber "#O.mnyTaxableSubtotal", "Taxable Subtotal"
	Search_AddNumber "#O.fltTaxRate", "Tax Rate"
	Search_AddNumber "#O.mnyTaxAmount", "Tax Amount"
	Search_AddNumber "#O.mnyShipAmount", "Shipping Amount"
	Search_AddRadioGroup "+O.chrShipTaxFlag", 2, dctYesNoValues, "Shipping Tax Flag"
end if
Search_AddNumber "#O.mnyGrandTotal", "Grand Total"
	
InitSearchEngine

dim strSQL, rsData
if strAction = "search" or strAction = "export" or strAction = "exportnotes" then
	if strMode = "b" then
		strSQL = BuildBasicSearchSQL()
	else
		strSQL = BuildSearchSQL()
	end if
	strSQL = replace(replace(strSQL,"(((","("),")))",")")

	if Instr(strAction,"export") > 0 then
		' remove invalid status check
		' allow X Deposited, H Shipped
		' strSQL = replace(strSQL,",'X','C','H','R','U','V'","")
		strSQL = replace(strSQL,",'R','U','V'","")
		strSQL = replace(strSQL,"'C',","")
	end if
	if strSQL <> "" then
'		response.write "Action: " & strAction & "<br />" & vbcrlf
'		response.write "<small>" & strSQL & "<br /></small>" & vbcrlf
'       response.end

		set rsData = gobjConn.execute(strSQL)
	else
		strAction = ""
	end if
end if

intCurrentGroup = intNumGroups + 1
if intCurrentGroup > INT_MAX_SEARCH_GROUPS then
	intCurrentGroup = 0
end if

if strAction = "exportnotes" then
	if rsData.eof then
		
		DrawSearchResults rsData
		
	else
		DrawExportNotes strFormat
	end if
elseif strAction = "export" then
	if rsData.eof then
		
		DrawSearchResults rsData
		
	else
		DrawExport strFormat
	end if
else
	
	if strAction = "search" then
		DrawSearchResults rsData
	else
		DrawSearchPage "Orders"
	end if
	
end if

if IsObject(rsData) then
	rsData.close
	set rsData = nothing
end if

response.end

sub LoadPresetSearch(strPreset)
	strMode = "i"
	select case strPreset
		case "open":
			arySearchGroups(1,0).Add "+O.chrStatus", "n~S+A+Z+E+V"
		case "48hours":
			arySearchGroups(1,0).Add "O.dtmUpdated", "n~" & DateAdd("h", -48, Now()) & "~"
		case "72hours":
			arySearchGroups(1,0).Add "O.dtmUpdated", "n~" & DateAdd("h", -72, Now()) & "~"
		case "thisweek":
			arySearchGroups(1,0).Add "O.dtmUpdated", "n~" & DateAdd("d", 1-Weekday(Date()), Date()) & "~"
		case "thismonth":
			arySearchGroups(1,0).Add "O.dtmUpdated", "n~" & Month(Date()) & "/1/" & Year(Date()) & "~"
		case "thisyear":
			arySearchGroups(1,0).Add "O.dtmUpdated", "n~1/1/" & Year(Date()) & "~"
		case "all":
			arySearchGroups(1,0).Add "O.chrStatus", "~D"
		case else:	' invalid preset
			response.redirect "default.asp"
	end select
end sub

sub DrawSearchResults(rsInput)
	'response.Write "eof = " & iif(rsInput.eof,"True","False")
	strPageTitle = "Search Results - Orders"
	DrawSearchListTitle strPageTitle
	DrawListHeader
	if rsInput.eof then
%>
<TR>
	<TD COLSPAN="5" ALIGN="center">No orders were found matching your search.</TD>
</TR>
<%
	else
		while not rsInput.eof
			DrawListItem rsInput
			rsInput.MoveNext
			if not rsInput.eof then
				DrawListItemSep
			end if
		wend
	end if
	DrawListFooter
end sub

sub DrawListHeader
%>
<!-- <TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVLtGrey) %>">
<TR>
	<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cWhite %>">
	<TR>
		<TD> --><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%">
		<TR BGCOLOR="<%= cVLtYellow %>">
			<TD>&nbsp;</TD>
			<TD NOWRAP><B>Order #</B></TD>
			<TD WIDTH="33%"><B>Shopper&nbsp;</B></TD>
			<TD NOWRAP><B>&nbsp;Submitted&nbsp;</B></TD>
			<TD NOWRAP><B>&nbsp;Shipped&nbsp;</B></TD>
			<TD NOWRAP ALIGN="right"><B>&nbsp;Amount&nbsp;</B></TD>
			<TD NOWRAP ALIGN="right"><B>Status</B></TD>
		</TR>
<%
end sub

sub DrawListItem(rsInput)
	dim intID, strName, mnyPrice, strStatus, dtmSubmitted, dtmShipped, chrViewed
	intID = rsInput("intID")
	strName = rsInput("S_vchLastName") & ", " & rsInput("S_vchFirstName")
	mnyPrice = rsInput("mnyGrandTotal")
	strStatus = rsInput("chrStatus")
	dtmSubmitted = rsInput("dtmSubmitted")
	dtmShipped = rsInput("dtmShipped")
'	chrViewed = rsInput("chrViewed")
	dim strColor
	if strStatus = "E" then
		strColor = "BGCOLOR=""#FFCCCC"""
	else
		strColor = ""
	end if
	'response.write "name: " & rsInput("S_vchLastName")
	'response.write "name: |" & rsInput("vchPaymentCardName") & "|"

	'THIS CODE IS HACKED. Since people can now check out without
	'creating an account I had to grab peoples name for the list from other places
	if len(strName) > 2  then
		'response.write "|" & len(strName) & "|"
	else
		'newer code
		if rsInput("intBillShopperID") <> "" Then
			strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE chrStatus<>'D' AND chrType='B' AND intID=" & rsInput("intBillShopperID") & " AND intShopperID=" & rsInput("intShopperID")
			'strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE  chrStatus<>'D' AND chrType='B' AND intShopperID=" & rsInput("intShopperID")
'			response.write strSQL & "<br />"

 			dim loopcrazyRS
			set loopcrazyRS = gobjConn.execute(strSQL)
			if not LoopCrazyRS.EOF then
				strName = loopcrazyRS("vchLastName") & ", " & loopcrazyRS("vchFirstName")
			end if 
			loopcrazyRS.close
			set loopcrazyRS = nothing
		end if
	end if
%>
		<TR VALIGN=BOTTOM <%= strColor %>>
			<TD ALIGN=CENTER><A HREF="<%= STR_DETAIL_SCRIPT %>id=<%= intID %>"><IMG SRC="<%= imagebase %>icon_order.gif" WIDTH="16" HEIGHT="16" ALT="View Order Details" BORDER="0"></A></TD>
			<TD NOWRAP><%= intID %></TD>
			<TD WIDTH="33%"><A HREF="<%= STR_DETAIL_SCRIPT %>id=<%= intID %>"><%= strName %></A></TD>
			<TD NOWRAP>&nbsp;<%= FormatDateTimeNoSeconds(dtmSubmitted) %>&nbsp;</TD>
			<TD NOWRAP>&nbsp;<%= FormatDateTimeNoSeconds(dtmShipped) %>&nbsp;</TD>
			<TD NOWRAP ALIGN="right">&nbsp;<%= SafeFormatCurrency(fontx(1,1,cLight) & "n/a</FONT>", mnyPrice, 2) %>&nbsp;</TD>
			<TD NOWRAP ALIGN="right"><%= GetArrayValue(strStatus, dctOrderStatus) %></TD>
		</TR>
<%
end sub

sub DrawListItemSep
%>
		<TR>
			<TD COLSPAN=7><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVVLtGrey) %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
		</TR>
<%
end sub

sub DrawListFooter
%>
		</TABLE><!-- </TD>
	</TR>
	</TABLE></TD>
</TR>
</TABLE> -->
<%
end sub

sub DrawExport(strFormat)
	dim strFieldSep, strFieldQuote, strFieldAltQuote, strFileName, strData, strEOL, strGiftMessage
	if strFormat = "CSV" then
		strFileName = "orderexport.csv"
		strFieldSep = ","
		strFieldQuote = """"
		strFieldAltQuote = "'"
		strEOL = vbCrLf
	else
		strFileName = "orderexport.xls"
		strFieldSep = Chr(9)
		strFieldQuote = ""
		strFieldAltQuote = ""
		strEOL = vbCrLf
	end If
	response.ContentType = "text/csv"
	response.AddHeader "Content-transfer-encoding", "binary"
	response.AddHeader "Content-Disposition", "attachment; filename=" & strFileName

	if strFieldQuote <> "" then
		while not rsData.eof
			strGiftMessage = replace(replace(rsData("txtGiftMessage")&"", vbCrLf, " "), vbcr, " ")

			response.write rsData("intID") & ","
'			response.write strFieldQuote & replace(rsData("intID") & "", strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("dtmCreated") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("dtmSubmitted") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("dtmUpdated") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
'			response.write strFieldQuote & replace(replace(rsData("vchEmail") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","

		if (rsData("intShopperID") = -1) and (rsData("intBillShopperID") > 0) then	' and (not blnIncludeBilling)
'			response.write rsData("intBillShopperID") & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchLastName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchFirstName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchEmail") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
		else
'			response.write rsData("intShopperID") & ","
			response.write strFieldQuote & replace(replace(rsData("S_vchLastName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("S_vchFirstName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("S_vchEmail") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
		end if

			response.write strFieldQuote & replace(replace(rsData("vchShippingNumber") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(GetArrayValue(rsData("chrStatus"), dctOrderStatus) & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","

		if blnIncludeBilling then
'			response.write rsData("intBillShopperID") & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchLastName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchFirstName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchAddress1") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchAddress2") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchCity") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchState") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchZip") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchDayPhone") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchEmail") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
		end if
		if blnIncludeShipping then
'			response.write rsData("intShipShopperID") & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchLastName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchFirstName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchAddress1") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchAddress2") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchCity") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchState") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchZip") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchDayPhone") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchEmail") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
		end if

			'/// get line items
			Dim mySQLString,myItemDataSet,myItemCounter,myPaddingCounter
			
			myItemCounter = 0
			mySQLString = "SELECT TOP 5 intQuantity, vchPartNumber FROM TCS_LineItem WHERE chrStatus='A' AND intOrderID=" & rsData("intID") & ""
			set myItemDataSet = gobjConn.Execute(mySQLString)
			
			while not myItemDataSet.eof
				response.write myItemDataSet("intQuantity") & ","
				response.write strFieldQuote & replace(replace(myItemDataSet("vchPartNumber") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
				myItemCounter = myItemCounter + 1
				myItemDataSet.MoveNext()
			wend
			
			for myPaddingCounter = myItemCounter to 4
				response.Write ","&","
			next

			response.write (rsData("mnyNonTaxSubTotal")+rsData("mnyTaxSubTotal")) & ","
			response.write rsData("mnyTaxAmount") & ","
			response.write rsData("mnyShipAmount") & ","
			response.write rsData("mnyGrandTotal") & ","

			response.write strFieldQuote & replace(replace(strGiftMessage & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote
			response.write strEOL
			rsData.MoveNext
		wend
	else
		while not rsData.eof
			strGiftMessage = rsData("txtGiftMessage")

'			response.write rsData("intID") & ","
			response.write replace(rsData("S_vchLastName") & "", ",", "") & ","
			response.write replace(rsData("S_vchFirstName") & "", ",", "") & ","
			response.write replace(rsData("S_vchEmail") & "", ",", "")
		if blnIncludeBilling then
'			response.write rsData("intBillShopperID") & ","
			response.write replace(rsData("P_vchLastName") & "", ",", "") & ","
			response.write replace(rsData("P_vchFirstName") & "", ",", "") & ","
			response.write replace(rsData("P_vchAddress1") & "", ",", "") & ","
			response.write replace(rsData("P_vchAddress2") & "", ",", "") & ","
			response.write replace(rsData("P_vchCity") & "", ",", "") & ","
			response.write replace(rsData("P_vchState") & "", ",", "") & ","
			response.write replace(rsData("P_vchZip") & "", ",", "") & ","
			response.write replace(rsData("P_vchDayPhone") & "", ",", "") & ","
			response.write replace(rsData("P_vchEmail") & "", ",", "") & ","
		end if
		if blnIncludeShipping then
'			response.write rsData("intShipShopperID") & ","
			response.write replace(rsData("H_vchLastName") & "", ",", "") & ","
			response.write replace(rsData("H_vchFirstName") & "", ",", "") & ","
			response.write replace(rsData("H_vchAddress1") & "", ",", "") & ","
			response.write replace(rsData("H_vchAddress2") & "", ",", "") & ","
			response.write replace(rsData("H_vchCity") & "", ",", "") & ","
			response.write replace(rsData("H_vchState") & "", ",", "") & ","
			response.write replace(rsData("H_vchZip") & "", ",", "") & ","
			response.write replace(rsData("H_vchDayPhone") & "", ",", "") & ","
			response.write replace(rsData("H_vchEmail") & "", ",", "") & ","
		end if
			response.write strEOL
			rsData.MoveNext
		wend
	end if
end sub

sub DrawExportNotes(strFormat)
	dim strFieldSep, strFieldQuote, strFieldAltQuote, strFileName, strData, strEOL, strGiftMessage
	response.ContentType = "text/csv"
	response.AddHeader "Content-transfer-encoding", "binary"
	if strFormat = "CSV" then
		strFileName = "ordernotesexport.csv"
		strFieldSep = ","
		strFieldQuote = """"
		strFieldAltQuote = "'"
		strEOL = vbCrLf
	else
		strFileName = "ordernotesexport.xls"
		strFieldSep = Chr(9)
		strFieldQuote = ""
		strFieldAltQuote = ""
		strEOL = vbCrLf
	end if
	response.AddHeader "Content-Disposition", "filename=" & strFileName

	while not rsData.eof
		strGiftMessage = replace(replace(rsData("txtGiftMessage")&"", vbCrLf, " "), vbcr, " ")

		response.write rsData("intID") & ","
		response.write strFieldQuote & replace(replace(rsData("dtmCreated") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","

'			response.write rsData("intShopperID") & ","
'			response.write strFieldQuote & replace(replace(rsData("S_vchLastName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
'			response.write strFieldQuote & replace(replace(rsData("S_vchFirstName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
'			response.write strFieldQuote & replace(replace(rsData("S_vchEmail") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","

'		response.write strFieldQuote & replace(replace(rsData("vchShippingNumber") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
'		response.write strFieldQuote & replace(replace(GetArrayValue(rsData("chrStatus"), dctOrderStatus) & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","

		if blnIncludeBilling then
'			response.write rsData("intBillShopperID") & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchLastName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchFirstName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchAddress1") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchAddress2") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchCity") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchState") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchZip") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchDayPhone") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchEmail") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
		end if
		if blnIncludeShipping then
'			response.write rsData("intShipShopperID") & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchLastName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchFirstName") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchAddress1") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchAddress2") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchCity") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchState") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchZip") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchDayPhone") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("H_vchEmail") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
		end if

		'/// get line items
		Dim mySQLString,myItemDataSet,myItemCounter,myPaddingCounter
			
		myItemCounter = 0
		mySQLString = "SELECT TOP 3 intQuantity, vchPartNumber FROM TCS_LineItem WHERE chrStatus='A' AND intOrderID=" & rsData("intID") & ""
		set myItemDataSet = gobjConn.Execute(mySQLString)
			
		while not myItemDataSet.eof
			response.write myItemDataSet("intQuantity") & ","
			response.write strFieldQuote & replace(replace(myItemDataSet("vchPartNumber") & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			myItemCounter = myItemCounter + 1
			myItemDataSet.MoveNext()
		wend
			
		for myPaddingCounter = myItemCounter to 2
			response.Write ","&","
		next

		response.write strFieldQuote & replace(replace(strGiftMessage & "", ",", ""), strFieldQuote, strFieldAltQuote) & strFieldQuote
		response.write strEOL
		rsData.MoveNext
	wend
end sub

%>