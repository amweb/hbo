<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: inv_clone.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Clone an inventory item, set status to Pending.
' = Revision History:
' =   08mar2002 mred: page created
' =====================================================================================

' script arguments:
'   action = "clone"
'     parent = parent folder ID
'     source = item ID to clone

CheckAdminLogin

if not CheckUserAccess(ACC_INVENTORY_EDIT) then
	response.redirect "main.asp"
end if

const STR_LIST_SCRIPT = "inv_list.asp?"

OpenConn

dim intSource
intSource = Request("source")
if IsNumeric(intSource) then
	intSource = CLng(intSource)
else
	intSource = 0
end if

CloneItem intSource
response.redirect STR_LIST_SCRIPT & "parentid=" & Request("parent")
response.end

sub CloneItem(intID)
	dim strSQL, rsData, intNewSort
	strSQL = "SELECT * FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intSource
	set rsData = gobjConn.execute(strSQL)
			dim dctSaveList
			set dctSaveList = Server.CreateObject("Scripting.Dictionary")
				dctSaveList.Add "@dtmCreated", "GETDATE()"
				dctSaveList.Add "@dtmUpdated", "GETDATE()"
				dctSaveList.Add "vchCreatedByUser", Session(SESSION_MERCHANT_ULOGIN)
				dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
				dctSaveList.Add "vchCreatedByIP", gstrUserIP
				dctSaveList.Add "vchUpdatedByIP", gstrUserIP
				dctSaveList.Add "chrType", rsData("chrType") & ""
				dctSaveList.Add "chrStatus", "P"
				dctSaveList.Add "#intParentID", rsData("intParentID") & ""
				dctSaveList.Add "#intSortOrder", rsData("intSortOrder") + 1
				dctSaveList.Add "#intHitCount", 0
				dctSaveList.Add "vchItemName", rsData("vchItemName") & ""
				dctSaveList.Add "vchPartNumber", rsData("vchPartNumber") & ""
				dctSaveList.Add "#mnyItemPrice", rsData("mnyItemPrice") & ""
				dctSaveList.Add "#mnyWholesale", rsData("mnyWholesale") & ""
				dctSaveList.Add "#mnyShipPrice", rsData("mnyShipPrice") & ""
				dctSaveList.Add "#fltShipWeight", rsData("fltShipWeight") & ""
				dctSaveList.Add "$chrTaxFlag", rsData("chrTaxFlag") & ""
				dctSaveList.Add "$chrICFlag", rsData("chrICFlag") & ""
				dctSaveList.Add "#intStock", rsData("intStock") & ""
				dctSaveList.Add "#intLowStock", rsData("intLowStock") & ""
				dctSaveList.Add "#intHighStock", rsData("intHighStock") & ""
				dctSaveList.Add "#intMinQty", rsData("intMinQty") & ""
				dctSaveList.Add "$chrSoftFlag", rsData("chrSoftFlag") & ""
				dctSaveList.Add "vchSoftURL", rsData("vchSoftURL") & ""
				dctSaveList.Add "vchImageURL", rsData("vchImageURL") & ""
				dctSaveList.Add "vchImageURL2", rsData("vchImageURL2") & ""
				dctSaveList.Add "#intSmImageHeight", rsData("intSmImageHeight") & ""
				dctSaveList.Add "#intSmImageWidth", rsData("intSmImageWidth") & ""
				dctSaveList.Add "#intLgImageHeight", rsData("intLgImageHeight") & ""
				dctSaveList.Add "#intLgImageWidth", rsData("intLgImageWidth") & ""
				dctSaveList.Add "#intBrand", rsData("intBrand") & ""
				dctSaveList.Add "#intForceShipMethod", rsData("intForceShipMethod") & ""
				dctSaveList.Add "chrForceSoloItem", rsData("chrForceSoloItem") & ""
				dctSaveList.Add "chrSpecial", rsData("chrSpecial") & ""
				dctSaveList.Add "vchOptionList1", rsData("vchOptionList1") & ""
				dctSaveList.Add "vchOptionList2", rsData("vchOptionList2") & ""
'				dctSaveList.Add "chrParentStatus", rsData("chrParentStatus") & ""
				dctSaveList.Add "txtDescription", rsData("txtDescription") & ""
	rsData.close
	set rsData = nothing
	intID = SaveDataRecord(STR_TABLE_INVENTORY, Request, 0, dctSaveList)
end sub

%>