<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: report_tax.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2000 SacWeb, Inc. All rights reserved.
' = Description:
' =   Generate report by tax zone
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_REPORT_VIEW) then
	response.redirect "menu.asp"
end if

dim strPageType, strPageTitle

strPageType = "adminrepsummary"
strPageTitle = "Order Report By Product"

OpenConn

dim strSQL, rsData, dtmStart, dtmEnd
GetReportDateRange dtmStart, dtmEnd

strSQL = "SELECT"
strSQL = strSQL & " I.vchItemName,"
strSQL = strSQL & " I.vchPartNumber,"
strSQL = strSQL & " COUNT(DISTINCT O.intID) AS intOrderCount,"
strSQL = strSQL & " SUM(L.intQuantity) AS intQuantityCount,"
strSQL = strSQL & " SUM(L.intQuantity * L.mnyUnitPrice) AS mnyTotalAmount"
strSQL = strSQL & " FROM " & STR_TABLE_ORDER & " AS O,"
strSQL = strSQL & " " & STR_TABLE_LINEITEM & " AS L,"
strSQL = strSQL & " " & STR_TABLE_INVENTORY & " AS I"
strSQL = strSQL & " WHERE O.chrStatus IN ('H','X')"	' only show shipped or deposited orders
strSQL = strSQL & " AND O.dtmCreated >= '" & dtmStart & "' AND O.dtmCreated <= '" & dtmEnd & "'"
strSQL = strSQL & " AND L.chrStatus <> 'D'"
strSQL = strSQL & " AND L.mnyUnitPrice > 0"
strSQL = strSQL & " AND L.intOrderID = O.intID"
strSQL = strSQL & " AND L.intInvID = I.intID"
strSQL = strSQL & " GROUP BY I.vchItemName, I.vchPartNumber"
'response.write strSQL
set rsData = gobjConn.execute(strSQL)


DrawPage


rsData.close
set rsData = nothing
response.end

sub DrawPage
	call DrawHeaderUpdate()
%>
<center>
<%= font(2) %><b><%= STR_SITE_NAME %><br />
<%= strPageTitle %><br />
<%= DateValue(dtmStart) %> to <%= DateValue(dtmEnd) %></b></font>
<%= font(1) %><br />
<br />
<table border="0" cellpadding="2" cellspacing="2">
<tr>
	<td><b>Item</b></td>
	<TD align="center"><b>Part Number</b></td>
	<TD align="center"><b># Items</b></td>
	<TD align="center"><b># Orders</b></td>
	<TD align="center"><b>Total Amount</b></td>
</tr>
<%
	while not rsData.eof
%>
<tr>
	<td><%= rsData("vchItemName") %></td>
	<td align="center"><%= rsData("vchPartNumber") %></td>
	<td align="center"><%= rsData("intOrderCount") %></td>
	<td align="center"><%= rsData("intQuantityCount") %></td>
	<td align="center"><%= SafeFormatCurrency("$0.00", rsData("mnyTotalAmount"), 2) %></td>
</tr>
<%
		rsData.MoveNext
	wend
%>
</table>
</font>
</center>
<%
end sub
%>