<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: brand_order_ship.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Order shipment screen -- for orders without online card processing
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_ORDER_SHIP) then
	response.redirect "menu.asp"
end if

const STR_PAGE_TITLE = "Ship Order"
const STR_PAGE_TYPE = "adminordership"

OpenConn

dim intID, strAction
strAction = Request("action")
intID = Request("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if

if intID <= 0 then
	response.redirect "brand_order_list.asp"
end if

dim STR_SHIP_SCRIPT, STR_DETAIL_SCRIPT, STR_SEARCH_SCRIPT
STR_SHIP_SCRIPT = "brand_order_ship.asp?id=" & intID
STR_DETAIL_SCRIPT = "brand_order_detail.asp?id=" & intID
STR_SEARCH_SCRIPT = "brand_order_list.asp"

dim strBrand, strBrandClause, intBrand
if Session(SESSION_MERCHANT_BID) <> "" then
	strBrand = "brand_"
	intBrand = Session(SESSION_MERCHANT_BID)
	strBrandClause = strBrandClause & " LEFT JOIN " & STR_TABLE_LINEITEM & " AS I ON O.intID = I.intOrderID AND I.chrStatus<>'D'"
	strBrandClause = strBrandClause & " RIGHT JOIN " & STR_TABLE_INVENTORY & " AS B ON I.intInvID = B.intID AND B.intBrand=" & intBrand & " AND (B.chrType='I' OR B.chrType IS NULL)"
end if

dim strStatusClause
strStatusClause = UserAccessStatus_BuildSQLWhereX("SAZE")
if strStatusClause = "" then
	response.redirect STR_SEARCH_SCRIPT
end if

select case strAction
	case "submit":
		AutoCheck Request, ""
		if FormErrors.count > 0 then
			
			DrawPage ""
			
		else
			UpdateBrandItemShippingNumber_Other intID, Request("vchShippingNumber")
'			UpdateOrderShippingNumber_Other intID, Request("vchShippingNumber")
'			UpdateOrderStatus_Other intID, "H"
			SendBrandShipEmail_Other intID
			response.redirect STR_DETAIL_SCRIPT
		end if
	case else:
		
		DrawPage ""
		
end select
response.end

sub DrawPage(strError)
	dim s
	s = "&nbsp;<a href=""" & STR_DETAIL_SCRIPT & """><b>Back</b></a>"
	DrawFormHeader "100%", STR_PAGE_TITLE, s
	stlBeginStdTable "100%"
%>
<FORM ACTION="<%= STR_SHIP_SCRIPT %>" METHOD="POST">
<%
	HiddenInput "action", "submit"

	stlBeginFormSection "100%", 2
	stlCaption "Please enter the shipping/conf number for this order:", 2
	stlTextInput "vchShippingNumber", 32, 32, Request, "Shipping/Conf Number", "IsEmpty", "Missing shipping/conf number"
	stlEndFormSection

	stlSubmit "Submit"
%>
</FORM>
<%
	stlEndStdTable
end sub

sub UpdateBrandItemShippingNumber_Other(intOrderID, strShippingNumber)
	if intOrderID > 0 then
	 	dim strSQL
		strSQL = "UPDATE " & STR_TABLE_LINEITEM & " SET dtmUpdated = GETDATE(), vchUpdatedByUser = '" & Session(SESSION_MERCHANT_ULOGIN) & "', vchUpdatedByIP = '" & gstrUserIP & "', vchShippingNumber = '" & strShippingNumber & "'"
		strSQL =  strSQL & " WHERE intID IN (" 
		strSQL =  strSQL & "SELECT I.intID FROM " & STR_TABLE_LINEITEM & " AS I, " & STR_TABLE_INVENTORY & " AS B "
		strSQL =  strSQL & " WHERE (I.intOrderID=" & intOrderID & ") AND (I.chrStatus IN (" & strStatusClause & "))"
		strSQL =  strSQL & "  AND (B.intID=I.intInvID) AND (B.intBrand=" & intBrand & ") AND (B.chrType='I' OR B.chrType IS NULL)"
		strSQL =  strSQL & "  AND (IsNull(vchShippingNumber,'') = '')"
		strSQL =  strSQL & ")"
'		response.write "<br />" & strSQL & "<br />"
		gobjConn.execute(strSQL)
	end if
end sub

' The Merchant must update the Order shipping number and Order status 
' so UpdateBrandShippingNumber_Other() and UpdateBrandStatus_Other()
' should not be called...

sub UpdateBrandShippingNumber_Other(intOrderID, strShippingNumber)
	if intOrderID > 0 then
		dim dctSaveList
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "@dtmUpdated", "GETDATE()"
			dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
			dctSaveList.Add "vchUpdatedByIP", gstrUserIP
			dctSaveList.Add "vchShippingNumber", strShippingNumber
		if strShippingNumber <> "" then
			dctSaveList.Add "chrStatus", "H"
		end if
		SaveDataRecord STR_TABLE_ORDER, Request, intOrderID, dctSaveList
	end if
end sub

sub UpdateBrandStatus_Other(intOrderID, strStatus)
	if intOrderID > 0 then
		dim dctSaveList
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "@dtmUpdated", "GETDATE()"
			dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
			dctSaveList.Add "vchUpdatedByIP", gstrUserIP
			dctSaveList.Add "chrStatus", strStatus
		SaveDataRecord STR_TABLE_ORDER, Request, intOrderID, dctSaveList
	end if
end sub

sub SendBrandShipEmail_Other(intOrderID)
	if intOrderID > 0 then
		dim strMerchantMessage, strSubject
		dim strSQL, rsData
		strSQL = "SELECT O.intID AS O_intID, S.vchFirstName + ' ' + S.vchLastName AS S_vchShopperAccount, I.* "
		strSQL = strSQL & " FROM " & STR_TABLE_ORDER & " AS O"
		strSQL = strSQL & " LEFT JOIN " & STR_TABLE_SHOPPER & " AS S ON (S.intID = O.intShipShopperID) AND (S.chrStatus<>'D')"
		strSQL = strSQL & " LEFT JOIN " & STR_TABLE_LINEITEM & " AS I ON (I.intOrderID = O.intID) AND I.chrStatus IN (" & strStatusClause & ")"
		strSQL = strSQL & " RIGHT JOIN " & STR_TABLE_INVENTORY & " AS B ON (B.intID = I.intInvID) AND (B.intBrand=" & intBrand & ") AND (B.chrType='I' OR B.chrType IS NULL)"
		strSQL = strSQL & " WHERE O.intID=" & intOrderID & " AND( O.chrStatus<>'D') AND (S.chrType='B' OR S.chrType IS NULL)"
'		response.write strSQL & "<br />"
		set rsData = gobjConn.execute(strSQL)
		
		if not rsData.eof then
			strMerchantMessage = "Shipped Line Item(s) on an order. Please review the order by visiting your merchant website." & vbcrlf
			strMerchantMessage = strMerchantMessage & "Confirm that all Partners have shipped their line items before changing the Order status to shipped." & vbcrlf
			strMerchantMessage = strMerchantMessage & vbcrlf
			strMerchantMessage = strMerchantMessage & "Shipped Item(s) - Order # " & STR_MERCHANT_TRACKING_PREFIX & rsData("O_intID") & vbcrlf
			strMerchantMessage = strMerchantMessage & " Order Date: " & FormatDateTimeNoSeconds(rsData("dtmCreated")) & vbcrlf
			strMerchantMessage = strMerchantMessage & "  Ship Date: " & FormatDateTimeNoSeconds(rsData("dtmUpdated")) & vbcrlf
			strMerchantMessage = strMerchantMessage & " Shipped To: " & rsData("S_vchShopperAccount") & vbcrlf
			strMerchantMessage = strMerchantMessage & " Shipped By: " & rsData("vchUpdatedByUser") & vbcrlf
			if len(rsData("vchShippingNumber") & "") > 0 then
				strMerchantMessage = strMerchantMessage & " Shipping/Conf Number: " & rsData("vchShippingNumber") & vbcrlf
			end if
			strMerchantMessage = strMerchantMessage & vbcrlf
			gintOrderID = intOrderID
		end if
		rsData.close
		set rsData = nothing

		if strMerchantMessage <> "" then
			strSubject = "Shipped Item(s) - Order # " & STR_MERCHANT_TRACKING_PREFIX & gintOrderID
			SendMerchantEmail strSubject, strMerchantMessage
		end if
	end if
end sub
%>