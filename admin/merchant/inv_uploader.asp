<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: inv_idetail.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Inventory Admin - details for items
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_INVENTORY_VIEW) then
	response.write "Insufficient Permissions"
end if

dim blnCanEdit
blnCanEdit = CheckUserAccess(ACC_INVENTORY_EDIT)

const STR_PAGE_TITLE = "Item Image Uploader"
const STR_PAGE_TYPE = "admininvlist"

OpenConn

dim dctBrands, dctVideos, dctPrograms,dctCampaigns,dctCategories
dim strSQL, rsRequestData, objUpload
Set objUpload = Server.CreateObject("SoftArtisans.FileUp")

Set rsRequestData = objUpload.Form

dim intID, intParentID, strAction
strAction = RequestPoll("action")
intID = RequestPoll("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if

intParentID = RequestPoll("parentid")
if IsNumeric(intParentID) then
	intParentID = CLng(intParentID)
else
	intParentID = 0
end if

if strAction = "submit" then
	if blnCanEdit then
		AutoCheck rsRequestData, ""
		if FormErrors.count = 0 then
			dim fImage, uploadPath, IsImageGood, fImageOrient
			IsImageGood = false
			fImageOrient = 0
			If IsObject(objUpload.Form("ImageFile")) then
				if (objUpload.Form("ImageFile").TotalBytes <> 0) and (lcase(right(objUpload.Form("ImageFile").UserFilename, 4))=".jpg" or lcase(right(objUpload.Form("ImageFile").UserFilename, 4))=".gif") then
					fImage = "Photo" & intID & "." & Mid(objUpload.form("ImageFile").UserFilename, InstrRev(objUpload.form("ImageFile").UserFilename, "\") + 1)
					uploadPath = Server.MapPath(liveimagebase) & "\"
					objUpload.Form("ImageFile").SaveAs uploadPath & fImage
					if lcase(right(objUpload.Form("ImageFile").UserFilename, 4))=".gif" then 
						IsImageGood = true	' - GIF Files don't work with JDO
					else
						IsImageGood = SetImageReSize( uploadPath, fImage, fImageOrient)	' true
					end if
				end if
			end if
			if IsImageGood then
				dim dctSaveList
				set dctSaveList = Server.CreateObject("Scripting.Dictionary")
				dctSaveList.Add "*@dtmUpdated", "GETDATE()"
				dctSaveList.Add "vchImageURL", fImage
				SaveDataRecord STR_TABLE_INVENTORY, rsRequestData, intID, dctSaveList
			end if
			
			on error goto 0	

			' send admin email
			dim strMerchantMessage, strSubject
			strMerchantMessage = "An image has been uploaded: " & intID & vbcrlf
			strMerchantMessage = strMerchantMessage & vbcrlf
			strSubject = "Image Uploade Confirmation #" & intID & ""
			SendMerchantEmail strSubject, strMerchantMessage
			
			Response.Write "Upload Complete."
			
		else
			
			DrawPage rsRequestData
			
		end if
	else
		Response.Write "Item Not Available"
	end if
else
	dim rsData, blnDataIsRS,rsPromoCodes
	if intID > 0 then
		strSQL = "SELECT * FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intID & " AND chrStatus<>'D'"
		set rsData = gobjConn.execute(strSQL)
		blnDataIsRS = true
		if rsData.eof then
			response.write "Item Not Available"
		end if
	else
		set rsData = rsRequestData
		blnDataIsRS = false
	end if
	
	DrawPage rsData
	
	if blnDataIsRS then
		rsData.close
		set rsData = nothing
	end if
end if
response.end

sub DrawPage(rsInput)
	dim strTitle, strInfoText
	dim txtDescription, vchSpecifications, txtShortDescription
	txtDescription = rsInput("txtDescription") & ""
	intParentID = rsInput("intParentID")
	strTitle = "Upload Image"
%>
<FORM ACTION="inv_uploader.asp" METHOD="POST" NAME="frm" ENCTYPE="multipart/form-data">
<INPUT TYPE="hidden" NAME="action" VALUE="submit">
<INPUT TYPE="hidden" NAME="id" VALUE="<%= intID %>">
<INPUT TYPE="hidden" NAME="parentid" VALUE="<%= intParentID %>">
<%
	strInfoText = ""
	
	DrawFormHeader "100%", strTitle, strInfoText
	stlBeginStdTable "100%"
	response.write "<TR><TD>"
	
	if blnCanEdit then
		stlBeginFormSection "", 3
		if not IsNull(rsInput("vchImageURL")) then
			stlRawCell "Current Image:<BR>(click to view full size)"
			stlRawCell "<A HREF=""" & imagebase & "products/big/" & rsInput("vchImageURL") & """ TARGET=""_blank""><IMG SRC=""" & imagebase & "products/big/" & rsInput("vchImageURL") & """ WIDTH=""50"" HEIGHT=""50"" ALT="""" BORDER=""0""></A>"
			if not IsNull(rsInput("vchImageURL2")) then
			stlRawCell "<A HREF=""" & imagebase & "products/big/" & rsInput("vchImageURL2") & """ TARGET=""_blank""><IMG SRC=""" & imagebase & "products/big/" & rsInput("vchImageURL2") & """ WIDTH=""50"" HEIGHT=""50"" ALT="""" BORDER=""0""></A>"
			else
			stlRawCell "&nbsp;&nbsp;"
			end if
		end if
		stlEndFormSection

		stlBeginFormSection "", 4
		stlTextInput "vchImageURL", 30, 255, rsInput, "\Image", "", ""
		stlFileInput "ImageFile", 30, 255, "", "or \Upload Image", "", ""
		'stlTextInput "vchImageURL2", 30, 255, rsInput, "\Image2", "", ""
		'stlFileInput "ImageFile2", 30, 255, "", "or \Upload Image2", "", ""
		stlEndFormSection
	end if
	stlSubmit iif(blnCanEdit, "Submit", "Return")
	
	stlEndStdTable
	response.write "</FORM>"
	
end sub

function RequestPoll (strField)
	Dim strValue
	strValue = rsRequestData(strField)
	If strValue = "" Then
		strValue = Request.QueryString(strField)
	end if
	RequestPoll = strValue
end function

function SetImageReSize( uploadPath, fImage, fImageOrient)

'dim fImage, uploadPath, IsImageGood
'IsImageGood = false
'fImage = "Photo" & intID & "." & Mid(objUpload.form("ImageFile").UserFilename, InstrRev(objUpload.form("ImageFile").UserFilename, "\") + 1)
'uploadPath = Server.MapPath(liveimagebase) & "\"
'if right(uploadPath,1) <> "\" then
'	uploadPath = uploadPath & "\"
'end if
if IsNull(fImageOrient) or fImageOrient = "" then
	fImageOrient = ""
end if

if IsNull(fImage) or fImage = "" then
	SetImageReSize = false
else
	' resize image here
	dim objExec, dnloadPath1, dnloadPath2, strExecResult1, strExecResult2, strSizeDim1, strSizeDim2
	Set objExec = Server.CreateObject("ASPExec.Execute")
	if fImageOrient = 3 then
		strSizeDim1 = "-rh 100 -rw 100"		' -rh 100 -rw 250
		strSizeDim2 = "-rh 300 -rw 750"
	else
		strSizeDim1 = "-rh 100 -rw 100"
		strSizeDim2 = "-rh 300 -rw 300"
	end if

	' 1st pass create thumbnail image
	dnloadPath1 = Server.MapPath(thumbimagebase) & "\"
	objExec.Application = "c:\com\jdo.exe -ss -q 75 " & strSizeDim1 & " -rb -op " & dnloadPath1 & " " & uploadpath & fImage
	objExec.Parameters = ""
	strExecResult1 = objExec.ExecuteDosApp

	' 2nd pass create photo image
	dnloadPath2 = Server.MapPath(photoimagebase) & "\"
	objExec.Application = "c:\com\jdo.exe -ss -q 85 " & strSizeDim2 & " -rb -op " & dnloadPath2 & " " & uploadpath & fImage
	objExec.Parameters = ""
	strExecResult2 = objExec.ExecuteDosApp

	' IsImageGood = true
	set objExec = Nothing
	SetImageReSize = true
end if

end function


%>