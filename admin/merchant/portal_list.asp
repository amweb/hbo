<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: inv_list.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Inventory Admin
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_INVENTORY_VIEW) then
	response.redirect "main.asp"
end if

dim blnCanEdit
blnCanEdit = CheckUserAccess(ACC_INVENTORY_EDIT)

dim strPageType, strPageTitle

strPageType = "admininvlist"
strPageTitle = "Portal Listing"

const STR_DETAIL_ITEM_SCRIPT = "portal_detail.asp?"

OpenConn

dim strSQL, rsData

' get folder list
dim dctNavList, intParentID

intParentID = Request("parentid")
if IsNumeric(intParentID) then
	intParentID = CLng(intParentID)
else
	intParentID = 0
end if

BuildNavList intParentID

strSQL = "SELECT intID, vchHeaderImg, vchFooterImg, vchURL, intInvID, vchItemName, chrStatus"
strSQL = strSQL & " FROM " & STR_TABLE_STOREFRONT
strSQL = strSQL & " WHERE chrStatus<>'D'"
strSQL = strSQL & " ORDER BY intID"
set rsData = gobjConn.execute(strSQL)


DrawPage


rsData.close
set rsData = nothing
response.end

sub DrawPage
	dim intID, strStatus, strName, strHeaderImg, strFooterImg, strURL, strInvID
	
	call DrawHeaderUpdate()
	
	DrawListTitle
	DrawListNavigation
	DrawListHeader
	if rsData.eof then
		%>
		<TR>
		<TD></TD>
		<TD COLSPAN=6><%= font(1) %>&nbsp;<BR>
		<%= font(2) %><I>This folder contains no items or sub-folders.</I></FONT><BR>
		&nbsp;</TD>
		</TR>
		<%
	else
		while not rsData.eof
			intID = rsData("intID")
			strStatus = rsData("chrStatus")
			strName = rsData("vchItemName")
			strHeaderImg = rsData("vchHeaderImg")
			strFooterImg = rsData("vchFooterImg")
			strURL = rsData("vchURL")
			strInvID = rsData("intInvID")
			rsData.MoveNext
			
			
			DrawListItem intID, strStatus, strName, strHeaderImg, strFooterImg, strURL, strInvID
			if not rsData.eof then
				DrawListItemSep
			end if
		wend
	end if
	DrawListFooter
end sub

sub DrawListTitle
%>
<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%">
<TR>
	<TD><%= font(5) %><%= strPageTitle %></TD>
<% if not blnPrintMode then %>
	<TD ALIGN=RIGHT><%= font(1) %>
	<% ' page actions %>
	<A HREF="<%= STR_DETAIL_ITEM_SCRIPT %>parentid=<%= intParentID %>">New Portal<img src="<%= imagebase %>icon_newproduct.gif" width="21" height="16" alt="" border="0" align="absmiddle" /></A>
	&nbsp;
	</TD>
<% end if %>
</TR>
</TABLE>
<%= spacer(1,5) %><BR>

<%
end sub

sub DrawListNavigation
	' dctNavList - reversed order, key=ID, item=name
	dim c, x, i, k
	x = dctNavList.count - 1
	i = dctNavList.items
	k = dctNavList.keys
	response.write "<TABLE BORDER=0 CELLPADDING=6 CELLSPACING=0 WIDTH=""100%"">"
	response.write "<TR><TD>" & font(1)
	for c = x to 0 step -1
		if c > 0 then
			response.write "<B><A HREF=""" & STR_LIST_SCRIPT & "parentid=" & k(c) & """>" & i(c) & "</A></B> : "
		else
			response.write i(c)
		end if
	next
	response.write "</TD>"
	if x > 0 then
		response.write "<TD ALIGN=RIGHT>" & font(1) & "<A HREF=""" & STR_LIST_SCRIPT & "parentid=" & k(1) & """>To Previous Folder<IMG SRC=""" & imagebase & "icon_folderup.gif"" WIDTH=""16"" HEIGHT=""16"" ALT=""To Previous Folder"" BORDER=""0"" ALIGN=""absmiddle"" HSPACE=""5""></A></TD>"
	end if
	response.write "</TR></TABLE>"
end sub

sub BuildNavList(byVal intParentID)
	dim strSQL, rsData
	set dctNavList = Server.CreateObject("Scripting.Dictionary")
	while intParentID <> 0
		strSQL = "SELECT intID, vchItemName FROM " & STR_TABLE_STOREFRONT & " WHERE chrStatus<>'D'"
		set rsData = gobjConn.execute(strSQL)
		dctNavList.Add intParentID, rsData("vchItemName") & ""
		intParentID = rsData("intParentID")
		rsData.close
		set rsData = nothing
	wend
	dctNavList.Add 0, "Top"
end sub

sub DrawListHeader
%>
<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVLtGrey) %>">
<TR>
	<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cWhite %>">
	<TR>
		<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%">
		<TR BGCOLOR="<%= cVLtYellow %>">
			<TD><%= font(1) %>&nbsp;</TD>
			<TD NOWRAP><%= font(1) %><B>Name</B></TD>
			<TD NOWRAP><%= font(1) %><B>&nbsp;URL&nbsp;</B></TD>
			<TD NOWRAP ALIGN="right"><%= font(1) %><B>Status</B></TD>
		</TR>
<%
end sub

sub DrawListItem(intID, strStatus, strName, strHeaderImg, strFooterImg, strURL, strInvID)
%>	
		<TR VALIGN=BOTTOM>
			<TD ALIGN=CENTER><A HREF="<%= STR_DETAIL_ITEM_SCRIPT %>id=<%= intID %>"><IMG SRC="<%= imagebase %>icon_product.gif" WIDTH="16" HEIGHT="16" BORDER="0"></A></TD>
			<TD><%= font(2) %><A HREF="<%= STR_DETAIL_ITEM_SCRIPT %>id=<%= intID %>"><%= strName %></A></TD>
			<TD NOWRAP><%= strURL %></TD>
			<TD NOWRAP ALIGN="right"><%= font(1) %><%= GetStatusStr(strStatus, true) %></TD>
		</TR>
<% if not blnPrintMode then %>
		<TR>
			<TD></TD>
			<TD><%= fontx(1,1,cLight) %><% if blnCanEdit then %><A HREF="<%= STR_DETAIL_ITEM_SCRIPT %>id=<%= intID %>">Edit</A>&nbsp;<% end if %></TD>
		</TR>
<%
		end if
end sub

sub DrawListItemSep
%>
		<TR>
			<TD COLSPAN=7><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVVLtGrey) %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
		</TR>
<%
end sub

sub DrawListFooter
%>
		</TABLE></TD>
	</TR>
	</TABLE></TD>
</TR>
</TABLE>
<%
end sub
%>