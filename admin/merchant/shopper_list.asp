<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<!--#include file="../config/incSearch.asp"-->
<%

' =====================================================================================
' = File: shopper_list.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Merchant shopper admin
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_SHOPPER_VIEW) and Not CheckUserAccess(ACC_ADMIN) then
	response.redirect "menu.asp"
end if

dim strPageTitle
const STR_PAGE_TYPE = "adminshopperlist"

const STR_SEARCH_SCRIPT = "shopper_list.asp?"
const STR_LIST_SCRIPT = "shopper_list.asp?"
const STR_DETAIL_SCRIPT = "shopper_detail.asp?"

OpenConn

strSearchAuxText = "Export Mailing List"
strSearchAuxAction = "export"

dim strSQL, rsData
dim strMode, strAction
dim blnIncludeShipping, blnIncludeBilling, blnIncludeNotes, strFormat

strMode = lcase(Request("mode"))
if strMode <> "a" and strMode <> "i" then
	strMode = "b"
end if
strAction = lcase(Request("auxaction"))
if strAction = "" then
	strAction = lcase(Request("action"))
end if
if strAction = "export" then
	GetExportFlags blnIncludeBilling, blnIncludeShipping, blnIncludeNotes, strFormat
end if

strSearchSQLPrefix = "SELECT TOP 4000 B.intID, B.chrStatus, B.vchEmail, B.vchLastName, B.vchFirstName"
if false and strAction = "export" then
	if blnIncludeBilling then
		strSearchSQLPrefix = strSearchSQLPrefix & ","
		strSearchSQLPrefix = strSearchSQLPrefix & "P.vchLastName AS P_vchLastName,"
		strSearchSQLPrefix = strSearchSQLPrefix & "P.vchFirstName AS P_vchFirstName,"
		strSearchSQLPrefix = strSearchSQLPrefix & "P.vchAddress1 AS P_vchAddress1,"
		strSearchSQLPrefix = strSearchSQLPrefix & "P.vchAddress2 AS P_vchAddress2,"
		strSearchSQLPrefix = strSearchSQLPrefix & "P.vchCity AS P_vchCity,"
		strSearchSQLPrefix = strSearchSQLPrefix & "P.vchState AS P_vchState,"
		strSearchSQLPrefix = strSearchSQLPrefix & "P.vchZip AS P_vchZip,"
		strSearchSQLPrefix = strSearchSQLPrefix & "P.vchDayPhone AS P_vchDayPhone,"
		strSearchSQLPrefix = strSearchSQLPrefix & "P.vchEmail AS P_vchEmail "
	end if
	if blnIncludeShipping then
		strSearchSQLPrefix = strSearchSQLPrefix & ","
		strSearchSQLPrefix = strSearchSQLPrefix & "S.vchLastName AS S_vchLastName,"
		strSearchSQLPrefix = strSearchSQLPrefix & "S.vchFirstName AS S_vchFirstName,"
		strSearchSQLPrefix = strSearchSQLPrefix & "S.vchAddress1 AS S_vchAddress1,"
		strSearchSQLPrefix = strSearchSQLPrefix & "S.vchAddress2 AS S_vchAddress2,"
		strSearchSQLPrefix = strSearchSQLPrefix & "S.vchCity AS S_vchCity,"
		strSearchSQLPrefix = strSearchSQLPrefix & "S.vchState AS S_vchState,"
		strSearchSQLPrefix = strSearchSQLPrefix & "S.vchZip AS S_vchZip,"
		strSearchSQLPrefix = strSearchSQLPrefix & "S.vchDayPhone AS S_vchDayPhone,"
		strSearchSQLPrefix = strSearchSQLPrefix & "S.vchEmail AS S_vchEmail "
	end if
end if
strSearchSQLPrefix = strSearchSQLPrefix & " FROM " & STR_TABLE_SHOPPER & " AS B "
if false and strAction = "export" then
	if blnIncludeBilling then
		strSearchSQLPrefix = strSearchSQLPrefix & "LEFT JOIN " & STR_TABLE_SHOPPER & " AS P ON B.intID = P.intShopperID "
	end if
	if blnIncludeShipping then
		strSearchSQLPrefix = strSearchSQLPrefix & "LEFT JOIN " & STR_TABLE_SHOPPER & " AS S ON  B.intID = S.intShopperID  "
	end if
end if
strSearchSQLPrefix = strSearchSQLPrefix & " WHERE "
strSearchSQLPrefix = strSearchSQLPrefix & " B.chrStatus <> 'D'"
if strMode = "b" or strAction = "export" then
    strSearchSQLPrefix = strSearchSQLPrefix & " AND B.chrType IN ('A'"
	if blnIncludeBilling then strSearchSQLPrefix = strSearchSQLPrefix & ",'B'"
	if blnIncludeShipping then strSearchSQLPrefix = strSearchSQLPrefix & ",'S'"
    strSearchSQLPrefix = strSearchSQLPrefix & ")"
end if
strSearchSQLSuffix = "ORDER BY B.vchLastName, B.vchFirstName, B.vchEmail"

Search_AddLabel "Shopper Information"
Search_AddNumberRange "#B.intID", "Shopper ID"
Search_AddDateRange "%B.dtmCreated", "Date Created"
Search_AddDateRange "%B.dtmUpdated", "Date Updated"
if strMode = "a" then
	Search_AddText "B.vchCreatedByIP", 15, 15, "Created by IP"
	Search_AddText "B.vchUpdatedByIP", 15, 15, "Updated by IP"
	Search_AddText "B.vchCreatedByUser", 15, 32, "Created by User"
	Search_AddText "B.vchUpdatedByUser", 15, 32, "Updated by User"
end if
if strMode <> "b" then
	Search_AddCheckboxGroup "+B.chrStatus", 4, dctShopperStatusValues, "Status"
	Search_AddCheckboxGroup "+B.chrType", 4, dctShopperTypeValues, "Type"
end if
Search_AddText "B.vchLastName", 24, 32, "Last Name"
Search_AddText "B.vchFirstName", 24, 32, "First Name"

InitSearchEngine
	
if strAction = "search" or strAction = "export" then
    if strAction = "export" then
        strSearchSQLPrefix = replace(strSearchSQLPrefix,"B.intID, B.chrStatus, ", "") & " AND B.vchEmail LIKE '%@%.%'"
        strSearchSQLSuffix = "GROUP BY B.vchEmail, B.vchLastName, B.vchFirstName " & strSearchSQLSuffix
    end if

	if strMode = "b" then
		strSQL = BuildBasicSearchSQL()
	else
		strSQL = BuildSearchSQL()
	end if
'	response.write "<small>" & strSQL & "<br /></small>" & vbcrlf
'	response.end
	if strSQL <> "" then
		set rsData = gobjConn.execute(strSQL)
	else
		strAction = ""
	end if
end if

intCurrentGroup = intNumGroups + 1
if intCurrentGroup > INT_MAX_SEARCH_GROUPS then
	intCurrentGroup = 0
end if

if strAction = "export" then
	DrawExport strFormat
else
DrawHeaderUpdate
	if strAction = "search" then
		DrawSearchResults
	else
		DrawSearchPage "Shoppers"
	end if
	
end if

if IsObject(rsData) then
	rsData.close
	set rsData = nothing
end if
response.end

sub LoadPresetSearch(strPreset)
	strMode = "i"
	select case strPreset
		case "all":
			arySearchGroups(1,0).Add "B.chrStatus", "y~D"
		case "active":
			arySearchGroups(1,0).Add "B.chrStatus", "n~A"
		case else:	' invalid preset
			response.redirect "default.asp"
	end select
end sub

sub DrawSearchResults
	strPageTitle = "Search Results - Shoppers"
	DrawSearchListTitle strPageTitle
	DrawListHeader
	if rsData.eof then
%>
<TR>
	<TD COLSPAN="5" ALIGN="center">No shoppers were found matching your search.</TD>
</TR>
<%
	else
		while not rsData.eof
			DrawListItem rsData("intID"), rsData("vchEmail"), rsData("vchLastName") & ", " & rsData("vchFirstName"), rsData("chrStatus")
			rsData.MoveNext
			if not rsData.eof then
				DrawListItemSep
			end if
		wend
	end if
	DrawListFooter
end sub

sub DrawListHeader
%>
<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVLtGrey) %>">
<TR>
	<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cWhite %>">
	<TR>
		<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%">
		<TR BGCOLOR="<%= cVLtYellow %>">
			<TD>&nbsp;</TD>
			<TD NOWRAP><B>&nbsp;Full Name&nbsp;</B></TD>
			<TD NOWRAP><B>&nbsp;Email Address&nbsp;</B></TD>
			<TD NOWRAP><B>Status</B></TD>
		</TR>
<%
end sub

sub DrawListItem(intID, strUsername, strName, strStatus)
%>
		<TR VALIGN=BOTTOM>
			<TD ALIGN=CENTER><A HREF="<%= STR_DETAIL_SCRIPT %>id=<%= intID %>"><IMG SRC="<%= imagebase %>icon_shopper.gif" WIDTH="16" HEIGHT="16" ALT="View Shopper Details" BORDER="0"></A></TD>
			<TD NOWRAP>&nbsp;<A HREF="<%= STR_DETAIL_SCRIPT %>id=<%= intID %>"><%= strName %></A>&nbsp;</TD>
			<TD NOWRAP>&nbsp;<%= strUsername %>&nbsp;</TD>
			<TD NOWRAP><%= GetStatusStr(strStatus, true) %></TD>
		</TR>
<%
end sub

sub DrawListItemSep
%>
		<TR>
			<TD COLSPAN=5><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVVLtGrey) %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
		</TR>
<%
end sub

sub DrawListFooter
%>
		</TABLE></TD>
	</TR>
	</TABLE></TD>
</TR>
</TABLE>
<%
end sub

sub xDrawPage
%>
<CENTER>

<B><%= STR_PAGE_TITLE %></B><BR>
<BR>

<TABLE BORDER="1" CELLPADDING="2" CELLSPACING="0" WIDTH="540">
<TR BGCOLOR="#AAAAAA">
	<TD NOWRAP><B>Username</B></TD>
	<TD NOWRAP><B>Full Name</B></TD>
	<TD NOWRAP><B>Status</B></TD>
	<TD NOWRAP>[<A HREF="shopper_detail.asp">Add Shopper</A>]</TD>
</TR>
<%
	if rsData.eof then
%>
<TR BGCOLOR="#DDDDDD">
	<TD COLSPAN="6" ALIGN="middle">No shoppers exist at this time.</TD>
</TR>
<%
	else
		dim tfAltColor
		while not rsData.eof
			tfAltColor = not tfAltColor
%>
<TR<%= iif(tfAltColor=true, " BGCOLOR=""#DDDDDD""", "") %>>
	<TD NOWRAP><%= rsData("vchEmail") %></TD>
	<TD NOWRAP><%= rsData("vchLastName") & ", " & rsData("vchFirstName") %></TD>
	<TD NOWRAP><%= GetStatusStr(rsData("chrStatus"), true) %></TD>
	<TD NOWRAP>[<a href="shopper_detail.asp?id=<%= rsData("intID") %>">Edit</a>]</TD>
</TR>
<%
			rsData.MoveNext
		wend
	end if
%>
</TABLE>
<%
end sub

sub DrawExport(strFormat)
	dim strFieldSep, strFieldQuote, strFieldAltQuote, strFileName, strData, strEOL
	if strFormat = "CSV" then
		strFileName = "export.csv"
		strFieldSep = ","
		strFieldQuote = """"
		strFieldAltQuote = "'"
		strEOL = vbCrLf
	else
		strFileName = "export.xls"
		strFieldSep = Chr(9)
		strFieldQuote = ""
		strFieldAltQuote = ""
		strEOL = vbCrLf
	End If
	response.ContentType = "text/csv"
	response.AddHeader "Content-transfer-encoding", "binary"
	response.AddHeader "Content-Disposition", "attachment; filename=" & strFileName

    if true then
        '--- generic method to include column headers
		dim separator, q, i, s
		separator = strFieldSep     ' ","
		q = strFieldQuote           ' """"
		s = 0
		for i = s to rsData.Fields.count - 1
			if i > s then
				response.write separator
			end if
			response.write q & replace(replace(rsData(i).Name & "", "_", " "),"vch","") & q
		next
		response.write vbCrlf
        '--- generic method to include all columns
		While Not rsData.eof and Response.IsClientConnected()
			For i = s to rsData.Fields.Count - 1
				if i > s then
					response.write separator
				end if
				if strFieldQuote <> "" then
    			    response.write q & replace(replace(rsData(i) & "", ",", " "), strFieldQuote, strFieldAltQuote) & q
				else
    				response.Write q & rsData(i) & q
    		    end if
			Next
			Response.Write vbCrLf
			rsData.MoveNext
		Wend
	elseif strFieldQuote <> "" then
		while not rsData.eof and Response.IsClientConnected()
'			response.write strFieldQuote & replace(rsData("intID") & "", strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("vchEmail") & "", ",", " "), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("vchLastName") & "", ",", " "), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("vchFirstName") & "", ",", " "), strFieldQuote, strFieldAltQuote) & strFieldQuote

		if false and blnIncludeBilling then
			response.write ","
			response.write strFieldQuote & replace(replace(rsData("P_vchEmail") & "", ",", " "), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchLastName") & "", ",", " "), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchFirstName") & "", ",", " "), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchAddress1") & "", ",", " "), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchAddress2") & "", ",", " "), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchCity") & "", ",", " "), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchState") & "", ",", " "), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchZip") & "", ",", " "), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("P_vchDayPhone") & "", ",", " "), strFieldQuote, strFieldAltQuote) & strFieldQuote
		end if
		if false and blnIncludeShipping then
			response.write ","
			response.write strFieldQuote & replace(replace(rsData("S_vchEmail") & "", ",", " "), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("S_vchLastName") & "", ",", " "), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("S_vchFirstName") & "", ",", " "), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("S_vchAddress1") & "", ",", " "), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("S_vchAddress2") & "", ",", " "), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("S_vchCity") & "", ",", " "), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("S_vchState") & "", ",", " "), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("S_vchZip") & "", ",", " "), strFieldQuote, strFieldAltQuote) & strFieldQuote & ","
			response.write strFieldQuote & replace(replace(rsData("S_vchDayPhone") & "", ",", " "), strFieldQuote, strFieldAltQuote) & strFieldQuote
		end if

			response.write strEOL
			rsData.MoveNext
		wend
	else
		while not rsData.eof and Response.IsClientConnected()
'			response.write rsData("intID") & ","
			response.write replace(rsData("vchEmail") & "", ",", " ") & ","
			response.write replace(rsData("vchLastName") & "", ",", " ") & ","
			response.write replace(rsData("vchFirstName") & "", ",", " ")

		if false and blnIncludeBilling then
			response.write ","
			response.write replace(rsData("P_vchEmail") & "", ",", " ") & ","
			response.write replace(rsData("P_vchLastName") & "", ",", " ") & ","
			response.write replace(rsData("P_vchFirstName") & "", ",", " ") & ","
			response.write replace(rsData("P_vchAddress1") & "", ",", " ") & ","
			response.write replace(rsData("P_vchAddress2") & "", ",", " ") & ","
			response.write replace(rsData("P_vchCity") & "", ",", " ") & ","
			response.write replace(rsData("P_vchState") & "", ",", " ") & ","
			response.write replace(rsData("P_vchZip") & "", ",", " ") & ","
			response.write replace(rsData("P_vchDayPhone") & "", ",", " ")
		end if
		if false and blnIncludeShipping then
			response.write ","
			response.write replace(rsData("S_vchEmail") & "", ",", " ") & ","
			response.write replace(rsData("S_vchLastName") & "", ",", " ") & ","
			response.write replace(rsData("S_vchFirstName") & "", ",", " ") & ","
			response.write replace(rsData("S_vchAddress1") & "", ",", " ") & ","
			response.write replace(rsData("S_vchAddress2") & "", ",", " ") & ","
			response.write replace(rsData("S_vchCity") & "", ",", " ") & ","
			response.write replace(rsData("S_vchState") & "", ",", " ") & ","
			response.write replace(rsData("S_vchZip") & "", ",", " ") & ","
			response.write replace(rsData("S_vchDayPhone") & "", ",", " ")
		end if

			response.write strEOL
			rsData.MoveNext
		wend
	end if
end sub

%>