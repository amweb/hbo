<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: group_list.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Inventory Admin
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_INVENTORY_VIEW) then
	response.redirect "main.asp"
end if

dim blnCanEdit
blnCanEdit = CheckUserAccess(ACC_INVENTORY_EDIT)

dim strPageType, strPageTitle
strPageType = "admininvlist"
strPageTitle = "Group List"

const STR_LIST_SCRIPT = "group_list.asp?"
const STR_DETAIL_GROUP_SCRIPT = "group_list_detail.asp?"
const STR_STOCK_LOW_SCRIPT = "group_list.asp?"
const STR_STOCK_HIGH_SCRIPT = "group_list.asp?"

OpenConn
dim strSQL, rsData

' gather inventory statistics
dim intLowCount, intHighCount, intInvCount
strSQL = "SELECT "
strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_GROUPS & " where chrStatus <> 'D') AS intCount"
'response.write strSQL
set rsData = gobjConn.execute(strSQL)
intInvCount = rsData("intCount")
rsData.close
set rsData = nothing

' get folder list
dim dctNavList, intParentID

intParentID = 0

' SWSCodeChange - 04aug2000 added chrParentStatus
' , chrParentStatus - currently is not supported in thecampaignstore
strSQL = "SELECT intID, chrStatus, vchGroupName"
strSQL = strSQL & " FROM " & STR_TABLE_GROUPS
strSQL = strSQL & " WHERE chrStatus <> 'D'"' AND intParentID=" & intParentID
strSQL = strSQL & " ORDER BY vchGroupName"
set rsData = gobjConn.execute(strSQL)


DrawPage


rsData.close
set rsData = nothing
response.end

sub DrawPage
	dim c, strPrevType, blnCanMoveUp, blnCanMoveDown
	dim intID, strType, strStatus, strPartNumber, strName, mnyPrice, mnyShipPrice, intStock
	' SWSCodeChange - 04aug2000 added strParentStatus
	dim strParentStatus
	
	call DrawHeaderUpdate()
	
	DrawListTitle
	DrawListHeader
	strPrevType = ""
	if rsData.eof then
		%>
		<tr>
		<td></td>
		<td class="font1" colspan=6>&nbsp;<BR />
		<span class="font2" ><I>There are no groups currently in inventory system.</I></span><BR />&nbsp;
		</td>
		</tr>
		<%
	else
		while not rsData.eof
			intID = rsData("intID")
			strName = rsData("vchGroupName")
			strStatus = rsData("chrStatus")
			rsData.MoveNext
			' SWSCodeChange - 04aug2000 added parent status
			DrawListGroups intID, strStatus, strName, blnCanMoveUp, blnCanMoveDown
			if not rsData.eof then
				DrawListItemSep
			end if
		wend
	end if
	DrawListFooter
end sub

sub DrawListTitle
%>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
	<td class="font5"><%= strPageTitle %></td>
<% if not blnPrintMode then %>
	<td class="font1" align=right>
	<A HREF="<%= STR_DETAIL_GROUP_SCRIPT %>">New Group<img src="<%= imagebase %>icon_newfolder.gif" width="21" height="16" alt="" border="0" align="absmiddle" /></A>
	</td>
<% end if %>
</tr>
</table>
<%= spacer(1,5) %><BR>

	<% stlBeginInfoBar INT_PAGE_WIDTH %>
	<B>Total Groups:</B> <%= intInvCount %>&nbsp;
	</FONT></td>
	<td align=right><%= fontx(1,1,cWhite) & FormatDateTime(date,1) %>
	<% stlEndInfoBar %>
<%
end sub

sub DrawListHeader
%>
<table border="0" cellpadding="1" cellspacing="0" width="100%" bgcolor="<%= iif(blnPrintMode, cBlack, cVLtGrey) %>">
<tr>
	<td><table border="0" cellpadding="2" cellspacing="0" width="100%" bgcolor="<%= cWhite %>">
	<tr>
		<td><table border="0" cellpadding="2" cellspacing="0" width="100%">
		<tr class="font1" bgcolor="<%= cVLtYellow %>">
			<td NOWRAP><B>Name</B></td>
			<td NOWRAP align="right"><B>Status</B></td>
		</tr>
<%
end sub

' SWSCodeChange - 04aug2000 added strParentStatus
sub DrawListGroups(intID, strStatus, strName, blnCanMoveUp, blnCanMoveDown)
	dim strActionLabel
	strActionLabel = iif(blnCanEdit, "Edit", "View")
%>
		<tr valign=BOTTOM>
		<% if intID = "1" then %>
			<td class="font2"><%= strName %></td>
		<% else %>
			<td class="font2"><A HREF="<%= STR_DETAIL_GROUP_SCRIPT %>id=<%= intID %>"><%= strName %></A></td>
		<% end if %>
			<% ' SWSCodeChange - 04aug2000 added parent status 
			%>
			<td class="font1" NOWRAP align="right"><%= GetStatusStr(strStatus, true) %></td>
		</tr>
<%	if not blnPrintMode then %>
		<tr>
			<% if intID = "1" then %>
				<td ><%= fontx(1,1,cLight) %>System Created</td>
			<% else %>
				<td ><%= fontx(1,1,cLight) %><% if blnCanEdit then %><A HREF="<%= STR_DETAIL_GROUP_SCRIPT %>id=<%= intID %>"><%= strActionLabel %></A><% end if %></td>
			<% end if %>
		</tr>
<%
	end if
end sub

sub DrawListItemSep
%>
		<tr>
			<td colspan=7><table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="<%= iif(blnPrintMode, cBlack, cVVLtGrey) %>"><tr><td><%= spacer(1,1) %></td></tr></table></td>
		</tr>
<%
end sub

sub DrawListFooter
%>
		</table></td>
	</tr>
	</table></td>
</tr>
</table>
<%
end sub
%>