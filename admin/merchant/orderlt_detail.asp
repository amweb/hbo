<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<!--#include file="../../config/incVeraCore.asp"-->
<%

' =====================================================================================
' = File: orderlt_detail.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Display order details.
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_ORDER_VIEW) then
	response.redirect "menu.asp"
end if
Session(SESSION_MERCHANT_ACCESS_STATUS) = "*"
const STR_PAGE_TITLE = "Order Details"
const STR_PAGE_TYPE = "adminorderlist"

OpenConn

dim blnCanRaw, blnCanEdit, blnCanApprove, blnCanDecline, blnCanShip, blnCanTrans, blnCanSendReceipt, blnCanShipOption
blnCanRaw = CheckUserAccess(ACC_RAW)
blnCanEdit = CheckUserAccess(ACC_ORDER_EDIT)
blnCanApprove = CheckUserAccess(ACC_ORDER_APPROVE)
blnCanDecline = CheckUserAccess(ACC_ORDER_DECLINE)
blnCanShip = CheckUserAccess(ACC_ORDER_SHIP)
blnCanShipOption = blnCanShip
blnCanSendReceipt = CheckUserAccess(ACC_ORDER_EMAILRECEIPT)
blnCanTrans = CheckUserAccess(ACC_ORDER_TRANS_AUTH) OR CheckUserAccess(ACC_ORDER_TRANS_DEPOSIT) OR CheckUserAccess(ACC_ORDER_TRANS_CREDIT) OR CheckUserAccess(ACC_ORDER_TRANS_MANUAL) OR CheckUserAccess(ACC_ORDER_TRANS_AUTHDEP)

dim intID, strAction
strAction = Request("action")
intID = Request("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if

dim STR_SEARCH_SCRIPT, STR_DETAIL_SCRIPT, STR_EDIT_SCRIPT, STR_EDIT_LINES_SCRIPT
STR_SEARCH_SCRIPT = "orderlt_list.asp?"
STR_DETAIL_SCRIPT = "orderlt_detail.asp?id=" & intID
STR_EDIT_SCRIPT = "orderlt_ordermod.asp?order=" & intID
STR_EDIT_LINES_SCRIPT = "order_itemmod.asp?order=" & intID
select case strAction
	case "createnew":
		' create a new order
		if CheckUserAccess(ACC_ORDER_CREATE) then
			response.redirect "orderlt_detail.asp?id=" & CreateNewOrder_Other("", "9")
		else
			response.redirect "menu.asp"
		end if
	case "recalc":
		' recalc order
		ReCalcOrder_Other intID
		response.redirect "orderlt_detail.asp?id=" & intID
	case "btnOpen":
		' change status to 'B' (abandoned)
       
		if blnCanEdit then
			UpdateOrderStatus_Other intID, "0"
		end if
		response.redirect "orderlt_detail.asp?id=" & intID
	case "btnVeraCore":	
		call AddOrder(intID)
	case "btnClose":
		if blnCanEdit then
			UpdateOrderStatus_Other intID, "S"

            
    Dim strText,strPreBuy, strEmail, strLabel, strShipLabel

    strSQL = "SELECT TOP 1 C.vchItemName FROM HBO_ORDER O INNER JOIN HBO_LINEITEM L ON O.intID=L.intOrderID INNER JOIN " & STR_TABLE_INVENTORY_LONG_TERM & " I ON L.intInvId=I.intID INNER JOIN " & STR_TABLE_INVENTORY_LONG_TERM & " C ON I.intCampaign=C.intID WHERE O.intID=" & intID & " AND C.chrStatus='A'" 
    set rsData = gobjConn.execute(strSQL) 

    strPreBuy = rsData(0)&""

    strSQL = "SELECT vchEmail, S.vchFirstName + ' ' + S.vchLastName as vchLabel  FROM HBO_SHOPPER S INNER JOIN HBO_ORDER O ON O.intShopperID=S.intID WHERE O.intID=" & intID
    set rsData = gobjConn.execute(strSQL) 
    strEmail = rsData("vchEmail")&""
    strLabel = rsData("vchLabel")&""

    strSQL = "SELECT vchEmail, S.vchFirstName + ' ' + S.vchLastName as vchLabel  FROM HBO_SHOPPER S INNER JOIN HBO_ORDER O ON O.intShipShopperID=S.intID WHERE O.intID=" & intID
    set rsData = gobjConn.execute(strSQL) 
    strShipLabel = rsData("vchLabel")


    strText = strText & "" & vbNewLine
strText = strText & "<html xmlns=""http://www.w3.org/1999/xhtml""><head>" & vbNewLine
strText = strText & "      <meta http-equiv=""Content-Type"" content=""text/html; charset=utf-8"">" & vbNewLine
strText = strText & "      <meta name=""viewport"" content=""width=device-width, initial-scale=1.0"">" & vbNewLine
strText = strText & "      <title>HBO POS Resource Center</title>" & vbNewLine
strText = strText & "      <style type=""text/css"">" & vbNewLine
strText = strText & "         /* Client-specific Styles */" & vbNewLine
strText = strText & "         #outlook a {padding:0;} /* Force Outlook to provide a ""view in browser"" menu link. */" & vbNewLine
strText = strText & "         body{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}" & vbNewLine
strText = strText & "         /* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */" & vbNewLine
strText = strText & "         .ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */" & vbNewLine
strText = strText & "         .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing.  More on that: http://www.emailonacid.com/forum/viewthread/43/ */" & vbNewLine
strText = strText & "         #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}" & vbNewLine
strText = strText & "         img {outline:none; text-decoration:none;border:none; -ms-interpolation-mode: bicubic;}" & vbNewLine
strText = strText & "         a img {border:none;}" & vbNewLine
strText = strText & "         .image_fix {display:block;}" & vbNewLine
strText = strText & "         p {margin: 0px 0px !important;}" & vbNewLine
strText = strText & "         " & vbNewLine
strText = strText & "         table td {border-collapse: collapse;}" & vbNewLine
strText = strText & "         table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }" & vbNewLine
strText = strText & "         /*a {color: #e95353;text-decoration: none;text-decoration:none!important;}*/" & vbNewLine
strText = strText & "         /*STYLES*/" & vbNewLine
strText = strText & "         table[class=full] { width: 100%; clear: both; }" & vbNewLine
strText = strText & "         " & vbNewLine
strText = strText & "         /*################################################*/" & vbNewLine
strText = strText & "         /*IPAD STYLES*/" & vbNewLine
strText = strText & "         /*################################################*/" & vbNewLine
strText = strText & "         @media only screen and (max-width: 640px) {" & vbNewLine
strText = strText & "         a[href^=""tel""], a[href^=""sms""] {" & vbNewLine
strText = strText & "         text-decoration: none;" & vbNewLine
strText = strText & "         color: #ffffff; /* or whatever your want */" & vbNewLine
strText = strText & "         pointer-events: none;" & vbNewLine
strText = strText & "         cursor: default;" & vbNewLine
strText = strText & "         }" & vbNewLine
strText = strText & "         .mobile_link a[href^=""tel""], .mobile_link a[href^=""sms""] {" & vbNewLine
strText = strText & "         text-decoration: default;" & vbNewLine
strText = strText & "         color: #ffffff !important;" & vbNewLine
strText = strText & "         pointer-events: auto;" & vbNewLine
strText = strText & "         cursor: default;" & vbNewLine
strText = strText & "         }" & vbNewLine
strText = strText & "         table[class=devicewidth] {width: 440px!important;text-align:center!important;}" & vbNewLine
strText = strText & "         table[class=devicewidthinner] {width: 420px!important;text-align:center!important;}" & vbNewLine
strText = strText & "         table[class=""sthide""]{display: none!important;}" & vbNewLine
strText = strText & "         img[class=""bigimage""]{width: 420px!important;height:219px!important;}" & vbNewLine
strText = strText & "         img[class=""col2img""]{width: 420px!important;height:258px!important;}" & vbNewLine
strText = strText & "         img[class=""image-banner""]{width: 440px!important;height:106px!important;}" & vbNewLine
strText = strText & "         td[class=""menu""]{text-align:center !important; padding: 0 0 10px 0 !important;}" & vbNewLine
strText = strText & "         td[class=""logo""]{padding:10px 0 5px 0!important;margin: 0 auto !important;}" & vbNewLine
strText = strText & "         img[class=""logo""]{width: 420px!important;padding:0!important;margin: 0 auto !important;}" & vbNewLine
strText = strText & "" & vbNewLine
strText = strText & "         }" & vbNewLine
strText = strText & "         /*##############################################*/" & vbNewLine
strText = strText & "         /*IPHONE STYLES*/" & vbNewLine
strText = strText & "         /*##############################################*/" & vbNewLine
strText = strText & "         @media only screen and (max-width: 480px) {" & vbNewLine
strText = strText & "         a[href^=""tel""], a[href^=""sms""] {" & vbNewLine
strText = strText & "         text-decoration: none;" & vbNewLine
strText = strText & "         color: #ffffff; /* or whatever your want */" & vbNewLine
strText = strText & "         pointer-events: none;" & vbNewLine
strText = strText & "         cursor: default;" & vbNewLine
strText = strText & "         }" & vbNewLine
strText = strText & "         .mobile_link a[href^=""tel""], .mobile_link a[href^=""sms""] {" & vbNewLine
strText = strText & "         text-decoration: default;" & vbNewLine
strText = strText & "         color: #ffffff !important; " & vbNewLine
strText = strText & "         pointer-events: auto;" & vbNewLine
strText = strText & "         cursor: default;" & vbNewLine
strText = strText & "         }" & vbNewLine
strText = strText & "         table[class=devicewidth] {width: 280px!important;text-align:center!important;}" & vbNewLine
strText = strText & "         table[class=devicewidthinner] {width: 260px!important;text-align:center!important;}" & vbNewLine
strText = strText & "         table[class=""sthide""]{display: none!important;}" & vbNewLine
strText = strText & "         img[class=""bigimage""]{width: 260px!important;height:136px!important;}" & vbNewLine
strText = strText & "         img[class=""col2img""]{width: 260px!important;height:160px!important;}" & vbNewLine
strText = strText & "         img[class=""image-banner""]{width: 280px!important;height:68px!important;}" & vbNewLine
strText = strText & "	     img[class=""logo""]{width: 260px!important;padding:0!important;margin: 0 auto !important;}" & vbNewLine
strText = strText & "" & vbNewLine
strText = strText & "         " & vbNewLine
strText = strText & "         }" & vbNewLine
strText = strText & "      </style>" & vbNewLine
strText = strText & "" & vbNewLine
strText = strText & "      " & vbNewLine
strText = strText & "   </head>" & vbNewLine
strText = strText & "<body bgcolor=""#f6f4f5"">" & vbNewLine
strText = strText & "<div class=""block"">" & vbNewLine
strText = strText & "   <!-- Start of preheader -->" & vbNewLine
strText = strText & "   <table width=""100%"" bgcolor=""#f6f4f5"" cellpadding=""0"" cellspacing=""0"" border=""0"" id=""backgroundTable"" st-sortable=""preheader"">" & vbNewLine
strText = strText & "      <tbody>" & vbNewLine
strText = strText & "         <tr>" & vbNewLine
strText = strText & "            <td width=""100%"">" & vbNewLine
strText = strText & "               <table width=""580"" cellpadding=""0"" cellspacing=""0"" border=""0"" align=""center"" class=""devicewidth"">" & vbNewLine
strText = strText & "                  <tbody>" & vbNewLine
strText = strText & "                     <!-- Spacing -->" & vbNewLine
strText = strText & "                     <tr>" & vbNewLine
strText = strText & "                        <td width=""100%"" height=""20""></td>" & vbNewLine
strText = strText & "                     </tr>" & vbNewLine
strText = strText & "                     <!-- Spacing -->" & vbNewLine
strText = strText & "                  </tbody>" & vbNewLine
strText = strText & "               </table>" & vbNewLine
strText = strText & "            </td>" & vbNewLine
strText = strText & "         </tr>" & vbNewLine
strText = strText & "      </tbody>" & vbNewLine
strText = strText & "   </table>" & vbNewLine
strText = strText & "   <!-- End of preheader -->" & vbNewLine
strText = strText & "</div>" & vbNewLine
strText = strText & "<div class=""block"">" & vbNewLine
strText = strText & "   <!-- start of header -->" & vbNewLine
strText = strText & "   <table width=""100%"" bgcolor=""#f6f4f5"" cellpadding=""0"" cellspacing=""0"" border=""0"" id=""backgroundTable"" st-sortable=""header"">" & vbNewLine
strText = strText & "      <tbody>" & vbNewLine
strText = strText & "         <tr>" & vbNewLine
strText = strText & "            <td>" & vbNewLine
strText = strText & "               <table width=""580"" bgcolor=""#ffffff"" cellpadding=""0"" cellspacing=""0"" border=""0"" align=""center"" class=""devicewidth"" hlitebg=""edit"" shadow=""edit"">" & vbNewLine
strText = strText & "                  <tbody>" & vbNewLine
strText = strText & "                     <tr>" & vbNewLine
strText = strText & "                        <td>" & vbNewLine
strText = strText & "                           <!-- logo -->" & vbNewLine
strText = strText & "                           <table width=""280"" cellpadding=""0"" cellspacing=""0"" border=""0"" align=""left"" class=""devicewidth"">" & vbNewLine
strText = strText & "                              <tbody>" & vbNewLine
strText = strText & "                                 <tr>" & vbNewLine
strText = strText & "                                    <td valign=""middle"" width=""270"" style=""padding: 20px 0px 10px 20px;"" class=""logo"">" & vbNewLine
strText = strText & "                                       <div class=""imgpop"">" & vbNewLine
strText = strText & "                                          <a href=""http://HBO.drcportal.com/""><img src=""http://HBO.drcportal.com/images/logo.gif"" alt=""logo"" border=""0"" style=""display:block; border:none; outline:none; text-decoration:none;"" st-image=""edit"" class=""logo""></a>" & vbNewLine
strText = strText & "                                       </div>" & vbNewLine
strText = strText & "                                    </td>" & vbNewLine
strText = strText & "                                 </tr>" & vbNewLine
strText = strText & "                              </tbody>" & vbNewLine
strText = strText & "                           </table>" & vbNewLine
strText = strText & "                           <!-- End of logo -->" & vbNewLine
strText = strText & "                        </td>" & vbNewLine
strText = strText & "                     </tr>" & vbNewLine
strText = strText & "                  </tbody>" & vbNewLine
strText = strText & "               </table>" & vbNewLine
strText = strText & "            </td>" & vbNewLine
strText = strText & "         </tr>" & vbNewLine
strText = strText & "      </tbody>" & vbNewLine
strText = strText & "   </table>" & vbNewLine
strText = strText & "   <!-- end of header -->" & vbNewLine
strText = strText & "</div><div class=""block"">" & vbNewLine
strText = strText & "   <!-- image + text -->" & vbNewLine
strText = strText & "   <table width=""100%"" bgcolor=""#f6f4f5"" cellpadding=""0"" cellspacing=""0"" border=""0"" id=""backgroundTable"" st-sortable=""bigimage"">" & vbNewLine
strText = strText & "      <tbody>" & vbNewLine
strText = strText & "         <tr>" & vbNewLine
strText = strText & "            <td>" & vbNewLine
strText = strText & "               <table bgcolor=""#ffffff"" width=""580"" align=""center"" cellspacing=""0"" cellpadding=""0"" border=""0"" class=""devicewidth"" modulebg=""edit"">" & vbNewLine
strText = strText & "                  <tbody>" & vbNewLine
strText = strText & "                     <tr>" & vbNewLine
strText = strText & "                        <td width=""100%"" height=""20""></td>" & vbNewLine
strText = strText & "                     </tr>" & vbNewLine
strText = strText & "                     <tr>" & vbNewLine
strText = strText & "                        <td>" & vbNewLine
strText = strText & "                           <table width=""540"" align=""center"" cellspacing=""0"" cellpadding=""0"" border=""0"" class=""devicewidthinner"">" & vbNewLine
strText = strText & "                              <tbody>" & vbNewLine
strText = strText & "                                 <!-- title -->" & vbNewLine
strText = strText & "                                 <tr>" & vbNewLine
strText = strText & "                                    <td style=""font-family: Helvetica, arial, sans-serif; font-size: 18px; color: #333333; text-align:left;line-height: 20px;"" st-title=""rightimage-title"">" & strLabel & ",<br />The order for " & strShipLabel &  " has been finalized</td>" & vbNewLine
strText = strText & "                                 </tr>" & vbNewLine
strText = strText & "                                 <!-- end of title -->" & vbNewLine
strText = strText & "                                 <!-- Spacing -->" & vbNewLine
strText = strText & "                                 <tr>" & vbNewLine
strText = strText & "                                    <td width=""100%"" height=""20""></td>" & vbNewLine
strText = strText & "                                 </tr>" & vbNewLine
strText = strText & "                                 <!-- Spacing -->" & vbNewLine
strText = strText & "                                 <!-- content -->" & vbNewLine
strText = strText & "                                 <tr>" & vbNewLine
strText = strText & "                                    <td style=""font-family: Helvetica, arial, sans-serif; font-size: 13px; color: #666666; text-align:left;line-height: 24px;"" st-content=""rightimage-paragraph"">" & vbNewLine
strText = strText & "                                       <p>The items that were in your cart at the close of the pre-buy have been submitted to the HBO admin. Items that do not meet the minimum order quantity will not be produced. Once items are finalized, your Order History will be updated. To view your order History, log into the <a href=""http://HBO.drcportal.com/"" style=""color: #c1262f"">POS Resource Center</a>.</p></td>" & vbNewLine
strText = strText & "                                 </tr>" & vbNewLine
strText = strText & "                                 <!-- end of content -->" & vbNewLine
strText = strText & "                                 <!-- Spacing -->" & vbNewLine
strText = strText & "                                 <tr>" & vbNewLine
strText = strText & "                                    <td width=""100%"" height=""50""></td>" & vbNewLine
strText = strText & "                                 </tr>" & vbNewLine
strText = strText & "                                 <!-- Spacing -->" & vbNewLine
strText = strText & "                              </tbody>" & vbNewLine
strText = strText & "                           </table>" & vbNewLine
strText = strText & "                        </td>" & vbNewLine
strText = strText & "                     </tr>" & vbNewLine
strText = strText & "                  </tbody>" & vbNewLine
strText = strText & "               </table>" & vbNewLine
strText = strText & "            </td>" & vbNewLine
strText = strText & "         </tr>" & vbNewLine
strText = strText & "      </tbody>" & vbNewLine
strText = strText & "   </table>" & vbNewLine
strText = strText & "</div>" & vbNewLine
strText = strText & "" & vbNewLine
strText = strText & "</body></html>" & vbNewLine

Dim myMail
Set myMail = CreateObject("CDONTS.NewMail")
myMail.From = "HBOsupport@domeprinting.com"
myMail.To = strEmail
myMail.Subject = "The " & strPreBuy &  " Is Now Closed"
myMail.Body = strText
myMail.BodyFormat=0
myMail.MailFormat=0
myMail.Send
Set myMail = Nothing


		end if
		response.redirect "orderlt_detail.asp?id=" & intID
    case "btnAbandon":
		' change status to 'B' (abandoned)
       
		if blnCanEdit then
			UpdateOrderStatus_Other intID, "B"
		end if
		response.redirect "../main.asp"
	case "btnLock":
		if blnCanEdit then
			UpdateOrderStatus_Other intID, "9"
		end if
		response.redirect "orderlt_detail.asp?id=" & intID
	case "btnUnlock":
		if blnCanEdit then
			UpdateOrderStatus_Other intID, "P"
		end if
		response.redirect "orderlt_detail.asp?id=" & intID
	case "btnSubmit":
		' change status to 'S' (submitted)
		if blnCanEdit then
			UpdateOrderStatus_Other intID, "S"
		end if
		response.redirect "orderlt_detail.asp?id=" & intID
	case "btnApprove":
		' change status to 'A' (approved)
		if blnCanApprove then
			UpdateOrderStatus_Other intID, "A"
			ProcessOrderApprove intID
		end if
		response.redirect "orderlt_detail.asp?id=" & intID
	case "btnDecline":
		' change status to 'K' (declined)
		if blnCanDecline then
			UpdateOrderStatus_Other intID, "K"
		end if
		response.redirect "orderlt_detail.asp?id=" & intID
	case "btnShip":
		' redirect to shipping form
		if blnCanShip then
			response.redirect "order_ship.asp?id=" & intID
		else
			response.redirect "menu.asp"
		end if
	case "btnTrans":
		' redirect to transaction form
		if blnCanTrans then
			response.redirect "order_trans.asp?id=" & intID
		else
			response.redirect "menu.asp"
		end if
'	case "btnModifyOrder":
		' redirect to order mod form
'		response.redirect "orderlt_ordermod.asp?order=" & intID
	case "btnReceipt":
		if blnCanSendReceipt then
			response.redirect "order_emailinvoice.asp?id=" & intiD
		else
			response.redirect "menu.asp"
		end if
	case "btnSystemEdit"
		' redirect to system edit form
		if blnCanRaw then
			response.redirect "order_sysedit.asp?id=" & intID
		else
			response.redirect "menu.asp"
		end if
    case "editsubmit":
		Dim strPartNumber, strItemName, mnyUnitPrice, mnyShipPrice, intInvID, strType, chrTaxFlag, intForceShipMethod, chrForceSoloItem,intViewItemID,rsTemp, intOrderID, intLineItemID

        intViewItemID = CLNG(Request("intInvID")&"")
        intLineItemID = CLNG(Request("intLineItemID")&"")
        intOrderID = CLNG(Request("intOrderID")&"")

        strSQL = "SELECT chrType, vchPartNumber, vchItemName, chrTaxFlag, mnyItemPrice, mnyShipPrice, intForceShipMethod, chrForceSoloItem FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intViewItemID & " AND chrStatus<>'D'"
        response.Write strSQL
		set rsTemp = gobjConn.execute(strSQL)
		strPartNumber = rsTemp("vchPartNumber")&""
		strItemName = rsTemp("vchItemName")&""
		mnyUnitPrice = rsTemp("mnyItemPrice")&""
		mnyShipPrice = rsTemp("mnyShipPrice")&""
		intInvID = intViewItemID&""
		strType = rsTemp("chrType")&""
		chrTaxFlag = rsTemp("chrTaxFlag")&""
		intForceShipMethod = rsTemp("intForceShipMethod")&""
		chrForceSoloItem = rsTemp("chrForceSoloItem")&""

		EditOrderItem_Other intOrderID, intLineItemID, intViewItemID, strPartNumber, strItemName, mnyUnitPrice, mnyShipPrice, chrTaxFlag = "Y", Request("intQuantity"), intForceShipMethod, chrForceSoloItem
			
        response.redirect "orderlt_detail.asp?id=" & intOrderID
    case "cancel":

        strSQL = "UPDATE " & STR_TABLE_LINEITEM_LONG_TERM & " SET chrStatus='C' WHERE intID=" & Cint(Request("id"))
        gobjConn.execute strSQL
        RecalcOrder_Other Cint(Request("orderid"))
        response.redirect "orderlt_detail.asp?id="& Cint(Request("orderid"))
    case "substitute":

        strSQL = "UPDATE " & STR_TABLE_LINEITEM_LONG_TERM & " SET chrStatus='S',vchShippingNUmber=(SELECT vchItemName FROM " & STR_TABLE_INVENTORY_LONG_TERM & " WHERE intID=" & Cint(Request("sub")) & ")  WHERE intID=" & Cint(Request("id"))
        gobjConn.execute strSQL
        response.redirect "orderlt_detail.asp?id="& Cint(Request("orderid"))

end select

dim strSQL, rsData

if intID <= 0 then
	response.redirect STR_SEARCH_SCRIPT
end if

dim strStatusClause
strStatusClause = UserAccessStatus_BuildSQLWhere()
if strStatusClause = "" then
	response.redirect STR_SEARCH_SCRIPT
end if
strSQL = "SELECT S.vchFirstName + ' ' + S.vchLastName AS S_vchShopperAccount, O.* FROM " & STR_TABLE_ORDER_LONG_TERM & " AS O LEFT JOIN " & STR_TABLE_SHOPPER & " AS S ON O.intShopperID = S.intID WHERE O.intID=" & intID & " AND O.chrStatus<>'D' AND (S.chrType='A' OR S.chrType IS NULL)"
strSQL = strSQL & " AND O.chrStatus IN (" & strStatusClause & ")"


set rsData = gobjConn.execute(strSQL)
if rsData.eof then
	rsData.close
	set rsData = nothing
	response.redirect STR_SEARCH_SCRIPT
end if

DrawPage rsData
if (Request.QueryString("print").count > 0) then
%>
	<SCRIPT LANGUAGE="JavaScript"><!--
	// print the window
	window.print();
// --></SCRIPT>
<%
end if


rsData.close
set rsData = nothing

response.end

sub DrawPage(rsInput)
call DrawHeaderUpdate()
	dim rsLineItem, rsShopper, strColor
	dim blnErrors, dctErrorList
	set dctErrorList = Server.CreateObject("Scripting.Dictionary")
	blnErrors = ValidateOrder_Other(intID, dctErrorList)
	' menu options based on current order status
	dim dctMenuList
	set dctMenuList = Server.CreateObject("Scripting.Dictionary")
	select case rsInput("chrStatus")
		case "0","1","2","3","4","5","6","7","8": ' in progress
			if blnCanEdit then
				dctMenuList.Add "btnAbandon", "Abandon Order"
				dctMenuList.Add "btnClose", "Finalize Order"
				dctMenuList.Add "btnVeraCore", "Resend Order To VeraCore"
			end if
		case "9": ' in progress by merchant
			if blnCanEdit then
				dctMenuList.Add "btnAbandon", "Abandon Order"
					dctMenuList.Add "btnClose", "Close Order"
				if not IsNull(rsInput("intShopperID")) then
					dctMenuList.Add "btnUnlock", "Open Order"
				end if
			end if
		case "P": ' pending
			if blnCanEdit then
				dctMenuList.Add "btnAbandon", "Abandon Order"
				dctMenuList.Add "btnLock", "Lock Order"
				dctMenuList.Add "btnVeraCore", "Resend Order To VeraCore"
			end if
		case "S", "A": ' submitted or voice-auth
			if blnCanEdit then
				dctMenuList.Add "btnAbandon", "Abandon Order"
			end if
			if blnCanApprove then
				dctMenuList.Add "btnOpen", "Open Order"
			end if
		case "E": ' error - order locked until sysadmin intervention
		case else: ' unknown status
	end select
	if blnCanRaw then
		dctMenuList.Add "btnSystemEdit", "System View"
		dctMenuList.Add "recalc", "Recalc Order"
	end if
	
	dim strStatus, strStatusText, strStatusCode
	strStatusCode = rsInput("chrStatus")
	strStatus = GetArrayValue(strStatusCode, dctOrderStatus)
	select case rsInput("chrStatus")
		case "0","1","2","3","4","5","6","7","8", "P": ' in progress
			strStatusText = "This order is open, and can be edited by the user."
		case "9": ' in progress by merchant
			strStatusText = "This order is being created or modified by the admin, and has been locked out from the shopper's account."
		case "S":	' submitted
			strStatusText = "This order is closed, and cannot be edited by the user."
		case "A":	' approved
			strStatusText = "This order is closed, and cannot be edited by the user."
		case "B":	' abandoned
			strStatusText = "This order has been abandoned."
		case "H":	' deposited
			strStatusText = "This order has been shipped."
		case "Z":	' authorized
			strStatusText = "Funds have not yet been deposited."
		case "X":	' deposited
			strStatusText = "Funds have been deposited and this order has been shipped."
		case "C":	' credited
			strStatusText = "Funds have been returned to purchaser."
		case "K":	' declined
		case "V":	' voice-auth required
			strStatusText = "The credit card issuer has requested verbal authorization."
		case "E":	' error
			strStatusText = "A system error occurred while processing this order. Please contact technical support."
	end select
	
	dim blnCanEditAccount, blnCanEditPayment, blnCanEditBilling, blnCanEditShipping, blnCanEditItems, blnCanEditShipOption
	if strStatusCode = "9" then
		blnCanEditAccount = blnCanEdit
		blnCanEditPayment = blnCanEdit
		blnCanEditBilling = blnCanEdit
		blnCanEditShipping = blnCanEdit
		blnCanEditItems = blnCanEdit
	else
		blnCanEditAccount = false
		blnCanEditPayment = (InStr("SVAZ", strStatusCode) > 0) and not IsNull(rsInput("intShopperID")) and CheckUserAccess(ACC_ORDER_PAYMENT_EDIT)
		blnCanEditBilling = (InStr("SVAZ", strStatusCode) > 0) and not IsNull(rsInput("intShopperID")) and CheckUserAccess(ACC_ORDER_BILLING_EDIT)
		blnCanEditShipping = (InStr("SVAZ", strStatusCode) > 0) and not IsNull(rsInput("intShopperID")) and CheckUserAccess(ACC_ORDER_SHIPPING_EDIT)
		blnCanEditItems = (InStr("SVAZXH", strStatusCode) > 0) and (CheckUserAccess(ACC_ORDER_ITEMS_EDIT) or CheckUserAccess(ACC_ORDER_NOTES_ADD) or CheckUserAccess(ACC_ORDER_RETURNS_ADD))
	end if
	
	blnCanEditBilling = blnCanEditBilling AND NOT dctErrorList.Exists("account")
	blnCanEditShipping = blnCanEditShipping AND NOT dctErrorList.Exists("account")
	blnCanEditPayment = blnCanEditPayment AND NOT dctErrorList.Exists("billing")
	blnCanEditShipOption = blnCanEditShipping AND NOT dctErrorList.Exists("shipping")
	if (Request.QueryString("print").count > 0) then
%>
	<div style="width:500;align:right;float:right;">
		<a href="<%= nonsecurebase %>store/"><img src="<%= g_imagebase %>banners/banner-sm2.jpg" border="0" /></a>
	</div>
<%
	end if
	DrawTitle "Order Details - " & STR_MERCHANT_TRACKING_PREFIX & rsInput("intID"), dctMenuList
%>
<% if false then %>
<CENTER>
<FONT SIZE="2"><B><%= STR_MERCHANT_NAME %></B></FONT><BR>
<%= STR_MERCHANT_ADDRESS1 & iif(STR_MERCHANT_ADDRESS2 <> "", ", " & STR_MERCHANT_ADDRESS2, "") %><BR>
<%= STR_MERCHANT_CITY & ", " & STR_MERCHANT_STATE & " " & STR_MERCHANT_ZIP %><BR>
<%= "Phone " & STR_MERCHANT_PHONE & "&nbsp;&nbsp;Fax " & STR_MERCHANT_FAX %>
</CENTER>
<BR>
<% end if %>

<% stlBeginStdTable "100%" %>

<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
<TR BGCOLOR="<%= cVLtYellow %>">
	<TD><B>Created:</B></TD>
	<TD><B>Submitted:</B></TD>
	<TD><B>Updated:</B></TD>
</TR>
<TR>
	<TD>&nbsp;&nbsp;<%= FormatDateTimeNoSeconds(rsInput("dtmCreated")) %>&nbsp;&nbsp;&nbsp;</TD>
	<TD>&nbsp;&nbsp;<%= FormatDateTimeNoSeconds(rsInput("dtmSubmitted")) %>&nbsp;&nbsp;&nbsp;</TD>
	<TD>&nbsp;&nbsp;<%= FormatDateTimeNoSeconds(rsInput("dtmUpdated")) %>&nbsp;&nbsp;&nbsp;</TD>
</TR>
<TR>
	<TD COLSPAN="3">&nbsp;</TD>
</TR>
<TR BGCOLOR="<%= cVLtYellow %>">
	<TD><B>Tracking Number:</B></TD>
	<TD <% if dctErrorList.Exists("account") then %>BGCOLOR="<%= cErrorBG %>"<% end if %>><B>Store:</B><% if blnCanEditAccount then %>&nbsp;&nbsp;&nbsp;(<B><A HREF="<%= STR_EDIT_SCRIPT %>&action=step0">Change</A></B>)<% end if %></TD>
	<TD><B>Shipping/Conf #:</B></TD>
</TR>
<TR>
	<TD>&nbsp;&nbsp;<%= STR_MERCHANT_TRACKING_PREFIX & rsInput("intID") %>&nbsp;&nbsp;&nbsp;</TD>
	<TD>&nbsp;&nbsp;<%= rsInput("S_vchShopperAccount") %>&nbsp;&nbsp;&nbsp;</TD>
	<TD>&nbsp;&nbsp;<%= rsInput("vchShippingNumber") %>&nbsp;&nbsp;&nbsp;</TD>
</TR>
</TABLE>
<BR>
<TABLE CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
<TR BGCOLOR="<%= cVLtYellow %>">
	<TD><B> VeraCore Status:</B></TD>
</TR>
<TR>
	<TD>&nbsp;&nbsp; 
    <% 
	Dim dctOrder
	set dctOrder = GetOrder(intID)
    If dctOrder("Unapproved")&""="true" Then
        Response.Write "[Unapproved] "
    End If
    If dctOrder("Unprocessed")&""="true" Then
        Response.Write "[Unprocessed] "
    End If
    If dctOrder("Accepted")&""="true" Then
        Response.Write "[Accepted] "
    End If
    If dctOrder("Hold")&""="true" Then
        Response.Write "[Hold] "
    End If
    If dctOrder("DeniedCredit")&""="true" Then
        Response.Write "[Denied Credit] "
    End If
    If dctOrder("Processed")&""="true" Then
        Response.Write "[Processed] "
    End If
    If dctOrder("Pending")&""="true" Then
        Response.Write "[Pending] "
    End If
    If dctOrder("Picked")&""="true" Then
        Response.Write "[Picked] "
    End If
    If dctOrder("Backordered")&""="true" Then
        Response.Write "[Backordered] "
    End If
    If dctOrder("Shipped")&""="true" Then
        Response.Write "[Shipped] "
    End If
    If dctOrder("Canceled")&""="true" Then
        Response.Write "[Canceled] "
    End If
    If dctOrder("Complete")&""="true" Then
        Response.Write "[Complete]"
    End If
   %>
    </TD>
</TR>
</TABLE>
<BR>
<TABLE CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
<TR BGCOLOR="<%= cVLtYellow %>">
	<TD><B> Shipping Tracking Number:</B></TD>
</TR>
<TR>
	<TD>&nbsp;&nbsp; 
    <%= iif(dctOrder("TrackingId")&""<>"", dctOrder("TrackingId"),"None Provided or Not Shipped Yet")%>
    </TD>
</TR>
</TABLE>
<BR />
<%
	dim blnCanViewBilling, blnCanViewShipping
	blnCanViewBilling = CheckUserAccess(ACC_ORDER_BILLING_VIEW)
	blnCanViewShipping = CheckUserAccess(ACC_ORDER_SHIPPING_VIEW)
	if blnCanViewBilling or blnCanViewShipping then
%>
<TABLE BORDER="0" CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
<TR>
	<TD VALIGN="top" WIDTH="50%" ALIGN="right">
<% if blnCanViewShipping then %>
	<TABLE CELLPADDING="0" CELLSPACING="0" WIDTH="100%">		<% 'iif(blnCanViewBilling, "90%", "100%") %>
	<TR BGCOLOR="<%= cVLtYellow %>">
		<TD <% if dctErrorList.Exists("shipping") then %>BGCOLOR="<%= cErrorBG %>"<% end if %>><B>Ship To:</B><% if blnCanEditShipping then %>&nbsp;&nbsp;&nbsp;(<B><A HREF="<%= STR_EDIT_SCRIPT %>&action=step2">Change</A></B>)<% end if %></TD>
	</TR>
	<TR>
		<TD>
<%
		if IsNull(rsInput("intShipShopperID")) or isNull(rsInput("intShopperID")) then
			response.write "&nbsp;&nbsp;no information"
		else
			strSQL = "SELECT * FROM " & STR_TABLE_SHOPPER & " WHERE intID=" & rsInput("intShipShopperID") & " AND chrStatus<>'D' and (chrType='S' OR chrType='B') and intShopperID=" & rsInput("intShopperID")
			set rsShopper = gobjConn.execute(strSQL)
			if rsShopper.eof then
				response.write "&nbsp;&nbsp;<B>System error</B>--unable to retrieve shopper information."
			else
				response.write "&nbsp;&nbsp;" & rsShopper("vchLastName") & ", " & rsShopper("vchFirstName") & "<BR>"
				if not IsNull(rsShopper("vchCompany")) then
					response.write "&nbsp;&nbsp;" & rsShopper("vchCompany") & "<BR>"
				end if
				if not IsNull(rsShopper("vchAddress1")) then
					response.write "&nbsp;&nbsp;" & rsShopper("vchAddress1") & "<BR>"
				end if
				if not IsNull(rsShopper("vchAddress2")) then
					response.write "&nbsp;&nbsp;" & rsShopper("vchAddress2") & "<BR>"
				end if
				response.write "&nbsp;&nbsp;" & rsShopper("vchCity") & ", " & rsShopper("vchState") & " " & rsShopper("vchZip") & "<BR>"
				if not IsNull(rsShopper("vchCountry")) then
					response.write "&nbsp;&nbsp;" & rsShopper("vchCountry") & "<BR>"
				end if
				if not IsNull(rsShopper("vchDayPhone")) then
					response.write "&nbsp;&nbsp;Day: " & rsShopper("vchDayPhone") & "<BR>"
				end if
				if not IsNull(rsShopper("vchNightPhone")) then
					response.write "&nbsp;&nbsp;Night: " & rsShopper("vchNightPhone") & "<BR>"
				end if
				if not IsNull(rsShopper("vchFax")) then
					response.write "&nbsp;&nbsp;Fax: " & rsShopper("vchFax") & "<BR>"
				end if
				if not IsNull(rsShopper("vchEmail")) then
					response.write "&nbsp;&nbsp;Email: " & rsShopper("vchEmail") & "<BR>"
				end if
                
			end if
			rsShopper.close
			set rsShopper = nothing
		end if
%>
		</TD>
	</TR>
	</TABLE>
<% else %>
	&nbsp;&nbsp;
<% end if %>
	</TD>
</TR>
</TABLE>
<BR>
<%
	end if
	if true then	' - kill gift message
%>
<TABLE CELLPADDING="0" CELLSPACING="0" WIDTH="100%">
<%
if not IsNull(rsInput("vchReferalName")) and false then
	response.write "<TR><TD>Referral: " & rsInput("vchReferalName") & "</TD></TR>"
end if
%>
<%
	if not IsNull(rsInput("vchCustomReferralName")) then
%>
<TR BGCOLOR="<%= cVLtYellow %>">
	<TD><B>Referred By:</B></TD>
</TR>
<tr><td><%=rsInput("vchCustomReferralName") & ""%>&nbsp;&nbsp;&nbsp;(<B><A HREF="<%= STR_EDIT_SCRIPT %>&action=refModify">Change</A></B>)</td>
</tr>
<%
	end if
%>
<TR BGCOLOR="<%= cVLtYellow %>">
	<TD <% if dctErrorList.Exists("giftmsg") then %>BGCOLOR="<%= cErrorBG %>"<% end if %>><B>Comments:</B><% if blnCanEditShipOption then %>&nbsp;&nbsp;&nbsp;(<B><A HREF="<%= STR_EDIT_SCRIPT %>&action=step5">Change</A></B>)<% end if %></TD>
</TR>

<TR>
	<TD><%= replace(Server.HTMLEncode(rsInput("txtGiftMessage") & ""), vbCR, "<BR>") %></TD>
</TR>
</TABLE>
<BR>
<%
	end if 
%>
<% if blnCanEditItems and not isNull(rsInput("intShopperID")) then %>(<B><A HREF="<%= STR_EDIT_LINES_SCRIPT %>">Edit Items</A></B>)<BR><% end if %>
<%
'	strSQL = "SELECT vchPartNumber, vchItemName, mnyUnitPrice, intQuantity, (mnyUnitPrice * intQuantity) AS mnyPrice FROM " & STR_TABLE_LINEITEM & " WHERE chrStatus<>'D' AND intOrderID=" & rsInput("intID") & " ORDER BY intSortOrder,intID"
'	set rsLineItem = gobjConn.execute(strSQL)
	set rsLineItem = GetOrderLineItems_OtherLt(intID)
	Merchant_DrawOrderCartLT rsInput, rsLineItem, false, "", "", "", iif(dctErrorList.Exists("items"), cErrorBG, ""), iif(dctErrorList.Exists("shipoption"), cErrorBG, "")
	rsLineItem.close
	set rsLineItem = nothing
	
	stlEndStdTable
end sub

sub DrawTitle(strPageTitle, dctMenuList)
	dim s, c, x, i, k
	x = dctMenuList.count - 1
	i = dctMenuList.items
	k = dctMenuList.keys
'	s = ""
	s = "&nbsp;<a href=""" & STR_SEARCH_SCRIPT & """><b>Back</b></a>"
	for c = 0 to x
		if c >= 0 then
			s = s & "&nbsp;|"
		end if
		s = s & "&nbsp;<A HREF=""" & STR_DETAIL_SCRIPT & "&action=" & k(c) & """><B>" & i(c) & "</B></A>"
	next
	DrawFormHeader "100%", strPageTitle, s
end sub

function DrawMenu( dctMenuList)
	dim s, c, x, i, k
	x = dctMenuList.count - 1
	i = dctMenuList.items
	k = dctMenuList.keys
'	s = ""
	for c = 0 to x
		if i(c) <> "System View" and i(c) <> "Recalc Order" then
		s = s & "&nbsp;<A HREF=""" & STR_DETAIL_SCRIPT & "&action=" & k(c) & """><B>(" & i(c) & ")</B></A>"
        end if
	next
    DrawMenu = s
end function

' -----------------------------------
' ====== PROCESS ORDER APPROVE ======
' -----------------------------------

sub ProcessOrderApprove(intID)
	gintOrderID = intID
	' process flow:
	'  (1) set order status to 'approved'
	'  (2) display/email invoice (unless declined or rejected)

	' SWSCodeChange - 7/20 - added merchant email
	dim strMerchantMessage, strSubject
	strMerchantMessage = "An order has been approved. Please review the order by visiting your merchant website." & vbcrlf
	strMerchantMessage = strMerchantMessage & vbcrlf
	strMerchantMessage = strMerchantMessage & "Order # "& STR_MERCHANT_TRACKING_PREFIX & gintOrderID & vbcrlf
	strMerchantMessage = strMerchantMessage & "Merchant Website: " & nonsecurebase & "admin/" & vbcrlf
	strMerchantMessage = strMerchantMessage & vbcrlf
	if lcase(strServerHost) = "awsdev" then
		AddStdEmailSecurity strMerchantMessage
	end if
	strSubject = "Order Notification # " & STR_MERCHANT_TRACKING_PREFIX & gintOrderID & ""
	SendMerchantEmail strSubject, strMerchantMessage		' SWSCodeChange - 7/20 - added merchant email

end sub
%>