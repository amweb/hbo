<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: order_emailinvoice.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Generate additional email invoices to the shopper.
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_ORDER_EMAILRECEIPT) then
	response.redirect "menu.asp"
end if

const STR_PAGE_TITLE = "Order Invoice"
const STR_PAGE_TYPE = "adminorderlist"
dim STR_DETAIL_SCRIPT

OpenConn

dim intID, strAction
strAction = Request("action")
intID = Request("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if

STR_DETAIL_SCRIPT = "order_detail.asp?id=" & intID & "&"

dim strStatusClause
strStatusClause = UserAccessStatus_BuildSQLWhere()
if strStatusClause = "" then
	response.redirect "order_list.asp"
end if
strSQL = "SELECT O.*, A.vchFirstName + ' ' + A.vchLastName AS A_vchShopperAccount, A.vchEmail AS A_vchEmail, B.vchEmail AS B_vchEmail, S.vchEmail AS S_vchEmail FROM ((" & STR_TABLE_ORDER & " AS O LEFT JOIN " & STR_TABLE_SHOPPER & " AS A ON O.intShopperID = A.intID) LEFT JOIN " & STR_TABLE_SHOPPER & " AS B ON O.intBillShopperID = B.intID) LEFT JOIN " & STR_TABLE_SHOPPER & " AS S ON O.intShipShopperID = S.intID WHERE O.intID=" & intID & " AND O.chrStatus<>'D' AND (A.chrType='A' OR A.chrType IS NULL)"
strSQL = strSQL & " AND O.chrStatus IN (" & strStatusClause & ")"
set rsData = gobjConn.execute(strSQL)
if rsData.eof then
	rsData.close
	set rsData = nothing
	response.redirect "order_list.asp"
end if

dim strEmail1, strEmail2, strEmail3
strEmail1 = rsData("A_vchEmail") & ""
strEmail2 = rsData("B_vchEmail") & ""
'strEmail3 = rsData("S_vchEmail") & ""

if strEmail1 = strEmail2 then
	strEmail2 = ""
end if
if (strEmail1 = strEmail3) or (strEmail2 = strEmail3) then
	strEmail3 = ""
end if

select case strAction
	case "submit":
		' recalc order
		SendInvoiceMail rsData, Request("strMessage"), strEmail1, strEmail2, strEmail3
		rsData.close
		set rsData = nothing
		response.redirect STR_DETAIL_SCRIPT
end select

dim strSQL, rsData
if intID <= 0 then
	response.redirect "order_list.asp"
end if


DrawPage rsData


rsData.close
set rsData = nothing

response.end

sub DrawPage(rsInput)
	response.write "<FORM ACTION=""order_emailinvoice.asp"" METHOD=""POST"">"
	HiddenInput "action", "submit"
	HiddenInput "id", intID
	DrawFormHeader "100%", "Invoice Receipt", "<A HREF=""" & STR_DETAIL_SCRIPT & """>Cancel</A>"
	response.write "A copy of the following invoice will be sent by email to these addresses:<BR>"
	
	if strEmail1 <> "" then
		response.write "<LI>" & strEmail1 & " (account)</LI>"
	end if
	if strEmail2 <> "" then
		response.write "<LI>" & strEmail2 & " (billing)</LI>"
	end if
	if strEmail3 <> "" then
		response.write "<LI>" & strEmail3 & " (shipping)</LI>"
	end if
	response.write "<BR><BR>"
	
	response.write "Enter any message text that you would like include at the top of this email:<BR>"
	response.write "<TEXTAREA ROWS=4 COLS=40 NAME=""strMessage""></TEXTAREA><BR>"
	response.write "<INPUT TYPE=""submit"" VALUE=""Send Email Invoice"">"
	response.write "</FORM>"
	response.write "<HR>"
	
	response.write GetInvoiceHTML(rsInput, "Message typed in box above will be presented in this spot.")
end sub
%>