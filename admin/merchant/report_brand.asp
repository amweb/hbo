<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: report_tax.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2000 SacWeb, Inc. All rights reserved.
' = Description:
' =   Generate report by tax zone
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_REPORT_VIEW) then
	response.redirect "menu.asp"
end if

dim strPageType, strPageTitle

strPageType = "adminrepsummary"
strPageTitle = "Order Report By Brand"

OpenConn

dim strSQL, rsData, dtmStart, dtmEnd, intRptType, strSQLColumn
GetReportDateRangeX dtmStart, dtmEnd, intRptType

strSQLColumn = GetSQLColumnName
'response.End

strSQL = "SELECT"
strSQL = strSQL & " B.intID, B.vchItemName,"
strSQL = strSQL & " COUNT(DISTINCT O.intID) AS intOrderCount,"
strSQL = strSQL & " COUNT(DISTINCT L.intInvID) AS intLineCount,"
strSQL = strSQL & " SUM(L.intQuantity) AS intInvCount,"
strSQL = strSQL & " SUM(L.intQuantity * L.mnyUnitPrice) AS mnyTotalAmount"
strSQL = strSQL & " FROM " & STR_TABLE_ORDER & " AS O"
strSQL = strSQL & " INNER JOIN " & STR_TABLE_LINEITEM & " AS L"
strSQL = strSQL & "  ON O.intID = L.intOrderID"
strSQL = strSQL & "  AND L.chrStatus <> 'D'"
strSQL = strSQL & "  AND L.mnyUnitPrice > 0"
strSQL = strSQL & " INNER JOIN " & STR_TABLE_INVENTORY & " AS I"
strSQL = strSQL & "  ON L.intInvID = I.intID"
strSQL = strSQL & "  AND I.chrStatus <> 'D'"
strSQL = strSQL & " INNER JOIN " & STR_TABLE_INVENTORY & " AS B"
strSQL = strSQL & "  ON I.intBrand = B.intID"
strSQL = strSQL & "  AND B.chrStatus <> 'D'"
strSQL = strSQL & " WHERE O.chrStatus NOT IN ('D')"	' only show shipped or deposited orders
'strSQL = strSQL & strSQLColumn
strSQL = strSQL & " GROUP BY B.intID, B.vchItemName"
'response.write strSQL
set rsData = gobjConn.execute(strSQL)


DrawPage


rsData.close
set rsData = nothing
response.end

sub DrawPage
	call DrawHeaderUpdate()
%>
<center>
<%= font(2) %><b><%= STR_SITE_NAME %><br />
<%= strPageTitle %><br />
<%= GetDateRangeType() %> </B></FONT><BR>
<%= font(1) %><br />
<br />
<table border="0" cellpadding="2" cellspacing="2">
<tr>
	<td>&nbsp;</td>
	<td align="center"><b># Products</b></td>
	<td align="center"><b># Items</b></td>
	<td align="center"><b># Orders</b></td>
	<td align="center"><b>Total Amount</b></td>
</tr>
<%
	while not rsData.eof
%>
<tr>
	<td><%= rsData("vchItemName") %></td>
	<td align="center"><%= rsData("intLineCount") %></td>
	<td align="center"><%= rsData("intInvCount") %></td>
	<td align="center"><%= rsData("intOrderCount") %></td>
	<td align="center"><%= SafeFormatCurrency("$0.00", rsData("mnyTotalAmount"), 2) %></td>
</tr>
<%
		rsData.MoveNext
	wend
%>
</table>
</font>
</center>
<%
end sub
%>