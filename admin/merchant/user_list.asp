<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: user_list.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Merchant User Admin
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

const STR_PAGE_TITLE = "Administrative Users"
const STR_PAGE_TYPE = "adminuserlist"
const STR_DETAIL_SCRIPT = "user_detail.asp?"

OpenConn

dim strSQL, rsData

strSQL = "SELECT intID, chrStatus, vchUsername, vchFullName, dtmLastLogin"
strSQL = strSQL & " FROM " & STR_TABLE_USER
strSQL = strSQL & " WHERE chrStatus<>'D'"
strSQL = strSQL & " ORDER BY vchUsername"
set rsData = gobjConn.execute(strSQL)


DrawPage


rsData.close
set rsData = nothing
response.end

sub DrawPage
call DrawHeaderUpdate()
	DrawFormHeader "100%", "Administrative Users", "<A HREF=""" & STR_DETAIL_SCRIPT & """>Add User...</A>"
	DrawListHeader
	while not rsData.eof
		DrawListItem rsData("intID"), rsData("vchFullName"), rsData("vchUsername"), rsData("dtmLastLogin"), rsData("chrStatus")
		rsData.MoveNext
		if not rsData.eof then
			DrawListItemSep
		end if
	wend
	DrawListFooter
end sub

sub DrawListHeader
%>
<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVLtGrey) %>">
<TR>
	<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cWhite %>">
	<TR>
		<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%">
		<TR BGCOLOR="<%= cVLtYellow %>">
			<TD><%= font(1) %>&nbsp;</TD>
			<TD NOWRAP><%= font(1) %><B>Full Name</B></TD>
			<TD NOWRAP><%= font(1) %><B>&nbsp;Username&nbsp;</B></TD>
			<TD NOWRAP><%= font(1) %><B>&nbsp;Last Login&nbsp;</B></TD>
			<TD NOWRAP><%= font(1) %><B>&nbsp;Status&nbsp;</B></TD>
		</TR>
<%
end sub

sub DrawListItem(intID, strFullName, strUsername, dtmLastLogin, strStatus)
%>
		<TR VALIGN=BOTTOM>
			<TD ALIGN=CENTER><A HREF="<%= STR_DETAIL_SCRIPT %>id=<%= intID %>"><IMG SRC="<%= imagebase %>icon_user.gif" WIDTH="16" HEIGHT="16" ALT="Edit This User" BORDER="0"></A></TD>
			<TD NOWRAP><A HREF="<%= STR_DETAIL_SCRIPT %>id=<%= intID %>"><%= strFullName %></A></TD>
			<TD NOWRAP>&nbsp;<%= strUsername %>&nbsp;</TD>
			<TD NOWRAP>&nbsp;<%= dtmLastLogin %>&nbsp;</TD>
			<TD NOWRAP>&nbsp;<%= GetStatusStr(strStatus, true) %></TD>
		</TR>
<%
end sub

sub DrawListItemSep
%>
		<TR>
			<TD COLSPAN=5><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVVLtGrey) %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
		</TR>
<%
end sub

sub DrawListFooter
%>
		</TABLE></TD>
	</TR>
	</TABLE></TD>
</TR>
</TABLE>
<%
end sub
%>