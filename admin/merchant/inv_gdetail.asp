<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: inv_fdetail.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Inventory admin - details on folders
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   (1) SWSCodeChange - fixed error on NULL chrParentStatus on creating new record
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_INVENTORY_VIEW) then
	response.redirect "main.asp"
end if

dim blnCanEdit
blnCanEdit = CheckUserAccess(ACC_INVENTORY_EDIT)

const STR_PAGE_TITLE = "Category Details"
const STR_PAGE_TYPE = "admininvlist"

OpenConn

dim dctShoppers
dim strSQL, rsRequestData, objUpload
Set objUpload = Server.CreateObject("SoftArtisans.FileUp")
Set rsRequestData = objUpload.Form

dim intID, intParentID, strAction, returnURL
strAction = RequestPoll("action")
intID = RequestPoll("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if
returnURL = RequestPoll("return")
if returnURL = "" then
	returnURL = "inv_glist.asp" 
end if

intParentID = RequestPoll("parentid")
if IsNumeric(intParentID) then
	intParentID = CLng(intParentID)
else
	intParentID = 0
end if

if strAction = "delete" then
	if blnCanEdit then
		gobjConn.execute("UPDATE " & STR_TABLE_INVENTORY & " SET chrStatus='D' WHERE intID=" & intID)
		' SWSCodeChange - 04aug2000 added call to ResyncInvParentStatus
		'ResyncInvParentStatus intID
	end if
	response.redirect returnURL	'"inv_blist.asp"
elseif strAction = "setstatus" then
	if blnCanEdit then
		gobjConn.execute("UPDATE " & STR_TABLE_INVENTORY & " SET chrStatus='" & RequestPoll("status") & "' WHERE intID=" & intID)
		' SWSCodeChange - 04aug2000 added call to ResyncInvParentStatus
		'ResyncInvParentStatus intID
	end if
	response.redirect returnURL	'"inv_blist.asp"
elseif strAction = "submit" then
	if blnCanEdit then
		AutoCheck rsRequestData, ""
		if RequestPoll("chrSoftFlag") = "Y" then
			CheckField "vchSoftURL", "IsEmpty", rsRequestData, "Missing Download URL"
		end if
		if FormErrors.count = 0 then
			dim rsTemp, intNewOrder
			strSQL = "SELECT ISNULL(MAX(intSortOrder),0) AS intMaxSortOrder FROM " & STR_TABLE_INVENTORY
			set rsTemp = gobjConn.execute(strSQL)
			intNewOrder = rsTemp("intMaxSortOrder") + 1
			rsTemp.close
			set rsTemp = nothing
			dim dctSaveList
			set dctSaveList = Server.CreateObject("Scripting.Dictionary")
				dctSaveList.Add "*@dtmCreated", "GETDATE()"
				dctSaveList.Add "@dtmUpdated", "GETDATE()"
				dctSaveList.Add "*vchCreatedByUser", Session(SESSION_MERCHANT_ULOGIN)
				dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
				dctSaveList.Add "*vchCreatedByIP", gstrUserIP
				dctSaveList.Add "vchUpdatedByIP", gstrUserIP
				dctSaveList.Add "chrType", "G"
				dctSaveList.Add "*chrStatus", "A"
				dctSaveList.Add "*#intParentID", intParentID
				dctSaveList.Add "*#intSortOrder", intNewOrder
				dctSaveList.Add "*#intHitCount", 0
				dctSaveList.Add "!vchItemName", ""
				dctSaveList.Add "#mnyItemPrice", "0"
				dctSaveList.Add "chrICFlag", "N"
				dctSaveList.Add "chrTaxFlag", "N"
				dctSaveList.Add "chrSoftFlag", "N"
'				dctSaveList.Add "!chrSpecial", ""
'				dctSaveList.Add "#intNodeID", -1
				dctSaveList.Add "!#intStock", ""
' Begin SWSCodeChange (1) - 13oct2000
'				dctSaveList.Add "*chrParentStatus", "D"
				dctSaveList.Add "!txtDescription", ""
			SaveDataRecord STR_TABLE_INVENTORY, rsRequestData, intID, dctSaveList
			'if intID = 0 then
			'	ResyncInvParentStatus intID
			'end if
			dim fImage, uploadPath, IsImageGood
			IsImageGood = false
			If IsObject(objUpload.Form("ImageFile")) then
				if (objUpload.Form("ImageFile").TotalBytes <> 0) and (lcase(right(objUpload.Form("ImageFile").UserFilename, 4))=".jpg" or lcase(right(objUpload.Form("ImageFile").UserFilename, 4))=".gif") then
					fImage = RemoveSpaces("Brand" & intID & "." & Mid(objUpload.form("ImageFile").UserFilename, InstrRev(objUpload.form("ImageFile").UserFilename, "\") + 1))
					uploadPath = Server.MapPath(brandimagebase) & "\"
					objUpload.Form("ImageFile").SaveAs uploadPath & fImage
					IsImageGood = SetImageReSize( uploadPath, fImage)	' true
				end if
			end if
			if IsImageGood then
				dctSaveList.RemoveAll
				dctSaveList.Add "*@dtmCreated", "GETDATE()"
				dctSaveList.Add "vchImageURL", fImage
				SaveDataRecord STR_TABLE_INVENTORY, rsRequestData, intID, dctSaveList
			end if
' End SWSCodeChange (1)
			response.redirect "inv_glist.asp"
		else
			
			DrawPage rsRequestData
			
		end if
	else
		response.redirect returnURL	'"inv_blist.asp"
	end if
else
	dim rsData, blnDataIsRS
	if intID > 0 then
		strSQL = "SELECT * FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intID & " AND chrStatus<>'D'"
		set rsData = gobjConn.execute(strSQL)
		blnDataIsRS = true
		if rsData.eof then
			response.redirect returnURL	'"inv_blist.asp"
		end if
		intParentID = rsData("intParentID")
	else
		set rsData = rsRequestData
		blnDataIsRS = false
	end if
	
	
	DrawPage rsData
	
	
	if blnDataIsRS then
		rsData.close
		set rsData = nothing
	end if
end if
response.end

sub DrawPage(rsInput)
call DrawHeaderUpdate()
	dim strTitle, strInfoText
	if intID > 0 then
		strTitle = "Edit Category - " & rsInput("vchItemName")
	else
		strTitle = "Add New Category"
	end if
%>
<FORM ACTION="inv_gdetail.asp" METHOD="POST" NAME="frm" ENCTYPE="multipart/form-data">
<INPUT TYPE="hidden" NAME="action" VALUE="submit" />
<INPUT TYPE="hidden" NAME="id" VALUE="<%= intID %>" />
<INPUT TYPE="hidden" NAME="parentid" VALUE="<%= intParentID %>" />
<%
	if blnCanEdit then		'"inv_list.asp?parentid=" & intParentID
		AddPageAction "Cancel", returnURL, ""
		if intID > 0 then
			if rsInput("chrStatus") <> "A" then
				AddPageAction "Activate", "inv_gdetail.asp?action=setstatus&status=A&id=" & intID & "&parentid=" & intParentID, "Are you sure you want to activate this item?"
			end if
			if rsInput("chrStatus") <> "I" then
				AddPageAction "Deactivate", "inv_gdetail.asp?action=setstatus&status=I&id=" & intID & "&parentid=" & intParentID, "Are you sure you want to deactivate this item?"
			end if
			AddPageAction "Delete", "inv_gdetail.asp?action=delete&id=" & intID & "&intParentID=" & intParentID, "Are you sure you want to delete this item?"
		end if
	else
		AddPageAction "Return", "inv_list.asp?parentid=" & intParentID, ""
	end if
	
	if intID > 0 then
		strInfoText = "<B>Record ID:</B> " & rsInput("intParentID") & "-" & rsInput("intID")
		strInfoText = strInfoText & "&nbsp; <B>Created:</B> " & rsInput("dtmCreated")
		strInfoText = strInfoText & "&nbsp; <B>Updated:</B> " & rsInput("dtmUpdated")
	else
		strInfoText = ""
	end if
	
	DrawFormHeader "100%", strTitle, strInfoText
	
	stlBeginStdTable "100%"
	response.write "<tr><td>"
	
	if blnCanEdit then
		stlBeginFormSection "100%", 4
		stlTextInput "vchItemName", 20, 32, rsInput, "Category \Name", "IsEmpty", "Missing Program Name"
'		stlCheckbox "chrSpecial", "Y", rsInput, "\Special", "yes"
		stlEndFormSection
		
		stlRule

		stlBeginFormSection "100%", 2
		if trim(rsInput("vchImageURL")&"") <> "" then
			stlRawCell "Current Image:"
			stlRawCell "<IMG SRC=""" & brandimagebase & rsInput("vchImageURL") & """ width=""50"" height=""50"" ALT="""" border=""0"" />"
		end if
		
		stlTextInput "vchImageURL", 30, 255, rsInput, "Existing \Image", "", ""
		'stlFileInput "ImageFile", 30, 255, rsRequestData("ImageFile"), "or \Upload Image", "", ""
		stlFileInput "ImageFile", 30, 255, "", "or \Upload Image", "", ""

		stlEndFormSection
		
		stlRule
		
		response.write "<U>D</U>escription:<BR /><BR />"
		response.write "<TEXTAREA NAME=""txtDescription"" ROWS=6 COLS=50 ACCESSKEY=""D"" CLASS=""textarea"">" & Server.HTMLEncode(rsInput("txtDescription") & "") & "</TEXTAREA>"
	else
		stlBeginFormSection "100%", 2
		stlStaticText "Brand Name", rsInput("vchItemName")
'		stlStaticText "Special:", iif(rsInput("chrSpecial") = "Y", "Yes", "No")
		stlEndFormSection

		stlRule
		
		stlBeginFormSection "", 2
		if not IsNull(rsInput("vchImageURL")) then
			stlRawCell "Current Image:"
			stlRawCell "<IMG SRC=""" & brand & rsInput("vchImageURL") & """ width=""50"" height=""50"" ALT="""" border=""0"" /><BR />" & rsInput("vchImageURL")
		else
			stlStaticText "Current Image:", "None"
		end if
		stlEndFormSection

		stlRule
		
		stlStaticText "Description", rsInput("txtDescription")
	end if
	response.write "</td></tr>"
	stlSubmit iif(blnCanEdit, "Submit", "Return")
	
	stlEndStdTable
	response.write "</FORM>"
	
	if blnCanEdit then
		SetFieldFocus "frm", "vchItemName"
	end if
end sub

function RequestPoll (strField)
	Dim strValue
	strValue = rsRequestData(strField)
	If strValue = "" Then
		strValue = Request.QueryString(strField)
	end if
	RequestPoll = strValue
end function

' Added RFKCodeChange (1) - 8oct2002

Function IsValid(TheString)
	If IsNull(TheString) Or IsEmpty(TheString) Or TheString = "" Or TheString = " " Then
		IsValid = False
	Else
		If IsNumeric(TheString) Then
			IsValid = iif(TheString > 0, True, False)
		Else
			IsValid = True
		End if
	End If
End Function

' ========== RenoveSpaces ==========
function RemoveSpaces(strInput)
	dim strOut,thischar,i
	strOut = ""
	for i = 1 to len(strInput)
		thischar = mid(strInput,i,1)
		select case thischar
			case " ", ",", "'", """", "(", ")", "{", "}", "[", "]", "|"
			' remove Whitespace characters
			case "!", "@", "#", "$", "%", "^", "*", "+", ":", ";", "<", ">", "?"
			' remove DOS reserved characters
			case else
				StrOut = StrOut & thischar
		end select
	next
	RemoveSpaces = StrOut
end function

function SetImageReSize( uploadPath, fImage)

'dim fImage, uploadPath, IsImageGood
'IsImageGood = false
'fImage = RemoveSpaces("Brand" & intID & "." & Mid(objUpload.form("ImageFile").UserFilename, InstrRev(objUpload.form("ImageFile").UserFilename, "\") + 1))
'uploadPath = Server.MapPath(brandimagebase) & "\"

if IsNull(fImage) or fImage = "" then
	SetImageReSize = false
else
	' resize image here
	dim objExec, dnloadPath1, dnloadPath2, strExecResult1, strExecResult2
	Set objExec = Server.CreateObject("ASPExec.Execute")

	' 1st pass resize category image
	' dnloadPath1 = Server.MapPath(categoryimagebase) & "\"
	objExec.Application = "c:\com\jdo.exe -ss -q 75 -rh 100 -rw 100 -rb" & uploadpath & fImage
	objExec.Parameters = ""
	strExecResult1 = objExec.ExecuteDosApp

	' IsImageGood = true
	set objExec = Nothing
	SetImageReSize = true
end if

end function

%>