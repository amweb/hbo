<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: report_city.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Generate report by city
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_REPORT_VIEW) then
	response.redirect "menu.asp"
end if

dim strPageType, strPageTitle

strPageType = "adminrepsummary"
strPageTitle = "Order Report By City/State"

OpenConn

dim strSQL, rsData, dtmStart, dtmEnd, intRptType, strSQLColumn
GetReportDateRangeX dtmStart, dtmEnd, intRptType

strSQLColumn = GetSQLColumnName
'response.End



DrawPageHeader

strSQL = "SELECT"
strSQL = stRSQL & " COUNT(DISTINCT O.intID) AS intOrderCount,"
strSQL = strSQL & " SUM(O.mnyGrandTotal) AS mnyGrandTotal,"
strSQL = strSQL & " SUM(O.mnyTaxAmount) AS mnyTaxAmount,"
strSQL = strSQL & " SUM(O.mnyShipAmount) AS mnyShipAmount,"
strSQL = strSQL & " B.vchState AS vchBillState,"
strSQL = strSQL & " B.vchCity AS vchBillCity"
strSQL = strSQL & " FROM " & STR_TABLE_ORDER & " AS O, " & STR_TABLE_SHOPPER & " AS B"
strSQL = strSQL & " WHERE O.chrStatus NOT IN ('D')"	' only show shipped or deposited orders
strSQL = strSQL & " AND O.intBillShopperID = B.intID"
'strSQL = strSQL & strSQLColumn
strSQL = strSQL & " GROUP BY B.vchState, B.vchCity"
strSQL = strSQL & " ORDER BY B.vchState, B.vchCity"

set rsData = gobjConn.execute(strSQL)
DrawSummary
rsData.close
set rsData = nothing

strSQL = "SELECT O.intID, "
strSQL = stRSQL & " O.mnyGrandTotal,"
strSQL = strSQL & " O.mnyTaxAmount,"
strSQL = strSQL & " O.mnyShipAmount,"
strSQL = strSQL & " B.vchState AS B_vchState,"
strSQL = strSQL & " B.vchCity AS B_vchCity"
strSQL = strSQL & " FROM " & STR_TABLE_ORDER & " AS O, " & STR_TABLE_SHOPPER & " AS B"
strSQL = strSQL & " WHERE O.chrStatus IN ('H','X')"	' only show shipped or deposited orders
strSQL = strSQL & " AND O.intBillShopperID = B.intID"
strSQL = strSQL & strSQLColumn
'strSQL = strSQL & " GROUP BY B.vchState, B.vchCity"
strSQL = strSQL & " ORDER BY B.vchState, B.vchCity"
set rsData = gobjConn.execute(strSQL)

rsData.close
set rsData = nothing


response.end

sub DrawPageHeader
call DrawHeaderUpdate()
	DrawFormHeader "100%", strPageTitle, GetDateRangeType()
end sub

sub DrawReportSectionTitle(strTitle)
	response.write "<BR><FONT SIZE=""2""><B>" & strTitle & "</B></FONT><BR>"
end sub

sub DrawSummary
	DrawReportSectionTitle "Summary"
%>
<TABLE BORDER="0" CELLPADDING="2" CELLSPACING="2">
<TR>
	<TD>&nbsp;</TD>
	<TD ALIGN="center"><B># Orders</B></TD>
	<TD ALIGN="center" COLSPAN="2"><B>Amount</B></TD>
	<TD ALIGN="center" COLSPAN="2"><B>Tax</B></TD>
	<TD ALIGN="center" COLSPAN="2"><B>Shipping</B></TD>
</TR>
<TR>
	<TD>&nbsp;</TD>
	<TD ALIGN="center">&nbsp;</TD>
	<TD ALIGN="center"><B>Total</B></TD>
	<TD ALIGN="center"><B>Avg</B></TD>
	<TD ALIGN="center"><B>Total</B></TD>
	<TD ALIGN="center"><B>Avg</B></TD>
	<TD ALIGN="center"><B>Total</B></TD>
	<TD ALIGN="center"><B>Avg</B></TD>
</TR>
<%
	dim strState, intStateOrderCount, mnyStateGrandTotal, mnyStateTaxAmount, mnyStateShipAmount
	strState = ""
	while not rsData.eof
		if strState <> rsData("vchBillState") then
			if strState <> "" then
%>
<TR>
	<TD><B>&nbsp;&nbsp;Total for <%= strState %>:</B></TD>
	<TD ALIGN="center"><B><%= intStateOrderCount %></B></TD>
	<TD ALIGN="center"><B><%= SafeFormatCurrency("$0.00", mnyStateGrandTotal, 2) %></B></TD>
	<TD ALIGN="center"><B><%= SafeFormatCurrency("$0.00", mnyStateGrandTotal / intStateOrderCount, 2) %></B></TD>
	<TD ALIGN="center"><B><%= SafeFormatCurrency("$0.00", mnyStateTaxAmount, 2) %></B></TD>
	<TD ALIGN="center"><B><%= SafeFormatCurrency("$0.00", mnyStateTaxAmount / intStateOrderCount, 2) %></B></TD>
	<TD ALIGN="center"><B><%= SafeFormatCurrency("$0.00", mnyStateShipAmount, 2) %></B></TD>
	<TD ALIGN="center"><B><%= SafeFormatCurrency("$0.00", mnyStateShipAmount / intStateOrderCount, 2) %></B></TD>
</TR>
<%
			end if
			strState = rsData("vchBillState")
			intStateOrderCount = 0
			mnyStateGrandTotal = 0
			mnyStateTaxAmount = 0
			mnyStateShipAmount = 0
%>
<TR>
	<TD><B>State: <%= strState %>:</B></TD>
	<TD ALIGN="center"><%= rsData("intOrderCount") %></TD>
	<TD ALIGN="center">&nbsp;</TD>
	<TD ALIGN="center">&nbsp;</TD>
	<TD ALIGN="center">&nbsp;</TD>
	<TD ALIGN="center">&nbsp;</TD>
	<TD ALIGN="center">&nbsp;</TD>
	<TD ALIGN="center">&nbsp;</TD>
</TR>
<%
		end if
		intStateOrderCount = intStateOrderCount + rsData("intOrderCount")
		mnyStateGrandTotal = mnyStateGrandTotal + rsData("mnyGrandTotal")
		mnyStateTaxAmount = mnyStateTaxAmount + rsData("mnyTaxAmount")
		mnyStateShipAmount = mnyStateShipAmount + rsData("mnyShipAmount")
%>
<TR>
	<TD>&nbsp;&nbsp;City: <%= rsData("vchBillCity") %></TD>
	<TD ALIGN="center"><%= rsData("intOrderCount") %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData("mnyGrandTotal"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData("mnyGrandTotal") / rsData("intOrderCount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData("mnyTaxAmount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData("mnyTaxAmount") / rsData("intOrderCount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData("mnyShipAmount"), 2) %></TD>
	<TD ALIGN="center"><%= SafeFormatCurrency("$0.00", rsData("mnyShipAmount") / rsData("intOrderCount"), 2) %></TD>
</TR>
<%
		rsData.MoveNext
	wend
	if strState <> "" then
%>
<TR>
	<TD><B>&nbsp;&nbsp;Total for <%= strState %>:</B></TD>
	<TD ALIGN="center"><B><%= intStateOrderCount %></B></TD>
	<TD ALIGN="center"><B><%= SafeFormatCurrency("$0.00", mnyStateGrandTotal, 2) %></B></TD>
	<TD ALIGN="center"><B><%= SafeFormatCurrency("$0.00", mnyStateGrandTotal / intStateOrderCount, 2) %></B></TD>
	<TD ALIGN="center"><B><%= SafeFormatCurrency("$0.00", mnyStateTaxAmount, 2) %></B></TD>
	<TD ALIGN="center"><B><%= SafeFormatCurrency("$0.00", mnyStateTaxAmount / intStateOrderCount, 2) %></B></TD>
	<TD ALIGN="center"><B><%= SafeFormatCurrency("$0.00", mnyStateShipAmount, 2) %></B></TD>
	<TD ALIGN="center"><B><%= SafeFormatCurrency("$0.00", mnyStateShipAmount / intStateOrderCount, 2) %></B></TD>
</TR>
<%
	end if
%>
</TABLE>
<%
end sub
%>