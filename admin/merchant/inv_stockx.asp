<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: inv_stock.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Inventory stock-range report and flat-inventory listing.
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_INVENTORY_VIEW) then
	response.redirect "main.asp"
end if

dim blnCanEdit
blnCanEdit = CheckUserAccess(ACC_INVENTORY_EDIT)

dim strPageTitle

const STR_PAGE_TYPE = "admininvlist"

const STR_LIST_SCRIPT = "inv_stock.asp?all=1&"
const STR_STOCK_SCRIPT = "inv_stock.asp?"
const STR_DETAIL_FOLDER_SCRIPT = "inv_fdetail.asp?"
const STR_DETAIL_ITEM_SCRIPT = "inv_idetail.asp?"
const STR_STOCK_LOW_SCRIPT = "inv_stock.asp?"
const STR_STOCK_HIGH_SCRIPT = "inv_stock.asp?"

OpenConn

dim tfShowAll
tfShowAll = (Request.QueryString("all").count > 0)

if tfShowAll then
	strPageTitle = "Flat Inventory Listing"
else
	strPageTitle = "Low/High Stock Quantities"
end if

dim strSQL, rsData

' gather inventory statistics
dim intLowCount, intHighCount, intInvCount
strSQL = "SELECT "
strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_INVENTORY & " WHERE chrStatus<>'D') AS intCount,"
strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_INVENTORY & " WHERE chrStatus<>'D' AND intStock < intLowStock) AS intLowCount,"
strSQL = strSQL & "(SELECT COUNT(*) FROM " & STR_TABLE_INVENTORY & " WHERE chrStatus<>'D' AND intStock > intHighStock) AS intHighCount"
set rsData = gobjConn.execute(strSQL)
intInvCount = rsData("intCount")
intLowCount = rsData("intLowCount")
intHighCount = rsData("intHighCount")
rsData.close
set rsData = nothing

' get folder list

strSQL = "SELECT intID, chrType, chrStatus, vchPartNumber, vchItemName, intLowStock, intHighStock, intStock"
strSQL = strSQL & " FROM " & STR_TABLE_INVENTORY
strSQL = strSQL & " WHERE chrStatus<>'D' AND chrType='I'"
if tfShowAll = false then
	strSQL = strSQL & " AND (intStock < intLowStock OR intStock > intHighStock)"
end if
strSQL = strSQL & " ORDER BY vchPartNumber,vchItemName"
set rsData = gobjConn.execute(strSQL)

DrawPage

rsData.close
set rsData = nothing
response.end

sub DrawPage
	dim c, strPrevType, blnCanMoveUp, blnCanMoveDown
	dim intID, strStatus, strPartNumber, strName, intMinStock, intMaxStock, intStock
	
	call DrawHeaderUpdate()
	
	DrawListTitle
	'DrawListNavigation
	DrawListHeader
	while not rsData.eof
		intID = rsData("intID")
		strStatus = rsData("chrStatus")
		strPartNumber = rsData("vchPartNumber")
		strName = rsData("vchItemName")
		intMinStock = rsData("intLowStock")
		intMaxStock = rsData("intHighStock")
		intStock = rsData("intStock")
		rsData.MoveNext
		DrawListItem intID, strStatus, strName, strPartNumber, intMinStock, intMaxStock, intStock
		if not rsData.eof then
			DrawListItemSep
		end if
	wend
	DrawListFooter
end sub

sub DrawListTitle
%>
<TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="<%= INT_PAGE_WIDTH %>">
<TR>
	<TD><%= font(5) %><%= strPageTitle %></TD>
<% if not blnPrintMode then %>
	<TD ALIGN=RIGHT><%= font(1) %>
	<% ' page actions %>
	<% if tfShowAll then %>
	<A HREF="<%= STR_STOCK_SCRIPT %>">Show Out of Stock Range</A>
	<% else %>
	<A HREF="<%= STR_LIST_SCRIPT %>">Show All Items<IMG SRC="<%= imagebase %>icon_product.gif" WIDTH="16" HEIGHT="16" ALT="" BORDER="0" ALIGN=ABSMIDDLE HSPACE=5></A>
	<% end if %>
	</TD>
<% end if %>
</TR>
</TABLE>
<%= spacer(1,5) %><BR>
<% stlBeginInfoBar INT_PAGE_WIDTH %>
	<B>Total Inventory:</B> <%= intInvCount %>&nbsp;
	<B><A HREF="<%= STR_STOCK_LOW_SCRIPT %>">Low Stock:</A></B> <%= intLowCount %>&nbsp;
	<B><A HREF="<%= STR_STOCK_HIGH_SCRIPT %>">High Stock:</A></B> <%= intHighCount %>&nbsp;
	</FONT></TD>
	<TD ALIGN=RIGHT><%= fontx(1,1,cWhite) & FormatDateTime(date,1) %>
<% stlEndInfoBar %>
<%= spacer(1,10) %>
<%
end sub

sub DrawListNavigation
	' dctNavList - reversed order, key=ID, item=name
	dim c, x, i, k
	x = dctNavList.count - 1
	i = dctNavList.items
	k = dctNavList.keys
	response.write "<TABLE BORDER=0 CELLPADDING=6 CELLSPACING=0 WIDTH=""" & INT_PAGE_WIDTH & """>"
	response.write "<TR><TD>" & font(1)
	for c = x to 0 step -1
		if c > 0 then
			response.write "<B><A HREF=""" & STR_LIST_SCRIPT & "parentid=" & k(c) & """>" & i(c) & "</A></B> : "
		else
			response.write i(c)
		end if
	next
	response.write "</TD>"
	if x > 0 then
		response.write "<TD ALIGN=RIGHT>" & font(1) & "<A HREF=""" & STR_LIST_SCRIPT & "parentid=" & k(1) & """>To Previous Folder<IMG SRC=""" & imagebase & "icon_folderup.gif"" WIDTH=""16"" HEIGHT=""16"" ALT=""To Previous Folder"" BORDER=""0"" ALIGN=""absmiddle"" HSPACE=""5""></A></TD>"
	end if
	response.write "</TR></TABLE>"
end sub

sub BuildNavList(byVal intParentID)
	dim strSQL, rsData
	set dctNavList = Server.CreateObject("Scripting.Dictionary")
	while intParentID <> 0
		strSQL = "SELECT intParentID, vchItemName FROM " & STR_TABLE_INVENTORY & " WHERE intID=" & intParentID & " AND chrStatus<>'D'"
		set rsData = gobjConn.execute(strSQL)
		dctNavList.Add intParentID, rsData("vchItemName") & ""
		intParentID = rsData("intParentID")
		rsData.close
		set rsData = nothing
	wend
	dctNavList.Add 0, "Top"
end sub

sub DrawListHeader
%>
<TABLE BORDER=0 CELLPADDING=1 CELLSPACING=0 WIDTH="<%= INT_PAGE_WIDTH %>" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVLtGrey) %>">
<TR>
	<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= cWhite %>">
	<TR>
		<TD><TABLE BORDER=0 CELLPADDING=2 CELLSPACING=0 WIDTH="100%">
		<TR BGCOLOR="<%= cVLtYellow %>">
			<TD><%= font(1) %>&nbsp;</TD>
			<TD NOWRAP><B>Name</B></TD>
			<TD NOWRAP><B>&nbsp;Part #&nbsp;</B></TD>
			<TD NOWRAP ALIGN="center"><B>&nbsp;Preferred&nbsp;Stock</B></TD>
			<TD NOWRAP ALIGN="center"><B>&nbsp;In&nbsp;Stock</B></TD>
			<TD NOWRAP ALIGN="right"><B>Status&nbsp;</B></TD>
<% if not blnPrintMode then %>
			<TD>&nbsp;</TD>
<% end if %>
		</TR>
<%
end sub

sub DrawListItem(intID, strStatus, strName, strPartNumber, intMinStock, intMaxStock, intQty)
%>
		<TR VALIGN=BOTTOM>
			<TD ALIGN=CENTER><A HREF="<%= STR_DETAIL_ITEM_SCRIPT %>id=<%= intID %>"><IMG SRC="<%= imagebase %>icon_product.gif" WIDTH="16" HEIGHT="16" ALT="<%= iif(blnCanEdit, "Edit", "View") %> This Item" BORDER="0"></A></TD>
			<TD><%= font(2) %><A HREF="<%= STR_DETAIL_ITEM_SCRIPT %>id=<%= intID %>"><%= strName %></A></TD>
			<TD NOWRAP><%= font(1) %>&nbsp;<%= iif(IsNull(strPartNumber), "n/a", strPartNumber) %>&nbsp;</TD>
			<TD NOWRAP ALIGN="center">&nbsp;<%= intMinStock & " - " & intMaxStock %>&nbsp;</TD>
			<TD NOWRAP ALIGN="center">&nbsp;<%= intQty %>&nbsp;</TD>
			<TD NOWRAP ALIGN="right"><%= font(1) %><%= GetStatusStr(strStatus, true) %>&nbsp;</TD>
<% if not blnPrintMode then %>
			<TD ALIGN=RIGHT><A HREF="<%= STR_DETAIL_ITEM_SCRIPT %>id=<%= intID %>"><%= iif(blnCanEdit, "Edit", "View") %></A></TD>
<% end if %>
		</TR>
<%
end sub

sub DrawListItemSep
%>
		<TR>
			<TD COLSPAN=7><TABLE BORDER=0 CELLPADDING=0 CELLSPACING=0 WIDTH="100%" BGCOLOR="<%= iif(blnPrintMode, cBlack, cVVLtGrey) %>"><TR><TD><%= spacer(1,1) %></TD></TR></TABLE></TD>
		</TR>
<%
end sub

sub DrawListFooter
%>
		</TABLE></TD>
	</TR>
	</TABLE></TD>
</TR>
</TABLE>
<%
end sub

sub xDrawPage
%>
<CENTER>

<B><%= strPageTitle %></B><BR>
<BR>

<TABLE BORDER="0" CELLPADDING="1" CELLSPACING="0">
<TR>
	<TD ALIGN="right" COLSPAN="2"><B>Total Inventory:</B></TD>
	<TD>&nbsp;<%= intInvCount %></TD>
</TR>
<TR>
	<TD ALIGN="right"><A HREF="inv_stock.asp"><B>Low Stock:</B></A></TD>
	<TD>&nbsp;<%= intLowCount %></TD>
	<TD>&nbsp;</TD>
	<TD ALIGN="right"><A HREF="inv_stock.asp"><B>High Stock:</B></A></TD>
	<TD>&nbsp;<%= intHighCount %></TD>
</TR>
</TABLE>

<FONT SIZE=1>
<BR>
<% if tfShowAll then %>
	[<A HREF="inv_stock.asp">Show Out of Stock Range</A>]
<% else %>
	[<A HREF="inv_stock.asp?all=1">Show All Items</A>]
<% end if %>
<BR>
(add a keyword search here)
<BR><BR>
</FONT>
<TABLE BORDER="1" CELLPADDING="2" CELLSPACING="0" WIDTH="540">
<TR BGCOLOR="#AAAAAA">
	<TD COLSPAN="6" ALIGN="center"><FONT SIZE="2"><B><% if tfShowAll then %>All Inventory Items<% else %>Inventory Items Out of Range<% end if %></B></FONT></TD>
</TR>
<TR BGCOLOR="#AAAAAA">
	<TD NOWRAP VALIGN="bottom"><B>Part #</B></TD>
	<TD NOWRAP WIDTH="100%" VALIGN="bottom"><B>Item Name</B></TD>
	<TD NOWRAP ALIGN="center"><B>Preferred<BR>Stock</B></TD>
	<TD NOWRAP ALIGN="center"><B>Current<BR>Stock</B></TD>
	<TD ALIGN="center" VALIGN="bottom"><B>Status</B></TD>
	<TD>&nbsp;</TD>
</TR>
<%
	if rsData.eof then
%>
<TR BGCOLOR="#DDDDDD">
	<TD COLSPAN="6" ALIGN="middle">This folder is empty.</TD>
</TR>
<%
	else
		dim tfAltColor
		while not rsData.eof
			tfAltColor = not tfAltColor
%>
<TR<%= iif(tfAltColor=true, " BGCOLOR=""#DDDDDD""", "") %>>
	<TD NOWRAP VALIGN="top"><%= rsData("vchPartNumber") %>&nbsp;</TD>
	<TD VALIGN="top"><%= rsData("vchItemName") %></TD>
<% if rsData("chrType") = "A" then %>
	<TD COLSPAN="3" VALIGN="top">&nbsp;</TD>
	<TD NOWRAP VALIGN="top">[<A HREF="inv_list.asp?parentid=<%= rsData("intID") %>">Open Folder</A>] [<A HREF="inv_fdetail.asp?id=<%= rsData("intID") %>"><%= iif(blnCanEdit, "Edit", "View") %></A>]</TD>
<% else %>
	<TD ALIGN="center" VALIGN="top"><%= rsData("intLowStock") %> - <%= rsData("intHighStock") %></TD>
	<TD ALIGN="center" VALIGN="top"><%= rsData("intStock") %></TD>
	<TD ALIGN="center" VALIGN="top"><%= GetStatusStr(rsData("chrStatus"), true) %></TD>
	<TD NOWRAP ALIGN="center" VALIGN="top">[<A HREF="inv_idetail.asp?id=<%= rsData("intID") %>"><%= iif(blnCanEdit, "Edit", "View") %></A>]</TD>
<% end if %>
</TR>
<%
			rsData.MoveNext
		wend
	end if
%>
</TABLE>
</CENTER>
<%
end sub
%>