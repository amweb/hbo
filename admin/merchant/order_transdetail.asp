<%@ LANGUAGE="VBScript" %>
<!--#include file="../config/incInit.asp"-->
<%

' =====================================================================================
' = File: order_transdetail.asp
' = File Version: 5.1 (beta)
' = Library Version: GoCart 5b1 (beta)
' = Copyright (c)1997-2003 American Web Services, Inc. All rights reserved.
' = Description:
' =   Display transaction details
' = Revision History:
' =   14jul2000 (5.1 beta) ssutterfield: general code cleanup/documentation
' = Description of Customizations:
' =   
' =====================================================================================

CheckAdminLogin

if not CheckUserAccess(ACC_RAW) then
	response.redirect "menu.asp"
end if

const STR_PAGE_TITLE = "Transaction Details"
const STR_PAGE_TYPE = "adminordertrans"

OpenConn

dim intID, strAction, intOrderID
strAction = Request("action")
intID = Request("id")
if IsNumeric(intID) then
	intID = CLng(intID)
else
	intID = 0
end if

intOrderID = Request("order")

if Request("action") = "submit" then
	AutoCheck Request, ""
	if FormErrors.count = 0 then
		dim dctSaveList
		set dctSaveList = Server.CreateObject("Scripting.Dictionary")
			dctSaveList.Add "@dtmUpdated", "GETDATE()"
			dctSaveList.Add "vchUpdatedByUser", Session(SESSION_MERCHANT_ULOGIN)
			dctSaveList.Add "vchUpdatedByIP", gstrUserIP
			dctSaveList.Add "!chrType", ""
			dctSaveList.Add "!chrStatus", ""
			dctSaveList.Add "!chrAccountType", ""
			dctSaveList.Add "!chrTransType", ""
			dctSaveList.Add "!vchAuthCode", ""
			dctSaveList.Add "!vchAVCode", ""
			dctSaveList.Add "!vchRefCode", ""
			dctSaveList.Add "!vchOrderCode", ""
			dctSaveList.Add "!vchDeclineCode", ""
			dctSaveList.Add "!vchShopperMsg", ""
			dctSaveList.Add "!%dtmTransDate", ""
			dctSaveList.Add "!#mnyTransAmount", ""
			dctSaveList.Add "!txtComments", ""
		SaveDataRecord STR_TABLE_TRANS, Request, intID, dctSaveList
		if intID = "" or intID = 0 then
			response.redirect "trans_search.asp"
		else
			response.redirect "order_detail.asp?id=" & intOrderID
		end if
	end if
end if

dim strSQL, rsData
strSQL = "SELECT * FROM " & STR_TABLE_TRANS & " WHERE intID = " & intID & " AND chrStatus<>'D'"
set rsData = gobjConn.execute(strSQL)
if rsData.eof then
	rsData.close
	set rsData = nothing
	response.redirect "order_list.asp"
end if


DrawPage rsData


rsData.close
set rsData = nothing
response.end

sub DrawPage(rsInput)
%>
<FORM ACTION="order_transdetail.asp" METHOD="POST" NAME="frm">
<INPUT TYPE="hidden" NAME="action" VALUE="submit">
<INPUT TYPE="hidden" NAME="id" VALUE="<%= intID %>">
<INPUT TYPE="hidden" NAME="order" VALUE="<%= intOrderID %>">
<%
	DrawFormHeader "100%", "Transaction Details", ""
	
	stlBeginStdTable "100%"
	response.write "<TR><TD>"
	
	stlBeginFormSection "530", 2
	stlRawCell "Record ID:"
	stlRawCell rsInput("intID")
	stlTextInput "chrType", 1, 1, rsInput, "Record Type", "IsEmpty", "Missing Record Type"
	stlTextInput "chrStatus", 1, 1, rsInput, "Record Status", "IsEmpty", "Missing Record Status"
	stlRawCell "Date Created:"
	stlRawCell rsInput("dtmCreated") & " by " & rsInput("vchCreatedByUser") & " " & rsInput("vchCreatedByIP")
	stlRawCell "Date Updated:"
	stlRawCell rsInput("dtmUpdated") & " by " & rsInput("vchUpdatedByUser") & " " & rsInput("vchUpdatedByIP")
	stlRawCell "Order ID:"
	stlRawCell rsInput("intOrderID")
	stlTextInput "chrAccountType", 1, 1, rsInput, "Account Type", "IsEmpty", "Missing Account Type"
	stlTextInput "chrTransType", 1, 1, rsInput, "Transaction Type", "IsEmpty", "Missing Transaction Type"
	stlTextInput "vchAuthCode", 12, 12, rsInput, "Authorization Code", "", ""
	stlTextInput "vchAVCode", 12, 12, rsInput, "Address Verify Code", "", ""
	stlTextInput "vchRefCode", 12, 12, rsInput, "ECHO Reference Code", "", ""
	stlTextInput "vchOrderCode", 20, 20, rsInput, "ECHO Order Code", "", ""
	stlTextInput "vchDeclineCode", 12, 12, rsInput, "Decline Code", "", ""
	stlTextInput "vchShopperMsg", 32, 255, rsInput, "ECHO Shopper Message", "", ""
	stlTextInput "dtmTransDate", 20, 20, rsInput, "Transaction Date", "IsDate", "Invalid Transaction Date"
	stlTextInput "mnyTransAmount", 20, 20, rsInput, "Transaction Amount", "IsNumeric", "Invalid Transaction Amount"
	stlTextArea "txtComments", 20, 45, rsInput("txtComments") & "", ""
	stlEndFormSection
	
	response.write "</TD></TR>"
	stlSubmit "Submit"
	
	stlEndStdTable
	response.write "</FORM>"
	
	SetFieldFocus "frm", "chrType"
end sub
%>